////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 2/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using DataProcessingBlocks;
using System.Globalization;
using EDI.Constant;

namespace EDI.Message
{
    /// <summary>
    /// This class is used to parse the message and find the details and content fo message
    /// details like To,From,Subject,Body,Attachmentname and Attachment Content.
    /// </summary>
    internal class POP3Message
    {
        #region Private Members

        private string m_message;
        private string[] m_splitMessage;
        private string[] m_splitMessage1;
       
        /// <summary>
        /// String is " "
        /// </summary>
        private const string SPACE = " ";

        /// <summary>
        ///  [0] "Content-Type: multipart/mixed"
        ///  [1] "Content-Type: multipart/relative"
        ///  [2] "Content-Type: multipart/alternative"
        ///  [3] "Content-Type: text/plain"
        ///  [4] "Content-Type: application/octet-stream"
        /// </summary>
        
        private static string[] ContentType =
        {
            "content-type: multipart/mixed",
            "content-type: multipart/relative",
            "content-type: multipart/alternative",
            "content-type: text/plain",
            "content-type: application/octet-stream"
        };

        /// <summary>
        ///  [0] "Date: "
        ///  [1] "To: "
        ///  [2] "From: "
        ///  [3] "Subject: "
        /// </summary>
        private static string[] MessageTags =
        {
            "date: ",
            "to: ",
            "from: ",
            "subject: ",
            "cc: "
        };
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the message string recived from server
        /// </summary>
        internal string Message
        {
            get { return m_message; }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="message">Message string recieved from the server</param>
        internal POP3Message(string message)
        {
            m_message = message;
            ParseMessage();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the To address in the message
        /// Only 1st address is currently parsed.
        /// </summary>
        /// <returns>To address</returns>
        internal string GetToAddress()
        {
            try
            {
                //Get substring from the "To: " tag
                //start index is first space char, the end index is CTRLF
               
                string tempMessage = m_message.Substring(m_message.ToLower().IndexOf(MessageTags[1], 0));
                int startIndex = tempMessage.IndexOf(SPACE, 0, tempMessage.Length) + 1;
                int midIndex = tempMessage.IndexOf(":", startIndex, tempMessage.Length - startIndex);
                int endIndex = tempMessage.Substring(startIndex, midIndex - startIndex).LastIndexOf(Constants.CRLF);
                return tempMessage.Substring(startIndex, endIndex).Replace("<","[").Replace(">","]").Replace("\"","").Trim();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

        }

        /// <summary>
        /// Get the Cc address in the message
        /// Only 1st address is currently parsed.
        /// </summary>
        /// <returns>To address</returns>
        internal string GetCcAddress()
        {
            try
            {
                //Get substring from the "Cc: " tag
                //start index is first space char, the end index is CTRLF
                if (m_message.ToLower().Contains(MessageTags[4].ToString()))
                {
                    string tempMessage = m_message.Substring(m_message.ToLower().IndexOf(MessageTags[4], 0));
                    int startIndex = tempMessage.IndexOf(SPACE, 0, tempMessage.Length) + 1;
                    int midIndex = tempMessage.IndexOf(":", startIndex, tempMessage.Length - startIndex);
                    int endIndex = tempMessage.Substring(startIndex, midIndex - startIndex).LastIndexOf(Constants.CRLF);
                    return tempMessage.Substring(startIndex, endIndex).Replace("<", "[").Replace(">", "]").Replace("\"", "").Trim();
                }
                return string.Empty;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

        }

        /// <summary>
        /// Retrieve the from email address
        /// </summary>
        /// <returns></returns>
        internal string GetFromAddress()
        {
            try
            {
                //Get substring from the "From: " tag
                //start index is first space char, the end index is CTRLF
                string tempMessage = m_message.Substring(m_message.ToLower().IndexOf(MessageTags[2], 0));
                int startIndex = tempMessage.IndexOf(SPACE, 0, tempMessage.Length) + 1;
                int endIndex = tempMessage.IndexOf(Constants.CRLF, startIndex, tempMessage.Length - startIndex);
                return tempMessage.Substring(startIndex, endIndex - startIndex).Replace("<","[").Replace(">","]").Replace("\"","").Trim();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

        }

        /// <summary>
        /// Gets the message subject.
        /// </summary>
        /// <returns></returns>
        internal string GetSubject()
        {
            try
            {
                //Get substring from the "Subject: " tag
                //start index is first space char, the end index is CTRLF
                string tempMessage = m_message.Substring(m_message.ToLower().IndexOf(MessageTags[3], 0));
                int startIndex = tempMessage.IndexOf(SPACE, 0, tempMessage.Length) + 1;
                int endIndex = tempMessage.IndexOf(Constants.CRLF, startIndex, tempMessage.Length - startIndex);
                return tempMessage.Substring(startIndex, endIndex - startIndex).Trim();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the date when message was recieved.
        /// </summary>
        /// <returns>Message Date</returns>
        internal DateTime GetMessageDate()
        {
            try
            {
                //Get substring from the "Date: " tag
                string tempMessage = m_message.Substring(m_message.ToLower().IndexOf(MessageTags[0], 0));

                int startIndex;
                if (tempMessage.Substring(MessageTags[0].Length,20).Contains(","))
                {
                    //The starting tag will be "," in the date ignore the day name.
                     startIndex= tempMessage.IndexOf(",", 0, tempMessage.Length) + 1;
                }
                else
                {
                    //The starting tag will be "," in the date ignore the day name.
                    startIndex = tempMessage.IndexOf(SPACE, 0, tempMessage.Length) + 1;
                }
                int endIndex;

                //End index will be the +/- sign depending on the time zone.
                //31 is the length of time string
                if (tempMessage.Substring(MessageTags[0].Length, 31).Contains("+"))
                {
                    endIndex = tempMessage.IndexOf("+", startIndex, tempMessage.Length - startIndex);
                }
                else
                {
                    endIndex = tempMessage.IndexOf("-", startIndex, tempMessage.Length - startIndex);
                }


                //return Convert.ToDateTime(DateTime.ParseExact(tempMessage.Substring(startIndex, endIndex - startIndex).Trim(), "dd MMM yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss"), CultureInfo.InvariantCulture);
                //Convert the string to DateTime and return.
                return Convert.ToDateTime(tempMessage.Substring(startIndex, endIndex - startIndex));
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return DateTime.Now;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return DateTime.Now;
            }
            catch (InvalidCastException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return DateTime.Now;
            }
           
        }

        /// <summary>
        /// Gets the message body
        /// </summary>
        /// <returns></returns>
        internal string GetBody()
        {
            try
            {
                //if the string contains "Content-Type: text/plain" then
                //staring from CTRLF + CTRLF till the end of message find the substring.
                if (m_splitMessage1 != null && m_splitMessage1[1].ToLower().Contains(ContentType[3]))
                    return m_splitMessage1[1].Substring(m_splitMessage1[1].IndexOf(Constants.CRLF + Constants.CRLF), m_splitMessage1[1].Length - m_splitMessage1[1].IndexOf(Constants.CRLF + Constants.CRLF)).Trim();
                return string.Empty;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentNullException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }


        }

        /// <summary>
        /// Gets the attachment name
        /// </summary>
        /// <returns>List</returns>
        internal List<string> GetAttachmentName()
        {
            if (m_splitMessage != null)
            {
                string nameTag = "name=";
                string inlineTag = "content-disposition: inline";
                string attachmentTag = "content-disposition: attachment";
                List<string> attachName = new List<string>();
                try
                {

                    for (int index = 2; index < m_splitMessage.Length - 1; index++)
                    {
                        //If the message contains "Content-Disposition:" tag then 
                        //get the attachment file name based on "name=" tag
                        if (m_splitMessage != null && (m_splitMessage[index].ToLower().Contains(attachmentTag) && m_splitMessage[index].ToLower().Contains(ContentType[3])) || m_splitMessage[index].ToLower().Contains(ContentType[4]) || m_splitMessage[index].ToLower().Contains(inlineTag))
                        {

                            int nameIndex = m_splitMessage[index].IndexOf(nameTag);
                           
                                attachName.Add(m_splitMessage[index].Substring(nameIndex + nameTag.Length,
                                        m_splitMessage[index].IndexOf(Constants.CRLF, nameIndex) - (nameIndex + nameTag.Length)).Replace('\"'
                                        , ' ').Trim());
                            
                        }
                    }
                    return attachName;

                }
                catch (ArgumentOutOfRangeException ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    return null;
                }
                catch (ArgumentNullException ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    return null;
                }
                catch (ArgumentException ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    return null;
                }
            }
        return null;
        }

        /// <summary>
        /// Gets the attachment content
        /// </summary>
        /// <returns>List</returns>
        internal List<string> GetAttachment()
        {
            if (m_splitMessage != null)
            {
            try
            {
              
                string attachmentTag = "content-disposition: attachment";
                string inlineTag = "content-disposition: inline";
                string encodingTag = "content-transfer-encoding: base64";
                List<string> attachFile = new List<string>();

                for (int index = 2; index < m_splitMessage.Length - 1; index++)
                {
                    //If the message contains attachment.
                    //if (m_splitMessage != null && (m_splitMessage[index].ToLower().Contains(attachmentTag) && m_splitMessage[index].ToLower().Contains(ContentType[3])) || m_splitMessage[index].ToLower().Contains(ContentType[4]))
                    if (m_splitMessage != null && (m_splitMessage[index].ToLower().Contains(attachmentTag) && m_splitMessage[index].ToLower().Contains(ContentType[3])) || m_splitMessage[index].ToLower().Contains(ContentType[4]) || m_splitMessage[index].ToLower().Contains(inlineTag))
                    {
                        int result = 0;
                        m_splitMessage[index] = m_splitMessage[index].Replace("=0A=\r\n", "\n");
                        m_splitMessage[index] = m_splitMessage[index].Replace("=\r\n", string.Empty);
                        m_splitMessage[index] = m_splitMessage[index].Replace("=0A=", string.Empty);
                        m_splitMessage[index] = m_splitMessage[index].Replace("=20", string.Empty);
                        try
                        {
                            //Check for the type of encoding and decode the attachment accordingly.
                            if (m_splitMessage[index].ToLower().Contains(encodingTag))
                            {
                                string dataAdd = m_splitMessage[index].Substring(m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF),m_splitMessage[index].Length - m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)).Trim();
                                attachFile.Add(Encoding.Default.GetString(Convert.FromBase64String(dataAdd)));
                            }
                            else
                                attachFile.Add(m_splitMessage[index].Substring(m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)
                                               , m_splitMessage[index].Length - m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)).Trim());
                        }
                        catch
                        {
                            result = 1;
                        }
                        //if format exception arises
                        if (result == 1)
                        {
                            if (m_splitMessage[index].ToLower().Contains(encodingTag))
                            {
                                try
                                {
                                    string dataAdd = m_splitMessage[index].Substring(m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF), m_splitMessage[index].Length - m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)).Trim();
                                    if (dataAdd.Contains("="))
                                        dataAdd = dataAdd.Replace("=", string.Empty);
                                    dataAdd = dataAdd + "A";
                                    attachFile.Add(Encoding.Default.GetString(Convert.FromBase64String(dataAdd)));
                                }
                                catch 
                                {
                                    result = 2;
                                }
                                if (result > 1)
                                {
                                    string dataAdd = m_splitMessage[index].Substring(m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF), m_splitMessage[index].Length - m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)).Trim();
                                    if (dataAdd.Contains("="))
                                        dataAdd = dataAdd.Replace("=", string.Empty);
                                    dataAdd = dataAdd + "AA";
                                    attachFile.Add(Encoding.Default.GetString(Convert.FromBase64String(dataAdd)));
                                }
                            }
                            else
                                attachFile.Add(m_splitMessage[index].Substring(m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)
                                               , m_splitMessage[index].Length - m_splitMessage[index].IndexOf(Constants.CRLF + Constants.CRLF)).Trim());

                        }
                        
                    }
                }
                return attachFile;
            }
            catch (FormatException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message + "\n" + "Attachment not appropriate.");
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
            catch (ArgumentNullException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }
        return null;

        }

        /// <summary>
        /// Splits the content of the message based the content type passed and the content type boundary value
        /// </summary>
        /// <param name="contentType">ContentType array value</param>
        /// <param name="message">String message</param>
        /// <param name="splitMessage">String [] to store the splits</param>
        internal void ContentTypeSplit(string contentType,string message,ref string[] splitMessage)
        {
            try
            {
                if (!contentType.Equals(ContentType[3]))
                {
                    //Call the GetBoundaryValue() to get the boundary value for the passed ContentType.
                    string boundary = GetBoundaryValue(contentType, message);

                    //Split the message based on the obtained boundary value.
                    if (message != null && boundary != string.Empty)
                    {
                        splitMessage = Regex.Split(message, "--" + boundary);
                    }
                }
                else
                {
                    splitMessage = new string[2];
                    splitMessage[1] = message;
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

        }
              
        /// <summary>
        /// Find the boundary vlaue for Content-Type: passed
        /// </summary>
        /// <param name="content">Content-Type:</param>
        /// <returns>Boundary value</returns>
        internal string GetBoundaryValue(string content,string message)
        {
            try
            {
            string boundaryTag = "boundary=";
            if (m_message != null && m_message.ToLower().Contains(content))
            {
                //find the start index of the boundary value for the Content-Type:
                int indexBoundary = message.IndexOf(boundaryTag, message.ToLower().IndexOf(content));

                //Return the boundary value for the Content-Type:
                return message.Substring(indexBoundary + boundaryTag.Length,
                                           message.IndexOf(Constants.CRLF, indexBoundary) - (indexBoundary + boundaryTag.Length)).Replace('\"', ' ').Trim();
            }
            return string.Empty;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
        }

        /// <summary>
        /// Split the message based on the content type and store in the string []
        /// </summary>
        internal void ParseMessage()
        {
            //If the message is of "Content-Type: multipart/mixed"
            if (this.m_message != null && this.m_message.ToLower().Contains(ContentType[0]))        
            {
                //Split the message based on "Content-Type: multipart/mixed" boundary and
                //store in m_splitMessage.
                //Split the message based on "Content-Type: multipart/alternative" and 
                //store in m_splitMessage1
                //m_splitMessage[1] contains the "Content-Type: multipart/alternative" boundary value.
                ContentTypeSplit(ContentType[0],this.m_message,ref m_splitMessage);
                if (this.m_splitMessage[1] != null && this.m_splitMessage[1].ToLower().Contains(ContentType[2]))
                {
                    ContentTypeSplit(ContentType[2], this.m_splitMessage[1], ref m_splitMessage1);
                }
                else if (this.m_splitMessage[1] != null && this.m_splitMessage[1].ToLower().Contains(ContentType[3]))
                {
                    ContentTypeSplit(ContentType[3], this.m_splitMessage[1], ref m_splitMessage1);
                }
            }
            //If the message is of "Content-Type: multipart/relative",
            else if (this.m_message != null && this.m_message.ToLower().Contains(ContentType[1]))
            {
                //Split the message based on "Content-Type: multipart/relative" boundary and
                //store in m_splitMessage.
                //Split the message based on "Content-Type: multipart/alternative" and 
                //store in m_splitMessage1
                ContentTypeSplit(ContentType[1], this.m_message, ref m_splitMessage);
                ContentTypeSplit(ContentType[2], this.m_splitMessage[1], ref m_splitMessage1);

            }
           //If the message is of "Content-Type: multipart/alternative",
            else if (this.m_message != null && this.m_message.ToLower().Contains(ContentType[2]))
            {
                //Split the message based on "Content-Type: multipart/alternative" and 
                //store in m_splitMessage1
                ContentTypeSplit(ContentType[2], this.m_message, ref m_splitMessage1);
            }
            //If the message is of "Content-Type: text/plain"
            else if (this.m_message != null && this.m_message.ToLower().Contains("mime-version: 1.0") && this.m_message.ToLower().Contains(ContentType[3]))
            {

                ContentTypeSplit(ContentType[3], this.m_message, ref m_splitMessage1);
                //ContentTypeSplit("mime-version: 1.0", this.m_message, ref m_splitMessage1);
            }
            else
            {
                ContentTypeSplit(Constants.CRLF + Constants.CRLF, this.m_message, ref m_splitMessage1);
            }
        }

        #endregion

    }
}
