////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 2/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using DBConnection;
using SQLQueries;
using EDI.Constant;
using System.Data;
using DataProcessingBlocks;
using System.IO;
using System.Data.Odbc;
using System.Configuration;
using System.Windows.Forms;
using System.Net.Mail;

namespace EDI.Message
{
    /// <summary>
    /// Controller class allows to save message and retrieve them
    /// </summary>
    public class MessageController   
    {
        /// <summary>
        /// Save the message in DB 
        /// </summary>
        /// <param name="message">Message object</param>
        /// <returns>bool</returns>
        public bool SaveMessage(Message message)
        {
           
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ021,
                                                  message.MessageDate.ToString(Constants.MySqlNewDateFormat),
                                                  message.ArchiveDate.ToString(Constants.MySqlNewDateFormat),
                                                  message.FromAddress,
                                                  message.ToAddress,
                                                  message.RefMessageId,
                                                  message.Subject,
                                                  message.Body,
                                                  message.Status,
                                                  message.ReadFlag,
                                                  message.Cc);
                //Execute query
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch(Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        public bool SaveAttachment(Message message)
        {

            try
            {
               

                for (int index = 0; index < message.MailAttachment.Count; index++)
                {

                    string Filepath = message.MailAttachment[index].AttachFile;

                    FileInfo flinfo = new FileInfo(Filepath);

                    //Open file into File stream.
                    FileStream ismfileStream = new FileStream(Filepath, FileMode.Open, FileAccess.Read);
                    //Reading File content into Binary.
                    BinaryReader ismfileReader = new BinaryReader(ismfileStream);

                    OdbcParameter paramFile = new OdbcParameter(Constants.ISMParameter.ToString(), ismfileReader.ReadBytes((int)ismfileStream.Length));

                    //Create query by adding parameters
                    string commandText = string.Format(Queries.SQ028,
                                                      message.MailAttachment[index].MessageID,
                                                      message.MailAttachment[index].AttachName,
                                                      "?",
                                                      message.MailAttachment[index].Status);
                    //Execute query
                    MySqlDataAcess.InsertISMFile(commandText, paramFile);
                    ismfileReader.Close();
                    ismfileStream.Close();
                    File.Delete(Filepath);
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Save the message in DB 
        /// </summary>
        /// <param name="message">Message object</param>
        /// <returns>bool</returns>
        public bool UpdateMessage(Message message)
        {

            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ024,
                                                  message.MessageDate.ToString(Constants.MySqlNewDateFormat),
                                                  message.ArchiveDate.ToString(Constants.MySqlNewDateFormat),
                                                  message.FromAddress,
                                                  message.ToAddress,
                                                  message.RefMessageId,
                                                  message.Subject,
                                                  message.Body,
                                                  message.Status,
                                                  message.Cc, 
                                                  message.MessageID);
                //Execute query
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Save the message in DB 
        /// </summary>
        /// <param name="message">Message object</param>
        /// <returns>bool</returns>
        public bool UpdateMessageStatus(int MessageId,string Stat)
        {

            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ053,
                                                   Stat,
                                                   MessageId);
                                                  
                //Execute query
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }


        public bool DeleteSelectedRow(int MessageId)
        {
            try
            {
                string commandText = string.Format(Queries.SQLDeleteMail, MessageId);
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }
        public bool DeleteAttachment(int MessageId)
        {
            try
            {
                string commandText = string.Format(Queries.SQLDeleteAttachment, MessageId);
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// Save the message in DB 
        /// </summary>
        /// <param name="message">Message object</param>
        /// <returns>bool</returns>
        public bool UpdateMessage(int messageID, MailMessage message)
        {

            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ034,
                                                  DateTime.Now.ToString(Constants.MySqlNewDateFormat),
                                                  message.From.Address,
                                                  message.To.ToString(),
                                                  message.Subject,
                                                  message.Body,
                                                  (int)MessageStatus.Draft,
                                                  message.CC.ToString(),
                                                  messageID);
                //Execute query
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Save the message in DB 
        /// </summary>
        /// <param name="message">Message object</param>
        /// <returns>bool</returns>
        public bool UpdateAttachment(int messageID, MailAttachment attachment)
        {
            try
            {
			//bug481
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ051,
                                                  attachment.Status, attachment.AttachFile,                                                 
                                                  messageID);
                //Execute query
                MySqlDataAcess.ExecuteNonQuery(commandText);
                return true;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }


       
        /// <summary>
        /// Retrieve messages from DB with given message status
        /// </summary>
        /// <param name="messageStatus1">Message Status enumeration</param>
        /// <param name="messageStatus2">Message Status enumeration</param>
        /// <returns>DataSet</returns>
        public DataSet ShowMessages(int messageStatus1, int messageStatus2, int messageStatus3, int messageStatus4)
        {
            
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ014,
                                                   messageStatus1.ToString(),
                                                   messageStatus2.ToString(),
                                                   messageStatus3.ToString(),
                                                   messageStatus4.ToString()
                                                   );
                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteDataset(commandText);
            
        }

        /// <summary>
        /// Retrieve messages from DB with given message status
        /// </summary>
        /// <param name="messageStatus1">Message Status enumeration</param>
        /// <param name="messageStatus2">Message Status enumeration</param>
        /// <returns>DataSet</returns>
        public DataSet GetMessage(int messageId, int messageStatus1, int messageStatus2, int messageStatus3, int messageStatus4)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ023,
                                                   messageId,
                                                   messageStatus1.ToString(),
                                                   messageStatus2.ToString(),
                                                   messageStatus3.ToString(),
                                                   messageStatus4.ToString());

                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetMaxMessageId()
        {
            try
            {
                //Create query 
                string commandText = Queries.SQ027;

                //Execute query and obtainits Dataset
                return Convert.ToInt32( MySqlDataAcess.ExecuteScaler(commandText).ToString());
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }
         
        //#endregion


        internal DataSet GetAttachmentNames(int messageId, int messageStatus1, int messageStatus2, int messageStatus3, int messageStatus4)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ029,
                                                   messageId,
                                                   messageStatus1.ToString(),
                                                   messageStatus2.ToString(),
                                                   messageStatus3.ToString(),
                                                   messageStatus4.ToString());
                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        internal DataSet GetAttachmentFiles(int messageId, int messageStatus1, int messageStatus2, int messageStatus3)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ030,
                                                   messageId,
                                                   messageStatus1.ToString(),
                                                   messageStatus2.ToString(),
                                                   messageStatus3.ToString());
                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        internal DataSet GetUnprocessedAttachmentFiles(int messageId)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ070,
                                                   messageId
                                                  );
                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }


        internal object GetAttachmentFile(int messageId, string fileName, int messageStatus1, int messageStatus2, int messageStatus3, int messageStatus4)
        {
            try
            {
                //Create query by adding parameters.
                string commandText = string.Format(Queries.SQ031,
                                                   messageId,
                                                   messageStatus1.ToString(),
                                                   fileName,
                                                   messageStatus2.ToString(),
                                                   messageStatus3.ToString(),
                                                   messageStatus4.ToString());
                //Execute query and obtainits Dataset
                return MySqlDataAcess.ExecuteScaler(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        internal void ChangeMessageStatus(int messageId, int Status)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ032,
                                                   messageId,
                                                    Status.ToString());
                //Execute query and obtainits Dataset
                 MySqlDataAcess.ExecuteNonQuery(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
               
            }
        }

        internal void ChangeAttachmentStatus(int messageId, int Status)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ033,
                                                   messageId,
                                                    Status.ToString());
                //Execute query and obtainits Dataset
                MySqlDataAcess.ExecuteNonQuery(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                
            }
        }

        internal DataSet GetDraftMails()
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ035,
                                                   ((int)MessageStatus.Draft).ToString() );
                //Execute query and obtainits Dataset
               return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageId"></param>
        internal void UpdateReadFlag(int messageId)
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ036,
                                                   messageId);
                //Execute query and obtainits Dataset
                MySqlDataAcess.ExecuteNonQuery(commandText);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);

            }
        }

        internal int GetDraftMessageCount()
        {
            try
            {
                //Create query by adding parameters
                string commandText = string.Format(Queries.SQ037,
                                                   ((int)MessageStatus.Draft).ToString());
                //Execute query and obtainits Dataset
                return Convert.ToInt32(MySqlDataAcess.ExecuteScaler(commandText));
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;

            }
        }
    }
}
