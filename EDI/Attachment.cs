////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 5/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Xml;
using System.Windows.Forms;
using System.Data;
using DataProcessingBlocks;
using EDI.Constant;

namespace EDI.Message
{
    /// <summary>
    /// Class contains members and properties for mail_attachment table.
    /// </summary>
    public class MailAttachment
    {
        #region Private Members

        /// <summary>
        /// Attachment Id
        /// </summary>
        private int m_attachId;
        /// <summary>
        /// Message Id
        /// </summary>
        private int m_messageId;
        /// <summary>
        /// Mail status flag
        /// </summary>
        private string m_status;
        /// <summary>
        /// Attachment content
        /// </summary>
        private string m_attachFile;
        /// <summary>
        /// Attachment Name
        /// </summary>
        private string m_attachName;

        #endregion

        #region Public Properties

        /// <summary>
        /// Attachment ID, auto incremented in DB
        /// </summary>
        public int AttachID
        {
            get { return m_attachId; }
            set { m_attachId = value; }
        }

        /// <summary>
        /// Reference MessageID
        /// </summary>
        public int MessageID
        {
            get { return m_messageId; }
            set { m_messageId = value; }
        }

        /// <summary>
        /// Attached file 
        /// </summary>
        public string AttachFile
        {
            get { return m_attachFile; }
            set { m_attachFile = value; }
        }

        /// <summary>
        /// Attached file name
        /// </summary>
        public string AttachName
        {
            get { return m_attachName; }
            set { m_attachName = value; }
        }

        /// <summary>
        /// Status of the attachment form MessageStatus enumeration
        /// </summary>
        public string Status
        {
            get { return m_status; }
            set { m_status = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Message rule for attachment.
        /// </summary>
        /// <param name="dtAttach"></param>
        /// <returns></returns>
        public bool IsAttachmentEqual(string dtAttach, string ruleName)
        {
            if (!string.IsNullOrEmpty(dtAttach))
            {
                try
                {
                    if (m_attachName.Equals(dtAttach))
                        return true;
                    else
                    {
                        CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("ExactAttachRuleMsg"));
                        return false;
                    }
                }
                catch 
                {
                    CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("ExactAttachRuleMsg"));
                    return false; 
                }
            }
            return true;
        }

        /// <summary>
        /// Message rule for attachment.
        /// </summary>
        /// <param name="dtAttach"></param>
        /// <returns></returns>
        public bool IsAttachmentMatch(string dtAttachMatch, string ruleName)
        {
            int result = -1;
            if (!string.IsNullOrEmpty(dtAttachMatch))
            {
                 try
                 {

                     if (m_attachName.ToLower().Contains(dtAttachMatch.ToLower()))
                         return true;
                         // if (result != (CultureInfo.CurrentUICulture.CompareInfo.IndexOf(m_attachName, dtAttachMatch, CompareOptions.IgnoreCase)))
                         
                     else
                     {
                         CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("SpecificAttachRuleMsg"));
                         return false;
                     }
                 }
                 catch 
                 {
                     CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("SpecificAttachRuleMsg"));                     
                     return false; 
                 }             
            }
            return true;
        }

        /// <summary>
        /// Check Mappings column with the response attachment columns.
        /// </summary>
        /// <param name="dtMapping"></param>
        /// <returns></returns>
        public DataTable IsMappingsMatch(string dtMapping , string ruleName)
        {
            if (string.IsNullOrEmpty(dtMapping))
                return null;
            List<string> attachmentHeaders = GetHeaderColumns();
            List<string> mappingHeaders = new List<string>();
            List<string> mappingNodes = new List<string>();
            System.Collections.Hashtable constantMappingsData = new System.Collections.Hashtable();


            //Axis Bug No 446 EDI Rule         changes done by - Developer Sunita Salunkhe 15 Oct 2015

            #region 1. Get Setings.xml path
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            
            #endregion

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(m_settingsPath);
            }
            catch (Exception) { }

            XmlNode root = (XmlNode)xmlDoc.DocumentElement;
            
            #region GET USERMAPPINGS.
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "UserMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in userMappingsXML)
                    {
                        if (node.FirstChild.Name == "name" && node.FirstChild.InnerText.ToString() == dtMapping)
                        {
                            //Start from the first columnName of mapping.
                            for (int nodeName = 3; nodeName < node.ChildNodes.Count; nodeName++)
                            {
                                if (node.ChildNodes[nodeName].InnerText != string.Empty)
                                {
                                    //Get the headers in the list.
                                    mappingHeaders.Add(node.ChildNodes[nodeName].InnerText.ToString().Trim());
                                    //Get its corresonding name which is in the QuickBook or MYOB.
                                    mappingNodes.Add(node.ChildNodes[nodeName].Name.ToString().Trim());
                                }

                            }                          
                        }
                    }
                }
            }

            #region CONSTANT MAPPINGS

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in userMappingsXML)
                    {
                        if (node.FirstChild.Name == "name" && node.FirstChild.InnerText.ToString() == dtMapping)
                        {
                            //Start from the first columnName of mapping.
                            for (int nodeName = 3; nodeName < node.ChildNodes.Count; nodeName++)
                            {
                                if (node.ChildNodes[nodeName].InnerText != string.Empty)
                                {
                                    ////Get the headers in the list.
                                    //mappingHeaders.Add(node.ChildNodes[nodeName].InnerText.ToString());
                                    ////Get its corresonding name which is in the QuickBook or MYOB.
                                    //mappingNodes.Add(node.ChildNodes[nodeName].Name.ToString());
                                    object keyColumnName = node.ChildNodes[nodeName].Name.ToString().Trim();
                                    object keyColumnvalue = node.ChildNodes[nodeName].InnerText.ToString().Trim();

                                    constantMappingsData.Add(keyColumnName, keyColumnvalue);
                                }
                            }
                        }
                    }
                }
            }
                            
            #endregion


            if (!MatchHeaders(mappingHeaders, attachmentHeaders))
            {
                CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("HeadersRuleMsg"));
                return null;
            }

            DataTable dt = GetDataTable(mappingHeaders, mappingNodes,constantMappingsData);

            return dt;

            #endregion

            #region GET CONSTANTMAPPINGS.

            //for (int i = 0; i < root.ChildNodes.Count; i++)
            //{
            //    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
            //    {
            //        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
            //        foreach (XmlNode node in userMappingsXML)
            //        {
            //            if (node.FirstChild.Name == "name" && node.FirstChild.InnerText.ToString() == dtMapping)
            //            {
            //                //Start from the first columnName of mapping.
            //                for (int nodeName = 3; nodeName < node.ChildNodes.Count; nodeName++)
            //                {
            //                    if (node.ChildNodes[nodeName].InnerText != string.Empty)
            //                    {
            //                        //Get the headers in the list.
            //                        mappingHeaders.Add(node.ChildNodes[nodeName].InnerText.ToString());
            //                        //Get its corresonding name which is in the QuickBook or MYOB.
            //                        mappingNodes.Add(node.ChildNodes[nodeName].Name.ToString());
            //                    }

            //                }
            //                if (!MatchHeaders(mappingHeaders, attachmentHeaders))
            //                    return null;
            //                DataTable dt = GetDataTable(mappingHeaders, mappingNodes);

            //                return dt;
            //            }
            //        }
            //    }
            //}
            #endregion

            //return null;
        }

        /// <summary>
        /// Get the header columns from  the incoming attachment.
        /// </summary>
        /// <returns></returns>
        public List<string> GetHeaderColumns()
        {
            List<string> headerNames = new List<string>();
            try
            {
                if (m_attachName == null)
                {
                    CommonUtilities.WriteErrorLog("Attachment is not of proper format.Please check mail attachment.");
                    return headerNames;
                }
                
                string[] headerString = m_attachFile.ToString().Split('\n');              
                //For the text file.
                if (m_attachName.Contains(".txt"))
                {
                    char[] txtSeparator = { '\t', '\r' ,'|',','};
                    string[] columns = headerString[0].ToString().Split(txtSeparator);

						//bug481
                    for (int i = 0; i<columns.Length; i++)
                        if (columns[i] != string.Empty && columns[i] != null)
                        {
                            headerNames.Add(columns[i].Trim());
                        }
                }

                //For the csv file.
                if (m_attachName.Contains(".csv"))
                {
                    char[] csvSeparator = { ',', '\r' };
                    string[] columns = headerString[0].ToString().Split(csvSeparator);
					//bug481

                    for (int i = 0; i < columns.Length; i++)
                        if (columns[i].ToString() != string.Empty && columns[i] != null)
                        {
                            headerNames.Add(columns[i].Trim());
                        }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog("GetHeaderColumns() method failed.");
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
            
            return headerNames;
        }

        /// <summary>
        /// Matches the attachment headers with the mapping headers.
        /// </summary>
        /// <param name="mapHeaders"></param>
        /// <param name="attachHeaders"></param>
        /// <returns></returns>
        public bool MatchHeaders(List<string> mapHeaders, List<string> attachHeaders)
        {
            foreach (string head in mapHeaders)
            {
                if (attachHeaders.Contains(head))
                    continue;
                else
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Prepare a datatable from the attachment and mapping which will be imported into the connected QuickBook\MYOB.
        /// </summary>
        /// <param name="mHeaders">mapping Headers</param>
        /// <param name="nodeHeaders">Corresponding Node Headers</param>
        /// <returns></returns>
        public DataTable GetDataTable(List<string> mHeaders , List<string> nodeHeaders , System.Collections.Hashtable ConstantMappingData)
        {
            string[] fields = null;
            DataTable dt = new DataTable();
            DataTable newDt = new DataTable();

            #region Get Attachment data in datatable.
            try
            {
                string[] rows = m_attachFile.ToString().Split('\n');              
                char[] separator = { '\t','\r','|',',' };  
                string[] columns = rows[0].ToString().Split(separator);

                foreach (string colmn in columns)
                {
                    if (dt.Columns.Contains(colmn.ToString()))
                    {
                        if (!string.IsNullOrEmpty(colmn))
                            dt.Columns.Add(colmn.ToString().Trim() + "new");
                    }
                    else
                    {                        
                        dt.Columns.Add(colmn.ToString().Trim());
                    }
                }
                
                //create a datatable which  will contain the whole data from the attachment.
                for (int i = 1; i <= rows.Length - 1; i++)
                {
                    fields = rows[i].Split(separator);
                    
                    DataRow nRow = dt.NewRow();
                    for (int temp = 0; temp < columns.Length; temp++)
                    {
                        if(temp < fields.Length)
                        nRow[temp] = fields[temp];
                    }
                        
                    dt.Rows.Add(nRow);
                }
            }
            catch{ }

            #endregion

            #region Store the mapping column data from the above datatable into a new datatable.
            try
            {

                #region Add mapped column name from QuickBook/.
                for (int colCount = 0;colCount < mHeaders.Count ; colCount++)
                {                 
                    if(dt.Columns.Contains(mHeaders[colCount]))
                    {
                        //Replace name of the columns with the Quickbook/MYOB column name.
                        string temp = dt.Columns[mHeaders[colCount]].ColumnName.Replace(dt.Columns[mHeaders[colCount]].ColumnName, nodeHeaders[colCount].ToString());                        
                        dt.Columns[mHeaders[colCount]].ColumnName = dt.Columns[mHeaders[colCount]].ColumnName.Replace(dt.Columns[mHeaders[colCount]].ColumnName, temp);
                        newDt.Columns.Add(temp);
                    }
                }
                #endregion

                #region Add data from the attachment into the new datatable.
                for (int temp = 0; temp < dt.Rows.Count; temp++)
                {
                    object[] rowArray = new object[newDt.Columns.Count];
                    for (int columnIndex = 0; columnIndex < newDt.Columns.Count; columnIndex++)
                    {
                        rowArray[columnIndex] = dt.Rows[temp][newDt.Columns[columnIndex].ColumnName].ToString();
                    }
                    //Load mapping data row into the new datatable.
                    newDt.LoadDataRow(rowArray, LoadOption.PreserveChanges);
                }
                #endregion
            }
            catch (Exception) { }

            #endregion

            //Adding the constant data mappings:
            if(ConstantMappingData.Count != 0)
            {
                foreach (System.Collections.DictionaryEntry mappingData in ConstantMappingData)
                {
                    newDt.Columns.Add(mappingData.Key.ToString());

                    for (int index = 0; index < newDt.Rows.Count; index++)
                    {
                        newDt.Rows[index][mappingData.Key.ToString()] = mappingData.Value.ToString();
                    }
                }
            }

            return newDt;
        }

        #endregion
    }
}
