////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 2/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Text;
using DataProcessingBlocks;
using System.Data;
using MailSettings;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.ComponentModel;
using System.Net;
using EDI.Constant;

namespace EDI.Message
{
    /// <summary>
    /// Class provides functions to display Inbox,Outbox and Sent Items
    /// Calls to Send Recieve are made throught this class by creating the POP/SMTP server objects.
    /// </summary>
    public class Outlook
    {

        #region Private Members

        /// <summary>
        /// Pop3 host server
        /// </summary>
        private string m_popServer;
        /// <summary>
        /// Incoming port
        /// </summary>
        private int m_port;
        /// <summary>
        /// Mail username
        /// </summary>
        private string m_username;
        /// <summary>
        /// Mail Password
        /// </summary>
        private string m_password;
        /// <summary>
        /// Is SSL used
        /// </summary>
        private bool m_isSSL;
        /// <summary>
        /// Positive POP3 server response
        /// </summary>
        private const string OK = "+OK";
        string m_Connected = null;

        #endregion

        #region Constructor
        public Outlook() { }

        public Outlook(string connectedSoftware)
        {
            this.m_Connected = connectedSoftware;
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Set the pop3 password
        /// </summary>
        public string Password
        {
            set { m_password = value; }
        }

        /// <summary>
        /// Set the pop3 Username
        /// </summary>
        public string Username
        {
            set { m_username = value; }
        }

        #endregion

        #region Public Method

        /// <summary>
        /// Get the pop3 connection details from the database
        /// </summary>
        internal void POP3ConnectionDetails()
        {
            //Create object of MailSetup and retrieve the mail setup details
            
             using (DataSet dataSet = MailSetup.GetMailSetupDetails())
            {
                //If mail settings popserver,port,username,password,SSL flag are found
                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    //PopServer name
                    this.m_popServer = dataSet.Tables[0].Rows[0].ItemArray[5].ToString();
                    //Incoming port number
                    this.m_port = Convert.ToInt32(dataSet.Tables[0].Rows[0].ItemArray[2]);
                    //Email Id
                    this.m_username = dataSet.Tables[0].Rows[0].ItemArray[7].ToString();
                    //Email password
                    this.m_password = dataSet.Tables[0].Rows[0].ItemArray[9].ToString();
                    //SSL authentication supported
                    this.m_isSSL = Convert.ToBoolean(dataSet.Tables[0].Rows[0].ItemArray[4]);

                    if (this.m_password == string.Empty)
                        throw new ArgumentNullException("Password empty");
                }
                //else
                //    throw new Exception("Please create mail settings");
            }
        }

        /// <summary>
        /// Gets the total count of messages on server.
        /// </summary>
        /// <returns>count</returns>
        internal int GetMessageCount()
        {
            //create Pop3MailClient object 
            //connect and get the count of messages and disconnect
            Pop3MailClient pop3MailClient = new Pop3MailClient(this.m_popServer, this.m_port, this.m_isSSL, this.m_username, this.m_password);
            try
            {
                pop3MailClient.Connect();
            }
            catch { }
            string[] serverResponse = pop3MailClient.GetMessageCount().Split(' ');
            try
            {
                pop3MailClient.Disconnect();
            }
            catch { }
            if (serverResponse.Length == 3 && serverResponse[0] == OK)
            {
                return Convert.ToInt32(serverResponse[1]);
            }
            return 0;
        }

        /// <summary>
        /// Fecthes message with given message number from server
        /// </summary>
        /// <param name="messageNo">Message number</param>
        /// <returns>POP3Message</returns>
        internal POP3Message FetchFromServer(int messageNo)
        {
            //Create Pop3MailClient object
            //connect to the server and download the message disconnect
            //return the pop3 message object
            Pop3MailClient pop3MailClient = new Pop3MailClient(this.m_popServer, this.m_port, this.m_isSSL, this.m_username, this.m_password);
            pop3MailClient.Connect();
            string emailMessage;
            pop3MailClient.GetRawEmail(messageNo, out emailMessage);
            POP3Message pop3Message = new POP3Message(emailMessage);
            pop3MailClient.Disconnect();
            return pop3Message;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal Pop3MailClient Connect()
        {
            Pop3MailClient pop3MailClient = new Pop3MailClient(this.m_popServer, this.m_port, this.m_isSSL, this.m_username, this.m_password);
            pop3MailClient.Connect();
            return pop3MailClient;
        }

        /// <summary>
        /// Download and Save the message in DB.
        /// </summary>
        internal void Save(Pop3MailClient pop3MailClient, int index)
        {
            //create MessageController and Pop3MailClient objects.
            MessageController messageController = new MessageController();
            string emailMessage = string.Empty;
            //Connect to pop3 server.

            //Get raw message format for a particular message number.
            pop3MailClient.GetRawEmail(index, out emailMessage);

            //POP3 message object for parsing.
            POP3Message pop3Message = new POP3Message(emailMessage);
            //Create Message object for saving the message.
            Message message = new Message();

            //obtain various message fields by parsing the message and assign to
            //message object
            message.MessageDate = pop3Message.GetMessageDate();
            message.ArchiveDate = DateTime.Now;
            message.FromAddress = pop3Message.GetFromAddress();
            message.ToAddress = pop3Message.GetToAddress();
            message.Cc = pop3Message.GetCcAddress();

            //This will be avial when the reponse is recieved from trading partner as client id.
            //currently hard coded for testing purpose.
            message.RefMessageId = "1";
            message.Subject = pop3Message.GetSubject();
            message.Body = pop3Message.GetBody();
            message.Status = ((int)MessageStatus.Recieved).ToString();
            message.ReadFlag = 0;
            //save the message object in DB
            messageController.SaveMessage(message);

            //Get attachment names and files.
            List<string> attachNames = pop3Message.GetAttachmentName();
            List<string> attachFiles = pop3Message.GetAttachment();

            if (attachNames != null && attachFiles != null && attachNames.Count == attachFiles.Count)
            {
                string logDirectory = GetDirectory("\\Recieve");

                int messageID = messageController.GetMaxMessageId();
                for (int i = 0; i < attachNames.Count; i++)
                {
                    MailAttachment mailattachment = new MailAttachment();
                    mailattachment.MessageID = messageID;
                    mailattachment.AttachName = attachNames[i];
                    string Filepath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory + attachNames[i] : logDirectory + Path.DirectorySeparatorChar + attachNames[i];
                    //Information about files.
                    File.WriteAllText(Filepath, attachFiles[i]);
                    mailattachment.AttachFile = Filepath;
                    mailattachment.Status = ((int)MessageStatus.Recieved).ToString();
                    message.MailAttachment.Add(mailattachment);
                }
                //save the message object in DB.
                messageController.SaveAttachment(message);
            }
            pop3MailClient.DeleteEmail(index);
        }          
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pop3MailClient"></param>
        internal void Disconnect(Pop3MailClient pop3MailClient)
        {
            //Disconnect to pop3 server.
            pop3MailClient.Disconnect();
        }


        /// <summary>
        /// Save a message as draft in the database
        /// </summary>
        /// <param name="sendMessage">Reference of SendMessage screen</param>
        internal bool SaveDraft(SendMessage sendMessage)
        {
            Message message = new Message();
            message.MessageID = sendMessage.MessageId;
            message.MessageDate = sendMessage.MessageDate;
            message.ArchiveDate = DateTime.Now;
            message.ToAddress = sendMessage.textBoxTo.Text;
            message.Cc = sendMessage.textBoxCc.Text;
            message.FromAddress = " ";
            message.RefMessageId = sendMessage.RefMessageId;
            message.Subject = sendMessage.textBoxSubject.Text;
            message.Body = sendMessage.richTextBoxBody.Text;
            message.Status = ((int)MessageStatus.Draft).ToString();
            if (sendMessage.MessageId == 0)
                return new MessageController().SaveMessage(message);
            else
                return new MessageController().UpdateMessage(message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textbox"></param>
        /// <returns></returns>
        internal bool CheckEmailAddresses(string emailAddresess)
        {
            string[] emailAddresses = GetMailAddresses(emailAddresess);

            for (int index = 0; index < emailAddresses.Length; index++)
            {
                if (!CommonUtilities.IsEmail(emailAddresses[index].Trim()))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textbox"></param>
        /// <returns></returns>
        internal string[] GetMailAddresses(string emailAddresess)
        {
            string[] emailAddresses = null;
            while(emailAddresess.Contains("\""))
            {
                emailAddresess = emailAddresess.Remove(emailAddresess.IndexOf('\"'), emailAddresess.IndexOf('\"', emailAddresess.IndexOf('\"') + 1) - emailAddresess.IndexOf('\"') + 1).Replace(',', ' ');
            }
            if (emailAddresess.Contains(";"))
            {
                emailAddresses = Regex.Split(emailAddresess.Replace('<', ' ').Replace('>', ' ').Replace('\t',' '), ";");
            }
            else if (emailAddresess.Contains(","))
            {
                emailAddresses = Regex.Split(emailAddresess.Replace('<', ' ').Replace('>', ' ').Replace('\t', ' '), ",");
            }
            else
            {
                emailAddresses = Regex.Split(emailAddresess.Replace('<', ' ').Replace('>', ' ').Trim(), " ");
            }
                
            for (int index = 0; index < emailAddresses.Length; index++)
            {
                string email = emailAddresses[index];
                //if (email.Contains("\""))
                //    email = email.Substring(email.LastIndexOf('"') + 1, email.Length - email.LastIndexOf('"') - 1);
                if (email.Contains(","))
                    email = email.Substring(email.LastIndexOf(',') + 1, email.Length - email.LastIndexOf(',') - 1).Trim();
                emailAddresses[index] = email.Trim();
            }

            return emailAddresses;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailMessage"></param>
        /// <param name="messageId"></param>
        internal void GetAttachments(ref MailMessage mailMessage, int messageId)
        {
            DataSet dataSetAttachments = new MessageController().GetAttachmentFiles(messageId, (int)MessageStatus.Draft, 0,0);

            string logDirectory = GetDirectory("\\Send");

            if (dataSetAttachments != null && dataSetAttachments.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dataRow in dataSetAttachments.Tables[0].Rows)
                {
                    byte[] buffer =  (byte [])dataRow.ItemArray[1];
                  
                    //Filepath to create log files in directory .
                    string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory + dataRow.ItemArray[0] : logDirectory + Path.DirectorySeparatorChar + dataRow.ItemArray[0];
                    //Checking file existance for appending logs.
                    
                    if (File.Exists(filePath))
                    {
                         File.Delete(filePath);
                    }
                    
                    File.WriteAllBytes(filePath, buffer);
                    Attachment attachment = new Attachment(filePath);
                    mailMessage.Attachments.Add(attachment);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal string GetDirectory(string directory)
        {
            //Create log directory for storing errors logs.
            string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

            //Adding Directory separate character to Logdirectory file path.
            if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
            {
                logDirectory = Application.StartupPath + logDirectory + directory;
                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    else
                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                }
                catch { }
            }
            //Checking if directory does not exists then create it.
            if (!Directory.Exists(logDirectory))
            {
                Directory.CreateDirectory(logDirectory);
            }
            return logDirectory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailAddressOf"></param>
        /// <param name="addressString"></param>
        internal void SetAddresses(MailAddressCollection mailAddressOf, string addressString)
        {
            if (addressString != string.Empty)
            {
                if (CheckEmailAddresses(addressString))
                {
                    MailAddress mailAddress;
                    string[] ToAddresses = GetMailAddresses(addressString);
                    for (int index = 0; index < ToAddresses.Length; index++)
                    {
                        mailAddress = new MailAddress(ToAddresses[index].Trim());
                        mailAddressOf.Add(mailAddress);
                    }
                }
                else
                {
                    throw new ArgumentException(MessageCodes.GetValue("Zed Axis ER013"));
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSetSettings"></param>
        /// <returns></returns>
        internal MailMessage Create(int messageId,string from, string to, string cc, string subject, string body,string displayName)
        {
            MailAddress From = new MailAddress(from,displayName);
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.From = From;
            mailMessage.ReplyTo = From;
            mailMessage.Sender = From;
            SetAddresses(mailMessage.To, to.Replace("["," ").Replace("]"," ").Trim());
            SetAddresses(mailMessage.CC, cc);
            GetAttachments(ref mailMessage, messageId);
            return mailMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendMessage"></param>
        /// <returns></returns>
        public bool Send(SendMessage sendMessage)
        {
            using (DataSet dataSetMailSettings = MailSetup.GetMailSetupDetails())
            {
                if (dataSetMailSettings.Tables[0].Rows.Count == 1)
                {
                    string smtpServer = dataSetMailSettings.Tables[0].Rows[0].ItemArray[6].ToString();
                    int outPort = Convert.ToInt32(dataSetMailSettings.Tables[0].Rows[0].ItemArray[3].ToString());
                    string displayName = dataSetMailSettings.Tables[0].Rows[0].ItemArray[7].ToString();

                    SmtpClient smtp = new SmtpClient(smtpServer, outPort);
                    string from = dataSetMailSettings.Tables[0].Rows[0].ItemArray[8].ToString();
                    MailMessage mailMessage = Create(sendMessage.MessageId,
                                                        from, 
                                                        sendMessage.textBoxTo.Text.Trim(), 
                                                        sendMessage.textBoxCc.Text.Trim(), 
                                                        sendMessage.textBoxSubject.Text.Trim(), 
                                                        sendMessage.richTextBoxBody.Text.Trim(),
                                                        displayName);
                    MessageController messageController = new MessageController();
                    messageController.UpdateMessage(sendMessage.MessageId,mailMessage);
                    NetworkCredential networkCredential = new NetworkCredential(m_username, m_password);
                    smtp.Credentials = networkCredential;
                    smtp.Send(mailMessage);
                    messageController.ChangeMessageStatus(sendMessage.MessageId, (int)MessageStatus.Sent);
                    messageController.ChangeAttachmentStatus(sendMessage.MessageId, (int)MessageStatus.Sent);
                    return true;
                }
                else
                {
                    throw new ArgumentNullException();                    
                }
            }
        }

        public bool Send(DataRow dataRow)
        {
            using (DataSet dataSetMailSettings = MailSetup.GetMailSetupDetails())
            {
                if (dataSetMailSettings != null && dataSetMailSettings.Tables[0].Rows.Count == 1)
                {
                    string smtpServer = dataSetMailSettings.Tables[0].Rows[0].ItemArray[6].ToString();
                    int outPort = Convert.ToInt32(dataSetMailSettings.Tables[0].Rows[0].ItemArray[3].ToString());
                    string displayName = dataSetMailSettings.Tables[0].Rows[0].ItemArray[7].ToString();

                    SmtpClient smtp = new SmtpClient(smtpServer, outPort);
                    string from = dataSetMailSettings.Tables[0].Rows[0].ItemArray[8].ToString();

                    int messageId = Convert.ToInt32(dataRow.ItemArray[0].ToString());
                    string to = dataRow.ItemArray[1].ToString();
                    string cc = dataRow.ItemArray[2].ToString();
                    string subject = dataRow.ItemArray[3].ToString();
                    string body = dataRow.ItemArray[4].ToString();

                    MailMessage mailMessage = Create(messageId, from, to, cc, subject, body, displayName);
                    MessageController messageController = new MessageController();
                    messageController.UpdateMessage(messageId, mailMessage);

                    NetworkCredential networkCredential = new NetworkCredential(m_username, m_password);
                    smtp.Credentials = networkCredential;
                    smtp.Send(mailMessage);

                    messageController.ChangeMessageStatus(messageId, (int)MessageStatus.Sent);
                    messageController.ChangeAttachmentStatus(messageId, (int)MessageStatus.Sent);

                    return true;
                }
                else
                {
                    throw new ArgumentNullException();

                }
            }
        }

                          
        /// <summary>
        /// Show messages in Inbox.
        /// </summary>
        /// <returns></returns>
        internal DataSet ShowInbox()
        {
            //Get messages with message status Recieved and Processed
            MessageController messageController = new MessageController();
            return messageController.ShowMessages((int)MessageStatus.Recieved, (int)MessageStatus.Processed ,(int)MessageStatus.Failed,(int)MessageStatus.Pending);
        }

        /// <summary>
        /// Show messages in outbox
        /// </summary>
        /// <returns></returns>
        internal DataSet ShowOutbox()
        {
            //Get messages with message status Draft
            MessageController messageController = new MessageController();
            return messageController.ShowMessages((int)MessageStatus.Draft, 0,0,0);
        }

        /// <summary>
        /// Show messages in Sentitems.
        /// </summary>
        /// <returns></returns>
        internal DataSet ShowSentItems()
        {
            //Get messages with message status Sent.
            MessageController messageController = new MessageController();
            return messageController.ShowMessages((int)MessageStatus.Sent, (int)MessageStatus.Dispatched,0,0);
        }   
      

        public string UpdateMail(int result)
        {
            string res = string.Empty;
            if (result == 0)
            {
                //Message nMessage;

            }
            return res;
        }
        #endregion
    }
}
