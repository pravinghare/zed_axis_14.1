using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class AddressList : Form
    {
        #region Private Members

        private string m_addresses;

        #endregion

        #region Public Properties

        public string Addresses
        {
            get { return m_addresses; }
        }

        #endregion

        public AddressList(DataSet dataSetAddress)
        {
            InitializeComponent();
            for (int index = 0; index < dataSetAddress.Tables[0].Rows.Count; index++)
                checkedListBoxAddress.Items.Add("\"" + dataSetAddress.Tables[0].Rows[index].ItemArray[1].ToString().ToUpper() + "\", " + dataSetAddress.Tables[0].Rows[index].ItemArray[3]);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            m_addresses = string.Empty;
            CheckedListBox.CheckedItemCollection checkedItem= checkedListBoxAddress.CheckedItems;

            for (int index = 0; index < checkedItem.Count;index++ )
            {
               if(m_addresses ==string.Empty)
                   m_addresses = m_addresses + checkedItem[index].ToString();
               else
                m_addresses=m_addresses+"; "+checkedItem[index].ToString();
            }
            this.Close();
        }
    }
}