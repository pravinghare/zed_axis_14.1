namespace MessageMonitoringService
{
    partial class MessageMonitoring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageMonitoring));
            this.labelHeader = new System.Windows.Forms.Label();
            this.dataGridViewMessageServices = new Telerik.WinControls.UI.RadGridView();
            this.folderBrowserDialogService = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessageServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessageServices.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = true;
            this.labelHeader.Location = new System.Drawing.Point(192, 31);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(341, 13);
            this.labelHeader.TabIndex = 0;
            this.labelHeader.Text = "The following locations are being monitored for messages ";
            // 
            // dataGridViewMessageServices
            // 
            this.dataGridViewMessageServices.AutoScroll = true;
            this.dataGridViewMessageServices.Location = new System.Drawing.Point(24, 61);
            // 
            // 
            // 
            this.dataGridViewMessageServices.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dataGridViewMessageServices.MasterTemplate.EnableAlternatingRowColor = true;
            this.dataGridViewMessageServices.MasterTemplate.EnableFiltering = true;
            this.dataGridViewMessageServices.MasterTemplate.MultiSelect = true;
            this.dataGridViewMessageServices.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.dataGridViewMessageServices.MasterTemplate.ShowFilteringRow = false;
            this.dataGridViewMessageServices.MasterTemplate.ShowHeaderCellButtons = true;
            this.dataGridViewMessageServices.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridViewMessageServices.Name = "dataGridViewMessageServices";
            this.dataGridViewMessageServices.ShowHeaderCellButtons = true;
            this.dataGridViewMessageServices.Size = new System.Drawing.Size(689, 324);
            this.dataGridViewMessageServices.TabIndex = 17;
            this.dataGridViewMessageServices.Text = "dataGridViewMessageServices";
            // 
            // MessageMonitoring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(743, 437);
            this.Controls.Add(this.dataGridViewMessageServices);
            this.Controls.Add(this.labelHeader);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageMonitoring";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Message Monitoring";
            this.Load += new System.EventHandler(this.MessageMonitoring_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessageServices.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessageServices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHeader;
        public Telerik.WinControls.UI.RadGridView dataGridViewMessageServices;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogService;

    }
}