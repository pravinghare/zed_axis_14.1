namespace DataProcessingBlocks
{
    partial class ErrorSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorSummary));
            this.buttonErrorMsgSkip = new System.Windows.Forms.Button();
            this.pictureBoxError = new System.Windows.Forms.PictureBox();
            this.labelQBErrorMessage = new GrowLabel();
            this.buttonQuit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxError)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonErrorMsgSkip
            // 
            this.buttonErrorMsgSkip.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonErrorMsgSkip.Location = new System.Drawing.Point(318, 145);
            this.buttonErrorMsgSkip.Name = "buttonErrorMsgSkip";
            this.buttonErrorMsgSkip.Size = new System.Drawing.Size(87, 23);
            this.buttonErrorMsgSkip.TabIndex = 0;
            this.buttonErrorMsgSkip.Text = "&Skip";
            this.buttonErrorMsgSkip.UseVisualStyleBackColor = true;
            this.buttonErrorMsgSkip.Click += new System.EventHandler(this.buttonErrorMsgSkip_Click);
            // 
            // pictureBoxError
            // 
            this.pictureBoxError.Location = new System.Drawing.Point(14, 12);
            this.pictureBoxError.Name = "pictureBoxError";
            this.pictureBoxError.Size = new System.Drawing.Size(47, 32);
            this.pictureBoxError.TabIndex = 2;
            this.pictureBoxError.TabStop = false;
            // 
            // labelQBErrorMessage
            // 
            this.labelQBErrorMessage.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQBErrorMessage.Location = new System.Drawing.Point(69, 15);
            this.labelQBErrorMessage.Name = "labelQBErrorMessage";
            this.labelQBErrorMessage.Size = new System.Drawing.Size(337, 13);
            this.labelQBErrorMessage.TabIndex = 3;
            this.labelQBErrorMessage.Text = "Message";
            // 
            // buttonQuit
            // 
            this.buttonQuit.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonQuit.Location = new System.Drawing.Point(201, 145);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(87, 23);
            this.buttonQuit.TabIndex = 4;
            this.buttonQuit.Text = "Quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.buttonQuit_Click);
            // 
            // ErrorSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(420, 180);
            this.Controls.Add(this.buttonQuit);
            this.Controls.Add(this.labelQBErrorMessage);
            this.Controls.Add(this.pictureBoxError);
            this.Controls.Add(this.buttonErrorMsgSkip);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorSummary";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Error Message";
            this.Load += new System.EventHandler(this.ErrorSummary_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxError)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonErrorMsgSkip;
        private System.Windows.Forms.PictureBox pictureBoxError;
        private GrowLabel labelQBErrorMessage;
        private System.Windows.Forms.Button buttonQuit;
    }
}