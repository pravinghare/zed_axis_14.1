namespace MessageMonitoringService
{
    partial class SetupMonitoringService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupMonitoringService));
            this.labelNameOfService = new System.Windows.Forms.Label();
            this.textBoxServiceName = new System.Windows.Forms.TextBox();
            this.checkBoxStatus = new System.Windows.Forms.CheckBox();
            this.labelFolderMessage = new System.Windows.Forms.Label();
            this.textBoxFolderPath = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.labelCondition = new System.Windows.Forms.Label();
            this.comboBoxPeriod = new System.Windows.Forms.ComboBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.folderBrowserDialogService = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // labelNameOfService
            // 
            this.labelNameOfService.AutoSize = true;
            this.labelNameOfService.Location = new System.Drawing.Point(27, 23);
            this.labelNameOfService.Name = "labelNameOfService";
            this.labelNameOfService.Size = new System.Drawing.Size(111, 13);
            this.labelNameOfService.TabIndex = 0;
            this.labelNameOfService.Text = "Name of Service :";
            // 
            // textBoxServiceName
            // 
            this.textBoxServiceName.Location = new System.Drawing.Point(160, 20);
            this.textBoxServiceName.MaxLength = 40;
            this.textBoxServiceName.Name = "textBoxServiceName";
            this.textBoxServiceName.Size = new System.Drawing.Size(196, 21);
            this.textBoxServiceName.TabIndex = 1;
            this.textBoxServiceName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxServiceName_KeyPress);
            // 
            // checkBoxStatus
            // 
            this.checkBoxStatus.AutoSize = true;
            this.checkBoxStatus.Checked = true;
            this.checkBoxStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxStatus.Location = new System.Drawing.Point(381, 19);
            this.checkBoxStatus.Name = "checkBoxStatus";
            this.checkBoxStatus.Size = new System.Drawing.Size(61, 17);
            this.checkBoxStatus.TabIndex = 2;
            this.checkBoxStatus.Text = "Active";
            this.checkBoxStatus.UseVisualStyleBackColor = true;
            // 
            // labelFolderMessage
            // 
            this.labelFolderMessage.AutoSize = true;
            this.labelFolderMessage.Location = new System.Drawing.Point(27, 78);
            this.labelFolderMessage.Name = "labelFolderMessage";
            this.labelFolderMessage.Size = new System.Drawing.Size(244, 13);
            this.labelFolderMessage.TabIndex = 3;
            this.labelFolderMessage.Text = "Select the folder that you want monitor : ";
            // 
            // textBoxFolderPath
            // 
            this.textBoxFolderPath.Location = new System.Drawing.Point(30, 109);
            this.textBoxFolderPath.MaxLength = 255;
            this.textBoxFolderPath.Name = "textBoxFolderPath";
            this.textBoxFolderPath.ReadOnly = true;
            this.textBoxFolderPath.Size = new System.Drawing.Size(375, 21);
            this.textBoxFolderPath.TabIndex = 4;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Location = new System.Drawing.Point(432, 109);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowse.TabIndex = 5;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // labelCondition
            // 
            this.labelCondition.AutoSize = true;
            this.labelCondition.Location = new System.Drawing.Point(27, 155);
            this.labelCondition.Name = "labelCondition";
            this.labelCondition.Size = new System.Drawing.Size(146, 13);
            this.labelCondition.TabIndex = 6;
            this.labelCondition.Text = "Check the folder every..";
            // 
            // comboBoxPeriod
            // 
            this.comboBoxPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPeriod.FormattingEnabled = true;
            this.comboBoxPeriod.Items.AddRange(new object[] {
            "< Select >",
            "1 minute",
            "5 minute",
            "10 minute",
            "15 minute",
            "30 minute",
            "45 minute",
            "1 hour",
            "2 hour",
            "5 hour",
            "10 hour",
            "12 hour"});
            this.comboBoxPeriod.Location = new System.Drawing.Point(30, 188);
            this.comboBoxPeriod.Name = "comboBoxPeriod";
            this.comboBoxPeriod.Size = new System.Drawing.Size(196, 21);
            this.comboBoxPeriod.TabIndex = 7;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(271, 222);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "&Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(367, 222);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 9;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // SetupMonitoringService
            // 
            this.AcceptButton = this.buttonSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(530, 317);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.comboBoxPeriod);
            this.Controls.Add(this.labelCondition);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.textBoxFolderPath);
            this.Controls.Add(this.labelFolderMessage);
            this.Controls.Add(this.checkBoxStatus);
            this.Controls.Add(this.textBoxServiceName);
            this.Controls.Add(this.labelNameOfService);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupMonitoringService";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setup Monitoring Service";
            this.Load += new System.EventHandler(this.SetupMonitoringService_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNameOfService;
        private System.Windows.Forms.TextBox textBoxServiceName;
        private System.Windows.Forms.CheckBox checkBoxStatus;
        private System.Windows.Forms.Label labelFolderMessage;
        private System.Windows.Forms.TextBox textBoxFolderPath;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Label labelCondition;
        private System.Windows.Forms.ComboBox comboBoxPeriod;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogService;
    }
}