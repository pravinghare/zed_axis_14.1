﻿using Telerik.WinForms.Documents.Model;
using Telerik.WinControls.UI;
using Telerik.WinControls;
namespace TransactionImporter
{
    partial class TransactionImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransactionImporter));
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            this.radChkDDListAccountTypeFilter = new Telerik.WinControls.UI.RadCheckedDropDownListElement();
            this.radCheckedDropDownListEntityFilter = new Telerik.WinControls.UI.RadCheckedDropDownListElement();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.TabPageConnection = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radioButtonQBDesktop = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonQBOnline = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonQBPOS = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonXero = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRibbonBarGroup3 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.buttonQuickBooksConnect = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup31 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup27 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.linkLabelQB = new Telerik.WinControls.UI.RadButtonElement();
            this.TabFileImport = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup16 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.buttonBrowseFile = new Telerik.WinControls.UI.RadButtonElement();
            this.groupBoxDataMapping = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup19 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.labelDataMapingInfo = new Telerik.WinControls.UI.RadLabelElement();
            this.comboBoxMappings = new Telerik.WinControls.UI.RadDropDownListElement();
            this.buttonMapping = new Telerik.WinControls.UI.RadButtonElement();
            this.buttonExportMapping = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup7 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup18 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.checkBoxAutoNumbering = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.chkListValidate = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxAddressDetails = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarGroup8 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup25 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radioButtonSkip = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonOverwrite = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonAppend = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radioButtonDuplicate = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRibbonBarGroup9 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup21 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.buttonOption = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup10 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.groupBoxStatementImport = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.labelImportStatement = new Telerik.WinControls.UI.RadLabelElement();
            this.comboBoxImportStatement = new Telerik.WinControls.UI.RadDropDownListElement();
            this.labelCodingTemplate = new Telerik.WinControls.UI.RadLabelElement();
            this.comboBoxCodingTemplate = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radRibbonBarGroup17 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.buttonPreviewImportStatement = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup29 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.linkLabelLearnMore = new Telerik.WinControls.UI.RadButtonElement();
            this.TabPageExport = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup11 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup7 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.comboBoxDatatype = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radRibbonBarButtonGroup13 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.checkBoxReportType = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.comboBoxReportType = new Telerik.WinControls.UI.RadDropDownListElement();
            this.filterGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.checkBoxLastExport = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxDaterange = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxRefnumber = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxEntityFilter = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarButtonGroup9 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement6 = new Telerik.WinControls.UI.RadLabelElement();
            this.textBoxFromRefnumber = new Telerik.WinControls.UI.RadTextBoxElement();
            this.radLabelElement7 = new Telerik.WinControls.UI.RadLabelElement();
            this.textBoxToRefNumber = new Telerik.WinControls.UI.RadTextBoxElement();
            this.groupDateFilter = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.From = new Telerik.WinControls.UI.RadLabelElement();
            this.dateTimePickerFrom = new Telerik.WinControls.UI.RadDateTimePickerElement();
            this.LabelTo = new Telerik.WinControls.UI.RadLabelElement();
            this.dateTimePickerTo = new Telerik.WinControls.UI.RadDateTimePickerElement();
            this.panelPaidStatus = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup23 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.checkBoxIncludeInactive = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxPaidStatus = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.comboBoxPaidStatus = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radRibbonBarGroup27 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarGroup16 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radImageButtonElement1 = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup15 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.buttonGet = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup14 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.GroupBoxColumnChooser = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.comboBoxColChooser = new Telerik.WinControls.UI.RadDropDownListElement();
            this.btnColChooserExport = new Telerik.WinControls.UI.RadImageButtonElement();
            this.btnResetTemplate = new Telerik.WinControls.UI.RadImageButtonElement();
            this.TabPageEDI = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup20 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemSendReceive = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup21 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemTradingPartner = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup22 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemMailSettings = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup23 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemRules = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup24 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemMonitor = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup25 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radMenuItemRefresh = new Telerik.WinControls.UI.RadImageButtonElement();
            this.TabPageAbout = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup17 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.checkBoxMaintainLog = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarGroup18 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup15 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.labelProductName = new Telerik.WinControls.UI.RadLabelElement();
            this.labelBN = new Telerik.WinControls.UI.RadLabelElement();
            this.buttonReactivateLicense = new Telerik.WinControls.UI.RadImageButtonElement();
            this.buttonLicenseInfo = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup12 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup3 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement8 = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radLabelElement9 = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radLabelElement10 = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup13 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.btncheckforupdate = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radRibbonBarGroup19 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.buttonDeactivateLicense = new Telerik.WinControls.UI.RadImageButtonElement();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.labelMessageID = new Telerik.WinControls.UI.RadLabel();
            this.labelMailCount = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.growLabelQBCompanyFile = new Telerik.WinControls.UI.RadLabelElement();
            this.textBoxFileName = new Telerik.WinControls.UI.RadLabelElement();
            this.toolStripStatusLabelProduct = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.toolStripStatusLabelMajorVersion = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.toolStripStatusLabelMinorVersion = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.toolStripStatusLabelCountry = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.toolStripStatusLabelConnectionStatus = new Telerik.WinControls.UI.RadStatusBarPanelElement();
            this.toolStripProgressBarIndicator = new Telerik.WinControls.UI.RadProgressBarElement();
            this.openFileDialogBrowseFile = new System.Windows.Forms.OpenFileDialog();
            this.backgroundWorkerQBItemLoader = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerExportLoad = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerStartupProcessLoader = new System.ComponentModel.BackgroundWorker();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorkerSendRecieveLoader = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStripAddTo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialogExportMapping = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialogForImportTemplate = new System.Windows.Forms.OpenFileDialog();
            this.notifyIconRecievedMessages = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorkerDelet = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerAutomaticSendReceive = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStripDeleteRows = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bindingSourcePreviewData = new System.Windows.Forms.BindingSource(this.components);
            this.saveExportedDialog = new System.Windows.Forms.SaveFileDialog();
            this.backgroundWorkerProcessLoader = new System.ComponentModel.BackgroundWorker();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarGroup2 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownListElement1 = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radRibbonBarButtonGroup4 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarButtonGroup8 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement1 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement1 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radRibbonBarButtonGroup6 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radDropDownButtonElement4 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radCheckBoxElement5 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarButtonGroup10 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement2 = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement5 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radRibbonBarButtonGroup11 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radDropDownButtonElement6 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radCheckBoxElement6 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarButtonGroup12 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement3 = new Telerik.WinControls.UI.RadButtonElement();
            this.radLabelElement3 = new Telerik.WinControls.UI.RadLabelElement();
            this.radRibbonBarButtonGroup5 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radCheckBoxElement7 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarButtonGroup14 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radCheckBoxElement1 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radDropDownListElement2 = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radDropDownListElement3 = new Telerik.WinControls.UI.RadDropDownListElement();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.radLabelElement2 = new Telerik.WinControls.UI.RadLabelElement();
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.radBtnOk = new Telerik.WinControls.UI.RadButtonElement();
            this.ellipseShape1 = new Telerik.WinControls.EllipseShape();
            this.chamferedRectShape1 = new Telerik.WinControls.ChamferedRectShape();
            this.radTextBoxElement1 = new Telerik.WinControls.UI.RadTextBoxElement();
            this.radRibbonBarButtonGroup20 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarButtonGroup24 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRadioButtonElement1 = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRadioButtonElement2 = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRadioButtonElement3 = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRadioButtonElement4 = new Telerik.WinControls.UI.RadRadioButtonElement();
            this.radRibbonBarGroup26 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.backgroundWorkerExportData = new System.ComponentModel.BackgroundWorker();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabImport = new System.Windows.Forms.TabPage();
            this.panelImportModule = new System.Windows.Forms.Panel();
            this.GroupBoxImportGrid = new Telerik.WinControls.UI.RadGroupBox();
            this.radPanelTop = new Telerik.WinControls.UI.RadPanel();
            this.dataGridViewDataPreview = new Telerik.WinControls.UI.RadGridView();
            this.radPanelSelectFile = new Telerik.WinControls.UI.RadPanel();
            this.radPopupContainerText = new Telerik.WinControls.UI.RadPopupContainer();
            this.groupBoxFileDelimiter = new Telerik.WinControls.UI.RadGroupBox();
            this.radioButtonPipe = new Telerik.WinControls.UI.RadRadioButton();
            this.radioButtoncomma = new Telerik.WinControls.UI.RadRadioButton();
            this.radioButtonTab = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabelElement11 = new Telerik.WinControls.UI.RadLabel();
            this.radPopupEditorTextFile = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupEditorCmnOptn = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupContainerCmnOptn = new Telerik.WinControls.UI.RadPopupContainer();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.numericUpDownImport = new System.Windows.Forms.NumericUpDown();
            this.labelImportRow = new Telerik.WinControls.UI.RadLabel();
            this.checkBoxHasHeaders = new Telerik.WinControls.UI.RadCheckBox();
            this.radPopupContainerIIF = new Telerik.WinControls.UI.RadPopupContainer();
            this.groupBoxTransType = new Telerik.WinControls.UI.RadGroupBox();
            this.buttonPreview = new Telerik.WinControls.UI.RadButton();
            this.comboBoxXmlPreviewTable = new Telerik.WinControls.UI.RadDropDownList();
            this.comboBoxIIFTransType = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPopupEditorIIF = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupEditorExcel = new Telerik.WinControls.UI.RadPopupEditor();
            this.radPopupContainerexcel = new Telerik.WinControls.UI.RadPopupContainer();
            this.groupBoxExcelSheet = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxWorkSheet = new Telerik.WinControls.UI.RadDropDownList();
            this.radpanelbottom = new Telerik.WinControls.UI.RadPanel();
            this.panelImport = new System.Windows.Forms.Panel();
            this.buttonSaveData = new Telerik.WinControls.UI.RadButton();
            this.buttonSearch = new Telerik.WinControls.UI.RadButton();
            this.butttonOpenlogs = new Telerik.WinControls.UI.RadButton();
            this.buttonRefresh = new Telerik.WinControls.UI.RadButton();
            this.buttonImport = new Telerik.WinControls.UI.RadButton();
            this.buttonDelete = new Telerik.WinControls.UI.RadButton();
            this.tabExport = new System.Windows.Forms.TabPage();
            this.panelExportModule = new System.Windows.Forms.Panel();
            this.GroupBoxDataGridViewResult = new Telerik.WinControls.UI.RadGroupBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanelexporttop = new Telerik.WinControls.UI.RadPanel();
            this.dataGridViewResult = new Telerik.WinControls.UI.RadGridView();
            this.groupBoxExportFormat = new System.Windows.Forms.GroupBox();
            this.panelbtnbtm = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSearchExp = new Telerik.WinControls.UI.RadButton();
            this.checkBoxDeleteFromQB = new System.Windows.Forms.CheckBox();
            this.buttonOK = new Telerik.WinControls.UI.RadButton();
            this.buttonCancelExport = new Telerik.WinControls.UI.RadButton();
            this.checkBoxSend = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxExportFormat = new System.Windows.Forms.ComboBox();
            this.tabEDI = new System.Windows.Forms.TabPage();
            this.PanelGridInfo = new System.Windows.Forms.Panel();
            this.panelmailbxandgrid = new System.Windows.Forms.Panel();
            this.GroupBoxEDI = new Telerik.WinControls.UI.RadGroupBox();
            this.radGridViewEDI = new Telerik.WinControls.UI.RadGridView();
            this.radTreeViewEDI = new Telerik.WinControls.UI.RadTreeView();
            this.LabelNodeSearch = new System.Windows.Forms.Label();
            this.radPanelEDI = new Telerik.WinControls.UI.RadPanel();
            this.radLabelSubjectOut = new Telerik.WinControls.UI.RadLabel();
            this.radLabelFrom1 = new Telerik.WinControls.UI.RadLabel();
            this.radButtonSend = new Telerik.WinControls.UI.RadButton();
            this.radButtonCc = new Telerik.WinControls.UI.RadButton();
            this.radButtonTo = new Telerik.WinControls.UI.RadButton();
            this.radButtonSaveDraft = new Telerik.WinControls.UI.RadButton();
            this.radLabelSubject1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCc1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabelSubject = new Telerik.WinControls.UI.RadLabel();
            this.radLabelCc = new Telerik.WinControls.UI.RadLabel();
            this.radLabelFrom = new Telerik.WinControls.UI.RadLabel();
            this.radPanelAttachment = new Telerik.WinControls.UI.RadPanel();
            this.radTextBoxSubject = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxCc = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxTo = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBoxMailData = new Telerik.WinControls.UI.RadTextBox();
            this.radLabelAttachment = new Telerik.WinControls.UI.RadLabel();
            this.object_ee5a844a_8a5a_48af_958d_95850aee92f5 = new Telerik.WinControls.RootRadElement();
            this.checkBoxUpdatednewnames = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.checkBoxAppendItems = new Telerik.WinControls.UI.RadCheckBoxElement();
            ((System.ComponentModel.ISupportInitialize)(this.radChkDDListAccountTypeFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownListEntityFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMappings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImportStatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCodingTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDatatype)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPaidStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxColChooser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMessageID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMailCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.contextMenuStripAddTo.SuspendLayout();
            this.contextMenuStripDeleteRows.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePreviewData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement3)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabImport.SuspendLayout();
            this.panelImportModule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxImportGrid)).BeginInit();
            this.GroupBoxImportGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelTop)).BeginInit();
            this.radPanelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview.MasterTemplate)).BeginInit();
            this.dataGridViewDataPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelSelectFile)).BeginInit();
            this.radPanelSelectFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerText)).BeginInit();
            this.radPopupContainerText.PanelContainer.SuspendLayout();
            this.radPopupContainerText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxFileDelimiter)).BeginInit();
            this.groupBoxFileDelimiter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonPipe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtoncomma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelElement11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorTextFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorCmnOptn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerCmnOptn)).BeginInit();
            this.radPopupContainerCmnOptn.PanelContainer.SuspendLayout();
            this.radPopupContainerCmnOptn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImportRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxHasHeaders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerIIF)).BeginInit();
            this.radPopupContainerIIF.PanelContainer.SuspendLayout();
            this.radPopupContainerIIF.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTransType)).BeginInit();
            this.groupBoxTransType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxXmlPreviewTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxIIFTransType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorIIF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerexcel)).BeginInit();
            this.radPopupContainerexcel.PanelContainer.SuspendLayout();
            this.radPopupContainerexcel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxExcelSheet)).BeginInit();
            this.groupBoxExcelSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxWorkSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radpanelbottom)).BeginInit();
            this.radpanelbottom.SuspendLayout();
            this.panelImport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSaveData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.butttonOpenlogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonImport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).BeginInit();
            this.tabExport.SuspendLayout();
            this.panelExportModule.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxDataGridViewResult)).BeginInit();
            this.GroupBoxDataGridViewResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelexporttop)).BeginInit();
            this.radPanelexporttop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult.MasterTemplate)).BeginInit();
            this.groupBoxExportFormat.SuspendLayout();
            this.panelbtnbtm.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSearchExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancelExport)).BeginInit();
            this.tabEDI.SuspendLayout();
            this.PanelGridInfo.SuspendLayout();
            this.panelmailbxandgrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxEDI)).BeginInit();
            this.GroupBoxEDI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEDI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEDI.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeViewEDI)).BeginInit();
            this.radTreeViewEDI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelEDI)).BeginInit();
            this.radPanelEDI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubjectOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFrom1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveDraft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubject1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxSubject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxCc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxMailData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radChkDDListAccountTypeFilter
            // 
            this.radChkDDListAccountTypeFilter.ArrowButtonMinWidth = 17;
            this.radChkDDListAccountTypeFilter.AutoCompleteAppend = null;
            this.radChkDDListAccountTypeFilter.AutoCompleteDataSource = null;
            this.radChkDDListAccountTypeFilter.AutoCompleteSuggest = null;
            this.radChkDDListAccountTypeFilter.AutoSize = false;
            this.radChkDDListAccountTypeFilter.Bounds = new System.Drawing.Rectangle(0, 0, 112, 24);
            this.radChkDDListAccountTypeFilter.DataMember = "";
            this.radChkDDListAccountTypeFilter.DataSource = null;
            this.radChkDDListAccountTypeFilter.DefaultValue = null;
            this.radChkDDListAccountTypeFilter.DisplayMember = "";
            this.radChkDDListAccountTypeFilter.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radChkDDListAccountTypeFilter.DropDownAnimationEnabled = true;
            this.radChkDDListAccountTypeFilter.EditableElementText = "";
            this.radChkDDListAccountTypeFilter.EditorElement = this.radChkDDListAccountTypeFilter;
            this.radChkDDListAccountTypeFilter.EditorManager = null;
            this.radChkDDListAccountTypeFilter.Filter = null;
            this.radChkDDListAccountTypeFilter.FilterExpression = "";
            this.radChkDDListAccountTypeFilter.Focusable = true;
            this.radChkDDListAccountTypeFilter.FormatString = "";
            this.radChkDDListAccountTypeFilter.FormattingEnabled = true;
            this.radChkDDListAccountTypeFilter.ItemHeight = 18;
            this.radChkDDListAccountTypeFilter.MaxDropDownItems = 0;
            this.radChkDDListAccountTypeFilter.MaxLength = 2147483647;
            this.radChkDDListAccountTypeFilter.MaxValue = null;
            this.radChkDDListAccountTypeFilter.MinSize = new System.Drawing.Size(140, 0);
            this.radChkDDListAccountTypeFilter.MinValue = null;
            this.radChkDDListAccountTypeFilter.Name = "radChkDDListAccountTypeFilter";
            this.radChkDDListAccountTypeFilter.NullValue = null;
            this.radChkDDListAccountTypeFilter.OwnerOffset = 0;
            this.radChkDDListAccountTypeFilter.ShowCheckAllItems = false;
            this.radChkDDListAccountTypeFilter.ShowImageInEditorArea = true;
            this.radChkDDListAccountTypeFilter.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radChkDDListAccountTypeFilter.StretchVertically = false;
            this.radChkDDListAccountTypeFilter.Value = null;
            this.radChkDDListAccountTypeFilter.ValueMember = "";
            // 
            // radCheckedDropDownListEntityFilter
            // 
            this.radCheckedDropDownListEntityFilter.ArrowButtonMinWidth = 17;
            this.radCheckedDropDownListEntityFilter.AutoCompleteAppend = null;
            this.radCheckedDropDownListEntityFilter.AutoCompleteDataSource = null;
            this.radCheckedDropDownListEntityFilter.AutoCompleteSuggest = null;
            this.radCheckedDropDownListEntityFilter.AutoSize = false;
            this.radCheckedDropDownListEntityFilter.Bounds = new System.Drawing.Rectangle(1, 0, 225, 24);
            this.radCheckedDropDownListEntityFilter.DataMember = "";
            this.radCheckedDropDownListEntityFilter.DataSource = null;
            this.radCheckedDropDownListEntityFilter.DefaultValue = null;
            this.radCheckedDropDownListEntityFilter.DisplayMember = "";
            this.radCheckedDropDownListEntityFilter.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radCheckedDropDownListEntityFilter.DropDownAnimationEnabled = true;
            this.radCheckedDropDownListEntityFilter.EditableElementText = "";
            this.radCheckedDropDownListEntityFilter.EditorElement = this.radCheckedDropDownListEntityFilter;
            this.radCheckedDropDownListEntityFilter.EditorManager = null;
            this.radCheckedDropDownListEntityFilter.Filter = null;
            this.radCheckedDropDownListEntityFilter.FilterExpression = "";
            this.radCheckedDropDownListEntityFilter.Focusable = true;
            this.radCheckedDropDownListEntityFilter.FormatString = "";
            this.radCheckedDropDownListEntityFilter.FormattingEnabled = true;
            this.radCheckedDropDownListEntityFilter.ItemHeight = 18;
            this.radCheckedDropDownListEntityFilter.MaxDropDownItems = 0;
            this.radCheckedDropDownListEntityFilter.MaxLength = 2147483647;
            this.radCheckedDropDownListEntityFilter.MaxValue = null;
            this.radCheckedDropDownListEntityFilter.MinSize = new System.Drawing.Size(140, 0);
            this.radCheckedDropDownListEntityFilter.MinValue = null;
            this.radCheckedDropDownListEntityFilter.Name = "radCheckedDropDownListEntityFilter";
            this.radCheckedDropDownListEntityFilter.NullValue = null;
            this.radCheckedDropDownListEntityFilter.OwnerOffset = 0;
            this.radCheckedDropDownListEntityFilter.ShowCheckAllItems = false;
            this.radCheckedDropDownListEntityFilter.ShowImageInEditorArea = true;
            this.radCheckedDropDownListEntityFilter.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radCheckedDropDownListEntityFilter.StretchVertically = false;
            this.radCheckedDropDownListEntityFilter.Value = null;
            this.radCheckedDropDownListEntityFilter.ValueMember = "";
            this.radCheckedDropDownListEntityFilter.ItemCheckedChanged += new Telerik.WinControls.UI.RadCheckedListDataItemEventHandler(this.radCheckedDropDownListEntityFilter_ItemCheckedChanged);
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.TabPageConnection,
            this.TabFileImport,
            this.TabPageExport,
            this.TabPageEDI,
            this.TabPageAbout});
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.MinimumSize = new System.Drawing.Size(0, 200);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.Text = "Options";
            this.radRibbonBar1.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.radRibbonBar1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radRibbonBar1.RootElement.MinSize = new System.Drawing.Size(0, 200);
            this.radRibbonBar1.ShowExpandButton = false;
            this.radRibbonBar1.Size = new System.Drawing.Size(1352, 200);
            this.radRibbonBar1.StartButtonImage = ((System.Drawing.Image)(resources.GetObject("radRibbonBar1.StartButtonImage")));
            this.radRibbonBar1.TabIndex = 0;
            this.radRibbonBar1.Resize += new System.EventHandler(this.radRibbonBar1_Resize);
            ((Telerik.WinControls.UI.RadRibbonBarElement)(this.radRibbonBar1.GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadToggleButtonElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(6).GetChildAt(0))).Enabled = false;
            ((Telerik.WinControls.UI.RadToggleButtonElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(6).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // TabPageConnection
            // 
            this.TabPageConnection.ClickMode = Telerik.WinControls.ClickMode.Hover;
            this.TabPageConnection.IsSelected = true;
            this.TabPageConnection.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup1,
            this.radRibbonBarGroup3,
            this.radRibbonBarGroup31});
            this.TabPageConnection.Name = "TabPageConnection";
            this.TabPageConnection.Text = "Connection";
            this.TabPageConnection.UseDefaultDisabledPaint = true;
            this.TabPageConnection.UseMnemonic = false;
            this.TabPageConnection.Click += new System.EventHandler(this.tabControltabHolder_Click);
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.AutoSize = false;
            this.radRibbonBarGroup1.Bounds = new System.Drawing.Rectangle(0, 0, 137, 125);
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radioButtonQBDesktop,
            this.radioButtonQBOnline,
            this.radioButtonQBPOS,
            this.radioButtonXero});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup1.Text = "Select type";
            // 
            // radioButtonQBDesktop
            // 
            this.radioButtonQBDesktop.MinSize = new System.Drawing.Size(20, 0);
            this.radioButtonQBDesktop.Name = "radioButtonQBDesktop";
            this.radioButtonQBDesktop.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonQBDesktop.ReadOnly = false;
            this.radioButtonQBDesktop.StretchVertically = false;
            this.radioButtonQBDesktop.Text = "QuickBooks Desktop";
            this.radioButtonQBDesktop.CheckStateChanged += new System.EventHandler(this.radioButtonQBDesktop_Click);
            // 
            // radioButtonQBOnline
            // 
            this.radioButtonQBOnline.MinSize = new System.Drawing.Size(20, 0);
            this.radioButtonQBOnline.Name = "radioButtonQBOnline";
            this.radioButtonQBOnline.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonQBOnline.ReadOnly = false;
            this.radioButtonQBOnline.StretchVertically = false;
            this.radioButtonQBOnline.Text = "QuickBooks Online";
            this.radioButtonQBOnline.CheckStateChanged += new System.EventHandler(this.radioButtonQBOnline_Click);
            // 
            // radioButtonQBPOS
            // 
            this.radioButtonQBPOS.MinSize = new System.Drawing.Size(20, 0);
            this.radioButtonQBPOS.Name = "radioButtonQBPOS";
            this.radioButtonQBPOS.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonQBPOS.ReadOnly = false;
            this.radioButtonQBPOS.StretchVertically = false;
            this.radioButtonQBPOS.Text = "QuickBooks POS";
            this.radioButtonQBPOS.CheckStateChanged += new System.EventHandler(this.radioButtonQBPOS_Click);
            // 
            // radioButtonXero
            // 
            this.radioButtonXero.MinSize = new System.Drawing.Size(20, 0);
            this.radioButtonXero.Name = "radioButtonXero";
            this.radioButtonXero.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonXero.ReadOnly = false;
            this.radioButtonXero.StretchVertically = false;
            this.radioButtonXero.Text = "Xero";
            this.radioButtonXero.CheckStateChanged += new System.EventHandler(this.radioButtonXero_Click);
            // 
            // radRibbonBarGroup3
            // 
            this.radRibbonBarGroup3.AutoSize = false;
            this.radRibbonBarGroup3.Bounds = new System.Drawing.Rectangle(5, 0, 120, 125);
            this.radRibbonBarGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonQuickBooksConnect});
            this.radRibbonBarGroup3.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.radRibbonBarGroup3.Name = "radRibbonBarGroup3";
            this.radRibbonBarGroup3.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup3.Text = "Connect";
            // 
            // buttonQuickBooksConnect
            // 
            this.buttonQuickBooksConnect.AutoSize = false;
            this.buttonQuickBooksConnect.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.buttonQuickBooksConnect.Bounds = new System.Drawing.Rectangle(0, 0, 113, 96);
            this.buttonQuickBooksConnect.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonQuickBooksConnect.Name = "buttonQuickBooksConnect";
            this.buttonQuickBooksConnect.Text = "Connect";
            this.buttonQuickBooksConnect.Click += new System.EventHandler(this.buttonQuickBooksConnect_Click);
            // 
            // radRibbonBarGroup31
            // 
            this.radRibbonBarGroup31.AutoSize = false;
            this.radRibbonBarGroup31.Bounds = new System.Drawing.Rectangle(6, 0, 210, 125);
            this.radRibbonBarGroup31.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup27});
            this.radRibbonBarGroup31.Name = "radRibbonBarGroup31";
            this.radRibbonBarGroup31.Text = "Help";
            // 
            // radRibbonBarButtonGroup27
            // 
            this.radRibbonBarButtonGroup27.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linkLabelQB});
            this.radRibbonBarButtonGroup27.Name = "radRibbonBarButtonGroup27";
            this.radRibbonBarButtonGroup27.Text = "radRibbonBarButtonGroup27";
            // 
            // linkLabelQB
            // 
            this.linkLabelQB.AccessibleRole = System.Windows.Forms.AccessibleRole.Link;
            this.linkLabelQB.AutoSize = false;
            this.linkLabelQB.Bounds = new System.Drawing.Rectangle(0, 0, 200, 96);
            this.linkLabelQB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(96)))), ((int)(((byte)(251)))));
            this.linkLabelQB.Name = "linkLabelQB";
            this.linkLabelQB.Text = "Help connect to Quickbooks desktop";
            this.linkLabelQB.UseCompatibleTextRendering = false;
            this.linkLabelQB.Click += new System.EventHandler(this.linkLabelXero_LinkClicked);
            // 
            // TabFileImport
            // 
            this.TabFileImport.BackgroundImage = null;
            this.TabFileImport.ClickMode = Telerik.WinControls.ClickMode.Release;
            this.TabFileImport.ClipDrawing = true;
            this.TabFileImport.IsSelected = false;
            this.TabFileImport.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup5,
            this.groupBoxDataMapping,
            this.radRibbonBarGroup7,
            this.radRibbonBarGroup8,
            this.radRibbonBarGroup9,
            this.radRibbonBarGroup10,
            this.radRibbonBarGroup17,
            this.radRibbonBarGroup29});
            this.TabFileImport.Name = "TabFileImport";
            this.TabFileImport.Text = "Import";
            this.TabFileImport.UseMnemonic = false;
            this.TabFileImport.Click += new System.EventHandler(this.tabControltabHolder_Click);
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.AutoSize = false;
            this.radRibbonBarGroup5.Bounds = new System.Drawing.Rectangle(0, 0, 190, 125);
            this.radRibbonBarGroup5.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentPadding;
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup16});
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup5.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup5.ShouldPaint = true;
            this.radRibbonBarGroup5.Text = "Select a file";
            // 
            // radRibbonBarButtonGroup16
            // 
            this.radRibbonBarButtonGroup16.AutoSize = false;
            this.radRibbonBarButtonGroup16.Bounds = new System.Drawing.Rectangle(0, 0, 181, 100);
            this.radRibbonBarButtonGroup16.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonBrowseFile});
            this.radRibbonBarButtonGroup16.Name = "radRibbonBarButtonGroup16";
            this.radRibbonBarButtonGroup16.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // buttonBrowseFile
            // 
            this.buttonBrowseFile.AutoSize = false;
            this.buttonBrowseFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.buttonBrowseFile.Bounds = new System.Drawing.Rectangle(0, 0, 177, 31);
            this.buttonBrowseFile.Font = new System.Drawing.Font("Segoe UI", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBrowseFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.buttonBrowseFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonBrowseFile.Image")));
            this.buttonBrowseFile.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBrowseFile.Name = "buttonBrowseFile";
            this.buttonBrowseFile.Padding = new System.Windows.Forms.Padding(2);
            this.buttonBrowseFile.ShouldPaint = false;
            this.buttonBrowseFile.ShowBorder = false;
            this.buttonBrowseFile.Text = "Browse";
            this.buttonBrowseFile.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBrowseFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonBrowseFile.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.buttonBrowseFile.Click += new System.EventHandler(this.buttonBrowseFile_Click);
            // 
            // groupBoxDataMapping
            // 
            this.groupBoxDataMapping.AutoSize = false;
            this.groupBoxDataMapping.Bounds = new System.Drawing.Rectangle(3, 0, 150, 125);
            this.groupBoxDataMapping.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup19});
            this.groupBoxDataMapping.MaxSize = new System.Drawing.Size(0, 0);
            this.groupBoxDataMapping.MinSize = new System.Drawing.Size(0, 130);
            this.groupBoxDataMapping.Name = "groupBoxDataMapping";
            this.groupBoxDataMapping.Padding = new System.Windows.Forms.Padding(0);
            this.groupBoxDataMapping.Text = "Select mapping";
            // 
            // radRibbonBarButtonGroup19
            // 
            this.radRibbonBarButtonGroup19.AutoSize = false;
            this.radRibbonBarButtonGroup19.Bounds = new System.Drawing.Rectangle(0, 0, 142, 98);
            this.radRibbonBarButtonGroup19.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.radRibbonBarButtonGroup19.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.labelDataMapingInfo,
            this.comboBoxMappings,
            this.buttonMapping,
            this.buttonExportMapping});
            this.radRibbonBarButtonGroup19.Name = "radRibbonBarButtonGroup19";
            this.radRibbonBarButtonGroup19.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup19.Text = "radRibbonBarButtonGroup19";
            // 
            // labelDataMapingInfo
            // 
            this.labelDataMapingInfo.AutoSize = false;
            this.labelDataMapingInfo.Bounds = new System.Drawing.Rectangle(0, 0, 140, 2);
            this.labelDataMapingInfo.Name = "labelDataMapingInfo";
            this.labelDataMapingInfo.TextWrap = true;
            // 
            // comboBoxMappings
            // 
            this.comboBoxMappings.AccessibleRole = System.Windows.Forms.AccessibleRole.List;
            this.comboBoxMappings.ArrowButtonMinWidth = 17;
            this.comboBoxMappings.AutoCompleteAppend = null;
            this.comboBoxMappings.AutoCompleteDataSource = null;
            this.comboBoxMappings.AutoCompleteSuggest = null;
            this.comboBoxMappings.AutoSize = false;
            this.comboBoxMappings.Bounds = new System.Drawing.Rectangle(0, 0, 140, 24);
            this.comboBoxMappings.CaseSensitive = false;
            this.comboBoxMappings.DataMember = "";
            this.comboBoxMappings.DataSource = null;
            this.comboBoxMappings.DefaultItemsCountInDropDown = 10;
            this.comboBoxMappings.DefaultValue = null;
            this.comboBoxMappings.DisplayMember = "";
            this.comboBoxMappings.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxMappings.DropDownAnimationEnabled = true;
            this.comboBoxMappings.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxMappings.EditableElementText = "select mapping";
            this.comboBoxMappings.EditorElement = this.comboBoxMappings;
            this.comboBoxMappings.EditorManager = null;
            this.comboBoxMappings.Filter = null;
            this.comboBoxMappings.FilterExpression = "";
            this.comboBoxMappings.Focusable = true;
            this.comboBoxMappings.FormatString = "";
            this.comboBoxMappings.FormattingEnabled = true;
            this.comboBoxMappings.ItemHeight = 18;
            this.comboBoxMappings.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxMappings.MaxDropDownItems = 0;
            this.comboBoxMappings.MaxLength = 32767;
            this.comboBoxMappings.MaxValue = null;
            this.comboBoxMappings.MinSize = new System.Drawing.Size(140, 0);
            this.comboBoxMappings.MinValue = null;
            this.comboBoxMappings.Name = "comboBoxMappings";
            this.comboBoxMappings.NullValue = null;
            this.comboBoxMappings.OwnerOffset = 0;
            this.comboBoxMappings.Padding = new System.Windows.Forms.Padding(5);
            this.comboBoxMappings.ShowImageInEditorArea = true;
            this.comboBoxMappings.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxMappings.StretchVertically = false;
            this.comboBoxMappings.SyncSelectionWithText = false;
            this.comboBoxMappings.Value = null;
            this.comboBoxMappings.ValueMember = "";
            this.comboBoxMappings.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxMappings_SelectedIndexChanged);
            // 
            // buttonMapping
            // 
            this.buttonMapping.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.buttonMapping.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.buttonMapping.Image = ((System.Drawing.Image)(resources.GetObject("buttonMapping.Image")));
            this.buttonMapping.Margin = new System.Windows.Forms.Padding(0);
            this.buttonMapping.Name = "buttonMapping";
            this.buttonMapping.Padding = new System.Windows.Forms.Padding(5);
            this.buttonMapping.ShouldPaint = true;
            this.buttonMapping.ShowBorder = false;
            this.buttonMapping.Text = "Edit";
            this.buttonMapping.Click += new System.EventHandler(this.buttonMapping_Click);
            // 
            // buttonExportMapping
            // 
            this.buttonExportMapping.AutoSize = false;
            this.buttonExportMapping.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.buttonExportMapping.Bounds = new System.Drawing.Rectangle(0, 0, 140, 34);
            this.buttonExportMapping.Image = ((System.Drawing.Image)(resources.GetObject("buttonExportMapping.Image")));
            this.buttonExportMapping.Name = "buttonExportMapping";
            this.buttonExportMapping.Padding = new System.Windows.Forms.Padding(5);
            this.buttonExportMapping.ShouldPaint = true;
            this.buttonExportMapping.ShowBorder = false;
            this.buttonExportMapping.Text = "Export";
            this.buttonExportMapping.Click += new System.EventHandler(this.buttonExportMapping_Click);
            // 
            // radRibbonBarGroup7
            // 
            this.radRibbonBarGroup7.AutoSize = false;
            this.radRibbonBarGroup7.Bounds = new System.Drawing.Rectangle(6, 0, 135, 125);
            this.radRibbonBarGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup18});
            this.radRibbonBarGroup7.Name = "radRibbonBarGroup7";
            this.radRibbonBarGroup7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup7.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup7.Text = "Import options";
            // 
            // radRibbonBarButtonGroup18
            // 
            this.radRibbonBarButtonGroup18.AutoSize = false;
            this.radRibbonBarButtonGroup18.Bounds = new System.Drawing.Rectangle(0, 0, 127, 98);
            this.radRibbonBarButtonGroup18.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.checkBoxAutoNumbering,
            this.chkListValidate,
            this.checkBoxAddressDetails});
            this.radRibbonBarButtonGroup18.Name = "radRibbonBarButtonGroup18";
            this.radRibbonBarButtonGroup18.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup18.Text = "radRibbonBarButtonGroup18";
            // 
            // checkBoxAutoNumbering
            // 
            this.checkBoxAutoNumbering.AutoSize = false;
            this.checkBoxAutoNumbering.Bounds = new System.Drawing.Rectangle(0, 2, 125, 22);
            this.checkBoxAutoNumbering.Checked = false;
            this.checkBoxAutoNumbering.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxAutoNumbering.Name = "checkBoxAutoNumbering";
            this.checkBoxAutoNumbering.Padding = new System.Windows.Forms.Padding(2);
            this.checkBoxAutoNumbering.ReadOnly = false;
            this.checkBoxAutoNumbering.StretchVertically = false;
            this.checkBoxAutoNumbering.Text = "Use auto numbering";
            this.checkBoxAutoNumbering.CheckStateChanged += new System.EventHandler(this.checkBoxAutoNumbering_CheckedChanged_1);
            // 
            // chkListValidate
            // 
            this.chkListValidate.AutoSize = false;
            this.chkListValidate.Bounds = new System.Drawing.Rectangle(0, 12, 125, 22);
            this.chkListValidate.Checked = false;
            this.chkListValidate.Margin = new System.Windows.Forms.Padding(0);
            this.chkListValidate.Name = "chkListValidate";
            this.chkListValidate.Padding = new System.Windows.Forms.Padding(2);
            this.chkListValidate.ReadOnly = false;
            this.chkListValidate.StretchVertically = false;
            this.chkListValidate.Text = "Skip list validation";
            this.chkListValidate.CheckStateChanged += new System.EventHandler(this.chkListValidate_CheckedChanged);
            // 
            // checkBoxAddressDetails
            // 
            this.checkBoxAddressDetails.AutoSize = false;
            this.checkBoxAddressDetails.Bounds = new System.Drawing.Rectangle(0, 15, 125, 33);
            this.checkBoxAddressDetails.Checked = false;
            this.checkBoxAddressDetails.Margin = new System.Windows.Forms.Padding(0);
            this.checkBoxAddressDetails.Name = "checkBoxAddressDetails";
            this.checkBoxAddressDetails.Padding = new System.Windows.Forms.Padding(2);
            this.checkBoxAddressDetails.ReadOnly = false;
            this.checkBoxAddressDetails.StretchVertically = false;
            this.checkBoxAddressDetails.Text = "Update address and contact details";
            this.checkBoxAddressDetails.TextWrap = true;
            // 
            // radRibbonBarGroup8
            // 
            this.radRibbonBarGroup8.AutoSize = false;
            this.radRibbonBarGroup8.Bounds = new System.Drawing.Rectangle(9, 0, 105, 125);
            this.radRibbonBarGroup8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup25});
            this.radRibbonBarGroup8.Name = "radRibbonBarGroup8";
            this.radRibbonBarGroup8.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup8.Text = "If transaction exists";
            // 
            // radRibbonBarButtonGroup25
            // 
            this.radRibbonBarButtonGroup25.AutoSize = false;
            this.radRibbonBarButtonGroup25.Bounds = new System.Drawing.Rectangle(0, 0, 97, 98);
            this.radRibbonBarButtonGroup25.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radioButtonSkip,
            this.radioButtonOverwrite,
            this.radioButtonAppend,
            this.radioButtonDuplicate});
            this.radRibbonBarButtonGroup25.Name = "radRibbonBarButtonGroup25";
            this.radRibbonBarButtonGroup25.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup25.Text = "radRibbonBarButtonGroup25";
            // 
            // radioButtonSkip
            // 
            this.radioButtonSkip.AutoSize = false;
            this.radioButtonSkip.Bounds = new System.Drawing.Rectangle(4, 0, 75, 24);
            this.radioButtonSkip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radioButtonSkip.Name = "radioButtonSkip";
            this.radioButtonSkip.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonSkip.ReadOnly = false;
            this.radioButtonSkip.Text = "Skip";
            this.radioButtonSkip.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radioButtonOverwrite
            // 
            this.radioButtonOverwrite.AutoSize = false;
            this.radioButtonOverwrite.Bounds = new System.Drawing.Rectangle(4, 0, 75, 24);
            this.radioButtonOverwrite.Name = "radioButtonOverwrite";
            this.radioButtonOverwrite.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonOverwrite.ReadOnly = false;
            this.radioButtonOverwrite.Text = "Overwrite";
            // 
            // radioButtonAppend
            // 
            this.radioButtonAppend.AutoSize = false;
            this.radioButtonAppend.Bounds = new System.Drawing.Rectangle(4, 0, 75, 24);
            this.radioButtonAppend.Name = "radioButtonAppend";
            this.radioButtonAppend.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonAppend.ReadOnly = false;
            this.radioButtonAppend.Text = "Append";
            // 
            // radioButtonDuplicate
            // 
            this.radioButtonDuplicate.AutoSize = false;
            this.radioButtonDuplicate.Bounds = new System.Drawing.Rectangle(4, 0, 75, 24);
            this.radioButtonDuplicate.Name = "radioButtonDuplicate";
            this.radioButtonDuplicate.Padding = new System.Windows.Forms.Padding(3);
            this.radioButtonDuplicate.ReadOnly = false;
            this.radioButtonDuplicate.Text = "Duplicate";
            // 
            // radRibbonBarGroup9
            // 
            this.radRibbonBarGroup9.AutoSize = false;
            this.radRibbonBarGroup9.Bounds = new System.Drawing.Rectangle(12, 0, 87, 125);
            this.radRibbonBarGroup9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup21});
            this.radRibbonBarGroup9.Name = "radRibbonBarGroup9";
            this.radRibbonBarGroup9.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup9.Text = "More options";
            // 
            // radRibbonBarButtonGroup21
            // 
            this.radRibbonBarButtonGroup21.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonOption});
            this.radRibbonBarButtonGroup21.Name = "radRibbonBarButtonGroup21";
            this.radRibbonBarButtonGroup21.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.radRibbonBarButtonGroup21.Text = "radRibbonBarButtonGroup21";
            // 
            // buttonOption
            // 
            this.buttonOption.AutoSize = false;
            this.buttonOption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.buttonOption.Bounds = new System.Drawing.Rectangle(0, 0, 77, 96);
            this.buttonOption.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.buttonOption.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.buttonOption.FlipText = false;
            this.buttonOption.Image = ((System.Drawing.Image)(resources.GetObject("buttonOption.Image")));
            this.buttonOption.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonOption.Margin = new System.Windows.Forms.Padding(0);
            this.buttonOption.Name = "buttonOption";
            this.buttonOption.Padding = new System.Windows.Forms.Padding(0);
            this.buttonOption.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.buttonOption.ScaleTransform = new System.Drawing.SizeF(1F, 1F);
            this.buttonOption.ShouldPaint = false;
            this.buttonOption.Text = "6";
            this.buttonOption.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonOption.Click += new System.EventHandler(this.buttonOption_Click);
            // 
            // radRibbonBarGroup10
            // 
            this.radRibbonBarGroup10.AutoSize = false;
            this.radRibbonBarGroup10.Bounds = new System.Drawing.Rectangle(15, 0, 155, 125);
            this.radRibbonBarGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.groupBoxStatementImport});
            this.radRibbonBarGroup10.Name = "radRibbonBarGroup10";
            this.radRibbonBarGroup10.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup10.Text = "Statement import option";
            // 
            // groupBoxStatementImport
            // 
            this.groupBoxStatementImport.AutoSize = false;
            this.groupBoxStatementImport.Bounds = new System.Drawing.Rectangle(0, 0, 147, 98);
            this.groupBoxStatementImport.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.labelImportStatement,
            this.comboBoxImportStatement,
            this.labelCodingTemplate,
            this.comboBoxCodingTemplate});
            this.groupBoxStatementImport.Margin = new System.Windows.Forms.Padding(0);
            this.groupBoxStatementImport.Name = "groupBoxStatementImport";
            this.groupBoxStatementImport.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.groupBoxStatementImport.Text = "radRibbonBarButtonGroup2";
            // 
            // labelImportStatement
            // 
            this.labelImportStatement.AutoSize = false;
            this.labelImportStatement.Bounds = new System.Drawing.Rectangle(0, 0, 141, 22);
            this.labelImportStatement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.labelImportStatement.Margin = new System.Windows.Forms.Padding(2);
            this.labelImportStatement.Name = "labelImportStatement";
            this.labelImportStatement.Padding = new System.Windows.Forms.Padding(2);
            this.labelImportStatement.Text = "Account to import Into";
            this.labelImportStatement.TextWrap = true;
            // 
            // comboBoxImportStatement
            // 
            this.comboBoxImportStatement.ArrowButtonMinWidth = 17;
            this.comboBoxImportStatement.AutoCompleteAppend = null;
            this.comboBoxImportStatement.AutoCompleteDataSource = null;
            this.comboBoxImportStatement.AutoCompleteSuggest = null;
            this.comboBoxImportStatement.AutoSize = false;
            this.comboBoxImportStatement.Bounds = new System.Drawing.Rectangle(0, 0, 145, 24);
            this.comboBoxImportStatement.DataMember = "";
            this.comboBoxImportStatement.DataSource = null;
            this.comboBoxImportStatement.DefaultValue = null;
            this.comboBoxImportStatement.DisplayMember = "";
            this.comboBoxImportStatement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxImportStatement.DropDownAnimationEnabled = true;
            this.comboBoxImportStatement.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxImportStatement.EditableElementText = "";
            this.comboBoxImportStatement.EditorElement = this.comboBoxImportStatement;
            this.comboBoxImportStatement.EditorManager = null;
            this.comboBoxImportStatement.Filter = null;
            this.comboBoxImportStatement.FilterExpression = "";
            this.comboBoxImportStatement.Focusable = true;
            this.comboBoxImportStatement.FormatString = "";
            this.comboBoxImportStatement.FormattingEnabled = true;
            this.comboBoxImportStatement.ItemHeight = 18;
            this.comboBoxImportStatement.MaxDropDownItems = 0;
            this.comboBoxImportStatement.MaxLength = 32767;
            this.comboBoxImportStatement.MaxValue = null;
            this.comboBoxImportStatement.MinValue = null;
            this.comboBoxImportStatement.Name = "comboBoxImportStatement";
            this.comboBoxImportStatement.NullValue = null;
            this.comboBoxImportStatement.OwnerOffset = 0;
            this.comboBoxImportStatement.Padding = new System.Windows.Forms.Padding(1);
            this.comboBoxImportStatement.ShowImageInEditorArea = true;
            this.comboBoxImportStatement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxImportStatement.SyncSelectionWithText = false;
            this.comboBoxImportStatement.Value = null;
            this.comboBoxImportStatement.ValueMember = "";
            this.comboBoxImportStatement.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxImportStatement_SelectedIndexChanged);
            // 
            // labelCodingTemplate
            // 
            this.labelCodingTemplate.AutoSize = false;
            this.labelCodingTemplate.Bounds = new System.Drawing.Rectangle(0, 2, 145, 18);
            this.labelCodingTemplate.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelCodingTemplate.Name = "labelCodingTemplate";
            this.labelCodingTemplate.Padding = new System.Windows.Forms.Padding(0);
            this.labelCodingTemplate.Text = "Coding template";
            this.labelCodingTemplate.TextWrap = true;
            // 
            // comboBoxCodingTemplate
            // 
            this.comboBoxCodingTemplate.ArrowButtonMinWidth = 17;
            this.comboBoxCodingTemplate.AutoCompleteAppend = null;
            this.comboBoxCodingTemplate.AutoCompleteDataSource = null;
            this.comboBoxCodingTemplate.AutoCompleteSuggest = null;
            this.comboBoxCodingTemplate.AutoSize = false;
            this.comboBoxCodingTemplate.Bounds = new System.Drawing.Rectangle(0, 4, 145, 24);
            this.comboBoxCodingTemplate.DataMember = "";
            this.comboBoxCodingTemplate.DataSource = null;
            this.comboBoxCodingTemplate.DefaultValue = null;
            this.comboBoxCodingTemplate.DisplayMember = "";
            this.comboBoxCodingTemplate.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxCodingTemplate.DropDownAnimationEnabled = true;
            this.comboBoxCodingTemplate.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxCodingTemplate.EditableElementText = "Select template";
            this.comboBoxCodingTemplate.EditorElement = this.comboBoxCodingTemplate;
            this.comboBoxCodingTemplate.EditorManager = null;
            this.comboBoxCodingTemplate.Filter = null;
            this.comboBoxCodingTemplate.FilterExpression = "";
            this.comboBoxCodingTemplate.Focusable = true;
            this.comboBoxCodingTemplate.FormatString = "";
            this.comboBoxCodingTemplate.FormattingEnabled = true;
            this.comboBoxCodingTemplate.ItemHeight = 18;
            this.comboBoxCodingTemplate.MaxDropDownItems = 0;
            this.comboBoxCodingTemplate.MaxLength = 32767;
            this.comboBoxCodingTemplate.MaxValue = null;
            this.comboBoxCodingTemplate.MinValue = null;
            this.comboBoxCodingTemplate.Name = "comboBoxCodingTemplate";
            this.comboBoxCodingTemplate.NullValue = null;
            this.comboBoxCodingTemplate.OwnerOffset = 0;
            this.comboBoxCodingTemplate.ShowImageInEditorArea = true;
            this.comboBoxCodingTemplate.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            this.comboBoxCodingTemplate.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxCodingTemplate.SyncSelectionWithText = false;
            this.comboBoxCodingTemplate.Value = null;
            this.comboBoxCodingTemplate.ValueMember = "";
            this.comboBoxCodingTemplate.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxCodingTemplate_SelectedIndexChanged);
            // 
            // radRibbonBarGroup17
            // 
            this.radRibbonBarGroup17.AutoSize = false;
            this.radRibbonBarGroup17.Bounds = new System.Drawing.Rectangle(18, 0, 65, 125);
            this.radRibbonBarGroup17.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonPreviewImportStatement});
            this.radRibbonBarGroup17.Name = "radRibbonBarGroup17";
            this.radRibbonBarGroup17.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup17.Text = "Preview";
            // 
            // buttonPreviewImportStatement
            // 
            this.buttonPreviewImportStatement.AutoSize = false;
            this.buttonPreviewImportStatement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.buttonPreviewImportStatement.Bounds = new System.Drawing.Rectangle(0, 0, 50, 92);
            this.buttonPreviewImportStatement.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.buttonPreviewImportStatement.Image = ((System.Drawing.Image)(resources.GetObject("buttonPreviewImportStatement.Image")));
            this.buttonPreviewImportStatement.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonPreviewImportStatement.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPreviewImportStatement.Name = "buttonPreviewImportStatement";
            this.buttonPreviewImportStatement.Padding = new System.Windows.Forms.Padding(0);
            this.buttonPreviewImportStatement.Text = "Preview";
            this.buttonPreviewImportStatement.Click += new System.EventHandler(this.buttonPreviewImportStatement_Click);
            // 
            // radRibbonBarGroup29
            // 
            this.radRibbonBarGroup29.AutoSize = false;
            this.radRibbonBarGroup29.Bounds = new System.Drawing.Rectangle(23, 0, 125, 125);
            this.radRibbonBarGroup29.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.linkLabelLearnMore});
            this.radRibbonBarGroup29.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.radRibbonBarGroup29.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup29.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup29.Name = "radRibbonBarGroup29";
            this.radRibbonBarGroup29.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup29.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup29.Text = "Help";
            // 
            // linkLabelLearnMore
            // 
            this.linkLabelLearnMore.AutoSize = false;
            this.linkLabelLearnMore.Bounds = new System.Drawing.Rectangle(0, 0, 116, 98);
            this.linkLabelLearnMore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(97)))), ((int)(((byte)(250)))));
            this.linkLabelLearnMore.Name = "linkLabelLearnMore";
            this.linkLabelLearnMore.Text = "Watch online tutorials";
            this.linkLabelLearnMore.Click += new System.EventHandler(this.linkLabelLearnMore_Click);
            // 
            // TabPageExport
            // 
            this.TabPageExport.ClickMode = Telerik.WinControls.ClickMode.Release;
            this.TabPageExport.IsSelected = false;
            this.TabPageExport.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup11,
            this.filterGroup1,
            this.panelPaidStatus,
            this.radRibbonBarGroup27,
            this.radRibbonBarGroup16,
            this.radRibbonBarGroup15,
            this.radRibbonBarGroup14});
            this.TabPageExport.Name = "TabPageExport";
            this.TabPageExport.Text = "Export";
            this.TabPageExport.UseMnemonic = false;
            this.TabPageExport.Click += new System.EventHandler(this.tabControltabHolder_Click);
            // 
            // radRibbonBarGroup11
            // 
            this.radRibbonBarGroup11.AutoSize = false;
            this.radRibbonBarGroup11.Bounds = new System.Drawing.Rectangle(0, 0, 180, 125);
            this.radRibbonBarGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup7,
            this.radRibbonBarButtonGroup13});
            this.radRibbonBarGroup11.Name = "radRibbonBarGroup11";
            this.radRibbonBarGroup11.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup11.Text = "Select data type";
            // 
            // radRibbonBarButtonGroup7
            // 
            this.radRibbonBarButtonGroup7.AutoSize = false;
            this.radRibbonBarButtonGroup7.Bounds = new System.Drawing.Rectangle(0, 0, 172, 35);
            this.radRibbonBarButtonGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.comboBoxDatatype});
            this.radRibbonBarButtonGroup7.Name = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup7.Padding = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.radRibbonBarButtonGroup7.Text = "radRibbonBarButtonGroup7";
            // 
            // comboBoxDatatype
            // 
            this.comboBoxDatatype.ArrowButtonMinWidth = 17;
            this.comboBoxDatatype.AutoCompleteAppend = null;
            this.comboBoxDatatype.AutoCompleteDataSource = null;
            this.comboBoxDatatype.AutoCompleteSuggest = null;
            this.comboBoxDatatype.AutoSize = false;
            this.comboBoxDatatype.Bounds = new System.Drawing.Rectangle(2, 0, 165, 24);
            this.comboBoxDatatype.DataMember = "";
            this.comboBoxDatatype.DataSource = null;
            this.comboBoxDatatype.DefaultItemsCountInDropDown = 12;
            this.comboBoxDatatype.DefaultValue = null;
            this.comboBoxDatatype.DisplayMember = "";
            this.comboBoxDatatype.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxDatatype.DropDownAnimationEnabled = true;
            this.comboBoxDatatype.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxDatatype.EditableElementText = "Select";
            this.comboBoxDatatype.EditorElement = this.comboBoxDatatype;
            this.comboBoxDatatype.EditorManager = null;
            this.comboBoxDatatype.Filter = null;
            this.comboBoxDatatype.FilterExpression = "";
            this.comboBoxDatatype.Focusable = true;
            this.comboBoxDatatype.FormatString = "";
            this.comboBoxDatatype.FormattingEnabled = true;
            this.comboBoxDatatype.ItemHeight = 18;
            this.comboBoxDatatype.MaxDropDownItems = 0;
            this.comboBoxDatatype.MaxLength = 32767;
            this.comboBoxDatatype.MaxValue = null;
            this.comboBoxDatatype.MinValue = null;
            this.comboBoxDatatype.Name = "comboBoxDatatype";
            this.comboBoxDatatype.NullValue = null;
            this.comboBoxDatatype.OwnerOffset = 0;
            this.comboBoxDatatype.ShowImageInEditorArea = true;
            this.comboBoxDatatype.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxDatatype.SyncSelectionWithText = false;
            this.comboBoxDatatype.Value = null;
            this.comboBoxDatatype.ValueMember = "";
            this.comboBoxDatatype.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxDatatype_SelectedIndexChanged);
            // 
            // radRibbonBarButtonGroup13
            // 
            this.radRibbonBarButtonGroup13.AutoSize = false;
            this.radRibbonBarButtonGroup13.Bounds = new System.Drawing.Rectangle(0, 2, 172, 61);
            this.radRibbonBarButtonGroup13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.checkBoxReportType,
            this.comboBoxReportType});
            this.radRibbonBarButtonGroup13.Name = "radRibbonBarButtonGroup13";
            this.radRibbonBarButtonGroup13.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // checkBoxReportType
            // 
            this.checkBoxReportType.AutoSize = false;
            this.checkBoxReportType.Bounds = new System.Drawing.Rectangle(3, 5, 150, 18);
            this.checkBoxReportType.Checked = false;
            this.checkBoxReportType.Name = "checkBoxReportType";
            this.checkBoxReportType.ReadOnly = false;
            this.checkBoxReportType.Text = "Report";
            this.checkBoxReportType.CheckStateChanged += new System.EventHandler(this.checkBoxReportType_CheckedChanged);
            // 
            // comboBoxReportType
            // 
            this.comboBoxReportType.ArrowButtonMinWidth = 17;
            this.comboBoxReportType.AutoCompleteAppend = null;
            this.comboBoxReportType.AutoCompleteDataSource = null;
            this.comboBoxReportType.AutoCompleteSuggest = null;
            this.comboBoxReportType.AutoSize = false;
            this.comboBoxReportType.Bounds = new System.Drawing.Rectangle(2, 14, 165, 24);
            this.comboBoxReportType.DataMember = "";
            this.comboBoxReportType.DataSource = null;
            this.comboBoxReportType.DefaultValue = null;
            this.comboBoxReportType.DisplayMember = "";
            this.comboBoxReportType.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxReportType.DropDownAnimationEnabled = true;
            this.comboBoxReportType.EditableElementText = "";
            this.comboBoxReportType.EditorElement = this.comboBoxReportType;
            this.comboBoxReportType.EditorManager = null;
            this.comboBoxReportType.Filter = null;
            this.comboBoxReportType.FilterExpression = "";
            this.comboBoxReportType.Focusable = true;
            this.comboBoxReportType.FormatString = "";
            this.comboBoxReportType.FormattingEnabled = true;
            this.comboBoxReportType.ItemHeight = 18;
            this.comboBoxReportType.MaxDropDownItems = 0;
            this.comboBoxReportType.MaxLength = 32767;
            this.comboBoxReportType.MaxValue = null;
            this.comboBoxReportType.MinValue = null;
            this.comboBoxReportType.Name = "comboBoxReportType";
            this.comboBoxReportType.NullValue = null;
            this.comboBoxReportType.OwnerOffset = 0;
            this.comboBoxReportType.ShowImageInEditorArea = true;
            this.comboBoxReportType.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxReportType.Value = null;
            this.comboBoxReportType.ValueMember = "";
            this.comboBoxReportType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxReportType_SelectedIndexChanged);
            // 
            // filterGroup1
            // 
            this.filterGroup1.AutoSize = false;
            this.filterGroup1.Bounds = new System.Drawing.Rectangle(3, 0, 504, 125);
            this.filterGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup2,
            this.radRibbonBarButtonGroup9,
            this.groupDateFilter});
            this.filterGroup1.MaxSize = new System.Drawing.Size(0, 0);
            this.filterGroup1.MinSize = new System.Drawing.Size(0, 0);
            this.filterGroup1.Name = "filterGroup1";
            this.filterGroup1.Padding = new System.Windows.Forms.Padding(0);
            this.filterGroup1.Text = "Choose filter";
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.AutoSize = false;
            this.radRibbonBarButtonGroup2.Bounds = new System.Drawing.Rectangle(0, 0, 230, 98);
            this.radRibbonBarButtonGroup2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.checkBoxLastExport,
            this.checkBoxDaterange,
            this.checkBoxRefnumber,
            this.checkBoxEntityFilter,
            this.radCheckedDropDownListEntityFilter});
            this.radRibbonBarButtonGroup2.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup2";
            // 
            // checkBoxLastExport
            // 
            this.checkBoxLastExport.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxLastExport.AutoSize = false;
            this.checkBoxLastExport.Bounds = new System.Drawing.Rectangle(0, 0, 155, 17);
            this.checkBoxLastExport.Checked = false;
            this.checkBoxLastExport.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.checkBoxLastExport.Name = "checkBoxLastExport";
            this.checkBoxLastExport.ReadOnly = false;
            this.checkBoxLastExport.Text = "Only since last export";
            this.checkBoxLastExport.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxLastExport.CheckStateChanged += new System.EventHandler(this.checkBoxLastExport_CheckedChanged);
            // 
            // checkBoxDaterange
            // 
            this.checkBoxDaterange.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxDaterange.AutoSize = false;
            this.checkBoxDaterange.Bounds = new System.Drawing.Rectangle(0, 0, 120, 17);
            this.checkBoxDaterange.Checked = false;
            this.checkBoxDaterange.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.checkBoxDaterange.Name = "checkBoxDaterange";
            this.checkBoxDaterange.ReadOnly = false;
            this.checkBoxDaterange.Text = "Date range";
            this.checkBoxDaterange.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxDaterange.CheckStateChanged += new System.EventHandler(this.checkBoxDaterange_CheckedChanged);
            // 
            // checkBoxRefnumber
            // 
            this.checkBoxRefnumber.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxRefnumber.AutoSize = false;
            this.checkBoxRefnumber.Bounds = new System.Drawing.Rectangle(0, 0, 120, 17);
            this.checkBoxRefnumber.CanFocus = false;
            this.checkBoxRefnumber.Checked = false;
            this.checkBoxRefnumber.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.checkBoxRefnumber.Name = "checkBoxRefnumber";
            this.checkBoxRefnumber.ReadOnly = false;
            this.checkBoxRefnumber.Text = "Ref number";
            this.checkBoxRefnumber.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBoxRefnumber.CheckStateChanged += new System.EventHandler(this.checkBoxRefnumber_CheckedChanged);
            // 
            // checkBoxEntityFilter
            // 
            this.checkBoxEntityFilter.AutoSize = false;
            this.checkBoxEntityFilter.Bounds = new System.Drawing.Rectangle(0, 0, 110, 18);
            this.checkBoxEntityFilter.Checked = false;
            this.checkBoxEntityFilter.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.checkBoxEntityFilter.Name = "checkBoxEntityFilter";
            this.checkBoxEntityFilter.ReadOnly = false;
            this.checkBoxEntityFilter.StretchVertically = false;
            this.checkBoxEntityFilter.Text = "Name filter";
            this.checkBoxEntityFilter.CheckStateChanged += new System.EventHandler(this.checkBoxEntityFilter_CheckedChanged);
            // 
            // radRibbonBarButtonGroup9
            // 
            this.radRibbonBarButtonGroup9.AutoSize = false;
            this.radRibbonBarButtonGroup9.Bounds = new System.Drawing.Rectangle(5, 0, 128, 98);
            this.radRibbonBarButtonGroup9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement6,
            this.textBoxFromRefnumber,
            this.radLabelElement7,
            this.textBoxToRefNumber});
            this.radRibbonBarButtonGroup9.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarButtonGroup9.Name = "radRibbonBarButtonGroup9";
            this.radRibbonBarButtonGroup9.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup9.Padding = new System.Windows.Forms.Padding(2);
            this.radRibbonBarButtonGroup9.Text = "radRibbonBarButtonGroup9";
            // 
            // radLabelElement6
            // 
            this.radLabelElement6.AutoSize = false;
            this.radLabelElement6.Bounds = new System.Drawing.Rectangle(0, 0, 120, 20);
            this.radLabelElement6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElement6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radLabelElement6.Name = "radLabelElement6";
            this.radLabelElement6.Text = "From";
            this.radLabelElement6.TextWrap = true;
            // 
            // textBoxFromRefnumber
            // 
            this.textBoxFromRefnumber.AutoSize = false;
            this.textBoxFromRefnumber.Bounds = new System.Drawing.Rectangle(0, 3, 122, 24);
            this.textBoxFromRefnumber.Name = "textBoxFromRefnumber";
            this.textBoxFromRefnumber.Padding = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.textBoxFromRefnumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // radLabelElement7
            // 
            this.radLabelElement7.AutoSize = false;
            this.radLabelElement7.Bounds = new System.Drawing.Rectangle(0, 4, 120, 20);
            this.radLabelElement7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelElement7.Name = "radLabelElement7";
            this.radLabelElement7.Text = "To";
            this.radLabelElement7.TextWrap = true;
            // 
            // textBoxToRefNumber
            // 
            this.textBoxToRefNumber.AutoSize = false;
            this.textBoxToRefNumber.Bounds = new System.Drawing.Rectangle(0, 3, 122, 24);
            this.textBoxToRefNumber.Name = "textBoxToRefNumber";
            this.textBoxToRefNumber.Padding = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.textBoxToRefNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBoxToRefNumber.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // groupDateFilter
            // 
            this.groupDateFilter.AutoSize = false;
            this.groupDateFilter.Bounds = new System.Drawing.Rectangle(10, 0, 128, 98);
            this.groupDateFilter.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.From,
            this.dateTimePickerFrom,
            this.LabelTo,
            this.dateTimePickerTo});
            this.groupDateFilter.Margin = new System.Windows.Forms.Padding(0);
            this.groupDateFilter.Name = "groupDateFilter";
            this.groupDateFilter.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.groupDateFilter.Padding = new System.Windows.Forms.Padding(4);
            this.groupDateFilter.Text = "radRibbonBarButtonGroup23";
            // 
            // From
            // 
            this.From.AutoSize = false;
            this.From.Bounds = new System.Drawing.Rectangle(0, -1, 120, 20);
            this.From.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.From.Name = "From";
            this.From.Padding = new System.Windows.Forms.Padding(0);
            this.From.Text = "From";
            this.From.TextWrap = true;
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.AutoSize = false;
            this.dateTimePickerFrom.Bounds = new System.Drawing.Rectangle(0, 0, 120, 24);
            this.dateTimePickerFrom.CustomFormat = "dd-MM-yyyy";
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerFrom.MinDate = new System.DateTime(((long)(0)));
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.NullDate = new System.DateTime(((long)(0)));
            this.dateTimePickerFrom.Padding = new System.Windows.Forms.Padding(2);
            // 
            // LabelTo
            // 
            this.LabelTo.AutoSize = false;
            this.LabelTo.Bounds = new System.Drawing.Rectangle(0, 0, 120, 18);
            this.LabelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTo.Name = "LabelTo";
            this.LabelTo.Padding = new System.Windows.Forms.Padding(2);
            this.LabelTo.Text = "To";
            this.LabelTo.TextWrap = true;
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.AutoSize = false;
            this.dateTimePickerTo.Bounds = new System.Drawing.Rectangle(0, 3, 120, 24);
            this.dateTimePickerTo.CustomFormat = "dd-MM-yyyy";
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dateTimePickerTo.MinDate = new System.DateTime(((long)(0)));
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.NullDate = new System.DateTime(((long)(0)));
            this.dateTimePickerTo.Padding = new System.Windows.Forms.Padding(2);
            // 
            // panelPaidStatus
            // 
            this.panelPaidStatus.AutoSize = false;
            this.panelPaidStatus.Bounds = new System.Drawing.Rectangle(6, 0, 130, 125);
            this.panelPaidStatus.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup23});
            this.panelPaidStatus.Name = "panelPaidStatus";
            this.panelPaidStatus.Padding = new System.Windows.Forms.Padding(0);
            this.panelPaidStatus.Text = "Entity filter";
            // 
            // radRibbonBarButtonGroup23
            // 
            this.radRibbonBarButtonGroup23.AutoSize = false;
            this.radRibbonBarButtonGroup23.Bounds = new System.Drawing.Rectangle(0, 0, 122, 98);
            this.radRibbonBarButtonGroup23.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.checkBoxIncludeInactive,
            this.checkBoxPaidStatus,
            this.comboBoxPaidStatus});
            this.radRibbonBarButtonGroup23.Name = "radRibbonBarButtonGroup23";
            this.radRibbonBarButtonGroup23.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup23.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarButtonGroup23.Text = "radRibbonBarButtonGroup23";
            // 
            // checkBoxIncludeInactive
            // 
            this.checkBoxIncludeInactive.AutoSize = false;
            this.checkBoxIncludeInactive.Bounds = new System.Drawing.Rectangle(0, 0, 120, 26);
            this.checkBoxIncludeInactive.Checked = false;
            this.checkBoxIncludeInactive.Margin = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.checkBoxIncludeInactive.Name = "checkBoxIncludeInactive";
            this.checkBoxIncludeInactive.Padding = new System.Windows.Forms.Padding(4);
            this.checkBoxIncludeInactive.ReadOnly = false;
            this.checkBoxIncludeInactive.StretchVertically = false;
            this.checkBoxIncludeInactive.Text = "Include inactive";
            // 
            // checkBoxPaidStatus
            // 
            this.checkBoxPaidStatus.AutoSize = false;
            this.checkBoxPaidStatus.Bounds = new System.Drawing.Rectangle(0, 5, 120, 26);
            this.checkBoxPaidStatus.Checked = false;
            this.checkBoxPaidStatus.Margin = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.checkBoxPaidStatus.Name = "checkBoxPaidStatus";
            this.checkBoxPaidStatus.Padding = new System.Windows.Forms.Padding(4);
            this.checkBoxPaidStatus.ReadOnly = false;
            this.checkBoxPaidStatus.Text = "Paid status";
            this.checkBoxPaidStatus.CheckStateChanged += new System.EventHandler(this.checkBoxPaidStatus_CheckedChanged);
            // 
            // comboBoxPaidStatus
            // 
            this.comboBoxPaidStatus.ArrowButtonMinWidth = 17;
            this.comboBoxPaidStatus.AutoCompleteAppend = null;
            this.comboBoxPaidStatus.AutoCompleteDataSource = null;
            this.comboBoxPaidStatus.AutoCompleteSuggest = null;
            this.comboBoxPaidStatus.AutoSize = false;
            this.comboBoxPaidStatus.Bounds = new System.Drawing.Rectangle(2, 13, 115, 24);
            this.comboBoxPaidStatus.DataMember = "";
            this.comboBoxPaidStatus.DataSource = null;
            this.comboBoxPaidStatus.DefaultValue = null;
            this.comboBoxPaidStatus.DisplayMember = "";
            this.comboBoxPaidStatus.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxPaidStatus.DropDownAnimationEnabled = true;
            this.comboBoxPaidStatus.EditableElementText = "";
            this.comboBoxPaidStatus.EditorElement = this.comboBoxPaidStatus;
            this.comboBoxPaidStatus.EditorManager = null;
            this.comboBoxPaidStatus.Enabled = false;
            this.comboBoxPaidStatus.Filter = null;
            this.comboBoxPaidStatus.FilterExpression = "";
            this.comboBoxPaidStatus.Focusable = true;
            this.comboBoxPaidStatus.FormatString = "";
            this.comboBoxPaidStatus.FormattingEnabled = true;
            this.comboBoxPaidStatus.ItemHeight = 18;
            radListDataItem1.Text = "All";
            radListDataItem2.Text = "PaidOnly";
            radListDataItem3.Text = "NotPaidOnly";
            this.comboBoxPaidStatus.Items.Add(radListDataItem1);
            this.comboBoxPaidStatus.Items.Add(radListDataItem2);
            this.comboBoxPaidStatus.Items.Add(radListDataItem3);
            this.comboBoxPaidStatus.Margin = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.comboBoxPaidStatus.MaxDropDownItems = 0;
            this.comboBoxPaidStatus.MaxLength = 32767;
            this.comboBoxPaidStatus.MaxValue = null;
            this.comboBoxPaidStatus.MinValue = null;
            this.comboBoxPaidStatus.Name = "comboBoxPaidStatus";
            this.comboBoxPaidStatus.NullValue = null;
            this.comboBoxPaidStatus.OwnerOffset = 0;
            this.comboBoxPaidStatus.Padding = new System.Windows.Forms.Padding(0);
            this.comboBoxPaidStatus.ShowImageInEditorArea = true;
            this.comboBoxPaidStatus.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxPaidStatus.Value = null;
            this.comboBoxPaidStatus.ValueMember = "";
            this.comboBoxPaidStatus.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxPaidStatus_SelectedIndexChanged);
            // 
            // radRibbonBarGroup27
            // 
            this.radRibbonBarGroup27.AutoSize = false;
            this.radRibbonBarGroup27.Bounds = new System.Drawing.Rectangle(9, 0, 120, 125);
            this.radRibbonBarGroup27.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radChkDDListAccountTypeFilter});
            this.radRibbonBarGroup27.Name = "radRibbonBarGroup27";
            this.radRibbonBarGroup27.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup27.Text = "Account";
            // 
            // radRibbonBarGroup16
            // 
            this.radRibbonBarGroup16.AutoSize = false;
            this.radRibbonBarGroup16.Bounds = new System.Drawing.Rectangle(12, 0, 70, 125);
            this.radRibbonBarGroup16.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radImageButtonElement1});
            this.radRibbonBarGroup16.Name = "radRibbonBarGroup16";
            this.radRibbonBarGroup16.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup16.Text = "Clear filters";
            // 
            // radImageButtonElement1
            // 
            this.radImageButtonElement1.AutoSize = false;
            this.radImageButtonElement1.Bounds = new System.Drawing.Rectangle(0, 0, 63, 100);
            this.radImageButtonElement1.Image = ((System.Drawing.Image)(resources.GetObject("radImageButtonElement1.Image")));
            this.radImageButtonElement1.ImageIndexClicked = 0;
            this.radImageButtonElement1.ImageIndexHovered = 0;
            this.radImageButtonElement1.Name = "radImageButtonElement1";
            this.radImageButtonElement1.Text = "radImageButtonElement1";
            this.radImageButtonElement1.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // radRibbonBarGroup15
            // 
            this.radRibbonBarGroup15.AutoSize = false;
            this.radRibbonBarGroup15.Bounds = new System.Drawing.Rectangle(15, 0, 70, 125);
            this.radRibbonBarGroup15.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radRibbonBarGroup15.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonGet});
            this.radRibbonBarGroup15.Name = "radRibbonBarGroup15";
            this.radRibbonBarGroup15.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup15.Text = "Get";
            // 
            // buttonGet
            // 
            this.buttonGet.AutoSize = false;
            this.buttonGet.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.buttonGet.Bounds = new System.Drawing.Rectangle(0, 0, 63, 100);
            this.buttonGet.Image = ((System.Drawing.Image)(resources.GetObject("buttonGet.Image")));
            this.buttonGet.Name = "buttonGet";
            this.buttonGet.Text = "radImageButtonElement2";
            this.buttonGet.Click += new System.EventHandler(this.buttonGet_Click);
            // 
            // radRibbonBarGroup14
            // 
            this.radRibbonBarGroup14.AutoSize = false;
            this.radRibbonBarGroup14.Bounds = new System.Drawing.Rectangle(18, 0, 140, 125);
            this.radRibbonBarGroup14.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.GroupBoxColumnChooser});
            this.radRibbonBarGroup14.Name = "radRibbonBarGroup14";
            this.radRibbonBarGroup14.Padding = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup14.Text = "Column chooser";
            // 
            // GroupBoxColumnChooser
            // 
            this.GroupBoxColumnChooser.AutoSize = false;
            this.GroupBoxColumnChooser.Bounds = new System.Drawing.Rectangle(0, 0, 132, 100);
            this.GroupBoxColumnChooser.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.GroupBoxColumnChooser.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.comboBoxColChooser,
            this.btnColChooserExport,
            this.btnResetTemplate});
            this.GroupBoxColumnChooser.Name = "GroupBoxColumnChooser";
            this.GroupBoxColumnChooser.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.GroupBoxColumnChooser.Text = "radRibbonBarButtonGroup13";
            // 
            // comboBoxColChooser
            // 
            this.comboBoxColChooser.ArrowButtonMinWidth = 17;
            this.comboBoxColChooser.AutoCompleteAppend = null;
            this.comboBoxColChooser.AutoCompleteDataSource = null;
            this.comboBoxColChooser.AutoCompleteSuggest = null;
            this.comboBoxColChooser.AutoSize = false;
            this.comboBoxColChooser.Bounds = new System.Drawing.Rectangle(3, 2, 124, 24);
            this.comboBoxColChooser.DataMember = "";
            this.comboBoxColChooser.DataSource = null;
            this.comboBoxColChooser.DefaultValue = null;
            this.comboBoxColChooser.DisplayMember = "";
            this.comboBoxColChooser.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.comboBoxColChooser.DropDownAnimationEnabled = true;
            this.comboBoxColChooser.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxColChooser.EditableElementText = "";
            this.comboBoxColChooser.EditorElement = this.comboBoxColChooser;
            this.comboBoxColChooser.EditorManager = null;
            this.comboBoxColChooser.Filter = null;
            this.comboBoxColChooser.FilterExpression = "";
            this.comboBoxColChooser.Focusable = true;
            this.comboBoxColChooser.FormatString = "";
            this.comboBoxColChooser.FormattingEnabled = true;
            this.comboBoxColChooser.ItemHeight = 18;
            this.comboBoxColChooser.MaxDropDownItems = 0;
            this.comboBoxColChooser.MaxLength = 32767;
            this.comboBoxColChooser.MaxValue = null;
            this.comboBoxColChooser.MinValue = null;
            this.comboBoxColChooser.Name = "comboBoxColChooser";
            this.comboBoxColChooser.NullValue = null;
            this.comboBoxColChooser.OwnerOffset = 0;
            this.comboBoxColChooser.Padding = new System.Windows.Forms.Padding(4);
            this.comboBoxColChooser.ShowImageInEditorArea = true;
            this.comboBoxColChooser.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.comboBoxColChooser.SyncSelectionWithText = false;
            this.comboBoxColChooser.Value = null;
            this.comboBoxColChooser.ValueMember = "";
            this.comboBoxColChooser.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBoxColChooser.MouseDown += new System.Windows.Forms.MouseEventHandler(this.comboBoxColChooser_Click);
            this.comboBoxColChooser.Click += new System.EventHandler(this.comboBoxColChooser_Click);
            // 
            // btnColChooserExport
            // 
            this.btnColChooserExport.AutoSize = false;
            this.btnColChooserExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.btnColChooserExport.Bounds = new System.Drawing.Rectangle(3, 5, 124, 33);
            this.btnColChooserExport.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.btnColChooserExport.Image = ((System.Drawing.Image)(resources.GetObject("btnColChooserExport.Image")));
            this.btnColChooserExport.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnColChooserExport.Name = "btnColChooserExport";
            this.btnColChooserExport.ShowBorder = false;
            this.btnColChooserExport.Text = "Export template";
            this.btnColChooserExport.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnColChooserExport.Click += new System.EventHandler(this.btnColChooserExport_Click);
            // 
            // btnResetTemplate
            // 
            this.btnResetTemplate.AutoSize = false;
            this.btnResetTemplate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.btnResetTemplate.Bounds = new System.Drawing.Rectangle(3, 5, 124, 33);
            this.btnResetTemplate.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.btnResetTemplate.Name = "btnResetTemplate";
            this.btnResetTemplate.ShowBorder = false;
            this.btnResetTemplate.Text = "Reset";
            this.btnResetTemplate.Click += new System.EventHandler(this.btnResetTemplate_Click);
            // 
            // TabPageEDI
            // 
            this.TabPageEDI.ClickMode = Telerik.WinControls.ClickMode.Release;
            this.TabPageEDI.IsSelected = false;
            this.TabPageEDI.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup20,
            this.radRibbonBarGroup21,
            this.radRibbonBarGroup22,
            this.radRibbonBarGroup23,
            this.radRibbonBarGroup24,
            this.radRibbonBarGroup25});
            this.TabPageEDI.Name = "TabPageEDI";
            this.TabPageEDI.Text = "Automate";
            this.TabPageEDI.UseMnemonic = false;
            this.TabPageEDI.Click += new System.EventHandler(this.tabControltabHolder_Click);
            // 
            // radRibbonBarGroup20
            // 
            this.radRibbonBarGroup20.AutoSize = false;
            this.radRibbonBarGroup20.Bounds = new System.Drawing.Rectangle(0, 0, 90, 125);
            this.radRibbonBarGroup20.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemSendReceive});
            this.radRibbonBarGroup20.Name = "radRibbonBarGroup20";
            this.radRibbonBarGroup20.Text = "Send/Receive";
            // 
            // radMenuItemSendReceive
            // 
            this.radMenuItemSendReceive.AutoSize = false;
            this.radMenuItemSendReceive.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemSendReceive.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemSendReceive.Image")));
            this.radMenuItemSendReceive.Name = "radMenuItemSendReceive";
            this.radMenuItemSendReceive.Text = "Send/Receive";
            this.radMenuItemSendReceive.Click += new System.EventHandler(this.radMenuItemSendReceive_Click);
            // 
            // radRibbonBarGroup21
            // 
            this.radRibbonBarGroup21.AutoSize = false;
            this.radRibbonBarGroup21.Bounds = new System.Drawing.Rectangle(5, 0, 90, 125);
            this.radRibbonBarGroup21.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemTradingPartner});
            this.radRibbonBarGroup21.Name = "radRibbonBarGroup21";
            this.radRibbonBarGroup21.Text = "Trading partner";
            // 
            // radMenuItemTradingPartner
            // 
            this.radMenuItemTradingPartner.AutoSize = false;
            this.radMenuItemTradingPartner.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemTradingPartner.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemTradingPartner.Image")));
            this.radMenuItemTradingPartner.Name = "radMenuItemTradingPartner";
            this.radMenuItemTradingPartner.Text = "Trading Partner";
            this.radMenuItemTradingPartner.Click += new System.EventHandler(this.radMenuItemTradingPartner_Click);
            // 
            // radRibbonBarGroup22
            // 
            this.radRibbonBarGroup22.AutoSize = false;
            this.radRibbonBarGroup22.Bounds = new System.Drawing.Rectangle(10, 0, 90, 125);
            this.radRibbonBarGroup22.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemMailSettings});
            this.radRibbonBarGroup22.Name = "radRibbonBarGroup22";
            this.radRibbonBarGroup22.Text = "Mail settings";
            // 
            // radMenuItemMailSettings
            // 
            this.radMenuItemMailSettings.AutoSize = false;
            this.radMenuItemMailSettings.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemMailSettings.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemMailSettings.Image")));
            this.radMenuItemMailSettings.Name = "radMenuItemMailSettings";
            this.radMenuItemMailSettings.Text = "Mail Settings";
            this.radMenuItemMailSettings.Click += new System.EventHandler(this.radMenuItemMailSettings_Click);
            // 
            // radRibbonBarGroup23
            // 
            this.radRibbonBarGroup23.AutoSize = false;
            this.radRibbonBarGroup23.Bounds = new System.Drawing.Rectangle(15, 0, 90, 125);
            this.radRibbonBarGroup23.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemRules});
            this.radRibbonBarGroup23.Name = "radRibbonBarGroup23";
            this.radRibbonBarGroup23.Text = "Rules";
            // 
            // radMenuItemRules
            // 
            this.radMenuItemRules.AutoSize = false;
            this.radMenuItemRules.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemRules.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemRules.Image")));
            this.radMenuItemRules.Name = "radMenuItemRules";
            this.radMenuItemRules.Text = "Rules";
            this.radMenuItemRules.Click += new System.EventHandler(this.radMenuItemRules_Click);
            // 
            // radRibbonBarGroup24
            // 
            this.radRibbonBarGroup24.AutoSize = false;
            this.radRibbonBarGroup24.Bounds = new System.Drawing.Rectangle(20, 0, 90, 125);
            this.radRibbonBarGroup24.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemMonitor});
            this.radRibbonBarGroup24.Name = "radRibbonBarGroup24";
            this.radRibbonBarGroup24.Text = "Monitor";
            // 
            // radMenuItemMonitor
            // 
            this.radMenuItemMonitor.AutoSize = false;
            this.radMenuItemMonitor.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemMonitor.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemMonitor.Image")));
            this.radMenuItemMonitor.Name = "radMenuItemMonitor";
            this.radMenuItemMonitor.Text = "Monitor";
            this.radMenuItemMonitor.Click += new System.EventHandler(this.radMenuItemMonitor_Click);
            // 
            // radRibbonBarGroup25
            // 
            this.radRibbonBarGroup25.AutoSize = false;
            this.radRibbonBarGroup25.Bounds = new System.Drawing.Rectangle(25, 0, 90, 125);
            this.radRibbonBarGroup25.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemRefresh});
            this.radRibbonBarGroup25.Name = "radRibbonBarGroup25";
            this.radRibbonBarGroup25.Text = "Refresh";
            // 
            // radMenuItemRefresh
            // 
            this.radMenuItemRefresh.AutoSize = false;
            this.radMenuItemRefresh.Bounds = new System.Drawing.Rectangle(0, 0, 81, 98);
            this.radMenuItemRefresh.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItemRefresh.Image")));
            this.radMenuItemRefresh.Name = "radMenuItemRefresh";
            this.radMenuItemRefresh.Text = "Refresh";
            this.radMenuItemRefresh.Click += new System.EventHandler(this.radMenuItemRefresh_Click);
            // 
            // TabPageAbout
            // 
            this.TabPageAbout.ClickMode = Telerik.WinControls.ClickMode.Hover;
            this.TabPageAbout.IsSelected = false;
            this.TabPageAbout.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup6,
            this.radRibbonBarGroup18,
            this.radRibbonBarGroup12,
            this.radRibbonBarGroup13,
            this.radRibbonBarGroup19});
            this.TabPageAbout.Name = "TabPageAbout";
            this.TabPageAbout.Text = "About";
            this.TabPageAbout.UseMnemonic = false;
            this.TabPageAbout.Click += new System.EventHandler(this.tabControltabHolder_Click);
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.AutoSize = false;
            this.radRibbonBarGroup6.Bounds = new System.Drawing.Rectangle(0, 0, 134, 125);
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup17});
            this.radRibbonBarGroup6.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup6.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Text = "Settings";
            // 
            // radRibbonBarButtonGroup17
            // 
            this.radRibbonBarButtonGroup17.AutoSize = false;
            this.radRibbonBarButtonGroup17.Bounds = new System.Drawing.Rectangle(2, 0, 122, 98);
            this.radRibbonBarButtonGroup17.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.checkBoxMaintainLog});
            this.radRibbonBarButtonGroup17.Name = "radRibbonBarButtonGroup17";
            this.radRibbonBarButtonGroup17.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // checkBoxMaintainLog
            // 
            this.checkBoxMaintainLog.AutoSize = false;
            this.checkBoxMaintainLog.Bounds = new System.Drawing.Rectangle(2, 0, 120, 18);
            this.checkBoxMaintainLog.Checked = false;
            this.checkBoxMaintainLog.Name = "checkBoxMaintainLog";
            this.checkBoxMaintainLog.ReadOnly = false;
            this.checkBoxMaintainLog.Text = "Maintain log files";
            this.checkBoxMaintainLog.CheckStateChanged += new System.EventHandler(this.checkBoxMaintainLog_CheckedChanged);
            // 
            // radRibbonBarGroup18
            // 
            this.radRibbonBarGroup18.AutoSize = false;
            this.radRibbonBarGroup18.Bounds = new System.Drawing.Rectangle(2, 0, 136, 125);
            this.radRibbonBarGroup18.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup15});
            this.radRibbonBarGroup18.Name = "radRibbonBarGroup18";
            this.radRibbonBarGroup18.Text = "Product info";
            // 
            // radRibbonBarButtonGroup15
            // 
            this.radRibbonBarButtonGroup15.AutoSize = false;
            this.radRibbonBarButtonGroup15.Bounds = new System.Drawing.Rectangle(2, 0, 124, 98);
            this.radRibbonBarButtonGroup15.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.labelProductName,
            this.labelBN,
            this.buttonReactivateLicense,
            this.buttonLicenseInfo});
            this.radRibbonBarButtonGroup15.Name = "radRibbonBarButtonGroup15";
            this.radRibbonBarButtonGroup15.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // labelProductName
            // 
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Padding = new System.Windows.Forms.Padding(1);
            this.labelProductName.Text = "radLabelElement4";
            this.labelProductName.TextWrap = true;
            // 
            // labelBN
            // 
            this.labelBN.Name = "labelBN";
            this.labelBN.Padding = new System.Windows.Forms.Padding(1);
            this.labelBN.Text = "radLabelElement4";
            this.labelBN.TextWrap = true;
            // 
            // buttonReactivateLicense
            // 
            this.buttonReactivateLicense.AutoSize = false;
            this.buttonReactivateLicense.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.buttonReactivateLicense.Bounds = new System.Drawing.Rectangle(0, 0, 122, 26);
            this.buttonReactivateLicense.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.buttonReactivateLicense.Image = ((System.Drawing.Image)(resources.GetObject("buttonReactivateLicense.Image")));
            this.buttonReactivateLicense.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonReactivateLicense.Name = "buttonReactivateLicense";
            this.buttonReactivateLicense.Padding = new System.Windows.Forms.Padding(1);
            this.buttonReactivateLicense.ShowBorder = false;
            this.buttonReactivateLicense.Text = "Reactivate license";
            this.buttonReactivateLicense.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReactivateLicense.Click += new System.EventHandler(this.buttonReactivateLicense_Click);
            // 
            // buttonLicenseInfo
            // 
            this.buttonLicenseInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.buttonLicenseInfo.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.buttonLicenseInfo.Image = ((System.Drawing.Image)(resources.GetObject("buttonLicenseInfo.Image")));
            this.buttonLicenseInfo.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonLicenseInfo.Name = "buttonLicenseInfo";
            this.buttonLicenseInfo.Padding = new System.Windows.Forms.Padding(1);
            this.buttonLicenseInfo.ShowBorder = false;
            this.buttonLicenseInfo.Text = "License info";
            this.buttonLicenseInfo.Click += new System.EventHandler(this.buttonLicenseInfo_Click_1);
            // 
            // radRibbonBarGroup12
            // 
            this.radRibbonBarGroup12.AutoSize = false;
            this.radRibbonBarGroup12.Bounds = new System.Drawing.Rectangle(4, 0, 154, 125);
            this.radRibbonBarGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup3});
            this.radRibbonBarGroup12.Name = "radRibbonBarGroup12";
            this.radRibbonBarGroup12.Text = "Support";
            // 
            // radRibbonBarButtonGroup3
            // 
            this.radRibbonBarButtonGroup3.AutoSize = false;
            this.radRibbonBarButtonGroup3.Bounds = new System.Drawing.Rectangle(2, 0, 142, 98);
            this.radRibbonBarButtonGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement8,
            this.radLabelElement9,
            this.radLabelElement10});
            this.radRibbonBarButtonGroup3.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup3.Padding = new System.Windows.Forms.Padding(0);
            // 
            // radLabelElement8
            // 
            this.radLabelElement8.AutoSize = false;
            this.radLabelElement8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.radLabelElement8.Bounds = new System.Drawing.Rectangle(0, 0, 140, 32);
            this.radLabelElement8.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.radLabelElement8.Image = ((System.Drawing.Image)(resources.GetObject("radLabelElement8.Image")));
            this.radLabelElement8.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElement8.Name = "radLabelElement8";
            this.radLabelElement8.Padding = new System.Windows.Forms.Padding(4);
            this.radLabelElement8.ShowBorder = false;
            this.radLabelElement8.Text = "Buy";
            this.radLabelElement8.Click += new System.EventHandler(this.linkLabelBuy_LinkClicked);
            // 
            // radLabelElement9
            // 
            this.radLabelElement9.AutoSize = false;
            this.radLabelElement9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.radLabelElement9.Bounds = new System.Drawing.Rectangle(0, 0, 140, 32);
            this.radLabelElement9.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.radLabelElement9.Image = ((System.Drawing.Image)(resources.GetObject("radLabelElement9.Image")));
            this.radLabelElement9.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElement9.Name = "radLabelElement9";
            this.radLabelElement9.Padding = new System.Windows.Forms.Padding(4);
            this.radLabelElement9.ShowBorder = false;
            this.radLabelElement9.Text = "Support";
            this.radLabelElement9.Click += new System.EventHandler(this.linkLabelSupport_LinkClicked);
            // 
            // radLabelElement10
            // 
            this.radLabelElement10.AutoSize = false;
            this.radLabelElement10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(236)))), ((int)(((byte)(234)))));
            this.radLabelElement10.Bounds = new System.Drawing.Rectangle(0, 0, 140, 32);
            this.radLabelElement10.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.radLabelElement10.Image = ((System.Drawing.Image)(resources.GetObject("radLabelElement10.Image")));
            this.radLabelElement10.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radLabelElement10.Name = "radLabelElement10";
            this.radLabelElement10.Padding = new System.Windows.Forms.Padding(4);
            this.radLabelElement10.ShowBorder = false;
            this.radLabelElement10.Text = "About Zed Systems";
            this.radLabelElement10.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radLabelElement10.Click += new System.EventHandler(this.linkLabelZed_LinkClicked);
            // 
            // radRibbonBarGroup13
            // 
            this.radRibbonBarGroup13.AutoSize = false;
            this.radRibbonBarGroup13.Bounds = new System.Drawing.Rectangle(105, 0, 100, 125);
            this.radRibbonBarGroup13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btncheckforupdate});
            this.radRibbonBarGroup13.Name = "radRibbonBarGroup13";
            this.radRibbonBarGroup13.Text = "Check for updates";
            // 
            // btncheckforupdate
            // 
            this.btncheckforupdate.AutoSize = false;
            this.btncheckforupdate.Bounds = new System.Drawing.Rectangle(0, 0, 92, 100);
            this.btncheckforupdate.Image = ((System.Drawing.Image)(resources.GetObject("btncheckforupdate.Image")));
            this.btncheckforupdate.Name = "btncheckforupdate";
            this.btncheckforupdate.Text = "radImageButtonElement2";
            this.btncheckforupdate.Click += new System.EventHandler(this.btncheckforupdate_Click);
            // 
            // radRibbonBarGroup19
            // 
            this.radRibbonBarGroup19.AutoSize = false;
            this.radRibbonBarGroup19.Bounds = new System.Drawing.Rectangle(-94, 0, 97, 125);
            this.radRibbonBarGroup19.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.buttonDeactivateLicense});
            this.radRibbonBarGroup19.Name = "radRibbonBarGroup19";
            this.radRibbonBarGroup19.Text = "Deactivate license";
            // 
            // buttonDeactivateLicense
            // 
            this.buttonDeactivateLicense.AutoSize = false;
            this.buttonDeactivateLicense.Bounds = new System.Drawing.Rectangle(0, 0, 90, 100);
            this.buttonDeactivateLicense.Image = ((System.Drawing.Image)(resources.GetObject("buttonDeactivateLicense.Image")));
            this.buttonDeactivateLicense.Name = "buttonDeactivateLicense";
            this.buttonDeactivateLicense.Text = "radImageButtonElement2";
            this.buttonDeactivateLicense.Click += new System.EventHandler(this.buttonDeactivateLicense_Click);
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "Tab seprated";
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Pipe seprated";
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Comma seprated";
            // 
            // labelMessageID
            // 
            this.labelMessageID.Location = new System.Drawing.Point(0, 0);
            this.labelMessageID.Name = "labelMessageID";
            this.labelMessageID.Size = new System.Drawing.Size(100, 18);
            this.labelMessageID.TabIndex = 0;
            this.labelMessageID.Visible = false;
            // 
            // labelMailCount
            // 
            this.labelMailCount.Location = new System.Drawing.Point(0, 0);
            this.labelMailCount.Name = "labelMailCount";
            this.labelMailCount.Size = new System.Drawing.Size(100, 18);
            this.labelMailCount.TabIndex = 0;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.growLabelQBCompanyFile,
            this.textBoxFileName,
            this.toolStripStatusLabelProduct,
            this.toolStripStatusLabelMajorVersion,
            this.toolStripStatusLabelMinorVersion,
            this.toolStripStatusLabelCountry,
            this.toolStripStatusLabelConnectionStatus,
            this.toolStripProgressBarIndicator});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 693);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(1352, 30);
            this.radStatusStrip1.SizingGrip = false;
            this.radStatusStrip1.TabIndex = 1;
            // 
            // growLabelQBCompanyFile
            // 
            this.growLabelQBCompanyFile.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.growLabelQBCompanyFile.AutoSize = false;
            this.growLabelQBCompanyFile.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.growLabelQBCompanyFile.Bounds = new System.Drawing.Rectangle(-1, -5, 460, 29);
            this.growLabelQBCompanyFile.Name = "growLabelQBCompanyFile";
            this.growLabelQBCompanyFile.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.growLabelQBCompanyFile, false);
            this.growLabelQBCompanyFile.Text = "Comapny:";
            this.growLabelQBCompanyFile.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.growLabelQBCompanyFile.TextWrap = true;
            this.growLabelQBCompanyFile.UseCompatibleTextRendering = false;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.AutoSize = false;
            this.textBoxFileName.Bounds = new System.Drawing.Rectangle(7, -5, 340, 29);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.textBoxFileName, false);
            this.textBoxFileName.Text = "";
            this.textBoxFileName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.textBoxFileName.TextWrap = true;
            this.textBoxFileName.UseCompatibleTextRendering = false;
            this.textBoxFileName.TextChanged += new System.EventHandler(this.textBoxFileName_Leave);
            // 
            // toolStripStatusLabelProduct
            // 
            this.toolStripStatusLabelProduct.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripStatusLabelProduct.AutoSize = false;
            this.toolStripStatusLabelProduct.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.toolStripStatusLabelProduct.Bounds = new System.Drawing.Rectangle(20, 0, 230, 18);
            this.toolStripStatusLabelProduct.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripStatusLabelProduct.Name = "toolStripStatusLabelProduct";
            this.toolStripStatusLabelProduct.Opacity = 0.75D;
            this.toolStripStatusLabelProduct.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabelProduct, false);
            this.toolStripStatusLabelProduct.UseCompatibleTextRendering = false;
            // 
            // toolStripStatusLabelMajorVersion
            // 
            this.toolStripStatusLabelMajorVersion.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripStatusLabelMajorVersion.AutoSize = false;
            this.toolStripStatusLabelMajorVersion.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.toolStripStatusLabelMajorVersion.Bounds = new System.Drawing.Rectangle(20, 0, 24, 18);
            this.toolStripStatusLabelMajorVersion.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripStatusLabelMajorVersion.Name = "toolStripStatusLabelMajorVersion";
            this.toolStripStatusLabelMajorVersion.Opacity = 0.75D;
            this.toolStripStatusLabelMajorVersion.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripStatusLabelMajorVersion.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabelMajorVersion, false);
            this.toolStripStatusLabelMajorVersion.UseCompatibleTextRendering = false;
            // 
            // toolStripStatusLabelMinorVersion
            // 
            this.toolStripStatusLabelMinorVersion.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripStatusLabelMinorVersion.AutoSize = false;
            this.toolStripStatusLabelMinorVersion.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.toolStripStatusLabelMinorVersion.Bounds = new System.Drawing.Rectangle(20, 0, 20, 18);
            this.toolStripStatusLabelMinorVersion.Name = "toolStripStatusLabelMinorVersion";
            this.toolStripStatusLabelMinorVersion.Opacity = 0.75D;
            this.toolStripStatusLabelMinorVersion.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripStatusLabelMinorVersion.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabelMinorVersion, false);
            this.toolStripStatusLabelMinorVersion.UseCompatibleTextRendering = false;
            // 
            // toolStripStatusLabelCountry
            // 
            this.toolStripStatusLabelCountry.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripStatusLabelCountry.AutoSize = false;
            this.toolStripStatusLabelCountry.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.toolStripStatusLabelCountry.Bounds = new System.Drawing.Rectangle(19, 0, 20, 18);
            this.toolStripStatusLabelCountry.Name = "toolStripStatusLabelCountry";
            this.toolStripStatusLabelCountry.Opacity = 0.75D;
            this.toolStripStatusLabelCountry.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabelCountry, false);
            this.toolStripStatusLabelCountry.UseCompatibleTextRendering = false;
            // 
            // toolStripStatusLabelConnectionStatus
            // 
            this.toolStripStatusLabelConnectionStatus.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripStatusLabelConnectionStatus.AutoSize = false;
            this.toolStripStatusLabelConnectionStatus.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize;
            this.toolStripStatusLabelConnectionStatus.Bounds = new System.Drawing.Rectangle(19, 0, 80, 18);
            this.toolStripStatusLabelConnectionStatus.Name = "toolStripStatusLabelConnectionStatus";
            this.toolStripStatusLabelConnectionStatus.Opacity = 0.75D;
            this.toolStripStatusLabelConnectionStatus.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.radStatusStrip1.SetSpring(this.toolStripStatusLabelConnectionStatus, false);
            this.toolStripStatusLabelConnectionStatus.UseCompatibleTextRendering = false;
            // 
            // toolStripProgressBarIndicator
            // 
            this.toolStripProgressBarIndicator.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripProgressBarIndicator.AutoSize = false;
            this.toolStripProgressBarIndicator.Bounds = new System.Drawing.Rectangle(18, 0, 90, 18);
            this.toolStripProgressBarIndicator.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolStripProgressBarIndicator.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.toolStripProgressBarIndicator.FlipText = false;
            this.toolStripProgressBarIndicator.Hatch = false;
            this.toolStripProgressBarIndicator.Margin = new System.Windows.Forms.Padding(0);
            this.toolStripProgressBarIndicator.Name = "toolStripProgressBarIndicator";
            this.toolStripProgressBarIndicator.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripProgressBarIndicator.PositionOffset = new System.Drawing.SizeF(0F, 0F);
            this.toolStripProgressBarIndicator.SeparatorColor1 = System.Drawing.Color.White;
            this.toolStripProgressBarIndicator.SeparatorColor2 = System.Drawing.Color.White;
            this.toolStripProgressBarIndicator.SeparatorColor3 = System.Drawing.Color.White;
            this.toolStripProgressBarIndicator.SeparatorColor4 = System.Drawing.Color.White;
            this.toolStripProgressBarIndicator.SeparatorGradientAngle = 0;
            this.toolStripProgressBarIndicator.SeparatorGradientPercentage1 = 0.4F;
            this.toolStripProgressBarIndicator.SeparatorGradientPercentage2 = 0.6F;
            this.toolStripProgressBarIndicator.SeparatorNumberOfColors = 2;
            this.toolStripProgressBarIndicator.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.radStatusStrip1.SetSpring(this.toolStripProgressBarIndicator, false);
            this.toolStripProgressBarIndicator.StepWidth = 14;
            this.toolStripProgressBarIndicator.SweepAngle = 90;
            this.toolStripProgressBarIndicator.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolStripProgressBarIndicator.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolStripProgressBarIndicator.UseCompatibleTextRendering = false;
            // 
            // openFileDialogBrowseFile
            // 
            this.openFileDialogBrowseFile.RestoreDirectory = true;
            // 
            // backgroundWorkerQBItemLoader
            // 
            this.backgroundWorkerQBItemLoader.WorkerReportsProgress = true;
            this.backgroundWorkerQBItemLoader.WorkerSupportsCancellation = true;
            this.backgroundWorkerQBItemLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerQBItemLoader_DoWork);
            this.backgroundWorkerQBItemLoader.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerQBItemLoader_ProgressChanged);
            this.backgroundWorkerQBItemLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerQBItemLoader_RunWorkerCompleted);
            // 
            // backgroundWorkerExportLoad
            // 
            this.backgroundWorkerExportLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerExportLoad_DoWork);
            this.backgroundWorkerExportLoad.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerExportLoad_ProgressChanged);
            this.backgroundWorkerExportLoad.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerExportLoad_RunWorkerCompleted);
            // 
            // backgroundWorkerStartupProcessLoader
            // 
            this.backgroundWorkerStartupProcessLoader.WorkerReportsProgress = true;
            this.backgroundWorkerStartupProcessLoader.WorkerSupportsCancellation = true;
            this.backgroundWorkerStartupProcessLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerStartupProcessLoader_DoWork);
            this.backgroundWorkerStartupProcessLoader.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerStartupProcessLoader_ProgressChanged);
            this.backgroundWorkerStartupProcessLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerStartupProcessLoader_RunWorkerCompleted);
            // 
            // toolTipInfo
            // 
            this.toolTipInfo.AutomaticDelay = 100;
            this.toolTipInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // contextMenuStripAddTo
            // 
            this.contextMenuStripAddTo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AxisToolStripMenuItem,
            this.toolStripMenuItem1,
            this.deleteMessageToolStripMenuItem});
            this.contextMenuStripAddTo.Name = "contextMenuStripAddTo";
            this.contextMenuStripAddTo.Size = new System.Drawing.Size(167, 70);
            // 
            // AxisToolStripMenuItem
            // 
            this.AxisToolStripMenuItem.Name = "AxisToolStripMenuItem";
            this.AxisToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.AxisToolStripMenuItem.Text = "Not connected";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem1.Text = "Shipping Request";
            // 
            // deleteMessageToolStripMenuItem
            // 
            this.deleteMessageToolStripMenuItem.Name = "deleteMessageToolStripMenuItem";
            this.deleteMessageToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.deleteMessageToolStripMenuItem.Text = "Delete Message";
            // 
            // openFileDialogForImportTemplate
            // 
            this.openFileDialogForImportTemplate.FileName = "openFileDialog1";
            // 
            // notifyIconRecievedMessages
            // 
            this.notifyIconRecievedMessages.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIconRecievedMessages.BalloonTipTitle = "Zed Axis";
            this.notifyIconRecievedMessages.Text = "notifyIconRecievedMessages";
            this.notifyIconRecievedMessages.Visible = true;
            // 
            // backgroundWorkerDelet
            // 
            this.backgroundWorkerDelet.WorkerReportsProgress = true;
            this.backgroundWorkerDelet.WorkerSupportsCancellation = true;
            this.backgroundWorkerDelet.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerDeletLoader_DoWork);
            this.backgroundWorkerDelet.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerDeletLoader_ProgressChanged);
            this.backgroundWorkerDelet.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerDeletLoader_RunWorkerCompleted);
            // 
            // backgroundWorkerAutomaticSendReceive
            // 
            this.backgroundWorkerAutomaticSendReceive.WorkerReportsProgress = true;
            this.backgroundWorkerAutomaticSendReceive.WorkerSupportsCancellation = true;
            this.backgroundWorkerAutomaticSendReceive.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerAutomaticSendReceive_DoWork);
            this.backgroundWorkerAutomaticSendReceive.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerAutomaticSendReceive_RunWorkerCompleted);
            // 
            // contextMenuStripDeleteRows
            // 
            this.contextMenuStripDeleteRows.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRowsToolStripMenuItem});
            this.contextMenuStripDeleteRows.Name = "contextMenuStripDeleteRows";
            this.contextMenuStripDeleteRows.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStripDeleteRows.Size = new System.Drawing.Size(139, 26);
            this.contextMenuStripDeleteRows.Text = "Delete Rows";
            // 
            // deleteRowsToolStripMenuItem
            // 
            this.deleteRowsToolStripMenuItem.Name = "deleteRowsToolStripMenuItem";
            this.deleteRowsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.deleteRowsToolStripMenuItem.Text = "Delete Rows";
            // 
            // backgroundWorkerProcessLoader
            // 
            this.backgroundWorkerProcessLoader.WorkerReportsProgress = true;
            this.backgroundWorkerProcessLoader.WorkerSupportsCancellation = true;
            this.backgroundWorkerProcessLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerProcessLoader_DoWork);
            this.backgroundWorkerProcessLoader.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerProcessLoader_ProgressChanged);
            this.backgroundWorkerProcessLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerProcessLoader_RunWorkerCompleted);
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup4.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "Select a file";
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Orientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // radRibbonBarGroup2
            // 
            this.radRibbonBarGroup2.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup2.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup2.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup2.Name = "radRibbonBarGroup2";
            this.radRibbonBarGroup2.Text = "Statement Import options";
            // 
            // radDropDownListElement1
            // 
            this.radDropDownListElement1.ArrowButtonMinWidth = 17;
            this.radDropDownListElement1.AutoCompleteAppend = null;
            this.radDropDownListElement1.AutoCompleteDataSource = null;
            this.radDropDownListElement1.AutoCompleteSuggest = null;
            this.radDropDownListElement1.DataMember = "";
            this.radDropDownListElement1.DataSource = null;
            this.radDropDownListElement1.DefaultValue = null;
            this.radDropDownListElement1.DisplayMember = "";
            this.radDropDownListElement1.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radDropDownListElement1.DropDownAnimationEnabled = true;
            this.radDropDownListElement1.EditableElementText = "";
            this.radDropDownListElement1.EditorElement = this.radDropDownListElement1;
            this.radDropDownListElement1.EditorManager = null;
            this.radDropDownListElement1.Filter = null;
            this.radDropDownListElement1.FilterExpression = "";
            this.radDropDownListElement1.Focusable = true;
            this.radDropDownListElement1.FormatString = "";
            this.radDropDownListElement1.FormattingEnabled = true;
            this.radDropDownListElement1.ItemHeight = 18;
            this.radDropDownListElement1.MaxDropDownItems = 0;
            this.radDropDownListElement1.MaxLength = 32767;
            this.radDropDownListElement1.MaxValue = null;
            this.radDropDownListElement1.MinSize = new System.Drawing.Size(140, 0);
            this.radDropDownListElement1.MinValue = null;
            this.radDropDownListElement1.Name = "radDropDownListElement1";
            this.radDropDownListElement1.NullValue = null;
            this.radDropDownListElement1.OwnerOffset = 0;
            this.radDropDownListElement1.ShowImageInEditorArea = true;
            this.radDropDownListElement1.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radDropDownListElement1.StretchVertically = false;
            this.radDropDownListElement1.Value = null;
            this.radDropDownListElement1.ValueMember = "";
            // 
            // radRibbonBarButtonGroup4
            // 
            this.radRibbonBarButtonGroup4.Name = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.Text = "radRibbonBarButtonGroup4";
            // 
            // radRibbonBarButtonGroup8
            // 
            this.radRibbonBarButtonGroup8.Name = "radRibbonBarButtonGroup8";
            this.radRibbonBarButtonGroup8.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup8.Text = "radRibbonBarButtonGroup8";
            // 
            // radButtonElement1
            // 
            this.radButtonElement1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(148)))), ((int)(((byte)(234)))));
            this.radButtonElement1.Image = ((System.Drawing.Image)(resources.GetObject("radButtonElement1.Image")));
            this.radButtonElement1.Name = "radButtonElement1";
            this.radButtonElement1.Padding = new System.Windows.Forms.Padding(2);
            this.radButtonElement1.Text = "Browse";
            this.radButtonElement1.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radDropDownButtonElement1
            // 
            this.radDropDownButtonElement1.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement1.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement1.ExpandArrowButton = false;
            this.radDropDownButtonElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3});
            this.radDropDownButtonElement1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.radDropDownButtonElement1.Name = "radDropDownButtonElement1";
            this.radDropDownButtonElement1.Padding = new System.Windows.Forms.Padding(0);
            this.radDropDownButtonElement1.Text = "Text file option";
            this.radDropDownButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // radRibbonBarButtonGroup6
            // 
            this.radRibbonBarButtonGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement4,
            this.radCheckBoxElement5});
            this.radRibbonBarButtonGroup6.Name = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup6.Text = "radRibbonBarButtonGroup3";
            // 
            // radDropDownButtonElement4
            // 
            this.radDropDownButtonElement4.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement4.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement4.ExpandArrowButton = false;
            this.radDropDownButtonElement4.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.radDropDownButtonElement4.Name = "radDropDownButtonElement4";
            this.radDropDownButtonElement4.Padding = new System.Windows.Forms.Padding(0);
            this.radDropDownButtonElement4.Text = "Excel Options";
            this.radDropDownButtonElement4.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // radCheckBoxElement5
            // 
            this.radCheckBoxElement5.Checked = false;
            this.radCheckBoxElement5.Name = "radCheckBoxElement5";
            this.radCheckBoxElement5.ReadOnly = false;
            this.radCheckBoxElement5.Text = "Has Header";
            // 
            // radRibbonBarButtonGroup10
            // 
            this.radRibbonBarButtonGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement1,
            this.radRibbonBarButtonGroup6});
            this.radRibbonBarButtonGroup10.Name = "radRibbonBarButtonGroup10";
            this.radRibbonBarButtonGroup10.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup10.Text = "radRibbonBarButtonGroup5";
            // 
            // radButtonElement2
            // 
            this.radButtonElement2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(148)))), ((int)(((byte)(234)))));
            this.radButtonElement2.Image = ((System.Drawing.Image)(resources.GetObject("radButtonElement2.Image")));
            this.radButtonElement2.Name = "radButtonElement2";
            this.radButtonElement2.Padding = new System.Windows.Forms.Padding(2);
            this.radButtonElement2.Text = "Browse";
            this.radButtonElement2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radDropDownButtonElement5
            // 
            this.radDropDownButtonElement5.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement5.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement5.ExpandArrowButton = false;
            this.radDropDownButtonElement5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3});
            this.radDropDownButtonElement5.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.radDropDownButtonElement5.Name = "radDropDownButtonElement5";
            this.radDropDownButtonElement5.Padding = new System.Windows.Forms.Padding(0);
            this.radDropDownButtonElement5.Text = "Text file option";
            this.radDropDownButtonElement5.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // radRibbonBarButtonGroup11
            // 
            this.radRibbonBarButtonGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement6,
            this.radCheckBoxElement6});
            this.radRibbonBarButtonGroup11.Name = "radRibbonBarButtonGroup11";
            this.radRibbonBarButtonGroup11.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup11.Text = "radRibbonBarButtonGroup3";
            // 
            // radDropDownButtonElement6
            // 
            this.radDropDownButtonElement6.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement6.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement6.ExpandArrowButton = false;
            this.radDropDownButtonElement6.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.radDropDownButtonElement6.Name = "radDropDownButtonElement6";
            this.radDropDownButtonElement6.Padding = new System.Windows.Forms.Padding(0);
            this.radDropDownButtonElement6.Text = "Excel Options";
            this.radDropDownButtonElement6.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // radCheckBoxElement6
            // 
            this.radCheckBoxElement6.Checked = false;
            this.radCheckBoxElement6.Name = "radCheckBoxElement6";
            this.radCheckBoxElement6.ReadOnly = false;
            this.radCheckBoxElement6.Text = "Has Header";
            // 
            // radRibbonBarButtonGroup12
            // 
            this.radRibbonBarButtonGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement5,
            this.radRibbonBarButtonGroup11});
            this.radRibbonBarButtonGroup12.Name = "radRibbonBarButtonGroup12";
            this.radRibbonBarButtonGroup12.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup12.Text = "radRibbonBarButtonGroup5";
            // 
            // radButtonElement3
            // 
            this.radButtonElement3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.radButtonElement3.Image = ((System.Drawing.Image)(resources.GetObject("radButtonElement3.Image")));
            this.radButtonElement3.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.radButtonElement3.Name = "radButtonElement3";
            this.radButtonElement3.Text = "Edit";
            // 
            // radLabelElement3
            // 
            this.radLabelElement3.Name = "radLabelElement3";
            this.radLabelElement3.Text = "Select Transaction Type";
            this.radLabelElement3.TextWrap = true;
            // 
            // radRibbonBarButtonGroup5
            // 
            this.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup5";
            // 
            // radCheckBoxElement7
            // 
            this.radCheckBoxElement7.Checked = false;
            this.radCheckBoxElement7.Name = "radCheckBoxElement7";
            this.radCheckBoxElement7.ReadOnly = false;
            this.radCheckBoxElement7.Text = "radCheckBoxElement7";
            // 
            // radRibbonBarButtonGroup14
            // 
            this.radRibbonBarButtonGroup14.Name = "radRibbonBarButtonGroup14";
            this.radRibbonBarButtonGroup14.Text = "radRibbonBarButtonGroup14";
            // 
            // radCheckBoxElement1
            // 
            this.radCheckBoxElement1.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radCheckBoxElement1.Checked = false;
            this.radCheckBoxElement1.Name = "radCheckBoxElement1";
            this.radCheckBoxElement1.ReadOnly = false;
            this.radCheckBoxElement1.Text = "Report";
            this.radCheckBoxElement1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radDropDownListElement2
            // 
            this.radDropDownListElement2.ArrowButtonMinWidth = 17;
            this.radDropDownListElement2.AutoCompleteAppend = null;
            this.radDropDownListElement2.AutoCompleteDataSource = null;
            this.radDropDownListElement2.AutoCompleteSuggest = null;
            this.radDropDownListElement2.DataMember = "";
            this.radDropDownListElement2.DataSource = null;
            this.radDropDownListElement2.DefaultValue = null;
            this.radDropDownListElement2.DisplayMember = "";
            this.radDropDownListElement2.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radDropDownListElement2.DropDownAnimationEnabled = true;
            this.radDropDownListElement2.EditableElementText = "";
            this.radDropDownListElement2.EditorElement = this.radDropDownListElement2;
            this.radDropDownListElement2.EditorManager = null;
            this.radDropDownListElement2.Filter = null;
            this.radDropDownListElement2.FilterExpression = "";
            this.radDropDownListElement2.Focusable = true;
            this.radDropDownListElement2.FormatString = "";
            this.radDropDownListElement2.FormattingEnabled = true;
            this.radDropDownListElement2.ItemHeight = 18;
            this.radDropDownListElement2.MaxDropDownItems = 0;
            this.radDropDownListElement2.MaxLength = 32767;
            this.radDropDownListElement2.MaxValue = null;
            this.radDropDownListElement2.MinValue = null;
            this.radDropDownListElement2.Name = "radDropDownListElement2";
            this.radDropDownListElement2.NullValue = null;
            this.radDropDownListElement2.OwnerOffset = 0;
            this.radDropDownListElement2.Padding = new System.Windows.Forms.Padding(2);
            this.radDropDownListElement2.ShowImageInEditorArea = true;
            this.radDropDownListElement2.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radDropDownListElement2.Value = null;
            this.radDropDownListElement2.ValueMember = "";
            // 
            // radDropDownListElement3
            // 
            this.radDropDownListElement3.ArrowButtonMinWidth = 17;
            this.radDropDownListElement3.AutoCompleteAppend = null;
            this.radDropDownListElement3.AutoCompleteDataSource = null;
            this.radDropDownListElement3.AutoCompleteSuggest = null;
            this.radDropDownListElement3.DataMember = "";
            this.radDropDownListElement3.DataSource = null;
            this.radDropDownListElement3.DefaultValue = null;
            this.radDropDownListElement3.DisplayMember = "";
            this.radDropDownListElement3.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radDropDownListElement3.DropDownAnimationEnabled = true;
            this.radDropDownListElement3.EditableElementText = "";
            this.radDropDownListElement3.EditorElement = this.radDropDownListElement3;
            this.radDropDownListElement3.EditorManager = null;
            this.radDropDownListElement3.Filter = null;
            this.radDropDownListElement3.FilterExpression = "";
            this.radDropDownListElement3.Focusable = true;
            this.radDropDownListElement3.FormatString = "";
            this.radDropDownListElement3.FormattingEnabled = true;
            this.radDropDownListElement3.ItemHeight = 18;
            this.radDropDownListElement3.MaxDropDownItems = 0;
            this.radDropDownListElement3.MaxLength = 32767;
            this.radDropDownListElement3.MaxValue = null;
            this.radDropDownListElement3.MinValue = null;
            this.radDropDownListElement3.Name = "radDropDownListElement3";
            this.radDropDownListElement3.NullValue = null;
            this.radDropDownListElement3.OwnerOffset = 0;
            this.radDropDownListElement3.ShowImageInEditorArea = true;
            this.radDropDownListElement3.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radDropDownListElement3.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radDropDownListElement3.Value = null;
            this.radDropDownListElement3.ValueMember = "";
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.Name = "radLabelElement1";
            this.radLabelElement1.Text = "radLabelElement1";
            this.radLabelElement1.TextWrap = true;
            // 
            // radLabelElement2
            // 
            this.radLabelElement2.Name = "radLabelElement2";
            this.radLabelElement2.Text = "radLabelElement1";
            this.radLabelElement2.TextWrap = true;
            // 
            // radDesktopAlert1
            // 
            this.radDesktopAlert1.AutoSize = true;
            this.radDesktopAlert1.ButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radBtnOk});
            this.radDesktopAlert1.FadeAnimationFrames = 50;
            // 
            // radBtnOk
            // 
            this.radBtnOk.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radBtnOk.Name = "radBtnOk";
            this.radBtnOk.Text = "OK";
            // 
            // radTextBoxElement1
            // 
            this.radTextBoxElement1.Name = "radTextBoxElement1";
            this.radTextBoxElement1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.radTextBoxElement1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.radTextBoxElement1.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            // 
            // radRibbonBarButtonGroup20
            // 
            this.radRibbonBarButtonGroup20.Name = "radRibbonBarButtonGroup20";
            this.radRibbonBarButtonGroup20.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup20.Text = "radRibbonBarButtonGroup20";
            // 
            // radRibbonBarButtonGroup24
            // 
            this.radRibbonBarButtonGroup24.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRadioButtonElement1,
            this.radRadioButtonElement2,
            this.radRadioButtonElement3,
            this.radRadioButtonElement4});
            this.radRibbonBarButtonGroup24.Name = "radRibbonBarButtonGroup24";
            this.radRibbonBarButtonGroup24.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup24.Text = "radRibbonBarButtonGroup24";
            // 
            // radRadioButtonElement1
            // 
            this.radRadioButtonElement1.Name = "radRadioButtonElement1";
            this.radRadioButtonElement1.ReadOnly = false;
            this.radRadioButtonElement1.Text = "radRadioButtonElement1";
            // 
            // radRadioButtonElement2
            // 
            this.radRadioButtonElement2.Name = "radRadioButtonElement2";
            this.radRadioButtonElement2.ReadOnly = false;
            this.radRadioButtonElement2.Text = "radRadioButtonElement2";
            // 
            // radRadioButtonElement3
            // 
            this.radRadioButtonElement3.Name = "radRadioButtonElement3";
            this.radRadioButtonElement3.ReadOnly = false;
            this.radRadioButtonElement3.Text = "radRadioButtonElement3";
            // 
            // radRadioButtonElement4
            // 
            this.radRadioButtonElement4.Name = "radRadioButtonElement4";
            this.radRadioButtonElement4.ReadOnly = false;
            this.radRadioButtonElement4.Text = "radRadioButtonElement4";
            // 
            // radRibbonBarGroup26
            // 
            this.radRibbonBarGroup26.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup24});
            this.radRibbonBarGroup26.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup26.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup26.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup26.Name = "radRibbonBarGroup26";
            this.radRibbonBarGroup26.Text = "If transaction exists";
            // 
            // backgroundWorkerExportData
            // 
            this.backgroundWorkerExportData.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerExportData_DoWork);
            this.backgroundWorkerExportData.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerExportData_ProgressChanged);
            this.backgroundWorkerExportData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerExportData_RunWorkerCompleted);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabImport);
            this.tabControl.Controls.Add(this.tabExport);
            this.tabControl.Controls.Add(this.tabEDI);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl.Location = new System.Drawing.Point(0, 200);
            this.tabControl.Name = "tabControl";
            this.tabControl.Padding = new System.Drawing.Point(0, 0);
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1352, 493);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 2;
            // 
            // tabImport
            // 
            this.tabImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tabImport.Controls.Add(this.panelImportModule);
            this.tabImport.Location = new System.Drawing.Point(4, 5);
            this.tabImport.Margin = new System.Windows.Forms.Padding(0);
            this.tabImport.Name = "tabImport";
            this.tabImport.Size = new System.Drawing.Size(1344, 484);
            this.tabImport.TabIndex = 0;
            // 
            // panelImportModule
            // 
            this.panelImportModule.Controls.Add(this.GroupBoxImportGrid);
            this.panelImportModule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImportModule.Location = new System.Drawing.Point(0, 0);
            this.panelImportModule.Name = "panelImportModule";
            this.panelImportModule.Size = new System.Drawing.Size(1344, 484);
            this.panelImportModule.TabIndex = 6;
            // 
            // GroupBoxImportGrid
            // 
            this.GroupBoxImportGrid.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBoxImportGrid.Controls.Add(this.radPanelTop);
            this.GroupBoxImportGrid.Controls.Add(this.radpanelbottom);
            this.GroupBoxImportGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBoxImportGrid.HeaderText = "";
            this.GroupBoxImportGrid.Location = new System.Drawing.Point(0, 0);
            this.GroupBoxImportGrid.Name = "GroupBoxImportGrid";
            this.GroupBoxImportGrid.Padding = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.GroupBoxImportGrid.Size = new System.Drawing.Size(1344, 484);
            this.GroupBoxImportGrid.TabIndex = 3;
            // 
            // radPanelTop
            // 
            this.radPanelTop.Controls.Add(this.dataGridViewDataPreview);
            this.radPanelTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelTop.Location = new System.Drawing.Point(2, 0);
            this.radPanelTop.Name = "radPanelTop";
            this.radPanelTop.Size = new System.Drawing.Size(1340, 432);
            this.radPanelTop.TabIndex = 1;
            // 
            // dataGridViewDataPreview
            // 
            this.dataGridViewDataPreview.AutoScroll = true;
            this.dataGridViewDataPreview.BackColor = System.Drawing.Color.LightSteelBlue;
            this.dataGridViewDataPreview.Controls.Add(this.radPanelSelectFile);
            this.dataGridViewDataPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewDataPreview.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.dataGridViewDataPreview.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewDataPreview.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridViewDataPreview.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridViewDataPreview.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dataGridViewDataPreview.MasterTemplate.EnableAlternatingRowColor = true;
            this.dataGridViewDataPreview.MasterTemplate.EnableFiltering = true;
            this.dataGridViewDataPreview.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.dataGridViewDataPreview.MasterTemplate.ShowFilteringRow = false;
            this.dataGridViewDataPreview.MasterTemplate.ShowHeaderCellButtons = true;
            this.dataGridViewDataPreview.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridViewDataPreview.MinimumSize = new System.Drawing.Size(900, 267);
            this.dataGridViewDataPreview.Name = "dataGridViewDataPreview";
            this.dataGridViewDataPreview.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.dataGridViewDataPreview.RootElement.MinSize = new System.Drawing.Size(900, 267);
            this.dataGridViewDataPreview.ShowHeaderCellButtons = true;
            this.dataGridViewDataPreview.Size = new System.Drawing.Size(1340, 432);
            this.dataGridViewDataPreview.TabIndex = 17;
            // 
            // radPanelSelectFile
            // 
            this.radPanelSelectFile.Controls.Add(this.radPopupContainerText);
            this.radPanelSelectFile.Controls.Add(this.radPopupEditorTextFile);
            this.radPanelSelectFile.Controls.Add(this.radPopupEditorCmnOptn);
            this.radPanelSelectFile.Controls.Add(this.radPopupContainerCmnOptn);
            this.radPanelSelectFile.Controls.Add(this.radPopupContainerIIF);
            this.radPanelSelectFile.Controls.Add(this.radPopupEditorIIF);
            this.radPanelSelectFile.Controls.Add(this.radPopupEditorExcel);
            this.radPanelSelectFile.Controls.Add(this.radPopupContainerexcel);
            this.radPanelSelectFile.Location = new System.Drawing.Point(0, 0);
            this.radPanelSelectFile.Name = "radPanelSelectFile";
            this.radPanelSelectFile.Size = new System.Drawing.Size(757, 147);
            this.radPanelSelectFile.TabIndex = 1;
            this.radPanelSelectFile.Visible = false;
            // 
            // radPopupContainerText
            // 
            this.radPopupContainerText.Location = new System.Drawing.Point(385, 30);
            this.radPopupContainerText.Name = "radPopupContainerText";
            // 
            // radPopupContainerText.PanelContainer
            // 
            this.radPopupContainerText.PanelContainer.Controls.Add(this.groupBoxFileDelimiter);
            this.radPopupContainerText.PanelContainer.Size = new System.Drawing.Size(172, 106);
            this.radPopupContainerText.Size = new System.Drawing.Size(174, 108);
            this.radPopupContainerText.TabIndex = 11;
            // 
            // groupBoxFileDelimiter
            // 
            this.groupBoxFileDelimiter.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBoxFileDelimiter.Controls.Add(this.radioButtonPipe);
            this.groupBoxFileDelimiter.Controls.Add(this.radioButtoncomma);
            this.groupBoxFileDelimiter.Controls.Add(this.radioButtonTab);
            this.groupBoxFileDelimiter.Controls.Add(this.radLabelElement11);
            this.groupBoxFileDelimiter.HeaderText = "";
            this.groupBoxFileDelimiter.Location = new System.Drawing.Point(0, 0);
            this.groupBoxFileDelimiter.Name = "groupBoxFileDelimiter";
            this.groupBoxFileDelimiter.Size = new System.Drawing.Size(172, 106);
            this.groupBoxFileDelimiter.TabIndex = 1;
            // 
            // radioButtonPipe
            // 
            this.radioButtonPipe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radioButtonPipe.Location = new System.Drawing.Point(10, 77);
            this.radioButtonPipe.Name = "radioButtonPipe";
            this.radioButtonPipe.Size = new System.Drawing.Size(42, 18);
            this.radioButtonPipe.TabIndex = 2;
            this.radioButtonPipe.TabStop = false;
            this.radioButtonPipe.Text = "Pipe";
            this.radioButtonPipe.CheckStateChanged += new System.EventHandler(this.radioButtoncomma_CheckedChanged);
            // 
            // radioButtoncomma
            // 
            this.radioButtoncomma.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radioButtoncomma.Location = new System.Drawing.Point(10, 53);
            this.radioButtoncomma.Name = "radioButtoncomma";
            this.radioButtoncomma.Size = new System.Drawing.Size(59, 18);
            this.radioButtoncomma.TabIndex = 2;
            this.radioButtoncomma.TabStop = false;
            this.radioButtoncomma.Text = "Comma";
            this.radioButtoncomma.CheckStateChanged += new System.EventHandler(this.radioButtoncomma_CheckedChanged);
            // 
            // radioButtonTab
            // 
            this.radioButtonTab.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radioButtonTab.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radioButtonTab.Location = new System.Drawing.Point(10, 29);
            this.radioButtonTab.Name = "radioButtonTab";
            this.radioButtonTab.Size = new System.Drawing.Size(39, 18);
            this.radioButtonTab.TabIndex = 1;
            this.radioButtonTab.Text = "Tab";
            this.radioButtonTab.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radioButtonTab.CheckStateChanged += new System.EventHandler(this.radioButtoncomma_CheckedChanged);
            // 
            // radLabelElement11
            // 
            this.radLabelElement11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radLabelElement11.Location = new System.Drawing.Point(8, 5);
            this.radLabelElement11.Name = "radLabelElement11";
            this.radLabelElement11.Size = new System.Drawing.Size(81, 18);
            this.radLabelElement11.TabIndex = 0;
            this.radLabelElement11.Text = "Text file option";
            // 
            // radPopupEditorTextFile
            // 
            this.radPopupEditorTextFile.AssociatedControl = this.radPopupContainerText;
            this.radPopupEditorTextFile.AutoSize = false;
            this.radPopupEditorTextFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.radPopupEditorTextFile.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.None;
            this.radPopupEditorTextFile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radPopupEditorTextFile.Location = new System.Drawing.Point(385, 3);
            this.radPopupEditorTextFile.Name = "radPopupEditorTextFile";
            this.radPopupEditorTextFile.ShowTextBox = false;
            this.radPopupEditorTextFile.Size = new System.Drawing.Size(104, 23);
            this.radPopupEditorTextFile.TabIndex = 10;
            this.radPopupEditorTextFile.Text = "Text file options";
            // 
            // radPopupEditorCmnOptn
            // 
            this.radPopupEditorCmnOptn.AssociatedControl = this.radPopupContainerCmnOptn;
            this.radPopupEditorCmnOptn.AutoSize = false;
            this.radPopupEditorCmnOptn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.radPopupEditorCmnOptn.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.None;
            this.radPopupEditorCmnOptn.EnableGestures = false;
            this.radPopupEditorCmnOptn.EnableTheming = false;
            this.radPopupEditorCmnOptn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radPopupEditorCmnOptn.Location = new System.Drawing.Point(575, 3);
            this.radPopupEditorCmnOptn.Name = "radPopupEditorCmnOptn";
            this.radPopupEditorCmnOptn.ShowItemToolTips = false;
            this.radPopupEditorCmnOptn.ShowTextBox = false;
            this.radPopupEditorCmnOptn.Size = new System.Drawing.Size(131, 23);
            this.radPopupEditorCmnOptn.TabIndex = 9;
            this.radPopupEditorCmnOptn.Text = "Common options";
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radPopupEditorCmnOptn.GetChildAt(0).GetChildAt(2).GetChildAt(1))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.Primitives.ArrowPrimitive)(this.radPopupEditorCmnOptn.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(2))).ShouldPaint = true;
            // 
            // radPopupContainerCmnOptn
            // 
            this.radPopupContainerCmnOptn.AllowAutomaticScrollToControl = false;
            this.radPopupContainerCmnOptn.EnableTheming = false;
            this.radPopupContainerCmnOptn.Location = new System.Drawing.Point(575, 30);
            this.radPopupContainerCmnOptn.Name = "radPopupContainerCmnOptn";
            // 
            // radPopupContainerCmnOptn.PanelContainer
            // 
            this.radPopupContainerCmnOptn.PanelContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.radPopupContainerCmnOptn.PanelContainer.Controls.Add(this.radGroupBox1);
            this.radPopupContainerCmnOptn.PanelContainer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radPopupContainerCmnOptn.PanelContainer.Size = new System.Drawing.Size(172, 59);
            this.radPopupContainerCmnOptn.ShowItemToolTips = false;
            this.radPopupContainerCmnOptn.Size = new System.Drawing.Size(174, 61);
            this.radPopupContainerCmnOptn.TabIndex = 8;
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerCmnOptn.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(1);
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerCmnOptn.GetChildAt(0))).StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerCmnOptn.GetChildAt(0))).StretchVertically = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radPopupContainerCmnOptn.GetChildAt(0).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.numericUpDownImport);
            this.radGroupBox1.Controls.Add(this.labelImportRow);
            this.radGroupBox1.Controls.Add(this.checkBoxHasHeaders);
            this.radGroupBox1.HeaderText = "";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(172, 59);
            this.radGroupBox1.TabIndex = 0;
            // 
            // numericUpDownImport
            // 
            this.numericUpDownImport.Location = new System.Drawing.Point(115, 34);
            this.numericUpDownImport.Name = "numericUpDownImport";
            this.numericUpDownImport.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownImport.TabIndex = 6;
            this.numericUpDownImport.ValueChanged += new System.EventHandler(this.numericUpDownImport_ValueChanged);
            // 
            // labelImportRow
            // 
            this.labelImportRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.labelImportRow.Location = new System.Drawing.Point(5, 37);
            this.labelImportRow.Name = "labelImportRow";
            this.labelImportRow.Size = new System.Drawing.Size(103, 18);
            this.labelImportRow.TabIndex = 5;
            this.labelImportRow.Text = "Start import at row:";
            // 
            // checkBoxHasHeaders
            // 
            this.checkBoxHasHeaders.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.checkBoxHasHeaders.Location = new System.Drawing.Point(6, 9);
            this.checkBoxHasHeaders.Name = "checkBoxHasHeaders";
            this.checkBoxHasHeaders.Size = new System.Drawing.Size(81, 18);
            this.checkBoxHasHeaders.TabIndex = 4;
            this.checkBoxHasHeaders.Text = "Has headers";
            this.checkBoxHasHeaders.CheckStateChanged += new System.EventHandler(this.checkBoxHasHeaders_CheckedChanged);
            // 
            // radPopupContainerIIF
            // 
            this.radPopupContainerIIF.Location = new System.Drawing.Point(194, 30);
            this.radPopupContainerIIF.Name = "radPopupContainerIIF";
            // 
            // radPopupContainerIIF.PanelContainer
            // 
            this.radPopupContainerIIF.PanelContainer.Controls.Add(this.groupBoxTransType);
            this.radPopupContainerIIF.PanelContainer.Size = new System.Drawing.Size(172, 109);
            this.radPopupContainerIIF.Size = new System.Drawing.Size(174, 111);
            this.radPopupContainerIIF.TabIndex = 7;
            // 
            // groupBoxTransType
            // 
            this.groupBoxTransType.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBoxTransType.Controls.Add(this.buttonPreview);
            this.groupBoxTransType.Controls.Add(this.comboBoxXmlPreviewTable);
            this.groupBoxTransType.Controls.Add(this.comboBoxIIFTransType);
            this.groupBoxTransType.Controls.Add(this.radLabel1);
            this.groupBoxTransType.HeaderText = "";
            this.groupBoxTransType.Location = new System.Drawing.Point(0, -1);
            this.groupBoxTransType.Name = "groupBoxTransType";
            this.groupBoxTransType.Size = new System.Drawing.Size(172, 110);
            this.groupBoxTransType.TabIndex = 1;
            // 
            // buttonPreview
            // 
            this.buttonPreview.Location = new System.Drawing.Point(7, 80);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(121, 24);
            this.buttonPreview.TabIndex = 3;
            this.buttonPreview.Text = "Preview";
            // 
            // comboBoxXmlPreviewTable
            // 
            this.comboBoxXmlPreviewTable.Location = new System.Drawing.Point(8, 53);
            this.comboBoxXmlPreviewTable.Name = "comboBoxXmlPreviewTable";
            this.comboBoxXmlPreviewTable.Size = new System.Drawing.Size(155, 20);
            this.comboBoxXmlPreviewTable.TabIndex = 2;
            this.comboBoxXmlPreviewTable.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxXmlPreviewTable_SelectedIndexChanged);
            // 
            // comboBoxIIFTransType
            // 
            this.comboBoxIIFTransType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.comboBoxIIFTransType.Location = new System.Drawing.Point(7, 27);
            this.comboBoxIIFTransType.Name = "comboBoxIIFTransType";
            this.comboBoxIIFTransType.Size = new System.Drawing.Size(155, 20);
            this.comboBoxIIFTransType.TabIndex = 1;
            this.comboBoxIIFTransType.Text = "Select";
            this.comboBoxIIFTransType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxIIFTransType_SelectedIndexChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radLabel1.Location = new System.Drawing.Point(3, 2);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(137, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Select IIF transaction Type";
            // 
            // radPopupEditorIIF
            // 
            this.radPopupEditorIIF.AssociatedControl = this.radPopupContainerIIF;
            this.radPopupEditorIIF.AutoSize = false;
            this.radPopupEditorIIF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.radPopupEditorIIF.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.None;
            this.radPopupEditorIIF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radPopupEditorIIF.Location = new System.Drawing.Point(194, 3);
            this.radPopupEditorIIF.Name = "radPopupEditorIIF";
            this.radPopupEditorIIF.ShowTextBox = false;
            this.radPopupEditorIIF.Size = new System.Drawing.Size(132, 23);
            this.radPopupEditorIIF.TabIndex = 6;
            this.radPopupEditorIIF.Text = "Select IIF transaction";
            // 
            // radPopupEditorExcel
            // 
            this.radPopupEditorExcel.AssociatedControl = this.radPopupContainerexcel;
            this.radPopupEditorExcel.AutoSize = false;
            this.radPopupEditorExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.radPopupEditorExcel.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.None;
            this.radPopupEditorExcel.EnableGestures = false;
            this.radPopupEditorExcel.EnableTheming = false;
            this.radPopupEditorExcel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radPopupEditorExcel.Location = new System.Drawing.Point(3, 3);
            this.radPopupEditorExcel.Name = "radPopupEditorExcel";
            this.radPopupEditorExcel.ShowItemToolTips = false;
            this.radPopupEditorExcel.ShowTextBox = false;
            this.radPopupEditorExcel.Size = new System.Drawing.Size(131, 23);
            this.radPopupEditorExcel.TabIndex = 3;
            this.radPopupEditorExcel.Text = "Excel options";
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radPopupEditorExcel.GetChildAt(0).GetChildAt(2).GetChildAt(1))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.Primitives.ArrowPrimitive)(this.radPopupEditorExcel.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(2))).ShouldPaint = true;
            // 
            // radPopupContainerexcel
            // 
            this.radPopupContainerexcel.AllowAutomaticScrollToControl = false;
            this.radPopupContainerexcel.EnableTheming = false;
            this.radPopupContainerexcel.Location = new System.Drawing.Point(3, 30);
            this.radPopupContainerexcel.Name = "radPopupContainerexcel";
            // 
            // radPopupContainerexcel.PanelContainer
            // 
            this.radPopupContainerexcel.PanelContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.radPopupContainerexcel.PanelContainer.Controls.Add(this.groupBoxExcelSheet);
            this.radPopupContainerexcel.PanelContainer.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radPopupContainerexcel.PanelContainer.Size = new System.Drawing.Size(172, 48);
            this.radPopupContainerexcel.ShowItemToolTips = false;
            this.radPopupContainerexcel.Size = new System.Drawing.Size(174, 50);
            this.radPopupContainerexcel.TabIndex = 2;
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerexcel.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(1);
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerexcel.GetChildAt(0))).StretchHorizontally = true;
            ((Telerik.WinControls.UI.RadScrollablePanelElement)(this.radPopupContainerexcel.GetChildAt(0))).StretchVertically = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radPopupContainerexcel.GetChildAt(0).GetChildAt(0))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // groupBoxExcelSheet
            // 
            this.groupBoxExcelSheet.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.groupBoxExcelSheet.Controls.Add(this.radLabel2);
            this.groupBoxExcelSheet.Controls.Add(this.comboBoxWorkSheet);
            this.groupBoxExcelSheet.HeaderText = "";
            this.groupBoxExcelSheet.Location = new System.Drawing.Point(0, 0);
            this.groupBoxExcelSheet.Name = "groupBoxExcelSheet";
            this.groupBoxExcelSheet.Size = new System.Drawing.Size(172, 48);
            this.groupBoxExcelSheet.TabIndex = 0;
            // 
            // radLabel2
            // 
            this.radLabel2.AutoSize = false;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radLabel2.Location = new System.Drawing.Point(5, 4);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(129, 18);
            this.radLabel2.TabIndex = 4;
            this.radLabel2.Text = "Select excel sheet";
            // 
            // comboBoxWorkSheet
            // 
            this.comboBoxWorkSheet.Location = new System.Drawing.Point(5, 24);
            this.comboBoxWorkSheet.Name = "comboBoxWorkSheet";
            this.comboBoxWorkSheet.Size = new System.Drawing.Size(160, 20);
            this.comboBoxWorkSheet.TabIndex = 3;
            this.comboBoxWorkSheet.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxWorkSheet_SelectedIndexChanged);
            // 
            // radpanelbottom
            // 
            this.radpanelbottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.radpanelbottom.Controls.Add(this.panelImport);
            this.radpanelbottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radpanelbottom.Location = new System.Drawing.Point(2, 432);
            this.radpanelbottom.Name = "radpanelbottom";
            this.radpanelbottom.Size = new System.Drawing.Size(1340, 50);
            this.radpanelbottom.TabIndex = 0;
            // 
            // panelImport
            // 
            this.panelImport.Controls.Add(this.buttonSaveData);
            this.panelImport.Controls.Add(this.buttonSearch);
            this.panelImport.Controls.Add(this.butttonOpenlogs);
            this.panelImport.Controls.Add(this.buttonRefresh);
            this.panelImport.Controls.Add(this.buttonImport);
            this.panelImport.Controls.Add(this.buttonDelete);
            this.panelImport.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelImport.Location = new System.Drawing.Point(-2, 0);
            this.panelImport.Name = "panelImport";
            this.panelImport.Size = new System.Drawing.Size(1342, 50);
            this.panelImport.TabIndex = 37;
            // 
            // buttonSaveData
            // 
            this.buttonSaveData.Location = new System.Drawing.Point(596, 13);
            this.buttonSaveData.Name = "buttonSaveData";
            this.buttonSaveData.Size = new System.Drawing.Size(110, 24);
            this.buttonSaveData.TabIndex = 24;
            this.buttonSaveData.Text = "Save Data";
            this.buttonSaveData.Click += new System.EventHandler(this.buttonSaveData_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(712, 13);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(110, 24);
            this.buttonSearch.TabIndex = 23;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.Click += new System.EventHandler(this.ButtonSearch_Click);
            // 
            // butttonOpenlogs
            // 
            this.butttonOpenlogs.Location = new System.Drawing.Point(1064, 13);
            this.butttonOpenlogs.Name = "butttonOpenlogs";
            this.butttonOpenlogs.Size = new System.Drawing.Size(110, 24);
            this.butttonOpenlogs.TabIndex = 22;
            this.butttonOpenlogs.Text = "Open Logs";
            this.butttonOpenlogs.Click += new System.EventHandler(this.buttonOpenlogs_Click);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(828, 13);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(110, 24);
            this.buttonRefresh.TabIndex = 21;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_click);
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(1216, 13);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(110, 24);
            this.buttonImport.TabIndex = 22;
            this.buttonImport.Text = "Import";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(944, 13);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(110, 24);
            this.buttonDelete.TabIndex = 22;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // tabExport
            // 
            this.tabExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tabExport.Controls.Add(this.panelExportModule);
            this.tabExport.Location = new System.Drawing.Point(4, 5);
            this.tabExport.Margin = new System.Windows.Forms.Padding(0);
            this.tabExport.Name = "tabExport";
            this.tabExport.Size = new System.Drawing.Size(1344, 484);
            this.tabExport.TabIndex = 1;
            // 
            // panelExportModule
            // 
            this.panelExportModule.Controls.Add(this.GroupBoxDataGridViewResult);
            this.panelExportModule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelExportModule.Location = new System.Drawing.Point(0, 0);
            this.panelExportModule.Margin = new System.Windows.Forms.Padding(0);
            this.panelExportModule.Name = "panelExportModule";
            this.panelExportModule.Size = new System.Drawing.Size(1344, 484);
            this.panelExportModule.TabIndex = 39;
            // 
            // GroupBoxDataGridViewResult
            // 
            this.GroupBoxDataGridViewResult.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBoxDataGridViewResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.GroupBoxDataGridViewResult.Controls.Add(this.radPanel1);
            this.GroupBoxDataGridViewResult.Controls.Add(this.groupBoxExportFormat);
            this.GroupBoxDataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBoxDataGridViewResult.HeaderText = "";
            this.GroupBoxDataGridViewResult.Location = new System.Drawing.Point(0, 0);
            this.GroupBoxDataGridViewResult.Name = "GroupBoxDataGridViewResult";
            this.GroupBoxDataGridViewResult.Padding = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.GroupBoxDataGridViewResult.Size = new System.Drawing.Size(1344, 484);
            this.GroupBoxDataGridViewResult.TabIndex = 3;
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radPanelexporttop);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Location = new System.Drawing.Point(2, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1340, 423);
            this.radPanel1.TabIndex = 39;
            // 
            // radPanelexporttop
            // 
            this.radPanelexporttop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.radPanelexporttop.Controls.Add(this.dataGridViewResult);
            this.radPanelexporttop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanelexporttop.Location = new System.Drawing.Point(0, 0);
            this.radPanelexporttop.Name = "radPanelexporttop";
            this.radPanelexporttop.Size = new System.Drawing.Size(1340, 423);
            this.radPanelexporttop.TabIndex = 38;
            // 
            // dataGridViewResult
            // 
            this.dataGridViewResult.AutoScroll = true;
            this.dataGridViewResult.BackColor = System.Drawing.Color.LightSteelBlue;
            this.dataGridViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewResult.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.dataGridViewResult.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewResult.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridViewResult.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.dataGridViewResult.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dataGridViewResult.MasterTemplate.EnableAlternatingRowColor = true;
            this.dataGridViewResult.MasterTemplate.EnableFiltering = true;
            this.dataGridViewResult.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.dataGridViewResult.MasterTemplate.ShowFilteringRow = false;
            this.dataGridViewResult.MasterTemplate.ShowHeaderCellButtons = true;
            this.dataGridViewResult.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.dataGridViewResult.MinimumSize = new System.Drawing.Size(900, 267);
            this.dataGridViewResult.Name = "dataGridViewResult";
            this.dataGridViewResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.dataGridViewResult.RootElement.MinSize = new System.Drawing.Size(900, 267);
            this.dataGridViewResult.ShowHeaderCellButtons = true;
            this.dataGridViewResult.Size = new System.Drawing.Size(1340, 423);
            this.dataGridViewResult.TabIndex = 19;
            // 
            // groupBoxExportFormat
            // 
            this.groupBoxExportFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.groupBoxExportFormat.Controls.Add(this.panelbtnbtm);
            this.groupBoxExportFormat.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBoxExportFormat.Location = new System.Drawing.Point(2, 423);
            this.groupBoxExportFormat.Name = "groupBoxExportFormat";
            this.groupBoxExportFormat.Size = new System.Drawing.Size(1340, 59);
            this.groupBoxExportFormat.TabIndex = 37;
            this.groupBoxExportFormat.TabStop = false;
            this.groupBoxExportFormat.Visible = false;
            // 
            // panelbtnbtm
            // 
            this.panelbtnbtm.Controls.Add(this.panel1);
            this.panelbtnbtm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelbtnbtm.Location = new System.Drawing.Point(3, 3);
            this.panelbtnbtm.Name = "panelbtnbtm";
            this.panelbtnbtm.Size = new System.Drawing.Size(1334, 53);
            this.panelbtnbtm.TabIndex = 18;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSearchExp);
            this.panel1.Controls.Add(this.checkBoxDeleteFromQB);
            this.panel1.Controls.Add(this.buttonOK);
            this.panel1.Controls.Add(this.buttonCancelExport);
            this.panel1.Controls.Add(this.checkBoxSend);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBoxExportFormat);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1334, 53);
            this.panel1.TabIndex = 18;
            // 
            // buttonSearchExp
            // 
            this.buttonSearchExp.Location = new System.Drawing.Point(970, 16);
            this.buttonSearchExp.Name = "buttonSearchExp";
            this.buttonSearchExp.Size = new System.Drawing.Size(110, 24);
            this.buttonSearchExp.TabIndex = 18;
            this.buttonSearchExp.Text = "Search";
            this.buttonSearchExp.Click += new System.EventHandler(this.ButtonSearchExp_Click);
            // 
            // checkBoxDeleteFromQB
            // 
            this.checkBoxDeleteFromQB.AutoSize = true;
            this.checkBoxDeleteFromQB.Location = new System.Drawing.Point(326, 22);
            this.checkBoxDeleteFromQB.Name = "checkBoxDeleteFromQB";
            this.checkBoxDeleteFromQB.Size = new System.Drawing.Size(150, 17);
            this.checkBoxDeleteFromQB.TabIndex = 15;
            this.checkBoxDeleteFromQB.Text = "Delete from QuickBooks";
            this.checkBoxDeleteFromQB.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(1086, 16);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(110, 24);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancelExport
            // 
            this.buttonCancelExport.Location = new System.Drawing.Point(1211, 16);
            this.buttonCancelExport.Name = "buttonCancelExport";
            this.buttonCancelExport.Size = new System.Drawing.Size(110, 24);
            this.buttonCancelExport.TabIndex = 17;
            this.buttonCancelExport.Text = "Cancel";
            this.buttonCancelExport.Click += new System.EventHandler(this.buttonCancelExport_Click_1);
            // 
            // checkBoxSend
            // 
            this.checkBoxSend.AutoSize = true;
            this.checkBoxSend.Font = new System.Drawing.Font("Verdana", 8F);
            this.checkBoxSend.Location = new System.Drawing.Point(815, 21);
            this.checkBoxSend.Name = "checkBoxSend";
            this.checkBoxSend.Size = new System.Drawing.Size(108, 17);
            this.checkBoxSend.TabIndex = 14;
            this.checkBoxSend.Text = "Send by email";
            this.checkBoxSend.UseVisualStyleBackColor = true;
            this.checkBoxSend.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8F);
            this.label1.Location = new System.Drawing.Point(482, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Export format :";
            // 
            // comboBoxExportFormat
            // 
            this.comboBoxExportFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExportFormat.FormattingEnabled = true;
            this.comboBoxExportFormat.Items.AddRange(new object[] {
            "EDI Outbox"});
            this.comboBoxExportFormat.Location = new System.Drawing.Point(583, 20);
            this.comboBoxExportFormat.Name = "comboBoxExportFormat";
            this.comboBoxExportFormat.Size = new System.Drawing.Size(223, 21);
            this.comboBoxExportFormat.TabIndex = 4;
            this.comboBoxExportFormat.SelectedIndexChanged += new System.EventHandler(this.comboBoxExportFormat_SelectedIndexChanged);
            // 
            // tabEDI
            // 
            this.tabEDI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tabEDI.Controls.Add(this.PanelGridInfo);
            this.tabEDI.Location = new System.Drawing.Point(4, 5);
            this.tabEDI.Margin = new System.Windows.Forms.Padding(0);
            this.tabEDI.Name = "tabEDI";
            this.tabEDI.Size = new System.Drawing.Size(1344, 484);
            this.tabEDI.TabIndex = 2;
            // 
            // PanelGridInfo
            // 
            this.PanelGridInfo.Controls.Add(this.panelmailbxandgrid);
            this.PanelGridInfo.Controls.Add(this.radPanelEDI);
            this.PanelGridInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelGridInfo.Location = new System.Drawing.Point(0, 0);
            this.PanelGridInfo.Name = "PanelGridInfo";
            this.PanelGridInfo.Size = new System.Drawing.Size(1344, 484);
            this.PanelGridInfo.TabIndex = 4;
            // 
            // panelmailbxandgrid
            // 
            this.panelmailbxandgrid.Controls.Add(this.GroupBoxEDI);
            this.panelmailbxandgrid.Controls.Add(this.radTreeViewEDI);
            this.panelmailbxandgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelmailbxandgrid.Location = new System.Drawing.Point(0, 0);
            this.panelmailbxandgrid.Name = "panelmailbxandgrid";
            this.panelmailbxandgrid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panelmailbxandgrid.Size = new System.Drawing.Size(1344, 278);
            this.panelmailbxandgrid.TabIndex = 40;
            // 
            // GroupBoxEDI
            // 
            this.GroupBoxEDI.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GroupBoxEDI.Controls.Add(this.radGridViewEDI);
            this.GroupBoxEDI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupBoxEDI.HeaderText = "";
            this.GroupBoxEDI.Location = new System.Drawing.Point(110, 0);
            this.GroupBoxEDI.Name = "GroupBoxEDI";
            this.GroupBoxEDI.Padding = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.GroupBoxEDI.Size = new System.Drawing.Size(1234, 278);
            this.GroupBoxEDI.TabIndex = 3;
            // 
            // radGridViewEDI
            // 
            this.radGridViewEDI.AutoScroll = true;
            this.radGridViewEDI.BackColor = System.Drawing.Color.LightSteelBlue;
            this.radGridViewEDI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridViewEDI.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridViewEDI.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridViewEDI.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewEDI.Location = new System.Drawing.Point(2, 0);
            // 
            // 
            // 
            this.radGridViewEDI.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridViewEDI.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewEDI.MasterTemplate.EnableFiltering = true;
            this.radGridViewEDI.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.radGridViewEDI.MasterTemplate.ShowFilteringRow = false;
            this.radGridViewEDI.MasterTemplate.ShowHeaderCellButtons = true;
            this.radGridViewEDI.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridViewEDI.MinimumSize = new System.Drawing.Size(500, 100);
            this.radGridViewEDI.Name = "radGridViewEDI";
            this.radGridViewEDI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radGridViewEDI.RootElement.MinSize = new System.Drawing.Size(500, 100);
            this.radGridViewEDI.ShowHeaderCellButtons = true;
            this.radGridViewEDI.Size = new System.Drawing.Size(1230, 276);
            this.radGridViewEDI.TabIndex = 16;
            // 
            // radTreeViewEDI
            // 
            this.radTreeViewEDI.BackColor = System.Drawing.Color.AliceBlue;
            this.radTreeViewEDI.Controls.Add(this.LabelNodeSearch);
            this.radTreeViewEDI.Dock = System.Windows.Forms.DockStyle.Left;
            this.radTreeViewEDI.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTreeViewEDI.ForeColor = System.Drawing.Color.Black;
            this.radTreeViewEDI.Location = new System.Drawing.Point(0, 0);
            this.radTreeViewEDI.Name = "radTreeViewEDI";
            radTreeNode1.Expanded = true;
            radTreeNode2.Name = "radTreeInbox";
            radTreeNode2.Text = "Inbox";
            radTreeNode3.Name = "radTreeOutbox";
            radTreeNode3.Text = "Outbox";
            radTreeNode4.Name = "radTreeSentItems";
            radTreeNode4.Text = "Sent Items";
            radTreeNode1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode2,
            radTreeNode3,
            radTreeNode4});
            radTreeNode1.Text = "Mailbox";
            this.radTreeViewEDI.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1});
            this.radTreeViewEDI.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // 
            // 
            this.radTreeViewEDI.RootElement.Shape = null;
            this.radTreeViewEDI.ShowLines = true;
            this.radTreeViewEDI.Size = new System.Drawing.Size(110, 278);
            this.radTreeViewEDI.SpacingBetweenNodes = -1;
            this.radTreeViewEDI.TabIndex = 37;
            this.radTreeViewEDI.TreeIndent = 16;
            // 
            // LabelNodeSearch
            // 
            this.LabelNodeSearch.AutoSize = true;
            this.LabelNodeSearch.Location = new System.Drawing.Point(14, 336);
            this.LabelNodeSearch.Name = "LabelNodeSearch";
            this.LabelNodeSearch.Size = new System.Drawing.Size(0, 13);
            this.LabelNodeSearch.TabIndex = 0;
            this.LabelNodeSearch.Visible = false;
            // 
            // radPanelEDI
            // 
            this.radPanelEDI.AutoScroll = true;
            this.radPanelEDI.Controls.Add(this.radLabelSubjectOut);
            this.radPanelEDI.Controls.Add(this.radLabelFrom1);
            this.radPanelEDI.Controls.Add(this.radButtonSend);
            this.radPanelEDI.Controls.Add(this.radButtonCc);
            this.radPanelEDI.Controls.Add(this.radButtonTo);
            this.radPanelEDI.Controls.Add(this.radButtonSaveDraft);
            this.radPanelEDI.Controls.Add(this.radLabelSubject1);
            this.radPanelEDI.Controls.Add(this.radLabelCc1);
            this.radPanelEDI.Controls.Add(this.radLabelSubject);
            this.radPanelEDI.Controls.Add(this.radLabelCc);
            this.radPanelEDI.Controls.Add(this.radLabelFrom);
            this.radPanelEDI.Controls.Add(this.radPanelAttachment);
            this.radPanelEDI.Controls.Add(this.radTextBoxSubject);
            this.radPanelEDI.Controls.Add(this.radTextBoxCc);
            this.radPanelEDI.Controls.Add(this.radTextBoxTo);
            this.radPanelEDI.Controls.Add(this.radTextBoxMailData);
            this.radPanelEDI.Controls.Add(this.radLabelAttachment);
            this.radPanelEDI.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanelEDI.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelEDI.ForeColor = System.Drawing.Color.Black;
            this.radPanelEDI.Location = new System.Drawing.Point(0, 278);
            this.radPanelEDI.Name = "radPanelEDI";
            this.radPanelEDI.Size = new System.Drawing.Size(1344, 206);
            this.radPanelEDI.TabIndex = 39;
            // 
            // radLabelSubjectOut
            // 
            this.radLabelSubjectOut.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelSubjectOut.Location = new System.Drawing.Point(101, 67);
            this.radLabelSubjectOut.Name = "radLabelSubjectOut";
            this.radLabelSubjectOut.Size = new System.Drawing.Size(54, 17);
            this.radLabelSubjectOut.TabIndex = 0;
            this.radLabelSubjectOut.Text = "Subject:";
            this.radLabelSubjectOut.Visible = false;
            // 
            // radLabelFrom1
            // 
            this.radLabelFrom1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelFrom1.Location = new System.Drawing.Point(146, 11);
            this.radLabelFrom1.Name = "radLabelFrom1";
            this.radLabelFrom1.Size = new System.Drawing.Size(2, 2);
            this.radLabelFrom1.TabIndex = 5;
            // 
            // radButtonSend
            // 
            this.radButtonSend.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonSend.ForeColor = System.Drawing.Color.Black;
            this.radButtonSend.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonSend.Location = new System.Drawing.Point(6, 8);
            this.radButtonSend.Name = "radButtonSend";
            this.radButtonSend.Size = new System.Drawing.Size(51, 36);
            this.radButtonSend.TabIndex = 0;
            this.radButtonSend.Text = "Send";
            this.radButtonSend.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonSend.Visible = false;
            // 
            // radButtonCc
            // 
            this.radButtonCc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonCc.ForeColor = System.Drawing.Color.Black;
            this.radButtonCc.Location = new System.Drawing.Point(93, 40);
            this.radButtonCc.Name = "radButtonCc";
            this.radButtonCc.Size = new System.Drawing.Size(62, 18);
            this.radButtonCc.TabIndex = 0;
            this.radButtonCc.Text = "Cc...";
            // 
            // radButtonTo
            // 
            this.radButtonTo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonTo.ForeColor = System.Drawing.Color.Black;
            this.radButtonTo.Location = new System.Drawing.Point(93, 17);
            this.radButtonTo.Name = "radButtonTo";
            this.radButtonTo.Size = new System.Drawing.Size(62, 18);
            this.radButtonTo.TabIndex = 23;
            this.radButtonTo.Text = "To...";
            // 
            // radButtonSaveDraft
            // 
            this.radButtonSaveDraft.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radButtonSaveDraft.ForeColor = System.Drawing.Color.Black;
            this.radButtonSaveDraft.Location = new System.Drawing.Point(5, 74);
            this.radButtonSaveDraft.Name = "radButtonSaveDraft";
            this.radButtonSaveDraft.Size = new System.Drawing.Size(77, 24);
            this.radButtonSaveDraft.TabIndex = 22;
            this.radButtonSaveDraft.Text = "Save Draft";
            this.radButtonSaveDraft.Visible = false;
            // 
            // radLabelSubject1
            // 
            this.radLabelSubject1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelSubject1.Location = new System.Drawing.Point(146, 76);
            this.radLabelSubject1.Name = "radLabelSubject1";
            this.radLabelSubject1.Size = new System.Drawing.Size(2, 2);
            this.radLabelSubject1.TabIndex = 21;
            // 
            // radLabelCc1
            // 
            this.radLabelCc1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelCc1.Location = new System.Drawing.Point(146, 42);
            this.radLabelCc1.Name = "radLabelCc1";
            this.radLabelCc1.Size = new System.Drawing.Size(2, 2);
            this.radLabelCc1.TabIndex = 20;
            // 
            // radLabelSubject
            // 
            this.radLabelSubject.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelSubject.Location = new System.Drawing.Point(17, 76);
            this.radLabelSubject.Name = "radLabelSubject";
            this.radLabelSubject.Size = new System.Drawing.Size(53, 21);
            this.radLabelSubject.TabIndex = 18;
            this.radLabelSubject.Text = "Subject:";
            // 
            // radLabelCc
            // 
            this.radLabelCc.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelCc.Location = new System.Drawing.Point(17, 42);
            this.radLabelCc.Name = "radLabelCc";
            this.radLabelCc.Size = new System.Drawing.Size(24, 21);
            this.radLabelCc.TabIndex = 17;
            this.radLabelCc.Text = "Cc:";
            // 
            // radLabelFrom
            // 
            this.radLabelFrom.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelFrom.Location = new System.Drawing.Point(17, 11);
            this.radLabelFrom.Name = "radLabelFrom";
            this.radLabelFrom.Size = new System.Drawing.Size(40, 21);
            this.radLabelFrom.TabIndex = 16;
            this.radLabelFrom.Text = "From:";
            // 
            // radPanelAttachment
            // 
            this.radPanelAttachment.AutoScroll = true;
            this.radPanelAttachment.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radPanelAttachment.Location = new System.Drawing.Point(162, 89);
            this.radPanelAttachment.Name = "radPanelAttachment";
            this.radPanelAttachment.Size = new System.Drawing.Size(680, 47);
            this.radPanelAttachment.TabIndex = 15;
            // 
            // radTextBoxSubject
            // 
            this.radTextBoxSubject.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBoxSubject.Location = new System.Drawing.Point(162, 64);
            this.radTextBoxSubject.Name = "radTextBoxSubject";
            this.radTextBoxSubject.Size = new System.Drawing.Size(680, 19);
            this.radTextBoxSubject.TabIndex = 14;
            this.radTextBoxSubject.TabStop = false;
            // 
            // radTextBoxCc
            // 
            this.radTextBoxCc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBoxCc.Location = new System.Drawing.Point(162, 40);
            this.radTextBoxCc.Name = "radTextBoxCc";
            this.radTextBoxCc.Size = new System.Drawing.Size(680, 19);
            this.radTextBoxCc.TabIndex = 13;
            this.radTextBoxCc.TabStop = false;
            // 
            // radTextBoxTo
            // 
            this.radTextBoxTo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBoxTo.Location = new System.Drawing.Point(162, 16);
            this.radTextBoxTo.Name = "radTextBoxTo";
            this.radTextBoxTo.Size = new System.Drawing.Size(680, 19);
            this.radTextBoxTo.TabIndex = 12;
            this.radTextBoxTo.TabStop = false;
            // 
            // radTextBoxMailData
            // 
            this.radTextBoxMailData.AutoScroll = true;
            this.radTextBoxMailData.AutoSize = false;
            this.radTextBoxMailData.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBoxMailData.Location = new System.Drawing.Point(93, 142);
            this.radTextBoxMailData.Multiline = true;
            this.radTextBoxMailData.Name = "radTextBoxMailData";
            this.radTextBoxMailData.Size = new System.Drawing.Size(749, 54);
            this.radTextBoxMailData.TabIndex = 4;
            this.radTextBoxMailData.TabStop = false;
            // 
            // radLabelAttachment
            // 
            this.radLabelAttachment.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabelAttachment.Location = new System.Drawing.Point(79, 103);
            this.radLabelAttachment.Name = "radLabelAttachment";
            this.radLabelAttachment.Size = new System.Drawing.Size(77, 17);
            this.radLabelAttachment.TabIndex = 3;
            this.radLabelAttachment.Text = "Attachment:";
            // 
            // object_ee5a844a_8a5a_48af_958d_95850aee92f5
            // 
            this.object_ee5a844a_8a5a_48af_958d_95850aee92f5.Name = "object_ee5a844a_8a5a_48af_958d_95850aee92f5";
            this.object_ee5a844a_8a5a_48af_958d_95850aee92f5.StretchHorizontally = true;
            this.object_ee5a844a_8a5a_48af_958d_95850aee92f5.StretchVertically = true;
            // 
            // checkBoxUpdatednewnames
            // 
            this.checkBoxUpdatednewnames.AutoSize = false;
            this.checkBoxUpdatednewnames.Bounds = new System.Drawing.Rectangle(0, 0, 161, 26);
            this.checkBoxUpdatednewnames.Checked = false;
            this.checkBoxUpdatednewnames.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.checkBoxUpdatednewnames.Name = "checkBoxUpdatednewnames";
            this.checkBoxUpdatednewnames.Padding = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.checkBoxUpdatednewnames.ReadOnly = false;
            this.checkBoxUpdatednewnames.StretchVertically = false;
            this.checkBoxUpdatednewnames.Text = "Append updated or new names";
            // 
            // checkBoxAppendItems
            // 
            this.checkBoxAppendItems.AutoSize = false;
            this.checkBoxAppendItems.Bounds = new System.Drawing.Rectangle(0, 0, 160, 35);
            this.checkBoxAppendItems.Checked = false;
            this.checkBoxAppendItems.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.checkBoxAppendItems.Name = "checkBoxAppendItems";
            this.checkBoxAppendItems.Padding = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.checkBoxAppendItems.ReadOnly = false;
            this.checkBoxAppendItems.StretchVertically = false;
            this.checkBoxAppendItems.Text = "Append updated or new items";
            // 
            // TransactionImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 723);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.radRibbonBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TransactionImporter";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TransactionImporter_FormClosing);
            this.Load += new System.EventHandler(this.TransactionImporter_Load);
            this.Resize += new System.EventHandler(this.TransactionImporter_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.radChkDDListAccountTypeFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckedDropDownListEntityFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMappings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImportStatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxCodingTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDatatype)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxPaidStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxColChooser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMessageID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMailCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.contextMenuStripAddTo.ResumeLayout(false);
            this.contextMenuStripDeleteRows.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePreviewData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListElement3)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabImport.ResumeLayout(false);
            this.panelImportModule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxImportGrid)).EndInit();
            this.GroupBoxImportGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelTop)).EndInit();
            this.radPanelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview)).EndInit();
            this.dataGridViewDataPreview.ResumeLayout(false);
            this.dataGridViewDataPreview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelSelectFile)).EndInit();
            this.radPanelSelectFile.ResumeLayout(false);
            this.radPopupContainerText.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerText)).EndInit();
            this.radPopupContainerText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxFileDelimiter)).EndInit();
            this.groupBoxFileDelimiter.ResumeLayout(false);
            this.groupBoxFileDelimiter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonPipe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtoncomma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioButtonTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelElement11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorTextFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorCmnOptn)).EndInit();
            this.radPopupContainerCmnOptn.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerCmnOptn)).EndInit();
            this.radPopupContainerCmnOptn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImportRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxHasHeaders)).EndInit();
            this.radPopupContainerIIF.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerIIF)).EndInit();
            this.radPopupContainerIIF.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxTransType)).EndInit();
            this.groupBoxTransType.ResumeLayout(false);
            this.groupBoxTransType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxXmlPreviewTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxIIFTransType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorIIF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPopupEditorExcel)).EndInit();
            this.radPopupContainerexcel.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPopupContainerexcel)).EndInit();
            this.radPopupContainerexcel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupBoxExcelSheet)).EndInit();
            this.groupBoxExcelSheet.ResumeLayout(false);
            this.groupBoxExcelSheet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxWorkSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radpanelbottom)).EndInit();
            this.radpanelbottom.ResumeLayout(false);
            this.panelImport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonSaveData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.butttonOpenlogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonImport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).EndInit();
            this.tabExport.ResumeLayout(false);
            this.panelExportModule.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxDataGridViewResult)).EndInit();
            this.GroupBoxDataGridViewResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelexporttop)).EndInit();
            this.radPanelexporttop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewResult)).EndInit();
            this.groupBoxExportFormat.ResumeLayout(false);
            this.panelbtnbtm.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSearchExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancelExport)).EndInit();
            this.tabEDI.ResumeLayout(false);
            this.PanelGridInfo.ResumeLayout(false);
            this.panelmailbxandgrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupBoxEDI)).EndInit();
            this.GroupBoxEDI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEDI.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEDI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeViewEDI)).EndInit();
            this.radTreeViewEDI.ResumeLayout(false);
            this.radTreeViewEDI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelEDI)).EndInit();
            this.radPanelEDI.ResumeLayout(false);
            this.radPanelEDI.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubjectOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFrom1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonSaveDraft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubject1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSubject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelCc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxSubject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxCc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxMailData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RibbonTab TabPageConnection;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RibbonTab TabFileImport;
        private Telerik.WinControls.UI.RibbonTab TabPageExport;
        private Telerik.WinControls.UI.RibbonTab TabPageEDI;
        private Telerik.WinControls.UI.RibbonTab TabPageAbout;
        private Telerik.WinControls.UI.RadRadioButtonElement radioButtonQBDesktop;
        private Telerik.WinControls.UI.RadRadioButtonElement radioButtonQBOnline;
        private Telerik.WinControls.UI.RadRadioButtonElement radioButtonQBPOS;
        private Telerik.WinControls.UI.RadRadioButtonElement radioButtonXero;
        private Telerik.WinControls.UI.RadLabelElement growLabelQBCompanyFile;
        public System.Windows.Forms.OpenFileDialog openFileDialogBrowseFile;
        public Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup18;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement2;
        public System.ComponentModel.BackgroundWorker backgroundWorkerQBItemLoader;
        private System.ComponentModel.BackgroundWorker backgroundWorkerExportLoad;
        public System.ComponentModel.BackgroundWorker backgroundWorkerStartupProcessLoader;
        private System.Windows.Forms.ToolTip toolTipInfo;
        private System.ComponentModel.BackgroundWorker backgroundWorkerSendRecieveLoader;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripAddTo;
        private System.Windows.Forms.ToolStripMenuItem AxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteMessageToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialogExportMapping;
        private System.Windows.Forms.OpenFileDialog openFileDialogForImportTemplate;
        private System.Windows.Forms.NotifyIcon notifyIconRecievedMessages;
        public System.ComponentModel.BackgroundWorker backgroundWorkerDelet;
        private System.ComponentModel.BackgroundWorker backgroundWorkerAutomaticSendReceive;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDeleteRows;
        private System.Windows.Forms.ToolStripMenuItem deleteRowsToolStripMenuItem;
        private System.Windows.Forms.BindingSource bindingSourcePreviewData;
        private System.Windows.Forms.SaveFileDialog saveExportedDialog;
        public System.ComponentModel.BackgroundWorker backgroundWorkerProcessLoader;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadRibbonBarGroup groupBoxDataMapping;
        private Telerik.WinControls.UI.RadDropDownListElement comboBoxMappings;
        private Telerik.WinControls.UI.RadButtonElement buttonMapping;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup7;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup9;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxAutoNumbering;
        private Telerik.WinControls.UI.RadCheckBoxElement chkListValidate;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxAddressDetails;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadButtonElement buttonBrowseFile;
        private Telerik.WinControls.UI.RadButtonElement buttonOption;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup10;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup2;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup groupBoxStatementImport;
        private Telerik.WinControls.UI.RadLabelElement labelImportStatement;
        private Telerik.WinControls.UI.RadDropDownListElement comboBoxImportStatement;
        private Telerik.WinControls.UI.RadButtonElement buttonPreviewImportStatement;
        private Telerik.WinControls.UI.RadDropDownListElement radDropDownListElement1;
        private Telerik.WinControls.UI.RadLabelElement labelCodingTemplate;
        private Telerik.WinControls.UI.RadDropDownListElement comboBoxCodingTemplate;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup4;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement1;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup6;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement4;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement5;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement2;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement5;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup11;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement6;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement6;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup12;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement3;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup11;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup7;
        public Telerik.WinControls.UI.RadDropDownListElement comboBoxDatatype;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement3;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadLabelElement textBoxFileName;
        private Telerik.WinControls.UI.RadButtonElement buttonExportMapping;
        private Telerik.WinControls.UI.RadRibbonBarGroup filterGroup1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup5;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxLastExport;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxDaterange;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxRefnumber;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement7;
        private Telerik.WinControls.UI.RadStatusBarPanelElement toolStripStatusLabelProduct;
        private Telerik.WinControls.UI.RadStatusBarPanelElement toolStripStatusLabelMajorVersion;
        private Telerik.WinControls.UI.RadStatusBarPanelElement toolStripStatusLabelMinorVersion;
        private Telerik.WinControls.UI.RadStatusBarPanelElement toolStripStatusLabelCountry;
        private Telerik.WinControls.UI.RadStatusBarPanelElement toolStripStatusLabelConnectionStatus;
        private Telerik.WinControls.UI.RadProgressBarElement toolStripProgressBarIndicator;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup14;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup15;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup GroupBoxColumnChooser;
        public Telerik.WinControls.UI.RadDropDownListElement comboBoxColChooser;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup14;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement1;
        private Telerik.WinControls.UI.RadDropDownListElement radDropDownListElement2;
        private Telerik.WinControls.UI.RadDropDownListElement radDropDownListElement3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup9;
        private Telerik.WinControls.UI.RadRibbonBarGroup panelPaidStatus;
        // private Telerik.WinControls.UI.RadCheckedDropDownList radCheckedDropDownListEntityFilter;

        //Background worker
        private System.ComponentModel.BackgroundWorker backgroundWorkerExportData;
        // private Telerik.WinControls.UI.RadCheckedDropDownListElement radChkDDListAccountTypeFilter;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup17;
        private Telerik.WinControls.UI.RadCheckBoxElement checkBoxIncludeInactive;
        private Telerik.WinControls.UI.RadTextBoxElement textBoxToRefNumber;

        // 
        //598
        private Telerik.WinControls.UI.RadButtonElement radBtnOk;
        private Telerik.WinControls.UI.RadLabel labelMessageID;
        private Telerik.WinControls.UI.RadLabel labelMailCount;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup19;
        private Telerik.WinControls.UI.RadLabelElement labelDataMapingInfo;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup18;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup21;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup16;
        private Telerik.WinControls.EllipseShape ellipseShape1;
        private Telerik.WinControls.ChamferedRectShape chamferedRectShape1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup20;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemSendReceive;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup21;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemTradingPartner;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup22;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemMailSettings;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup23;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup24;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup25;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemRules;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemMonitor;
        private Telerik.WinControls.UI.RadImageButtonElement radMenuItemRefresh;
        private RadRibbonBarButtonGroup groupDateFilter;

        private Telerik.WinControls.UI.RadDateTimePickerElement dateTimePickerTo;
        private Telerik.WinControls.UI.RadDateTimePickerElement dateTimePickerFrom;
        private RadLabelElement From;
        private RadLabelElement LabelTo;
        private RadRibbonBarGroup radRibbonBarGroup3;
        private RadButtonElement buttonQuickBooksConnect;
        private RadTextBoxElement textBoxFromRefnumber;
        private RadTextBoxElement radTextBoxElement1;
        private RadLabelElement radLabelElement6;
        private RadLabelElement radLabelElement7;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup23;
        private RadCheckBoxElement checkBoxPaidStatus;
        private RadDropDownListElement comboBoxPaidStatus;

        private RadRibbonBarButtonGroup radRibbonBarButtonGroup20;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup24;
        private RadRadioButtonElement radRadioButtonElement1;
        private RadRadioButtonElement radRadioButtonElement2;
        private RadRadioButtonElement radRadioButtonElement3;
        private RadRadioButtonElement radRadioButtonElement4;
        private RadRibbonBarGroup radRibbonBarGroup26;
        private RadRibbonBarGroup radRibbonBarGroup8;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup25;
        private RadRadioButtonElement radioButtonSkip;
        private RadRadioButtonElement radioButtonOverwrite;
        private RadRadioButtonElement radioButtonAppend;
        private RadRadioButtonElement radioButtonDuplicate;
        private RadRibbonBarGroup radRibbonBarGroup27;
        private RadHostItem datePickerHost;

        // public Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;

        //AXIS-587 Checked DropDown
        private Telerik.WinControls.UI.RadCheckedDropDownListElement radChkDDListAccountTypeFilter;
        private Telerik.WinControls.UI.RadCheckedDropDownListElement radCheckedDropDownListEntityFilter;
        private RadCheckBoxElement checkBoxEntityFilter;
        private RadImageButtonElement radImageButtonElement1;
        private RadImageButtonElement buttonGet;
        private RadRibbonBarGroup radRibbonBarGroup29;
        private RadRibbonBarGroup radRibbonBarGroup31;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup27;
        private RadButtonElement linkLabelQB;
        private RadButtonElement linkLabelLearnMore;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabImport;
        private System.Windows.Forms.TabPage tabExport;
        private System.Windows.Forms.TabPage tabEDI;
        private System.Windows.Forms.Panel panelImportModule;
        private RadGroupBox GroupBoxImportGrid;
        private RadPanel radPanelTop;
        private RadPanel radpanelbottom;
        private System.Windows.Forms.Panel panelImport;
        private RadButton butttonOpenlogs;
        private RadButton buttonRefresh;
        private RadButton buttonImport;
        private RadButton buttonDelete;
        public RadGridView dataGridViewDataPreview;
        private System.Windows.Forms.Panel panelExportModule;
        private RadGroupBox GroupBoxDataGridViewResult;
        private System.Windows.Forms.GroupBox groupBoxExportFormat;
        private System.Windows.Forms.CheckBox checkBoxDeleteFromQB;
        private System.Windows.Forms.CheckBox checkBoxSend;
        private System.Windows.Forms.ComboBox comboBoxExportFormat;
        private System.Windows.Forms.Label label1;
        private RadPanel radPanelexporttop;
        public RadGridView dataGridViewResult;
        private System.Windows.Forms.Panel PanelGridInfo;
        private RadGroupBox GroupBoxEDI;
        public RadGridView radGridViewEDI;
        private RadTreeView radTreeViewEDI;
        private System.Windows.Forms.Label LabelNodeSearch;
        private RadPanel radPanelEDI;
        private RadLabel radLabelSubjectOut;
        private RadLabel radLabelFrom1;
        private RadButton radButtonSend;
        private RadButton radButtonCc;
        private RadButton radButtonTo;
        private RadButton radButtonSaveDraft;
        private RadLabel radLabelSubject1;
        private RadLabel radLabelCc1;
        private RadLabel radLabelSubject;
        private RadLabel radLabelCc;
        private RadLabel radLabelFrom;
        private RadPanel radPanelAttachment;
        private RadTextBox radTextBoxSubject;
        private RadTextBox radTextBoxCc;
        private RadTextBox radTextBoxTo;
        private RadTextBox radTextBoxMailData;
        private RadLabel radLabelAttachment;
        private System.Windows.Forms.Panel panelmailbxandgrid;
        private RadPanel radPanelSelectFile;
        private RadGroupBox groupBoxTransType;
        private RadGroupBox groupBoxExcelSheet;
        private RadDropDownList comboBoxWorkSheet;
        private RadPopupContainer radPopupContainerexcel;
        private RadPopupEditor radPopupEditorExcel;
        private RadPopupEditor radPopupEditorIIF;
        private RadLabel radLabel1;
        private RadDropDownList comboBoxXmlPreviewTable;
        private RadButton buttonPreview;
        private RadPopupContainer radPopupContainerIIF;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup16;
        public RadDropDownList comboBoxIIFTransType;
        private RootRadElement object_ee5a844a_8a5a_48af_958d_95850aee92f5;
        private RadLabel radLabel2;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup15;
        private RadLabelElement labelProductName;
        private RadLabelElement labelBN;
        private RadImageButtonElement buttonReactivateLicense;
        private RadImageButtonElement buttonLicenseInfo;
        private RadRibbonBarGroup radRibbonBarGroup12;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup3;
        private RadImageButtonElement radLabelElement8;
        private RadImageButtonElement radLabelElement9;
        private RadImageButtonElement radLabelElement10;
        private RadCheckBoxElement checkBoxUpdatednewnames;
        private RadCheckBoxElement checkBoxAppendItems;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup13;
        private RadCheckBoxElement checkBoxReportType;
        private RadDropDownListElement comboBoxReportType;
        private RadRibbonBarButtonGroup radRibbonBarButtonGroup17;
        private RadCheckBoxElement checkBoxMaintainLog;
        private RadImageButtonElement btnColChooserExport;
        private RadImageButtonElement btnResetTemplate;
        private RadCheckBox checkBoxHasHeaders;
        private RadPopupEditor radPopupEditorCmnOptn;
        private RadPopupContainer radPopupContainerCmnOptn;
        private RadGroupBox radGroupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownImport;
        private RadLabel labelImportRow;
        private RadPopupContainer radPopupContainerText;
        private RadGroupBox groupBoxFileDelimiter;
        private RadRadioButton radioButtonPipe;
        private RadRadioButton radioButtoncomma;
        private RadRadioButton radioButtonTab;
        private RadLabel radLabelElement11;
        private RadPopupEditor radPopupEditorTextFile;
        private RadButton buttonCancelExport;
        private RadButton buttonOK;
        private RadRibbonBarGroup radRibbonBarGroup13;
        private RadImageButtonElement btncheckforupdate;
        private System.Windows.Forms.Panel panelbtnbtm;
        private System.Windows.Forms.Panel panel1;
        private RadPanel radPanel1;
        private RadRibbonBarGroup radRibbonBarGroup19;
        private RadImageButtonElement buttonDeactivateLicense;
        private RadButton buttonSearch;
        private RadButton buttonSearchExp;
        private RadButton buttonSaveData;
        //AXIS-587 Checked DropDown  end
    }
}
