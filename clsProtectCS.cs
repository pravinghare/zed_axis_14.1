using System;
using System.Text;
using System.Runtime.InteropServices;

namespace TransactionImporter
{
	/// <summary>
  /// This class demonstates how to create wrapper class for Logic Protect API usage
  /// This technique also can be used in any .NET language e.g. VB.NET as well as in other
  /// laguages such as C++, Delphi, VB6 and others.
	/// </summary>
	public class clsProtectCS
	{
    /*-------------------------------------------------------------------------------------
    ' Declartions below describe how to use Logic Protect API in the C# application   
    -------------------------------------------------------------------------------------*/

    //----------------------------------------------------------------------
    // This declaration is for import SetActivationServiceURL from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetActivationServiceURL( [MarshalAs(UnmanagedType.LPStr)] string ServerName);

    //----------------------------------------------------------------------
    // This declaration is for import SetModeAP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetModeLP ( [MarshalAs(UnmanagedType.I4)] int Mode);

    //----------------------------------------------------------------------
    // This declaration is for import GetModeAP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int GetModeLP ( );

    //----------------------------------------------------------------------
    // This declaration is for import SetLicenseFilePathLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetLicenseFilePathLP( [MarshalAs(UnmanagedType.LPStr)] string licenseFileFullPath);

    //----------------------------------------------------------------------
    // This declaration is for import SetKeyFilePathLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetKeyFilePathLP( [MarshalAs(UnmanagedType.LPStr)] string keyFileFullPath);

    //----------------------------------------------------------------------
    // This declaration is for import SetTrialDaysLP
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int SetTrialDaysLP ( [MarshalAs(UnmanagedType.I4)] int maxDays );

    //----------------------------------------------------------------------
    // This declaration is for import APGetDaysLeft from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int GetDaysLeftLP ( [MarshalAs(UnmanagedType.I4)] int maxDays );

    //----------------------------------------------------------------------
    // This declaration is for import LPIsExpired
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int LPIsExpired ( [MarshalAs(UnmanagedType.I4)] int maxDays );

    //----------------------------------------------------------------------
    // This declaration is for import LPIsActivated from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int LPIsActivated ( );

    //----------------------------------------------------------------------
    // This declaration is for import APActivateWithDialog from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void ActivateWithDialogLP( [MarshalAs(UnmanagedType.LPStr)] string productName,
      [MarshalAs(UnmanagedType.LPStr)] string dialogTitle);

    //----------------------------------------------------------------------
    // This declaration is for import APActivateWithTrialDialogs
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void ActivateWithTrialDialogsLP( 
      [MarshalAs(UnmanagedType.LPStr)] string productName,
      [MarshalAs(UnmanagedType.I4)]  int maxDays,
      [MarshalAs(UnmanagedType.LPStr)] string dialogTitle);

    //----------------------------------------------------------------------
    // This declaration is for import APActivate
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int ActivateLP( 
      [MarshalAs(UnmanagedType.LPStr)] ref string productName,
      [MarshalAs(UnmanagedType.LPStr)] ref string serialNumber,
      [MarshalAs(UnmanagedType.LPStr)] ref string password);

    //----------------------------------------------------------------------
    // This declaration is for import APActivate1
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int Activate1LP( 
      [MarshalAs(UnmanagedType.LPStr)] ref string productName,
      [MarshalAs(UnmanagedType.LPStr)] ref string serialNumber,
      [MarshalAs(UnmanagedType.LPStr)] ref string password,
      [MarshalAs(UnmanagedType.LPStr)] ref string firstName,
      [MarshalAs(UnmanagedType.LPStr)] ref string lastName,
      [MarshalAs(UnmanagedType.LPStr)] ref string company,
      [MarshalAs(UnmanagedType.LPStr)] ref string city,
      [MarshalAs(UnmanagedType.LPStr)] ref string state,
      [MarshalAs(UnmanagedType.LPStr)] ref string zip,
      [MarshalAs(UnmanagedType.LPStr)] ref string email,
      [MarshalAs(UnmanagedType.LPStr)] ref string country
      );

    //----------------------------------------------------------------------
    // This declaration is for import SetProductVersionIdLP
    // from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int SetProductVersionIdLP ( [MarshalAs(UnmanagedType.I4)] int versionID );

    //----------------------------------------------------------------------
    // This declaration is for import SetActivationModeLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int SetActivationModeLP ( [MarshalAs(UnmanagedType.I4)] int mode );

    //----------------------------------------------------------------------
    // This declaration is for import GetActivationModeLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int GetActivationModeLP ( );

    //----------------------------------------------------------------------
    // This declaration is for import GetPINActivationCodeLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int GetPINActivationCodeLP ( );

    //----------------------------------------------------------------------
    // This declaration is for import StorePhoneActivationCodeLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void StorePhoneActivationCodeLP ( [MarshalAs(UnmanagedType.LPStr)] string activationCode );


    // New in LP 6.x version

    //----------------------------------------------------------------------
    // This declaration is for import SetLicenseTypeLP from Cli_LP.dll
    // licenseType : 0 - "per user- strong"; 1 - "per OS"; 2 - "per Computer"
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetLicenseTypeLP([MarshalAs(UnmanagedType.I4)] int licenseType); 

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseTypeLP from Cli_LP.dll
    // return current license type
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int GetLicenseTypeLP();                  

    //----------------------------------------------------------------------
    // This declaration is for import ShowSaasInformationLP from Cli_LP.dll
    // shows buildin saas informaton dialog
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void ShowPackageInformationLP();             

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseProductInfoLP from Cli_LP.dll
    //----------------------------------------------------------------------
    
    [DllImport ("Cli_LP.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern bool GetLicenseProductInfoLP(
      [In,Out,MarshalAs(UnmanagedType.LPStr  )] StringBuilder  productName, 
      [In,Out,MarshalAs(UnmanagedType.LPStr )] StringBuilder  productDecription, 
      [In,Out,MarshalAs(UnmanagedType.LPStr )] StringBuilder  expirationDate,
      [MarshalAs(UnmanagedType.I4)] ref int DaysLeft,
      [MarshalAs(UnmanagedType.I4)] ref int AllowedRuns,
      [MarshalAs(UnmanagedType.I4)] ref int RealRuns,
      [MarshalAs(UnmanagedType.I4)] ref int AllowedMnutes,
      [MarshalAs(UnmanagedType.I4)] ref int RealMinutes );
    
    
    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CharSet=CharSet.Auto, CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern bool GetLicenseModuleInfoLP(
      [In,Out,MarshalAs(UnmanagedType.LPStr)] StringBuilder  moduleName, 
      [In,Out,MarshalAs(UnmanagedType.LPStr)] StringBuilder  moduleDescription, 
      [In,Out,MarshalAs(UnmanagedType.LPStr)] StringBuilder  expirationDate, 
      [MarshalAs(UnmanagedType.I4)] ref int DaysLeft,
      [MarshalAs(UnmanagedType.I4)] ref int AllowedRuns,
      [MarshalAs(UnmanagedType.I4)] ref int RealRuns,
      [MarshalAs(UnmanagedType.I4)] ref int AllowedMnutes,
      [MarshalAs(UnmanagedType.I4)] ref int RealMinutes );

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
    // retrive license from the LP server using internal dialog
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int Reactivate1LP();                      

        //P Axis 13.1 : issue 696
        //----------------------------------------------------------------------
        // This declaration is for Deactivate License from Cli_LP.dll
        //----------------------------------------------------------------------
        [DllImport("Cli_LP.dll", CallingConvention = CallingConvention.Cdecl, PreserveSig = true)]
        public static extern int DeactivateLicenseLP();

        //----------------------------------------------------------------------
        // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
        // retrive license from the LP server using directserver coonect
        //----------------------------------------------------------------------
        [DllImport("Cli_LP.dll", CallingConvention = CallingConvention.Cdecl, PreserveSig = true)]
        public static extern int Reactivate2LP(
      [MarshalAs(UnmanagedType.LPStr)] string productName,
      [MarshalAs(UnmanagedType.LPStr)] string serialNumber,
      [MarshalAs(UnmanagedType.LPStr)] string password);

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
    // retrive license from the LP server using directserver coonect
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int Reactivate3LP(
      [MarshalAs(UnmanagedType.LPStr)] string productName, 
      [MarshalAs(UnmanagedType.LPStr)] string serialNumber, 
      [MarshalAs(UnmanagedType.LPStr)] string password, 
      [MarshalAs(UnmanagedType.LPStr)] string firstName,
      [MarshalAs(UnmanagedType.LPStr)] string lastName,
      [MarshalAs(UnmanagedType.LPStr)] string company,
      [MarshalAs(UnmanagedType.LPStr)] string city,
      [MarshalAs(UnmanagedType.LPStr)] string state,
      [MarshalAs(UnmanagedType.LPStr)] string zip,
      [MarshalAs(UnmanagedType.LPStr)] string email,
      [MarshalAs(UnmanagedType.LPStr)] string country); 

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
    // Set product info. Used with Reactivate1LP
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetProductInfo1LP(
      [MarshalAs(UnmanagedType.LPStr)] string productName, 
      [MarshalAs(UnmanagedType.LPStr)] string serialNumber, 
      [MarshalAs(UnmanagedType.LPStr)] string password); 

    //----------------------------------------------------------------------
    // This declaration is for import GetLicenseModuleInfoLP from Cli_LP.dll
    // Set product info. Used with Reactivate3LP
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetProductInfo2LP(
      [MarshalAs(UnmanagedType.LPStr)] string productName, 
      [MarshalAs(UnmanagedType.LPStr)] string serialNumber, 
      [MarshalAs(UnmanagedType.LPStr)] string password, 
      [MarshalAs(UnmanagedType.LPStr)] string firstName,
      [MarshalAs(UnmanagedType.LPStr)] string lastName,
      [MarshalAs(UnmanagedType.LPStr)] string company,
      [MarshalAs(UnmanagedType.LPStr)] string city,
      [MarshalAs(UnmanagedType.LPStr)] string state,
      [MarshalAs(UnmanagedType.LPStr)] string zip,
      [MarshalAs(UnmanagedType.LPStr)] string email,
      [MarshalAs(UnmanagedType.LPStr)] string country); 

    //----------------------------------------------------------------------
    // This declaration is for import SetMessagesHeaderLP from Cli_LP.dll
    // Set dialog messages headers
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetMessagesHeaderLP([MarshalAs(UnmanagedType.LPStr)] string header); 

    //----------------------------------------------------------------------
    // This declaration is for import GetMessagesHeaderLP from Cli_LP.dll
    // Set dialog messages headers
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern string GetMessagesHeaderLP(); 

    //----------------------------------------------------------------------
    // This declaration is for import CheckInternetConnectionLP from Cli_LP.dll
    // Check for internet cnnection
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int CheckInternetConnectionLP(); 

    //----------------------------------------------------------------------
    // This declaration is for import LPEndApplication from Cli_LP.dll
    // Intermidate stop appplication process
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void LPEndApplication(); 

    //----------------------------------------------------------------------
    // This declaration is for import ValidateLicenseLP from Cli_LP.dll
    // One time connect to the LP server and make advanced license verification
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int ValidateLicenseLP([MarshalAs(UnmanagedType.I4)] int bUseMessages ); 

    //----------------------------------------------------------------------
    // This declaration is for import EraseLicenseLP from Cli_LP.dll
    // Wipe and erase LP license file
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int EraseLicenseLP(); 

    //----------------------------------------------------------------------
    // This declaration is for import LoadstringLP from Cli_LP.dll
    // One time connect to the LP server and make advanced license verification
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern string LoadStringLP([MarshalAs(UnmanagedType.I4)] int code ); 

    //----------------------------------------------------------------------
    // This declaration is for import IsOnlineActivatedLP from Cli_LP.dll
    // One time connect to the LP server and make advanced license verification
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern int IsOnlineActivatedLP( ); 

    //----------------------------------------------------------------------
    // This declaration is for import SetMessagesFileName from Cli_LP.dll
    // One time connect to the LP server and make advanced license verification
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetMessagesFileName( string FileName ); 

    //----------------------------------------------------------------------
    // This declaration is for import SetDllPathLP from Cli_LP.dll
    // One time connect to the LP server and make advanced license verification
    //----------------------------------------------------------------------
    [DllImport ("Cli_LP.dll", CallingConvention=CallingConvention.Cdecl, PreserveSig=true)] 
    public static extern void SetDllPathLP(string FileName);


   
   }
}
