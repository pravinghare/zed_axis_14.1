﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class TimeMessage : Form
    {

        private static TimeMessage m_TimeMess;

        public static TimeMessage getInstance()
        {
           // if (m_TimeMess == null)
                m_TimeMess = new TimeMessage();
            return m_TimeMess;
        }

        public TimeMessage()
        {
            InitializeComponent();
        }

        private void TimeMessage_Load(object sender, EventArgs e)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        internal void SetDialogBox(string message, MessageBoxIcon icon)
        {
                  
            this.growLabelMessags.Text = message;
            
            this.pictureBox1.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
        }
    }
}
