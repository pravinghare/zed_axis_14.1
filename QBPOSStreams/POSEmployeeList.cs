﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;


namespace QBPOSStreams
{
  public  class POSEmployeeList:POS_BaseEmployeeList
    {
        #region  Properties

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public decimal CommissionPercent
        {
            get { return m_CommissionPercent; }
            set { m_CommissionPercent = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        public string Email
        {
            get { return m_EMail; }
            set { m_EMail = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        public string IsTrackingHours
        {
            get { return m_IsTrackingHours; }
            set { m_IsTrackingHours = value; }
        }

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public string LoginName
        {
            get { return m_LoginName; }
            set { m_LoginName = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Phone2
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }

        public string Phone3
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string Phone4
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string Street
        {
            get { return m_Street; }
            set { m_Street = value; }
        }

        public string Street2
        {
            get { return m_Street2; }
            set { m_Street2 = value; }
        }

        public string SecurityGroup
        {
            get { return m_SecurityGroup; }
            set { m_SecurityGroup = value; }
        }
        #endregion
    }

 public class POSEmployeeCollection : Collection<POSEmployeeList>
  {
     string XmlFileData = string.Empty;

        #region private method
            /// <summary>
            /// Assigning the xml value to the class objects
            /// </summary>
            /// <param name="employeeXmlList"></param>
            /// <returns></returns>
            private Collection<POSEmployeeList> GetEmployeeList(string employeeXmlList)
            {
                //Create a xml document for output result.
                XmlDocument outputXMLDocOfEmployee = new XmlDocument();

                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                
                if (employeeXmlList != string.Empty)
                {
                    //Loading Item List into XML.
                    outputXMLDocOfEmployee.LoadXml(employeeXmlList);
                    XmlFileData = employeeXmlList;

                    //Getting employee values from QuickBooks response.
                    XmlNodeList EmployeeNodeList = outputXMLDocOfEmployee.GetElementsByTagName(QBPOSEmployeeList.EmployeeRet.ToString());

                    foreach (XmlNode EmployeeNodes in EmployeeNodeList)
                    {
                        if (bkWorker.CancellationPending != true)
                        {
                           
                            POSEmployeeList employee = new POSEmployeeList();

                            foreach (XmlNode node in EmployeeNodes.ChildNodes)
                            {
                                //Checking ListID. 
                                if (node.Name.Equals(QBPOSEmployeeList.ListID.ToString()))
                                {
                                    employee.ListID = node.InnerText;
                                }

                                //Checking TimeCreated
                                if (node.Name.Equals(QBPOSEmployeeList.TimeCreated.ToString()))
                                {
                                    employee.TimeCreated = node.InnerText;
                                }

                                //Checking TimeModified
                                if (node.Name.Equals(QBPOSEmployeeList.TimeModified.ToString()))
                                {
                                    employee.TimeModified = node.InnerText;
                                }
                                //Checking City. 
                                if (node.Name.Equals(QBPOSEmployeeList.City.ToString()))
                                {
                                    employee.City = node.InnerText;
                                }
                                //Checking CommissionPercent
                                if (node.Name.Equals(QBPOSEmployeeList.CommissionPercent.ToString()))
                                {
                                    try
                                    {
                                        employee.CommissionPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                                //Checking Country
                                if (node.Name.Equals(QBPOSEmployeeList.Country.ToString()))
                                {
                                    employee.Country = node.InnerText;
                                }

                                //Checking Email
                                if (node.Name.Equals(QBPOSEmployeeList.EMail.ToString()))
                                {
                                    employee.Email = node.InnerText;
                                }

                                //Checking FirstName
                                if (node.Name.Equals(QBPOSEmployeeList.FirstName.ToString()))
                                {
                                    employee.FirstName = node.InnerText;
                                }

                                //Checking IsTrackingHours
                                if (node.Name.Equals(QBPOSEmployeeList.IsTrackingHours.ToString()))
                                {
                                    employee.IsTrackingHours = node.InnerText;
                                }

                                  //Checking LastName
                                if (node.Name.Equals(QBPOSEmployeeList.LastName.ToString()))
                                {
                                    employee.LastName = node.InnerText;
                                }
                                  //Checking LoginName
                                if (node.Name.Equals(QBPOSEmployeeList.LoginName.ToString()))
                                {
                                    employee.LoginName = node.InnerText;
                                }
                               
                                 if (node.Name.Equals(QBPOSEmployeeList.Notes.ToString()))
                                 {
                                        employee.Notes = node.InnerText;
                                 }

                                 if (node.Name.Equals(QBPOSEmployeeList.Phone.ToString()))
                                 {
                                     employee.Phone = node.InnerText;
                                 }

                                if (node.Name.Equals(QBPOSEmployeeList.Phone2.ToString()))
                                {
                                        employee.Phone2= node.InnerText;
                                }

                                if (node.Name.Equals(QBPOSEmployeeList.Phone3.ToString()))
                                {
                                        employee.Phone3 = node.InnerText;
                                }

                                if (node.Name.Equals(QBPOSEmployeeList.Phone4.ToString()))
                                {
                                        employee.Phone4 = node.InnerText;
                                }

                                 if (node.Name.Equals(QBPOSEmployeeList.PostalCode.ToString()))
                                 {
                                        employee.PostalCode = node.InnerText;
                                 }
                                 if (node.Name.Equals(QBPOSEmployeeList.State.ToString()))
                                 {
                                        employee.State = node.InnerText; 
                                 }
                                 if (node.Name.Equals(QBPOSEmployeeList.Street.ToString()))
                                 {
                                        employee.Street = node.InnerText;
                                 }
                                 if (node.Name.Equals(QBPOSEmployeeList.Street2.ToString()))
                                 {
                                        employee.Street2 = node.InnerText;
                                 }
                                 if (node.Name.Equals(QBPOSEmployeeList.SecurityGroup.ToString()))
                                 {
                                        employee.SecurityGroup = node.InnerText;
                                 }

                             }
                        this.Add(employee);
                     }
                    else
                    {
                        return null;
                    }
                }
                     outputXMLDocOfEmployee.RemoveAll();
            }
            return this;
        }   
       #endregion

        #region public Method
            /// <summary>
            /// if no filter was selected
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <returns></returns>
            /// 
            public List<string> GetAllPOSEmployeeList(string QBFileName)
            {
                string employeeXmlList = QBCommonUtilities.GetListFromQBPOS("EmployeeQueryRq", QBFileName);

                List<string> empList = new List<string>();
                List<string> empList1 = new List<string>();

                XmlDocument outputXMLDoc = new XmlDocument();

                outputXMLDoc.LoadXml(employeeXmlList);

                XmlFileData = employeeXmlList;

                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

                XmlNodeList employeeNodeList = outputXMLDoc.GetElementsByTagName(QBPOSEmployeeList.EmployeeRet.ToString());
               

                foreach (XmlNode custNodes in employeeNodeList)
                {
                    string firstname = string.Empty;
                    string lastname = string.Empty;
                    string fullname = string.Empty;
                    if (bkWorker.CancellationPending != true)
                    {
                        foreach (XmlNode node in custNodes.ChildNodes)
                        {                          

                            if (node.Name.Equals(QBPOSEmployeeList.FirstName.ToString()))
                            {
                                firstname = node.InnerText;
                            }
                            if (node.Name.Equals(QBPOSEmployeeList.LastName.ToString()))
                            {
                                lastname = node.InnerText;
                            }
                            if (node.Name.Equals(QBPOSEmployeeList.LoginName.ToString()))
                            {
                                if (firstname != null || lastname != null)
                                {
                                    if (!empList.Contains(firstname + " " + lastname))
                                    {
                                        fullname = firstname + " " + lastname;
                                        empList.Add(fullname);
                                        string name = fullname + "     :     Employee";
                                        empList1.Add(name);
                                    }
                                }
                            }
                        }
                    }
                }

                CommonUtilities.GetInstance().EmployeeListWithType = empList1;

                return empList;

            }
            
            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName)
            {
                string EmployeeXmlList = QBCommonUtilities.GetListFromQBPOS("EmployeeQueryRq", QBFileName);
                return GetEmployeeList(EmployeeXmlList);
            }

            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, string ToDate)
            {
                string EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate);
                return GetEmployeeList(EmployeeXmlList);
            }
            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, DateTime ToDate)
            {
                string EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate);
                return GetEmployeeList(EmployeeXmlList);
            }
            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName, List<string> customerName)
            {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }

            string EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("EmployeeQueryRq", QBFileName, firstname, lastname);
                return GetEmployeeList(EmployeeXmlList);
            }

            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> customerName)
            {
            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }

            string EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate, firstname, lastname);
                return GetEmployeeList(EmployeeXmlList);
            }

            public Collection<POSEmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, string ToDate, List<string> customerName)
            {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }

            string EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate, firstname, lastname);
                return GetEmployeeList(EmployeeXmlList);
            }
            /// <summary>
            /// This method is used to get the xml response from the QuikBooks.
            /// </summary>
            /// <returns></returns>
            public string GetXmlFileData()
            {
                return XmlFileData;
            }
            #endregion
  }

}
