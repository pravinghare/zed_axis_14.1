﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;
namespace QBPOSStreams
{
  public class POSPriceDiscountList:POS_BasePriceDiscountList
    {
      #region Public Properties
      public string TxnID
       {
           get { return m_TxnID; }
           set { m_TxnID = value; }
       }
      
       public string TimeCreated
       {
           get { return m_TimeCreated; }
           set { m_TimeCreated = value; }
       }
       public string TimeModified
       {
           get { return m_TimeModified; }
           set { m_TimeModified = value; }
       }

       public string StoreExchangeStatus
       {
           get { return m_StoreExchangeStatus; }
           set { m_StoreExchangeStatus = value; }
       }

       public string PriceDiscountName
       {
           get { return m_PriceDiscountName; }
           set { m_PriceDiscountName = value; }
       }

       public string PriceDiscountReason
       {
           get { return m_PriceDiscountReason; }
           set { m_PriceDiscountReason = value; }
       }
            
       public string Comments
       {
           get { return m_Comments; }
           set { m_Comments = value; }
       }
            

       public string Associate
       {
           get { return m_Associate; }
           set { m_Associate = value; }
       }

       public string LastAssociate
       {
           get { return m_LastAssociate; }
           set { m_LastAssociate = value; }
       }

       public string PriceDiscountType
       {
           get { return m_PriceDiscountType; }
           set { m_PriceDiscountType = value; }
       }
      
       public string IsInactive
      {
          get { return m_IsInactive; } 
          set { m_IsInactive = value; }
      }  
   
       public string PriceDiscountPriceLevels
      {
          get { return m_PriceDiscountPriceLevels; }
          set { m_PriceDiscountPriceLevels = value; }
      }  

       public string PriceDiscountXValue
      {
          get { return m_PriceDiscountXValue; }
          set { m_PriceDiscountXValue = value; }
      }  

       public string PriceDiscountYValue
      {
          get { return m_PriceDiscountYValue; }
          set { m_PriceDiscountYValue = value; }
      }  
      
       public string IsApplicableOverXValue
      {
          get { return m_IsApplicableOverXValue; }
          set { m_IsApplicableOverXValue = value; }
      }  
        public string StartDate
      {
          get { return m_StartDate; }
          set { m_StartDate = value; }
      }  
        public string StopDate
      {
          get { return m_StopDate; }
          set { m_StopDate = value; }
      }  
        public string ItemsCount
      {
          get { return m_ItemsCount; }
          set { m_ItemsCount = value; }
      }  
       public string LineListID
      {
          get { return m_ListID; }
          set { m_ListID = value; }
      }  

       public string LineTxnLineID
      {
          get { return m_TxnLineID; }
          set { m_TxnLineID = value; }
      }

       public string LineUnitOfMeasure
      {
          get { return m_UnitOfMeasure; }
          set { m_UnitOfMeasure = value; }
      }  
     
       #endregion  
    }

  public class POSPriceDiscountCollection : Collection<POSPriceDiscountList>
  {
      string XmlFileData = string.Empty;

      #region private method
      /// <summary>
      /// Assigning the xml value to the class objects
      /// </summary>
      /// <param name="employeeXmlList"></param>
      /// <returns></returns>
      private Collection<POSPriceDiscountList> GetPriceDiscountList(string PriceDiscountXmlList)
      {
          //Create a xml document for output result.
          XmlDocument outputXMLDocOfPriceDiscount = new XmlDocument();

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

          if (PriceDiscountXmlList != string.Empty)
          {
              //Loading Item List into XML.
              outputXMLDocOfPriceDiscount.LoadXml(PriceDiscountXmlList);
              XmlFileData = PriceDiscountXmlList;

              //Getting employee values from QuickBooks response.
              XmlNodeList PriceDiscountNodeList = outputXMLDocOfPriceDiscount.GetElementsByTagName(QBPOSPriceDiscountList.PriceDiscountRet.ToString());

              foreach (XmlNode PriceDiscountNodes in PriceDiscountNodeList)
              {
                  if (bkWorker.CancellationPending != true)
                  {
                      POSPriceDiscountList priceDiscount = new POSPriceDiscountList();
                      foreach (XmlNode node in PriceDiscountNodes.ChildNodes)
                      {
                          //Checking ListID. 
                          if (node.Name.Equals(QBPOSPriceDiscountList.TxnID.ToString()))
                          {
                              priceDiscount.TxnID = node.InnerText;
                          }

                          //Checking TimeCreated
                          if (node.Name.Equals(QBPOSPriceDiscountList.TimeCreated.ToString()))
                          {
                              priceDiscount.TimeCreated = node.InnerText;
                          }

                          //Checking TimeModified
                          if (node.Name.Equals(QBPOSPriceDiscountList.TimeModified.ToString()))
                          {
                              priceDiscount.TimeModified = node.InnerText;
                          }
                          //Checking City. 
                          if (node.Name.Equals(QBPOSPriceDiscountList.StoreExchangeStatus.ToString()))
                          {
                              priceDiscount.StoreExchangeStatus = node.InnerText;
                          }
                          //Checking CommissionPercent
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountName.ToString()))
                          {
                              priceDiscount.PriceDiscountName = node.InnerText;

                          }
                          //Checking Country
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountReason.ToString()))
                          {
                              priceDiscount.PriceDiscountReason = node.InnerText;
                          }

                          //Checking Email
                          if (node.Name.Equals(QBPOSPriceDiscountList.Comments.ToString()))
                          {
                              priceDiscount.Comments = node.InnerText;
                          }

                          //Checking FirstName
                          if (node.Name.Equals(QBPOSPriceDiscountList.Associate.ToString()))
                          {
                              priceDiscount.Associate = node.InnerText;
                          }

                          //Checking FirstName
                          if (node.Name.Equals(QBPOSPriceDiscountList.LastAssociate.ToString()))
                          {
                              priceDiscount.LastAssociate = node.InnerText;
                          }
                          //Checking IsTrackingHours
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountType.ToString()))
                          {
                              priceDiscount.PriceDiscountType = node.InnerText;
                          }

                          //Checking LastName
                          if (node.Name.Equals(QBPOSPriceDiscountList.IsInactive.ToString()))
                          {
                              priceDiscount.IsInactive = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountPriceLevels.ToString()))
                          {
                              priceDiscount.PriceDiscountPriceLevels = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountXValue.ToString()))
                          {
                              priceDiscount.PriceDiscountXValue = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountYValue.ToString()))
                          {
                              priceDiscount.PriceDiscountYValue = node.InnerText;
                          }
                          //Checking LastName
                          if (node.Name.Equals(QBPOSPriceDiscountList.IsApplicableOverXValue.ToString()))
                          {
                              priceDiscount.IsApplicableOverXValue = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceDiscountList.StartDate.ToString()))
                          {
                              priceDiscount.StartDate = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceDiscountList.StopDate.ToString()))
                          {
                              priceDiscount.StopDate = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSPriceDiscountList.ItemsCount.ToString()))
                          {
                              priceDiscount.ItemsCount = node.InnerText;
                          }

                          //Checking PriceDiscountItemRet
                          if (node.Name.Equals(QBPOSPriceDiscountList.PriceDiscountItemRet.ToString()))
                          {
                              //Getting values of address.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {

                                  if (childNode.Name.Equals(QBPOSPriceDiscountList.LineListID.ToString()))
                                      priceDiscount.LineListID = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSPriceDiscountList.LineTxnLineID.ToString()))
                                      priceDiscount.LineTxnLineID = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSPriceDiscountList.LineUnitOfMeasure.ToString()))
                                      priceDiscount.LineUnitOfMeasure = childNode.InnerText;
                                  
                              }
                          }

                      }
                      this.Add(priceDiscount);
                  }
                  else
                  {
                      return null;
                  }
              }
              outputXMLDocOfPriceDiscount.RemoveAll();
          }
          return this;
      }
      #endregion

      #region public Method
      /// <summary>
      /// if no filter was selected
      /// </summary>
      /// <param name="QBFileName"></param> 
      /// <returns></returns>
      /// 
      public Collection<POSPriceDiscountList> PopulatePriceDiscountList(string QBFileName)
      {
          string priceDiscountList = QBCommonUtilities.GetListFromQBPOS("PriceDiscountQueryRq", QBFileName);
          return GetPriceDiscountList(priceDiscountList);
      }

      public Collection<POSPriceDiscountList> PopulatePriceDiscountList(string QBFileName, DateTime FromDate, string ToDate)
      {
          string priceDiscountList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PriceDiscountQueryRq", QBFileName, FromDate, ToDate);
          return GetPriceDiscountList(priceDiscountList);
      }
      public Collection<POSPriceDiscountList> PopulatePriceDiscountList(string QBFileName, DateTime FromDate, DateTime ToDate)
      {
          string priceDiscountList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PriceDiscountQueryRq", QBFileName, FromDate, ToDate);
          return GetPriceDiscountList(priceDiscountList);
      }
      /// <summary>
      /// This method is used to get the xml response from the QuikBooks. 
      /// </summary>
      /// <returns></returns>
      public string GetXmlFileData()
      {
          return XmlFileData;
      }
      #endregion
  }
}
