﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;


namespace QBPOSStreams
{
    public class POSCustomerList : POS_BaseCustomerList
    {
       #region Public Properties

       public string ListID
       {
           get { return m_ListID; }
           set { m_ListID = value; }
       }

       public string TimeCreated
       {
           get { return m_TimeCreated; }
           set { m_TimeCreated = value; }
       }

       public string TimeModified
       {
           get { return m_TimeModified; }
           set { m_TimeModified = value; }
       }

       //public decimal?[] AccountBalance
       //{
       //    get { return m_AccountBalance; }
       //    set { m_AccountBalance = value; }
       //}

       //public decimal?[] AccountLimit
       //{
       //    get { return m_AccountLimit; }
       //    set { m_AccountLimit = value; }
       //}
      
       //public decimal?[] AmountPastDue
       //{
       //    get { return m_AmountPastDue; }
       //    set { m_AmountPastDue = value; }
       //}

       public string CompanyName
       {
           get { return m_CompanyName; }
           set { m_CompanyName = value; }
       }

       public string CustomerID
       {
           get { return m_CustomerID; }
           set { m_CustomerID = value; }
       }

       public decimal CustomerDiscPercent
       {
           get { return m_CustomerDiscPercent; }
           set { m_CustomerDiscPercent = value; }
       }

       public string CustomerDiscType
       {
           get { return m_CustomerDiscType; }
           set { m_CustomerDiscType = value; }
       }

       public string CustomerType
       {
           get { return m_CustomerType; }
           set { m_CustomerType = value; }
       }

       public string EMail
       {
           get { return m_Email; }
           set { m_Email = value; }
       }

       public string  IsOkToEMail
       {
           get { return m_IsOkToEMail; }
           set { m_IsOkToEMail = value; }
       }

       public string FirstName
       {
           get { return m_FirstName; }
           set { m_FirstName = value; }
       }

       public string FullName
       {
           get { return m_FullName; }
           set { m_FullName = value; }
       }

       public string IsAcceptingChecks
       {
           get { return m_IsAcceptingChecks; }
           set { m_IsAcceptingChecks = value; }
       }

       public string IsUsingChargeAccount
       {
           get { return m_IsUsingChargeAccount; }
           set { m_IsUsingChargeAccount = value; }
       }

       public string IsUsingWithQB
       {
           get { return m_IsUsingWithQB; }
           set { m_IsUsingWithQB = value; }
       }

       public string IsRewardsMember
       {
           get { return m_IsRewardsMember; }
           set { m_IsRewardsMember = value; }
       }

       public string IsNoShipToBilling
       {
           get { return m_IsNoShipToBilling; }
           set { m_IsNoShipToBilling = value; }
       }

      public  string LastName
      {
          get { return m_LastName; }
          set { m_LastName = value; }
      }

       public  string LastSale
       {
           get { return m_LastSale; }
           set { m_LastSale = value; }
       }

       public string Notes
       {
           get { return m_Notes; }
           set { m_Notes = value; }
       }

       public string Phone
       {
           get { return m_Phone; }
           set { m_Phone = value; }
       }

       public string Phone2
       {
           get { return m_Phone2; }
           set { m_Phone2 = value; }
       }

       public string Phone3
       {
           get { return m_Phone3; }
           set { m_Phone3 = value; }
       }

       public string Phone4
       {
           get { return m_Phone4; }
           set { m_Phone4 = value; }
       }

       public string PriceLevelNumber
       {
           get { return m_PriceLevelNumber; }
           set { m_PriceLevelNumber = value; }
       }
        
       public string Salutation
       {
           get { return m_Salutation; }
           set { m_Salutation = value; }
       }

       public string StoreExchangeStatus
       {
           get { return m_StoreExchangeStatus; }
           set { m_StoreExchangeStatus = value; }
       }

       public string TaxCategory
       {
           get { return m_TaxCategory; }
           set { m_TaxCategory = value; }
       }
        
       public string WebNumber
       {
           get { return m_WebNumber; }
           set { m_WebNumber = value; }
       }

       public string BillCity
       {
           get { return m_BillCity; }
           set { m_BillCity = value; }
       }

      public string BillCountry
       {
           get { return m_BillCountry; }
           set { m_BillCountry = value; }
       }

      public string BillPostalCode
      {
          get { return m_BillPostalCode; }
          set { m_BillPostalCode = value; }
      }
     
      public  string BillState
      {
          get { return m_BillState; }
          set { m_BillState = value; }
      }      

      public string BillStreet
      {
          get { return m_BillStreet; }
          set { m_BillStreet = value; }
      }
      
      public string BillStreet2
      {
          get { return m_BillStreet2; }
          set { m_BillStreet2 = value; }
      }

      public string DefaultShipAddress
      {
          get { return m_DefaultShipAddress; }
          set { m_DefaultShipAddress = value; }
      }
      public string ShipAddressName
      {
          get { return m_ShipAddressName; }
          set { m_ShipAddressName = value; }
      }
      

      public string ShipCompanyName
      {
          get { return m_ShipCompanyName; }
          set { m_ShipCompanyName = value; }
      }
      

      public string ShipFullName
      {
          get { return m_ShipFullName; }
          set { m_ShipFullName = value; }
      }
      
      public string ShipCity
      {
          get { return m_ShipCity; }
          set { m_ShipCity = value; }
      }
      
      public string ShipCountry
      {
          get { return m_ShipCountry; }
          set { m_ShipCountry = value; }
      }
      
      public string ShipPostalCode
      {
          get { return m_ShipPostalCode; }
          set { m_ShipPostalCode = value; }
      }
     
      public string ShipState
      {
          get { return m_ShipState; }
          set { m_ShipState = value; }
      }
      
      public string ShipStreet
      {
          get { return m_ShipStreet; }
          set { m_ShipStreet = value; }
      }
      
      public string ShipStreet2
      {
          get { return m_ShipStreet2; }
          set { m_ShipStreet2 = value; }
      }

      public string OwnerID
      {
          get { return m_OwnerID; }
          set { m_OwnerID = value; }
      }

      public string[] DataExtName = new string[20];
     
      public string DataExtType
      {
          get { return m_DataExtType; }
          set { m_DataExtType = value; }
      }  
     
      public string[] DataExtValue = new string[20];

       #endregion
    }
    

    public class POSCustomerListCollection : Collection<POSCustomerList>
    {
        #region public method
        string XmlFileData = string.Empty;
        public List<string> GetAllPOSCustomerList(string QBFileName)
        {
            string customerXmlList = QBCommonUtilities.GetListFromQBPOS("CustomerQueryRq", QBFileName);

            List<string> customerList = new List<string>();
            List<string> customerList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);

            XmlFileData = customerXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList CustomerNodeList = outputXMLDoc.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

            foreach (XmlNode custNodes in CustomerNodeList)
            {
                    if (bkWorker.CancellationPending != true)
                    {
                        foreach (XmlNode node in custNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                if (!customerList.Contains(node.InnerText))
                                {
                                    customerList.Add(node.InnerText);
                                    string name = node.InnerText + "     :     Customer";
                                    customerList1.Add(name);
                                }
                            }
                        }
                    }
              }      

            CommonUtilities.GetInstance().CustomerListWithType = customerList1;

            return customerList;

        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<POSCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Customer List from QuickBooks.

            //Getting customer list from QuickBooks.
            string customerXmlList = string.Empty;

            customerXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("CustomerQueryRq", QBFileName, FromDate, ToDate);  
            POSCustomerList CustomerList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Axis 8.0; declare integer i
            int i = 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();            

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;

                #region Getting Employee Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }
                            
                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else
                                {
                                    try
                                    {
                                        CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;                                
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2= node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    
                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText; 
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }
                           
                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }

                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfCustomer.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }
     
         public Collection<POSCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, string ToDate, List<string> customerName)
        {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }
            #region Getting Customer List from QuickBooks.

            //Getting customer list from QuickBooks.
            string customerXmlList = string.Empty;

            customerXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("CustomerQueryRq", QBFileName, FromDate, ToDate,firstname,lastname);  
            POSCustomerList CustomerList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Axis 8.0; declare integer i
            int i = 0;
            int count= 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();            

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;

                #region Getting Employee Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }
                            
                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else
                                {
                                    try
                                    {
                                        CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;                                
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2= node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    
                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfCustomer.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        public Collection<POSCustomerList> PopulateCustomerList(string QBFileName)
        {
              POSCustomerList CustomerList;
            string customerXmlList = QBCommonUtilities.GetListFromQBPOS("CustomerQueryRq", QBFileName);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());
           
            int count= 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();            

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;



                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else
                                {
                                    try
                                    {
                                        CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return this;
        }

        public Collection<POSCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            POSCustomerList CustomerList;

            string customerXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("CustomerQueryRq", QBFileName, FromDate, ToDate);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

            int count = 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;



                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else{
                                try
                                {
                                    CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                }
                                catch
                                { }
                                    }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return this;
        }

        public Collection<POSCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> customerName)
        {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }
            POSCustomerList CustomerList;
            string customerXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("CustomerQueryRq", QBFileName, FromDate, ToDate, firstname, lastname);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

            int count = 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;



                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else
                                {
                                    try
                                    {
                                        CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return this;
        }


        public Collection<POSCustomerList> PopulateCustomerList(string QBFileName, List<string> customerName)
        {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }
            POSCustomerList CustomerList;
            string customerXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("CustomerQueryRq", QBFileName, firstname, lastname);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

            int count = 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCustomer = new XmlDocument();

            if (customerXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCustomer.LoadXml(customerXmlList);
                XmlFileData = customerXmlList;
                
                //Getting Invoices values from QuickBooks response.
                XmlNodeList CustomerNodeList = outputXMLDocOfCustomer.GetElementsByTagName(QBPOSCustomerList.CustomerRet.ToString());

                foreach (XmlNode CustomerNodes in CustomerNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        CustomerList = new POSCustomerList();
                        foreach (XmlNode node in CustomerNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPOSCustomerList.ListID.ToString()))
                            {
                                CustomerList.ListID = node.InnerText;
                            }

                            //Checking TimeCreated
                            if (node.Name.Equals(QBPOSCustomerList.TimeCreated.ToString()))
                            {
                                CustomerList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified
                            if (node.Name.Equals(QBPOSCustomerList.TimeModified.ToString()))
                            {
                                CustomerList.TimeModified = node.InnerText;
                            }

                            ////Checking AccountBalance
                            //if (node.Name.Equals(QBPOSCustomerList.AccountBalance.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountBalance[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AccountLimit.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AccountLimit[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}
                            //if (node.Name.Equals(QBPOSCustomerList.AmountPastDue.ToString()))
                            //{
                            //    try
                            //    {
                            //        CustomerList.AmountPastDue[count] = Convert.ToDecimal(node.InnerText);
                            //    }
                            //    catch
                            //    { }
                            //}

                            //Checking CompanyName
                            if (node.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                            {
                                CustomerList.CompanyName = node.InnerText;
                            }

                            //Checking CustomerID
                            if (node.Name.Equals(QBPOSCustomerList.CustomerID.ToString()))
                            {
                                CustomerList.CustomerID = node.InnerText;
                            }

                            //Checking CustomerDiscPercent
                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString()))
                            {
                                if (node.Name.Equals(QBPOSCustomerList.CustomerDiscPercent.ToString() == null))
                                {
                                }
                                else
                                {
                                    try
                                    {
                                        CustomerList.CustomerDiscPercent = Convert.ToDecimal(node.InnerText);
                                    }
                                    catch
                                    { }
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerDiscType.ToString()))
                            {
                                CustomerList.CustomerDiscType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.CustomerType.ToString()))
                            {
                                CustomerList.CustomerType = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.EMail.ToString()))
                            {
                                CustomerList.EMail = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsOkToEMail.ToString()))
                            {
                                CustomerList.IsOkToEMail = node.InnerText;
                            }


                            if (node.Name.Equals(QBPOSCustomerList.FirstName.ToString()))
                            {
                                CustomerList.FirstName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                            {
                                CustomerList.FullName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsAcceptingChecks.ToString()))
                            {
                                CustomerList.IsAcceptingChecks = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingChargeAccount.ToString()))
                            {
                                CustomerList.IsUsingChargeAccount = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsUsingWithQB.ToString()))
                            {
                                CustomerList.IsUsingWithQB = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsRewardsMember.ToString()))
                            {
                                CustomerList.IsRewardsMember = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.IsNoShipToBilling.ToString()))
                            {
                                CustomerList.IsNoShipToBilling = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastName.ToString()))
                            {
                                CustomerList.LastName = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.LastSale.ToString()))
                            {
                                CustomerList.LastSale = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Notes.ToString()))
                            {
                                CustomerList.Notes = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone.ToString()))
                            {
                                CustomerList.Phone = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone2.ToString()))
                            {
                                CustomerList.Phone2 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone3.ToString()))
                            {
                                CustomerList.Phone3 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Phone4.ToString()))
                            {
                                CustomerList.Phone4 = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.PriceLevelNumber.ToString()))
                            {
                                CustomerList.PriceLevelNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.Salutation.ToString()))
                            {
                                CustomerList.Salutation = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.StoreExchangeStatus.ToString()))
                            {
                                CustomerList.StoreExchangeStatus = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.TaxCategory.ToString()))
                            {
                                CustomerList.TaxCategory = node.InnerText;
                            }

                            if (node.Name.Equals(QBPOSCustomerList.WebNumber.ToString()))
                            {
                                CustomerList.WebNumber = node.InnerText;
                            }
                            //Checking BillAddress
                            if (node.Name.Equals(QBPOSCustomerList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.BillCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.BillCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.BillPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.BillState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.BillStreet = childNode.InnerText;
                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.BillStreet2 = childNode.InnerText;
                                }
                            }

                            //Checking DefaultShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.DefaultShipAddress.ToString()))
                            {
                                CustomerList.DefaultShipAddress = node.InnerText;
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBPOSCustomerList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBPOSCustomerList.AddressName.ToString()))
                                        CustomerList.ShipAddressName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.CompanyName.ToString()))
                                        CustomerList.ShipCompanyName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.FullName.ToString()))
                                        CustomerList.ShipFullName = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.City.ToString()))
                                        CustomerList.ShipCity = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Country.ToString()))
                                        CustomerList.ShipCountry = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.PostalCode.ToString()))
                                        CustomerList.ShipPostalCode = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.State.ToString()))
                                        CustomerList.ShipState = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street.ToString()))
                                        CustomerList.ShipStreet = childNode.InnerText;

                                    if (childNode.Name.Equals(QBPOSCustomerList.Street2.ToString()))
                                        CustomerList.ShipStreet2 = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBPOSCustomerList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CustomerList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CustomerList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(CustomerList);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            return this;
        }

        public  string GetXmlFileData()
        {
            return XmlFileData;
        }

        #endregion

    }
}



