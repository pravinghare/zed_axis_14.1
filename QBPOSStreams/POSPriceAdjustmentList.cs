﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
  public  class POSPriceAdjustmentList : POS_BasePriceAdjustmentList
    { 
      #region Public Properties
      public string TxnID
       {
           get { return m_TxnID; }
           set { m_TxnID = value; }
       }
    
       public string TimeCreated
       {
           get { return m_TimeCreated; }
           set { m_TimeCreated = value; }
       }

       public string DateApplied
       {
           get { return m_DateApplied; }
           set { m_DateApplied = value; }
       }

       public string DateRestored
       {
           get { return m_DateRestored; }
           set { m_DateRestored = value; }
       }

       public string TimeModified
       {
           get { return m_TimeModified; }
           set { m_TimeModified = value; }
       }
            
       public string PriceAdjustmentName
       {
           get { return m_PriceAdjustmentName; }
           set { m_PriceAdjustmentName = value; }
       }

       public string Comments
       {
           get { return m_Comments; }
           set { m_Comments = value; }
       }

       public string Associate
       {
           get { return m_Associate; }
           set { m_Associate = value; }
       }

       public string AppliedBy
       {
           get { return m_AppliedBy; }
           set { m_AppliedBy = value; }
       }

       public string RestoredBy
       {
           get { return m_RestoredBy; }
           set { m_RestoredBy = value; }
       }

       public string ItemsCount
      {
          get { return m_ItemsCount; } 
          set { m_ItemsCount = value; }
      }  
   
       public string PriceAdjustmentStatus
      {
          get { return m_PriceAdjustmentStatus; }
          set { m_PriceAdjustmentStatus = value; }
      }  

       public string PriceLevelNumber
      {
          get { return m_PriceLevelNumber; }
          set { m_PriceLevelNumber = value; }
      }  

       public string StoreExchangeStatus
      {
          get { return m_StoreExchangeStatus; }
          set { m_StoreExchangeStatus = value; }
      }  

       public string LineListID
      {
          get { return m_ListID; }
          set { m_ListID = value; }
      }  

       public string LineTxnLineID
      {
          get { return m_TxnLineID; }
          set { m_TxnLineID = value; }
      }  

       public decimal LineNewPrice
      {
          get { return m_NewPrice; }
          set { m_NewPrice = value; }
      }  

       public decimal LineOldPrice
      {
          get { return m_OldPrice; }
          set { m_OldPrice = value; }
      }  

       public decimal LineOldCost
      {
          get { return m_OldCost; }
          set { m_OldCost = value; }
      }  
     
       #endregion  
    }

  public class POSPriceAdjustmentCollection : Collection<POSPriceAdjustmentList>
  {
      string XmlFileData = string.Empty;

      #region private method
      /// <summary>
      /// Assigning the xml value to the class objects
      /// </summary>
      /// <param name="employeeXmlList"></param>
      /// <returns></returns>
      private Collection<POSPriceAdjustmentList> GetPriceAdjustmentList(string PriceAdjustmentXmlList)
      {
          //Create a xml document for output result.
          XmlDocument outputXMLDocOfPriceAdjustment = new XmlDocument();

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

          if (PriceAdjustmentXmlList != string.Empty)
          {
              //Loading Item List into XML.
              outputXMLDocOfPriceAdjustment.LoadXml(PriceAdjustmentXmlList);
              XmlFileData = PriceAdjustmentXmlList;

              //Getting employee values from QuickBooks response.
              XmlNodeList PriceAdjustmentNodeList = outputXMLDocOfPriceAdjustment.GetElementsByTagName(QBPOSPriceAdjustmentList.PriceAdjustmentRet.ToString());

              foreach (XmlNode PriceAdjustmentNodes in PriceAdjustmentNodeList)
              {
                  if (bkWorker.CancellationPending != true)
                  {
                      POSPriceAdjustmentList priceAdjustment = new POSPriceAdjustmentList();
                      foreach (XmlNode node in PriceAdjustmentNodes.ChildNodes)
                      {
                          //Checking ListID. 
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.TxnID.ToString()))
                          {
                              priceAdjustment.TxnID = node.InnerText;
                          }

                          //Checking TimeCreated
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.TimeCreated.ToString()))
                          {
                              priceAdjustment.TimeCreated = node.InnerText;
                          }

                          //Checking TimeModified
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.TimeModified.ToString()))
                          {
                              priceAdjustment.TimeModified = node.InnerText;
                          }
                          //Checking City. 
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.DateApplied.ToString()))
                          {
                              priceAdjustment.DateApplied = node.InnerText;
                          }
                          //Checking CommissionPercent
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.DateRestored.ToString()))
                          {
                                priceAdjustment.DateRestored = node.InnerText;
                              
                          }
                          //Checking Country
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.PriceAdjustmentName.ToString()))
                          {
                              priceAdjustment.PriceAdjustmentName = node.InnerText;
                          }

                          //Checking Email
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.Comments.ToString()))
                          {
                              priceAdjustment.Comments = node.InnerText;
                          }

                          //Checking FirstName
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.Associate.ToString()))
                          {
                              priceAdjustment.Associate = node.InnerText;
                          }

                          //Checking FirstName
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.AppliedBy.ToString()))
                          {
                              priceAdjustment.AppliedBy = node.InnerText;
                          }
                          //Checking IsTrackingHours
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.RestoredBy.ToString()))
                          {
                              priceAdjustment.RestoredBy = node.InnerText;
                          }

                          //Checking LastName
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.ItemsCount.ToString()))
                          {
                              priceAdjustment.ItemsCount = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.PriceAdjustmentStatus.ToString()))
                          {
                              priceAdjustment.PriceAdjustmentStatus = node.InnerText;
                          }
                          //Checking LoginName
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.PriceLevelNumber.ToString()))
                          {
                              priceAdjustment.PriceLevelNumber = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSPriceAdjustmentList.StoreExchangeStatus.ToString()))
                          {
                              priceAdjustment.StoreExchangeStatus = node.InnerText;
                          }

                          //Checking PriceAdjustmentItemRet
                          if (node.Name.Equals(QBPOSPriceAdjustmentList.PriceAdjustmentItemRet.ToString()))
                          {
                              //Getting values of address.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {

                                  if (childNode.Name.Equals(QBPOSPriceAdjustmentList.LineListID.ToString()))
                                      priceAdjustment.LineListID = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSPriceAdjustmentList.LineTxnLineID.ToString()))
                                      priceAdjustment.LineTxnLineID = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSPriceAdjustmentList.LineNewPrice.ToString()))
                                      priceAdjustment.LineNewPrice = Convert.ToDecimal(childNode.InnerText.ToString());
                                  if (childNode.Name.Equals(QBPOSPriceAdjustmentList.LineOldPrice.ToString()))
                                      priceAdjustment.LineOldPrice = Convert.ToDecimal(childNode.InnerText.ToString());
                                  if (childNode.Name.Equals(QBPOSPriceAdjustmentList.LineOldCost.ToString()))
                                      priceAdjustment.LineOldCost = Convert.ToDecimal(childNode.InnerText.ToString());
                                 
                              }
                          }

                      }
                      this.Add(priceAdjustment);
                  }
                  else
                  {
                      return null;
                  }
              }
              outputXMLDocOfPriceAdjustment.RemoveAll();
          }
          return this;
      }
      #endregion

      #region public Method
      /// <summary>
      /// if no filter was selected
      /// </summary>
      /// <param name="QBFileName"></param> 
      /// <returns></returns>
      /// 
      public Collection<POSPriceAdjustmentList> PopulatePriceAdjustmentList(string QBFileName)
      {
          string priceAdjustmentList = QBCommonUtilities.GetListFromQBPOS("PriceAdjustmentQueryRq", QBFileName);
          return GetPriceAdjustmentList(priceAdjustmentList);
      }

      public Collection<POSPriceAdjustmentList> PopulatePriceAdjustmentList(string QBFileName, DateTime FromDate, string ToDate)
      {
          string priceAdjustmentList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PriceAdjustmentQueryRq", QBFileName, FromDate, ToDate);
          return GetPriceAdjustmentList(priceAdjustmentList);
      }
      public Collection<POSPriceAdjustmentList> PopulatePriceAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate)
      {
          string priceAdjustmentList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PriceAdjustmentQueryRq", QBFileName, FromDate, ToDate);
          return GetPriceAdjustmentList(priceAdjustmentList);
      }
      /// <summary>
      /// This method is used to get the xml response from the QuikBooks.
      /// </summary>
      /// <returns></returns>
      public string GetXmlFileData()
      {
          return XmlFileData;
      }
      #endregion
  }
}
 