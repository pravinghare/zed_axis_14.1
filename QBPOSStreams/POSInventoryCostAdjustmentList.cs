﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;
namespace QBPOSStreams
{
   public  class POSInventoryCostAdjustmentList:POS_BaseInventoryCostAdjustmentList
   {

       public POSInventoryCostAdjustmentList()
        {
            m_InventoryCostAdjustmentDetails = new Collection<QBInventoryCostAdjustmentItem>();
        }
       #region properties
       public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
       

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
      
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
       

        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }
        
        public decimal?[] CostDifference
        {
            get { return m_CostDifference; }
            set { m_CostDifference = value; }
        }
        
        public string HistoryDocStatus
        {
            get { return m_HistoryDocStatus; }
            set { m_HistoryDocStatus = value; }
        }
        
        public string InventoryAdjustmentNumber
        {
            get { return m_InventoryAdjustmentNumber; }
            set { m_InventoryAdjustmentNumber = value; }
        }
        
        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }
       
        public decimal?[] NewCost
        {
            get { return m_NewCost; }
            set { m_NewCost = value; }
        }
       
        public decimal?[] OldCost
        {
            get { return m_OldCost; }
            set { m_OldCost = value; }
        }
       
        public string Reason
        {
            get { return m_Reason; }
            set { m_Reason = value; }
        }
       
        public string StoreExchangeStatus
        {
            get { return m_StoreExchangeStatus; }
            set { m_StoreExchangeStatus = value; }
        }
       
        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
      
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
      
        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }
        

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

        public Collection<Streams.QBInventoryCostAdjustmentItem> InventoryCostAdjustmentItemsDetails
        {
            get { return m_InventoryCostAdjustmentDetails; }
            set { m_InventoryCostAdjustmentDetails = value; }
        }


       #endregion
   }


   public class POSInventoryCostAdjustmentCollection : Collection<POSInventoryCostAdjustmentList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="InventoryCostAdjustmentXmlList"></param>
       /// <returns></returns>
       private Collection<POSInventoryCostAdjustmentList> GetInventoryCostAdjustmentList(string InventoryCostAdjustmentXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfInventoryCostAdjustment = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
           QBInventoryCostAdjustmentItem itemList = null;
           if (InventoryCostAdjustmentXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfInventoryCostAdjustment.LoadXml(InventoryCostAdjustmentXmlList);
               XmlFileData = InventoryCostAdjustmentXmlList;

               //Getting Sales Order values from QuickBooks response.
               XmlNodeList InventoryCostAdjustmentNodeList = outputXMLDocOfInventoryCostAdjustment.GetElementsByTagName(QBPOSInventoryCostAdjustmentList.InventoryCostAdjustmentRet.ToString());

               foreach (XmlNode InventoryCostAdjustmentNodes in InventoryCostAdjustmentNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                       int count = 0;                     
                      
                       POSInventoryCostAdjustmentList InventoryCostAdjustment = new POSInventoryCostAdjustmentList();


                       foreach (XmlNode node in InventoryCostAdjustmentNodes.ChildNodes)
                       {
                           //Assinging values to class.

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.TxnID.ToString()))
                               InventoryCostAdjustment.TxnID = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.TimeCreated.ToString()))
                               InventoryCostAdjustment.TimeCreated = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.TimeModified.ToString()))
                               InventoryCostAdjustment.TimeModified = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.Associate.ToString()))
                               InventoryCostAdjustment.Associate = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.Comments.ToString()))
                               InventoryCostAdjustment.Comments = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.CostDifference.ToString()))
                               InventoryCostAdjustment.CostDifference[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.HistoryDocStatus.ToString()))
                               InventoryCostAdjustment.HistoryDocStatus = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.ItemsCount.ToString()))
                               InventoryCostAdjustment.ItemsCount = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.InventoryAdjustmentNumber.ToString()))
                               InventoryCostAdjustment.InventoryAdjustmentNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.NewCost.ToString()))
                               InventoryCostAdjustment.NewCost[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.OldCost.ToString()))
                               InventoryCostAdjustment.OldCost[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.Reason.ToString()))
                               InventoryCostAdjustment.Reason = node.InnerText;


                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.StoreExchangeStatus.ToString()))
                               InventoryCostAdjustment.StoreExchangeStatus = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.StoreNumber.ToString()))
                               InventoryCostAdjustment.StoreNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.TxnDate.ToString()))
                               InventoryCostAdjustment.TxnDate = node.InnerText;
                         
                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.TxnState.ToString()))
                               InventoryCostAdjustment.TxnState = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.Workstation.ToString()))
                               InventoryCostAdjustment.Workstation = node.InnerText;

                           if (node.Name.Equals(QBPOSInventoryCostAdjustmentList.InventoryCostAdjustmentItemRet.ToString()))
                           {
                               itemList = new QBInventoryCostAdjustmentItem();
                               foreach (XmlNode childNode in node.ChildNodes)
                               {

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.ListID.ToString()))
                                       itemList.ListID[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.CostDifference.ToString()))
                                       itemList.LineCostDifference[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.NumberOfBaseUnits.ToString()))
                                       itemList.NumberOfBaseUnits[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.NewCost.ToString()))
                                       itemList.LineNewCost[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.OldCost.ToString()))
                                       itemList.LineOldCost[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSInventoryCostAdjustmentList.UnitOfMeasure.ToString()))
                                       itemList.UnitOfMeasure[count] = childNode.InnerText;
                               }
                               count++;
                               InventoryCostAdjustment.InventoryCostAdjustmentItemsDetails.Add(itemList);
                           }                             
                   
                       }

                       this.Add(InventoryCostAdjustment);
                   }
                   else
                   { return null; }

               }
               //Removing all the references from OutPut Xml document.
               outputXMLDocOfInventoryCostAdjustment.RemoveAll();
           }

           return this;
       }
       #endregion

       #region public Method
       /// <summary>
       /// if no filter was selected
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <returns></returns>
       public Collection<POSInventoryCostAdjustmentList> PopulateInventoryCostAdjustmentList(string QBFileName)
       {
           string InventoryCostAdjustmentXmlList = QBCommonUtilities.GetListFromQBPOS("InventoryCostAdjustmentQueryRq", QBFileName);
           return GetInventoryCostAdjustmentList(InventoryCostAdjustmentXmlList);
       }

       public Collection<POSInventoryCostAdjustmentList> PopulateInventoryCostAdjustmentList(string QBFileName, DateTime FromDate, string ToDate)
       {
           string InventoryCostAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("InventoryCostAdjustmentQueryRq", QBFileName, FromDate, ToDate);
           return GetInventoryCostAdjustmentList(InventoryCostAdjustmentXmlList);
       }
       public Collection<POSInventoryCostAdjustmentList> PopulateInventoryCostAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate)
       {
           string InventoryCostAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("InventoryCostAdjustmentQueryRq", QBFileName, FromDate, ToDate);
           return GetInventoryCostAdjustmentList(InventoryCostAdjustmentXmlList);
       }

       /// <summary>
       /// This method is used to get the xml response from the QuikBooks.
       /// </summary>
       /// <returns></returns>
       public string GetXmlFileData()
       {
           return XmlFileData;
       }

       #endregion
   }
}
