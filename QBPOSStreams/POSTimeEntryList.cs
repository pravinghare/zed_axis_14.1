﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;
using System.Runtime.CompilerServices;

namespace QBPOSStreams
{
   public  class POSTimeEntryList:POS_BaseTimeEntryList
    {       

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }
       
        public string ClockInTime
        {
            get { return m_ClockInTime; }
            set { m_ClockInTime = value; }
        }       

        public string ClockOutTime
        {
            get { return m_ClockOutTime; }
            set { m_ClockOutTime = value; }
        }
       
        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }
       
        public string EmployeeListID
        {
            get { return m_EmployeeListID; }
            set { m_EmployeeListID = value; }
        }
      
        public string EmployeeLoginName
        {
            get { return m_EmployeeLoginName; }
            set { m_EmployeeLoginName = value; }
        }
       
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
       

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }       

        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
    }

   public class POSTimeEntryCollection : Collection<POSTimeEntryList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="employeeXmlList"></param>
       /// <returns></returns>
       private Collection<POSTimeEntryList> GetTimeEntryList(string timeEntryXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

           if (timeEntryXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfTimeEntry.LoadXml(timeEntryXmlList);
               XmlFileData = timeEntryXmlList;

               //Getting employee values from QuickBooks response.
               XmlNodeList TimeEntryNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBPOSTimeEntryList.TimeEntryRet.ToString());

               foreach (XmlNode TimeEntryNodes in TimeEntryNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                       POSTimeEntryList timeEntry = new POSTimeEntryList();
                       foreach (XmlNode node in TimeEntryNodes.ChildNodes)
                       {
                           //Checking ListID. 
                           if (node.Name.Equals(QBPOSTimeEntryList.ListID.ToString()))
                           {
                               timeEntry.ListID = node.InnerText;
                           }

                           //Checking ClockInTime
                           if (node.Name.Equals(QBPOSTimeEntryList.ClockInTime.ToString()))
                           {
                               timeEntry.ClockInTime = node.InnerText;
                           }

                           //Checking ClockOutTime
                           if (node.Name.Equals(QBPOSTimeEntryList.ClockOutTime.ToString()))
                           {
                               timeEntry.ClockOutTime = node.InnerText;
                           }
                           //Checking CreatedBy. 
                           if (node.Name.Equals(QBPOSTimeEntryList.CreatedBy.ToString()))
                           {
                               timeEntry.CreatedBy = node.InnerText;
                           }

                           //Checking EmployeeListID
                           if (node.Name.Equals(QBPOSTimeEntryList.EmployeeListID.ToString()))
                           {
                               timeEntry.EmployeeListID = node.InnerText;
                           }

                           //Checking EmployeeLoginName
                           if (node.Name.Equals(QBPOSTimeEntryList.EmployeeLoginName.ToString()))
                           {
                               timeEntry.EmployeeLoginName = node.InnerText;
                           }
                         

                           //Checking FirstName
                           if (node.Name.Equals(QBPOSTimeEntryList.FirstName.ToString()))
                           {
                               timeEntry.FirstName = node.InnerText;
                           }
                           
                           //Checking LastName
                           if (node.Name.Equals(QBPOSTimeEntryList.LastName.ToString()))
                           {
                               timeEntry.LastName = node.InnerText;
                           }
                           //Checking StoreNumber
                           if (node.Name.Equals(QBPOSTimeEntryList.StoreNumber.ToString()))
                           {
                               timeEntry.StoreNumber = node.InnerText;
                           }
                         

                       }
                       this.Add(timeEntry);
                   }
                   else
                   {
                       return null;
                   }
               }
               outputXMLDocOfTimeEntry.RemoveAll();
           }
           return this;
       }
       #endregion

       #region public Method
       /// <summary>
       /// if no filter was selected
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <returns></returns>
       /// 
   

       public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName)
       {
           string TimeEntryXmlList = QBCommonUtilities.GetListFromQBPOS("TimeEntryQueryRq", QBFileName);
           return GetTimeEntryList(TimeEntryXmlList);
       }

       public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName, DateTime FromDate, string ToDate)
       {
           string TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("TimeEntryQueryRq", QBFileName, FromDate, ToDate);
           return GetTimeEntryList(TimeEntryXmlList);
       }
       public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName, DateTime FromDate, DateTime ToDate)
       {
           string TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("TimeEntryQueryRq", QBFileName, FromDate, ToDate);
           return GetTimeEntryList(TimeEntryXmlList);
       }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName, List<string> customerName)
        {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "All")
                    {
                        firstname.Clear();
                        lastname.Clear();
                        break;
                    }
                    else if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }

            string TimeEntryXmlList = string.Empty;

            if (firstname.Count == 0 && lastname.Count == 0)
            {
                 TimeEntryXmlList = QBCommonUtilities.GetListFromQBPOS("TimeEntryQueryRq", QBFileName);
            }
            else
            {
                 TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("TimeEntryQueryRq", QBFileName, firstname, lastname);
            }
            return GetTimeEntryList(TimeEntryXmlList);
        }

       public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName, DateTime FromDate, DateTime ToDate,  List<string> customerName)
       {
            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                a = 0;

                string[] array_msg = item.ToString().Split(delimiters);
                foreach (string s in array_msg)
                {
                    if (s == "Ms." || s == "Mrs." || s == "Mr.")
                    {
                    }
                    else
                    {
                        if (a == 0)
                        {
                            firstname.Add(s);
                            a = 1;
                        }
                        else if (a == 1)
                        {
                            lastname.Add(s);
                            a = 0;
                        }
                        //a++;
                    }
                }
            }

           string TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("TimeEntryQueryRq", QBFileName, FromDate, ToDate, firstname, lastname);
           return GetTimeEntryList(TimeEntryXmlList);
       }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public Collection<POSTimeEntryList> PopulateTimeEntryList(string QBFileName, DateTime FromDate, string ToDate, List<string> customerName)
       {

            int a = 0;
            List<string> firstname = new List<string>();
            List<string> lastname = new List<string>();

            char[] delimiters = new char[] { ' ' };
            foreach (var item in customerName)
            {
                if (item == "All")
                {
                    firstname.Clear();
                    lastname.Clear();
                    break;
                }
                else
                {
                    a = 0;

                    string[] array_msg = item.ToString().Split(delimiters);
                    foreach (string s in array_msg)
                    {
                        if (s == "Ms." || s == "Mrs." || s == "Mr.")
                        {
                        }
                        else
                        {
                            if (a == 0)
                            {
                                firstname.Add(s);
                                a = 1;
                            }
                            else if (a == 1)
                            {
                                lastname.Add(s);
                                a = 0;
                            }
                            //a++;
                        }
                    }
                }
            }

           string TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("TimeEntryQueryRq", QBFileName, FromDate, ToDate, firstname, lastname);
           return GetTimeEntryList(TimeEntryXmlList);
       }
       /// <summary>
       /// This method is used to get the xml response from the QuikBooks.
       /// </summary>
       /// <returns></returns>
       public string GetXmlFileData()
       {
           return XmlFileData;
       }
       #endregion
   }
}
