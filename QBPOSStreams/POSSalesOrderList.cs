﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
  public class POSSalesOrderList :POS_BaseSalesOrderList
  {
      public POSSalesOrderList()
        {
            m_QBSalesOrderItemDetails = new Collection<QBSalesOrderItem>();
        }

        #region  properties
      public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }
       

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
       

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
       
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
       

        public string Cashier
        {
            get { return m_Cashier; }
            set { m_Cashier = value; }
        }
       

        public string CustomerListID
        {
            get { return m_CustomerListID; }
            set { m_CustomerListID = value; }
        }
       

        public string DepositBalance
        {
            get { return m_DepositBalance; }
            set { m_DepositBalance = value; }
        }
       

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }
       
        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
        

        public string Instructions
        {
            get { return m_Instructions; }
            set { m_Instructions = value; }
        }
       

        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }
       

        public string PriceLevelNumber
        {
            get { return m_PriceLevelNumber; }
            set { m_PriceLevelNumber = value; }
        }
       
        public string PromoCode
        {
            get { return m_PromoCode; }
            set { m_PromoCode = value; }
        }
       

        public string Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }
       

        public string SalesOrderNumber
        {
            get { return m_SalesOrderNumber; }
            set { m_SalesOrderNumber = value; }
        }
       
        public string SalesOrderStatusDesc
        {
            get { return m_SalesOrderStatusDesc; }
            set { m_SalesOrderStatusDesc = value; }
        }
       

        public string SalesOrderType
        {
            get { return m_SalesOrderType; }
            set { m_SalesOrderType = value; }
        }
        

        public string Subtotal
        {
            get { return m_Subtotal; }
            set { m_Subtotal = value; }
        }
      
        public string TaxAmount
        {
            get { return m_TaxAmount; }
            set { m_TaxAmount = value; }
        }
       

        public string TaxCategory
        {
            get { return m_TaxCategory; }
            set { m_TaxCategory = value; }
        }
      

        public string TaxPercentage
        {
            get { return m_TaxPercentage; }
            set { m_TaxPercentage = value; }
        }
       
        public string Total
        {
            get { return m_Total; }
            set { m_Total = value; }
        }        

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }             

        public string ShipAddressName
        {
            get { return m_AddressName; }
            set { m_AddressName = value; }
        }


        public string ShipCity
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string ShipCompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string ShipCountry
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        public string ShipFullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

        public string ShipPhone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }


        public string ShipPhone2
        {
            get { return m_Phone2; }
            set { m_Phone2 = value; }
        }


        public string ShipPhone3
        {
            get { return m_Phone3; }
            set { m_Phone3 = value; }
        }

        public string ShipPhone4
        {
            get { return m_Phone4; }
            set { m_Phone4 = value; }
        }

        public string ShipPostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }


        public string ShipBy
        {
            get { return m_ShipBy; }
            set { m_ShipBy = value; }
        }
       

        public string Shipping
        {
            get { return m_Shipping; }
            set { m_Shipping = value; }
        }


        public string ShipState
        {
            get { return m_State; }
            set { m_State = value; }
        }


        public string ShipStreet
        {
            get { return m_Street; }
            set { m_Street = value; }
        }


        public string ShipStreet2
        {
            get { return m_Street2; }
            set { m_Street2 = value; }
        }
        public Collection<QBSalesOrderItem> SalesOrderItemDetails
        {
            get { return m_QBSalesOrderItemDetails; }
            set { m_QBSalesOrderItemDetails = value; }

        }
        //for purchase Order Line Rate
        public string[] TxnLineID
        {
            get { return m_SalesTxnLineID; }
            set { m_SalesTxnLineID = value; }
        }

     
        
      
      
#endregion
      
  }

  public class POSSalesOrderCollection : Collection<POSSalesOrderList>
  {
      string XmlFileData = string.Empty;

      #region private method
      /// <summary>
      /// Assigning the xml value to the class objects
      /// </summary>
      /// <param name="employeeXmlList"></param>
      /// <returns></returns>
      private Collection<POSSalesOrderList> GetSalesOrderList(string salesOrderXmlList)
      {
          //Create a xml document for output result.
          XmlDocument outputXMLDocOfSalesOrder = new XmlDocument();

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
          QBSalesOrderItem itemList = null;
          if (salesOrderXmlList != string.Empty)
          {
              //Loading Item List into XML.
              outputXMLDocOfSalesOrder.LoadXml(salesOrderXmlList);
              XmlFileData = salesOrderXmlList;

              //Getting employee values from QuickBooks response.
              XmlNodeList SalesOrderNodeList = outputXMLDocOfSalesOrder.GetElementsByTagName(QBPOSSalesOrderList.SalesOrderRet.ToString());

              foreach (XmlNode SalesOrderNodes in SalesOrderNodeList)
              {
                  if (bkWorker.CancellationPending != true)
                  {
                      int count = 0;                     
                      POSSalesOrderList salesorder = new POSSalesOrderList();                     
                      count = 0;
                     
                      salesorder.TxnLineID = new string[SalesOrderNodes.ChildNodes.Count];
                      

                      foreach (XmlNode node in SalesOrderNodes.ChildNodes)
                      {
                           
                          if (node.Name.Equals(QBPOSSalesOrderList.TxnID.ToString()))
                          {
                              salesorder.TxnID = node.InnerText;
                          }

                          
                          if (node.Name.Equals(QBPOSSalesOrderList.TimeCreated.ToString()))
                          {
                              salesorder.TimeCreated = node.InnerText;
                          }

                         
                          if (node.Name.Equals(QBPOSSalesOrderList.TimeModified.ToString()))
                          {
                              salesorder.TimeModified = node.InnerText;
                          }
                          
                          if (node.Name.Equals(QBPOSSalesOrderList.Associate.ToString()))
                          {
                              salesorder.Associate = node.InnerText;
                          }
                         
                          if (node.Name.Equals(QBPOSSalesOrderList.Cashier.ToString()))
                          {                              
                                  salesorder.Cashier = node.InnerText;                             
                          }
                         
                          if (node.Name.Equals(QBPOSSalesOrderList.CustomerListID.ToString()))
                          {
                              salesorder.CustomerListID = node.InnerText;
                          }

                         
                          if (node.Name.Equals(QBPOSSalesOrderList.DepositBalance.ToString()))
                          {
                              salesorder.DepositBalance = node.InnerText;
                          }
                                                  
                          if (node.Name.Equals(QBPOSSalesOrderList.Discount.ToString()))
                          {
                              salesorder.Discount = node.InnerText;
                          }


                          if (node.Name.Equals(QBPOSSalesOrderList.DiscountPercent.ToString()))
                          {
                              salesorder.DiscountPercent = node.InnerText;
                          }
                          
                          if (node.Name.Equals(QBPOSSalesOrderList.Instructions.ToString()))
                          {
                              salesorder.Instructions = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.ItemsCount.ToString()))
                          {
                              salesorder.ItemsCount = node.InnerText;
                          }
                       
                          if (node.Name.Equals(QBPOSSalesOrderList.PriceLevelNumber.ToString()))
                          {
                              salesorder.PriceLevelNumber = node.InnerText;
                          }
                          
                          if (node.Name.Equals(QBPOSSalesOrderList.PromoCode.ToString()))
                          {
                              salesorder.PromoCode = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.Qty.ToString()))
                          {
                              salesorder.Qty = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.SalesOrderNumber.ToString()))
                          {
                              salesorder.SalesOrderNumber = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.SalesOrderStatusDesc.ToString()))
                          {
                              salesorder.SalesOrderStatusDesc = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.SalesOrderType.ToString()))
                          {
                              salesorder.SalesOrderType = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.Subtotal.ToString()))
                          {
                              salesorder.Subtotal = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSSalesOrderList.TaxAmount.ToString()))
                          {
                              salesorder.TaxAmount = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSSalesOrderList.TaxCategory.ToString()))
                          {
                              salesorder.TaxCategory = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSSalesOrderList.TaxPercentage.ToString()))
                          {
                              salesorder.TaxPercentage = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSSalesOrderList.Total.ToString()))
                          {
                              salesorder.Total = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSSalesOrderList.TxnDate.ToString()))
                          {
                              salesorder.TxnDate = node.InnerText;
                          }
                          //Checking Shipping Information
                          if (node.Name.Equals(QBPOSSalesOrderList.ShippingInformation.ToString()))
                          {
                              //Getting values of address.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {
                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipAddressName.ToString()))
                                      salesorder.ShipAddressName = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipCity.ToString()))
                                      salesorder.ShipCity = childNode.InnerText;
                                  
                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipCompanyName.ToString()))
                                      salesorder.ShipCompanyName = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipCountry.ToString()))
                                      salesorder.ShipCountry = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipFullName.ToString()))
                                      salesorder.ShipFullName = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipPhone.ToString()))
                                      salesorder.ShipPhone = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipPhone2.ToString()))
                                      salesorder.ShipPhone2 = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipPhone3.ToString()))
                                      salesorder.ShipPhone3 = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipPhone4.ToString()))
                                      salesorder.ShipPhone4 = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipPostalCode.ToString()))
                                      salesorder.ShipPostalCode = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipBy.ToString()))
                                      salesorder.ShipBy = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Shipping.ToString()))
                                      salesorder.Shipping = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipState.ToString()))
                                      salesorder.ShipState = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipStreet.ToString()))
                                      salesorder.ShipStreet = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ShipStreet2.ToString()))
                                      salesorder.ShipStreet2 = childNode.InnerText;
                              }
                          }

                          //Checking Sales Order Item
                          if (node.Name.Equals(QBPOSSalesOrderList.SalesOrderItemRet.ToString()))
                          {
                              itemList = new QBSalesOrderItem();
                              //Getting values of address.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {
                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ListID.ToString()))
                                      itemList.ListID[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.TxnLineID.ToString()))
                                  {
                                      if (childNode.InnerText.Length > 36)
                                          salesorder.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                      else
                                          salesorder.TxnLineID[count] = childNode.InnerText;
                                  }
                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ALU.ToString()))
                                      itemList.SalesALU[count] = childNode.InnerText;

                                 
                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Associate.ToString()))
                                      itemList.SalesAssociate[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Attribute.ToString()))
                                      itemList.SalesAttribute[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Commission.ToString()))
                                      itemList.SalesCommission[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Desc1.ToString()))
                                      itemList.SalesDesc1[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Desc2.ToString()))
                                      itemList.SalesDesc2[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Discount.ToString()))
                                      itemList.SalesDiscount[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.DiscountPercent.ToString()))
                                      itemList.SalesDiscountPercent[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.DiscountType.ToString()))
                                      itemList.SalesDiscountType[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.DiscountSource.ToString()))
                                      itemList.SalesDiscountSource[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ExtendedPrice.ToString()))
                                      itemList.SalesExtendedPrice[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ExtendedTax.ToString()))
                                      itemList.SalesExtendedTax[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.ItemNumber.ToString()))
                                      itemList.SalesItemNumber[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.NumberOfBaseUnits.ToString()))
                                      itemList.SalesNumberOfBaseUnits[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Price.ToString()))
                                      itemList.SalesPrice[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.PriceLevelNumber.ToString()))
                                      itemList.SalesPriceLevelNumber[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Qty.ToString()))
                                      itemList.SalesQty[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.QtySold.ToString()))
                                      itemList.SalesQtySold[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.SerialNumber.ToString()))
                                      itemList.SalesSerialNumber[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Size.ToString()))
                                      itemList.SalesSize[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.TaxAmount.ToString()))
                                      itemList.SalesTaxAmount[count] = Convert.ToDecimal(childNode.InnerText);

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.TaxCode.ToString()))
                                      itemList.SalesTaxCode[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.TaxPercentage.ToString()))
                                      itemList.SalesTaxPercentage[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.UnitOfMeasure.ToString()))
                                      itemList.SalesUnitOfMeasure[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.UPC.ToString()))
                                      itemList.SalesUPC[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Manufacturer.ToString()))
                                      itemList.SalesManufacturer[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSSalesOrderList.Weight.ToString()))
                                      itemList.SalesWeight[count] = childNode.InnerText;
                              }
                              count++;
                              salesorder.SalesOrderItemDetails.Add(itemList);
                             
                          }                                                   
                     }
                      this.Add(salesorder);
                  }
                  else
                  {
                      return null;
                  }
              }
              outputXMLDocOfSalesOrder.RemoveAll();
          }
          return this;
      }
      #endregion

      #region public Method
      /// <summary>
      /// if no filter was selected
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <returns></returns>
      /// 
      public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName)
      {
          string SalesOrderXmlList = QBCommonUtilities.GetListFromQBPOS("SalesOrderQueryRq", QBFileName);
          return GetSalesOrderList(SalesOrderXmlList);
      }

      public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, string ToDate)
      {
          string SalesOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesOrderQueryRq", QBFileName, FromDate, ToDate);
          return GetSalesOrderList(SalesOrderXmlList);
      }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, string ToDate, List<string> customerName)
        {
            string SalesOrderXmlList = QBCommonUtilities.GetSalesOrderListFromQuickBookPOSByDate("SalesOrderQueryRq", QBFileName, FromDate, ToDate, customerName);
            return GetSalesOrderList(SalesOrderXmlList);
        }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
      {
          string SalesOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesOrderQueryRq", QBFileName, FromDate, ToDate);
          return GetSalesOrderList(SalesOrderXmlList);
      }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> customerName)
        {
            string SalesOrderXmlList = QBCommonUtilities.GetSalesOrderFromQuickBookPOSByDate("SalesOrderQueryRq", QBFileName, FromDate, ToDate, customerName);
            return GetSalesOrderList(SalesOrderXmlList);
        }


        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, string FromRefnum, string ToRefNum)
      {
          string SalesOrderXmlList = QBCommonUtilities.GetListFromQBPOS("SalesOrderQueryRq", QBFileName, FromRefnum, ToRefNum);
          return GetSalesOrderList(SalesOrderXmlList);
      }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, string FromRefnum, string ToRefNum, List<string> customerName)
        {
            string SalesOrderXmlList = QBCommonUtilities.GetSalesOrderListFromQBPOS("SalesOrderQueryRq", QBFileName, FromRefnum, ToRefNum, customerName);
            return GetSalesOrderList(SalesOrderXmlList);
        }
        
        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefNum)
      {
          string SalesOrderXmlList = QBCommonUtilities.GetListFromQBPOS("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefNum);
          return GetSalesOrderList(SalesOrderXmlList);
      }

      public Collection<POSSalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefNum)
      {
          string SalesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPOS("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefNum);
          return GetSalesOrderList(SalesOrderXmlList);
      }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, List<string> customerName)
        {
            string SalesOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesOrderQueryRq", QBFileName, customerName);
            return GetSalesOrderList(SalesOrderXmlList);
        }

        public Collection<POSSalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName, string fromRefNum, string toRefNum)
        {
            string SalesOrderXmlList = QBCommonUtilities.ModifiedListFromQuickBookPOS("SalesOrderQueryRq", QBFileName, FromDate, ToDate, companyName, fromRefNum, toRefNum);
            return GetSalesOrderList(SalesOrderXmlList);
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
      {
          return XmlFileData;
      }
      #endregion
  }

}
