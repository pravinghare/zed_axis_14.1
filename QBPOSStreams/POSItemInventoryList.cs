﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
  public class POSItemInventoryList:POS_BaseItemInventoryList
    {
        #region  Properties

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }

        public string Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }

        public string COGSAccount
        {
            get { return m_COGSAccount; }
            set { m_COGSAccount = value; }
        }

        public string Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public string DepartmentListID
        {
            get { return m_DepartmentListID; }
            set { m_DepartmentListID = value; }
        }
              
        public string Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }

        public string Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }

        public string IncomeAccount
        {
            get { return m_IncomeAccount; }
            set { m_IncomeAccount = value; }
        }

        public string IsEligibleForCommission
        {
            get { return m_IsEligibleForCommission; }
            set { m_IsEligibleForCommission = value; }
        }

        public string IsPrintingTags
        {
            get { return m_IsPrintingTags; }
            set { m_IsPrintingTags = value; }
        }

        public string IsUnorderable
        {
            get { return m_IsUnorderable; }
            set { m_IsUnorderable = value; }
        }

        public string IsEligibleForRewards
        {
            get { return m_IsEligibleForRewards; }
            set { m_IsEligibleForRewards = value; }
        }

        public string IsWebItem
        {
            get { return m_IsWebItem; }
            set { m_IsWebItem = value; }
        }

        public string ItemType
        {
            get { return m_ItemType; }
            set { m_ItemType = value; }
        }

        public string MarginPercent
        {
            get { return m_MarginPercent; }
            set { m_MarginPercent = value; }
        }

        public string MarkupPercent
        {
            get { return m_MarkupPercent; }
            set { m_MarkupPercent = value; }
        }

        public string MSRP
        {
            get { return m_MSRP; }
            set { m_MSRP = value; }
        }

        public string OnHandStore01
        {
            get { return m_OnHandStore01; }
            set { m_OnHandStore01 = value; }
        }

        public string ReorderPointStore01
        {
            get { return m_ReorderPointStore01; }
            set { m_ReorderPointStore01 = value; }
        }

        public string OrderByUnit
        {
            get { return m_OrderByUnit; }
            set { m_OrderByUnit = value; }
        }

        public string OrderCost
        {
            get { return m_OrderCost; }
            set { m_OrderCost = value; }
        }

        public string Price1
        {
            get { return m_Price1; }
            set { m_Price1 = value; }
        }

        public string ReorderPoint
        {
            get { return m_ReorderPoint; }
            set { m_ReorderPoint = value; }
        }

        public string SellByUnit
        {
            get { return m_SellByUnit; }
            set { m_SellByUnit = value; }
        }

        public string Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        public string TaxCode
        {
            get { return m_TaxCode; }
            set { m_TaxCode = value; }
        }

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }

        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }

        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }

        public string Manufacturer
        {
            get { return m_Manufacturer; }
            set { m_Manufacturer = value; }
        }

        public string Weight
        {
            get { return m_Weight; }
            set { m_Weight = value; }
        }

        public string UOMALU
        {
            get { return m_UOMALU; }
            set { m_UOMALU = value; }
        }
      
        public string UOMMSRP
        {
            get { return m_UOMMSRP; }
            set { m_UOMMSRP = value; }
        }

        public string UOMNumberOfBaseUnits
        {
            get { return m_UOMNumberOfBaseUnits; }
            set { m_UOMNumberOfBaseUnits = value; }
        }

        public string UOMPrice1
        {
            get { return m_UOMPrice1; }
            set { m_UOMPrice1 = value; }
        }

        public string UOMUnitOfMeasure
        {
            get { return m_UOMUnitOfMeasure; }
            set { m_UOMUnitOfMeasure = value; }
        }

        public string UOMUPC
        {
            get { return m_UOMUPC; }
            set { m_UOMUPC = value; }
        }

        public string VendALU
        {
            get { return m_VendALU; }
            set { m_VendALU = value; }
        }

        public string VendOrderCost
        {
            get { return m_VendOrderCost; }
            set { m_VendOrderCost = value; }
        }

        public string VendUPC
        {
            get { return m_VendUPC; }
            set { m_VendUPC = value; }
        }

        public string VendVendorListID
        {
            get { return m_VendVendorListID; }
            set { m_VendVendorListID = value; }
        }
        #endregion
    }

  public class POSItemInventoryCollection : Collection<POSItemInventoryList>
  {
      string XmlFileData = string.Empty;

      #region private method
      /// <summary>
      /// Assigning the xml value to the class objects
      /// </summary>
      /// <param name="employeeXmlList"></param>
      /// <returns></returns>
      private Collection<POSItemInventoryList> GetItemInventoryList(string itemInventoryXmlList)
      {
          //Create a xml document for output result.
          XmlDocument outputXMLDocOfItemInventory = new XmlDocument();

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
          
          if (itemInventoryXmlList != string.Empty)
          {
              //Loading Item List into XML.
              outputXMLDocOfItemInventory.LoadXml(itemInventoryXmlList);
              XmlFileData = itemInventoryXmlList;

              //Getting Sales Order values from QuickBooks response.
              XmlNodeList ItemInventoryNodeList = outputXMLDocOfItemInventory.GetElementsByTagName(QBPOSItemInventoryList.ItemInventoryRet.ToString());

              foreach (XmlNode ItemInventoryNodes in ItemInventoryNodeList)
              {
                  if (bkWorker.CancellationPending != true)
                  {
                      POSItemInventoryList itemInvetory = new POSItemInventoryList();
                      #region values
                      foreach (XmlNode node in ItemInventoryNodes.ChildNodes)
                      {
                          //Checking ListID. 
                          if (node.Name.Equals(QBPOSItemInventoryList.ListID.ToString()))
                          {
                              itemInvetory.ListID = node.InnerText;
                          }

                          //Checking TimeCreated
                          if (node.Name.Equals(QBPOSItemInventoryList.TimeCreated.ToString()))
                          {
                              itemInvetory.TimeCreated = node.InnerText;
                          }

                          //Checking TimeModified
                          if (node.Name.Equals(QBPOSItemInventoryList.TimeModified.ToString()))
                          {
                              itemInvetory.TimeModified = node.InnerText;
                          }
                          //Checking ALU. 
                          if (node.Name.Equals(QBPOSItemInventoryList.ALU.ToString()))
                          {
                              itemInvetory.ALU = node.InnerText;
                          }
                          //Checking Attribute
                          if (node.Name.Equals(QBPOSItemInventoryList.Attribute.ToString()))
                          {
                              itemInvetory.Attribute = node.InnerText;
                          }
                          //Checking COGSAccount
                          if (node.Name.Equals(QBPOSItemInventoryList.COGSAccount.ToString()))
                          {
                              itemInvetory.COGSAccount = node.InnerText;
                          }

                          //Checking Cost
                          if (node.Name.Equals(QBPOSItemInventoryList.Cost.ToString()))
                          {
                              itemInvetory.Cost = node.InnerText;
                          }

                          //Checking DepartmentListID
                          if (node.Name.Equals(QBPOSItemInventoryList.DepartmentListID.ToString()))
                          {
                              itemInvetory.DepartmentListID = node.InnerText;
                          }

                          //Checking Desc1
                          if (node.Name.Equals(QBPOSItemInventoryList.Desc1.ToString()))
                          {
                              itemInvetory.Desc1 = node.InnerText;
                          }
                          //Checking Desc2
                          if (node.Name.Equals(QBPOSItemInventoryList.Desc2.ToString()))
                          {
                              itemInvetory.Desc2 = node.InnerText;
                          }
                          //Checking IncomeAccount
                          if (node.Name.Equals(QBPOSItemInventoryList.IncomeAccount.ToString()))
                          {
                              itemInvetory.IncomeAccount = node.InnerText;
                          }
                          //Checking IsEligibleForCommission
                          if (node.Name.Equals(QBPOSItemInventoryList.IsEligibleForCommission.ToString()))
                          {
                              itemInvetory.IsEligibleForCommission = node.InnerText;
                          }
                          //Checking IsPrintingTags
                          if (node.Name.Equals(QBPOSItemInventoryList.IsPrintingTags.ToString()))
                          {
                              itemInvetory.IsPrintingTags = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.IsUnorderable.ToString()))
                          {
                              itemInvetory.IsUnorderable = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.IsEligibleForRewards.ToString()))
                          {
                              itemInvetory.IsEligibleForRewards = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.IsWebItem.ToString()))
                          {
                              itemInvetory.IsWebItem = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.ItemType.ToString()))
                          {
                              itemInvetory.ItemType = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.MarginPercent.ToString()))
                          {
                              itemInvetory.MarginPercent = node.InnerText;
                          }

                          if (node.Name.Equals(QBPOSItemInventoryList.MarkupPercent.ToString()))
                          {
                              itemInvetory.MarkupPercent = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.MSRP.ToString()))
                          {
                              itemInvetory.MSRP = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.OnHandStore01.ToString()))
                          {
                              itemInvetory.OnHandStore01 = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.ReorderPointStore01.ToString()))
                          {
                              itemInvetory.ReorderPointStore01 = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.OrderByUnit.ToString()))
                          {
                              itemInvetory.OrderByUnit = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.OrderCost.ToString()))
                          {
                              itemInvetory.OrderCost = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.Price1.ToString()))
                          {
                              itemInvetory.Price1 = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.ReorderPoint.ToString()))
                          {
                              itemInvetory.ReorderPoint = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.SellByUnit.ToString()))
                          {
                              itemInvetory.SellByUnit = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.Size.ToString()))
                          {
                              itemInvetory.Size = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.TaxCode.ToString()))
                          {
                              itemInvetory.TaxCode = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.UnitOfMeasure.ToString()))
                          {
                              itemInvetory.UnitOfMeasure = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.UPC.ToString()))
                          {
                              itemInvetory.UPC = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.VendorListID.ToString()))
                          {
                              itemInvetory.VendorListID = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.Manufacturer.ToString()))
                          {
                              itemInvetory.Manufacturer = node.InnerText;
                          }
                          if (node.Name.Equals(QBPOSItemInventoryList.Weight.ToString()))
                          {
                              itemInvetory.Weight = node.InnerText;
                          }

                          //Checking UnitOfMeasure1
                          if (node.Name.Equals(QBPOSItemInventoryList.UnitOfMeasure1.ToString()))
                          {
                              //Getting values of Unit of Measure.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {

                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMALU.ToString()))
                                      itemInvetory.UOMALU = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMMSRP.ToString()))
                                      itemInvetory.UOMMSRP = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMNumberOfBaseUnits.ToString()))
                                      itemInvetory.UOMNumberOfBaseUnits = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMPrice1.ToString()))
                                      itemInvetory.UOMPrice1 = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMUnitOfMeasure.ToString()))
                                      itemInvetory.UOMUnitOfMeasure = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.UOMUPC.ToString()))
                                      itemInvetory.UOMUPC = childNode.InnerText;
                              }
                          }

                          //Checking Vendor Information
                          if (node.Name.Equals(QBPOSItemInventoryList.VendorInfo.ToString()))
                          {
                              //Getting values of vendor.
                              foreach (XmlNode childNode in node.ChildNodes)
                              {
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.VendALU.ToString()))
                                      itemInvetory.VendALU = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.VendOrderCost.ToString()))
                                      itemInvetory.VendOrderCost = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.VendUPC.ToString()))
                                      itemInvetory.VendUPC = childNode.InnerText;
                                  if (childNode.Name.Equals(QBPOSItemInventoryList.VendVendorListID.ToString()))
                                      itemInvetory.VendVendorListID = childNode.InnerText;
                              }
                          }
                      }
                      #endregion
                      
                      this.Add(itemInvetory);
                  }
                  else
                  {
                      return null;
                  }
              }
            //  outputXMLDocOfItemInventory.RemoveAll();
         }
          return this;
     }



      #endregion

      #region public Method
      /// <summary>
      /// if no filter was selected
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <returns></returns>
      /// 
      public List<string> GetAllPOSItemInventoryList(string QBFileName)
      {
          string itemInventoryXmlList = QBCommonUtilities.GetListFromQBPOS("ItemInventoryQueryRq", QBFileName);

          List<string> itemList = new List<string>();
          List<string> itemList1 = new List<string>();

          XmlDocument outputXMLDoc = new XmlDocument();

          outputXMLDoc.LoadXml(itemInventoryXmlList);

          XmlFileData = itemInventoryXmlList;

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

          XmlNodeList ItemInventoryNodeList = outputXMLDoc.GetElementsByTagName(QBPOSItemInventoryList.ItemInventoryRet.ToString());

          foreach (XmlNode itemInventoryNodes in ItemInventoryNodeList)
          {
              if (bkWorker.CancellationPending != true)
              {
                  foreach (XmlNode node in itemInventoryNodes.ChildNodes)
                  {
                      if (node.Name.Equals(QBEmployeeList.Name.ToString()))
                      {
                          if (!itemList.Contains(node.InnerText))
                          {
                              itemList.Add(node.InnerText);
                              string name = node.InnerText + "     :     Item Inventory";
                              itemList1.Add(name);
                          }
                      }
                  }
              }
          }

          CommonUtilities.GetInstance().ItemInventoryListWithType = itemList1;

          return itemList;

      }

      public Collection<POSItemInventoryList> PopulateItemInventoryList(string QBFileName)
      {
          string ItemInventoryXmlList = QBCommonUtilities.GetListFromQBPOS("ItemInventoryQueryRq", QBFileName);
          return GetItemInventoryList(ItemInventoryXmlList);
      }

      public Collection<POSItemInventoryList> PopulateItemInventoryList(string QBFileName, DateTime FromDate, string ToDate)
      {
          string ItemInventoryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
          return GetItemInventoryList(ItemInventoryXmlList);
      }
      public Collection<POSItemInventoryList> PopulateItemInventoryList(string QBFileName, DateTime FromDate, DateTime ToDate)
      {
          string ItemInventoryXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
          return GetItemInventoryList(ItemInventoryXmlList);
      }
      /// <summary>
      /// This method is used to get the xml response from the QuikBooks.
      /// </summary>
      /// <returns></returns>
      public string GetXmlFileData()
      {
          return XmlFileData;
      }
      #endregion
  }
}
