﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
   public  class POSDepartmentList :POS_BaseDepartmentList
    { 
       #region Public Properties

       public string ListID
       {
           get { return m_ListID; }
           set { m_ListID = value; }
       }

       public string TimeCreated
       {
           get { return m_TimeCreated; }
           set { m_TimeCreated = value; }
       }

       public string TimeModified
       {
           get { return m_TimeModified; }
           set { m_TimeModified = value; }
       }       

       public string DefaultMarginPercent
       {
           get { return m_DefaultMarginPercent; }
           set { m_DefaultMarginPercent = value; }
       }

       public string DefaultMarkupPercent
       {
           get { return m_DefaultMarkupPercent; }
           set { m_DefaultMarkupPercent = value; }
       }

       public string DepartmentCode
       {
           get { return m_DepartmentCode; }
           set { m_DepartmentCode = value; }
       }

       public string DepartmentName
       {
           get { return m_DepartmentName; }
           set { m_DepartmentName = value; }
       }

        public string StoreExchangeStatus
       {
           get { return m_StoreExchangeStatus; }
           set { m_StoreExchangeStatus = value; }
       }

       public string TaxCode
       {
           get { return m_TaxCode; }
           set { m_TaxCode = value; }
       }
            
       #endregion
    }


   public class POSDepartmentCollection : Collection<POSDepartmentList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="DepartmentXmlList"></param>
       /// <returns></returns>
       private Collection<POSDepartmentList> GetDepartmentList(string DepartmentXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfDepartment = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

           if (DepartmentXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfDepartment.LoadXml(DepartmentXmlList);
               XmlFileData = DepartmentXmlList;

               //Getting Department values from QuickBooks response.
               XmlNodeList DepartmentNodeList = outputXMLDocOfDepartment.GetElementsByTagName(QBPOSDepartmentList.DepartmentRet.ToString());

               foreach (XmlNode DepartmentNodes in DepartmentNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                      
                       POSDepartmentList Department = new POSDepartmentList();

                       foreach (XmlNode node in DepartmentNodes.ChildNodes)
                       {
                           //Checking ListID. 
                           if (node.Name.Equals(QBPOSDepartmentList.ListID.ToString()))
                           {
                               Department.ListID = node.InnerText;
                           }

                           //Checking TimeCreated
                           if (node.Name.Equals(QBPOSDepartmentList.TimeCreated.ToString()))
                           {
                               Department.TimeCreated = node.InnerText;
                           }

                           //Checking TimeModified
                           if (node.Name.Equals(QBPOSDepartmentList.TimeModified.ToString()))
                           {
                               Department.TimeModified = node.InnerText;
                           }
                           //Checking City. 
                           if (node.Name.Equals(QBPOSDepartmentList.DefaultMarginPercent.ToString()))
                           {
                               Department.DefaultMarginPercent = node.InnerText;
                           }
                           //Checking CommissionPercent
                           if (node.Name.Equals(QBPOSDepartmentList.DefaultMarkupPercent.ToString()))
                           {
                               Department.DefaultMarkupPercent = node.InnerText;
                             
                           }
                           //Checking Country
                           if (node.Name.Equals(QBPOSDepartmentList.DepartmentCode.ToString()))
                           {
                               Department.DepartmentCode = node.InnerText;
                           }

                           //Checking Email
                           if (node.Name.Equals(QBPOSDepartmentList.DepartmentName.ToString()))
                           {
                               Department.DepartmentName = node.InnerText;
                           }

                           //Checking FirstName
                           if (node.Name.Equals(QBPOSDepartmentList.StoreExchangeStatus.ToString()))
                           {
                               Department.StoreExchangeStatus = node.InnerText;
                           }

                           //Checking IsTrackingHours
                           if (node.Name.Equals(QBPOSDepartmentList.TaxCode.ToString()))
                           {
                               Department.TaxCode = node.InnerText;
                           }


                       }
                       this.Add(Department);
                   }
                   else
                   {
                       return null;
                   }
               }
               outputXMLDocOfDepartment.RemoveAll();
           }
           return this;
       }
       #endregion

       #region public Method
     
  

       public Collection<POSDepartmentList> PopulateDepartmentList(string QBFileName)
       {
           string DepartmentXmlList = QBCommonUtilities.GetListFromQBPOS("DepartmentQueryRq", QBFileName);
           return GetDepartmentList(DepartmentXmlList);
       }

    
       /// <summary>
       /// This method is used to get the xml response from the QuikBooks.
       /// </summary>
       /// <returns></returns>
       public string GetXmlFileData()
       {
           return XmlFileData;
       }
       #endregion
   }
}
