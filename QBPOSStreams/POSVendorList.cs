﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;


namespace QBPOSStreams
{
  public class POSVendorList
    {
      public List<string> GetAllPOSVendorList(string QBFileName)
      {
          string XmlFileData = string.Empty;
          string VendorXmlList = QBCommonUtilities.GetListFromQBPOS("VendorQueryRq", QBFileName);

          List<string> venList = new List<string>();
          List<string> venList1 = new List<string>();

          XmlDocument outputXMLDoc = new XmlDocument();

          outputXMLDoc.LoadXml(VendorXmlList);

          XmlFileData = VendorXmlList;

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

          XmlNodeList vendorNodeList = outputXMLDoc.GetElementsByTagName(QBPOSVendorList.VendorRet.ToString());


          foreach (XmlNode vendorNodes in vendorNodeList)
          {
              string companyName = string.Empty;
              string lastname = string.Empty;
              string fullname = string.Empty;
              if (bkWorker.CancellationPending != true)
              {
                  foreach (XmlNode node in vendorNodes.ChildNodes)
                  {

                      if (node.Name.Equals(QBPOSVendorList.CompanyName.ToString()))
                      {
                          companyName = node.InnerText;

                          if (companyName != string.Empty )
                          {
                              if (!venList.Contains(companyName))
                              {
                                  fullname = companyName;
                                  venList.Add(fullname);
                                  string name = fullname + "     :     Vendor";
                                  venList1.Add(name);
                              }
                          }
                      }
                  }
              }
          }

          CommonUtilities.GetInstance().VendorListWithType = venList1;
          return venList;
      }
    }
}
