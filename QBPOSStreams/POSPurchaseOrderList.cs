﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
   public class POSPurchaseOrderList: POS_BasePurchaseOrderList
   {
       public POSPurchaseOrderList()
        {
            m_PurchaseOrderItemsDetails = new Collection<QBPurchaseOrderItem>();
        }

       #region  properties
       public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }
   

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
     

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
     
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
    
        public string CancelDate
        {
            get { return m_CancelDate; }
            set { m_CancelDate = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }
        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }
       
        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
    
        public string Fee
        {
            get { return m_Fee; }
            set { m_Fee = value; }
        }
    
        public string Instructions
        {
            get { return m_Instructions; }
            set { m_Instructions = value; }
        }
        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }
        public string PurchaseOrderNumber
        {
            get { return m_PurchaseOrderNumber; }
            set { m_PurchaseOrderNumber = value; }
        }    

        public string PurchaseOrderStatusDesc
        {
            get { return m_PurchaseOrderStatusDesc; }
            set { m_PurchaseOrderStatusDesc = value; }
        }
        public string QtyDue
        {
            get { return m_QtyDue; }
            set { m_QtyDue = value; }
        }
        public string QtyOrdered
        {
            get { return m_QtyOrdered; }
            set { m_QtyOrdered = value; }
        }
        public string QtyReceived
        {
            get { return m_QtyReceived; }
            set { m_QtyReceived = value; }
        }
        public string SalesOrderNumber
        {
            get { return m_SalesOrderNumber; }
            set { m_SalesOrderNumber = value; }
        }
        public string ShipToStoreNumber
        {
            get { return m_ShipToStoreNumber; }
            set { m_ShipToStoreNumber = value; }
        }

        public string StartShipDate
        {
            get { return m_StartShipDate; }
            set { m_StartShipDate = value; }
        }
  
        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
        public string Subtotal
        {
            get { return m_Subtotal; }
            set { m_Subtotal = value; }
        }
        public string Terms
        {
            get { return m_Terms; }
            set { m_Terms = value; }
        }
        public string TermsDiscount
        {
            get { return m_TermsDiscount; }
            set { m_TermsDiscount = value; }
        }
       
        public string TermsDiscountDays
        {
            get { return m_TermsDiscountDays; }
            set { m_TermsDiscountDays = value; }
        }
   
        public string TermsNetDays
        {
            get { return m_TermsNetDays; }
            set { m_TermsNetDays = value; }
        }
        public string Total
        {
            get { return m_Total; }
            set { m_Total = value; }
        }
   
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        public string UnfilledPercent
        {
            get { return m_UnfilledPercent; }
            set { m_UnfilledPercent = value; }
        }
        public string VendorCode
        {
            get { return m_VendorCode; }
            set { m_VendorCode = value; }
        }

        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }

        public Collection<QBPurchaseOrderItem> PurchaseOrderItemDetails 
        {
            get { return m_PurchaseOrderItemsDetails; }
            set { m_PurchaseOrderItemsDetails = value; }

        }
       //for purchase Order Line Rate
        public string[] TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

       #endregion

   }

   public class POSPurchaseOrderCollection : Collection<POSPurchaseOrderList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="purchaseOrderXmlList"></param>
       /// <returns></returns>
       private Collection<POSPurchaseOrderList> GetPurchaseOrderList(string purchaseOrderXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfPurchaseOrder = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
           QBPurchaseOrderItem itemList = null;
           if (purchaseOrderXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfPurchaseOrder.LoadXml(purchaseOrderXmlList);
               XmlFileData = purchaseOrderXmlList;

               //Getting Sales Order values from QuickBooks response.
               XmlNodeList PurchaseOrderNodeList = outputXMLDocOfPurchaseOrder.GetElementsByTagName(QBPOSPurchaseOrderList.PurchaseOrderRet.ToString());

               foreach (XmlNode PurchaseOrderNodes in PurchaseOrderNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                       int count = 0;
                      
                       POSPurchaseOrderList purchaseOrder = new POSPurchaseOrderList();
                     
                       purchaseOrder.TxnLineID = new string[PurchaseOrderNodes.ChildNodes.Count];
                       foreach (XmlNode node in PurchaseOrderNodes.ChildNodes)
                       {
                           //Assinging values to class.
                          
                           if (node.Name.Equals(QBPOSPurchaseOrderList.TxnID.ToString()))
                               purchaseOrder.TxnID = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TimeCreated.ToString()))
                               purchaseOrder.TimeCreated = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TimeModified.ToString()))
                               purchaseOrder.TimeModified = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Associate.ToString()))
                               purchaseOrder.TxnDate = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.CompanyName.ToString()))
                               purchaseOrder.CompanyName = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Discount.ToString()))
                               purchaseOrder.Discount = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.DiscountPercent.ToString()))
                               purchaseOrder.DiscountPercent = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Fee.ToString()))
                               purchaseOrder.Fee = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Instructions.ToString()))
                               purchaseOrder.Instructions = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.PurchaseOrderNumber.ToString()))
                               purchaseOrder.PurchaseOrderNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.PurchaseOrderStatusDesc.ToString()))
                               purchaseOrder.PurchaseOrderStatusDesc = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.QtyDue.ToString()))
                               purchaseOrder.QtyDue = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.QtyOrdered.ToString()))
                               purchaseOrder.QtyOrdered = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.QtyReceived.ToString()))
                               purchaseOrder.QtyReceived = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.SalesOrderNumber.ToString()))
                               purchaseOrder.SalesOrderNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.ShipToStoreNumber.ToString()))
                               purchaseOrder.ShipToStoreNumber = node.InnerText;


                           if (node.Name.Equals(QBPOSPurchaseOrderList.StartShipDate.ToString()))
                               purchaseOrder.StartShipDate = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.StoreNumber.ToString()))
                               purchaseOrder.StoreNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Subtotal.ToString()))
                               purchaseOrder.Subtotal = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Terms.ToString()))
                               purchaseOrder.Terms = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TermsDiscount.ToString()))
                               purchaseOrder.TermsDiscount = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TermsDiscountDays.ToString()))
                               purchaseOrder.TermsDiscountDays = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TermsNetDays.ToString()))
                               purchaseOrder.TermsNetDays = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.Total.ToString()))
                               purchaseOrder.Total = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.TxnDate.ToString()))
                               purchaseOrder.TxnDate = node.InnerText;
                          
                           if (node.Name.Equals(QBPOSPurchaseOrderList.UnfilledPercent.ToString()))
                               purchaseOrder.UnfilledPercent = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.VendorListID.ToString()))
                               purchaseOrder.VendorListID = node.InnerText;

                           if (node.Name.Equals(QBPOSPurchaseOrderList.VendorCode.ToString()))
                               purchaseOrder.VendorCode = node.InnerText;


                           //Checking Purchase Order Line ret values.
                           if (node.Name.Equals(QBPOSPurchaseOrderList.PurchaseOrderItemRet.ToString()))
                           {
                               itemList = new QBPurchaseOrderItem();
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   //Checking Txn line Id value.
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.TxnLineID.ToString()))
                                   {
                                       if (childNode.InnerText.Length > 36)
                                           purchaseOrder.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                       else
                                           purchaseOrder.TxnLineID[count] = childNode.InnerText;
                                   }

                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.ListID.ToString()))
                                   {
                                       itemList.ListID[count] = childNode.InnerText;
                                      
                                   }
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.ALU.ToString()))
                                   {
                                       itemList.ALU[count] = childNode.InnerText;
                                   }

                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Attribute.ToString()))
                                   {
                                       itemList.Attribute[count] = childNode.InnerText;
                                   }
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Cost.ToString()))
                                   {
                                       try
                                       {
                                           itemList.Cost[count] = Convert.ToDecimal(childNode.InnerText);
                                       }
                                       catch { }
                                   }
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Desc1.ToString()))
                                   {

                                       itemList.Desc1[count] = childNode.InnerText;
                                      
                                   }
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Desc2.ToString()))
                                   {

                                       itemList.Desc2[count] = childNode.InnerText;

                                   }
                                   
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.ExtendedCost.ToString()))
                                   {
                                       try
                                       {
                                           itemList.ExtendedCost[count] = Convert.ToDecimal(childNode.InnerText);
                                       }
                                       catch { }
                                   }
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.NumberOfBaseUnits.ToString()))
                                   {
                                       try
                                       {
                                           itemList.NumberOfBaseUnits[count] = childNode.InnerText;
                                       }
                                       catch { }
                                   }
                                                                    
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.ItemNumber.ToString()))
                                   {
                                        itemList.ItemNumber[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Qty.ToString()))
                                   {
                                       itemList.Qty[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.LineQtyReceived.ToString()))
                                   {
                                       itemList.LineQtyReceived[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.Size.ToString()))
                                   {
                                       itemList.Size[count] = childNode.InnerText;

                                   }
                                   
                                   //Received Quantity
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.UnitOfMeasure.ToString()))
                                   {
                                       itemList.UnitOfMeasure[count] = childNode.InnerText;

                                   }
                                   //LineOther1
                                   if (childNode.Name.Equals(QBPOSPurchaseOrderList.UPC.ToString()))
                                   {
                                       itemList.UPC[count] = childNode.InnerText;

                                   }                                 
                               }

                               count++;
                               purchaseOrder.PurchaseOrderItemDetails.Add(itemList);
                           }

                       }

                       this.Add(purchaseOrder);
                   }
                   else
                   { return null; }

               }

               //Removing all the references from OutPut Xml document.
               outputXMLDocOfPurchaseOrder.RemoveAll();

           }

           return this;
       }
       #endregion

       #region public Method
       /// <summary>
       /// if no filter was selected
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <returns></returns>
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQBPOS("PurchaseOrderQueryRq", QBFileName);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, string ToDate)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, string FromRefnum, string ToRefNum)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQBPOS("PurchaseOrderQueryRq", QBFileName, FromRefnum, ToRefNum);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefNum)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQBPOS("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefNum);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       public Collection<POSPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefNum)
       {
           string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPOS("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefNum);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       //Date range filter and companyName
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, companyName);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }
       // companyName
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, List<string> companyName)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, companyName);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }
       
       //companyName ,ref num
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, List<string> companyName, string fromRef, string toRef)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, companyName, fromRef, toRef);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }

       //last export,comapny
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, string ToDate, List<string> companyName)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, companyName);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }
       //last export,comapny,ref num
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, string ToDate, List<string> companyName, string fromRefNum, string toRefNum)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, companyName, fromRefNum, toRefNum);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }
       //Date range,comapny,ref num
       public Collection<POSPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName, string fromRefNum, string toRefNum)
       {
           string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, companyName, fromRefNum, toRefNum);
           return GetPurchaseOrderList(purchaseOrderXmlList);
       }
        /// <summary>
      /// This method is used to get the xml response from the QuikBooks.
      /// </summary>
      /// <returns></returns>
      public string GetXmlFileData()
      {
          return XmlFileData;
      }
    
       #endregion
   }
}
