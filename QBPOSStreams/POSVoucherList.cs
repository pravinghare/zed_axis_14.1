﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
   public class POSVoucherList:POS_BaseVoucherList
   {
       public POSVoucherList()
        {
            m_VoucherItemsDetails = new Collection<QBVoucherItem>();
        }

       #region Member properties 
       public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }       

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
        
        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
       
        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }       

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }
    
        public decimal?[] Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }      

        public decimal?[] DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
        

        public decimal?[] Fee
        {
            get { return m_Fee; }
            set { m_Fee = value; }
        }
       

        public decimal?[] Freight
        {
            get { return m_Freight; }
            set { m_Freight = value; }
        }
       

        public string HistoryDocStatus
        {
            get { return m_HistoryDocStatus; }
            set { m_HistoryDocStatus = value; }
        }
       
        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }
        
        public string PayeeCode
        {
            get { return m_PayeeCode; }
            set { m_PayeeCode = value; }
        }       

        public string PayeeListID
        {
            get { return m_PayeeListID; }
            set { m_PayeeListID = value; }
        }       

        public string PayeeName
        {
            get { return m_PayeeName; }
            set { m_PayeeName = value; }
        }      
      
        public string StoreExchangeStatus
        {
            get { return m_StoreExchangeStatus; }
            set { m_StoreExchangeStatus = value; }
        }

       
        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
       

        public decimal?[] Subtotal
        {
            get { return m_Subtotal; }
            set { m_Subtotal = value; }
        }
       

        public decimal?[] TermsDiscount
        {
            get { return m_TermsDiscount; }
            set { m_TermsDiscount = value; }
        }
       
        public string TermsDiscountDays
        {
            get { return m_TermsDiscountDays; }
            set { m_TermsDiscountDays = value; }
        }
       

        public string TermsNetDays
        {
            get { return m_TermsNetDays; }
            set { m_TermsNetDays = value; }
        }
        

        public decimal?[] Total
        {
            get { return m_Total; }
            set { m_Total = value; }
        }
       

        public string TotalQty
        {
            get { return m_TotalQty; }
            set { m_TotalQty = value; }
        }
        

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        

        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }
        

        public string VendorCode
        {
            get { return m_VendorCode; }
            set { m_VendorCode = value; }
        }
       

        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }
        

        public string VoucherNumber
        {
            get { return m_VoucherNumber; }
            set { m_VoucherNumber = value; }
        }
      

        public string VoucherType
        {
            get { return m_VoucherType; }
            set { m_VoucherType = value; }
        }
     

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

        public Collection<Streams.QBVoucherItem> VoucherItemsDetails
        {
            get { return m_VoucherItemsDetails; }
            set { m_VoucherItemsDetails = value; }
        }

        public string[] TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        #endregion       
    }

   public class POSVoucherCollection : Collection<POSVoucherList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="VoucherXmlList"></param>
       /// <returns></returns>
       private Collection<POSVoucherList> GetVoucherList(string VoucherXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfVoucher = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
           QBVoucherItem itemList = null;
           if (VoucherXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfVoucher.LoadXml(VoucherXmlList);
               XmlFileData = VoucherXmlList;

               //Getting Sales Order values from QuickBooks response.
               XmlNodeList VoucherNodeList = outputXMLDocOfVoucher.GetElementsByTagName(QBPOSVoucherList.VoucherRet.ToString());

               foreach (XmlNode VoucherNodes in VoucherNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                       int count = 0;                       
                      
                       POSVoucherList Voucher = new POSVoucherList();
                       Voucher.TxnLineID = new string[VoucherNodes.ChildNodes.Count];
                     
                       foreach (XmlNode node in VoucherNodes.ChildNodes)
                       {
                           //Assinging values to class.

                           if (node.Name.Equals(QBPOSVoucherList.TxnID.ToString()))
                               Voucher.TxnID = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.TimeCreated.ToString()))
                               Voucher.TimeCreated = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.TimeModified.ToString()))
                               Voucher.TimeModified = node.InnerText;
                        
                           if (node.Name.Equals(QBPOSVoucherList.Comments.ToString()))
                               Voucher.Comments = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.CompanyName.ToString()))
                               Voucher.CompanyName = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.Discount.ToString()))
                               Voucher.Discount[count] =Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.DiscountPercent.ToString()))
                               Voucher.DiscountPercent[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.Fee.ToString()))
                               Voucher.Fee[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.Freight.ToString()))
                               Voucher.Freight[count] = Convert.ToDecimal(node.InnerText);


                           if (node.Name.Equals(QBPOSVoucherList.HistoryDocStatus.ToString()))
                               Voucher.HistoryDocStatus = node.InnerText;

                          
                           if (node.Name.Equals(QBPOSVoucherList.ItemsCount.ToString()))
                               Voucher.ItemsCount = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.PayeeCode.ToString()))
                               Voucher.PayeeCode = node.InnerText;


                           if (node.Name.Equals(QBPOSVoucherList.PayeeListID.ToString()))
                               Voucher.PayeeListID = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.StoreNumber.ToString()))
                               Voucher.StoreNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.PayeeName.ToString()))
                               Voucher.PayeeName = node.InnerText;

                         
                           if (node.Name.Equals(QBPOSVoucherList.StoreExchangeStatus.ToString()))
                               Voucher.StoreExchangeStatus = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.StoreNumber.ToString()))
                               Voucher.StoreNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.Subtotal.ToString()))
                               Voucher.Subtotal[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.TermsDiscount.ToString()))
                               Voucher.TermsDiscount[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.TermsDiscountDays.ToString()))
                               Voucher.TermsDiscountDays = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.TermsNetDays.ToString()))
                               Voucher.TermsNetDays = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.Total.ToString()))
                               Voucher.Total[count] = Convert.ToDecimal(node.InnerText);

                           if (node.Name.Equals(QBPOSVoucherList.TotalQty.ToString()))
                               Voucher.TotalQty= node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.TxnDate.ToString()))
                               Voucher.TxnDate = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.TxnState.ToString()))
                               Voucher.TxnState = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.VendorCode.ToString()))
                               Voucher.VendorCode = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.VendorListID.ToString()))
                               Voucher.VendorListID = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.VoucherNumber.ToString()))
                               Voucher.VoucherNumber = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.VoucherType.ToString()))
                               Voucher.VoucherType = node.InnerText;

                           if (node.Name.Equals(QBPOSVoucherList.Workstation.ToString()))
                               Voucher.Workstation = node.InnerText;


                           //Checking Purchase Order Line ret values.
                           if (node.Name.Equals(QBPOSVoucherList.VoucherItemRet.ToString()))
                           {
                               itemList = new QBVoucherItem();
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSVoucherList.ListID.ToString()))
                                   {
                                       if (childNode.InnerText.Length > 36)
                                           Voucher.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                       else
                                           Voucher.TxnLineID[count] = childNode.InnerText;
                                   }
                                   if (childNode.Name.Equals(QBPOSVoucherList.ALU.ToString()))
                                   {
                                       itemList.ALU[count] = childNode.InnerText;
                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.Attribute.ToString()))
                                   {
                                       itemList.Attribute[count] = childNode.InnerText;
                                   }
                                   if (childNode.Name.Equals(QBPOSVoucherList.Cost.ToString()))
                                   {
                                       try
                                       {
                                           itemList.Cost[count] = Convert.ToDecimal(childNode.InnerText);
                                       }
                                       catch { }
                                   }
                                   if (childNode.Name.Equals(QBPOSVoucherList.Desc1.ToString()))
                                   {

                                       itemList.Desc1[count] = childNode.InnerText;

                                   }
                                   if (childNode.Name.Equals(QBPOSVoucherList.Desc2.ToString()))
                                   {

                                       itemList.Desc2[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.ExtendedCost.ToString()))
                                   {
                                       try
                                       {
                                           itemList.ExtendedCost[count] = Convert.ToDecimal(childNode.InnerText);
                                       }
                                       catch { }
                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.ItemNumber.ToString()))
                                   {
                                       
                                           itemList.ItemNumber[count] = childNode.InnerText;
                                       
                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.NumberOfBaseUnits.ToString()))
                                   {
                                       try
                                       {
                                           itemList.NumberOfBaseUnits[count] = childNode.InnerText;
                                       }
                                       catch { }
                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.OriginalOrderQty.ToString()))
                                   {
                                       itemList.OriginalOrderQty[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.QtyReceived.ToString()))
                                   {
                                       itemList.QtyReceived[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.SerialNumber.ToString()))
                                   {
                                       itemList.SerialNumber[count] = childNode.InnerText;

                                   }

                                   if (childNode.Name.Equals(QBPOSVoucherList.Size.ToString()))
                                   {
                                       itemList.Size[count] = childNode.InnerText;

                                   }

                                  
                                   //LineOther1
                                   if (childNode.Name.Equals(QBPOSVoucherList.UPC.ToString()))
                                   {
                                       itemList.UPC[count] = childNode.InnerText;

                                   }
                               }

                               count++;
                               Voucher.VoucherItemsDetails.Add(itemList);
                           }

                       }

                       this.Add(Voucher);
                   }
                   else
                   { return null; }

               }

               //Removing all the references from OutPut Xml document.
               outputXMLDocOfVoucher.RemoveAll();

           }

           return this;
       }
       #endregion

       #region public Method
       /// <summary>
       /// if no filter was selected
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <returns></returns>
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQBPOS("VoucherQueryRq", QBFileName);
           return GetVoucherList(VoucherXmlList);
       }
       // last export
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, string ToDate)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate);
           return GetVoucherList(VoucherXmlList);
       }
       //Date range filter
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, DateTime ToDate)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate);
           return GetVoucherList(VoucherXmlList);
       }
       //Date range filter and companyName
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, companyName);
           return GetVoucherList(VoucherXmlList);
       }
       // companyName
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, List<string> companyName)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, companyName);
           return GetVoucherList(VoucherXmlList);
       }

       // Ref
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, string fromRef, string toRef)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQPOSByDate("VoucherQueryRq", QBFileName, fromRef, toRef);
           return GetVoucherList(VoucherXmlList);
       }
       //companyName ,ref num
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, List<string> companyName,string fromRef,string toRef)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, companyName, fromRef, toRef);
           return GetVoucherList(VoucherXmlList);
       }
       //last export ,ref num
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, string ToDate, string fromRef, string toRef)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQBPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, fromRef, toRef);
           return GetVoucherList(VoucherXmlList);
       }
       //Date range,ref num
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, DateTime ToDate, string fromRef, string toRef)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQBPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, fromRef, toRef);
           return GetVoucherList(VoucherXmlList);
       }
       //last export,comapny
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, string ToDate, List<string> companyName)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, companyName);
           return GetVoucherList(VoucherXmlList);
       }
       //last export,comapny,ref num
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, string ToDate, List<string> companyName, string fromRefNum, string toRefNum)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, companyName, fromRefNum, toRefNum);
           return GetVoucherList(VoucherXmlList);
       }
       //Date range,comapny,ref num
       public Collection<POSVoucherList> PopulateVoucherList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName, string fromRefNum, string toRefNum)
       {
           string VoucherXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("VoucherQueryRq", QBFileName, FromDate, ToDate, companyName,fromRefNum, toRefNum);
           return GetVoucherList(VoucherXmlList);
       }
       /// <summary>
       /// This method is used to get the xml response from the QuikBooks.
       /// </summary>
       /// <returns></returns>
       public string GetXmlFileData()
       {
           return XmlFileData;
       }

       #endregion
   }
}
