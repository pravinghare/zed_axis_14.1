﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
   public class POSSalesReceiptList:POS_BaseSalesReceiptList
   {
       #region properties

       public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

       public string TimeCreated
       {
           get { return m_TimeCreated; }
           set { m_TimeCreated = value; }
       }

       public string TimeModified
       {
           get { return m_TimeModified; }
           set { m_TimeModified = value; }
       }
        public string Cashier
        {
            get { return m_Cashier; }
            set { m_Cashier = value; }
        }
               
       public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }
        
        public string CustomerListID
        {
            get { return m_CustomerListID; }
            set { m_CustomerListID = value; }
        }
        
        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }
        
        public string PromoCode
        {
            get { return m_PromoCode; }
            set { m_PromoCode = value; }
        }        

        public string SalesOrderTxnID
        {
            get { return m_SalesOrderTxnID; }
            set { m_SalesOrderTxnID = value; }
        }
       
        public string SalesReceiptNumber
        {
            get { return m_SalesReceiptNumber; }
            set { m_SalesReceiptNumber = value; }
        }
      
        public string SalesReceiptType
        {
            get { return m_SalesReceiptType; }
            set { m_SalesReceiptType = value; }
        }
        
        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }
        
        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
        public string Subtotal
        {
            get { return m_Subtotal; }
            set { m_Subtotal = value; }
        }
       
        public string TaxCategory
        {
            get { return m_TaxCategory; }
            set { m_TaxCategory = value; }
        }
       
        public string TenderType
        {
            get { return m_TenderType; }
            set { m_TenderType = value; }
        }
      

        public string Total
        {
            get { return m_Total; }
            set { m_Total = value; }
        }
        

        public string TrackingNumber
        {
            get { return m_TrackingNumber; }
            set { m_TrackingNumber = value; }
        }
        
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        
        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }
        
        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

       
        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }
       
        public string[] ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }
       
        public string[] Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
        
        public string[] Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }
        
        public decimal?[] Commission
        {
            get { return m_Commission; }
            set { m_Commission = value; }
        }
        
        public decimal?[] Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }
        
        public string[] Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }
        
        public string[] Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }
        
        public decimal?[] Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }
        
        public decimal?[] DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
       
        public string[] DiscountType
        {
            get { return m_DiscountType; }
            set { m_DiscountType = value; }
        }
       
        public string[] DiscountSource
        {
            get { return m_DiscountSource; }
            set { m_DiscountSource = value; }
        }
        
        public decimal?[] ExtendedPrice
        {
            get { return m_ExtendedPrice; }
            set { m_ExtendedPrice = value; }
        }
       
        public decimal?[] ExtendedTax
        {
            get { return m_ExtendedTax; }
            set { m_ExtendedTax = value; }
        }
        public string[] ItemNumber
        {
            get { return m_ItemNumber; }
            set { m_ItemNumber = value; }
        }
        public string[] NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }
       
        public decimal?[] Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }
       
        public string[] PriceLevelNumber
        {
            get { return m_PriceLevelNumber; }
            set { m_PriceLevelNumber = value; }
        }
       
        public decimal?[] Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }
     
        public string[] SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }
       
       public string[] Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }
        
        public decimal?[] TaxAmount
        {
            get { return m_TaxAmount; }
            set { m_TaxAmount = value; }
        }
            public string[] TaxCode
        {
            get { return m_TaxCode; }
            set { m_TaxCode = value; }
        }
      
        public decimal?[] TaxPercentage
        {
            get { return m_TaxPercentage; }
            set { m_TaxPercentage = value; }
        }
       
        public string[] UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
        private string[] m_UPC = new string[30000];

        public string[] UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }
        
        public string[] WebDesc
        {
            get { return m_WebDesc; }
            set { m_WebDesc = value; }
        }
        public string[] Manufacturer
        {
            get { return m_Manufacturer; }
            set { m_Manufacturer = value; }
        }
       
        public string[] Weight
        {
            get { return m_Weight; }
            set { m_Weight = value; }
        }

        public decimal? TenderAccountAmount
        {
            get { return m_TenderAccountAmount; }
            set { m_TenderAccountAmount = value; }
        }
       
        public decimal? TenderAccountTipAmount
        {
            get { return m_TenderAccountTipAmount; }
            set { m_TenderAccountTipAmount = value; }
        }

        public decimal? TenderCashAmount
        {
            get { return m_TenderCashAmount; }
            set { m_TenderCashAmount = value; }
        }

        public string TenderCheckNumber
        {
            get { return m_TenderCheckNumber; }
            set { m_TenderCheckNumber = value; }
        }
        
        public decimal? TenderCheckAmount
        {
            get { return m_TenderCheckAmount; }
            set { m_TenderCheckAmount = value; }
        }

        public string TenderCreditCardName
        {
            get { return m_TenderCreditCardName; }
            set { m_TenderCreditCardName = value; }
        }
      
        public decimal? TenderCreditCardAmount
        {
            get { return m_TenderCreditCardAmount; }
            set { m_TenderCreditCardAmount = value; }
        }
       
        public decimal? TenderCreditCardTipAmount
        {
            get { return m_TenderCreditCardTipAmount; }
            set { m_TenderCreditCardTipAmount = value; }
        }

        public decimal? TenderDebitCardCashback
        {
            get { return m_TenderDebitCardCashback; }
            set { m_TenderDebitCardCashback = value; }
        }
       
        public decimal? TenderDebitCardAmount
        {
            get { return m_TenderDebitCardAmount; }
            set { m_TenderDebitCardAmount = value; }
        }
           
        public decimal? TenderDepositAmount
        {
            get { return m_TenderDepositAmount; }
            set { m_TenderDepositAmount = value; }
        }


        public string TenderGiftCertificateNumber
        {
            get { return m_TenderGiftCertificateNumber; }
            set { m_TenderGiftCertificateNumber = value; }
        }
       
        public decimal? TenderGiftAmount
        {
            get { return m_TenderGiftAmount; }
            set { m_TenderGiftAmount = value; }
        }

        public decimal? TenderGiftCardAmount
        {
            get { return m_TenderGiftCardAmount; }
            set { m_TenderGiftCardAmount = value; }
        }
       
        public decimal? TenderGiftCardTipAmount
        {
            get { return m_TenderGiftCardTipAmount; }
            set { m_TenderGiftCardTipAmount = value; }
        }

       #endregion
   }


   public class POSSalesReceiptCollection : Collection<POSSalesReceiptList>
   {
       string XmlFileData = string.Empty;

       #region private method
       /// <summary>
       /// Assigning the xml value to the class objects
       /// </summary>
       /// <param name="employeeXmlList"></param>
       /// <returns></returns>
       private Collection<POSSalesReceiptList> GetSalesReceiptList(string salesReceiptXmlList)
       {
           //Create a xml document for output result.
           XmlDocument outputXMLDocOfSalesReceipt = new XmlDocument();

           TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
           BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

           if (salesReceiptXmlList != string.Empty)
           {
               //Loading Item List into XML.
               outputXMLDocOfSalesReceipt.LoadXml(salesReceiptXmlList);
               XmlFileData = salesReceiptXmlList;

               //Getting employee values from QuickBooks response.
               XmlNodeList SalesReceiptNodeList = outputXMLDocOfSalesReceipt.GetElementsByTagName(QBPOSSalesReceiptList.SalesReceiptRet.ToString());

               foreach (XmlNode SalesReceiptNodes in SalesReceiptNodeList)
               {
                   if (bkWorker.CancellationPending != true)
                   {
                       int count = 0;
                       int count1 = 0;
                       int i = 0;
                       POSSalesReceiptList salesReceipt = new POSSalesReceiptList();

                       count = 0;


                       salesReceipt.ALU = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Associate = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Attribute = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Commission = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Desc1 = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Desc2 = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Discount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.DiscountPercent = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.DiscountType = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.DiscountSource = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.ExtendedPrice = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.ExtendedTax = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.ItemNumber = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.NumberOfBaseUnits = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Price = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.PriceLevelNumber = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Qty = new decimal?[SalesReceiptNodes.ChildNodes.Count];                      
                       salesReceipt.SerialNumber = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Size = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.TaxAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.TaxCode = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.TaxPercentage = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.UnitOfMeasure = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.UPC = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Manufacturer = new string[SalesReceiptNodes.ChildNodes.Count];
                       salesReceipt.Weight = new string[SalesReceiptNodes.ChildNodes.Count];

                       //salesReceipt.TenderAccountAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderAccountTipAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCashAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCheckNumber = new string[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCheckAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCreditCardName = new string[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCreditCardAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderCreditCardTipAmount= new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderDebitCardCashback = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderDebitCardAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderDepositAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderGiftCertificateNumber = new string[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderGiftAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderGiftCardAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];
                       //salesReceipt.TenderGiftCardTipAmount = new decimal?[SalesReceiptNodes.ChildNodes.Count];

                       foreach (XmlNode node in SalesReceiptNodes.ChildNodes)
                       {

                           if (node.Name.Equals(QBPOSSalesReceiptList.TxnID.ToString()))
                           {
                               salesReceipt.TxnID = node.InnerText;
                           }
                           
                           if (node.Name.Equals(QBPOSSalesReceiptList.TimeCreated.ToString()))
                           {
                               salesReceipt.TimeCreated = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TimeModified.ToString()))
                           {
                               salesReceipt.TimeModified = node.InnerText;
                           }                                                   

                           if (node.Name.Equals(QBPOSSalesReceiptList.Cashier.ToString()))
                           {
                               salesReceipt.Cashier = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.Comments.ToString()))
                           {
                               salesReceipt.Comments = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.CustomerListID.ToString()))
                           {
                               salesReceipt.CustomerListID = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.ItemsCount.ToString()))
                           {
                               salesReceipt.ItemsCount = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.PromoCode.ToString()))
                           {
                               salesReceipt.PromoCode = node.InnerText;
                           }


                           if (node.Name.Equals(QBPOSSalesReceiptList.SalesOrderTxnID.ToString()))
                           {
                               salesReceipt.SalesOrderTxnID = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.SalesReceiptNumber.ToString()))
                           {
                               salesReceipt.SalesReceiptNumber = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.SalesReceiptType.ToString()))
                           {
                               salesReceipt.SalesReceiptType = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.ShipDate.ToString()))
                           {
                               salesReceipt.ShipDate = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.StoreNumber.ToString()))
                           {
                               salesReceipt.StoreNumber = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.Subtotal.ToString()))
                           {
                               salesReceipt.Subtotal = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TaxCategory.ToString()))
                           {
                               salesReceipt.TaxCategory = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderType.ToString()))
                           {
                               salesReceipt.TenderType = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.Total.ToString()))
                           {
                               salesReceipt.Total = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TrackingNumber.ToString()))
                           {
                               salesReceipt.TrackingNumber = node.InnerText;
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TxnDate.ToString()))
                           {
                               salesReceipt.TxnDate = node.InnerText;
                           }
                           if (node.Name.Equals(QBPOSSalesReceiptList.TxnState.ToString()))
                           {
                               salesReceipt.TxnState = node.InnerText;
                           }
                           if (node.Name.Equals(QBPOSSalesReceiptList.Workstation.ToString()))
                           {
                               salesReceipt.Workstation = node.InnerText;
                           }
                          
                           //Checking Sales Order Item
                           if (node.Name.Equals(QBPOSSalesReceiptList.SalesReceiptItemRet.ToString()))
                           {
                               
                               //Getting values of address.
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.ListID.ToString()))
                                   {
                                       if (childNode.InnerText.Length > 36)
                                           salesReceipt.ListID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                       else
                                           salesReceipt.ListID[count] = childNode.InnerText;
                                   }

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.ALU.ToString()))
                                       salesReceipt.ALU[count] = childNode.InnerText;


                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Associate.ToString()))
                                       salesReceipt.Associate[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Attribute.ToString()))
                                       salesReceipt.Attribute[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Commission.ToString()))
                                       salesReceipt.Commission[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Cost.ToString()))
                                       salesReceipt.Cost[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Desc1.ToString()))
                                       salesReceipt.Desc1[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Desc2.ToString()))
                                       salesReceipt.Desc2[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Discount.ToString()))
                                       salesReceipt.Discount[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.DiscountPercent.ToString()))
                                       salesReceipt.DiscountPercent[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.DiscountType.ToString()))
                                       salesReceipt.DiscountType[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.DiscountSource.ToString()))
                                       salesReceipt.DiscountSource[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.ExtendedPrice.ToString()))
                                       salesReceipt.ExtendedPrice[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.ExtendedTax.ToString()))
                                       salesReceipt.ExtendedTax[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.ItemNumber.ToString()))
                                       salesReceipt.ItemNumber[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.NumberOfBaseUnits.ToString()))
                                       salesReceipt.NumberOfBaseUnits[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Price.ToString()))
                                       salesReceipt.Price[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.PriceLevelNumber.ToString()))
                                       salesReceipt.PriceLevelNumber[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Qty.ToString()))
                                       salesReceipt.Qty[count] = Convert.ToDecimal(childNode.InnerText);

                                   
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.SerialNumber.ToString()))
                                       salesReceipt.SerialNumber[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Size.ToString()))
                                       salesReceipt.Size[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TaxAmount.ToString()))
                                       salesReceipt.TaxAmount[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TaxCode.ToString()))
                                       salesReceipt.TaxCode[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TaxPercentage.ToString()))
                                       salesReceipt.TaxPercentage[count] = Convert.ToDecimal(childNode.InnerText);

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.UnitOfMeasure.ToString()))
                                       salesReceipt.UnitOfMeasure[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.UPC.ToString()))
                                       salesReceipt.UPC[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.WebDesc.ToString()))
                                       salesReceipt.WebDesc[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Manufacturer.ToString()))
                                       salesReceipt.Manufacturer[count] = childNode.InnerText;

                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Weight.ToString()))
                                       salesReceipt.Weight[count] = childNode.InnerText;
                               }
                               count++;                                                             
                           }

                           //Checking Tender AccountRet list.
                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderAccountRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderAccountAmount = Convert.ToDecimal(childNode.InnerText);
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TipAmount.ToString()))
                                       salesReceipt.TenderAccountTipAmount = Convert.ToDecimal(childNode.InnerText);
                               }
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderCashRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderCashAmount = Convert.ToDecimal(childNode.InnerText);                                  
                               }
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderCheckRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.CheckNumber.ToString()))
                                       salesReceipt.TenderCheckNumber= childNode.InnerText;
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderCheckAmount = Convert.ToDecimal(childNode.InnerText);          
                               }
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderCreditCardRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.CardName.ToString()))
                                       salesReceipt.TenderCreditCardName = childNode.InnerText;
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderCreditCardAmount = Convert.ToDecimal(childNode.InnerText);
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TipAmount.ToString()))
                                       salesReceipt.TenderCreditCardTipAmount = Convert.ToDecimal(childNode.InnerText);
                               }
                           }

                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderDebitCardRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.Cashback.ToString()))
                                       salesReceipt.TenderDebitCardCashback = Convert.ToDecimal(childNode.InnerText);
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderDebitCardAmount = Convert.ToDecimal(childNode.InnerText);                                   
                               }
                           }
                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderDepositRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderDepositAmount = Convert.ToDecimal(childNode.InnerText);                                   
                               }
                           }
                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderGiftRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.GiftCertificateNumber.ToString()))
                                       salesReceipt.TenderGiftCertificateNumber = childNode.InnerText;
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderGiftAmount = Convert.ToDecimal(childNode.InnerText);
                               }
                           }
                           if (node.Name.Equals(QBPOSSalesReceiptList.TenderGiftCardRet.ToString()))
                           {
                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TenderAmount.ToString()))
                                       salesReceipt.TenderGiftCardAmount = Convert.ToDecimal(childNode.InnerText);
                                   if (childNode.Name.Equals(QBPOSSalesReceiptList.TipAmount.ToString()))
                                       salesReceipt.TenderGiftCardTipAmount = Convert.ToDecimal(childNode.InnerText);
                               }
                           }
                           
                       }
                       this.Add(salesReceipt);
                   }
                   else
                   {
                       return null;
                   }
               }
               outputXMLDocOfSalesReceipt.RemoveAll();
           }
           return this;
       }
       #endregion

       #region public Method
       /// <summary>
       /// if no filter was selected
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <returns></returns>
       /// 
       public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName)
       {
           string SalesReceiptXmlList = QBCommonUtilities.GetListFromQBPOS("SalesReceiptQueryRq", QBFileName);
           return GetSalesReceiptList(SalesReceiptXmlList);
       }

       public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, string ToDate)
       {
           string SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
           return GetSalesReceiptList(SalesReceiptXmlList);
       }

        public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, string ToDate, List<string> customerName)
        {
            string SalesReceiptXmlList = QBCommonUtilities.GetSalesOrderListFromQuickBookPOSByDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, customerName);
            return GetSalesReceiptList(SalesReceiptXmlList);
        }
        public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate)
       {
           string SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
           return GetSalesReceiptList(SalesReceiptXmlList);
       }
        public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> customerName)
        {
            string SalesReceiptXmlList = QBCommonUtilities.GetSalesOrderFromQuickBookPOSByDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, customerName);
            return GetSalesReceiptList(SalesReceiptXmlList);
        }


        public Collection<POSSalesReceiptList> PopulateSalesReceiptList(string QBFileName, List<string> companyName)
        {
            string SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("SalesReceiptQueryRq", QBFileName, companyName);
            return GetSalesReceiptList(SalesReceiptXmlList);
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
       {
           return XmlFileData;
       }
       #endregion
   }
}

