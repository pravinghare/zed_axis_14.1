﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using POS_BaseStream;
using System.Collections.ObjectModel;
using Interop.QBPOSXMLRPLIB;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using Streams;

namespace QBPOSStreams
{
  public  class POSInventoryQtyAdjustmentList :POS_BaseInventoryQtyAdjustmentList
    {
      public POSInventoryQtyAdjustmentList()
        {
            m_InventoryQtyAdjustmentDetails = new Collection<QBInventoryQtyAdjustmentItem>();
        }

       #region properties
       public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
       

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
      
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
       

        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }
        
        public decimal?[] CostDifference
        {
            get { return m_CostDifference; }
            set { m_CostDifference = value; }
        }
        
        public string HistoryDocStatus
        {
            get { return m_HistoryDocStatus; }
            set { m_HistoryDocStatus = value; }
        }
        
        public string InventoryAdjustmentNumber
        {
            get { return m_InventoryAdjustmentNumber; }
            set { m_InventoryAdjustmentNumber = value; }
        }

        public string InventoryAdjustmentSource
        {
            get { return m_InventoryAdjustmentSource; }
            set { m_InventoryAdjustmentSource = value; }
        }

       
        public string NewQuantity
        {
            get { return m_NewQuantity; }
            set { m_NewQuantity = value; }
        }

        public string OldQuantity
        {
            get { return m_OldQuantity; }
            set { m_OldQuantity = value; }
        }

        public string QtyDifference
        {
            get { return m_QtyDifference; }
            set { m_QtyDifference = value; }
        }

        public string ItemsCount
        {
            get { return m_ItemsCount; }
            set { m_ItemsCount = value; }
        }

        public string Reason
        {
            get { return m_Reason; }
            set { m_Reason = value; }
        }
       
        public string StoreExchangeStatus
        {
            get { return m_StoreExchangeStatus; }
            set { m_StoreExchangeStatus = value; }
        }
       
        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
      
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
      
        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }
        

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

        public Collection<Streams.QBInventoryQtyAdjustmentItem> InventoryQtyAdjustmentItemsDetails
        {
            get { return m_InventoryQtyAdjustmentDetails; }
            set { m_InventoryQtyAdjustmentDetails = value; }
        }


       #endregion
    }

  public class POSInventoryQtyAdjustmentCollection : Collection<POSInventoryQtyAdjustmentList>
  {
      string XmlFileData = string.Empty;

      #region private method
      /// <summary>
      /// Assigning the xml value to the class objects
      /// </summary>
      /// <param name="InventoryQtyAdjustmentXmlList"></param>
      /// <returns></returns>
      private Collection<POSInventoryQtyAdjustmentList> GetInventoryQtyAdjustmentList(string InventoryQtyAdjustmentXmlList)
      {
          //Create a xml document for output result.
          XmlDocument outputXMLDocOfInventoryQtyAdjustment = new XmlDocument();

          TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
          QBInventoryQtyAdjustmentItem itemList = null;
          if (InventoryQtyAdjustmentXmlList != string.Empty)
          {
              //Loading Item List into XML.
              outputXMLDocOfInventoryQtyAdjustment.LoadXml(InventoryQtyAdjustmentXmlList);
              XmlFileData = InventoryQtyAdjustmentXmlList;

              //Getting Sales Order values from QuickBooks response.
              XmlNodeList InventoryQtyAdjustmentNodeList = outputXMLDocOfInventoryQtyAdjustment.GetElementsByTagName(QBPOSInventoryQtyAdjustmentList.InventoryQtyAdjustmentRet.ToString());

              foreach (XmlNode InventoryQtyAdjustmentNodes in InventoryQtyAdjustmentNodeList)
              {
                  if (bkWorker.CancellationPending != true)
                  {
                      int count = 0;

                      POSInventoryQtyAdjustmentList InventoryQtyAdjustment = new POSInventoryQtyAdjustmentList();


                      foreach (XmlNode node in InventoryQtyAdjustmentNodes.ChildNodes)
                      {
                          //Assinging values to class.

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.TxnID.ToString()))
                              InventoryQtyAdjustment.TxnID = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.TimeCreated.ToString()))
                              InventoryQtyAdjustment.TimeCreated = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.TimeModified.ToString()))
                              InventoryQtyAdjustment.TimeModified = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.Associate.ToString()))
                              InventoryQtyAdjustment.Associate = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.Comments.ToString()))
                              InventoryQtyAdjustment.Comments = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.CostDifference.ToString()))
                              InventoryQtyAdjustment.CostDifference[count] = Convert.ToDecimal(node.InnerText);

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.HistoryDocStatus.ToString()))
                              InventoryQtyAdjustment.HistoryDocStatus = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.InventoryAdjustmentNumber.ToString()))
                              InventoryQtyAdjustment.InventoryAdjustmentNumber = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.InventoryAdjustmentSource.ToString()))
                              InventoryQtyAdjustment.InventoryAdjustmentSource = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.ItemsCount.ToString()))
                              InventoryQtyAdjustment.ItemsCount = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.NewQuantity.ToString()))
                              InventoryQtyAdjustment.NewQuantity =node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.OldQuantity.ToString()))
                              InventoryQtyAdjustment.OldQuantity = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.QtyDifference.ToString()))
                              InventoryQtyAdjustment.QtyDifference = node.InnerText;
                        
                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.Reason.ToString()))
                              InventoryQtyAdjustment.Reason = node.InnerText;


                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.StoreExchangeStatus.ToString()))
                              InventoryQtyAdjustment.StoreExchangeStatus = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.StoreNumber.ToString()))
                              InventoryQtyAdjustment.StoreNumber = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.TxnDate.ToString()))
                              InventoryQtyAdjustment.TxnDate = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.TxnState.ToString()))
                              InventoryQtyAdjustment.TxnState = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.Workstation.ToString()))
                              InventoryQtyAdjustment.Workstation = node.InnerText;

                          if (node.Name.Equals(QBPOSInventoryQtyAdjustmentList.InventoryQtyAdjustmentItemRet.ToString()))
                          {
                              itemList = new QBInventoryQtyAdjustmentItem();
                              foreach (XmlNode childNode in node.ChildNodes)
                              {

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.ListID.ToString()))
                                      itemList.ListID[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.NewQuantity.ToString()))
                                      itemList.LineNewQuantity[count] =childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.NumberOfBaseUnits.ToString()))
                                      itemList.NumberOfBaseUnits[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.OldQuantity.ToString()))
                                      itemList.LineOldQuantity[count] = childNode.InnerText;

                                 if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.QtyDifference.ToString()))
                                      itemList.LineQtyDifference[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.SerialNumber.ToString()))
                                      itemList.SerialNumber[count] = childNode.InnerText;

                                  if (childNode.Name.Equals(QBPOSInventoryQtyAdjustmentList.UnitOfMeasure.ToString()))
                                      itemList.UnitOfMeasure[count] = childNode.InnerText;
                              }
                              count++;
                              InventoryQtyAdjustment.InventoryQtyAdjustmentItemsDetails.Add(itemList);
                          }

                      }

                      this.Add(InventoryQtyAdjustment);
                  }
                  else
                  { return null; }

              }
              //Removing all the references from OutPut Xml document.
              outputXMLDocOfInventoryQtyAdjustment.RemoveAll();
          }

          return this;
      }
      #endregion

      #region public Method
      /// <summary>
      /// if no filter was selected
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <returns></returns>
      public Collection<POSInventoryQtyAdjustmentList> PopulateInventoryQtyAdjustmentList(string QBFileName)
      {
          string InventoryQtyAdjustmentXmlList = QBCommonUtilities.GetListFromQBPOS("InventoryQtyAdjustmentQueryRq", QBFileName);
          return GetInventoryQtyAdjustmentList(InventoryQtyAdjustmentXmlList);
      }

      public Collection<POSInventoryQtyAdjustmentList> PopulateInventoryQtyAdjustmentList(string QBFileName, DateTime FromDate, string ToDate)
      {
          string InventoryQtyAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("InventoryQtyAdjustmentQueryRq", QBFileName, FromDate, ToDate);
          return GetInventoryQtyAdjustmentList(InventoryQtyAdjustmentXmlList);
      }
      public Collection<POSInventoryQtyAdjustmentList> PopulateInventoryQtyAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate)
      {
          string InventoryQtyAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBookPOSByDate("InventoryQtyAdjustmentQueryRq", QBFileName, FromDate, ToDate);
          return GetInventoryQtyAdjustmentList(InventoryQtyAdjustmentXmlList);
      }

      /// <summary>
      /// This method is used to get the xml response from the QuikBooks.
      /// </summary>
      /// <returns></returns>
      public string GetXmlFileData()
      {
          return XmlFileData;
      }

      #endregion
  }
}
