﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;


namespace POSDataProcessingImportClass
{
   
    [XmlRootAttribute("POSVendorQBEntry", Namespace = "", IsNullable = false)]
   public class POSVendorQBEntry
   {
       #region Private Member Variable
       private string m_AccountNumber;      
       private string m_City;
       private string m_CompanyName;      
       private string m_Country;
       private string m_EMail;
       private string m_FirstName;
       private string m_IsInactive;      
       private string m_LastName;      
       private string m_Notes;
       private string m_Phone;
       private string m_AltPhone;
       private string m_Contact;
       private string m_AltContact;
       private string m_PostalCode;
       private string m_Salutation;       
       private string m_State;
       private string m_Street;
       private string m_Street2;
       private string m_TermsDiscount;
       private string m_TermsDiscountDays;
       private string m_TermsNetDays;
       private string m_VendorCode;              

       #endregion

       #region Constructor
       public POSVendorQBEntry()
       {
       }
       #endregion

       #region properties

       public string AccountNumber
       {
           get { return m_AccountNumber; }
           set { m_AccountNumber = value; }
       }
       public string City
       {
           get { return m_City; }
           set { m_City = value; }
       }

       public string CompanyName
       {
           get { return m_CompanyName; }
           set { m_CompanyName = value; }
       }

       public string Country
       {
           get { return m_Country; }
           set { m_Country = value; }
       }

       public string EMail
       {
           get { return m_EMail; }
           set { m_EMail = value; }
       }

       public string FirstName
       {
           get { return m_FirstName; }
           set { m_FirstName = value; }
       }

       public string IsInactive
       {
           get { return m_IsInactive; }
           set { m_IsInactive = value; }
       }

       public string LastName
       {
           get { return m_LastName; }
           set { m_LastName = value; }
       }
       
       public string Notes
       {
           get { return m_Notes; }
           set { m_Notes = value; }
       }

       public string Phone
       {
           get { return m_Phone; }
           set { m_Phone = value; }
       }

       public string Phone2
       {
           get { return m_AltPhone; }
           set { m_AltPhone = value; }
       }

       public string Phone3
       {
           get { return m_Contact; }
           set { m_Contact = value; }
       }

       public string Phone4
       {
           get { return m_AltContact; }
           set { m_AltContact = value; }
       }

       public string PostalCode
       {
           get { return m_PostalCode; }
           set { m_PostalCode = value; }
       }

       public string Salutation
       {
           get { return m_Salutation; }
           set { m_Salutation = value; }
       }

       public string State
       {
           get { return m_State; }
           set { m_State = value; }
       }

       public string Street
       {
           get { return m_Street; }
           set { m_Street = value; }
       }

       public string Street2
       {
           get { return m_Street2; }
           set { m_Street2 = value; }
       }

       public string TermsDiscount
       {
           get { return m_TermsDiscount; }
           set { m_TermsDiscount = value; }
       }

       public string TermsDiscountDays
       {
           get { return m_TermsDiscountDays; }
           set { m_TermsDiscountDays = value; }
       }

       public string TermsNetDays
       {
           get { return m_TermsNetDays; }
           set { m_TermsNetDays = value; }
       }

       public string VendorCode
       {
           get { return m_VendorCode; }
           set { m_VendorCode = value; }
       }

       #endregion

       #region  Public Methods
        /// <summary>
       /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
       public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
       {

           string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
           try
           {
               if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
               {
                   if (fileName.Contains("Program Files (x86)"))
                   {
                       fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                   }
                   else
                   {
                       fileName = fileName.Replace("Program Files", Constants.xpPath);
                   }
               }
               else
               {
                   if (fileName.Contains("Program Files (x86)"))
                   {
                       fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                   }
                   else
                   {
                       fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                   }
               }
           }
           catch { }
           try
           {
               DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSVendorQBEntry>.Save(this, fileName);
           }
           catch
           {
               statusMessage += "\n ";
               statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
               return false;
           }

           System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
           inputXMLDoc.Load(fileName);

           string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

           inputXMLDoc = new System.Xml.XmlDocument();

           System.IO.File.Delete(fileName);


           inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
           inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
           XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
           inputXMLDoc.AppendChild(qbXML);
           XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
           qbXML.AppendChild(qbXMLMsgsRq);
           qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
           XmlElement VendorAddRq = inputXMLDoc.CreateElement("VendorAddRq");
           qbXMLMsgsRq.AppendChild(VendorAddRq);
           VendorAddRq.SetAttribute("requestID", "1");
           XmlElement VendorAdd = inputXMLDoc.CreateElement("VendorAdd");
           VendorAddRq.AppendChild(VendorAdd);

           VendorAdd.InnerXml = requestXML;

           requestText = inputXMLDoc.OuterXml;
           string responseFile = string.Empty;
           string resp = string.Empty;
           try
           {
               responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
               if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
               {

                   resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
               }
               else
               {
                   resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
               }
           }
           catch (Exception ex)
           {
               CommonUtilities.WriteErrorLog(ex.Message);
               CommonUtilities.WriteErrorLog(ex.StackTrace);
           }

           finally
           {

               if (resp != string.Empty)
               {
                   string statusSeverity = string.Empty;
                   System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                   outputXMLDoc.LoadXml(resp);
                   foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VendorAddRs"))
                   {                      
                       try
                       {
                           statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                       }
                       catch
                       { }
                       TransactionImporter.TransactionImporter.testFlag = true;
                       if (statusSeverity == "Error")
                       {
                           string requesterror = string.Empty;
                           try
                           {
                               requesterror = oNode.Attributes["requestID"].Value.ToString();
                           }
                           catch { }
                           string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                           foreach (string s in array_msg)
                           {
                               if (s != "")
                               { statusMessage += s + ".\n"; }
                               if (s == "")
                               { statusMessage += s; }
                           }
                           statusMessage += "The Error location is " + requesterror;
                           TransactionImporter.TransactionImporter.testFlag = false;
                       }
                   }
                   if (statusSeverity != "Error")
                   {
                       foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VendorAddRs/VendorRet"))
                       {
                           CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                       }
                   }
                   CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
               }

           }

           if (resp == string.Empty)
           {
               statusMessage += "\n ";          
               statusMessage += "\n ";
               return false;
           }
           else
           {
               if (resp.Contains("statusSeverity=\"Error\""))
               {
                   return false;

               }
               else
                   return true;
           }
       }


       //update pos customer
        /// <summary>
        /// update transaction
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="listID"></param>
        /// <param name="vendorCode"></param>
        /// <returns></returns>
       public bool UpdateVendorInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string listID, string vendorCode)
       {
           string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
           try
           {
               if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
               {
                   if (fileName.Contains("Program Files (x86)"))
                   {
                       fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                   }
                   else
                   {
                       fileName = fileName.Replace("Program Files", Constants.xpPath);
                   }
               }
               else
               {
                   if (fileName.Contains("Program Files (x86)"))
                   {
                       fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                   }
                   else
                   {
                       fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                   }
               }
           }
           catch { }
           try
           {
               DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSVendorQBEntry>.Save(this, fileName);
           }
           catch
           {
               statusMessage += "\n ";
               statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
               return false;
           }

           System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
           inputXMLDoc.Load(fileName);

           string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

           inputXMLDoc = new System.Xml.XmlDocument();

           System.IO.File.Delete(fileName);


           inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
           inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
           XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
           inputXMLDoc.AppendChild(qbXML);
           XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
           qbXML.AppendChild(qbXMLMsgsRq);
           qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
           XmlElement VendorModRq = inputXMLDoc.CreateElement("VendorModRq");
           qbXMLMsgsRq.AppendChild(VendorModRq);
           //  customerAddRq.SetAttribute("requestID", "1");
           XmlElement VendorMod = inputXMLDoc.CreateElement("VendorMod");
           VendorModRq.AppendChild(VendorMod);
           
           VendorMod.InnerXml = requestXML;

           requestText = inputXMLDoc.OuterXml;


           XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/VendorModRq/VendorMod").FirstChild;

           //Create ListID aggregate and fill in field values for it
           System.Xml.XmlElement ListID = inputXMLDoc.CreateElement("ListID");
       
           inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/VendorModRq/VendorMod").InsertBefore(ListID, firstChild).InnerText = listID;

           //Create LoginName aggregate and fill in field values for it
           System.Xml.XmlElement VendorCode = inputXMLDoc.CreateElement("VendorCode");
           inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/VendorModRq/VendorMod").InsertBefore(VendorCode, ListID).InnerText = vendorCode;

           string responseFile = string.Empty;
           string resp = string.Empty;
           try
           {
               responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
               if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
               {

                   resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
               }
               else

                   resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

           }
           catch (Exception ex)
           {
               CommonUtilities.WriteErrorLog(ex.Message);
               CommonUtilities.WriteErrorLog(ex.StackTrace);
           }

           finally
           {

               if (resp != string.Empty)
               {
                   string statusSeverity = string.Empty;
                   System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                   outputXMLDoc.LoadXml(resp);
                   foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VendorModRs"))
                   {                      
                       try
                       {
                           statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                       }
                       catch
                       { }
                       TransactionImporter.TransactionImporter.testFlag = true;
                       if (statusSeverity == "Error")
                       {
                           string requesterror = string.Empty;
                           try
                           {
                               requesterror = oNode.Attributes["requestID"].Value.ToString();
                           }
                           catch { }
                           string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                           foreach (string s in array_msg)
                           {
                               if (s != "")
                               { statusMessage += s + ".\n"; }
                               if (s == "")
                               { statusMessage += s; }
                           }
                           statusMessage += "The Error location is " + requesterror;
                           TransactionImporter.TransactionImporter.testFlag = false;

                       }
                   }
                   if (statusSeverity != "Error")
                   {
                       foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VendorModRs/VendorRet"))
                       {
                           CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                       }
                   }
                   CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
               }

           }
           if (resp == string.Empty)
           {
               statusMessage += "\n ";
               statusMessage += "\n ";
               return false;
           }
           else
           {
               if (resp.Contains("statusSeverity=\"Error\""))
               {
                   return false;

               }
               else
                   return true;
           }
       }

       #endregion
   }



    public class POSVendorQBEntryCollection : Collection<POSVendorQBEntry>
    {

    }
}
