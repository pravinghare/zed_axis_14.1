﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
  public class ImportPOSPriceDiscount
    {

       private static ImportPOSPriceDiscount m_ImportPOSPriceDiscount;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSPriceDiscount()
        {   }

        #endregion

      /// <summary>
      /// creating new instance of class
      /// </summary>
      /// <returns></returns>
  
      public static ImportPOSPriceDiscount GetInstance()
        {
            if (m_ImportPOSPriceDiscount == null)
                m_ImportPOSPriceDiscount = new ImportPOSPriceDiscount();
            return m_ImportPOSPriceDiscount;
        }
      /// <summary>
        /// Creating new pricediscount transaction for pos.
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <param name="dt"></param>
      /// <param name="logDirectory"></param>
      /// <returns></returns>
        public POSDataProcessingImportClass.POSPriceDiscountQBEntryCollection ImportPOSPriceDiscountData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSPriceDiscountQBEntryCollection coll = new POSPriceDiscountQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
          
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    

                    DateTime priceDiscountStartDt = new DateTime();
                    string datevalue = string.Empty;

                    //Employee Validation
                    POSDataProcessingImportClass.POSPriceDiscountQBEntry PriceDiscount = new POSPriceDiscountQBEntry();
                    if (dt.Columns.Contains("PriceDiscountName"))
                    {

                        PriceDiscount = coll.FindpriceDiscountNameEntry(dr["PriceDiscountName"].ToString());

                        if (PriceDiscount == null)
                        {
                            PriceDiscount = new POSPriceDiscountQBEntry();

                            if (dt.Columns.Contains("PriceDiscountName"))
                            {
                                #region Validations of PriceDiscountName
                                if (dr["PriceDiscountName"].ToString() != string.Empty)
                                {
                                    if (dr["PriceDiscountName"].ToString().Length > 200)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountName (" + dr["PriceDiscountName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString().Substring(0, 200);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PriceDiscountReason"))
                            {
                                #region Validations of PriceDiscountReason
                                if (dr["PriceDiscountReason"].ToString() != string.Empty)
                                {
                                    if (dr["PriceDiscountReason"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountReason (" + dr["PriceDiscountReason"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 300)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.Associate = dr["Associate"].ToString().Substring(0, 300);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PriceDiscountType"))
                            {
                                #region Validations of PriceDiscountType
                                if (dr["PriceDiscountType"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        PriceDiscount.PriceDiscountType = Convert.ToString((POSDataProcessingImportClass.PriceDiscountType)Enum.Parse(typeof(POSDataProcessingImportClass.PriceDiscountType), dr["PriceDiscountType"].ToString(), true));
                                    }
                                    catch
                                    {
                                        PriceDiscount.PriceDiscountType = dr["PriceDiscountType"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("IsInactive"))
                            {
                                #region Validations of IsInactive
                                if (dr["IsInactive"].ToString() != "<None>" || dr["IsInactive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsInactive"].ToString(), out result))
                                    {
                                        PriceDiscount.IsInactive = Convert.ToInt32(dr["IsInactive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsInactive"].ToString().ToLower() == "true")
                                        {
                                            PriceDiscount.IsInactive = dr["IsInactive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsInactive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "IsInactive";
                                            }
                                            else
                                                PriceDiscount.IsInactive = dr["IsInactive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsInactive(" + dr["IsInactive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("PriceDiscountPriceLevels"))
                            {
                                #region Validations of PriceDiscountPriceLevels
                                if (dr["PriceDiscountPriceLevels"].ToString() != string.Empty)
                                {
                                    if (dr["PriceDiscountPriceLevels"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountPriceLevels(" + dr["PriceDiscountPriceLevels"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString().Substring(0, 12);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("PriceDiscountXValue"))
                            {
                                #region Validations for PriceDiscountXValue
                                if (dr["PriceDiscountXValue"].ToString() != string.Empty)
                                {
                                    int amount;
                                    if (!int.TryParse(dr["PriceDiscountXValue"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountXValue( " + dr["PriceDiscountXValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountXValue = string.Format("{0}", Convert.ToInt32(dr["PriceDiscountXValue"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("PriceDiscountYValue"))
                            {
                                #region Validations for PriceDiscountYValue
                                if (dr["PriceDiscountYValue"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PriceDiscountYValue"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountYValue( " + dr["PriceDiscountYValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountYValue = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PriceDiscountYValue"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("IsApplicableOverXValue"))
                            {
                                #region Validations of IsApplicableOverXValue
                                if (dr["IsApplicableOverXValue"].ToString() != "<None>" || dr["IsApplicableOverXValue"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsApplicableOverXValue"].ToString(), out result))
                                    {
                                        PriceDiscount.IsApplicableOverXValue = Convert.ToInt32(dr["IsApplicableOverXValue"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsApplicableOverXValue"].ToString().ToLower() == "true")
                                        {
                                            PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsApplicableOverXValue"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "IsApplicableOverXValue";
                                            }
                                            else
                                                PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsApplicableOverXValue(" + dr["IsApplicableOverXValue"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StartDate"))
                            {
                                #region validations of StartDate
                                if (dr["StartDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["StartDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out priceDiscountStartDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This StartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PriceDiscount.StartDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PriceDiscount.StartDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PriceDiscount.StartDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            priceDiscountStartDt = dttest;
                                            PriceDiscount.StartDate = dttest.ToString("yyyy-MM-dd");
                                        }
                                    }
                                    else
                                    {
                                        priceDiscountStartDt = Convert.ToDateTime(datevalue);
                                        PriceDiscount.StartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StopDate"))
                            {
                                #region validations of StopDate
                                if (dr["StopDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["StopDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out priceDiscountStartDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This StopDate(" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PriceDiscount.StopDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PriceDiscount.StopDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PriceDiscount.StopDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            priceDiscountStartDt = dttest;
                                            PriceDiscount.StopDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        priceDiscountStartDt = Convert.ToDateTime(datevalue);
                                        PriceDiscount.StopDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            QBPOSEntities.PriceDiscountItemAdd PriceDiscountItem = new QBPOSEntities.PriceDiscountItemAdd();

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscountItem.ListID = dr["ItemName"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PriceDiscountItemUOM"))
                            {
                                #region Validations of PriceDiscountItemUOM
                                if (dr["PriceDiscountItemUOM"].ToString() != string.Empty)
                                {
                                    if (dr["PriceDiscountItemUOM"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountItem UnitOfMeasure (" + dr["PriceDiscountItemUOM"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (PriceDiscountItem.ListID != null || PriceDiscountItem.UnitOfMeasure != null)
                                PriceDiscount.PriceDiscountItemAdd.Add(PriceDiscountItem);


                            coll.Add(PriceDiscount);
                        }
                        else
                        {
                            QBPOSEntities.PriceDiscountItemAdd PriceDiscountItem = new QBPOSEntities.PriceDiscountItemAdd();

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscountItem.ListID = dr["ItemName"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PriceDiscountItemUOM"))
                            {
                                #region Validations of PriceDiscountItemUOM
                                if (dr["PriceDiscountItemUOM"].ToString() != string.Empty)
                                {
                                    if (dr["PriceDiscountItemUOM"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceDiscountItem UnitOfMeasure (" + dr["PriceDiscountItemUOM"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (PriceDiscountItem.ListID != null || PriceDiscountItem.UnitOfMeasure != null)
                                PriceDiscount.PriceDiscountItemAdd.Add(PriceDiscountItem);
                        }
                    }
                    else
                    {
                        PriceDiscount = new POSPriceDiscountQBEntry();

                        if (dt.Columns.Contains("PriceDiscountName"))
                        {
                            #region Validations of PriceDiscountName
                            if (dr["PriceDiscountName"].ToString() != string.Empty)
                            {
                                if (dr["PriceDiscountName"].ToString().Length > 200)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountName (" + dr["PriceDiscountName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString().Substring(0, 200);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.PriceDiscountName = dr["PriceDiscountName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PriceDiscountReason"))
                        {
                            #region Validations of PriceDiscountReason
                            if (dr["PriceDiscountReason"].ToString() != string.Empty)
                            {
                                if (dr["PriceDiscountReason"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountReason (" + dr["PriceDiscountReason"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.PriceDiscountReason = dr["PriceDiscountReason"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 300)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.Associate = dr["Associate"].ToString().Substring(0, 300);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PriceDiscountType"))
                        {
                            #region Validations of PriceDiscountType
                            if (dr["PriceDiscountType"].ToString() != string.Empty)
                            {
                                try
                                {
                                    PriceDiscount.PriceDiscountType = Convert.ToString((POSDataProcessingImportClass.PriceDiscountType)Enum.Parse(typeof(POSDataProcessingImportClass.PriceDiscountType), dr["PriceDiscountType"].ToString(), true));
                                }
                                catch
                                {
                                    PriceDiscount.PriceDiscountType = dr["PriceDiscountType"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("IsInactive"))
                        {
                            #region Validations of IsInactive
                            if (dr["IsInactive"].ToString() != "<None>" || dr["IsInactive"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsInactive"].ToString(), out result))
                                {
                                    PriceDiscount.IsInactive = Convert.ToInt32(dr["IsInactive"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsInactive"].ToString().ToLower() == "true")
                                    {
                                        PriceDiscount.IsInactive = dr["IsInactive"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsInactive"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "IsInactive";
                                        }
                                        else
                                            PriceDiscount.IsInactive = dr["IsInactive"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsInactive(" + dr["IsInactive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.IsInactive = dr["IsInactive"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("PriceDiscountPriceLevels"))
                        {
                            #region Validations of PriceDiscountPriceLevels
                            if (dr["PriceDiscountPriceLevels"].ToString() != string.Empty)
                            {
                                if (dr["PriceDiscountPriceLevels"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountPriceLevels(" + dr["PriceDiscountPriceLevels"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString().Substring(0, 12);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.PriceDiscountPriceLevels = dr["PriceDiscountPriceLevels"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("PriceDiscountXValue"))
                        {
                            #region Validations for PriceDiscountXValue
                            if (dr["PriceDiscountXValue"].ToString() != string.Empty)
                            {
                                int amount;
                                if (!int.TryParse(dr["PriceDiscountXValue"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountXValue( " + dr["PriceDiscountXValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountXValue = dr["PriceDiscountXValue"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.PriceDiscountXValue = string.Format("{0}", Convert.ToInt32(dr["PriceDiscountXValue"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("PriceDiscountYValue"))
                        {
                            #region Validations for PriceDiscountYValue
                            if (dr["PriceDiscountYValue"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["PriceDiscountYValue"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountYValue( " + dr["PriceDiscountYValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscount.PriceDiscountYValue = dr["PriceDiscountYValue"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscount.PriceDiscountYValue = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PriceDiscountYValue"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("IsApplicableOverXValue"))
                        {
                            #region Validations of IsApplicableOverXValue
                            if (dr["IsApplicableOverXValue"].ToString() != "<None>" || dr["IsApplicableOverXValue"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsApplicableOverXValue"].ToString(), out result))
                                {
                                    PriceDiscount.IsApplicableOverXValue = Convert.ToInt32(dr["IsApplicableOverXValue"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsApplicableOverXValue"].ToString().ToLower() == "true")
                                    {
                                        PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsApplicableOverXValue"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "IsApplicableOverXValue";
                                        }
                                        else
                                            PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsApplicableOverXValue(" + dr["IsApplicableOverXValue"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.IsApplicableOverXValue = dr["IsApplicableOverXValue"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StartDate"))
                        {
                            #region validations of StartDate
                            if (dr["StartDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["StartDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out priceDiscountStartDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This StartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.StartDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.StartDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.StartDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        priceDiscountStartDt = dttest;
                                        PriceDiscount.StartDate = dttest.ToString("yyyy-MM-dd");
                                    }
                                }
                                else
                                {
                                    priceDiscountStartDt = Convert.ToDateTime(datevalue);
                                    PriceDiscount.StartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StopDate"))
                        {
                            #region validations of StopDate
                            if (dr["StopDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["StopDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out priceDiscountStartDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This StopDate(" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceDiscount.StopDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceDiscount.StopDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PriceDiscount.StopDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        priceDiscountStartDt = dttest;
                                        PriceDiscount.StopDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    priceDiscountStartDt = Convert.ToDateTime(datevalue);
                                    PriceDiscount.StopDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        QBPOSEntities.PriceDiscountItemAdd PriceDiscountItem = new QBPOSEntities.PriceDiscountItemAdd();

                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of ItemName
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscountItem.ListID = dr["ItemName"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscountItem.ListID = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("PriceDiscountItemUOM"))
                        {
                            #region Validations of PriceDiscountItemUOM
                            if (dr["PriceDiscountItemUOM"].ToString() != string.Empty)
                            {
                                if (dr["PriceDiscountItemUOM"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceDiscountItem UnitOfMeasure (" + dr["PriceDiscountItemUOM"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceDiscountItem.UnitOfMeasure = dr["PriceDiscountItemUOM"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (PriceDiscountItem.ListID != null || PriceDiscountItem.UnitOfMeasure != null)
                            PriceDiscount.PriceDiscountItemAdd.Add(PriceDiscountItem);


                        coll.Add(PriceDiscount); 
                    }

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }
    }
}
