﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
    public class ImportPOSTransferSlipClass
    {
        private static ImportPOSTransferSlipClass m_ImportPOSTransferSlipClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSTransferSlipClass()
        { }

        #endregion
        /// <summary>
        /// creating new instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSTransferSlipClass GetInstance()
        {
            if (m_ImportPOSTransferSlipClass == null)
                m_ImportPOSTransferSlipClass = new ImportPOSTransferSlipClass();
            return m_ImportPOSTransferSlipClass;
        }
        /// <summary>
        /// Creating new transfer slip transaction for pos.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>
        public POSDataProcessingImportClass.POSTransferSlipQBEntryCollection ImportPOSTransferSlipData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSTransferSlipQBEntryCollection coll = new POSTransferSlipQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;
                    DateTime SODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSTransferSlipQBEntry ItemInventory = new POSTransferSlipQBEntry();

                    if (dt.Columns.Contains("RefNumber"))
                    {
                        ItemInventory = coll.FindRefNumberEntry(dr["RefNumber"].ToString());
                        if (ItemInventory == null)
                        {
                            #region
                            ItemInventory = new POSTransferSlipQBEntry();

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region 
                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    ItemInventory.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Associate = dr["Associate"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Carrier"))
                            {
                                #region Validations of Carrier
                                if (dr["Carrier"].ToString() != string.Empty)
                                {
                                    if (dr["Carrier"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Carrier (" + dr["Carrier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Carrier = dr["Carrier"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Carrier = dr["Carrier"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Carrier = dr["Carrier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Carrier = dr["Carrier"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Comments"))
                            {
                                #region Validations of Comments
                                if (dr["Comments"].ToString() != string.Empty)
                                {
                                    if (dr["Comments"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Comments = dr["Comments"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Comments = dr["Comments"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Comments = dr["Comments"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Freight"))
                            {
                                #region Validations for Freight
                                if (dr["Freight"].ToString() != string.Empty)
                                {
                                    decimal freight;
                                    if (!decimal.TryParse(dr["Freight"].ToString(), out freight))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Freight = dr["Freight"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Freight = dr["Freight"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Freight = dr["Freight"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Freight = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Freight"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("FromStoreNumber"))
                            {
                                #region Validations of FromStoreNumber
                                if (dr["FromStoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["FromStoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FromStoreNumber(" + dr["FromStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SlipETA"))
                            {
                                #region validations of SlipETA
                                if (dr["SlipETA"].ToString() != "<None>" || dr["SlipETA"].ToString() != string.Empty)
                                {
                                    datevalue = dr["SlipETA"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SlipETA (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemInventory.SlipETA = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventory.SlipETA = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ItemInventory.SlipETA = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            ItemInventory.SlipETA = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        ItemInventory.SlipETA = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ToStoreNumber"))
                            {
                                #region Validations of ToStoreNumber
                                if (dr["ToStoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["ToStoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ToStoreNumber(" + dr["ToStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.ToStoreNumber = dr["v"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemInventory.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventory.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            ItemInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        ItemInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnState"))
                            {
                                #region Validations of ItemType
                                if (dr["TxnState"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemInventory.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemInventory.TxnState = dr["TxnState"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Workstation"))
                            {
                                #region Validations of Workstation
                                if (dr["Workstation"].ToString() != string.Empty)
                                {
                                    if (dr["Workstation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Workstation(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Workstation = dr["Workstation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                #endregion
                            }

                            QBPOSEntities.TransferSlipItemAdd TransferSlipItemAdd = new TransferSlipItemAdd();
                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemName(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //Axis-565
                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of SalesALU

                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {

                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis-565 end
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Qty"))
                            {
                                #region Validations of Qty
                                if (dr["Qty"].ToString() != string.Empty)
                                {
                                    if (dr["Qty"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Qty(" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.Qty = dr["Qty"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Axis-565
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {

                                    }
                                    else
                                    {
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                            if (TransferSlipItemAdd.ListID != null)
                                            {
                                                TransferSlipItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis-565
                            if (TransferSlipItemAdd.ListID != null || TransferSlipItemAdd.Qty != null || TransferSlipItemAdd.UnitOfMeasure != null)
                                ItemInventory.TransferSlipItemAdd.Add(TransferSlipItemAdd);
                            coll.Add(ItemInventory);

                            #endregion
                        }
                        else
                        {
                            QBPOSEntities.TransferSlipItemAdd TransferSlipItemAdd = new TransferSlipItemAdd();
                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemName(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of SalesALU

                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {

                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Qty"))
                            {
                                #region Validations of Qty
                                if (dr["Qty"].ToString() != string.Empty)
                                {
                                    if (dr["Qty"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Qty(" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.Qty = dr["Qty"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {

                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                            if (TransferSlipItemAdd.ListID != null)
                                            {
                                                TransferSlipItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (TransferSlipItemAdd.ListID != null || TransferSlipItemAdd.Qty != null || TransferSlipItemAdd.UnitOfMeasure != null)
                                ItemInventory.TransferSlipItemAdd.Add(TransferSlipItemAdd);
                        }
                    }
                    else
                    {
                        ItemInventory = new POSTransferSlipQBEntry();

                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region 
                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                ItemInventory.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Associate = dr["Associate"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Carrier"))
                        {
                            #region Validations of Carrier
                            if (dr["Carrier"].ToString() != string.Empty)
                            {
                                if (dr["Carrier"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Carrier (" + dr["Carrier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Carrier = dr["Carrier"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Carrier = dr["Carrier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Carrier = dr["Carrier"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Carrier = dr["Carrier"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Comments"))
                        {
                            #region Validations of Comments
                            if (dr["Comments"].ToString() != string.Empty)
                            {
                                if (dr["Comments"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Comments = dr["Comments"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Comments = dr["Comments"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Comments = dr["Comments"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Freight"))
                        {
                            #region Validations for Freight
                            if (dr["Freight"].ToString() != string.Empty)
                            {
                                decimal freight;
                                if (!decimal.TryParse(dr["Freight"].ToString(), out freight))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Freight = dr["Freight"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Freight = dr["Freight"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Freight = dr["Freight"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Freight = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Freight"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("FromStoreNumber"))
                        {
                            #region Validations of FromStoreNumber
                            if (dr["FromStoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["FromStoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FromStoreNumber(" + dr["FromStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.FromStoreNumber = dr["FromStoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SlipETA"))
                        {
                            #region validations of SlipETA
                            if (dr["SlipETA"].ToString() != "<None>" || dr["SlipETA"].ToString() != string.Empty)
                            {
                                datevalue = dr["SlipETA"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SlipETA (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.SlipETA = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.SlipETA = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.SlipETA = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        ItemInventory.SlipETA = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    ItemInventory.SlipETA = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ToStoreNumber"))
                        {
                            #region Validations of ToStoreNumber
                            if (dr["ToStoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["ToStoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ToStoreNumber(" + dr["ToStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ToStoreNumber = dr["v"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ToStoreNumber = dr["ToStoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        ItemInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    ItemInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnState"))
                        {
                            #region Validations of ItemType
                            if (dr["TxnState"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemInventory.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                }
                                catch
                                {
                                    ItemInventory.TxnState = dr["TxnState"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Workstation"))
                        {
                            #region Validations of Workstation
                            if (dr["Workstation"].ToString() != string.Empty)
                            {
                                if (dr["Workstation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Workstation(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Workstation = dr["Workstation"].ToString();
                                }
                            }
                            #endregion
                        }

                        QBPOSEntities.TransferSlipItemAdd TransferSlipItemAdd = new TransferSlipItemAdd();
                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of ItemName
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemName(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferSlipItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    TransferSlipItemAdd.ListID = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }
                        //Axis-565
                        if (dt.Columns.Contains("ALU"))
                        {
                            #region Validations of SalesALU

                            if (dr["ALU"].ToString() != string.Empty)
                            {
                                if (dr["ALU"].ToString().Length > 20)
                                {

                                }
                                else
                                {
                                    if (CommonUtilities.GetInstance().ALULookup == true)
                                    {
                                        TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis-565 end
                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Qty"))
                        {
                            #region Validations of Qty
                            if (dr["Qty"].ToString() != string.Empty)
                            {
                                if (dr["Qty"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Qty(" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferSlipItemAdd.Qty = dr["Qty"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                else
                                {
                                    TransferSlipItemAdd.Qty = dr["Qty"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    TransferSlipItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations of UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    TransferSlipItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Axis-565
                        if (dt.Columns.Contains("UPC"))
                        {
                            #region Validations of SalesUPC
                            if (dr["UPC"].ToString() != string.Empty)
                            {
                                if (dr["UPC"].ToString().Length > 18)
                                {

                                }
                                else
                                {
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        TransferSlipItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                        if (TransferSlipItemAdd.ListID != null)
                                        {
                                            TransferSlipItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis-565
                        if (TransferSlipItemAdd.ListID != null || TransferSlipItemAdd.Qty != null || TransferSlipItemAdd.UnitOfMeasure != null)
                            ItemInventory.TransferSlipItemAdd.Add(TransferSlipItemAdd);

                        coll.Add(ItemInventory);
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #endregion
            return coll;

        }
    }
}
