﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
    [XmlRootAttribute("POSVoucherQBEntry", Namespace = "", IsNullable = false)]
  public class POSVoucherQBEntry
  {     

        #region private member
        private string m_Associate;
        private string m_Comments;
        private string m_Discount;
        private string m_DiscountPercent;
        private string m_Fee;
        private string m_Freight;
        private string m_InvoiceDate;
        private string m_InvoiceDueDate;
        private string m_InvoiceNumber;
        private string m_PayeeListID;
        private string m_PurchaseOrderTxnID;
        private string m_QuickBooksFlag;
        private string m_StoreNumber;
        private string m_TermsDiscount;
        private string m_TermsDiscountDays;
        private string m_TermsNetDays;
        private string m_TxnDate;
        private string m_TxnState;
        private string m_VendorListID;
        private string m_VoucherType;
        private string m_Workstation;
        private Collection<VoucherItemAdd> m_VoucherItemAdd = new Collection<VoucherItemAdd>();
        #endregion

        #region Constructor
        public POSVoucherQBEntry()
        {
        }
        #endregion

        #region Private Member Variable
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }

        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }

        public string Fee
        {
            get { return m_Fee; }
            set { m_Fee = value; }
        }

        public string Freight
        {
            get { return m_Freight; }
            set { m_Freight = value; }
        }
            
        [XmlElement(DataType = "string")]
        public string InvoiceDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_InvoiceDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_InvoiceDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_InvoiceDate = value;
            }
        }

        [XmlElement(DataType = "string")]
        public string InvoiceDueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_InvoiceDueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_InvoiceDueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_InvoiceDueDate = value;
            }
        }      

        public string InvoiceNumber
        {
            get { return m_InvoiceNumber; }
            set { m_InvoiceNumber = value; }
        }

        public string PayeeListID
        {
            get { return m_PayeeListID; }
            set { m_PayeeListID = value; }
        }

        public string PurchaseOrderTxnID
        {
            get { return m_PurchaseOrderTxnID; }
            set { m_PurchaseOrderTxnID = value; }
        }

        public string QuickBooksFlag
        {
            get { return m_QuickBooksFlag; }
            set { m_QuickBooksFlag = value; }
        }

        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }

        public string TermsDiscount
        {
            get { return m_TermsDiscount; }
            set { m_TermsDiscount = value; }
        }

        public string TermsDiscountDays
        {
            get { return m_TermsDiscountDays; }
            set { m_TermsDiscountDays = value; }
        }

        public string TermsNetDays
        {
            get { return m_TermsNetDays; }
            set { m_TermsNetDays = value; }
        }  

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }
            
        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }

        public string VoucherType
        {
            get { return m_VoucherType; }
            set { m_VoucherType = value; }
        }  

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

        [XmlArray("VoucherItemAddREM")]
        public Collection<VoucherItemAdd> VoucherItemAdd
        {
            get { return m_VoucherItemAdd; }
            set { m_VoucherItemAdd = value; }
        }

        #endregion

      #region public methods
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSVoucherQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement VoucherAddRq = inputXMLDoc.CreateElement("VoucherAddRq");
            qbXMLMsgsRq.AppendChild(VoucherAddRq);
            VoucherAddRq.SetAttribute("requestID", "1");
            XmlElement VoucherAdd = inputXMLDoc.CreateElement("VoucherAdd");
            VoucherAddRq.AppendChild(VoucherAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<VoucherItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<VoucherItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</VoucherItemAddREM>", string.Empty);

            VoucherAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {
                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VoucherAddRs"))
                    {
                       
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/VoucherAddRs/VoucherRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofVoucher(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

      #endregion
  }

  public class POSVoucherQBEntryCollection : Collection<POSVoucherQBEntry>
  {
      public POSVoucherQBEntry FindInvoiceNumberVocucherEntry(string invoiceNumber)
      {
          foreach (POSVoucherQBEntry item in this)
          {
              if (item.InvoiceNumber == invoiceNumber)
              {
                  return item;
              }
          }
          return null;
      }
  }

  public enum TxnState
  {
      Normal,
      Held
  }

}
