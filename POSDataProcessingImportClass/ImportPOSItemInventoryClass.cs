﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
   public class ImportPOSItemInventoryClass
    {
       private static ImportPOSItemInventoryClass m_ImportPOSItemInventoryClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSItemInventoryClass()
        {   }

        #endregion
       /// <summary>
       /// creating new instance of class
       /// </summary>
       /// <returns></returns>
        public static ImportPOSItemInventoryClass GetInstance()
        {
            if (m_ImportPOSItemInventoryClass == null)
                m_ImportPOSItemInventoryClass = new ImportPOSItemInventoryClass();
            return m_ImportPOSItemInventoryClass;
        }
       /// <summary>
       /// Creating new inventory transaction for pos.
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <param name="dt"></param>
       /// <param name="logDirectory"></param>
       /// <returns></returns>
        public POSDataProcessingImportClass.POSItemInventoryQBEntryCollection ImportPOSItemInventoryData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSItemInventoryQBEntryCollection coll = new POSItemInventoryQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            // int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                  
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    //   DateTime EmployeeDt = new DateTime();
                    string datevalue = string.Empty;

                    //Employee Validation
                    POSDataProcessingImportClass.POSItemInventoryQBEntry ItemInventory = new POSItemInventoryQBEntry();

                    if (dt.Columns.Contains("ALU"))
                    {
                        #region Validations of ALU
                        if (dr["ALU"].ToString() != string.Empty)
                        {
                            if (dr["ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ALU = dr["ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ALU = dr["ALU"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ALU = dr["ALU"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ALU = dr["ALU"].ToString();
                                //Axis-565
                                //if (ItemInventory.ALU != null)
                                //ItemInventory.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, ItemInventory.ALU);
                                //Axis 692
                                if (ItemInventory.ALU != null)
                                {
                                    string itemname = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, ItemInventory.ALU);
                                    if (itemname != null)
                                    {
                                        ItemInventory.Desc1 = itemname;
                                    }
                                }
                                //Axis 692 ends
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Attribute"))
                    {
                        #region Validations of Attribute
                        if (dr["Attribute"].ToString() != string.Empty)
                        {
                            if (dr["Attribute"].ToString().Length > 12)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Attribute = dr["Attribute"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Attribute = dr["Attribute"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("COGSAccount"))
                    {
                        #region Validations of COGSAccount
                        if (dr["COGSAccount"].ToString() != string.Empty)
                        {
                            if (dr["COGSAccount"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This COGSAccount (" + dr["COGSAccount"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.COGSAccount = dr["COGSAccount"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.COGSAccount = dr["COGSAccount"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.COGSAccount = dr["COGSAccount"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.COGSAccount = dr["COGSAccount"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Cost"))
                    {
                        #region Validations for Cost
                        if (dr["Cost"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Cost = dr["Cost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Cost = dr["Cost"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Cost = dr["Cost"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                            }
                        }

                        #endregion
                    }
                    if (dt.Columns.Contains("Department"))
                    {
                        #region Validations of DepartmentListID 
                        if (dr["Department"].ToString() != string.Empty)
                        {
                            if (dr["Department"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Department (" + dr["Department"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.DepartmentListID = dr["Department"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.DepartmentListID = dr["Department"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.DepartmentListID = dr["Department"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.DepartmentListID = dr["Department"].ToString();
                            }
                        }
                        #endregion
                    }                                     

                    if (dt.Columns.Contains("Desc1"))
                    {
                        #region Validations of Desc1
                        if (dr["Desc1"].ToString() != string.Empty)
                        {
                            if (dr["Desc1"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Desc1 (" + dr["Desc1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Desc1 = dr["Desc1"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Desc1 = dr["Desc1"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Desc1 = dr["Desc1"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Desc1 = dr["Desc1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Desc2"))
                    {
                        #region Validations of Desc2
                        if (dr["Desc2"].ToString() != string.Empty)
                        {
                            if (dr["Desc2"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Desc2 = dr["Desc2"].ToString().Substring(0, 500);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Desc2 = dr["Desc2"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Desc2 = dr["Desc2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IncomeAccount"))
                    {
                        #region Validations of IncomeAccount
                        if (dr["IncomeAccount"].ToString() != string.Empty)
                        {
                            if (dr["IncomeAccount"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This IncomeAccount (" + dr["IncomeAccount"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.IncomeAccount = dr["IncomeAccount"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.IncomeAccount = dr["IncomeAccount"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.IncomeAccount = dr["IncomeAccount"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.IncomeAccount = dr["IncomeAccount"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("IsEligibleForCommission"))
                    {
                        #region Validations of IsEligibleForCommission
                        if (dr["IsEligibleForCommission"].ToString() != "<None>" || dr["IsEligibleForCommission"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsEligibleForCommission"].ToString(), out result))
                            {
                                ItemInventory.IsEligibleForCommission = Convert.ToInt32(dr["IsEligibleForCommission"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsEligibleForCommission"].ToString().ToLower() == "true")
                                {
                                    ItemInventory.IsEligibleForCommission = dr["IsEligibleForCommission"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsEligibleForCommission"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsEligibleForCommission";
                                    }
                                    else
                                        ItemInventory.IsEligibleForCommission = dr["IsEligibleForCommission"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsEligibleForCommission(" + dr["IsEligibleForCommission"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemInventory.IsEligibleForCommission = dr["IsEligibleForCommission"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.IsEligibleForCommission = dr["IsEligibleForCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.IsEligibleForCommission = dr["IsEligibleForCommission"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsPrintingTags"))
                    {
                        #region Validations of IsPrintingTags
                        if (dr["IsPrintingTags"].ToString() != "<None>" || dr["IsPrintingTags"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsPrintingTags"].ToString(), out result))
                            {
                                ItemInventory.IsPrintingTags = Convert.ToInt32(dr["IsPrintingTags"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsPrintingTags"].ToString().ToLower() == "true")
                                {
                                    ItemInventory.IsPrintingTags = dr["IsPrintingTags"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsPrintingTags"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsPrintingTags";
                                    }
                                    else
                                        ItemInventory.IsPrintingTags = dr["IsPrintingTags"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsPrintingTags(" + dr["IsPrintingTags"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemInventory.IsPrintingTags = dr["IsPrintingTags"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.IsPrintingTags = dr["IsPrintingTags"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.IsPrintingTags = dr["IsPrintingTags"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsUnorderable"))
                    {
                        #region Validations of IsUnorderable
                        if (dr["IsUnorderable"].ToString() != "<None>" || dr["IsUnorderable"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsUnorderable"].ToString(), out result))
                            {
                                ItemInventory.IsUnorderable = Convert.ToInt32(dr["IsUnorderable"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsUnorderable"].ToString().ToLower() == "true")
                                {
                                    ItemInventory.IsUnorderable = dr["IsUnorderable"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsUnorderable"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsUnorderable";
                                    }
                                    else
                                        ItemInventory.IsUnorderable = dr["IsUnorderable"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsUnorderable(" + dr["IsUnorderable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemInventory.IsUnorderable = dr["IsUnorderable"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.IsUnorderable = dr["IsUnorderable"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.IsUnorderable = dr["IsUnorderable"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsEligibleForRewards"))
                    {
                        #region Validations of IsEligibleForRewards
                        if (dr["IsEligibleForRewards"].ToString() != "<None>" || dr["IsEligibleForRewards"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsEligibleForRewards"].ToString(), out result))
                            {
                                ItemInventory.IsEligibleForRewards = Convert.ToInt32(dr["IsEligibleForRewards"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsEligibleForRewards"].ToString().ToLower() == "true")
                                {
                                    ItemInventory.IsEligibleForRewards = dr["IsEligibleForRewards"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsEligibleForRewards"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsEligibleForRewards";
                                    }
                                    else
                                        ItemInventory.IsEligibleForRewards = dr["IsEligibleForRewards"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsEligibleForRewards(" + dr["IsEligibleForRewards"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemInventory.IsEligibleForRewards = dr["IsEligibleForRewards"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.IsEligibleForRewards = dr["IsEligibleForRewards"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.IsEligibleForRewards = dr["IsEligibleForRewards"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("IsWebItem"))
                    {
                        #region Validations of IsWebItem
                        if (dr["IsWebItem"].ToString() != "<None>" || dr["IsWebItem"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsWebItem"].ToString(), out result))
                            {
                                ItemInventory.IsWebItem = Convert.ToInt32(dr["IsWebItem"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsWebItem"].ToString().ToLower() == "true")
                                {
                                    ItemInventory.IsWebItem = dr["IsWebItem"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsWebItem"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsWebItem";
                                    }
                                    else
                                        ItemInventory.IsWebItem = dr["IsWebItem"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsWebItem(" + dr["IsWebItem"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemInventory.IsWebItem = dr["IsWebItem"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.IsWebItem = dr["IsWebItem"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.IsWebItem = dr["IsWebItem"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ItemType"))
                    {
                        #region Validations of ItemType
                        if (dr["ItemType"].ToString() != string.Empty)
                        {
                            try
                            {
                                ItemInventory.ItemType = Convert.ToString((POSDataProcessingImportClass.ItemType)Enum.Parse(typeof(POSDataProcessingImportClass.ItemType), dr["ItemType"].ToString(), true));
                            }
                            catch
                            {
                                ItemInventory.ItemType = dr["ItemType"].ToString();
                            }
                        }
                        #endregion
                    }                    

                    if (dt.Columns.Contains("MarginPercent"))
                    {
                        #region Validations of MarginPercent
                        if (dr["MarginPercent"].ToString() != string.Empty)
                        {
                            if (dr["MarginPercent"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MarginPercent(" + dr["MarginPercent"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.MarginPercent = dr["MarginPercent"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.MarginPercent = dr["MarginPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.MarginPercent = dr["MarginPercent"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.MarginPercent = dr["MarginPercent"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    
                    if (dt.Columns.Contains("MSRP"))
                    {
                        #region Validations for MSRP
                        if (dr["MSRP"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["MSRP"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MSRP( " + dr["MSRP"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.MSRP = dr["MSRP"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.MSRP = dr["MSRP"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.MSRP = dr["MSRP"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.MSRP = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["MSRP"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore01"))
                    {
                        #region Validations of OnHandStore01
                        if (dr["OnHandStore01"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore01"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore01(" + dr["OnHandStore01"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore01 = dr["OnHandStore01"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore01 = dr["OnHandStore01"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore01 = dr["OnHandStore01"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore01 = dr["OnHandStore01"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OnHandStore02"))
                    {
                        #region Validations of OnHandStore02
                        if (dr["OnHandStore02"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore02"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore02(" + dr["OnHandStore02"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore02 = dr["OnHandStore02"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore02 = dr["OnHandStore02"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore02 = dr["OnHandStore02"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore02 = dr["OnHandStore02"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OnHandStore03"))
                    {
                        #region Validations of OnHandStore03
                        if (dr["OnHandStore03"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore03"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore03(" + dr["OnHandStore03"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore03 = dr["OnHandStore03"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore03 = dr["OnHandStore03"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore03 = dr["OnHandStore03"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore03 = dr["OnHandStore03"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore04"))
                    {
                        #region Validations of OnHandStore04
                        if (dr["OnHandStore04"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore04"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore04(" + dr["OnHandStore04"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore04 = dr["OnHandStore04"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore04 = dr["OnHandStore04"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore04 = dr["OnHandStore04"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore04 = dr["OnHandStore04"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OnHandStore05"))
                    {
                        #region Validations of OnHandStore05
                        if (dr["OnHandStore05"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore05"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore05(" + dr["OnHandStore05"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore05 = dr["OnHandStore05"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore05 = dr["OnHandStore05"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore05 = dr["OnHandStore05"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore05 = dr["OnHandStore05"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore06"))
                    {
                        #region Validations of OnHandStore06
                        if (dr["OnHandStore06"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore06"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore06(" + dr["OnHandStore06"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore06 = dr["OnHandStore06"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore06 = dr["OnHandStore06"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore06 = dr["OnHandStore06"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore06 = dr["OnHandStore06"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore07"))
                    {
                        #region Validations of OnHandStore07
                        if (dr["OnHandStore07"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore07"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore07(" + dr["OnHandStore07"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore07 = dr["OnHandStore07"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore07 = dr["OnHandStore07"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore07 = dr["OnHandStore07"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore07 = dr["OnHandStore07"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore08"))
                    {
                        #region Validations of OnHandStore08
                        if (dr["OnHandStore08"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore08"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore08(" + dr["OnHandStore08"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore08 = dr["OnHandStore08"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore08 = dr["OnHandStore08"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore08 = dr["OnHandStore08"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore08 = dr["OnHandStore08"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore09"))
                    {
                        #region Validations of OnHandStore09
                        if (dr["OnHandStore09"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore09"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore09(" + dr["OnHandStore09"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore09 = dr["OnHandStore09"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore09 = dr["OnHandStore09"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore09 = dr["OnHandStore09"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore09 = dr["OnHandStore09"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore10"))
                    {
                        #region Validations of OnHandStore10
                        if (dr["OnHandStore10"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore10"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore10(" + dr["OnHandStore10"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore10 = dr["OnHandStore10"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore10 = dr["OnHandStore10"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore10 = dr["OnHandStore10"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore10 = dr["OnHandStore10"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore11"))
                    {
                        #region Validations of OnHandStore11
                        if (dr["OnHandStore11"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore11"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore11(" + dr["OnHandStore11"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore11 = dr["OnHandStore11"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore11 = dr["OnHandStore11"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore11 = dr["OnHandStore11"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore11 = dr["OnHandStore11"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore12"))
                    {
                        #region Validations of OnHandStore12
                        if (dr["OnHandStore12"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore12"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore12(" + dr["OnHandStore12"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore12 = dr["OnHandStore12"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore12 = dr["OnHandStore12"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore12 = dr["OnHandStore12"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore12 = dr["OnHandStore12"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore13"))
                    {
                        #region Validations of OnHandStore13
                        if (dr["OnHandStore13"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore13"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore13(" + dr["OnHandStore13"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore13 = dr["OnHandStore13"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore13 = dr["OnHandStore13"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore13 = dr["OnHandStore13"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore13 = dr["OnHandStore13"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore14"))
                    {
                        #region Validations of OnHandStore14
                        if (dr["OnHandStore14"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore14"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore14(" + dr["OnHandStore14"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore14 = dr["OnHandStore14"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore14 = dr["OnHandStore14"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore14 = dr["OnHandStore14"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore14 = dr["OnHandStore14"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore15"))
                    {
                        #region Validations of OnHandStore15
                        if (dr["OnHandStore15"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore15"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore15(" + dr["OnHandStore15"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore15 = dr["OnHandStore15"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore15 = dr["OnHandStore15"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore15 = dr["OnHandStore15"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore15 = dr["OnHandStore15"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore16"))
                    {
                        #region Validations of OnHandStore16
                        if (dr["OnHandStore16"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore16"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore16(" + dr["OnHandStore16"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore16 = dr["OnHandStore16"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore16 = dr["OnHandStore16"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore16 = dr["OnHandStore16"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore16 = dr["OnHandStore16"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore17"))
                    {
                        #region Validations of OnHandStore17
                        if (dr["OnHandStore17"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore17"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore17(" + dr["OnHandStore17"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore17 = dr["OnHandStore17"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore17 = dr["OnHandStore17"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore17 = dr["OnHandStore17"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore17 = dr["OnHandStore17"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore18"))
                    {
                        #region Validations of OnHandStore18
                        if (dr["OnHandStore18"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore18"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore18(" + dr["OnHandStore18"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore18 = dr["OnHandStore18"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore18 = dr["OnHandStore18"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore18 = dr["OnHandStore18"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore18 = dr["OnHandStore18"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OnHandStore19"))
                    {
                        #region Validations of OnHandStore19
                        if (dr["OnHandStore19"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore19"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore19(" + dr["OnHandStore19"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore19 = dr["OnHandStore19"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore19 = dr["OnHandStore19"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore19 = dr["OnHandStore19"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore19 = dr["OnHandStore19"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OnHandStore20"))
                    {
                        #region Validations of OnHandStore20
                        if (dr["OnHandStore20"].ToString() != string.Empty)
                        {
                            if (dr["OnHandStore20"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OnHandStore20(" + dr["OnHandStore20"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OnHandStore20 = dr["OnHandStore20"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OnHandStore20 = dr["OnHandStore20"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OnHandStore20 = dr["OnHandStore20"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OnHandStore20 = dr["OnHandStore20"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore01"))
                    {
                        #region Validations of ReorderPointStore01
                        if (dr["ReorderPointStore01"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore01"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore01(" + dr["ReorderPointStore01"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore01 = dr["ReorderPointStore01"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore01 = dr["ReorderPointStore01"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore01 = dr["ReorderPointStore01"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore01 = dr["ReorderPointStore01"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore02"))
                    {
                        #region Validations of ReorderPointStore02
                        if (dr["ReorderPointStore02"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore02"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore02(" + dr["ReorderPointStore02"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore02 = dr["ReorderPointStore02"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore02 = dr["ReorderPointStore02"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore02 = dr["ReorderPointStore02"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore02 = dr["ReorderPointStore02"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore03"))
                    {
                        #region Validations of ReorderPointStore03
                        if (dr["ReorderPointStore03"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore03"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore03(" + dr["ReorderPointStore03"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore03 = dr["ReorderPointStore03"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore03 = dr["ReorderPointStore03"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore03 = dr["ReorderPointStore03"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore03 = dr["ReorderPointStore03"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore04"))
                    {
                        #region Validations of ReorderPointStore04
                        if (dr["ReorderPointStore04"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore04"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore04(" + dr["ReorderPointStore04"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore04 = dr["ReorderPointStore04"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore04 = dr["ReorderPointStore04"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore04 = dr["ReorderPointStore04"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore04 = dr["ReorderPointStore04"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ReorderPointStore05"))
                    {
                        #region Validations of ReorderPointStore05
                        if (dr["ReorderPointStore05"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore05"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore05(" + dr["ReorderPointStore05"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore05 = dr["ReorderPointStore05"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore05 = dr["ReorderPointStore05"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore05 = dr["ReorderPointStore05"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore05 = dr["ReorderPointStore05"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore06"))
                    {
                        #region Validations of ReorderPointStore06
                        if (dr["ReorderPointStore06"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore06"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore06(" + dr["ReorderPointStore06"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore06 = dr["ReorderPointStore06"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore06 = dr["ReorderPointStore06"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore06 = dr["ReorderPointStore06"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore06 = dr["ReorderPointStore06"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore07"))
                    {
                        #region Validations of ReorderPointStore07
                        if (dr["ReorderPointStore07"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore07"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore07(" + dr["ReorderPointStore07"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore07 = dr["ReorderPointStore07"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore07 = dr["ReorderPointStore07"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore07 = dr["ReorderPointStore07"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore07 = dr["ReorderPointStore07"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore08"))
                    {
                        #region Validations of ReorderPointStore08
                        if (dr["ReorderPointStore08"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore08"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore08(" + dr["ReorderPointStore08"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore08 = dr["ReorderPointStore08"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore08 = dr["ReorderPointStore08"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore08 = dr["ReorderPointStore08"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore08 = dr["ReorderPointStore08"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore09"))
                    {
                        #region Validations of ReorderPointStore09
                        if (dr["ReorderPointStore09"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore09"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore09(" + dr["ReorderPointStore09"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore09 = dr["ReorderPointStore09"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore09 = dr["ReorderPointStore09"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore09 = dr["ReorderPointStore09"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore09 = dr["ReorderPointStore09"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore10"))
                    {
                        #region Validations of ReorderPointStore10
                        if (dr["ReorderPointStore10"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore10"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore10(" + dr["ReorderPointStore10"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore10 = dr["ReorderPointStore10"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore10 = dr["ReorderPointStore10"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore10 = dr["ReorderPointStore10"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore10 = dr["ReorderPointStore10"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore11"))
                    {
                        #region Validations of ReorderPointStore11
                        if (dr["ReorderPointStore11"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore11"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore11(" + dr["ReorderPointStore11"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore11 = dr["ReorderPointStore11"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore11 = dr["ReorderPointStore11"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore11 = dr["ReorderPointStore11"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore11 = dr["ReorderPointStore11"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ReorderPointStore12"))
                    {
                        #region Validations of ReorderPointStore12
                        if (dr["ReorderPointStore12"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPointStore12"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPointStore12(" + dr["ReorderPointStore12"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPointStore12 = dr["ReorderPointStore12"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPointStore12 = dr["ReorderPointStore12"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore12 = dr["ReorderPointStore12"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPointStore12 = dr["ReorderPointStore12"].ToString();
                            }
                        }
                        #endregion
                    }

                        if (dt.Columns.Contains("ReorderPointStore13"))
                        {
                            #region Validations of ReorderPointStore13
                            if (dr["ReorderPointStore13"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore13"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore13(" + dr["ReorderPointStore13"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore13 = dr["ReorderPointStore13"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore13 = dr["ReorderPointStore13"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore13 = dr["ReorderPointStore13"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore13 = dr["ReorderPointStore13"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore14"))
                        {
                            #region Validations of ReorderPointStore14
                            if (dr["ReorderPointStore14"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore14"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore14(" + dr["ReorderPointStore14"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore14 = dr["ReorderPointStore14"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore14 = dr["ReorderPointStore14"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore14 = dr["ReorderPointStore14"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore14 = dr["ReorderPointStore14"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore15"))
                        {
                            #region Validations of ReorderPointStore15
                            if (dr["ReorderPointStore15"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore15"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore15(" + dr["ReorderPointStore15"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore15 = dr["ReorderPointStore15"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore15 = dr["ReorderPointStore15"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore15 = dr["ReorderPointStore15"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore15 = dr["ReorderPointStore15"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore16"))
                        {
                            #region Validations of ReorderPointStore16
                            if (dr["ReorderPointStore16"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore16"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore16(" + dr["ReorderPointStore16"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore16 = dr["ReorderPointStore16"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore16 = dr["ReorderPointStore16"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore16 = dr["ReorderPointStore16"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore16 = dr["ReorderPointStore16"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore17"))
                        {
                            #region Validations of ReorderPointStore17
                            if (dr["ReorderPointStore17"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore17"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore17(" + dr["ReorderPointStore17"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore17 = dr["ReorderPointStore17"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore17 = dr["ReorderPointStore17"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore17 = dr["ReorderPointStore17"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore17 = dr["ReorderPointStore17"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore18"))
                        {
                            #region Validations of ReorderPointStore18
                            if (dr["ReorderPointStore18"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore18"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore18(" + dr["ReorderPointStore18"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore18 = dr["ReorderPointStore18"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore18 = dr["ReorderPointStore18"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore18 = dr["ReorderPointStore18"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore18 = dr["ReorderPointStore18"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore19"))
                        {
                            #region Validations of ReorderPointStore19
                            if (dr["ReorderPointStore19"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore19"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore19(" + dr["ReorderPointStore19"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore19 = dr["ReorderPointStore19"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore19 = dr["ReorderPointStore19"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore19 = dr["ReorderPointStore19"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore19 = dr["ReorderPointStore19"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ReorderPointStore20"))
                        {
                            #region Validations of ReorderPointStore20
                            if (dr["ReorderPointStore20"].ToString() != string.Empty)
                            {
                                if (dr["ReorderPointStore20"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReorderPointStore20(" + dr["ReorderPointStore20"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.ReorderPointStore20 = dr["ReorderPointStore20"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.ReorderPointStore20 = dr["ReorderPointStore20"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.ReorderPointStore20 = dr["ReorderPointStore20"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPointStore20 = dr["ReorderPointStore20"].ToString();
                                }
                            }
                            #endregion
                        }
                       
                    if (dt.Columns.Contains("OrderByUnit"))
                    {
                        #region Validations of OrderByUnit
                        if (dr["OrderByUnit"].ToString() != string.Empty)
                        {
                            if (dr["OrderByUnit"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OrderByUnit(" + dr["OrderByUnit"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OrderByUnit = dr["OrderByUnit"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OrderByUnit = dr["OrderByUnit"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OrderByUnit = dr["OrderByUnit"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OrderByUnit = dr["OrderByUnit"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OrderCost"))
                    {
                        #region Validations for OrderCost
                        if (dr["OrderCost"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["OrderCost"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OrderCost( " + dr["OrderCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.OrderCost = dr["OrderCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.OrderCost = dr["OrderCost"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.OrderCost = dr["OrderCost"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.OrderCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["OrderCost"].ToString()));
                            }
                        }

                        #endregion
                    }
                    if (dt.Columns.Contains("Price1"))
                    {
                        #region Validations for Price1
                        if (dr["Price1"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Price1"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Price1( " + dr["Price1"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Price1 = dr["Price1"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Price1 = dr["Price1"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Price1 = dr["Price1"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Price1 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Price1"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Price2"))
                    {
                        #region Validations for Price2
                        if (dr["Price2"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Price2"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Price2( " + dr["Price2"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Price2 = dr["Price2"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Price2 = dr["Price2"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Price2 = dr["Price2"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Price2 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Price2"].ToString()));
                            }
                        }

                        #endregion
                    }
                    if (dt.Columns.Contains("Price3"))
                    {
                        #region Validations for Price3
                        if (dr["Price3"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Price3"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Price3( " + dr["Price3"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Price3 = dr["Price3"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Price3 = dr["Price3"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Price3 = dr["Price3"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Price3 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Price3"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Price4"))
                    {
                        #region Validations for Price4
                        if (dr["Price4"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Price4"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Price4( " + dr["Price4"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Price4 = dr["Price4"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Price4 = dr["Price4"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Price4 = dr["Price4"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Price4 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Price4"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Price5"))
                    {
                        #region Validations for Price5
                        if (dr["Price5"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Price5"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Price5( " + dr["Price5"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Price5 = dr["Price5"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Price5 = dr["Price5"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Price5 = dr["Price5"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Price5 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Price5"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("ReorderPoint"))
                    {
                        #region Validations of ReorderPoint
                        if (dr["ReorderPoint"].ToString() != string.Empty)
                        {
                            if (dr["ReorderPoint"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReorderPoint(" + dr["ReorderPoint"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.ReorderPoint = dr["ReorderPoint"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.ReorderPoint = dr["ReorderPoint"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.ReorderPoint = dr["ReorderPoint"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.ReorderPoint = dr["ReorderPoint"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("SellByUnit"))
                    {
                        #region Validations of SellByUnit
                        if (dr["SellByUnit"].ToString() != string.Empty)
                        {
                            if (dr["SellByUnit"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SellByUnit(" + dr["SellByUnit"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.SellByUnit = dr["SellByUnit"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.SellByUnit = dr["SellByUnit"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.SellByUnit = dr["SellByUnit"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.SellByUnit = dr["SellByUnit"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("SerialFlag"))
                    {
                        #region Validations of SerialFlag
                        if (dr["SerialFlag"].ToString() != string.Empty)
                        {
                            try
                            {
                                ItemInventory.SerialFlag = Convert.ToString((POSDataProcessingImportClass.SerialFlag)Enum.Parse(typeof(POSDataProcessingImportClass.SerialFlag), dr["SerialFlag"].ToString(), true));
                            }
                            catch
                            {
                                ItemInventory.SerialFlag = dr["SerialFlag"].ToString();
                            }
                        }
                        #endregion
                    } 
                    if (dt.Columns.Contains("Size"))
                    {
                        #region Validations of Size
                        if (dr["Size"].ToString() != string.Empty)
                        {
                            if (dr["Size"].ToString().Length > 12)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Size(" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Size = dr["Size"].ToString().Substring(0, 12);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Size = dr["Size"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Size = dr["Size"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Size = dr["Size"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TaxCode"))
                    {
                        #region Validations of TaxCode
                        if (dr["TaxCode"].ToString() != string.Empty)
                        {
                            if (dr["TaxCode"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxCode(" + dr["TaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.TaxCode = dr["TaxCode"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.TaxCode = dr["TaxCode"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.TaxCode = dr["TaxCode"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.TaxCode = dr["TaxCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UnitOfMeasure"))
                    {
                        #region Validations of UnitOfMeasure
                        if (dr["UnitOfMeasure"].ToString() != string.Empty)
                        {
                            if (dr["UnitOfMeasure"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UPC"))
                    {
                        #region Validations of UPC
                        if (dr["UPC"].ToString() != string.Empty)
                        {
                            if (dr["UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UPC(" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.UPC = dr["UPC"].ToString().Substring(0, 18);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.UPC = dr["UPC"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.UPC = dr["UPC"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.UPC = dr["UPC"].ToString();
                                //Axis-565
                                //Axis 692
                                if (ItemInventory.UPC != null)
                                {
                                    string itemname = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, ItemInventory.UPC, null);
                                    if (itemname != null)
                                    {
                                        ItemInventory.Desc1 = itemname;
                                    }
                                }
                                //Axis 692 ends
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorName"))
                    {
                        #region Validations of VendorName
                        if (dr["VendorName"].ToString() != string.Empty)
                        {
                            if (dr["VendorName"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorName(" + dr["VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.VendorListID = dr["VendorName"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.VendorListID = dr["VendorName"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.VendorListID = dr["VendorName"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.VendorListID = dr["VendorName"].ToString();
                            }
                        }
                        #endregion
                    }                                  
                    
                    if (dt.Columns.Contains("WebDesc"))
                    {
                        #region Validations of WebDesc
                        if (dr["WebDesc"].ToString() != string.Empty)
                        {
                            if (dr["WebDesc"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This WebDesc(" + dr["WebDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.WebDesc = dr["WebDesc"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.WebDesc = dr["WebDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.WebDesc = dr["WebDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.WebDesc = dr["WebDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("WebPrice"))
                    {
                        #region Validations of WebPrice
                        if (dr["WebPrice"].ToString() != string.Empty)
                        {
                            if (dr["WebPrice"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This WebPrice(" + dr["WebPrice"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.WebPrice = dr["WebPrice"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.WebPrice = dr["WebPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.WebPrice = dr["WebPrice"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.WebPrice = dr["WebPrice"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Manufacturer"))
                    {
                        #region Validations of Manufacturer
                        if (dr["Manufacturer"].ToString() != string.Empty)
                        {
                            if (dr["Manufacturer"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Manufacturer(" + dr["Manufacturer"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Manufacturer = dr["Manufacturer"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Manufacturer = dr["Manufacturer"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Manufacturer = dr["Manufacturer"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Manufacturer = dr["Manufacturer"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Weight"))
                    {
                        #region Validations of Weight
                        if (dr["Weight"].ToString() != string.Empty)
                        {
                            if (dr["Weight"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Weight(" + dr["Weight"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Weight = dr["Weight"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Weight = dr["Weight"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Weight = dr["Weight"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Weight = dr["Weight"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Keywords"))
                    {
                        #region Validations of Keywords
                        if (dr["Keywords"].ToString() != string.Empty)
                        {
                            if (dr["Keywords"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Keywords(" + dr["Keywords"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.Keywords = dr["Keywords"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.Keywords = dr["Keywords"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Keywords = dr["Keywords"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.Keywords = dr["Keywords"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("WebCategories"))
                    {
                        #region Validations of WebCategories
                        if (dr["WebCategories"].ToString() != string.Empty)
                        {
                            if (dr["WebCategories"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This WebCategories(" + dr["WebCategories"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemInventory.WebCategories = dr["WebCategories"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemInventory.WebCategories = dr["WebCategories"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.WebCategories = dr["WebCategories"].ToString();
                                }
                            }
                            else
                            {
                                ItemInventory.WebCategories = dr["WebCategories"].ToString();
                            }
                        }
                        #endregion
                    }
                    QBPOSEntities.UnitOfMeasure1 UnitOfMeasure1Item = new UnitOfMeasure1();              
                    
                    if (dt.Columns.Contains("UOM1ALU"))
                    {
                        #region Validations of ALU
                        if (dr["UOM1ALU"].ToString() != string.Empty)
                        {
                            if (dr["UOM1ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 ALU (" + dr["UOM1ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.ALU = dr["UOM1ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.ALU = dr["UOM1ALU"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure1Item.ALU = dr["UOM1ALU"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure1Item.ALU = dr["UOM1ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM1MSRP"))
                    {
                        #region Validations for MSRP
                        if (dr["UOM1MSRP"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM1MSRP"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 MSRP( " + dr["UOM1MSRP"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.MSRP = dr["UOM1MSRP"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.MSRP = dr["UOM1MSRP"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure1Item.MSRP = dr["UOM1MSRP"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure1Item.MSRP = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1MSRP"].ToString()));
                            }
                        }

                        #endregion
                    }
                    
                    if (dt.Columns.Contains("UOM1NumberOfBaseUnits"))
                    {
                        #region Validations of UOM1NumberOfBaseUnits
                        if (dr["UOM1NumberOfBaseUnits"].ToString() != string.Empty)
                        {
                            if (dr["UOM1NumberOfBaseUnits"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 NumberOfBaseUnits (" + dr["UOM1NumberOfBaseUnits"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.NumberOfBaseUnits = dr["UOM1NumberOfBaseUnits"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.NumberOfBaseUnits = dr["UOM1NumberOfBaseUnits"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure1Item.NumberOfBaseUnits = dr["UOM1NumberOfBaseUnits"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure1Item.NumberOfBaseUnits = dr["UOM1NumberOfBaseUnits"].ToString();
                            }
                        }
                        #endregion
                    }

                     if (dt.Columns.Contains("UOM1Price1"))
                     {
                         #region Validations for UOM1Price1
                         if (dr["UOM1Price1"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM1Price1"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1  Price1( " + dr["UOM1Price1"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.Price1 = dr["UOM1Price1"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.Price1 = dr["UOM1Price1"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure1Item.Price1 = dr["UOM1Price1"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure1Item.Price1 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1Price1"].ToString()));
                            }
                        }

                        #endregion
                     }

                     if (dt.Columns.Contains("UOM1Price2"))
                     {
                         #region Validations for UOM1Price2
                         if (dr["UOM1Price2"].ToString() != string.Empty)
                         {
                             decimal msrp;
                             if (!decimal.TryParse(dr["UOM1Price2"].ToString(), out msrp))
                             {
                                 if (isIgnoreAll == false)
                                 {
                                     string strMessages = "This UnitOfMeasure2  Price2( " + dr["UOM1Price2"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                     if (Convert.ToString(result) == "Cancel")
                                     {
                                         continue;
                                     }
                                     if (Convert.ToString(result) == "No")
                                     {
                                         return null;
                                     }
                                     if (Convert.ToString(result) == "Ignore")
                                     {
                                         UnitOfMeasure1Item.Price2 = dr["UOM1Price2"].ToString();
                                     }
                                     if (Convert.ToString(result) == "Abort")
                                     {
                                         isIgnoreAll = true;
                                         UnitOfMeasure1Item.Price2 = dr["UOM1Price2"].ToString();
                                     }
                                 }
                                 else
                                 {
                                     UnitOfMeasure1Item.Price2 = dr["UOM1Price2"].ToString();
                                 }
                             }
                             else
                             {
                                 UnitOfMeasure1Item.Price2 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1Price2"].ToString()));
                             }
                         }

                         #endregion
                     }

                     if (dt.Columns.Contains("UOM1Price3"))
                     {  
                         #region Validations for UOM1Price3
                         if (dr["UOM1Price3"].ToString() != string.Empty)
                         {
                             decimal msrp;
                             if (!decimal.TryParse(dr["UOM1Price3"].ToString(), out msrp))
                             {
                                 if (isIgnoreAll == false)
                                 {
                                     string strMessages = "This UnitOfMeasure1  Price3( " + dr["UOM1Price3"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                     if (Convert.ToString(result) == "Cancel")
                                     {
                                         continue;
                                     }
                                     if (Convert.ToString(result) == "No")
                                     {
                                         return null;
                                     }
                                     if (Convert.ToString(result) == "Ignore")
                                     {
                                         UnitOfMeasure1Item.Price3 = dr["UOM1Price3"].ToString();
                                     }
                                     if (Convert.ToString(result) == "Abort")
                                     {
                                         isIgnoreAll = true;
                                         UnitOfMeasure1Item.Price3 = dr["UOM1Price3"].ToString();
                                     }
                                 }
                                 else
                                 {
                                     UnitOfMeasure1Item.Price3 = dr["UOM1Price3"].ToString();
                                 }
                             }
                             else
                             {
                                 UnitOfMeasure1Item.Price3 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1Price3"].ToString()));
                             }
                         }

                         #endregion
                     }

                    if (dt.Columns.Contains("UOM1Price4"))
                    {
                        #region Validations for UOM1Price4
                        if (dr["UOM1Price4"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM1Price4"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1  Price4( " + dr["UOM1Price4"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.Price4 = dr["UOM1Price4"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.Price4 = dr["UOM1Price4"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure1Item.Price4 = dr["UOM1Price4"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure1Item.Price4 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1Price4"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM1Price5"))
                    {
                        #region Validations for UOM1Price5
                        if (dr["UOM1Price5"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM1Price5"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1  Price5( " + dr["UOM1Price5"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.Price5 = dr["UOM1Price5"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.Price5 = dr["UOM1Price5"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure1Item.Price5 = dr["UOM1Price5"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure1Item.Price5 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM1Price5"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM1UnitOfMeasure"))
                    {
                        #region Validations of UnitOfMeasure
                        if (dr["UOM1UnitOfMeasure"].ToString() != string.Empty)
                        {
                            if (dr["UOM1UnitOfMeasure"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure (" + dr["UOM1UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.UnitOfMeasure = dr["UOM1UnitOfMeasure"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.UnitOfMeasure = dr["UOM1UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure1Item.UnitOfMeasure = dr["UOM1UnitOfMeasure"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure1Item.UnitOfMeasure = dr["UOM1UnitOfMeasure"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("UOM1UPC"))
                    {
                        #region Validations of UPC
                        if (dr["UOM1UPC"].ToString() != string.Empty)
                        {
                            if (dr["UOM1UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 UPC (" + dr["UOM1UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure1Item.UPC = dr["UOM1UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure1Item.UPC = dr["UOM1UPC"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure1Item.UPC = dr["UOM1UPC"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure1Item.UPC = dr["UOM1UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (UnitOfMeasure1Item.ALU != null || UnitOfMeasure1Item.MSRP != null || UnitOfMeasure1Item.NumberOfBaseUnits != null || UnitOfMeasure1Item.Price1 != null || UnitOfMeasure1Item.Price2 != null || UnitOfMeasure1Item.Price3 != null || UnitOfMeasure1Item.Price4 != null || UnitOfMeasure1Item.Price5 != null || UnitOfMeasure1Item.UnitOfMeasure != null || UnitOfMeasure1Item.UPC != null)
                         ItemInventory.UnitOfMeasure1.Add(UnitOfMeasure1Item);

                    QBPOSEntities.UnitOfMeasure2 UnitOfMeasure2Item = new UnitOfMeasure2();

                    if (dt.Columns.Contains("UOM2ALU"))
                    {
                        #region Validations of ALU
                        if (dr["UOM2ALU"].ToString() != string.Empty)
                        {
                            if (dr["UOM2ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2 ALU (" + dr["UOM2ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.ALU = dr["UOM2ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.ALU = dr["UOM2ALU"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure2Item.ALU = dr["UOM2ALU"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure2Item.ALU = dr["UOM2ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2MSRP"))
                    {
                        #region Validations for MSRP
                        if (dr["UOM2MSRP"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2MSRP"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2 MSRP( " + dr["UOM2MSRP"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.MSRP = dr["UOM2MSRP"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.MSRP = dr["UOM2MSRP"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.MSRP = dr["UOM2MSRP"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.MSRP = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2MSRP"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2NumberOfBaseUnits"))
                    {
                        #region Validations of UOM2NumberOfBaseUnits
                        if (dr["UOM2NumberOfBaseUnits"].ToString() != string.Empty)
                        {
                            if (dr["UOM2NumberOfBaseUnits"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2 NumberOfBaseUnits (" + dr["UOM2NumberOfBaseUnits"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.NumberOfBaseUnits = dr["UOM2NumberOfBaseUnits"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.NumberOfBaseUnits = dr["UOM2NumberOfBaseUnits"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure2Item.NumberOfBaseUnits = dr["UOM2NumberOfBaseUnits"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure2Item.NumberOfBaseUnits = dr["UOM2NumberOfBaseUnits"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2Price1"))
                    {
                        #region Validations for UOM2Price1
                        if (dr["UOM2Price1"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2Price1"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2  Price1( " + dr["UOM2Price1"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.Price1 = dr["UOM2Price1"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.Price1 = dr["UOM2Price1"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.Price1 = dr["UOM2Price1"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.Price1 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2Price1"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2Price2"))
                    {
                        #region Validations for UOM2Price2
                        if (dr["UOM2Price2"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2Price2"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2  Price2( " + dr["UOM2Price2"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.Price2 = dr["UOM2Price2"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.Price2 = dr["UOM2Price2"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.Price2 = dr["UOM2Price2"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.Price2 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2Price2"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2Price3"))
                    {
                        #region Validations for UOM2Price3
                        if (dr["UOM2Price3"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2Price3"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2  Price3( " + dr["UOM2Price3"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.Price3 = dr["UOM2Price3"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.Price3 = dr["UOM2Price3"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.Price3 = dr["UOM2Price3"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.Price3 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2Price3"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2Price4"))
                    {
                        #region Validations for UOM2Price4
                        if (dr["UOM2Price4"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2Price4"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2  Price4( " + dr["UOM2Price4"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.Price4 = dr["UOM2Price4"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.Price4 = dr["UOM2Price4"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.Price4 = dr["UOM2Price4"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.Price4 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2Price4"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2Price5"))
                    {
                        #region Validations for UOM2Price5
                        if (dr["UOM2Price5"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM2Price5"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2  Price5( " + dr["UOM2Price5"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.Price5 = dr["UOM2Price5"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.Price5 = dr["UOM2Price5"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure2Item.Price5 = dr["UOM2Price5"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure2Item.Price5 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM2Price5"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2UnitOfMeasure"))
                    {
                        #region Validations of UnitOfMeasure
                        if (dr["UOM2UnitOfMeasure"].ToString() != string.Empty)
                        {
                            if (dr["UOM2UnitOfMeasure"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure (" + dr["UOM2UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.UnitOfMeasure = dr["UOM2UnitOfMeasure"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.UnitOfMeasure = dr["UOM2UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure2Item.UnitOfMeasure = dr["UOM2UnitOfMeasure"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure2Item.UnitOfMeasure = dr["UOM2UnitOfMeasure"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM2UPC"))
                    {
                        #region Validations of UPC
                        if (dr["UOM2UPC"].ToString() != string.Empty)
                        {
                            if (dr["UOM2UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure2 UPC (" + dr["UOM2UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure2Item.UPC = dr["UOM2UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure2Item.UPC = dr["UOM2UPC"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure2Item.UPC = dr["UOM2UPC"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure2Item.UPC = dr["UOM2UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (UnitOfMeasure2Item.ALU != null || UnitOfMeasure2Item.MSRP != null || UnitOfMeasure2Item.NumberOfBaseUnits != null || UnitOfMeasure2Item.Price1 != null || UnitOfMeasure2Item.Price2 != null || UnitOfMeasure2Item.Price3 != null || UnitOfMeasure2Item.Price4 != null || UnitOfMeasure2Item.Price5 != null || UnitOfMeasure2Item.UnitOfMeasure != null || UnitOfMeasure2Item.UPC != null)
                        ItemInventory.UnitOfMeasure2.Add(UnitOfMeasure2Item);


                    QBPOSEntities.UnitOfMeasure3 UnitOfMeasure3Item = new UnitOfMeasure3();

                    if (dt.Columns.Contains("UOM3ALU"))
                    {
                        #region Validations of ALU
                        if (dr["UOM3ALU"].ToString() != string.Empty)
                        {
                            if (dr["UOM3ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 ALU (" + dr["UOM3ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.ALU = dr["UOM3ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.ALU = dr["UOM3ALU"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure3Item.ALU = dr["UOM3ALU"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure3Item.ALU = dr["UOM3ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3MSRP"))
                    {
                        #region Validations for MSRP
                        if (dr["UOM3MSRP"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3MSRP"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure1 MSRP( " + dr["UOM3MSRP"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.MSRP = dr["UOM3MSRP"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.MSRP = dr["UOM3MSRP"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.MSRP = dr["UOM3MSRP"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.MSRP = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3MSRP"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3NumberOfBaseUnits"))
                    {
                        #region Validations of UOM3NumberOfBaseUnits
                        if (dr["UOM3NumberOfBaseUnits"].ToString() != string.Empty)
                        {
                            if (dr["UOM3NumberOfBaseUnits"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3 NumberOfBaseUnits (" + dr["UOM3NumberOfBaseUnits"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.NumberOfBaseUnits = dr["UOM3NumberOfBaseUnits"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.NumberOfBaseUnits = dr["UOM3NumberOfBaseUnits"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure3Item.NumberOfBaseUnits = dr["UOM3NumberOfBaseUnits"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure3Item.NumberOfBaseUnits = dr["UOM3NumberOfBaseUnits"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3Price1"))
                    {
                        #region Validations for UOM3Price1
                        if (dr["UOM3Price1"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3Price1"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3  Price1( " + dr["UOM3Price1"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.Price1 = dr["UOM3Price1"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.Price1 = dr["UOM3Price1"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.Price1 = dr["UOM3Price1"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.Price1 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3Price1"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3Price2"))
                    {
                        #region Validations for UOM3Price2
                        if (dr["UOM3Price2"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3Price2"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3  Price2( " + dr["UOM3Price2"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.Price2 = dr["UOM3Price2"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.Price2 = dr["UOM3Price2"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.Price2 = dr["UOM3Price2"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.Price2 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3Price2"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3Price3"))
                    {
                        #region Validations for UOM3Price3
                        if (dr["UOM3Price3"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3Price3"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3  Price3( " + dr["UOM3Price3"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.Price3 = dr["UOM3Price3"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.Price3 = dr["UOM3Price3"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.Price3 = dr["UOM3Price3"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.Price3 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3Price3"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3Price4"))
                    {
                        #region Validations for UOM3Price4
                        if (dr["UOM3Price4"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3Price4"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3  Price4( " + dr["UOM3Price4"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.Price4 = dr["UOM3Price4"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.Price4 = dr["UOM3Price4"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.Price4 = dr["UOM3Price4"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.Price4 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3Price4"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3Price5"))
                    {
                        #region Validations for UOM3Price5
                        if (dr["UOM3Price5"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["UOM3Price5"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3  Price5( " + dr["UOM3Price5"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.Price5 = dr["UOM3Price5"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.Price5 = dr["UOM3Price5"].ToString();
                                    }
                                }
                                else
                                {
                                    UnitOfMeasure3Item.Price5 = dr["UOM3Price5"].ToString();
                                }
                            }
                            else
                            {
                                UnitOfMeasure3Item.Price5 = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["UOM3Price5"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3UnitOfMeasure"))
                    {
                        #region Validations of UnitOfMeasure
                        if (dr["UOM3UnitOfMeasure"].ToString() != string.Empty)
                        {
                            if (dr["UOM3UnitOfMeasure"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure (" + dr["UOM3UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.UnitOfMeasure = dr["UOM3UnitOfMeasure"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.UnitOfMeasure = dr["UOM3UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure3Item.UnitOfMeasure = dr["UOM3UnitOfMeasure"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure3Item.UnitOfMeasure = dr["UOM3UnitOfMeasure"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("UOM3UPC"))
                    {
                        #region Validations of UPC
                        if (dr["UOM3UPC"].ToString() != string.Empty)
                        {
                            if (dr["UOM3UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This UnitOfMeasure3 UPC (" + dr["UOM3UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        UnitOfMeasure3Item.UPC = dr["UOM3UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        UnitOfMeasure3Item.UPC = dr["UOM3UPC"].ToString();
                                    }
                                }
                                else
                                    UnitOfMeasure3Item.UPC = dr["UOM3UPC"].ToString();
                            }
                            else
                            {
                                UnitOfMeasure3Item.UPC = dr["UOM3UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (UnitOfMeasure3Item.ALU != null || UnitOfMeasure3Item.MSRP != null || UnitOfMeasure3Item.NumberOfBaseUnits != null || UnitOfMeasure3Item.Price1 != null || UnitOfMeasure3Item.Price2 != null || UnitOfMeasure3Item.Price3 != null || UnitOfMeasure3Item.Price4 != null || UnitOfMeasure3Item.Price5 != null || UnitOfMeasure3Item.UnitOfMeasure != null || UnitOfMeasure3Item.UPC != null)
                        ItemInventory.UnitOfMeasure3.Add(UnitOfMeasure3Item);


                    VendorInfo2 vendorinfo2 = new VendorInfo2();

                    if (dt.Columns.Contains("Vendor2ALU"))
                    {
                        #region Validations of ALU
                        if (dr["Vendor2ALU"].ToString() != string.Empty)
                        {
                            if (dr["Vendor2ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorInfo2 ALU (" + dr["Vendor2ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo2.ALU = dr["Vendor2ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo2.ALU = dr["Vendor2ALU"].ToString();
                                    }
                                }
                                else
                                    vendorinfo2.ALU = dr["Vendor2ALU"].ToString();
                            }
                            else
                            {
                                vendorinfo2.ALU = dr["Vendor2ALU"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("Vendor2OrderCost"))
                    {
                        #region Validations for MSRP
                        if (dr["Vendor2OrderCost"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["Vendor2OrderCost"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo2 Vendor2OrderCost( " + dr["Vendor2OrderCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo2.OrderCost = dr["Vendor2OrderCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo2.OrderCost = dr["Vendor2OrderCost"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorinfo2.OrderCost = dr["Vendor2OrderCost"].ToString();
                                }
                            }
                            else
                            {
                                vendorinfo2.OrderCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Vendor2OrderCost"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor2UPC"))
                    {
                        #region Validations of UPC
                        if (dr["Vendor2UPC"].ToString() != string.Empty)
                        {
                            if (dr["Vendor2UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorInfo2 UPC (" + dr["Vendor2UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo2.UPC = dr["Vendor2UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo2.UPC = dr["Vendor2UPC"].ToString();
                                    }
                                }
                                else
                                    vendorinfo2.UPC = dr["Vendor2UPC"].ToString();
                            }
                            else
                            {
                                vendorinfo2.UPC = dr["Vendor2UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor2VendorName"))
                    {
                        #region Validations of VendorListID
                        if (dr["Vendor2VendorName"].ToString() != string.Empty)
                        {
                            if (dr["Vendor2VendorName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor2 Name (" + dr["Vendor2VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo2.VendorListID = dr["Vendor2VendorName"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo2.VendorListID = dr["Vendor2VendorName"].ToString();
                                    }
                                }
                                else
                                    vendorinfo2.VendorListID = dr["Vendor2VendorName"].ToString();
                            }
                            else
                            {
                                vendorinfo2.VendorListID = dr["Vendor2VendorName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (vendorinfo2.ALU != null || vendorinfo2.OrderCost != null || vendorinfo2.UPC != null || vendorinfo2.VendorListID != null)
                        ItemInventory.VendorInfo2.Add(vendorinfo2);


                    //vendor3

                    VendorInfo3 vendorinfo3 = new VendorInfo3();

                    if (dt.Columns.Contains("Vendor3ALU"))
                    {
                        #region Validations of ALU
                        if (dr["Vendor3ALU"].ToString() != string.Empty)
                        {
                            if (dr["Vendor3ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo3 ALU (" + dr["Vendor3ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo3.ALU = dr["Vendor3ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo3.ALU = dr["Vendor3ALU"].ToString();
                                    }
                                }
                                else
                                    vendorinfo3.ALU = dr["Vendor3ALU"].ToString();
                            }
                            else
                            {
                                vendorinfo3.ALU = dr["Vendor3ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor3OrderCost"))
                    {
                        #region Validations for MSRP
                        if (dr["Vendor3OrderCost"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["Vendor3OrderCost"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo3 OrderCost( " + dr["Vendor3OrderCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo3.OrderCost = dr["Vendor3OrderCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo3.OrderCost = dr["Vendor3OrderCost"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorinfo3.OrderCost = dr["Vendor3OrderCost"].ToString();
                                }
                            }
                            else
                            {
                                vendorinfo3.OrderCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Vendor3OrderCost"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor3UPC"))
                    {
                        #region Validations of UPC
                        if (dr["Vendor3UPC"].ToString() != string.Empty)
                        {
                            if (dr["Vendor3UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo3 UPC (" + dr["Vendor3UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo3.UPC = dr["Vendor3UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo3.UPC = dr["Vendor3UPC"].ToString();
                                    }
                                }
                                else
                                    vendorinfo3.UPC = dr["Vendor3UPC"].ToString();
                            }
                            else
                            {
                                vendorinfo3.UPC = dr["Vendor3UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor3VendorName"))
                    {
                        #region Validations of VendorListID
                        if (dr["Vendor3VendorName"].ToString() != string.Empty)
                        {
                            if (dr["Vendor3VendorName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor3 Name(" + dr["Vendor3VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo3.VendorListID = dr["Vendor3VendorName"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo3.VendorListID = dr["Vendor3VendorName"].ToString();
                                    }
                                }
                                else
                                    vendorinfo3.VendorListID = dr["Vendor3VendorName"].ToString();
                            }
                            else
                            {
                                vendorinfo3.VendorListID = dr["Vendor3VendorName"].ToString();
                            }
                        }
                        #endregion
                    }
                  

                    if (vendorinfo3.ALU != null || vendorinfo3.OrderCost != null || vendorinfo3.UPC != null || vendorinfo3.VendorListID != null)
                        ItemInventory.VendorInfo3.Add(vendorinfo3);

                    //vendor4

                    VendorInfo4 vendorinfo4 = new VendorInfo4();

                    if (dt.Columns.Contains("Vendor4ALU"))
                    {
                        #region Validations of ALU
                        if (dr["Vendor4ALU"].ToString() != string.Empty)
                        {
                            if (dr["Vendor4ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo4 ALU (" + dr["Vendor4ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo4.ALU = dr["Vendor4ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo4.ALU = dr["Vendor4ALU"].ToString();
                                    }
                                }
                                else
                                    vendorinfo4.ALU = dr["Vendor4ALU"].ToString();
                            }
                            else
                            {
                                vendorinfo4.ALU = dr["Vendor4ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor4OrderCost"))
                    {
                        #region Validations for MSRP
                        if (dr["Vendor4OrderCost"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["Vendor4OrderCost"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo4 OrderCost( " + dr["Vendor4OrderCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo4.OrderCost = dr["Vendor4OrderCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo4.OrderCost = dr["Vendor4OrderCost"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorinfo4.OrderCost = dr["Vendor4OrderCost"].ToString();
                                }
                            }
                            else
                            {
                                vendorinfo4.OrderCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Vendor4OrderCost"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor4UPC"))
                    {
                        #region Validations of UPC
                        if (dr["Vendor4UPC"].ToString() != string.Empty)
                        {
                            if (dr["Vendor4UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo4 UPC (" + dr["Vendor4UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo4.UPC = dr["Vendor4UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo4.UPC = dr["Vendor4UPC"].ToString();
                                    }
                                }
                                else
                                    vendorinfo4.UPC = dr["Vendor4UPC"].ToString();
                            }
                            else
                            {
                                vendorinfo4.UPC = dr["Vendor4UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor4VendorName"))
                    {
                        #region Validations of VendorListID
                        if (dr["Vendor4VendorName"].ToString() != string.Empty)
                        {
                            if (dr["Vendor4VendorName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor4 VendorName  (" + dr["Vendor4VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo4.VendorListID = dr["Vendor4VendorName"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo4.VendorListID = dr["Vendor4VendorName"].ToString();
                                    }
                                }
                                else
                                    vendorinfo4.VendorListID = dr["Vendor4VendorName"].ToString();
                            }
                            else
                            {
                                vendorinfo4.VendorListID = dr["Vendor4VendorName"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (vendorinfo4.ALU != null || vendorinfo4.OrderCost != null || vendorinfo4.UPC != null || vendorinfo4.VendorListID != null)
                        ItemInventory.VendorInfo4.Add(vendorinfo4);


                    //Vendor5

                    VendorInfo5 vendorinfo5 = new VendorInfo5();

                    if (dt.Columns.Contains("Vendor5ALU"))
                    {
                        #region Validations of ALU
                        if (dr["Vendor5ALU"].ToString() != string.Empty)
                        {
                            if (dr["Vendor5ALU"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo5 ALU (" + dr["Vendor5ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo5.ALU = dr["Vendor5ALU"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo5.ALU = dr["Vendor5ALU"].ToString();
                                    }
                                }
                                else
                                    vendorinfo5.ALU = dr["Vendor5ALU"].ToString();
                            }
                            else
                            {
                                vendorinfo5.ALU = dr["Vendor5ALU"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor5OrderCost"))
                    {
                        #region Validations for MSRP
                        if (dr["Vendor5OrderCost"].ToString() != string.Empty)
                        {
                            decimal msrp;
                            if (!decimal.TryParse(dr["Vendor5OrderCost"].ToString(), out msrp))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo5 OrderCost( " + dr["Vendor5OrderCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo5.OrderCost = dr["Vendor5OrderCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo5.OrderCost = dr["Vendor5OrderCost"].ToString();
                                    }
                                }
                                else
                                {
                                    vendorinfo5.OrderCost = dr["Vendor5OrderCost"].ToString();
                                }
                            }
                            else
                            {
                                vendorinfo5.OrderCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Vendor5OrderCost"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor5UPC"))
                    {
                        #region Validations of UPC
                        if (dr["Vendor5UPC"].ToString() != string.Empty)
                        {
                            if (dr["Vendor5UPC"].ToString().Length > 18)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This vendorinfo5 UPC (" + dr["Vendor5UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo5.UPC = dr["Vendor5UPC"].ToString().Substring(0, 18);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo5.UPC = dr["Vendor5UPC"].ToString();
                                    }
                                }
                                else
                                    vendorinfo5.UPC = dr["Vendor5UPC"].ToString();
                            }
                            else
                            {
                                vendorinfo5.UPC = dr["Vendor5UPC"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Vendor5VendorName"))
                    {
                        #region Validations of VendorListID
                        if (dr["Vendor5VendorName"].ToString() != string.Empty)
                        {
                            if (dr["Vendor5VendorName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor5 Name (" + dr["Vendor5VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendorinfo5.VendorListID = dr["Vendor5VendorName"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendorinfo5.VendorListID = dr["Vendor5VendorName"].ToString();
                                    }
                                }
                                else
                                    vendorinfo5.VendorListID = dr["Vendor5VendorName"].ToString();
                            }
                            else
                            {
                                vendorinfo5.VendorListID = dr["Vendor5VendorName"].ToString();
                            }
                        }
                        #endregion
                    }
                   
                    if (vendorinfo5.ALU != null || vendorinfo5.OrderCost != null || vendorinfo5.UPC != null || vendorinfo5.VendorListID != null)
                        ItemInventory.VendorInfo5.Add(vendorinfo5);

                    coll.Add(ItemInventory);
                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }

    }
}
