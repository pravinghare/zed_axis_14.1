﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
  public class POSPriceDiscountQBEntry
    {
        #region Private Member Variable
        private string m_PriceDiscountName;
        private string m_PriceDiscountReason;
        private string m_Comments;
        private string m_Associate;
        private string m_PriceDiscountType;
        private string m_IsInactive;
        private string m_PriceDiscountPriceLevels;
        private string m_PriceDiscountXValue;
        private string m_PriceDiscountYValue;
        private string m_IsApplicableOverXValue;
        private string m_StartDate;
        private string m_StopDate;
        private Collection<PriceDiscountItemAdd> m_PriceDiscountItemAdd = new Collection<PriceDiscountItemAdd>();       

       
        #endregion

        #region Constructor
        public POSPriceDiscountQBEntry()
        {
        }
        #endregion

        #region Properties
        public string PriceDiscountName
        {
            get { return m_PriceDiscountName; }
            set { m_PriceDiscountName = value; }
        }

        public string PriceDiscountReason
        {
            get { return m_PriceDiscountReason; }
            set { m_PriceDiscountReason = value; }
        }

        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }

        public string PriceDiscountType
        {
            get { return m_PriceDiscountType; }
            set { m_PriceDiscountType = value; }
        }

        public string IsInactive
        {
            get { return m_IsInactive; }
            set { m_IsInactive = value; }
        }

        public string PriceDiscountPriceLevels
        {
            get { return m_PriceDiscountPriceLevels; }
            set { m_PriceDiscountPriceLevels = value; }
        }

        public string PriceDiscountXValue
        {
            get { return m_PriceDiscountXValue; }
            set { m_PriceDiscountXValue = value; }
        }

        public string PriceDiscountYValue
        {
            get { return m_PriceDiscountYValue; }
            set { m_PriceDiscountYValue = value; }
        }

        public string IsApplicableOverXValue
        {
            get { return m_IsApplicableOverXValue; }
            set { m_IsApplicableOverXValue = value; }
        }


        [XmlElement(DataType = "string")]
        public string StartDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_StartDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_StartDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_StartDate = value;
            }
        }


        [XmlElement(DataType = "string")]
        public string StopDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_StopDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_StopDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_StopDate = value;
            }
        }


        [XmlArray("PriceDiscountItemAddREM")]
        public Collection<PriceDiscountItemAdd> PriceDiscountItemAdd
        {
            get { return m_PriceDiscountItemAdd; }
            set { m_PriceDiscountItemAdd = value; }
        }
        #endregion

        #region Public Methods
      /// <summary>
        /// Creating new customer transaction for pos.
      /// </summary>
      /// <param name="statusMessage"></param>
      /// <param name="requestText"></param>
      /// <param name="rowcount"></param>
      /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPriceDiscountQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement PriceDiscountAddRq = inputXMLDoc.CreateElement("PriceDiscountAddRq");
            qbXMLMsgsRq.AppendChild(PriceDiscountAddRq);
            PriceDiscountAddRq.SetAttribute("requestID", "1");
            XmlElement PriceDiscountAdd = inputXMLDoc.CreateElement("PriceDiscountAdd");
            PriceDiscountAddRq.AppendChild(PriceDiscountAdd);

            requestXML = requestXML.Replace("<PriceDiscountItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PriceDiscountItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PriceDiscountItemAddREM>", string.Empty);

            PriceDiscountAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceDiscountAddRs"))
                    {
                      
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceDiscountAddRs/PriceDiscountRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPriceDiscount(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

      /// <summary>      
///This method is used for updating price discount information of existing with listid 
      /// </summary>
      /// <param name="statusMessage"></param>
      /// <param name="requestText"></param>
      /// <param name="rowcount"></param>
      /// <param name="txnID"></param>
      /// <returns></returns>
        public bool UpdatePriceDiscountInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string txnID)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPriceDiscountQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement PriceDiscountModRq = inputXMLDoc.CreateElement("PriceDiscountModRq");
            qbXMLMsgsRq.AppendChild(PriceDiscountModRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement PriceDiscountMod = inputXMLDoc.CreateElement("PriceDiscountMod");
            PriceDiscountModRq.AppendChild(PriceDiscountMod);

            requestXML = requestXML.Replace("<PriceDiscountItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PriceDiscountItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PriceDiscountItemAddREM>", string.Empty);

            requestXML = requestXML.Replace("<PriceDiscountItemAdd>", "<PriceDiscountItemMod>");
            requestXML = requestXML.Replace("</PriceDiscountItemAdd>", "</PriceDiscountItemMod>");

            requestXML = requestXML.Replace("</ListID>", "</ListID><TxnLineID>-1</TxnLineID>");

            PriceDiscountMod.InnerXml = requestXML;
          
            requestText = inputXMLDoc.OuterXml;

            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PriceDiscountModRq/PriceDiscountMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement TxnID = inputXMLDoc.CreateElement("TxnID");
          
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PriceDiscountModRq/PriceDiscountMod").InsertBefore(TxnID, firstChild).InnerText = txnID;

            ////Create LoginName aggregate and fill in field values for it
            
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceDiscountModRs"))
                    {
                       
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceDiscountModRs/PriceDiscountRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPriceDiscount(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion

    }

  public class POSPriceDiscountQBEntryCollection : Collection<POSPriceDiscountQBEntry>
  {
      /// <summary>
      /// check existing pricediscount
      /// </summary>
      /// <param name="priceDiscount"></param>
      /// <returns></returns>
      public POSPriceDiscountQBEntry FindpriceDiscountNameEntry(string priceDiscount)
      {
          foreach (POSPriceDiscountQBEntry item in this)
          {
              if (item.PriceDiscountName == priceDiscount)
              {
                  return item;
              }
          }
          return null;
      }
  }

  public enum PriceDiscountType
  {
      YPercentOff, 
      BuyXForY,
      BuyXGetYPercentOff
  }
}
