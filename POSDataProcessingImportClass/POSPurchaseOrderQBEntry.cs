﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
     [XmlRootAttribute("POSPurchaseOrderQBEntry", Namespace = "", IsNullable = false)]
   public class POSPurchaseOrderQBEntry
   {
        #region private member
        private string m_Associate;       
        private string m_CancelDate; 
        private string m_Discount;   
        private string m_DiscountPercent;  
        private string m_Fee;
        private string m_Instructions; 
        private string m_PurchaseOrderNumber;        
        private string m_PurchaseOrderStatusDesc;        
        private string m_ShipToStoreNumber;  
        private string m_StartShipDate;        
        private string m_StoreNumber; 
        private string m_TermsDiscount;        
        private string m_TermsDiscountDays;        
        private string m_TermsNetDays;  
        private string m_TxnDate;    
        private string m_VendorListID;
        private Collection<PurchaseOrderItemAdd> m_PurchaseOrderItemAdd = new Collection<PurchaseOrderItemAdd>();
        #endregion

        #region Constructor
        public POSPurchaseOrderQBEntry()
        {
        }
        #endregion

        #region Private Member Variable
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }

        public string CancelDate
        {
            get { return m_CancelDate; }
            set { m_CancelDate = value; }
        }

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }        

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }

        public string Fee
        {
            get { return m_Fee; }
            set { m_Fee = value; }
        }  
        public string Instructions
        {
            get { return m_Instructions; }
            set { m_Instructions = value; }
        }

        public string PurchaseOrderNumber
        {
            get { return m_PurchaseOrderNumber; }
            set { m_PurchaseOrderNumber = value; }
        }

        public string PurchaseOrderStatusDesc
        {
            get { return m_PurchaseOrderStatusDesc; }
            set { m_PurchaseOrderStatusDesc = value; }
        }

        public string ShipToStoreNumber
        {
            get { return m_ShipToStoreNumber; }
            set { m_ShipToStoreNumber = value; }
        }

        public string StartShipDate
        {
            get { return m_StartShipDate; }
            set { m_StartShipDate = value; }
        }

        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }
        public string TermsDiscount
        {
            get { return m_TermsDiscount; }
            set { m_TermsDiscount = value; }
        }
        public string TermsDiscountDays
        {
            get { return m_TermsDiscountDays; }
            set { m_TermsDiscountDays = value; }
        }
        public string TermsNetDays
        {
            get { return m_TermsNetDays; }
            set { m_TermsNetDays = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }
         
        [XmlArray("PurchaseOrderItemAddREM")]
        public Collection<PurchaseOrderItemAdd> PurchaseOrderItemAdd
        {
            get { return m_PurchaseOrderItemAdd; }
            set { m_PurchaseOrderItemAdd = value; }
        }

        #endregion

        #region  Public Methods
         /// <summary>
        /// Creating new customer transaction for pos.
         /// </summary>
         /// <param name="statusMessage"></param>
         /// <param name="requestText"></param>
         /// <param name="rowcount"></param>
         /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPurchaseOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement PurchaseOrderAddRq = inputXMLDoc.CreateElement("PurchaseOrderAddRq");
            qbXMLMsgsRq.AppendChild(PurchaseOrderAddRq);
            PurchaseOrderAddRq.SetAttribute("requestID", "1");
            XmlElement PurchaseOrderAdd = inputXMLDoc.CreateElement("PurchaseOrderAdd");
            PurchaseOrderAddRq.AppendChild(PurchaseOrderAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderItemAddREM>", string.Empty);

            PurchaseOrderAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {
                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PurchaseOrderAddRs"))
                    {

                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PurchaseOrderAddRs/PurchaseOrderRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPruchaseOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        //update pos purchase
         /// <summary>
         /// update transaction by using listid
         /// </summary>
         /// <param name="statusMessage"></param>
         /// <param name="requestText"></param>
         /// <param name="rowcount"></param>
         /// <param name="txnId"></param>
         /// <param name="purchaseOrderNumber"></param>
         /// <returns></returns>
        public bool UpdatePurchaseOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string txnId, string purchaseOrderNumber)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPurchaseOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement PurchaseOrderModRq = inputXMLDoc.CreateElement("PurchaseOrderModRq");
            qbXMLMsgsRq.AppendChild(PurchaseOrderModRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement PurchaseOrderMod = inputXMLDoc.CreateElement("PurchaseOrderMod");
            PurchaseOrderModRq.AppendChild(PurchaseOrderMod);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<PurchaseOrderItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderItemAddREM>", string.Empty);

            requestXML = requestXML.Replace("<PurchaseOrderItemAdd>", "<PurchaseOrderItemMod>");
            requestXML = requestXML.Replace("</PurchaseOrderItemAdd>", "</PurchaseOrderItemMod>");

            //Axis 565 reopen
            if (requestXML.Contains("<UPC>"))
            {
                requestXML = requestXML.Replace("<ListID>", "<TxnLineID>-1</TxnLineID><ListID>");
            }
            else
            {
                requestXML = requestXML.Replace("<Desc1>", "<TxnLineID>-1</TxnLineID><Desc1>");
            }
            //Axis 565 reopen ends

            PurchaseOrderMod.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;

            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement TxnID = inputXMLDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertBefore(TxnID, firstChild).InnerText = txnId;

            //Create LoginName aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderNumber = inputXMLDoc.CreateElement("PurchaseOrderNumber");
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertBefore(PurchaseOrderNumber, TxnID).InnerText = purchaseOrderNumber;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PurchaseOrderModRs"))
                    {
                        
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PurchaseOrderModRs/PurchaseOrderRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPruchaseOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion

   }

     public class POSPurchaseOrderQBEntryCollection : Collection<POSPurchaseOrderQBEntry>
     {
         /// <summary>
         /// get PO number
         /// </summary>
         /// <param name="purchaseOrderNumber"></param>
         /// <returns></returns>
         public POSPurchaseOrderQBEntry FindpurchaseOrderNumberEntry(string purchaseOrderNumber)
         {
             foreach (POSPurchaseOrderQBEntry item in this)
             {
                 if (item.PurchaseOrderNumber == purchaseOrderNumber)
                 {
                     return item;
                 }
             }
             return null;
         }
     }

}
