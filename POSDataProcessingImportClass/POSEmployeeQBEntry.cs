﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;


namespace POSDataProcessingImportClass
{
    [XmlRootAttribute("POSEmployeeQBEntry", Namespace = "", IsNullable = false)]
  public  class POSEmployeeQBEntry
    {
        #region Private Member Variable
        private string m_City;
        private string m_CommissionPercent;
        private string m_Country;
        private string m_EMail;
        private string m_FirstName;
        private string m_IsTrackingHours;
        private string m_LastName;
        private string m_LoginName;
        private string m_Notes;
        private string m_Phone;
        private string m_AltPhone;
        private string m_Contact;
        private string m_AltContact;
        private string m_PostalCode;
        private string m_State;
        private string m_Street;
        private string m_Street2;
        private string m_SecurityGroup;
        
        #endregion
       
        #region Constructor
        public POSEmployeeQBEntry()
        {
        }
        #endregion

        #region  Properties
        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string CommissionPercent
        {
            get { return m_CommissionPercent; }
            set { m_CommissionPercent = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        
        public string Email
        {
            get { return m_EMail; }
            set { m_EMail = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        public string IsTrackingHours
        {
            get { return m_IsTrackingHours; }
            set { m_IsTrackingHours = value; }
        }

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public string LoginName
        {
            get { return m_LoginName; }
            set { m_LoginName = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Phone2
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }

        public string Phone3
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string Phone4
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string Street
        {
            get { return m_Street; }
            set { m_Street = value; }
        }

        public string Street2
        {
            get { return m_Street2; }
            set { m_Street2 = value; }
        }

        public string SecurityGroup
        {
            get { return m_SecurityGroup; }
            set { m_SecurityGroup = value; }
        }
        #endregion

        #region  Public Methods
        /// <summary>
        /// Creating request file for exporting data to POS.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSEmployeeQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement EmployeeAddRq = inputXMLDoc.CreateElement("EmployeeAddRq");
            qbXMLMsgsRq.AppendChild(EmployeeAddRq);
            EmployeeAddRq.SetAttribute("requestID", "1");
            XmlElement EmployeeAdd = inputXMLDoc.CreateElement("EmployeeAdd");
            EmployeeAddRq.AppendChild(EmployeeAdd);

            
            EmployeeAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/EmployeeAddRs"))
                    {
                       
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if(statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/EmployeeAddRs/EmployeeRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofEmployee(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        //update pos customer
        /// <summary>
        /// This method is used for updating Employee information of existing transaction by using listid 
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="listID"></param>
        /// <param name="loginName"></param>
        /// <returns></returns>

        public bool UpdateEmployeeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string listID, string loginName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSEmployeeQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement EmployeeModRq = inputXMLDoc.CreateElement("EmployeeModRq");
            qbXMLMsgsRq.AppendChild(EmployeeModRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement EmployeeMod = inputXMLDoc.CreateElement("EmployeeMod");
            EmployeeModRq.AppendChild(EmployeeMod);


           EmployeeMod.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;


            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/EmployeeModRq/EmployeeMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = inputXMLDoc.CreateElement("ListID");
           
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/EmployeeModRq/EmployeeMod").InsertBefore(ListID, firstChild).InnerText = listID;

            //Create LoginName aggregate and fill in field values for it
            System.Xml.XmlElement LoginName = inputXMLDoc.CreateElement("LoginName");
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/EmployeeModRq/EmployeeMod").InsertBefore(LoginName, ListID).InnerText = loginName;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string requesterror = string.Empty;
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/EmployeeModRs"))
                    {
                       
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/EmployeeModRs/EmployeeRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                        }                       
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofEmployee(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }


    public class POSEmployeeQBEntryCollection : Collection<POSEmployeeQBEntry>
    {

    }

}
