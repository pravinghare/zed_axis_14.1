﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;


namespace POSDataProcessingImportClass
{
  public class ImportPOSPurchaseOrderClass
    {
       private static ImportPOSPurchaseOrderClass m_ImportPOSPurchaseOrderClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSPurchaseOrderClass()
        {   }

        #endregion

        public static ImportPOSPurchaseOrderClass GetInstance()
        {
            if (m_ImportPOSPurchaseOrderClass == null)
                m_ImportPOSPurchaseOrderClass = new ImportPOSPurchaseOrderClass();
            return m_ImportPOSPurchaseOrderClass;
        }


        public POSDataProcessingImportClass.POSPurchaseOrderQBEntryCollection ImportPOSPurchaseOrderData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSPurchaseOrderQBEntryCollection coll = new POSPurchaseOrderQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            // int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    //   DateTime EmployeeDt = new DateTime();
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSPurchaseOrderQBEntry PurchaseOrder = new POSPurchaseOrderQBEntry();

                    if (dt.Columns.Contains("PurchaseOrderNumber"))
                    {
                        PurchaseOrder = coll.FindpurchaseOrderNumberEntry(dr["PurchaseOrderNumber"].ToString());

                        if (PurchaseOrder == null)
                        {
                            PurchaseOrder = new POSPurchaseOrderQBEntry();
                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Associate = dr["Associate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CancelDate"))
                            {
                                #region validations of CancelDate
                                if (dr["CancelDate"].ToString() != "<None>" || dr["CancelDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["CancelDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This CancelDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.CancelDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.CancelDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.CancelDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            PurchaseOrder.CancelDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        PurchaseOrder.CancelDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Discount"))
                            {
                                #region Validations for Discount
                                if (dr["Discount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Discount = dr["Discount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Discount = dr["Discount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountPercent"))
                            {
                                #region Validations for DiscountPercent
                                if (dr["DiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Fee"))
                            {
                                #region Validations for Fee
                                if (dr["Fee"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Fee"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fee( " + dr["Fee"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Fee = dr["Fee"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Fee = dr["Fee"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Fee = dr["Fee"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Fee = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Fee"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Instructions"))
                            {
                                #region Validations of Instructions
                                if (dr["Instructions"].ToString() != string.Empty)
                                {
                                    if (dr["Instructions"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Instructions (" + dr["Instructions"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Instructions = dr["Instructions"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PurchaseOrderNumber"))
                            {
                                #region Validations of PurchaseOrderNumber
                                if (dr["PurchaseOrderNumber"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrderNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseOrderNumber (" + dr["PurchaseOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PurchaseOrderStatusDesc"))
                            {
                                #region Validations of PurchaseOrderStatusDesc
                                if (dr["PurchaseOrderStatusDesc"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrderStatusDesc"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Status Desc (" + dr["PurchaseOrderStatusDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipToStoreNumber"))
                            {
                                #region Validations of ShipToStoreNumber
                                if (dr["ShipToStoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["ShipToStoreNumber"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipTo StoreNumber (" + dr["ShipToStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StartShipDate"))
                            {
                                #region validations of StartShipDate
                                if (dr["StartShipDate"].ToString() != "<None>" || dr["StartShipDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["StartShipDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This StartShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.StartShipDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.StartShipDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.StartShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            PurchaseOrder.StartShipDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        PurchaseOrder.StartShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StoreNumber"))
                            {
                                #region Validations of StoreNumber
                                if (dr["StoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["StoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TermsDiscount"))
                            {
                                #region Validations for TermsDiscount
                                if (dr["TermsDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TermsDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms Discount( " + dr["TermsDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsDiscount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TermsDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TermsDiscountDays"))
                            {
                                #region Validations of TermsDiscountDays
                                if (dr["TermsDiscountDays"].ToString() != string.Empty)
                                {
                                    if (dr["TermsDiscountDays"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms DiscountDays(" + dr["TermsDiscountDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString().Substring(0, 1000);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TermsNetDays"))
                            {
                                #region Validations of TermsNetDays
                                if (dr["TermsNetDays"].ToString() != string.Empty)
                                {
                                    if (dr["TermsNetDays"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms DiscountDays(" + dr["TermsNetDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString().Substring(0, 1000);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            PurchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        PurchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorName"))
                            {
                                #region Validations of VendorName
                                if (dr["VendorName"].ToString() != string.Empty)
                                {
                                    if (dr["VendorName"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Name (" + dr["VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.VendorListID = dr["VendorName"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                    }
                                }
                                #endregion
                            }
                           
                            QBPOSEntities.PurchaseOrderItemAdd PurchaseOrderItemAdd = new QBPOSEntities.PurchaseOrderItemAdd();

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of ALU
                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            PurchaseOrderItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, PurchaseOrderItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Attribute"))
                            {
                                #region Validations of Attribute
                                if (dr["Attribute"].ToString() != string.Empty)
                                {
                                    if (dr["Attribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Desc2"))
                            {
                                #region Validations of Desc2
                                if (dr["Desc2"].ToString() != string.Empty)
                                {
                                    if (dr["Desc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ExtendedCost"))
                            {
                                #region Validations for ExtendedCost
                                if (dr["ExtendedCost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Qty"))
                            {
                                #region Validations of Qty
                                if (dr["Qty"].ToString() != string.Empty)
                                {
                                    if (dr["Qty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Quantity (" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Qty = dr["Qty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Size"))
                            {
                                #region Validations of Size
                                if (dr["Size"].ToString() != string.Empty)
                                {
                                    if (dr["Size"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of UPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            //Axis 565 reopen 
                                            PurchaseOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, PurchaseOrderItemAdd.UPC, null);
                                            if (PurchaseOrderItemAdd.ListID != null)
                                            {
                                                PurchaseOrderItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (PurchaseOrderItemAdd.ALU != null || PurchaseOrderItemAdd.Attribute != null || PurchaseOrderItemAdd.Cost != null || PurchaseOrderItemAdd.Desc1 != null || PurchaseOrderItemAdd.Desc2 != null || PurchaseOrderItemAdd.ExtendedCost != null || PurchaseOrderItemAdd.Qty != null || PurchaseOrderItemAdd.Size != null || PurchaseOrderItemAdd.UnitOfMeasure != null || PurchaseOrderItemAdd.UPC != null)
                                PurchaseOrder.PurchaseOrderItemAdd.Add(PurchaseOrderItemAdd);

                            coll.Add(PurchaseOrder);
                        }
                        else
                        {
                            QBPOSEntities.PurchaseOrderItemAdd PurchaseOrderItemAdd = new QBPOSEntities.PurchaseOrderItemAdd();

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of ALU
                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            PurchaseOrderItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, PurchaseOrderItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Attribute"))
                            {
                                #region Validations of Attribute
                                if (dr["Attribute"].ToString() != string.Empty)
                                {
                                    if (dr["Attribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Desc2"))
                            {
                                #region Validations of Desc2
                                if (dr["Desc2"].ToString() != string.Empty)
                                {
                                    if (dr["Desc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ExtendedCost"))
                            {
                                #region Validations for ExtendedCost
                                if (dr["ExtendedCost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Qty"))
                            {
                                #region Validations of Qty
                                if (dr["Qty"].ToString() != string.Empty)
                                {
                                    if (dr["Qty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Quantity (" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Qty = dr["Qty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Size"))
                            {
                                #region Validations of Size
                                if (dr["Size"].ToString() != string.Empty)
                                {
                                    if (dr["Size"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of UPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            //Axis 565 reopen
                                            PurchaseOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, PurchaseOrderItemAdd.UPC,null);
                                            if (PurchaseOrderItemAdd.ListID != null)
                                            {
                                                PurchaseOrderItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (PurchaseOrderItemAdd.ALU != null || PurchaseOrderItemAdd.Attribute != null || PurchaseOrderItemAdd.Cost != null || PurchaseOrderItemAdd.Desc1 != null || PurchaseOrderItemAdd.Desc2 != null || PurchaseOrderItemAdd.ExtendedCost != null || PurchaseOrderItemAdd.Qty != null || PurchaseOrderItemAdd.Size != null || PurchaseOrderItemAdd.UnitOfMeasure != null || PurchaseOrderItemAdd.UPC != null)
                                PurchaseOrder.PurchaseOrderItemAdd.Add(PurchaseOrderItemAdd);
                        }
                    }
                    else
                    {
                        PurchaseOrder = new POSPurchaseOrderQBEntry();
                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Associate = dr["Associate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("CancelDate"))
                        {
                            #region validations of CancelDate
                            if (dr["CancelDate"].ToString() != "<None>" || dr["CancelDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["CancelDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CancelDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.CancelDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.CancelDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.CancelDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        PurchaseOrder.CancelDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    PurchaseOrder.CancelDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Discount"))
                        {
                            #region Validations for Discount
                            if (dr["Discount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Discount = dr["Discount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Discount = dr["Discount"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountPercent"))
                        {
                            #region Validations for DiscountPercent
                            if (dr["DiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Fee"))
                        {
                            #region Validations for Fee
                            if (dr["Fee"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Fee"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Fee( " + dr["Fee"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Fee = dr["Fee"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Fee = dr["Fee"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Fee = dr["Fee"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Fee = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Fee"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Instructions"))
                        {
                            #region Validations of Instructions
                            if (dr["Instructions"].ToString() != string.Empty)
                            {
                                if (dr["Instructions"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Instructions (" + dr["Instructions"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Instructions = dr["Instructions"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Instructions = dr["Instructions"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PurchaseOrderNumber"))
                        {
                            #region Validations of PurchaseOrderNumber
                            if (dr["PurchaseOrderNumber"].ToString() != string.Empty)
                            {
                                if (dr["PurchaseOrderNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PurchaseOrderNumber (" + dr["PurchaseOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.PurchaseOrderNumber = dr["PurchaseOrderNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PurchaseOrderStatusDesc"))
                        {
                            #region Validations of PurchaseOrderStatusDesc
                            if (dr["PurchaseOrderStatusDesc"].ToString() != string.Empty)
                            {
                                if (dr["PurchaseOrderStatusDesc"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Status Desc (" + dr["PurchaseOrderStatusDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.PurchaseOrderStatusDesc = dr["PurchaseOrderStatusDesc"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipToStoreNumber"))
                        {
                            #region Validations of ShipToStoreNumber
                            if (dr["ShipToStoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["ShipToStoreNumber"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipTo StoreNumber (" + dr["ShipToStoreNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.ShipToStoreNumber = dr["ShipToStoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StartShipDate"))
                        {
                            #region validations of StartShipDate
                            if (dr["StartShipDate"].ToString() != "<None>" || dr["StartShipDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["StartShipDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This StartShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.StartShipDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.StartShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.StartShipDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        PurchaseOrder.StartShipDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    PurchaseOrder.StartShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StoreNumber"))
                        {
                            #region Validations of StoreNumber
                            if (dr["StoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["StoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.StoreNumber = dr["StoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TermsDiscount"))
                        {
                            #region Validations for TermsDiscount
                            if (dr["TermsDiscount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TermsDiscount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms Discount( " + dr["TermsDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsDiscount = dr["TermsDiscount"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.TermsDiscount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TermsDiscount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("TermsDiscountDays"))
                        {
                            #region Validations of TermsDiscountDays
                            if (dr["TermsDiscountDays"].ToString() != string.Empty)
                            {
                                if (dr["TermsDiscountDays"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms DiscountDays(" + dr["TermsDiscountDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString().Substring(0, 1000);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TermsNetDays"))
                        {
                            #region Validations of TermsNetDays
                            if (dr["TermsNetDays"].ToString() != string.Empty)
                            {
                                if (dr["TermsNetDays"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms DiscountDays(" + dr["TermsNetDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString().Substring(0, 1000);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.TermsNetDays = dr["TermsNetDays"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        PurchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    PurchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorName"))
                        {
                            #region Validations of VendorName
                            if (dr["VendorName"].ToString() != string.Empty)
                            {
                                if (dr["VendorName"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Name (" + dr["VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.VendorListID = dr["VendorName"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.VendorListID = dr["VendorName"].ToString();
                                }
                            }
                            #endregion
                        }
                     
                        QBPOSEntities.PurchaseOrderItemAdd PurchaseOrderItemAdd = new QBPOSEntities.PurchaseOrderItemAdd();

                        if (dt.Columns.Contains("ALU"))
                        {
                            #region Validations of ALU
                            if (dr["ALU"].ToString() != string.Empty)
                            {
                                if (dr["ALU"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.ALU = dr["ALU"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().ALULookup == true)
                                    {
                                        PurchaseOrderItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, PurchaseOrderItemAdd.ALU);
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Attribute"))
                        {
                            #region Validations of Attribute
                            if (dr["Attribute"].ToString() != string.Empty)
                            {
                                if (dr["Attribute"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Attribute = dr["Attribute"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Cost"))
                        {
                            #region Validations for Cost
                            if (dr["Cost"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Cost = dr["Cost"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Desc2"))
                        {
                            #region Validations of Desc2
                            if (dr["Desc2"].ToString() != string.Empty)
                            {
                                if (dr["Desc2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Desc2 = dr["Desc2"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ExtendedCost"))
                        {
                            #region Validations for ExtendedCost
                            if (dr["ExtendedCost"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Qty"))
                        {
                            #region Validations of Qty
                            if (dr["Qty"].ToString() != string.Empty)
                            {
                                if (dr["Qty"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Quantity (" + dr["Qty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Qty = dr["Qty"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Qty = dr["Qty"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Size"))
                        {
                            #region Validations of Size
                            if (dr["Size"].ToString() != string.Empty)
                            {
                                if (dr["Size"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.Size = dr["Size"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations of UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("UPC"))
                        {
                            #region Validations of UPC
                            if (dr["UPC"].ToString() != string.Empty)
                            {
                                if (dr["UPC"].ToString().Length > 18)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderItemAdd.UPC = dr["UPC"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        //Axis 565 reopen
                                        PurchaseOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, PurchaseOrderItemAdd.UPC,null);
                                        if (PurchaseOrderItemAdd.ListID != null)
                                        {
                                            PurchaseOrderItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (PurchaseOrderItemAdd.ALU != null || PurchaseOrderItemAdd.Attribute != null || PurchaseOrderItemAdd.Cost != null || PurchaseOrderItemAdd.Desc1 != null || PurchaseOrderItemAdd.Desc2 != null || PurchaseOrderItemAdd.ExtendedCost != null || PurchaseOrderItemAdd.Qty != null || PurchaseOrderItemAdd.Size != null || PurchaseOrderItemAdd.UnitOfMeasure != null || PurchaseOrderItemAdd.UPC != null)
                            PurchaseOrder.PurchaseOrderItemAdd.Add(PurchaseOrderItemAdd);

                        coll.Add(PurchaseOrder);
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }

    }
}
