﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
    [XmlRootAttribute("POSInventoryQtyAdjustmentQBEntry", Namespace = "", IsNullable = false)]
    public class POSInventoryQtyAdjustmentQBEntry
    {
        #region Private Member Variable
        private string m_Associate;
        private string m_Comments;
        private string m_InventoryAdjustmentSource;
        private string m_QuickBooksFlag;
        private string m_RefNumber;
        private string m_Reason;
        private string m_StoreNumber;
        private string m_TxnDate;
        private string m_TxnState;
        private string m_Workstation;

        private Collection<InventoryQtyAdjustmentItemAdd> m_InventoryQtyAdjustmentItemAdd = new Collection<InventoryQtyAdjustmentItemAdd>();

        #endregion

        #region Constructor
        public POSInventoryQtyAdjustmentQBEntry()
        {
        }
        #endregion

        #region Properties
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }

        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        public string InventoryAdjustmentSource
        {
            get { return m_InventoryAdjustmentSource; }
            set { m_InventoryAdjustmentSource = value; }
        }
        public string QuickBooksFlag
        {
            get { return m_QuickBooksFlag; }
            set { m_QuickBooksFlag = value; }
        }
        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }
        public string Reason
        {
            get { return m_Reason; }
            set { m_Reason = value; }
        }

        public string StoreNumber
        {
            get { return m_StoreNumber; }
            set { m_StoreNumber = value; }
        }

        public string TxnDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }



        [XmlArray("InventoryQtyAdjustmentItemAddREM")]
        public Collection<InventoryQtyAdjustmentItemAdd> InventoryQtyAdjustmentItemAdd
        {
            get { return m_InventoryQtyAdjustmentItemAdd; }
            set { m_InventoryQtyAdjustmentItemAdd = value; }
        }

        #endregion

        #region Public Methods

        /// Creating request file for exporting data to quickbook.
        
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSInventoryQtyAdjustmentQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement InventoryQtyAdjustmentAddRq = inputXMLDoc.CreateElement("InventoryQtyAdjustmentAddRq");
            qbXMLMsgsRq.AppendChild(InventoryQtyAdjustmentAddRq);
            InventoryQtyAdjustmentAddRq.SetAttribute("requestID", "1");
            XmlElement InventoryQtyAdjustmentAdd = inputXMLDoc.CreateElement("InventoryQtyAdjustmentAdd");
            InventoryQtyAdjustmentAddRq.AppendChild(InventoryQtyAdjustmentAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);
            requestXML = requestXML.Replace("<UPC/>", string.Empty);
            requestXML = requestXML.Replace("<UPC />", string.Empty);
            requestXML = requestXML.Replace("<Desc1 />", string.Empty);
            requestXML = requestXML.Replace("<Desc1/>", string.Empty);
            requestXML = requestXML.Replace("<InventoryQtyAdjustmentItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InventoryQtyAdjustmentItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryQtyAdjustmentItemAddREM>", string.Empty);

            InventoryQtyAdjustmentAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;

            foreach (System.Xml.XmlNode oNode in inputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRq/InventoryQtyAdjustmentAddRq/InventoryQtyAdjustmentAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    XmlNode node = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/InventoryQtyAdjustmentAddRq/InventoryQtyAdjustmentAdd/RefNumber");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();
                }
            }

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/InventoryQtyAdjustmentAddRs"))
                    {

                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/InventoryQtyAdjustmentAddRs/InventoryQtyAdjustmentRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofInventoryQtyAdjust(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class POSInventoryQtyAdjustmentQBEntryCollection : Collection<POSInventoryQtyAdjustmentQBEntry>
    {
        public POSInventoryQtyAdjustmentQBEntry FindRefNumberEntry(string refNumber)
        {
            foreach (POSInventoryQtyAdjustmentQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
