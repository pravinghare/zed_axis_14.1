﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using POSDataProcessingImportClass;

namespace POSDataProcessingImportClass
{
   public class ImportPOSVendorClass
    {
         private static ImportPOSVendorClass m_ImportPOSVendorClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSVendorClass()
        {   }

        #endregion
        /// <summary>
        /// creating new instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSVendorClass GetInstance()
        {
            if (m_ImportPOSVendorClass == null)
                m_ImportPOSVendorClass = new ImportPOSVendorClass();
            return m_ImportPOSVendorClass;
        }       
    }
}
