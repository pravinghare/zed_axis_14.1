﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
   public class ImportPOSInventoryQtyAdjustmentClass
    {
        private static ImportPOSInventoryQtyAdjustmentClass m_ImportPOSInventoryQtyAdjustmentClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSInventoryQtyAdjustmentClass()
        {   }

        #endregion

       /// <summary>
       /// creating instance of class 
       /// </summary>
       /// <returns></returns>
        public static ImportPOSInventoryQtyAdjustmentClass GetInstance()
        {
            if (m_ImportPOSInventoryQtyAdjustmentClass == null)
                m_ImportPOSInventoryQtyAdjustmentClass = new ImportPOSInventoryQtyAdjustmentClass();
            return m_ImportPOSInventoryQtyAdjustmentClass;
        }

       /// <summary>
        /// /// Creating new inventory qty adjustment transaction for pos.
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <param name="dt"></param>
       /// <param name="logDirectory"></param>
       /// <returns></returns>
        public POSDataProcessingImportClass.POSInventoryQtyAdjustmentQBEntryCollection ImportPOSInventoryQtyAdjustData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSInventoryQtyAdjustmentQBEntryCollection coll = new POSInventoryQtyAdjustmentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            // int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;
                    DateTime SODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSInventoryQtyAdjustmentQBEntry ItemInventory = new POSInventoryQtyAdjustmentQBEntry();
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        ItemInventory = coll.FindRefNumberEntry(dr["RefNumber"].ToString());
                        if (ItemInventory == null)
                        {
                            ItemInventory = new POSInventoryQtyAdjustmentQBEntry();

                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Associate = dr["Associate"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Comments"))
                            {
                                #region Validations of Comments
                                if (dr["Comments"].ToString() != string.Empty)
                                {
                                    if (dr["Comments"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Comments = dr["Comments"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Comments = dr["Comments"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Comments = dr["Comments"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("InventoryAdjustmentSource"))
                            {
                                #region Validations of ItemType
                                if (dr["InventoryAdjustmentSource"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemInventory.InventoryAdjustmentSource = Convert.ToString((POSDataProcessingImportClass.InventoryAdjustmentSource)Enum.Parse(typeof(POSDataProcessingImportClass.InventoryAdjustmentSource), dr["InventoryAdjustmentSource"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemInventory.InventoryAdjustmentSource = dr["InventoryAdjustmentSource"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("QuickBooksFlag"))
                            {
                                #region Validations of ItemType
                                if (dr["QuickBooksFlag"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemInventory.QuickBooksFlag = Convert.ToString((POSDataProcessingImportClass.QuickBooksFlag)Enum.Parse(typeof(POSDataProcessingImportClass.QuickBooksFlag), dr["QuickBooksFlag"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemInventory.QuickBooksFlag = dr["QuickBooksFlag"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Reason"))
                            {
                                #region Validations of Reason
                                if (dr["Reason"].ToString() != string.Empty)
                                {
                                    if (dr["Reason"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Reason(" + dr["Reason"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Reason = dr["Reason"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Reason = dr["Reason"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Reason = dr["Reason"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Reason = dr["Reason"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region 
                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    ItemInventory.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StoreNumber"))
                            {
                                #region Validations of StoreNumber
                                if (dr["StoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["StoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This StoreNumber(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemInventory.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventory.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            ItemInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        ItemInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnState"))
                            {
                                #region Validations of ItemType
                                if (dr["TxnState"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemInventory.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemInventory.TxnState = dr["TxnState"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Workstation"))
                            {
                                #region Validations of Workstation
                                if (dr["Workstation"].ToString() != string.Empty)
                                {
                                    if (dr["Workstation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Workstation(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.Workstation = dr["Workstation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                #endregion
                            }

                            QBPOSEntities.InventoryQtyAdjustmentItemAdd InventoryQtyAdjustmentItemAdd = new QBPOSEntities.InventoryQtyAdjustmentItemAdd();
                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Axis-565
                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of SalesALU

                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {

                                    }
                                    else
                                    {
                                        //InventoryQtyAdjustmentItemAdd.ALU = dr["ALU"].ToString();
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis-565 end

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("NewQuantity"))
                            {
                                #region Validations of NewQuantity
                                if (dr["NewQuantity"].ToString() != string.Empty)
                                {
                                    if (dr["NewQuantity"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This NewQuantity(" + dr["NewQuantity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Axis-565
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {

                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                            if (InventoryQtyAdjustmentItemAdd.ListID != null)
                                            {
                                                InventoryQtyAdjustmentItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis-565
                            if (InventoryQtyAdjustmentItemAdd.ListID != null || InventoryQtyAdjustmentItemAdd.NewQuantity != null || InventoryQtyAdjustmentItemAdd.UnitOfMeasure != null)
                                ItemInventory.InventoryQtyAdjustmentItemAdd.Add(InventoryQtyAdjustmentItemAdd);

                            coll.Add(ItemInventory);
                        }
                        else
                        {
                            QBPOSEntities.InventoryQtyAdjustmentItemAdd InventoryQtyAdjustmentItemAdd = new QBPOSEntities.InventoryQtyAdjustmentItemAdd();

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of SalesALU

                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {

                                    }
                                    else
                                    {
                                        //InventoryQtyAdjustmentItemAdd.ALU = dr["ALU"].ToString();
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("NewQuantity"))
                            {
                                #region Validations of NewQuantity
                                if (dr["NewQuantity"].ToString() != string.Empty)
                                {
                                    if (dr["NewQuantity"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This NewQuantity(" + dr["NewQuantity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {

                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                            if (InventoryQtyAdjustmentItemAdd.ListID != null)
                                            {
                                                InventoryQtyAdjustmentItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                           
                            if (InventoryQtyAdjustmentItemAdd.ListID != null || InventoryQtyAdjustmentItemAdd.NewQuantity != null || InventoryQtyAdjustmentItemAdd.UnitOfMeasure != null)
                                ItemInventory.InventoryQtyAdjustmentItemAdd.Add(InventoryQtyAdjustmentItemAdd);
                        }
                    }
                    else
                    {
                        ItemInventory = new POSInventoryQtyAdjustmentQBEntry();

                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Associate = dr["Associate"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Comments"))
                        {
                            #region Validations of Comments
                            if (dr["Comments"].ToString() != string.Empty)
                            {
                                if (dr["Comments"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Comments = dr["Comments"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Comments = dr["Comments"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Comments = dr["Comments"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("InventoryAdjustmentSource"))
                        {
                            #region Validations of ItemType
                            if (dr["InventoryAdjustmentSource"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemInventory.InventoryAdjustmentSource = Convert.ToString((POSDataProcessingImportClass.InventoryAdjustmentSource)Enum.Parse(typeof(POSDataProcessingImportClass.InventoryAdjustmentSource), dr["InventoryAdjustmentSource"].ToString(), true));
                                }
                                catch
                                {
                                    ItemInventory.InventoryAdjustmentSource = dr["InventoryAdjustmentSource"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("QuickBooksFlag"))
                        {
                            #region Validations of ItemType
                            if (dr["QuickBooksFlag"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemInventory.QuickBooksFlag = Convert.ToString((POSDataProcessingImportClass.QuickBooksFlag)Enum.Parse(typeof(POSDataProcessingImportClass.QuickBooksFlag), dr["QuickBooksFlag"].ToString(), true));
                                }
                                catch
                                {
                                    ItemInventory.QuickBooksFlag = dr["QuickBooksFlag"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Reason"))
                        {
                            #region Validations of Reason
                            if (dr["Reason"].ToString() != string.Empty)
                            {
                                if (dr["Reason"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Reason(" + dr["Reason"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Reason = dr["Reason"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Reason = dr["Reason"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Reason = dr["Reason"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Reason = dr["Reason"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region 
                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                ItemInventory.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StoreNumber"))
                        {
                            #region Validations of StoreNumber
                            if (dr["StoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["StoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This StoreNumber(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.StoreNumber = dr["StoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventory.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        ItemInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    ItemInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TxnState"))
                        {
                            #region Validations of ItemType
                            if (dr["TxnState"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemInventory.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                }
                                catch
                                {
                                    ItemInventory.TxnState = dr["TxnState"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Workstation"))
                        {
                            #region Validations of Workstation
                            if (dr["Workstation"].ToString() != string.Empty)
                            {
                                if (dr["Workstation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Workstation(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemInventory.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemInventory.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventory.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemInventory.Workstation = dr["Workstation"].ToString();
                                }
                            }
                            #endregion
                        }

                        QBPOSEntities.InventoryQtyAdjustmentItemAdd InventoryQtyAdjustmentItemAdd = new QBPOSEntities.InventoryQtyAdjustmentItemAdd();
                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of ItemName
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Name(" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    InventoryQtyAdjustmentItemAdd.Desc1 = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Axis-565
                        if (dt.Columns.Contains("ALU"))
                        {
                            #region Validations of SalesALU

                            if (dr["ALU"].ToString() != string.Empty)
                            {
                                if (dr["ALU"].ToString().Length > 20)
                                {

                                }
                                else
                                {
                                    //InventoryQtyAdjustmentItemAdd.ALU = dr["ALU"].ToString();
                                    if (CommonUtilities.GetInstance().ALULookup == true)
                                    {
                                        InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, dr["ALU"].ToString());
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis-565 end

                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("NewQuantity"))
                        {
                            #region Validations of NewQuantity
                            if (dr["NewQuantity"].ToString() != string.Empty)
                            {
                                if (dr["NewQuantity"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This NewQuantity(" + dr["NewQuantity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                    }
                                }
                                else
                                {
                                    InventoryQtyAdjustmentItemAdd.NewQuantity = dr["NewQuantity"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber(" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    InventoryQtyAdjustmentItemAdd.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations of UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure(" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    InventoryQtyAdjustmentItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Axis-565
                        if (dt.Columns.Contains("UPC"))
                        {
                            #region Validations of SalesUPC
                            if (dr["UPC"].ToString() != string.Empty)
                            {
                                if (dr["UPC"].ToString().Length > 18)
                                {

                                }
                                else
                                {
                                    InventoryQtyAdjustmentItemAdd.UPC = dr["UPC"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        InventoryQtyAdjustmentItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, dr["UPC"].ToString(), null);
                                        if (InventoryQtyAdjustmentItemAdd.ListID != null)
                                        {
                                            InventoryQtyAdjustmentItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis-565
                        if (InventoryQtyAdjustmentItemAdd.ListID != null || InventoryQtyAdjustmentItemAdd.NewQuantity != null || InventoryQtyAdjustmentItemAdd.UnitOfMeasure != null)
                            ItemInventory.InventoryQtyAdjustmentItemAdd.Add(InventoryQtyAdjustmentItemAdd);

                        coll.Add(ItemInventory);
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }

    }
}
