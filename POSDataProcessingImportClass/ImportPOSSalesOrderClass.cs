﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;

namespace POSDataProcessingImportClass
{
  public class ImportPOSSalesOrderClass
    {
        private static ImportPOSSalesOrderClass m_ImportPOSSalesOrderClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSSalesOrderClass()
        {   }

        #endregion
      /// <summary>
      /// creating new instance of class
      /// </summary>
      /// <returns></returns>
        public static ImportPOSSalesOrderClass GetInstance()
        {
            if (m_ImportPOSSalesOrderClass == null)
                m_ImportPOSSalesOrderClass = new ImportPOSSalesOrderClass();
            return m_ImportPOSSalesOrderClass;
        }
        /// <summary>
        /// Creating new sales order transaction for pos.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>
       
        public POSDataProcessingImportClass.POSSalesOrderQBEntryCollection ImportPOSSalesOrderData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSSalesOrderQBEntryCollection coll = new POSSalesOrderQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
              
                    string datevalue = string.Empty;
                    DateTime SODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSSalesOrderQBEntry SalesOrder = new POSSalesOrderQBEntry();
                    if (dt.Columns.Contains("SalesOrderNumber"))
                    {
                        SalesOrder = coll.FindsalesOrderNumberEntry(dr["SalesOrderNumber"].ToString());

                        if (SalesOrder == null)
                        {
                            SalesOrder = new POSSalesOrderQBEntry();

                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.Associate = dr["Associate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Cashier"))
                            {
                                #region Validations of Cashier
                                if (dr["Cashier"].ToString() != string.Empty)
                                {
                                    if (dr["Cashier"].ToString().Length > 16)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cashier (" + dr["Cashier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.Cashier = dr["Cashier"].ToString().Substring(0, 16);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.Cashier = dr["Cashier"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.Cashier = dr["Cashier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Cashier = dr["Cashier"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomerFullName"))
                            {
                                #region Validations of CustomerListID
                                if (dr["CustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer FullName (" + dr["CustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.CustomerListID = dr["CustomerFullName"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Discount"))
                            {
                                #region Validations for Discount
                                if (dr["Discount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.Discount = dr["Discount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.Discount = dr["Discount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountPercent"))
                            {
                                #region Validations for DiscountPercent
                                if (dr["DiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Instructions"))
                            {
                                #region Validations of Instructions
                                if (dr["Instructions"].ToString() != string.Empty)
                                {
                                    if (dr["Instructions"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Instructions (" + dr["Instructions"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.Instructions = dr["Instructions"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.Instructions = dr["Instructions"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.Instructions = dr["Instructions"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Instructions = dr["Instructions"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PriceLevelNumber"))
                            {
                                #region Validations of PriceLevelNumber
                                if (dr["PriceLevelNumber"].ToString() != string.Empty)
                                {
                                    if (dr["PriceLevelNumber"].ToString().Length > 5)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PromoCode"))
                            {
                                #region Validations of PromoCode
                                if (dr["PromoCode"].ToString() != string.Empty)
                                {
                                    if (dr["PromoCode"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PromoCode (" + dr["PromoCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.PromoCode = dr["PromoCode"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesOrderNumber"))
                            {
                                #region Validations of SalesOrderNumber
                                if (dr["SalesOrderNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesOrderNumber"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesOrderNumber (" + dr["SalesOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString().Substring(0, 32);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesOrderStatusDesc"))
                            {
                                #region Validations of SalesOrderStatusDesc
                                if (dr["SalesOrderStatusDesc"].ToString() != string.Empty)
                                {
                                    if (dr["SalesOrderStatusDesc"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Order Status Desc (" + dr["SalesOrderStatusDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesOrderType"))
                            {
                                #region Validations of SalesOrderType
                                if (dr["SalesOrderType"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        SalesOrder.SalesOrderType = Convert.ToString((POSDataProcessingImportClass.SalesOrderType)Enum.Parse(typeof(POSDataProcessingImportClass.SalesOrderType), dr["SalesOrderType"].ToString(), true));
                                    }
                                    catch
                                    {
                                        SalesOrder.SalesOrderType = dr["SalesOrderType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxCategory"))
                            {
                                #region Validations of TaxCategory
                                if (dr["TaxCategory"].ToString() != string.Empty)
                                {
                                    if (dr["TaxCategory"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["TaxCategory"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.TaxCategory = dr["TaxCategory"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesOrder.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesOrder.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                SalesOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            SalesOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        SalesOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            QBPOSEntities.ShippingInformation ShippingInformationItem = new QBPOSEntities.ShippingInformation();
                            if (dt.Columns.Contains("ShipAddressName"))
                            {
                                #region Validations of ShipAddressName
                                if (dr["ShipAddressName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddressName"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddressName (" + dr["ShipAddressName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipCity"))
                            {
                                #region Validations of ShipCity
                                if (dr["ShipCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCity (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.City = dr["ShipCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipCompanyName"))
                            {
                                #region Validations of ShipCompanyName
                                if (dr["ShipCompanyName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCompanyName"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCompanyName (" + dr["ShipCompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipCountry"))
                            {
                                #region Validations of ShipCountry
                                if (dr["ShipCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCountry"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCountry (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Country = dr["ShipCountry"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipFullName"))
                            {
                                #region Validations of ShipFullName
                                if (dr["ShipFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipFullName"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipFullName (" + dr["ShipFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.FullName = dr["ShipFullName"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipPhone"))
                            {
                                #region Validations of ShipPhone
                                if (dr["ShipPhone"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone (" + dr["ShipPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone = dr["ShipPhone"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPhone2"))
                            {
                                #region Validations of ShipPhone2
                                if (dr["ShipPhone2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone2 (" + dr["ShipPhone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPhone3"))
                            {
                                #region Validations of ShipPhone3
                                if (dr["ShipPhone3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone3 (" + dr["ShipPhone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipPhone4"))
                            {
                                #region Validations of ShipPhone4
                                if (dr["ShipPhone4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone4"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone4 (" + dr["ShipPhone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPostalCode"))
                            {
                                #region Validations of ShipPostalCode
                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPostalCode"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipPostalCode (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 10);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipBy"))
                            {
                                #region Validations of ShipBy
                                if (dr["ShipBy"].ToString() != string.Empty)
                                {
                                    if (dr["ShipBy"].ToString().Length > 50)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipBy (" + dr["ShipBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.ShipBy = dr["ShipBy"].ToString().Substring(0, 50);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Shipping"))
                            {
                                #region Validations for Shipping
                                if (dr["Shipping"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Shipping ( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Shipping = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ShipState"))
                            {
                                #region Validations of ShipState
                                if (dr["ShipState"].ToString() != string.Empty)
                                {
                                    if (dr["ShipState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipState (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipStreet"))
                            {
                                #region Validations of ShipStreet
                                if (dr["ShipStreet"].ToString() != string.Empty)
                                {
                                    if (dr["ShipStreet"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipStreet (" + dr["ShipStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Street = dr["ShipStreet"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipStreet2"))
                            {
                                #region Validations of ShipStreet2
                                if (dr["ShipStreet2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipStreet2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipStreet2 (" + dr["ShipStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (ShippingInformationItem.AddressName != null || ShippingInformationItem.City != null || ShippingInformationItem.CompanyName != null || ShippingInformationItem.Country != null || ShippingInformationItem.FullName != null || ShippingInformationItem.Phone != null || ShippingInformationItem.Phone2 != null || ShippingInformationItem.Phone3 != null || ShippingInformationItem.Phone4 != null || ShippingInformationItem.PostalCode != null || ShippingInformationItem.State != null || ShippingInformationItem.Street != null || ShippingInformationItem.Street2 != null)
                                SalesOrder.ShippingInformation.Add(ShippingInformationItem);

                            QBPOSEntities.SalesOrderItemAdd SalesOrderItemAdd = new QBPOSEntities.SalesOrderItemAdd();


                            if (dt.Columns.Contains("SalesALU"))
                            {
                                #region Validations of SalesALU
                                
                                    if (dr["SalesALU"].ToString() != string.Empty)
                                    {
                                        if (dr["SalesALU"].ToString().Length > 20)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Sales ALU (" + dr["SalesALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesOrderItemAdd.ALU = dr["SalesALU"].ToString().Substring(0, 20);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                            if (CommonUtilities.GetInstance().ALULookup == true)
                                            {
                                                SalesOrderItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesOrderItemAdd.ALU);
                                            }
                                        }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesAssociate"))
                            {
                                #region Validations of SalesAssociate
                                if (dr["SalesAssociate"].ToString() != string.Empty)
                                {
                                    if (dr["SalesAssociate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Associate (" + dr["SalesAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesAttribute"))
                            {
                                #region Validations of SalesAttribute
                                if (dr["SalesAttribute"].ToString() != string.Empty)
                                {
                                    if (dr["SalesAttribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Attribute (" + dr["SalesAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("SalesCommission"))
                            {
                                #region Validations for SalesCommission
                                if (dr["SalesCommission"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesCommission"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Commission( " + dr["SalesCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesCommission"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of SalesDesc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesDesc2"))
                            {
                                #region Validations of SalesDesc2
                                if (dr["SalesDesc2"].ToString() != string.Empty)
                                {
                                    if (dr["SalesDesc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Desc2 (" + dr["SalesDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscount"))
                            {
                                #region Validations for SalesDiscount
                                if (dr["SalesDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount( " + dr["SalesDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscountPercent"))
                            {
                                #region Validations for SalesDiscountPercent
                                if (dr["SalesDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount Percent( " + dr["SalesDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscountType"))
                            {
                                #region Validations of SalesDiscountType
                                if (dr["SalesDiscountType"].ToString() != string.Empty)
                                {
                                    if (dr["SalesDiscountType"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount Type (" + dr["SalesDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesExtendedPrice"))
                            {
                                #region Validations for SalesExtendedPrice
                                if (dr["SalesExtendedPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesExtendedPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Extended Price( " + dr["SalesExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesExtendedPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesPrice"))
                            {
                                #region Validations for SalesPrice
                                if (dr["SalesPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Price( " + dr["SalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("SalesQty"))
                            {
                                #region Validations of SalesQty
                                if (dr["SalesQty"].ToString() != string.Empty)
                                {
                                    if (dr["SalesQty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Qty (" + dr["SalesQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Qty = dr["SalesQty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesSerialNumber"))
                            {
                                #region Validations of SalesSerialNumber
                                if (dr["SalesSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesSerialNumber"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Serial Number (" + dr["SalesSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesSize"))
                            {
                                #region Validations of SalesSize
                                if (dr["SalesSize"].ToString() != string.Empty)
                                {
                                    if (dr["SalesSize"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Size (" + dr["SalesSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Size = dr["SalesSize"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesTaxCode"))
                            {
                                #region Validations of SalesTaxCode
                                if (dr["SalesTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCode"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales TaxCode (" + dr["SalesTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesUnitOfMeasure"))
                            {
                                #region Validations of SalesUnitOfMeasure
                                if (dr["SalesUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["SalesUnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Unit Of Measure (" + dr["SalesUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesUPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["SalesUPC"].ToString() != string.Empty)
                                {
                                    if (dr["SalesUPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales UPC (" + dr["SalesUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            //Axis 565 reopen
                                            SalesOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, SalesOrderItemAdd.UPC, null);
                                            if (SalesOrderItemAdd.ListID != null)
                                            {
                                                SalesOrderItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (SalesOrderItemAdd.ALU != null || SalesOrderItemAdd.Associate != null || SalesOrderItemAdd.Attribute != null || SalesOrderItemAdd.Commission != null || SalesOrderItemAdd.Desc1 != null || SalesOrderItemAdd.Desc2 != null || SalesOrderItemAdd.Discount != null || SalesOrderItemAdd.DiscountPercent != null || SalesOrderItemAdd.DiscountType != null || SalesOrderItemAdd.ExtendedPrice != null || SalesOrderItemAdd.Price != null || SalesOrderItemAdd.Qty != null || SalesOrderItemAdd.SerialNumber != null || SalesOrderItemAdd.Size != null || SalesOrderItemAdd.TaxCode != null || SalesOrderItemAdd.UnitOfMeasure != null || SalesOrderItemAdd.UPC != null)
                                SalesOrder.SalesOrderItemAdd.Add(SalesOrderItemAdd);

                            coll.Add(SalesOrder);
                        }
                        else
                        {
                            QBPOSEntities.SalesOrderItemAdd SalesOrderItemAdd = new QBPOSEntities.SalesOrderItemAdd();

                            if (dt.Columns.Contains("SalesALU"))
                            {
                                #region Validations of SalesALU
                                if (dr["SalesALU"].ToString() != string.Empty)
                                {
                                    if (dr["SalesALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales ALU (" + dr["SalesALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.ALU = dr["SalesALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            SalesOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesOrderItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesAssociate"))
                            {
                                #region Validations of SalesAssociate
                                if (dr["SalesAssociate"].ToString() != string.Empty)
                                {
                                    if (dr["SalesAssociate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Associate (" + dr["SalesAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesAttribute"))
                            {
                                #region Validations of SalesAttribute
                                if (dr["SalesAttribute"].ToString() != string.Empty)
                                {
                                    if (dr["SalesAttribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Attribute (" + dr["SalesAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("SalesCommission"))
                            {
                                #region Validations for SalesCommission
                                if (dr["SalesCommission"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesCommission"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Commission( " + dr["SalesCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesCommission"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of SalesDesc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesDesc2"))
                            {
                                #region Validations of SalesDesc2
                                if (dr["SalesDesc2"].ToString() != string.Empty)
                                {
                                    if (dr["SalesDesc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Desc2 (" + dr["SalesDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscount"))
                            {
                                #region Validations for SalesDiscount
                                if (dr["SalesDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount( " + dr["SalesDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscountPercent"))
                            {
                                #region Validations for SalesDiscountPercent
                                if (dr["SalesDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount Percent( " + dr["SalesDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDiscountType"))
                            {
                                #region Validations of SalesDiscountType
                                if (dr["SalesDiscountType"].ToString() != string.Empty)
                                {
                                    if (dr["SalesDiscountType"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Discount Type (" + dr["SalesDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesExtendedPrice"))
                            {
                                #region Validations for SalesExtendedPrice
                                if (dr["SalesExtendedPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesExtendedPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Extended Price( " + dr["SalesExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesExtendedPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesPrice"))
                            {
                                #region Validations for SalesPrice
                                if (dr["SalesPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Price( " + dr["SalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("SalesQty"))
                            {
                                #region Validations of SalesQty
                                if (dr["SalesQty"].ToString() != string.Empty)
                                {
                                    if (dr["SalesQty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Qty (" + dr["SalesQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Qty = dr["SalesQty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesSerialNumber"))
                            {
                                #region Validations of SalesSerialNumber
                                if (dr["SalesSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesSerialNumber"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Serial Number (" + dr["SalesSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesSize"))
                            {
                                #region Validations of SalesSize
                                if (dr["SalesSize"].ToString() != string.Empty)
                                {
                                    if (dr["SalesSize"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Size (" + dr["SalesSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.Size = dr["SalesSize"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesTaxCode"))
                            {
                                #region Validations of SalesTaxCode
                                if (dr["SalesTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCode"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales TaxCode (" + dr["SalesTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesUnitOfMeasure"))
                            {
                                #region Validations of SalesUnitOfMeasure
                                if (dr["SalesUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["SalesUnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Unit Of Measure (" + dr["SalesUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesUPC"))
                            {
                                #region Validations of SalesUPC
                                if (dr["SalesUPC"].ToString() != string.Empty)
                                {
                                    if (dr["SalesUPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales UPC (" + dr["SalesUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            //Axis 565 reopen
                                            SalesOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName,SalesOrderItemAdd.UPC, null);
                                            if (SalesOrderItemAdd.ListID != null)
                                            {
                                                SalesOrderItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (SalesOrderItemAdd.ALU != null || SalesOrderItemAdd.Associate != null || SalesOrderItemAdd.Attribute != null || SalesOrderItemAdd.Commission != null || SalesOrderItemAdd.Desc1 != null || SalesOrderItemAdd.Desc2 != null || SalesOrderItemAdd.Discount != null || SalesOrderItemAdd.DiscountPercent != null || SalesOrderItemAdd.DiscountType != null || SalesOrderItemAdd.ExtendedPrice != null || SalesOrderItemAdd.Price != null || SalesOrderItemAdd.Qty != null || SalesOrderItemAdd.SerialNumber != null || SalesOrderItemAdd.Size != null || SalesOrderItemAdd.TaxCode != null || SalesOrderItemAdd.UnitOfMeasure != null || SalesOrderItemAdd.UPC != null)
                                SalesOrder.SalesOrderItemAdd.Add(SalesOrderItemAdd);
                        }
                    }
                    else
                    {
                        SalesOrder = new POSSalesOrderQBEntry();

                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.Associate = dr["Associate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Cashier"))
                        {
                            #region Validations of Cashier
                            if (dr["Cashier"].ToString() != string.Empty)
                            {
                                if (dr["Cashier"].ToString().Length > 16)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Cashier (" + dr["Cashier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.Cashier = dr["Cashier"].ToString().Substring(0, 16);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.Cashier = dr["Cashier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Cashier = dr["Cashier"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.Cashier = dr["Cashier"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomerFullName"))
                        {
                            #region Validations of CustomerListID
                            if (dr["CustomerFullName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Customer FullName (" + dr["CustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.CustomerListID = dr["CustomerFullName"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.CustomerListID = dr["CustomerFullName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Discount"))
                        {
                            #region Validations for Discount
                            if (dr["Discount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.Discount = dr["Discount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Discount = dr["Discount"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountPercent"))
                        {
                            #region Validations for DiscountPercent
                            if (dr["DiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.DiscountPercent = dr["DiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Instructions"))
                        {
                            #region Validations of Instructions
                            if (dr["Instructions"].ToString() != string.Empty)
                            {
                                if (dr["Instructions"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Instructions (" + dr["Instructions"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.Instructions = dr["Instructions"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.Instructions = dr["Instructions"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.Instructions = dr["Instructions"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.Instructions = dr["Instructions"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PriceLevelNumber"))
                        {
                            #region Validations of PriceLevelNumber
                            if (dr["PriceLevelNumber"].ToString() != string.Empty)
                            {
                                if (dr["PriceLevelNumber"].ToString().Length > 5)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PromoCode"))
                        {
                            #region Validations of PromoCode
                            if (dr["PromoCode"].ToString() != string.Empty)
                            {
                                if (dr["PromoCode"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PromoCode (" + dr["PromoCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.PromoCode = dr["PromoCode"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.PromoCode = dr["PromoCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesOrderNumber"))
                        {
                            #region Validations of SalesOrderNumber
                            if (dr["SalesOrderNumber"].ToString() != string.Empty)
                            {
                                if (dr["SalesOrderNumber"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesOrderNumber (" + dr["SalesOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString().Substring(0, 32);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.SalesOrderNumber = dr["SalesOrderNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesOrderStatusDesc"))
                        {
                            #region Validations of SalesOrderStatusDesc
                            if (dr["SalesOrderStatusDesc"].ToString() != string.Empty)
                            {
                                if (dr["SalesOrderStatusDesc"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Order Status Desc (" + dr["SalesOrderStatusDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.SalesOrderStatusDesc = dr["SalesOrderStatusDesc"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesOrderType"))
                        {
                            #region Validations of SalesOrderType
                            if (dr["SalesOrderType"].ToString() != string.Empty)
                            {
                                try
                                {
                                    SalesOrder.SalesOrderType = Convert.ToString((POSDataProcessingImportClass.SalesOrderType)Enum.Parse(typeof(POSDataProcessingImportClass.SalesOrderType), dr["SalesOrderType"].ToString(), true));
                                }
                                catch
                                {
                                    SalesOrder.SalesOrderType = dr["SalesOrderType"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxCategory"))
                        {
                            #region Validations of TaxCategory
                            if (dr["TaxCategory"].ToString() != string.Empty)
                            {
                                if (dr["TaxCategory"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["TaxCategory"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrder.TaxCategory = dr["TaxCategory"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrder.TaxCategory = dr["TaxCategory"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrder.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SalesOrder.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        SalesOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    SalesOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }

                        QBPOSEntities.ShippingInformation ShippingInformationItem = new QBPOSEntities.ShippingInformation();
                        if (dt.Columns.Contains("ShipAddressName"))
                        {
                            #region Validations of ShipAddressName
                            if (dr["ShipAddressName"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddressName"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddressName (" + dr["ShipAddressName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipCity"))
                        {
                            #region Validations of ShipCity
                            if (dr["ShipCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCity (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.City = dr["ShipCity"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipCompanyName"))
                        {
                            #region Validations of ShipCompanyName
                            if (dr["ShipCompanyName"].ToString() != string.Empty)
                            {
                                if (dr["ShipCompanyName"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCompanyName (" + dr["ShipCompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipCountry"))
                        {
                            #region Validations of ShipCountry
                            if (dr["ShipCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipCountry"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCountry (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipFullName"))
                        {
                            #region Validations of ShipFullName
                            if (dr["ShipFullName"].ToString() != string.Empty)
                            {
                                if (dr["ShipFullName"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipFullName (" + dr["ShipFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipPhone"))
                        {
                            #region Validations of ShipPhone
                            if (dr["ShipPhone"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone (" + dr["ShipPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPhone2"))
                        {
                            #region Validations of ShipPhone2
                            if (dr["ShipPhone2"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone2 (" + dr["ShipPhone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPhone3"))
                        {
                            #region Validations of ShipPhone3
                            if (dr["ShipPhone3"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone3 (" + dr["ShipPhone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipPhone4"))
                        {
                            #region Validations of ShipPhone4
                            if (dr["ShipPhone4"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone4"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone4 (" + dr["ShipPhone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPostalCode"))
                        {
                            #region Validations of ShipPostalCode
                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipPostalCode"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipPostalCode (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 10);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipBy"))
                        {
                            #region Validations of ShipBy
                            if (dr["ShipBy"].ToString() != string.Empty)
                            {
                                if (dr["ShipBy"].ToString().Length > 50)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipBy (" + dr["ShipBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString().Substring(0, 50);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Shipping"))
                        {
                            #region Validations for Shipping
                            if (dr["Shipping"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Shipping ( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Shipping = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ShipState"))
                        {
                            #region Validations of ShipState
                            if (dr["ShipState"].ToString() != string.Empty)
                            {
                                if (dr["ShipState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipState (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.State = dr["ShipState"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipStreet"))
                        {
                            #region Validations of ShipStreet
                            if (dr["ShipStreet"].ToString() != string.Empty)
                            {
                                if (dr["ShipStreet"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipStreet (" + dr["ShipStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipStreet2"))
                        {
                            #region Validations of ShipStreet2
                            if (dr["ShipStreet2"].ToString() != string.Empty)
                            {
                                if (dr["ShipStreet2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipStreet2 (" + dr["ShipStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (ShippingInformationItem.AddressName != null || ShippingInformationItem.City != null || ShippingInformationItem.CompanyName != null || ShippingInformationItem.Country != null || ShippingInformationItem.FullName != null || ShippingInformationItem.Phone != null || ShippingInformationItem.Phone2 != null || ShippingInformationItem.Phone3 != null || ShippingInformationItem.Phone4 != null || ShippingInformationItem.PostalCode != null || ShippingInformationItem.State != null || ShippingInformationItem.Street != null || ShippingInformationItem.Street2 != null)
                            SalesOrder.ShippingInformation.Add(ShippingInformationItem);

                        QBPOSEntities.SalesOrderItemAdd SalesOrderItemAdd = new QBPOSEntities.SalesOrderItemAdd();


                        if (dt.Columns.Contains("SalesALU"))
                        {
                            #region Validations of SalesALU
                            //Axis-565
                            
                                if (dr["SalesALU"].ToString() != string.Empty)
                                {
                                    if (dr["SalesALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales ALU (" + dr["SalesALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesOrderItemAdd.ALU = dr["SalesALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.ALU = dr["SalesALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            SalesOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesOrderItemAdd.ALU);
                                        }
                                    }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesAssociate"))
                        {
                            #region Validations of SalesAssociate
                            if (dr["SalesAssociate"].ToString() != string.Empty)
                            {
                                if (dr["SalesAssociate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Associate (" + dr["SalesAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Associate = dr["SalesAssociate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesAttribute"))
                        {
                            #region Validations of SalesAttribute
                            if (dr["SalesAttribute"].ToString() != string.Empty)
                            {
                                if (dr["SalesAttribute"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Attribute (" + dr["SalesAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Attribute = dr["SalesAttribute"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("SalesCommission"))
                        {
                            #region Validations for SalesCommission
                            if (dr["SalesCommission"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesCommission"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Commission( " + dr["SalesCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Commission = dr["SalesCommission"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesCommission"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of SalesDesc1
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Desc1 = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesDesc2"))
                        {
                            #region Validations of SalesDesc2
                            if (dr["SalesDesc2"].ToString() != string.Empty)
                            {
                                if (dr["SalesDesc2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Desc2 (" + dr["SalesDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Desc2 = dr["SalesDesc2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesDiscount"))
                        {
                            #region Validations for SalesDiscount
                            if (dr["SalesDiscount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesDiscount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Discount( " + dr["SalesDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Discount = dr["SalesDiscount"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesDiscountPercent"))
                        {
                            #region Validations for SalesDiscountPercent
                            if (dr["SalesDiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesDiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Discount Percent( " + dr["SalesDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountPercent = dr["SalesDiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesDiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesDiscountType"))
                        {
                            #region Validations of SalesDiscountType
                            if (dr["SalesDiscountType"].ToString() != string.Empty)
                            {
                                if (dr["SalesDiscountType"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Discount Type (" + dr["SalesDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.DiscountType = dr["SalesDiscountType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesExtendedPrice"))
                        {
                            #region Validations for SalesExtendedPrice
                            if (dr["SalesExtendedPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesExtendedPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Extended Price( " + dr["SalesExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.ExtendedPrice = dr["SalesExtendedPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesExtendedPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesPrice"))
                        {
                            #region Validations for SalesPrice
                            if (dr["SalesPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Price( " + dr["SalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Price = dr["SalesPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("SalesQty"))
                        {
                            #region Validations of SalesQty
                            if (dr["SalesQty"].ToString() != string.Empty)
                            {
                                if (dr["SalesQty"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Qty (" + dr["SalesQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Qty = dr["SalesQty"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Qty = dr["SalesQty"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesSerialNumber"))
                        {
                            #region Validations of SalesSerialNumber
                            if (dr["SalesSerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SalesSerialNumber"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Serial Number (" + dr["SalesSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.SerialNumber = dr["SalesSerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesSize"))
                        {
                            #region Validations of SalesSize
                            if (dr["SalesSize"].ToString() != string.Empty)
                            {
                                if (dr["SalesSize"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Size (" + dr["SalesSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.Size = dr["SalesSize"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.Size = dr["SalesSize"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesTaxCode"))
                        {
                            #region Validations of SalesTaxCode
                            if (dr["SalesTaxCode"].ToString() != string.Empty)
                            {
                                if (dr["SalesTaxCode"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales TaxCode (" + dr["SalesTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.TaxCode = dr["SalesTaxCode"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesUnitOfMeasure"))
                        {
                            #region Validations of SalesUnitOfMeasure
                            if (dr["SalesUnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["SalesUnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Unit Of Measure (" + dr["SalesUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.UnitOfMeasure = dr["SalesUnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesUPC"))
                        {
                            #region Validations of SalesUPC
                            if (dr["SalesUPC"].ToString() != string.Empty)
                            {
                                if (dr["SalesUPC"].ToString().Length > 18)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales UPC (" + dr["SalesUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString().Substring(0, 18);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesOrderItemAdd.UPC = dr["SalesUPC"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        //Axis 565 reopen
                                        SalesOrderItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName,  SalesOrderItemAdd.UPC,null);
                                        if (SalesOrderItemAdd.ListID != null)
                                        {
                                            SalesOrderItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (SalesOrderItemAdd.ALU != null || SalesOrderItemAdd.Associate != null || SalesOrderItemAdd.Attribute != null || SalesOrderItemAdd.Commission != null || SalesOrderItemAdd.Desc1 != null || SalesOrderItemAdd.Desc2 != null || SalesOrderItemAdd.Discount != null || SalesOrderItemAdd.DiscountPercent != null || SalesOrderItemAdd.DiscountType != null || SalesOrderItemAdd.ExtendedPrice != null || SalesOrderItemAdd.Price != null || SalesOrderItemAdd.Qty != null || SalesOrderItemAdd.SerialNumber != null || SalesOrderItemAdd.Size != null || SalesOrderItemAdd.TaxCode != null || SalesOrderItemAdd.UnitOfMeasure != null || SalesOrderItemAdd.UPC != null)
                            SalesOrder.SalesOrderItemAdd.Add(SalesOrderItemAdd);

                        coll.Add(SalesOrder);
                    }

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }

    }
}
