﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
     [XmlRootAttribute("POSPriceAdjustmentQBEntry", Namespace = "", IsNullable = false)]
  public class POSPriceAdjustmentQBEntry
    {
       #region Private Member Variable
        private string m_PriceAdjustmentName;
        private string m_Comments;
        private string m_Associate;
        private string m_PriceLevelNumber;
        private Collection<PriceAdjustmentItemAdd> m_PriceAdjustmentItemAdd = new Collection<PriceAdjustmentItemAdd>();        
             
        #endregion

        #region Constructor
        public POSPriceAdjustmentQBEntry()
        {
        }
        #endregion

         #region Properties
        public string PriceAdjustmentName
        {
            get { return m_PriceAdjustmentName; }
            set { m_PriceAdjustmentName = value; }
        }

       
        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }

        public string PriceLevelNumber
        {
            get { return m_PriceLevelNumber; }
            set { m_PriceLevelNumber = value; }
        }
        [XmlArray("PriceAdjustmentItemAddREM")]
        public Collection<PriceAdjustmentItemAdd> PriceAdjustmentItemAdd
        {
            get { return m_PriceAdjustmentItemAdd; }
            set { m_PriceAdjustmentItemAdd = value; }
        }
      
        #endregion

        #region Public Method
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPriceAdjustmentQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement PriceAdjustmentAddRq = inputXMLDoc.CreateElement("PriceAdjustmentAddRq");
            qbXMLMsgsRq.AppendChild(PriceAdjustmentAddRq);
            PriceAdjustmentAddRq.SetAttribute("requestID", "1");
            XmlElement PriceAdjustmentAdd = inputXMLDoc.CreateElement("PriceAdjustmentAdd");
            PriceAdjustmentAddRq.AppendChild(PriceAdjustmentAdd);
            requestXML = requestXML.Replace("<PriceAdjustmentItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PriceAdjustmentItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PriceAdjustmentItemAddREM>", string.Empty);

            PriceAdjustmentAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceAdjustmentAddRs"))
                    {
                      
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceAdjustmentAddRs/PriceAdjustmentRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPriceAdjustment(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        /// <summary>
        ///This method is used for updating priceadjustment information of existing with listid 
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="txnID"></param>
        /// <returns></returns>
         public bool UpdatePriceAdjustmentInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string txnID)
         {
             string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
             try
             {
                 if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                 {
                     if (fileName.Contains("Program Files (x86)"))
                     {
                         fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                     }
                     else
                     {
                         fileName = fileName.Replace("Program Files", Constants.xpPath);
                     }
                 }
                 else
                 {
                     if (fileName.Contains("Program Files (x86)"))
                     {
                         fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                     }
                     else
                     {
                         fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                     }
                 }
             }
             catch { }
             try
             {
                 DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSPriceAdjustmentQBEntry>.Save(this, fileName);
             }
             catch
             {
                 statusMessage += "\n ";
                 statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                 return false;
             }

             System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
             inputXMLDoc.Load(fileName);

             string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

             inputXMLDoc = new System.Xml.XmlDocument();

             System.IO.File.Delete(fileName);


             inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
             inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
             XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
             inputXMLDoc.AppendChild(qbXML);
             XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
             qbXML.AppendChild(qbXMLMsgsRq);
             qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
             XmlElement PriceAdjustmentModRq = inputXMLDoc.CreateElement("PriceAdjustmentModRq");
             qbXMLMsgsRq.AppendChild(PriceAdjustmentModRq);
             
             XmlElement PriceAdjustmentMod = inputXMLDoc.CreateElement("PriceAdjustmentMod");
             PriceAdjustmentModRq.AppendChild(PriceAdjustmentMod);

             requestXML = requestXML.Replace("<PriceAdjustmentItemAddREM />", string.Empty);
             requestXML = requestXML.Replace("<PriceAdjustmentItemAddREM>", string.Empty);
             requestXML = requestXML.Replace("</PriceAdjustmentItemAddREM>", string.Empty);

             requestXML = requestXML.Replace("<PriceAdjustmentItemAdd>", "<PriceAdjustmentItemMod>");
             requestXML = requestXML.Replace("</PriceAdjustmentItemAdd>", "</PriceAdjustmentItemMod>");

             requestXML = requestXML.Replace("<NewPrice>", "<TxnLineID>-1</TxnLineID><NewPrice>");

             PriceAdjustmentMod.InnerXml = requestXML;

             requestText = inputXMLDoc.OuterXml;

             
             XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PriceAdjustmentModRq/PriceAdjustmentMod").FirstChild;
             //Create TxnID aggregate and fill in field values for it
             System.Xml.XmlElement TxnID = inputXMLDoc.CreateElement("TxnID");
           
             inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/PriceAdjustmentModRq/PriceAdjustmentMod").InsertBefore(TxnID, firstChild).InnerText = txnID;

            
             string responseFile = string.Empty;
             string resp = string.Empty;
             try
             {
                 responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                 if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                 {

                     resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                 }
                 else

                     resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

             }
             catch (Exception ex)
             {
                 CommonUtilities.WriteErrorLog(ex.Message);
                 CommonUtilities.WriteErrorLog(ex.StackTrace);
             }

             finally
             {

                 if (resp != string.Empty)
                 {
                     string statusSeverity = string.Empty;
                     System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                     outputXMLDoc.LoadXml(resp);
                     foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceAdjustmentModRs"))
                     {
                        
                         try
                         {
                             statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                         }
                         catch
                         { }
                         TransactionImporter.TransactionImporter.testFlag = true;
                         if (statusSeverity == "Error")
                         {
                             string requesterror = string.Empty;
                             try
                             {
                                 requesterror = oNode.Attributes["requestID"].Value.ToString();
                             }
                             catch { }
                             string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                             foreach (string s in array_msg)
                             {
                                 if (s != "")
                                 { statusMessage += s + ".\n"; }
                                 if (s == "")
                                 { statusMessage += s; }
                             }
                             statusMessage += "The Error location is " + requesterror;
                             TransactionImporter.TransactionImporter.testFlag = false;
                         }
                     }
                     if (statusSeverity != "Error")
                     {
                         foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/PriceAdjustmentModRs/PriceAdjustmentRet"))
                         {
                             CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                         }
                     }
                     CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                 }

             }
             if (resp == string.Empty)
             {
                 statusMessage += "\n ";
                 statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofPriceAdjustment(this);
                 statusMessage += "\n ";
                 return false;
             }
             else
             {
                 if (resp.Contains("statusSeverity=\"Error\""))
                 {
                     return false;

                 }
                 else
                     return true;
             }
         }
       #endregion
      
     }
    
  public class POSPriceAdjustmentQBEntryCollection : Collection<POSPriceAdjustmentQBEntry>
  {
        /// <summary>
        /// check existing item.
        /// </summary>
        /// <param name="priceAdjustmentName"></param>
        /// <returns></returns>
      public POSPriceAdjustmentQBEntry FindpriceAdjustmentNameEntry(string priceAdjustmentName)
      {
          foreach (POSPriceAdjustmentQBEntry item in this)
          {
              if (item.PriceAdjustmentName == priceAdjustmentName)
              {
                  return item;
              }
          }
          return null;
      }
  }

  public enum PriceLevelNumber
  {
      p1 = 1,
      p2 = 2,
      p3 = 3,
      p4 = 4,
      p5 = 5
  }
}
