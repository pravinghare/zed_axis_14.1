﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
     [XmlRootAttribute("POSItemInventoryQBEntry", Namespace = "", IsNullable = false)]
  public class POSItemInventoryQBEntry
    {
         #region Private Member Variable
        private string m_ALU;
        private string m_Attribute;
        private string m_COGSAccount;
        private string m_Cost;
        private string m_DepartmentListID;
        private string m_Desc1;
        private string m_Desc2;
        private string m_IncomeAccount;
        private string m_IsEligibleForCommission;
        private string m_IsPrintingTags;
        private string m_IsUnorderable;
        private string m_IsEligibleForRewards;
        private string m_IsWebItem;
        private string m_ItemType;
        private string m_MarginPercent;
        private string m_MarkupPercent;
        private string m_MSRP;

         private string m_OnHandStore01;
         private string m_OnHandStore02;
         private string m_OnHandStore03;
         private string m_OnHandStore04;
         private string m_OnHandStore05;
         private string m_OnHandStore06;
         private string m_OnHandStore07;
         private string m_OnHandStore08;
         private string m_OnHandStore09;
         private string m_OnHandStore10;
         private string m_OnHandStore11;
         private string m_OnHandStore12;
         private string m_OnHandStore13;
         private string m_OnHandStore14;
         private string m_OnHandStore15;
         private string m_OnHandStore16;
         private string m_OnHandStore17;
         private string m_OnHandStore18;
         private string m_OnHandStore19;
         private string m_OnHandStore20;         

         private string m_ReorderPointStore01;
         private string m_ReorderPointStore02;
         private string m_ReorderPointStore03;
         private string m_ReorderPointStore04;
         private string m_ReorderPointStore05;
         private string m_ReorderPointStore06;
         private string m_ReorderPointStore07;
         private string m_ReorderPointStore08;
         private string m_ReorderPointStore09;
         private string m_ReorderPointStore10;
         private string m_ReorderPointStore11;
         private string m_ReorderPointStore12;
         private string m_ReorderPointStore13;
         private string m_ReorderPointStore14;
         private string m_ReorderPointStore15;
         private string m_ReorderPointStore16;
         private string m_ReorderPointStore17;
         private string m_ReorderPointStore18;
         private string m_ReorderPointStore19;
         private string m_ReorderPointStore20;


         private string m_OrderByUnit;
         private string m_OrderCost;
         private string m_Price1;
         private string m_Price2;
         private string m_Price3;
         private string m_Price4;
         private string m_Price5;
         private string m_ReorderPoint;
         private string m_SellByUnit;
         private string m_SerialFlag;
         private string m_Size;
         
         private string m_TaxCode;
         private string m_UnitOfMeasure;
         private string m_UPC;
         private string m_VendorListID;
         private string m_WebDesc;
         private string m_WebPrice;
         private string m_Manufacturer;
         private string m_Weight;
         private string m_Keywords;
         private string m_WebCategories;       
         private Collection<UnitOfMeasure1> m_UnitOfMeasure1 = new Collection<UnitOfMeasure1>();
         private Collection<UnitOfMeasure2> m_UnitOfMeasure2 = new Collection<UnitOfMeasure2>();
         private Collection<UnitOfMeasure3> m_UnitOfMeasure3 = new Collection<UnitOfMeasure3>();
         private Collection<VendorInfo2> m_VendorInfo2 = new Collection<VendorInfo2>();
         private Collection<VendorInfo3> m_VendorInfo3 = new Collection<VendorInfo3>();
         private Collection<VendorInfo4> m_VendorInfo4 = new Collection<VendorInfo4>();
         private Collection<VendorInfo5> m_VendorInfo5 = new Collection<VendorInfo5>();
              
        #endregion
       
         #region Constructor
        public POSItemInventoryQBEntry()
        {
        }
        #endregion

        #region Properties
        public string ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }
         
        public string Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }

        public string COGSAccount
        {
            get { return m_COGSAccount; }
            set { m_COGSAccount = value; }
        }
         
        public string Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public string DepartmentListID
        {
            get { return m_DepartmentListID; }
            set { m_DepartmentListID = value; }
        }

        public string Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }

        public string Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }

        public string IncomeAccount
        {
            get { return m_IncomeAccount; }
            set { m_IncomeAccount = value; }
        }

        public string IsEligibleForCommission
        {
            get { return m_IsEligibleForCommission; }
            set { m_IsEligibleForCommission = value; }
        }

        public string IsPrintingTags
        {
            get { return m_IsPrintingTags; }
            set { m_IsPrintingTags = value; }
        }

        public string IsUnorderable
        {
            get { return m_IsUnorderable; }
            set { m_IsUnorderable = value; }
        }

        public string IsEligibleForRewards
        {
            get { return m_IsEligibleForRewards; }
            set { m_IsEligibleForRewards = value; }
        }

        public string IsWebItem
        {
            get { return m_IsWebItem; }
            set { m_IsWebItem = value; }
        }
        public string ItemType
        {
            get { return m_ItemType; }
            set { m_ItemType = value; }
        }

        public string MarginPercent
        {
            get { return m_MarginPercent; }
            set { m_MarginPercent = value; }
        }

        public string MarkupPercent
        {
            get { return m_MarkupPercent; }
            set { m_MarkupPercent = value; }
        }

        public string MSRP
        {
            get { return m_MSRP; }
            set { m_MSRP = value; }
        }

        public string OnHandStore01
        {
            get { return m_OnHandStore01; }
            set { m_OnHandStore01 = value; }
        }

        public string OnHandStore02
        {
            get { return m_OnHandStore02; }
            set { m_OnHandStore02 = value; }
        }
        public string OnHandStore03
        {
            get { return m_OnHandStore03; }
            set { m_OnHandStore03 = value; }
        }
        public string OnHandStore04
        {
            get { return m_OnHandStore04; }
            set { m_OnHandStore04 = value; }
        }
        public string OnHandStore05
        {
            get { return m_OnHandStore05; }
            set { m_OnHandStore05 = value; }
        }

        public string OnHandStore06
        {
            get { return m_OnHandStore06; }
            set { m_OnHandStore06 = value; }
        }
        public string OnHandStore07
        {
            get { return m_OnHandStore07; }
            set { m_OnHandStore07 = value; }
        }
        public string OnHandStore08
        {
            get { return m_OnHandStore08; }
            set { m_OnHandStore08 = value; }
        }
        public string OnHandStore09
        {
            get { return m_OnHandStore09; }
            set { m_OnHandStore09 = value; }
        }

        public string OnHandStore10
        {
            get { return m_OnHandStore10; }
            set { m_OnHandStore10 = value; }
        }

        public string OnHandStore11
        {
            get { return m_OnHandStore11; }
            set { m_OnHandStore11 = value; }
        }
        public string OnHandStore12
        {
            get { return m_OnHandStore12; }
            set { m_OnHandStore12 = value; }
        }
        public string OnHandStore13
        {
            get { return m_OnHandStore13; }
            set { m_OnHandStore13 = value; }
        }
        public string OnHandStore14
        {
            get { return m_OnHandStore14; }
            set { m_OnHandStore14 = value; }
        }

        public string OnHandStore15
        {
            get { return m_OnHandStore15; }
            set { m_OnHandStore15 = value; }
        }
        public string OnHandStore16
        {
            get { return m_OnHandStore16; }
            set { m_OnHandStore16 = value; }
        }
        public string OnHandStore17
        {
            get { return m_OnHandStore17; }
            set { m_OnHandStore17 = value; }
        }
        public string OnHandStore18
        {
            get { return m_OnHandStore18; }
            set { m_OnHandStore18 = value; }
        }
        public string OnHandStore19
        {
            get { return m_OnHandStore19; }
            set { m_OnHandStore19 = value; }
        }
        public string OnHandStore20
        {
            get { return m_OnHandStore20; }
            set { m_OnHandStore20 = value; }
        }

        public string ReorderPointStore01
        {
            get { return m_ReorderPointStore01; }
            set { m_ReorderPointStore01 = value; }
        }

        public string ReorderPointStore02
        {
            get { return m_ReorderPointStore02; }
            set { m_ReorderPointStore02 = value; }
        }

        public string ReorderPointStore03
        {
            get { return m_ReorderPointStore03; }
            set { m_ReorderPointStore03 = value; }
        }

        public string ReorderPointStore04
        {
            get { return m_ReorderPointStore04; }
            set { m_ReorderPointStore04 = value; }
        }

        public string ReorderPointStore05
        {
            get { return m_ReorderPointStore05; }
            set { m_ReorderPointStore05 = value; }
        }

        public string ReorderPointStore06
        {
            get { return m_ReorderPointStore06; }
            set { m_ReorderPointStore06 = value; }
        }

        public string ReorderPointStore07
        {
            get { return m_ReorderPointStore07; }
            set { m_ReorderPointStore07 = value; }
        }

        public string ReorderPointStore08
        {
            get { return m_ReorderPointStore08; }
            set { m_ReorderPointStore08 = value; }
        }

        public string ReorderPointStore09
        {
            get { return m_ReorderPointStore09; }
            set { m_ReorderPointStore09 = value; }
        }

        public string ReorderPointStore10
        {
            get { return m_ReorderPointStore10; }
            set { m_ReorderPointStore10 = value; }
        }

        public string ReorderPointStore11
        {
            get { return m_ReorderPointStore11; }
            set { m_ReorderPointStore11 = value; }
        }

        public string ReorderPointStore12
        {
            get { return m_ReorderPointStore12; }
            set { m_ReorderPointStore12 = value; }
        }

        public string ReorderPointStore13
        {
            get { return m_ReorderPointStore13; }
            set { m_ReorderPointStore13 = value; }
        }

        public string ReorderPointStore14
        {
            get { return m_ReorderPointStore14; }
            set { m_ReorderPointStore14 = value; }
        }

        public string ReorderPointStore15
        {
            get { return m_ReorderPointStore15; }
            set { m_ReorderPointStore15 = value; }
        }

        public string ReorderPointStore16
        {
            get { return m_ReorderPointStore16; }
            set { m_ReorderPointStore16 = value; }
        }

        public string ReorderPointStore17
        {
            get { return m_ReorderPointStore17; }
            set { m_ReorderPointStore17 = value; }
        }

        public string ReorderPointStore18
        {
            get { return m_ReorderPointStore18; }
            set { m_ReorderPointStore18 = value; }
        }

        public string ReorderPointStore19
        {
            get { return m_ReorderPointStore19; }
            set { m_ReorderPointStore19 = value; }
        }

        public string ReorderPointStore20
        {
            get { return m_ReorderPointStore20; }
            set { m_ReorderPointStore20 = value; }
        }

        public string OrderByUnit
        {
            get { return m_OrderByUnit; }
            set { m_OrderByUnit = value; }
        }

        public string OrderCost
        {
            get { return m_OrderCost; }
            set { m_OrderCost = value; }
        }

        public string Price1
        {
            get { return m_Price1; }
            set { m_Price1 = value; }
        }
        public string Price2
        {
            get { return m_Price2; }
            set { m_Price2 = value; }
        }

        public string Price3
        {
            get { return m_Price3; }
            set { m_Price3 = value; }
        }

        public string Price4
        {
            get { return m_Price4; }
            set { m_Price4 = value; }
        }

        public string Price5
        {
            get { return m_Price5; }
            set { m_Price5 = value; }
        }

        public string ReorderPoint
        {
            get { return m_ReorderPoint; }
            set { m_ReorderPoint = value; }
        }

        public string SellByUnit
        {
            get { return m_SellByUnit; }
            set { m_SellByUnit = value; }
        }
        public string SerialFlag
        {
            get { return m_SerialFlag; }
            set { m_SerialFlag = value; }
        }
        public string Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        public string TaxCode
        {
            get { return m_TaxCode; }
            set { m_TaxCode = value; }
        }

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }

        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }

        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }

        public string WebDesc
        {
            get { return m_WebDesc; }
            set { m_WebDesc = value; }
        }

        public string WebPrice
        {
            get { return m_WebPrice; }
            set { m_WebPrice = value; }
        }

        public string Manufacturer
        {
            get { return m_Manufacturer; }
            set { m_Manufacturer = value; }
        }
         
        public string Weight
        {
            get { return m_Weight; }
            set { m_Weight = value; }
        }

        public string Keywords
        {
            get { return m_Keywords; }
            set { m_Keywords = value; }
        }

        public string WebCategories
        {
            get { return m_WebCategories; }
            set { m_WebCategories = value; }
        }

        [XmlArray("UnitOfMeasure1REM")]
        public Collection<UnitOfMeasure1> UnitOfMeasure1
        {
            get { return m_UnitOfMeasure1; }
            set { m_UnitOfMeasure1 = value; }
        }
        [XmlArray("UnitOfMeasure2REM")]
        public Collection<UnitOfMeasure2> UnitOfMeasure2
        {
            get { return m_UnitOfMeasure2; }
            set { m_UnitOfMeasure2 = value; }
        }
        [XmlArray("UnitOfMeasure3REM")]
        public Collection<UnitOfMeasure3> UnitOfMeasure3
        {
            get { return m_UnitOfMeasure3; }
            set { m_UnitOfMeasure3 = value; }
        }
         [XmlArray("VendorInfo2REM")]
        public Collection<VendorInfo2> VendorInfo2
        {
            get { return m_VendorInfo2; }
            set { m_VendorInfo2 = value; }
        }

         [XmlArray("VendorInfo3REM")]
         public Collection<VendorInfo3> VendorInfo3
         {
             get { return m_VendorInfo3; }
             set { m_VendorInfo3 = value; }
         }

         [XmlArray("VendorInfo4REM")]
         public Collection<VendorInfo4> VendorInfo4
         {
             get { return m_VendorInfo4; }
             set { m_VendorInfo4 = value; }
         }

         [XmlArray("VendorInfo5REM")]
         public Collection<VendorInfo5> VendorInfo5
         {
             get { return m_VendorInfo5; }
             set { m_VendorInfo5 = value; }
         }

        
        #endregion

        #region Public Methods
         /// <summary>
         /// Creating request file for exporting data to quickbook.
         /// </summary>
         /// <param name="statusMessage"></param>
         /// <param name="requestText"></param>
         /// <param name="rowcount"></param>
         /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSItemInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemInventoryAddRq = inputXMLDoc.CreateElement("ItemInventoryAddRq");
            qbXMLMsgsRq.AppendChild(ItemInventoryAddRq);
            ItemInventoryAddRq.SetAttribute("requestID", "1");
            XmlElement ItemInventoryAdd = inputXMLDoc.CreateElement("ItemInventoryAdd");
            ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

            requestXML = requestXML.Replace("<UnitOfMeasure1REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure1REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure1REM>", string.Empty);

            requestXML = requestXML.Replace("<UnitOfMeasure2REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure2REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure2REM>", string.Empty);

            requestXML = requestXML.Replace("<UnitOfMeasure3REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure3REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure3REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo2REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo2REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo2REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo3REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo3REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo3REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo4REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo4REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo4REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo5REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo5REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo5REM>", string.Empty);

            ItemInventoryAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryAddRs"))
                    {
                        
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryAddRs/ItemInventoryRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                        }
                        CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                    }

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofItemInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

         /// <summary>
         /// This method is used for updating iteminv information of existing with listid 
         /// </summary>
         /// <param name="statusMessage"></param>
         /// <param name="requestText"></param>
         /// <param name="rowcount"></param>
         /// <param name="listID"></param>
         /// <param name="upcID"></param>
         /// <returns></returns>
        public bool UpdateEmployeeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string listID, string upcID)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSItemInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemInventoryModRq = inputXMLDoc.CreateElement("ItemInventoryModRq");
            qbXMLMsgsRq.AppendChild(ItemInventoryModRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement ItemInventoryMod = inputXMLDoc.CreateElement("ItemInventoryMod");
            ItemInventoryModRq.AppendChild(ItemInventoryMod);

            requestXML = requestXML.Replace("<UnitOfMeasure1REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure1REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure1REM>", string.Empty);

            requestXML = requestXML.Replace("<UnitOfMeasure2REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure2REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure2REM>", string.Empty);

            requestXML = requestXML.Replace("<UnitOfMeasure3REM />", string.Empty);
            requestXML = requestXML.Replace("<UnitOfMeasure3REM>", string.Empty);
            requestXML = requestXML.Replace("</UnitOfMeasure3REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo2REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo2REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo2REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo3REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo3REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo3REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo4REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo4REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo4REM>", string.Empty);

            requestXML = requestXML.Replace("<VendorInfo5REM />", string.Empty);
            requestXML = requestXML.Replace("<VendorInfo5REM>", string.Empty);
            requestXML = requestXML.Replace("</VendorInfo5REM>", string.Empty);


            ItemInventoryMod.InnerXml = requestXML;
            requestText = inputXMLDoc.OuterXml;


            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/ItemInventoryModRq/ItemInventoryMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = inputXMLDoc.CreateElement("ListID");
    
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/ItemInventoryModRq/ItemInventoryMod").InsertBefore(ListID, firstChild).InnerText = listID;

            //Create LoginName aggregate and fill in field values for it 
            System.Xml.XmlElement Desc1 = inputXMLDoc.CreateElement("Desc1");
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/ItemInventoryModRq/ItemInventoryMod").InsertBefore(Desc1, ListID).InnerText = upcID;
                      
            

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    string requesterror = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryModRs"))
                    {
                      
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                         
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryModRs/ItemInventoryRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofItemInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }

     public class POSItemInventoryQBEntryCollection : Collection<POSItemInventoryQBEntry>
     {

     }

     public enum ItemType
     {
         Inventory,
         NonInventory,
         Service, 
         Assembly,
         Group,
         SpecialOrder
     }

     public enum SerialFlag 
     {
         Optional,
         Prompt 
     }
}
