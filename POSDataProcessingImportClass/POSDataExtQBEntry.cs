﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;


namespace POSDataProcessingImportClass
{
   [XmlRootAttribute("POSDataExtQBEntry", Namespace = "", IsNullable = false)]
   public  class POSDataExtQBEntry
    {
        #region Private Member
        private string m_ownerID;
        private string m_dataExtName;
        private string m_listDataExtType;
        private Collection<ListObjRef> m_ListObjectRef = new Collection<ListObjRef>();
        private string m_dataExtValue;
        #endregion

         #region Constructor

        public POSDataExtQBEntry()
        {
        }

        #endregion

        #region Public Properties

        public string OwnerID
        {
            get { return m_ownerID; }
            set { m_ownerID = value; }
        }
        
        public string DataExtName
        {
            get { return m_dataExtName; }
            set { m_dataExtName = value; }
        }

        public string ListDataExtType
        {
            get { return m_listDataExtType; }
            set { m_listDataExtType = value; }
        }

        [XmlArray("ListObjREM")]
        public Collection<ListObjRef> ListObjRef
        {
            get { return m_ListObjectRef; }
            set { m_ListObjectRef = value; }
        }

        public string DataExtValue
        {
            get { return m_dataExtValue; }
            set { m_dataExtValue = value; }
        }
       
        #endregion

        #region Public Methods
       /// <summary>
       /// method for add data ext to qb
       /// </summary>
       /// <param name="statusMessage"></param>
       /// <param name="requestText"></param>
       /// <param name="rowcount"></param>
       /// <returns></returns>
        public bool DataExtAddToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSDataExtQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBPOSXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBPOSXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement DataExtAddRq = requestXmlDoc.CreateElement("DataExtAddRq");
            inner.AppendChild(DataExtAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement DataExtAdd = requestXmlDoc.CreateElement("DataExtAdd");

            DataExtAddRq.AppendChild(DataExtAdd);

            System.Xml.XmlElement ownerID = requestXmlDoc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtAdd.AppendChild(ownerID);

            requestXML = requestXML.Replace("<ListObjREM />", string.Empty);
            requestXML = requestXML.Replace("<ListObjREM>", string.Empty);
            requestXML = requestXML.Replace("</ListObjREM>", string.Empty);

            DataExtAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/DataExtAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage += "\n ";
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            statusMessage += "\nThe Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfPOSDataExt(this, "DataExtAdd");
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
            //return true;
        }

       /// <summary>
        /// method for add data ext values to qb
       /// </summary>
       /// <param name="statusMessage"></param>
       /// <param name="requestText"></param>
       /// <param name="rowcount"></param>
       /// <returns></returns>
        public bool DataExtValueToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSDataExtQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBPOSXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBPOSXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement DataExtModRq = requestXmlDoc.CreateElement("DataExtModRq");
            inner.AppendChild(DataExtModRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement DataExtMod = requestXmlDoc.CreateElement("DataExtMod");

            DataExtModRq.AppendChild(DataExtMod);

            System.Xml.XmlElement ownerID = requestXmlDoc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtMod.AppendChild(ownerID);

            requestXML = requestXML.Replace("<ListObjREM />", string.Empty);
            requestXML = requestXML.Replace("<ListObjREM>", string.Empty);
            requestXML = requestXML.Replace("</ListObjREM>", string.Empty);

            DataExtMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/DataExtModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage += "\n ";
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            statusMessage += "\nThe Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfPOSDataExt(this, "DataExtMod");
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
            
        }

        /// <summary>
        /// Method for DataExtDefMod
        /// Update assignToObject of CustomField
        /// </summary>
        /// <returns></returns>
        public bool DataExtDefModToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSDataExtQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;


            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
           
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBPOSXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBPOSXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement DataExtModRq = requestXmlDoc.CreateElement("DataExtDefModRq");
            inner.AppendChild(DataExtModRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement DataExtMod = requestXmlDoc.CreateElement("DataExtDefMod");

            DataExtModRq.AppendChild(DataExtMod);

            System.Xml.XmlElement ownerID = requestXmlDoc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtMod.AppendChild(ownerID);

            requestXML = requestXML.Replace("<ListObjREM />", string.Empty);
            requestXML = requestXML.Replace("<ListObjREM>", string.Empty);
            requestXML = requestXML.Replace("</ListObjREM>", string.Empty);

            requestXML = requestXML.Replace("<ListDataExtType>", "<AssignToObject>");
            requestXML = requestXML.Replace("</ListDataExtType>", "</AssignToObject>");

       

            try
            {
                int startIndex = requestXML.IndexOf("<ListObjRef>");

                if (startIndex <= 0)
                {
                    startIndex = requestXML.IndexOf("<ListObjRef/>");

                    requestXML = requestXML.Remove(startIndex);


                    DataExtMod.InnerXml = requestXML;
                }
                else
                {
                    requestXML = requestXML.Remove(startIndex);
                    DataExtMod.InnerXml = requestXML;
                }
            }
            catch
            {
            }
            requestText = requestXmlDoc.OuterXml;
            string requeststring = string.Empty;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/DataExtDefModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage += "\n ";
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            statusMessage += "\nThe Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfPOSDataExtDef(this, "DataExtDefMod");
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// Method for DataExtDefAdd
        /// add new Custom Field to list Type
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool DataExtDefAddToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSDataExtQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;


            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBPOSXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBPOSXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement DataExtAddRq = requestXmlDoc.CreateElement("DataExtDefAddRq");
            inner.AppendChild(DataExtAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement DataExtAdd = requestXmlDoc.CreateElement("DataExtDefAdd");

            DataExtAddRq.AppendChild(DataExtAdd);

            //System.Xml.XmlElement ownerID = requestXmlDoc.CreateElement("OwnerID");
            //ownerID.InnerText = "0";
            //DataExtMod.AppendChild(ownerID);

            requestXML = requestXML.Replace("<ListObjREM />", string.Empty);
            requestXML = requestXML.Replace("<ListObjREM>", string.Empty);
            requestXML = requestXML.Replace("</ListObjREM>", string.Empty);

            requestXML = requestXML.Replace("<ListDataExtType>", "<AssignToObject>");
            requestXML = requestXML.Replace("</ListDataExtType>", "</AssignToObject>");

            try
            {
                int startIndex = requestXML.IndexOf("<ListObjRef>");

                if (startIndex == 0)
                {
                    startIndex = requestXML.IndexOf("<ListObjRef />");

                    requestXML = requestXML.Remove(startIndex);


                    DataExtAdd.InnerXml = requestXML;
                }
                else
                {
                    requestXML = requestXML.Remove(startIndex);


                    DataExtAdd.InnerXml = requestXML;
                }
            }
            catch
            {


            }
            XmlElement dataExtType = requestXmlDoc.CreateElement("DataExtType");
            dataExtType.InnerText = "STR255TYPE";

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/DataExtDefAddRq/DataExtDefAdd/AssignToObject");

            requestXmlDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/DataExtDefAddRq/DataExtDefAdd").InsertBefore(dataExtType, firstChild);

            requestText = requestXmlDoc.OuterXml;
            string requeststring = string.Empty;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/DataExtDefAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage += "\n ";
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            statusMessage += "\nThe Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfPOSDataExtDef(this, "DataExtDefAdd");
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        #endregion
    }
   public class POSDataExtQBEntryCollection : Collection<POSDataExtQBEntry>
   {

   }
}
