﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;

namespace POSDataProcessingImportClass
{
   public class ImportPOSVoucherClass
    {
        private static ImportPOSVoucherClass m_ImportPOSVoucherClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSVoucherClass()
        {   }

        #endregion

        /// <summary>
        /// creating new instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSVoucherClass GetInstance()
        {
            if (m_ImportPOSVoucherClass == null)
                m_ImportPOSVoucherClass = new ImportPOSVoucherClass();
            return m_ImportPOSVoucherClass;
        }
       /// <summary>
        /// Creating new voucher transaction for pos.
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <param name="dt"></param>
       /// <param name="logDirectory"></param>
       /// <returns></returns>
        public POSDataProcessingImportClass.POSVoucherQBEntryCollection ImportPOSVoucherData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSVoucherQBEntryCollection coll = new POSVoucherQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                 
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSVoucherQBEntry Voucher = new POSVoucherQBEntry();
                    #region with ref number
                    if (dt.Columns.Contains("InvoiceNumber"))
                    {

                        Voucher = coll.FindInvoiceNumberVocucherEntry(dr["InvoiceNumber"].ToString());

                        if (Voucher == null)
                        {
                            Voucher = new POSVoucherQBEntry();
                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Associate = dr["Associate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Comments"))
                            {
                                #region Validations of Comments
                                if (dr["Comments"].ToString() != string.Empty)
                                {
                                    if (dr["Comments"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Comments = dr["Comments"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Comments = dr["Comments"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Comments = dr["Comments"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Discount"))
                            {
                                #region Validations for Discount
                                if (dr["Discount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Discount = dr["Discount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Discount = dr["Discount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountPercent"))
                            {
                                #region Validations for DiscountPercent
                                if (dr["DiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Fee"))
                            {
                                #region Validations for Fee
                                if (dr["Fee"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Fee"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fee( " + dr["Fee"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Fee = dr["Fee"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Fee = dr["Fee"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Fee = dr["Fee"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Fee = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Fee"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Freight"))
                            {
                                #region Validations for Freight
                                if (dr["Freight"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Freight"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Freight( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Freight = dr["Freight"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Freight = dr["Freight"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Freight = dr["Freight"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Freight = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Freight"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("InvoiceDate"))
                            {
                                #region validations of InvoiceDate
                                if (dr["InvoiceDate"].ToString() != "<None>" || dr["InvoiceDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["InvoiceDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This InvoiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Voucher.InvoiceDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Voucher.InvoiceDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Voucher.InvoiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            Voucher.InvoiceDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        Voucher.InvoiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("InvoiceDueDate"))
                            {
                                #region validations of InvoiceDueDate
                                if (dr["InvoiceDueDate"].ToString() != "<None>" || dr["InvoiceDueDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["InvoiceDueDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This InvoiceDueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Voucher.InvoiceDueDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Voucher.InvoiceDueDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Voucher.InvoiceDueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            Voucher.InvoiceDueDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        Voucher.InvoiceDueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("InvoiceNumber"))
                            {
                                #region Validations of InvoiceNumber
                                if (dr["InvoiceNumber"].ToString() != string.Empty)
                                {
                                    if (dr["InvoiceNumber"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InvoiceNumber (" + dr["InvoiceNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString().Substring(0, 15);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PayeeListID"))
                            {
                                #region Validations of PayeeListID
                                if (dr["PayeeListID"].ToString() != string.Empty)
                                {
                                    if (dr["PayeeListID"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Status Desc (" + dr["PayeeListID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.PayeeListID = dr["PayeeListID"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PurchaseOrderNumber"))
                            {
                                #region Validations of PurchaseOrderNumber
                                if (dr["PurchaseOrderNumber"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrderNumber"].ToString().Length > 2000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Number (" + dr["PurchaseOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString().Substring(0, 2000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("QuickBooksFlag"))
                            {
                                #region Validations of ItemType
                                if (dr["QuickBooksFlag"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        Voucher.QuickBooksFlag = Convert.ToString((POSDataProcessingImportClass.QuickBooksFlag)Enum.Parse(typeof(POSDataProcessingImportClass.QuickBooksFlag), dr["QuickBooksFlag"].ToString(), true));
                                    }
                                    catch
                                    {
                                        Voucher.QuickBooksFlag = dr["QuickBooksFlag"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("StoreNumber"))
                            {
                                #region Validations of StoreNumber
                                if (dr["StoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["StoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TermsDiscount"))
                            {
                                #region Validations for TermsDiscount
                                if (dr["TermsDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TermsDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms Discount( " + dr["TermsDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsDiscount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TermsDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TermsDiscountDays"))
                            {
                                #region Validations of TermsDiscountDays
                                if (dr["TermsDiscountDays"].ToString() != string.Empty)
                                {
                                    if (dr["TermsDiscountDays"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms DiscountDays(" + dr["TermsDiscountDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString().Substring(0, 1000);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TermsNetDays"))
                            {
                                #region Validations of TermsNetDays
                                if (dr["TermsNetDays"].ToString() != string.Empty)
                                {
                                    if (dr["TermsNetDays"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms DiscountDays(" + dr["TermsNetDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.TermsNetDays = dr["TermsNetDays"].ToString().Substring(0, 1000);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Voucher.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Voucher.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Voucher.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PODate = dttest;
                                            Voucher.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PODate = Convert.ToDateTime(datevalue);
                                        Voucher.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnState"))
                            {
                                #region Validations of TxnState
                                if (dr["TxnState"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        Voucher.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                    }
                                    catch
                                    {
                                        Voucher.TxnState = dr["TxnState"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorName"))
                            {
                                #region Validations of VendorListID
                                if (dr["VendorName"].ToString() != string.Empty)
                                {
                                    if (dr["VendorName"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Name (" + dr["VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.VendorListID = dr["VendorName"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.VendorListID = dr["VendorName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.VendorListID = dr["VendorName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.VendorListID = dr["VendorName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            
                            if (dt.Columns.Contains("VoucherType"))
                            {
                                #region Validations of VoucherType
                                if (dr["VoucherType"].ToString() != string.Empty)
                                {
                                    if (dr["VoucherType"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Voucher Type (" + dr["VoucherType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.VoucherType = dr["VoucherType"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.VoucherType = dr["VoucherType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.VoucherType = dr["VoucherType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.VoucherType = dr["VoucherType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Workstation"))
                            {
                                #region Validations of Workstation
                                if (dr["Workstation"].ToString() != string.Empty)
                                {
                                    if (dr["Workstation"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Workstation (" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.Workstation = dr["Workstation"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.Workstation = dr["Workstation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Voucher.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                #endregion
                            }
                            QBPOSEntities.VoucherItemAdd VoucherItemAdd = new QBPOSEntities.VoucherItemAdd();

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of ALU
                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.ALU = dr["ALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ALU = dr["ALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            VoucherItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, VoucherItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Attribute"))
                            {
                                #region Validations of Attribute
                                if (dr["Attribute"].ToString() != string.Empty)
                                {
                                    if (dr["Attribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Desc2"))
                            {
                                #region Validations of Desc2
                                if (dr["Desc2"].ToString() != string.Empty)
                                {
                                    if (dr["Desc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExtendedCost"))
                            {
                                #region Validations for ExtendedCost
                                if (dr["ExtendedCost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("QtyReceived"))
                            {
                                #region Validations of QtyReceived
                                if (dr["QtyReceived"].ToString() != string.Empty)
                                {
                                    if (dr["QtyReceived"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Quantity (" + dr["QtyReceived"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Size"))
                            {
                                #region Validations of Size
                                if (dr["Size"].ToString() != string.Empty)
                                {
                                    if (dr["Size"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Size = dr["Size"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of UPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.UPC = dr["UPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            VoucherItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, VoucherItemAdd.UPC, null);
                                            if (VoucherItemAdd.ListID != null)
                                            {
                                                VoucherItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (VoucherItemAdd.ListID != null || VoucherItemAdd.ALU != null || VoucherItemAdd.Attribute != null || VoucherItemAdd.Cost != null || VoucherItemAdd.Desc1 != null || VoucherItemAdd.Desc2 != null || VoucherItemAdd.ExtendedCost != null || VoucherItemAdd.QtyReceived != null || VoucherItemAdd.Size != null || VoucherItemAdd.UnitOfMeasure != null || VoucherItemAdd.UPC != null)
                                Voucher.VoucherItemAdd.Add(VoucherItemAdd);

                            coll.Add(Voucher);
                        }
                        else
                        {
                            QBPOSEntities.VoucherItemAdd VoucherItemAdd = new QBPOSEntities.VoucherItemAdd();

                            if (dt.Columns.Contains("ALU"))
                            {
                                #region Validations of ALU
                                if (dr["ALU"].ToString() != string.Empty)
                                {
                                    if (dr["ALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.ALU = dr["ALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ALU = dr["ALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            VoucherItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, VoucherItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Attribute"))
                            {
                                #region Validations of Attribute
                                if (dr["Attribute"].ToString() != string.Empty)
                                {
                                    if (dr["Attribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Cost = dr["Cost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Desc2"))
                            {
                                #region Validations of Desc2
                                if (dr["Desc2"].ToString() != string.Empty)
                                {
                                    if (dr["Desc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExtendedCost"))
                            {
                                #region Validations for ExtendedCost
                                if (dr["ExtendedCost"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("QtyReceived"))
                            {
                                #region Validations of QtyReceived
                                if (dr["QtyReceived"].ToString() != string.Empty)
                                {
                                    if (dr["QtyReceived"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Quantity (" + dr["QtyReceived"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Size"))
                            {
                                #region Validations of Size
                                if (dr["Size"].ToString() != string.Empty)
                                {
                                    if (dr["Size"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.Size = dr["Size"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations of UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UPC"))
                            {
                                #region Validations of UPC
                                if (dr["UPC"].ToString() != string.Empty)
                                {
                                    if (dr["UPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VoucherItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VoucherItemAdd.UPC = dr["UPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VoucherItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UPC = dr["UPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            VoucherItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, VoucherItemAdd.UPC,null);
                                            if (VoucherItemAdd.ListID != null)
                                            {
                                                VoucherItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (VoucherItemAdd.ListID != null || VoucherItemAdd.ALU != null || VoucherItemAdd.Attribute != null || VoucherItemAdd.Cost != null || VoucherItemAdd.Desc1 != null || VoucherItemAdd.Desc2 != null || VoucherItemAdd.ExtendedCost != null || VoucherItemAdd.QtyReceived != null || VoucherItemAdd.Size != null || VoucherItemAdd.UnitOfMeasure != null || VoucherItemAdd.UPC != null)
                                Voucher.VoucherItemAdd.Add(VoucherItemAdd);
                        }
                    }
                    #endregion
                    #region without ref number
                    else
                    {
                        Voucher = new POSVoucherQBEntry();
                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Associate = dr["Associate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Comments"))
                        {
                            #region Validations of Comments
                            if (dr["Comments"].ToString() != string.Empty)
                            {
                                if (dr["Comments"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Comments = dr["Comments"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Comments = dr["Comments"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Comments = dr["Comments"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Discount"))
                        {
                            #region Validations for Discount
                            if (dr["Discount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Discount = dr["Discount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Discount = dr["Discount"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountPercent"))
                        {
                            #region Validations for DiscountPercent
                            if (dr["DiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.DiscountPercent = dr["DiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Fee"))
                        {
                            #region Validations for Fee
                            if (dr["Fee"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Fee"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Fee( " + dr["Fee"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Fee = dr["Fee"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Fee = dr["Fee"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Fee = dr["Fee"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Fee = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Fee"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("Freight"))
                        {
                            #region Validations for Freight
                            if (dr["Freight"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Freight"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Freight( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Freight = dr["Freight"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Freight = dr["Freight"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Freight = dr["Freight"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Freight = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Freight"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("InvoiceDate"))
                        {
                            #region validations of InvoiceDate
                            if (dr["InvoiceDate"].ToString() != "<None>" || dr["InvoiceDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["InvoiceDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InvoiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.InvoiceDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.InvoiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Voucher.InvoiceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        Voucher.InvoiceDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    Voucher.InvoiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("InvoiceDueDate"))
                        {
                            #region validations of InvoiceDueDate
                            if (dr["InvoiceDueDate"].ToString() != "<None>" || dr["InvoiceDueDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["InvoiceDueDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InvoiceDueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.InvoiceDueDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.InvoiceDueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Voucher.InvoiceDueDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        Voucher.InvoiceDueDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    Voucher.InvoiceDueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("InvoiceNumber"))
                        {
                            #region Validations of InvoiceNumber
                            if (dr["InvoiceNumber"].ToString() != string.Empty)
                            {
                                if (dr["InvoiceNumber"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This InvoiceNumber (" + dr["InvoiceNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString().Substring(0, 15);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.InvoiceNumber = dr["InvoiceNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PayeeListID"))
                        {
                            #region Validations of PayeeListID
                            if (dr["PayeeListID"].ToString() != string.Empty)
                            {
                                if (dr["PayeeListID"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Status Desc (" + dr["PayeeListID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.PayeeListID = dr["PayeeListID"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.PayeeListID = dr["PayeeListID"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PurchaseOrderNumber"))
                        {
                            #region Validations of PurchaseOrderNumber
                            if (dr["PurchaseOrderNumber"].ToString() != string.Empty)
                            {
                                if (dr["PurchaseOrderNumber"].ToString().Length > 2000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Number (" + dr["PurchaseOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString().Substring(0, 2000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.PurchaseOrderTxnID = dr["PurchaseOrderNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("QuickBooksFlag"))
                        {
                            #region Validations of ItemType
                            if (dr["QuickBooksFlag"].ToString() != string.Empty)
                            {
                                try
                                {
                                    Voucher.QuickBooksFlag = Convert.ToString((POSDataProcessingImportClass.QuickBooksFlag)Enum.Parse(typeof(POSDataProcessingImportClass.QuickBooksFlag), dr["QuickBooksFlag"].ToString(), true));
                                }
                                catch
                                {
                                    Voucher.QuickBooksFlag = dr["QuickBooksFlag"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("StoreNumber"))
                        {
                            #region Validations of StoreNumber
                            if (dr["StoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["StoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.StoreNumber = dr["StoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TermsDiscount"))
                        {
                            #region Validations for TermsDiscount
                            if (dr["TermsDiscount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TermsDiscount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms Discount( " + dr["TermsDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsDiscount = dr["TermsDiscount"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.TermsDiscount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TermsDiscount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("TermsDiscountDays"))
                        {
                            #region Validations of TermsDiscountDays
                            if (dr["TermsDiscountDays"].ToString() != string.Empty)
                            {
                                if (dr["TermsDiscountDays"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms DiscountDays(" + dr["TermsDiscountDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString().Substring(0, 1000);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.TermsDiscountDays = dr["TermsDiscountDays"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TermsNetDays"))
                        {
                            #region Validations of TermsNetDays
                            if (dr["TermsNetDays"].ToString() != string.Empty)
                            {
                                if (dr["TermsNetDays"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms DiscountDays(" + dr["TermsNetDays"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.TermsNetDays = dr["TermsNetDays"].ToString().Substring(0, 1000);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.TermsNetDays = dr["TermsNetDays"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Voucher.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Voucher.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Voucher.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PODate = dttest;
                                        Voucher.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PODate = Convert.ToDateTime(datevalue);
                                    Voucher.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnState"))
                        {
                            #region Validations of TxnState
                            if (dr["TxnState"].ToString() != string.Empty)
                            {
                                try
                                {
                                    Voucher.TxnState = Convert.ToString((POSDataProcessingImportClass.TxnState)Enum.Parse(typeof(POSDataProcessingImportClass.TxnState), dr["TxnState"].ToString(), true));
                                }
                                catch
                                {
                                    Voucher.TxnState = dr["TxnState"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorName"))
                        {
                            #region Validations of VendorListID
                            if (dr["VendorName"].ToString() != string.Empty)
                            {
                                if (dr["VendorName"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Name (" + dr["VendorName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.VendorListID = dr["VendorName"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.VendorListID = dr["VendorName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.VendorListID = dr["VendorName"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.VendorListID = dr["VendorName"].ToString();
                                }
                            }
                            #endregion
                        }
                      
                        if (dt.Columns.Contains("VoucherType"))
                        {
                            #region Validations of VoucherType
                            if (dr["VoucherType"].ToString() != string.Empty)
                            {
                                if (dr["VoucherType"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Voucher Type (" + dr["VoucherType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.VoucherType = dr["VoucherType"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.VoucherType = dr["VoucherType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.VoucherType = dr["VoucherType"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.VoucherType = dr["VoucherType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Workstation"))
                        {
                            #region Validations of Workstation
                            if (dr["Workstation"].ToString() != string.Empty)
                            {
                                if (dr["Workstation"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Workstation (" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Voucher.Workstation = dr["Workstation"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Voucher.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Voucher.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                else
                                {
                                    Voucher.Workstation = dr["Workstation"].ToString();
                                }
                            }
                            #endregion
                        }
                        QBPOSEntities.VoucherItemAdd VoucherItemAdd = new QBPOSEntities.VoucherItemAdd();

                        if (dt.Columns.Contains("ALU"))
                        {
                            #region Validations of ALU
                            if (dr["ALU"].ToString() != string.Empty)
                            {
                                if (dr["ALU"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order ALU (" + dr["ALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.ALU = dr["ALU"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.ALU = dr["ALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ALU = dr["ALU"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.ALU = dr["ALU"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().ALULookup == true)
                                    {
                                        VoucherItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, VoucherItemAdd.ALU);
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Attribute"))
                        {
                            #region Validations of Attribute
                            if (dr["Attribute"].ToString() != string.Empty)
                            {
                                if (dr["Attribute"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Attribute (" + dr["Attribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.Attribute = dr["Attribute"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.Attribute = dr["Attribute"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("Cost"))
                        {
                            #region Validations for Cost
                            if (dr["Cost"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Cost"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Cost( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.Cost = dr["Cost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Cost = dr["Cost"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.Cost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Cost"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of ItemName
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.Desc1 = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Desc2"))
                        {
                            #region Validations of Desc2
                            if (dr["Desc2"].ToString() != string.Empty)
                            {
                                if (dr["Desc2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Desc2 (" + dr["Desc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.Desc2 = dr["Desc2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.Desc2 = dr["Desc2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExtendedCost"))
                        {
                            #region Validations for ExtendedCost
                            if (dr["ExtendedCost"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExtendedCost"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Extended Cost( " + dr["ExtendedCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.ExtendedCost = dr["ExtendedCost"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.ExtendedCost = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExtendedCost"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("QtyReceived"))
                        {
                            #region Validations of QtyReceived
                            if (dr["QtyReceived"].ToString() != string.Empty)
                            {
                                if (dr["QtyReceived"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Quantity (" + dr["QtyReceived"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.QtyReceived = dr["QtyReceived"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Size"))
                        {
                            #region Validations of Size
                            if (dr["Size"].ToString() != string.Empty)
                            {
                                if (dr["Size"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Size (" + dr["Size"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.Size = dr["Size"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.Size = dr["Size"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.Size = dr["Size"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.Size = dr["Size"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations of UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Unit Of Measure (" + dr["UnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("UPC"))
                        {
                            #region Validations of UPC
                            if (dr["UPC"].ToString() != string.Empty)
                            {
                                if (dr["UPC"].ToString().Length > 18)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  UPC (" + dr["UPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VoucherItemAdd.UPC = dr["UPC"].ToString().Substring(0, 18);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VoucherItemAdd.UPC = dr["UPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VoucherItemAdd.UPC = dr["UPC"].ToString();
                                    }
                                }
                                else
                                {
                                    VoucherItemAdd.UPC = dr["UPC"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        VoucherItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, VoucherItemAdd.UPC,null);
                                        if (VoucherItemAdd.ListID != null)
                                        {
                                            VoucherItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (VoucherItemAdd.ListID != null || VoucherItemAdd.ALU != null || VoucherItemAdd.Attribute != null || VoucherItemAdd.Cost != null || VoucherItemAdd.Desc1 != null || VoucherItemAdd.Desc2 != null || VoucherItemAdd.ExtendedCost != null || VoucherItemAdd.QtyReceived != null || VoucherItemAdd.Size != null || VoucherItemAdd.UnitOfMeasure != null || VoucherItemAdd.UPC != null)
                            Voucher.VoucherItemAdd.Add(VoucherItemAdd);

                        coll.Add(Voucher);
                    }
                     #endregion

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }
    }
}
