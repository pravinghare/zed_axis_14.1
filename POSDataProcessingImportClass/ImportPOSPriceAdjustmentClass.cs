﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
  public class ImportPOSPriceAdjustmentClass
    {
      private static ImportPOSPriceAdjustmentClass m_ImportPOSPriceAdjustmentClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSPriceAdjustmentClass()
        {   }

        #endregion
      /// <summary>
      /// creating new class of instance
      /// </summary>
      /// <returns></returns>
        public static ImportPOSPriceAdjustmentClass GetInstance()
        {
            if (m_ImportPOSPriceAdjustmentClass == null)
                m_ImportPOSPriceAdjustmentClass = new ImportPOSPriceAdjustmentClass();
            return m_ImportPOSPriceAdjustmentClass;
        }
      /// <summary>
        ///  Creating new price adjustment transaction for pos.
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <param name="dt"></param>
      /// <param name="logDirectory"></param>
      /// <returns></returns>

        public POSDataProcessingImportClass.POSPriceAdjustmentQBEntryCollection ImportPOSPriceAdjustmentData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSPriceAdjustmentQBEntryCollection coll = new POSPriceAdjustmentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    POSPriceAdjustmentQBEntry PriceAdjustment = new POSPriceAdjustmentQBEntry();
                    if (dt.Columns.Contains("PriceAdjustmentName"))
                    {

                        PriceAdjustment = coll.FindpriceAdjustmentNameEntry(dr["PriceAdjustmentName"].ToString());

                        if (PriceAdjustment == null)
                        {
                            PriceAdjustment = new POSPriceAdjustmentQBEntry();
                            if (dt.Columns.Contains("PriceAdjustmentName"))
                            {
                                #region Validations of PriceAdjustmentName
                                if (dr["PriceAdjustmentName"].ToString() != string.Empty)
                                {
                                    if (dr["PriceAdjustmentName"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceAdjustmentName (" + dr["PriceAdjustmentName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Comments"))
                            {
                                #region Validations of Comments
                                if (dr["Comments"].ToString() != string.Empty)
                                {
                                    if (dr["Comments"].ToString().Length > 300)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustment.Comments = dr["Comments"].ToString().Substring(0, 300);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustment.Comments = dr["Comments"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustment.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.Comments = dr["Comments"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 300)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustment.Associate = dr["Associate"].ToString().Substring(0, 300);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustment.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustment.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PriceLevelNumber"))
                            {
                                #region Validations of PriceLevelNumber
                                if (dr["PriceLevelNumber"].ToString() != string.Empty)
                                {
                                    if (dr["PriceLevelNumber"].ToString().Length > 5)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            QBPOSEntities.PriceAdjustmentItemAdd PriceAdjustmentItemAdd = new QBPOSEntities.PriceAdjustmentItemAdd();
                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PriceAdjustmentNewPrice"))
                            {
                                #region Validations for PriceAdjustmentNewPrice
                                if (dr["PriceAdjustmentNewPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PriceAdjustmentNewPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceAdjustmentNewPrice( " + dr["PriceAdjustmentNewPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.NewPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PriceAdjustmentNewPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (PriceAdjustmentItemAdd.ListID != null || PriceAdjustmentItemAdd.NewPrice != null)
                                PriceAdjustment.PriceAdjustmentItemAdd.Add(PriceAdjustmentItemAdd);

                            coll.Add(PriceAdjustment);
                        }

                        else
                        {

                            QBPOSEntities.PriceAdjustmentItemAdd PriceAdjustmentItemAdd = new QBPOSEntities.PriceAdjustmentItemAdd();
                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of ItemName
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PriceAdjustmentNewPrice"))
                            {
                                #region Validations for PriceAdjustmentNewPrice
                                if (dr["PriceAdjustmentNewPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PriceAdjustmentNewPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceAdjustmentNewPrice( " + dr["PriceAdjustmentNewPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.NewPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PriceAdjustmentNewPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (PriceAdjustmentItemAdd.ListID != null || PriceAdjustmentItemAdd.NewPrice != null)
                                PriceAdjustment.PriceAdjustmentItemAdd.Add(PriceAdjustmentItemAdd);


                        }


                    }

                    else
                    {
                        PriceAdjustment = new POSPriceAdjustmentQBEntry();
                        if (dt.Columns.Contains("PriceAdjustmentName"))
                        {
                            #region Validations of PriceAdjustmentName
                            if (dr["PriceAdjustmentName"].ToString() != string.Empty)
                            {
                                if (dr["PriceAdjustmentName"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceAdjustmentName (" + dr["PriceAdjustmentName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustment.PriceAdjustmentName = dr["PriceAdjustmentName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Comments"))
                        {
                            #region Validations of Comments
                            if (dr["Comments"].ToString() != string.Empty)
                            {
                                if (dr["Comments"].ToString().Length > 300)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustment.Comments = dr["Comments"].ToString().Substring(0, 300);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustment.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.Comments = dr["Comments"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustment.Comments = dr["Comments"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 300)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustment.Associate = dr["Associate"].ToString().Substring(0, 300);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustment.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustment.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PriceLevelNumber"))
                        {
                            #region Validations of PriceLevelNumber
                            if (dr["PriceLevelNumber"].ToString() != string.Empty)
                            {
                                if (dr["PriceLevelNumber"].ToString().Length > 5)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustment.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        QBPOSEntities.PriceAdjustmentItemAdd PriceAdjustmentItemAdd = new QBPOSEntities.PriceAdjustmentItemAdd();
                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of ItemName
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Name (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustmentItemAdd.ListID = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("PriceAdjustmentNewPrice"))
                        {
                            #region Validations for PriceAdjustmentNewPrice
                            if (dr["PriceAdjustmentNewPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["PriceAdjustmentNewPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceAdjustmentNewPrice( " + dr["PriceAdjustmentNewPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PriceAdjustmentItemAdd.NewPrice = dr["PriceAdjustmentNewPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    PriceAdjustmentItemAdd.NewPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PriceAdjustmentNewPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (PriceAdjustmentItemAdd.ListID != null || PriceAdjustmentItemAdd.NewPrice != null)
                            PriceAdjustment.PriceAdjustmentItemAdd.Add(PriceAdjustmentItemAdd);

                        coll.Add(PriceAdjustment);
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            #endregion
            
            return coll;

        }
    }
}
