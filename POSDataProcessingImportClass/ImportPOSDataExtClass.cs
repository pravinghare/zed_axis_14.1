﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
using QuickBookEntities;


namespace POSDataProcessingImportClass
{
  public   class ImportPOSDataExtClass
    {
      private static ImportPOSDataExtClass m_ImportPOSDataExtClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSDataExtClass()
        { 
        }

        #endregion

        /// <summary>
        /// Create an instance of Import Custom Field Class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSDataExtClass GetInstance()
        {
            if (m_ImportPOSDataExtClass == null)
                m_ImportPOSDataExtClass = new ImportPOSDataExtClass();
            return m_ImportPOSDataExtClass;
        }

        #region Public methods

        /// <summary>
        /// Method to get DataExt value collection 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <param name="defaultSettings"></param>
        /// <returns></returns>
        public POSDataProcessingImportClass.POSDataExtQBEntryCollection ImportPOSDataExt(string QBFileName, DataTable dt, ref string logDirectory)
        {
            POSDataProcessingImportClass.POSDataExtQBEntryCollection coll = new POSDataExtQBEntryCollection();
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;

                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    string datevalue = string.Empty;

                    //Customer Validation
                    POSDataProcessingImportClass.POSDataExtQBEntry Customer = new POSDataExtQBEntry();

                    Customer.OwnerID = "0";


                    if (dt.Columns.Contains("DataExtName"))
                    {
                        #region Validation of Ext Name
                        if (dr["DataExtName"].ToString() != string.Empty)
                        {
                            if (dr["DataExtName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["DataExtName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.DataExtName = dr["DataExtName"].ToString().Trim();
                                    }
                                }
                                else
                                {
                                    Customer.DataExtName = dr["DataExtName"].ToString().Trim();
                                }
                            }
                            else
                            {
                                Customer.DataExtName = dr["DataExtName"].ToString().Trim();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ListDataExtType"))
                    {
                        #region Validation of Value
                        if (dr["ListDataExtType"].ToString() != string.Empty)
                        {
                            if (dr["ListDataExtType"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["ListDataExtType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ListDataExtType = dr["ListDataExtType"].ToString().Trim();
                                    }
                                }
                                else
                                {
                                    Customer.ListDataExtType = dr["ListDataExtType"].ToString().Trim();
                                }
                            }
                            else
                            {
                                Customer.ListDataExtType = dr["ListDataExtType"].ToString().Trim();
                            }
                        }
                        #endregion
                    }

                    //aadding List obj ref fullnam.

                    QuickBookEntities.ListObjRef listObject = new QuickBookEntities.ListObjRef();

                   if (dt.Columns.Contains("List"))
                    {
                        if (dr["List"].ToString() != string.Empty)
                        {
                            listObject.ListID = dr["List"].ToString().Trim();
                        }
                    }
                    Customer.ListObjRef.Add(listObject);

                    if (dt.Columns.Contains("DataExtValue"))
                    {
                        #region Validation of Value
                        if (dr["DataExtValue"].ToString() != string.Empty)
                        {
                            if (dr["DataExtValue"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["DataExtValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.DataExtValue = dr["DataExtValue"].ToString().Trim();
                                    }
                                }
                                else
                                {
                                    Customer.DataExtValue = dr["DataExtValue"].ToString().Trim();
                                }
                            }
                            else
                            {
                                Customer.DataExtValue = dr["DataExtValue"].ToString().Trim();
                            }
                        }
                        #endregion
                    }
                    coll.Add(Customer);
                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    listCount++;
                }
            }
            return coll;

        }

        #endregion
    }
}
