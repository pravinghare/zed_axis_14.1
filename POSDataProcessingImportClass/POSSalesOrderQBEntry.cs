﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
    [XmlRootAttribute("POSSalesOrderQBEntry", Namespace = "", IsNullable = false)]
   public class POSSalesOrderQBEntry
    {
        #region Private Member Variable
        private string m_Associate;       
        private string m_Cashier;       
        private string m_CustomerListID;               
        private string m_Discount;   
        private string m_DiscountPercent;       
        private string m_Instructions;        
        private string m_PriceLevelNumber;        
        private string m_PromoCode;        
        private string m_SalesOrderNumber;        
        private string m_SalesOrderStatusDesc;        
        private string m_SalesOrderType;        
        private string m_TaxCategory;        
        private string m_TxnDate;        
        private Collection<ShippingInformation> m_ShippingInformation = new Collection<ShippingInformation>();               
        private Collection<SalesOrderItemAdd> m_SalesOrderItemAdd = new Collection<SalesOrderItemAdd>();
              
        #endregion

        #region Constructor
        public POSSalesOrderQBEntry()
        {
        }
        #endregion

        #region Private Member Variable
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
       
        public string Cashier
        {
            get { return m_Cashier; }
            set { m_Cashier = value; }
        }
        
        public string CustomerListID
        {
            get { return m_CustomerListID; }
            set { m_CustomerListID = value; }
        }       

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }        

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
        
        public string Instructions
        {
            get { return m_Instructions; }
            set { m_Instructions = value; }
        }

        public string PriceLevelNumber
        {
            get { return m_PriceLevelNumber; }
            set { m_PriceLevelNumber = value; }
        }
        
        public string PromoCode
        {
            get { return m_PromoCode; }
            set { m_PromoCode = value; }
        }       

        public string SalesOrderNumber
        {
            get { return m_SalesOrderNumber; }
            set { m_SalesOrderNumber = value; }
        }
       
        public string SalesOrderStatusDesc
        {
            get { return m_SalesOrderStatusDesc; }
            set { m_SalesOrderStatusDesc = value; }
        }
       
        public string SalesOrderType
        {
            get { return m_SalesOrderType; }
            set { m_SalesOrderType = value; }
        }       

        public string TaxCategory
        {
            get { return m_TaxCategory; }
            set { m_TaxCategory = value; }
        }
       
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        [XmlArray("ShippingInformationREM")]
        public Collection<ShippingInformation> ShippingInformation
        {
            get { return m_ShippingInformation; }
            set { m_ShippingInformation = value; }
        }

        [XmlArray("SalesOrderItemAddREM")]
        public Collection<SalesOrderItemAdd> SalesOrderItemAdd
        {
            get { return m_SalesOrderItemAdd; }
            set { m_SalesOrderItemAdd = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSSalesOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement SalesOrderAddRq = inputXMLDoc.CreateElement("SalesOrderAddRq");
            qbXMLMsgsRq.AppendChild(SalesOrderAddRq);
            SalesOrderAddRq.SetAttribute("requestID", "1");
            XmlElement SalesOrderAdd = inputXMLDoc.CreateElement("SalesOrderAdd");
            SalesOrderAddRq.AppendChild(SalesOrderAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<ShippingInformationREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingInformationREM>", string.Empty);
            requestXML = requestXML.Replace("</ShippingInformationREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesOrderItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderItemAddREM>", string.Empty);
            
            SalesOrderAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {
                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesOrderAddRs"))
                    {
                        
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesOrderAddRs/SalesOrderRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofSalesOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        //update pos customer
/// <summary>
/// update method
/// </summary>
/// <param name="statusMessage"></param>
/// <param name="requestText"></param>
/// <param name="rowcount"></param>
/// <param name="txnId"></param>
/// <returns></returns>
        public bool UpdateSalesOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount,  string txnId)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSSalesOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement SalesOrderModRq = inputXMLDoc.CreateElement("SalesOrderModRq");
            qbXMLMsgsRq.AppendChild(SalesOrderModRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement SalesOrderMod = inputXMLDoc.CreateElement("SalesOrderMod");
            SalesOrderModRq.AppendChild(SalesOrderMod);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<ShippingInformationREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingInformationREM>", string.Empty);
            requestXML = requestXML.Replace("</ShippingInformationREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesOrderItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderItemAddREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesOrderItemAdd>", "<SalesOrderItemMod>");
            requestXML = requestXML.Replace("</SalesOrderItemAdd>", "</SalesOrderItemMod>");
            //Axis 565 reopen
            if (requestXML.Contains("<UPC>"))
            {
                requestXML = requestXML.Replace("<ListID>", "<TxnLineID>-1</TxnLineID><ListID>");
            }
            else
            {
                requestXML = requestXML.Replace("<Desc1>", "<TxnLineID>-1</TxnLineID><Desc1>");
            }
            //Axis 565 reopen ends
            SalesOrderMod.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;

            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/SalesOrderModRq/SalesOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement TxnID = inputXMLDoc.CreateElement("TxnID");
           
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertBefore(TxnID, firstChild).InnerText = txnId;

          
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesOrderModRs"))
                    {
                       
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesOrderModRs/SalesOrderRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofSalesOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class POSSalesOrderQBEntryCollection : Collection<POSSalesOrderQBEntry>
    {
        public POSSalesOrderQBEntry FindsalesOrderNumberEntry(string salesOrderNumber)
        {
            foreach (POSSalesOrderQBEntry item in this)
            {
                if (item.SalesOrderNumber == salesOrderNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }

    public enum SalesOrderType
    {
        SalesOrder, 
        Layaway,
        WorkOrder,
        WebOrder
    }
    
}
