﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;
namespace POSDataProcessingImportClass
{
  public class ImportPOSTimeEntryClass
    {
       private static ImportPOSTimeEntryClass m_ImportPOSTimeEntryClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSTimeEntryClass()
        {   }

        #endregion

        /// <summary>
        /// creating new instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSTimeEntryClass GetInstance()
        {
            if (m_ImportPOSTimeEntryClass == null)
                m_ImportPOSTimeEntryClass = new ImportPOSTimeEntryClass();
            return m_ImportPOSTimeEntryClass;
        }
      /// <summary>
        /// Creating new time entry transaction for pos.
      /// </summary>
      /// <param name="QBFileName"></param>
      /// <param name="dt"></param>
      /// <param name="logDirectory"></param>
      /// <returns></returns>

        public POSDataProcessingImportClass.POSTimeQBEntryCollection ImportPOSTimeEntryData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSTimeQBEntryCollection coll = new POSTimeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
          
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    //   DateTime EmployeeDt = new DateTime();
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    POSDataProcessingImportClass.POSTimeQBEntry TimeQBEntry = new POSDataProcessingImportClass.POSTimeQBEntry();

                    
                    if (dt.Columns.Contains("ClockInTime"))
                    {
                        #region validations of ClockInTime
                        if (dr["ClockInTime"].ToString() != "<None>" || dr["ClockInTime"].ToString() != string.Empty)
                        {
                            datevalue = dr["ClockInTime"].ToString();
                            if (!DateTime.TryParse(datevalue, out PODate))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ClockInTime (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeQBEntry.ClockInTime = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeQBEntry.ClockInTime = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeQBEntry.ClockInTime = datevalue;
                                    }
                                }
                                else
                                {
                                    PODate = dttest;
                                    TimeQBEntry.ClockInTime = dttest.ToString("yyyy-MM-ddThh:mm:ss");
                                }

                            }
                            else
                            {
                                PODate = Convert.ToDateTime(datevalue);
                                TimeQBEntry.ClockInTime = DateTime.Parse(datevalue).ToString("yyyy-MM-ddThh:mm:ss");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ClockOutTime"))
                    {
                        #region validations of ClockOutTime
                        if (dr["ClockOutTime"].ToString() != "<None>" || dr["ClockOutTime"].ToString() != string.Empty)
                        {
                            datevalue = dr["ClockOutTime"].ToString();
                            if (!DateTime.TryParse(datevalue, out PODate))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ClockOutTime (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeQBEntry.ClockOutTime = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeQBEntry.ClockOutTime = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeQBEntry.ClockOutTime = datevalue;
                                    }
                                }
                                else
                                {
                                    PODate = dttest;
                                    TimeQBEntry.ClockOutTime = dttest.ToString("yyyy-MM-ddThh:mm:ss");
                                }

                            }
                            else
                            {
                                PODate = Convert.ToDateTime(datevalue);
                                TimeQBEntry.ClockOutTime = DateTime.Parse(datevalue).ToString("yyyy-MM-ddThh:mm:ss");
                            }
                        }
                        #endregion
                    }
                                      
                    if (dt.Columns.Contains("CreatedBy"))
                    {
                        #region Validations of Instructions
                        if (dr["CreatedBy"].ToString() != string.Empty)
                        {
                            if (dr["CreatedBy"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreatedBy (" + dr["CreatedBy"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeQBEntry.CreatedBy = dr["CreatedBy"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeQBEntry.CreatedBy = dr["CreatedBy"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeQBEntry.CreatedBy = dr["CreatedBy"].ToString();
                                }
                            }
                            else
                            {
                                TimeQBEntry.CreatedBy = dr["CreatedBy"].ToString();
                            }
                        }
                        #endregion
                    }

                   
                    if (dt.Columns.Contains("QuickBooksFlag"))
                    {
                        #region Validations of QuickBooksFlag
                        if (dr["QuickBooksFlag"].ToString() != string.Empty)
                        {
                            try
                            {
                                TimeQBEntry.QuickBooksFlag = Convert.ToString((POSDataProcessingImportClass.QuickBooksFlag)Enum.Parse(typeof(POSDataProcessingImportClass.QuickBooksFlag), dr["QuickBooksFlag"].ToString(), true));
                            }
                            catch
                            {
                                TimeQBEntry.QuickBooksFlag = dr["QuickBooksFlag"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeeLoginName"))
                    {
                        #region Validations of EmployeeLoginName
                        if (dr["EmployeeLoginName"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeLoginName"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee Login Name(" + dr["EmployeeLoginName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeQBEntry.EmployeeListID = dr["EmployeeLoginName"].ToString().Substring(0, 1000);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeQBEntry.EmployeeListID = dr["EmployeeLoginName"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeQBEntry.EmployeeListID = dr["EmployeeLoginName"].ToString();
                                }
                            }
                            else
                            {
                                TimeQBEntry.EmployeeListID = dr["EmployeeLoginName"].ToString();
                            }
                        }
                        #endregion
                    }
                   
                    if (dt.Columns.Contains("StoreNumber"))
                    {
                        #region Validations of StoreNumber
                        if (dr["StoreNumber"].ToString() != string.Empty)
                        {
                            if (dr["StoreNumber"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor ListID (" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeQBEntry.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeQBEntry.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeQBEntry.StoreNumber = dr["StoreNumber"].ToString();
                                }
                            }
                            else
                            {
                                TimeQBEntry.StoreNumber = dr["StoreNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    coll.Add(TimeQBEntry);
                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;

        }
    }
}
