﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;

namespace POSDataProcessingImportClass
{
   public  class ImportPOSEmployeeClass
    {
       private static ImportPOSEmployeeClass m_ImportPOSEmployeeClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSEmployeeClass()
        {   }

        #endregion
       /// <summary>
       /// creating new instance of class
       /// </summary>
       /// <returns></returns>
        public static ImportPOSEmployeeClass GetInstance()
        {
            if (m_ImportPOSEmployeeClass == null)
                m_ImportPOSEmployeeClass = new ImportPOSEmployeeClass();
            return m_ImportPOSEmployeeClass;
        }

       /// <summary>
        /// Creating new Employee transaction for pos.
       /// </summary>
       /// <param name="QBFileName"></param>
       /// <param name="dt"></param>
       /// <param name="logDirectory"></param>
       /// <returns></returns>
        public POSDataProcessingImportClass.POSEmployeeQBEntryCollection ImportPOSEmployeeData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSEmployeeQBEntryCollection coll = new POSEmployeeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            // int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                
                    string datevalue = string.Empty;

                    //Employee Validation
                    POSDataProcessingImportClass.POSEmployeeQBEntry Employee = new POSEmployeeQBEntry();

                    if (dt.Columns.Contains("City"))
                    {
                        #region Validations of City
                        if (dr["City"].ToString() != string.Empty)
                        {
                            if (dr["City"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This City (" + dr["City"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.City = dr["City"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.City = dr["City"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.City = dr["City"].ToString();
                                }
                            }
                            else
                            {
                                Employee.City = dr["City"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CommissionPercent"))
                    {
                        #region Validations for CommissionPercent
                        if (dr["CommissionPercent"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["CommissionPercent"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CommissionPercent( " + dr["CommissionPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.CommissionPercent = dr["CommissionPercent"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.CommissionPercent = dr["CommissionPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.CommissionPercent = dr["CommissionPercent"].ToString();
                                }
                            }
                            else
                            {
                                Employee.CommissionPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CommissionPercent"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("Country"))
                    {
                        #region Validations of Country
                        if (dr["Country"].ToString() != string.Empty)
                        {
                            if (dr["Country"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Country (" + dr["Country"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Country = dr["Country"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Country = dr["Country"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Country = dr["Country"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Country = dr["Country"].ToString();
                            }
                        }
                        #endregion
                    }                 
                    
                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Email = dr["Email"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.FirstName = dr["FirstName"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsTrackingHours"))
                    {
                        #region Validations of IsTrackingHours
                        if (dr["IsTrackingHours"].ToString() != "<None>" || dr["IsTrackingHours"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsTrackingHours"].ToString(), out result))
                            {
                                Employee.IsTrackingHours = Convert.ToInt32(dr["IsTrackingHours"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsTrackingHours"].ToString().ToLower() == "true")
                                {
                                    Employee.IsTrackingHours = dr["IsTrackingHours"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsTrackingHours"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsTrackingHours";
                                    }
                                    else
                                        Employee.IsTrackingHours = dr["IsTrackingHours"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsTrackingHours(" + dr["IsTrackingHours"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Employee.IsTrackingHours = dr["IsTrackingHours"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.IsTrackingHours = dr["IsTrackingHours"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Employee.IsTrackingHours = dr["IsTrackingHours"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }
                    
                   
                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName(" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.LastName = dr["LastName"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LoginName"))
                    {
                        #region Validations of LoginName
                        if (dr["LoginName"].ToString() != string.Empty)
                        {
                            if (dr["LoginName"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LoginName(" + dr["LoginName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.LoginName = dr["LoginName"].ToString().Substring(0, 20);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.LoginName = dr["LoginName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.LoginName = dr["LoginName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.LoginName = dr["LoginName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 245)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes(" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Notes = dr["Notes"].ToString().Substring(0, 245);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    Employee.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                Employee.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Phone = dr["Phone"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone2"))
                    {
                        #region Validations of  Phone2
                        if (dr["Phone2"].ToString() != string.Empty)
                        {
                            if (dr["Phone2"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone2 (" + dr["Phone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Phone2 = dr["Phone2"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Phone2 = dr["Phone2"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Phone2 = dr["Phone2"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Phone2 = dr["Phone2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone3"))
                    {
                        #region Validations of Phone3
                        if (dr["Phone3"].ToString() != string.Empty)
                        {
                            if (dr["Phone3"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone3 (" + dr["Phone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Phone3 = dr["Phone3"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Phone3 = dr["Phone3"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Phone3 = dr["Phone3"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Phone3 = dr["Phone3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone4"))
                    {
                        #region Validations of Phone4
                        if (dr["Phone4"].ToString() != string.Empty)
                        {
                            if (dr["Phone4"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone4 (" + dr["Phone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Phone4 = dr["Phone4"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Phone4 = dr["Phone4"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Phone4 = dr["Phone4"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Phone4 = dr["Phone4"].ToString();
                            }
                        }
                        #endregion
                    }
                                                          
                    if (dt.Columns.Contains("PostalCode"))
                    {
                        #region Validations of  Postal Code
                        if (dr["PostalCode"].ToString() != string.Empty)
                        {
                            if (dr["PostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Postal Code (" + dr["PostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.PostalCode = dr["PostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.PostalCode = dr["PostalCode"].ToString();
                                    }
                                }
                                else
                                    Employee.PostalCode = dr["PostalCode"].ToString();
                            }
                            else
                            {
                                Employee.PostalCode = dr["PostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("State"))
                    {
                        #region Validations of  State
                        if (dr["State"].ToString() != string.Empty)
                        {
                            if (dr["State"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This State (" + dr["State"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.State = dr["State"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.State = dr["State"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.State = dr["State"].ToString();
                                }
                            }
                            else
                            {
                                Employee.State = dr["State"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Street"))
                    {
                        #region Validations of Street
                        if (dr["Street"].ToString() != string.Empty)
                        {
                            if (dr["Street"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Street (" + dr["Street"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Street = dr["Street"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Street = dr["Street"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Street = dr["Street"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Street = dr["Street"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Street2"))
                    {
                        #region Validations of Street2
                        if (dr["Street2"].ToString() != string.Empty)
                        {
                            if (dr["Street2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Street2 (" + dr["Street2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Street2 = dr["Street2"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Street2 = dr["Street2"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Street2 = dr["Street2"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Street2 = dr["Street2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SecurityGroup"))
                    {
                        #region Validations of SecurityGroup
                        if (dr["SecurityGroup"].ToString() != string.Empty)
                        {
                            if (dr["SecurityGroup"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SecurityGroup (" + dr["SecurityGroup"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.SecurityGroup = dr["SecurityGroup"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.SecurityGroup = dr["SecurityGroup"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.SecurityGroup = dr["SecurityGroup"].ToString();
                                }
                            }
                            else
                            {
                                Employee.SecurityGroup = dr["SecurityGroup"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    coll.Add(Employee);

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion
            return coll;
            
        }
    }
}
