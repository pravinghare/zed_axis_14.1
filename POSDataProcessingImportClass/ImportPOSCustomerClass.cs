﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;


namespace POSDataProcessingImportClass
{
   public  class ImportPOSCustomerClass
    {
        private static ImportPOSCustomerClass m_ImportCustomerClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSCustomerClass()
        {   }

        #endregion

        /// <summary>
        /// creating instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSCustomerClass GetInstance()
        {
            if (m_ImportCustomerClass == null)
                m_ImportCustomerClass = new ImportPOSCustomerClass();
            return m_ImportCustomerClass;
        }
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>
        public DataProcessingBlocks.POSCustomerQBEntryCollection ImportPOSCustomerData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Customer Entry collections.
            DataProcessingBlocks.POSCustomerQBEntryCollection coll = new POSCustomerQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
         
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
            
                    string datevalue = string.Empty;

                    //Customer Validation
                    DataProcessingBlocks.POSCustomerQBEntry Customer = new POSCustomerQBEntry();
                   
                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of CompanyName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CompanyName = dr["CompanyName"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("CustomerID"))
                    {
                        #region Validations of CustomerID
                        if (dr["CustomerID"].ToString() != string.Empty)
                        {
                            if (dr["CustomerID"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerID(" + dr["CustomerID"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CustomerID = dr["CustomerID"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CustomerID = dr["CustomerID"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CustomerID = dr["CustomerID"].ToString();
                                }
                            }
                            else
                            {
                                Customer.CustomerID = dr["CustomerID"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("CustomerFullName"))
                    {
                        #region Validations of CustomerFullName
                        if (dr["CustomerFullName"].ToString() != string.Empty)
                        {
                            if (dr["CustomerFullName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerFullName (" + dr["CustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CustomerFullName = dr["CustomerFullName"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CustomerFullName = dr["CustomerFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CustomerFullName = dr["CustomerFullName"].ToString();
                                }
                            }
                            else
                            {
                               Customer.CustomerFullName = dr["CustomerFullName"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("CustomerDiscPercent"))
                    {
                        #region Validations for CustomerDiscPercent
                        if (dr["CustomerDiscPercent"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["CustomerDiscPercent"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerDiscPercent( " + dr["CustomerDiscPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CustomerDiscPercent = dr["CustomerDiscPercent"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CustomerDiscPercent = dr["CustomerDiscPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CustomerDiscPercent = dr["CustomerDiscPercent"].ToString();
                                }
                            }
                            else
                            {
                                Customer.CustomerDiscPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CustomerDiscPercent"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("CustomerDiscType"))
                    {
                        #region Validations of CustomerDiscType
                        if (dr["CustomerDiscType"].ToString() != string.Empty)
                        {
                            try
                            {
                                Customer.CustomerDiscType = Convert.ToString((DataProcessingBlocks.CustomerDiscType)Enum.Parse(typeof(DataProcessingBlocks.CustomerDiscType), dr["CustomerDiscType"].ToString(), true));
                            }
                            catch
                            {
                                Customer.CustomerDiscType = dr["CustomerDiscType"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CustomerType"))
                    {
                        #region Validations of CustomerType
                        if (dr["CustomerType"].ToString() != string.Empty)
                        {
                            if (dr["CustomerType"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerType (" + dr["CustomerType"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CustomerType = dr["CustomerType"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CustomerType = dr["CustomerType"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CustomerType = dr["CustomerType"].ToString();
                                }
                            }
                            else
                            {
                                Customer.CustomerType = dr["CustomerType"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Email = dr["Email"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsOkToEMail"))
                    {
                        #region Validations of IsActive
                        if (dr["IsOkToEMail"].ToString() != "<None>" || dr["IsOkToEMail"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsOkToEMail"].ToString(), out result))
                            {
                                Customer.IsOkToEMail = Convert.ToInt32(dr["IsOkToEMail"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsOkToEMail"].ToString().ToLower() == "true")
                                {
                                    Customer.IsOkToEMail = dr["IsOkToEMail"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsOkToEMail"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsOkToEMail";
                                    }
                                    else
                                        Customer.IsOkToEMail = dr["IsOkToEMail"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsOkToEMail(" + dr["IsOkToEMail"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsOkToEMail = dr["IsOkToEMail"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsOkToEMail = dr["IsOkToEMail"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsOkToEMail = dr["IsOkToEMail"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.FirstName = dr["FirstName"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsAcceptingChecks"))
                    {
                        #region Validations of IsAcceptingChecks
                        if (dr["IsAcceptingChecks"].ToString() != "<None>" || dr["IsAcceptingChecks"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsAcceptingChecks"].ToString(), out result))
                            {
                                Customer.IsAcceptingChecks  = Convert.ToInt32(dr["IsAcceptingChecks"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsAcceptingChecks"].ToString().ToLower() == "true")
                                {
                                    Customer.IsAcceptingChecks  = dr["IsAcceptingChecks"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsAcceptingChecks"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsAcceptingChecks";
                                    }
                                    else
                                        Customer.IsAcceptingChecks  = dr["IsAcceptingChecks"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsAcceptingChecks(" + dr["IsAcceptingChecks"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsAcceptingChecks  = dr["IsAcceptingChecks"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsAcceptingChecks  = dr["IsAcceptingChecks"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsAcceptingChecks  = dr["IsAcceptingChecks"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("IsUsingChargeAccount"))
                    {
                        #region Validations of IsUsingChargeAccount
                        if (dr["IsUsingChargeAccount"].ToString() != "<None>" || dr["IsUsingChargeAccount"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsUsingChargeAccount"].ToString(), out result))
                            {
                                Customer.IsUsingChargeAccount = Convert.ToInt32(dr["IsUsingChargeAccount"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsUsingChargeAccount"].ToString().ToLower() == "true")
                                {
                                    Customer.IsUsingChargeAccount = dr["IsUsingChargeAccount"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsUsingChargeAccount"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsUsingChargeAccount";
                                    }
                                    else
                                        Customer.IsUsingChargeAccount = dr["IsUsingChargeAccount"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsUsingChargeAccount(" + dr["IsUsingChargeAccount"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsUsingChargeAccount = dr["IsUsingChargeAccount"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsUsingChargeAccount = dr["IsUsingChargeAccount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsUsingChargeAccount = dr["IsUsingChargeAccount"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsUsingWithQB"))
                    {
                        #region Validations of IsUsingWithQB
                        if (dr["IsUsingWithQB"].ToString() != "<None>" || dr["IsUsingWithQB"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsUsingWithQB"].ToString(), out result))
                            {
                                Customer. IsUsingWithQB = Convert.ToInt32(dr["IsUsingWithQB"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsUsingWithQB"].ToString().ToLower() == "true")
                                {
                                    Customer. IsUsingWithQB = dr["IsUsingWithQB"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsUsingWithQB"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsUsingWithQB";
                                    }
                                    else
                                        Customer. IsUsingWithQB = dr["IsUsingWithQB"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  IsUsingWithQB(" + dr["IsUsingWithQB"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer. IsUsingWithQB = dr["IsUsingWithQB"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer. IsUsingWithQB = dr["IsUsingWithQB"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer. IsUsingWithQB = dr["IsUsingWithQB"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsRewardsMember"))
                    {
                        #region Validations of IsRewardsMember 
                        if (dr["IsRewardsMember"].ToString() != "<None>" || dr["IsRewardsMember"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsRewardsMember"].ToString(), out result))
                            {
                                Customer.IsRewardsMember = Convert.ToInt32(dr["IsRewardsMember"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsRewardsMember"].ToString().ToLower() == "true")
                                {
                                    Customer.IsRewardsMember = dr["IsRewardsMember"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsRewardsMember"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsRewardsMember";
                                    }
                                    else
                                        Customer.IsRewardsMember = dr["IsRewardsMember"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsRewardsMember (" + dr["IsRewardsMember"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsRewardsMember = dr["IsRewardsMember"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsRewardsMember = dr["IsRewardsMember"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsRewardsMember = dr["IsRewardsMember"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsNoShipToBilling"))
                    {
                        #region Validations of IsNoShipToBilling 
                        if (dr["IsNoShipToBilling"].ToString() != "<None>" || dr["IsNoShipToBilling"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsNoShipToBilling"].ToString(), out result))
                            {
                                Customer.IsNoShipToBilling  = Convert.ToInt32(dr["IsNoShipToBilling"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsNoShipToBilling"].ToString().ToLower() == "true")
                                {
                                    Customer.IsNoShipToBilling  = dr["IsNoShipToBilling"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsNoShipToBilling"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "IsNoShipToBilling";
                                    }
                                    else
                                        Customer.IsNoShipToBilling  = dr["IsNoShipToBilling"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  IsNoShipToBilling  (" + dr["IsNoShipToBilling"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsNoShipToBilling  = dr["IsNoShipToBilling"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsNoShipToBilling  = dr["IsNoShipToBilling"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsNoShipToBilling  = dr["IsNoShipToBilling"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName(" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.LastName = dr["LastName"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 245)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes(" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Notes = dr["Notes"].ToString().Substring(0, 245);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    Customer.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                Customer.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone = dr["Phone"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone2"))
                    {
                        #region Validations of  Phone2
                        if (dr["Phone2"].ToString() != string.Empty)
                        {
                            if (dr["Phone2"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone2 (" + dr["Phone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone2 = dr["Phone2"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone2 = dr["Phone2"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone2 = dr["Phone2"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone2 = dr["Phone2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone3"))
                    {
                        #region Validations of Phone3
                        if (dr["Phone3"].ToString() != string.Empty)
                        {
                            if (dr["Phone3"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone3 (" + dr["Phone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone3 = dr["Phone3"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone3 = dr["Phone3"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone3 = dr["Phone3"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone3 = dr["Phone3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone4"))
                    {
                        #region Validations of Phone4
                        if (dr["Phone4"].ToString() != string.Empty)
                        {
                            if (dr["Phone4"].ToString().Length > 40)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone4 (" + dr["Phone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone4 = dr["Phone4"].ToString().Substring(0, 40);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone4 = dr["Phone4"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone4 = dr["Phone4"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone4 = dr["Phone4"].ToString();
                            }
                        }
                        #endregion
                    }

                   

                    if (dt.Columns.Contains("PriceLevelNumber"))
                    {
                        #region Validations of PriceLevelNumber
                        if (dr["PriceLevelNumber"].ToString() != string.Empty)
                        {
                            if (dr["PriceLevelNumber"].ToString().Length > 5)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                }
                            }
                            else
                            {
                                Customer.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Salutation"))
                    {
                        #region Validations of Salutation
                        if (dr["Salutation"].ToString() != string.Empty)
                        {
                            if (dr["Salutation"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Salutation (" + dr["Salutation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Salutation = dr["Salutation"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Salutation = dr["Salutation"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Salutation = dr["Salutation"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Salutation = dr["Salutation"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("TaxCategory"))
                    {
                        #region Validations of TaxCategory
                        if (dr["TaxCategory"].ToString() != string.Empty)
                        {
                            if (dr["TaxCategory"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxCategory (" + dr["TaxCategory"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.TaxCategory = dr["TaxCategory"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.TaxCategory = dr["TaxCategory"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.TaxCategory = dr["TaxCategory"].ToString();
                                }
                            }
                            else
                            {
                                Customer.TaxCategory = dr["TaxCategory"].ToString();
                            }
                        }
                        #endregion
                    }                   
                   
                    QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                   
                   
                    if (dt.Columns.Contains("BillCity"))
                    {
                        #region Validations of Bill City
                        if (dr["BillCity"].ToString() != string.Empty)
                        {
                            if (dr["BillCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.City = dr["BillCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.City = dr["BillCity"].ToString();
                            }
                            else
                            {
                                BillAddressItem.City = dr["BillCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillCountry"))
                    {
                        #region Validations of Bill Country
                        if (dr["BillCountry"].ToString() != string.Empty)
                        {
                            if (dr["BillCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Country = dr["BillCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Country = dr["BillCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Country = dr["BillCountry"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Country = dr["BillCountry"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillPostalCode"))
                    {
                        #region Validations of Bill Postal Code
                        if (dr["BillPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["BillPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                            }
                            else
                            {
                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillState"))
                    {
                        #region Validations of Bill State
                        if (dr["BillState"].ToString() != string.Empty)
                        {
                            if (dr["BillState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.State = dr["BillState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.State = dr["BillState"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.State = dr["BillState"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.State = dr["BillState"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("BillStreet"))
                    {
                        #region Validations of BillStreet
                        if (dr["BillStreet"].ToString() != string.Empty)
                        {
                            if (dr["BillStreet"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add1 (" + dr["BillStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Street = dr["BillStreet"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Street = dr["BillStreet"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Street = dr["BillStreet"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Street = dr["BillStreet"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillStreet2"))
                    {
                        #region Validations of BillStreet2
                        if (dr["BillStreet2"].ToString() != string.Empty)
                        {
                            if (dr["BillStreet2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add1 (" + dr["BillStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Street2 = dr["BillStreet2"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Street2 = dr["BillStreet2"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Street2 = dr["BillStreet2"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Street2 = dr["BillStreet2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Street != null || BillAddressItem.Street2 != null)
                        Customer.BillAddress.Add(BillAddressItem);

                    if (dt.Columns.Contains("DefaultShipAddress"))
                    {
                        #region Validations of DefaultShipAddress 
                        if (dr["DefaultShipAddress"].ToString() != string.Empty)
                        {
                            if (dr["DefaultShipAddress"].ToString().Length > 32)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DefaultShipAddress (" + dr["DefaultShipAddress"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.DefaultShipAddress  = dr["DefaultShipAddress"].ToString().Substring(0, 32);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.DefaultShipAddress  = dr["DefaultShipAddress"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.DefaultShipAddress  = dr["DefaultShipAddress"].ToString();
                                }
                            }
                            else
                            {
                                Customer.DefaultShipAddress  = dr["DefaultShipAddress"].ToString();
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();

                    if (dt.Columns.Contains("ShipAddressName"))
                    {
                        #region Validations of Ship Address Name
                        if (dr["ShipAddressName"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddressName"].ToString().Length > 32)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Address Name (" + dr["ShipAddressName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.AddressName = dr["ShipAddressName"].ToString().Substring(0, 32);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.AddressName = dr["ShipAddressName"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.AddressName = dr["ShipAddressName"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.AddressName = dr["ShipAddressName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipCompanyName"))
                    {
                        #region Validations of Ship Company Name
                        if (dr["ShipCompanyName"].ToString() != string.Empty)
                        {
                            if (dr["ShipCompanyName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship CompanyName (" + dr["ShipCompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.CompanyName = dr["ShipCompanyName"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        ShipAddressItem.CompanyName = dr["ShipCompanyName"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.CompanyName = dr["ShipCompanyName"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.CompanyName = dr["ShipCompanyName"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("ShipFullName"))
                    {
                        #region Validations of Ship fullname
                        if (dr["ShipFullName"].ToString() != string.Empty)
                        {
                            if (dr["ShipFullName"].ToString().Length >64)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship full name (" + dr["ShipFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.FullName = dr["ShipFullName"].ToString().Substring(0, 64);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.FullName = dr["ShipFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.FullName = dr["ShipFullName"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.FullName = dr["ShipFullName"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("ShipCity"))
                    {
                        #region Validations of Ship City
                        if (dr["ShipCity"].ToString() != string.Empty)
                        {
                            if (dr["ShipCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.City = dr["ShipCity"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.City = dr["ShipCity"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ShipCountry"))
                    {
                        #region Validations of Ship Country
                        if (dr["ShipCountry"].ToString() != string.Empty)
                        {
                            if (dr["ShipCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Country = dr["ShipCountry"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipPostalCode"))
                    {
                        #region Validations of Ship Postal Code
                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["ShipPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ShipState"))
                    {
                        #region Validations of Ship State
                        if (dr["ShipState"].ToString() != string.Empty)
                        {
                            if (dr["ShipState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.State = dr["ShipState"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.State = dr["ShipState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipStreet"))
                    {
                        #region Validations of ShipStreet
                        if (dr["ShipStreet"].ToString() != string.Empty)
                        {
                            if (dr["ShipStreet"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship street (" + dr["ShipStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Street = dr["ShipStreet"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Street = dr["ShipStreet"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Street = dr["ShipStreet"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.Street = dr["ShipStreet"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipStreet2"))
                    {
                        #region Validations of ShipStreet2
                        if (dr["ShipStreet2"].ToString() != string.Empty)
                        {
                            if (dr["ShipStreet2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship stree2 (" + dr["ShipStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Street2 = dr["ShipStreet2"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Street2 = dr["ShipStreet2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Street2 = dr["ShipStreet2"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.Street2 = dr["ShipStreet2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (ShipAddressItem.AddressName != null || ShipAddressItem.CompanyName != null || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Street != null || ShipAddressItem.Street2 != null)
                        Customer.ShipAddress.Add(ShipAddressItem);
                                                           

                    coll.Add(Customer);

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion

            return coll;
        }
    }
}
