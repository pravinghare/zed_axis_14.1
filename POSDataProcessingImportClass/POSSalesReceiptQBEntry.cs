﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
     [XmlRootAttribute("POSSalesReceiptQBEntry", Namespace = "", IsNullable = false)]
  public class POSSalesReceiptQBEntry
  {
      #region member
      private string m_Associate;
      private string m_Cashier;
      private string m_Comments;   
      private string m_CustomerListID;
      private string m_Discount;
      private string m_DiscountPercent;
      private string m_PriceLevelNumber;
      private string m_PromoCode;
      private string m_SalesOrderTxnID;    
      private string m_SalesReceiptType;
      private string m_ShipDate;
      private string m_StoreNumber;
      private string m_TaxCategory;
         /// <summary>
         /// 11.4 451
         /// </summary>
      private string m_TxnDate;    
      private string m_Workstation;
      private string m_SalesReceiptNumber;

      private Collection<ShippingInformation> m_ShippingInformation = new Collection<ShippingInformation>();
      private Collection<SalesReceiptItemAdd> m_SalesReceiptItemAdd = new Collection<SalesReceiptItemAdd>();      
      private Collection<TenderAccountAdd> m_TenderAccountAdd = new Collection<TenderAccountAdd>();      
      private Collection<TenderCashAdd> m_TenderCashAdd = new Collection<TenderCashAdd>();      
      private Collection<TenderCheckAdd> m_TenderCheckAdd = new Collection<TenderCheckAdd>();      
      private Collection<TenderCreditCardAdd> m_TenderCreditCardAdd = new Collection<TenderCreditCardAdd>();     
      private Collection<TenderDebitCardAdd> m_TenderDebitCardAdd = new Collection<TenderDebitCardAdd>();      
      private Collection<TenderDepositAdd> m_TenderDepositAdd = new Collection<TenderDepositAdd>();      
      private Collection<TenderGiftAdd> m_TenderGiftAdd = new Collection<TenderGiftAdd>();      
      private Collection<TenderGiftCardAdd> m_TenderGiftCardAdd = new Collection<TenderGiftCardAdd>();

      #endregion

      #region Constructor
      public POSSalesReceiptQBEntry()
      {
      }
      #endregion

      #region properties

      public string Associate
      {
          get { return m_Associate; }
          set { m_Associate = value; }
      }

      public string Cashier
      {
          get { return m_Cashier; }
          set { m_Cashier = value; }
      }

      public string Comments
      {
          get { return m_Comments; }
          set { m_Comments = value; }
      }

      public string CustomerListID
      {
          get { return m_CustomerListID; }
          set { m_CustomerListID = value; }
      }

      public string Discount
      {
          get { return m_Discount; }
          set { m_Discount = value; }
      }

      public string DiscountPercent
      {
          get { return m_DiscountPercent; }
          set { m_DiscountPercent = value; }
      }

      public string PriceLevelNumber
      {
          get { return m_PriceLevelNumber; }
          set { m_PriceLevelNumber = value; }
      }

      public string PromoCode
      {
          get { return m_PromoCode; }
          set { m_PromoCode = value; }
      }

      public string SalesOrderTxnID
      {
          get { return m_SalesOrderTxnID; }
          set { m_SalesOrderTxnID = value; }
      }
      
      public string SalesReceiptType
      {
          get { return m_SalesReceiptType; }
          set { m_SalesReceiptType = value; }
      }

      public string ShipDate
      {
          get { return m_ShipDate; }
          set { m_ShipDate = value; }
      }

      public string StoreNumber
      {
          get { return m_StoreNumber; }
          set { m_StoreNumber = value; }
      }

      public string TaxCategory
      {
          get { return m_TaxCategory; }
          set { m_TaxCategory = value; }
      }
         //11.4 451
      public string TxnDate
      {
          get { return m_TxnDate; }
          set { m_TxnDate = value; }
      }
      public string Workstation
      {
          get { return m_Workstation; }
          set { m_Workstation = value; }
      }

      public string SalesReceiptNumber
      {
          get { return m_SalesReceiptNumber; }
          set { m_SalesReceiptNumber = value; }
      } 
 
         [XmlArray("ShippingInformationREM")]
      public Collection<ShippingInformation> ShippingInformation
      {
          get { return m_ShippingInformation; }
          set { m_ShippingInformation = value; }
      }

         [XmlArray("SalesReceiptItemAddREM")]
         public Collection<SalesReceiptItemAdd> SalesReceiptItemAdd
      {
          get { return m_SalesReceiptItemAdd; }
          set { m_SalesReceiptItemAdd = value; }
      }

         [XmlArray("TenderAccountAddREM")]
      public Collection<TenderAccountAdd> TenderAccountAdd
      {
          get { return m_TenderAccountAdd; }
          set { m_TenderAccountAdd = value; }
      }

         [XmlArray("TenderCashAddREM")]
      public Collection<TenderCashAdd> TenderCashAdd
      {
          get { return m_TenderCashAdd; }
          set { m_TenderCashAdd = value; }
      }

         [XmlArray("TenderCheckAddREM")]
      public Collection<TenderCheckAdd> TenderCheckAdd
      {
          get { return m_TenderCheckAdd; }
          set { m_TenderCheckAdd = value; }
      }

         [XmlArray("TenderCreditCardAddREM")]
      public Collection<TenderCreditCardAdd> TenderCreditCardAdd
      {
          get { return m_TenderCreditCardAdd; }
          set { m_TenderCreditCardAdd = value; }
      }

         [XmlArray("TenderDebitCardAddREM")]
      public Collection<TenderDebitCardAdd> TenderDebitCardAdd
      {
          get { return m_TenderDebitCardAdd; }
          set { m_TenderDebitCardAdd = value; }
      }

         [XmlArray("TenderDepositAddREM")]
      public Collection<TenderDepositAdd> TenderDepositAdd
      {
          get { return m_TenderDepositAdd; }
          set { m_TenderDepositAdd = value; }
      }

         [XmlArray("TenderGiftAddREM")]
      public Collection<TenderGiftAdd> TenderGiftAdd
      {
          get { return m_TenderGiftAdd; }
          set { m_TenderGiftAdd = value; }
      }

         [XmlArray("TenderGiftCardAddREM")]
      public Collection<TenderGiftCardAdd> TenderGiftCardAdd
      {
          get { return m_TenderGiftCardAdd; }
          set { m_TenderGiftCardAdd = value; }
      }
        #endregion

        #region  Public Methods
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
      
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
         {

             string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
             try
             {
                 if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                 {
                     if (fileName.Contains("Program Files (x86)"))
                     {
                         fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                     }
                     else
                     {
                         fileName = fileName.Replace("Program Files", Constants.xpPath);
                     }
                 }
                 else
                 {
                     if (fileName.Contains("Program Files (x86)"))
                     {
                         fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                     }
                     else
                     {
                         fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                     }
                 }
             }
             catch { }
             try
             {
                 DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSSalesReceiptQBEntry>.Save(this, fileName);
             }
             catch
             {
                 statusMessage += "\n ";
                 statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                 return false;
             }

             System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
             inputXMLDoc.Load(fileName);

             string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

             inputXMLDoc = new System.Xml.XmlDocument();

             System.IO.File.Delete(fileName);

             inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
             inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
             XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
             inputXMLDoc.AppendChild(qbXML);
             XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
             qbXML.AppendChild(qbXMLMsgsRq);
             qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
             XmlElement SalesReceiptAddRq = inputXMLDoc.CreateElement("SalesReceiptAddRq");
             qbXMLMsgsRq.AppendChild(SalesReceiptAddRq);
             SalesReceiptAddRq.SetAttribute("requestID", "1");
             XmlElement SalesReceiptAdd = inputXMLDoc.CreateElement("SalesReceiptAdd");
             SalesReceiptAddRq.AppendChild(SalesReceiptAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<SalesReceiptNumber />", string.Empty);
             requestXML = requestXML.Replace("<SalesReceiptNumber>", string.Empty);
             requestXML = requestXML.Replace("</SalesReceiptNumber>", string.Empty);

             requestXML = requestXML.Replace("<ShippingInformationREM />", string.Empty);
             requestXML = requestXML.Replace("<ShippingInformationREM>", string.Empty);
             requestXML = requestXML.Replace("</ShippingInformationREM>", string.Empty);

             requestXML = requestXML.Replace("<SalesReceiptItemAddREM />", string.Empty);
             requestXML = requestXML.Replace("<SalesReceiptItemAddREM>", string.Empty);
             requestXML = requestXML.Replace("</SalesReceiptItemAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderAccountAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderAccountAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderAccountAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderCashAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderCashAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderCashAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderCheckAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderCheckAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderCheckAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderCreditCardAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderCreditCardAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderCreditCardAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderDebitCardAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderDebitCardAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderDebitCardAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderDepositAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderDepositAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderDepositAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderGiftAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderGiftAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderGiftAddREM>", string.Empty);

             requestXML = requestXML.Replace("<TenderGiftCardAddREM />", string.Empty);
             requestXML = requestXML.Replace("<TenderGiftCardAddREM>", string.Empty);
             requestXML = requestXML.Replace("</TenderGiftCardAddREM>", string.Empty);

            

             SalesReceiptAdd.InnerXml = requestXML;

             requestText = inputXMLDoc.OuterXml;
             string responseFile = string.Empty;
             string resp = string.Empty;
             try
             {
                 responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                 if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                 {

                     resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                 }
                 else
                 {
                     resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                 }
             }
             catch (Exception ex)
             {
                 CommonUtilities.WriteErrorLog(ex.Message);
                 CommonUtilities.WriteErrorLog(ex.StackTrace);
             }

             finally
             {
                 if (resp != string.Empty)
                 {
                     string statusSeverity = string.Empty;
                     System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                     outputXMLDoc.LoadXml(resp);
                     foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesReceiptAddRs"))
                     {
                        
                         try
                         {
                             statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                         }
                         catch
                         { }
                         TransactionImporter.TransactionImporter.testFlag = true;
                         if (statusSeverity == "Error")
                         {
                             string requesterror = string.Empty;
                             try
                             {
                                 requesterror = oNode.Attributes["requestID"].Value.ToString();
                             }
                             catch { }
                             string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                             foreach (string s in array_msg)
                             {
                                 if (s != "")
                                 { statusMessage += s + ".\n"; }
                                 if (s == "")
                                 { statusMessage += s; }
                             }
                             statusMessage += "The Error location is " + requesterror;
                             TransactionImporter.TransactionImporter.testFlag = false;

                         }
                     }
                     if (statusSeverity != "Error")
                     {
                         foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/SalesReceiptAddRs/SalesReceiptRet"))
                         {
                             CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                         }
                     }
                     CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                 }
             }

             if (resp == string.Empty)
             {
                 statusMessage += "\n ";
                 statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofSalesReceipt(this);
                 statusMessage += "\n ";
                 return false;
             }
             else
             {
                 if (resp.Contains("statusSeverity=\"Error\""))
                 {
                     return false;

                 }
                 else
                     return true;
             }
         }

      #endregion

  }

     public class POSSalesReceiptQBEntryCollection : Collection<POSSalesReceiptQBEntry>
     {
         public POSSalesReceiptQBEntry FindpurchaseOrderNumberEntry(string salesReceiptNumber)
         {
             foreach (POSSalesReceiptQBEntry item in this)
             {
                 if (item.SalesReceiptNumber == salesReceiptNumber)
                 {
                     return item;
                 }
             }
             return null;
         }
     }
         
}
