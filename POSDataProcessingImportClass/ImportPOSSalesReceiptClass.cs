﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using DataProcessingBlocks;

namespace POSDataProcessingImportClass
{
    public class ImportPOSSalesReceiptClass
    {

        private static ImportPOSSalesReceiptClass m_ImportPOSSalesReceiptClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportPOSSalesReceiptClass()
        { }

        #endregion

        /// <summary>
        /// creating new instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportPOSSalesReceiptClass GetInstance()
        {
            if (m_ImportPOSSalesReceiptClass == null)
                m_ImportPOSSalesReceiptClass = new ImportPOSSalesReceiptClass();
            return m_ImportPOSSalesReceiptClass;
        }
        /// Creating new salesreceipt transaction for pos.
        public POSDataProcessingImportClass.POSSalesReceiptQBEntryCollection ImportPOSSalesReceiptData(string QBFileName, DataTable dt, ref string logDirectory)
        {

            //Create an instance of Employee Entry collections.
            POSDataProcessingImportClass.POSSalesReceiptQBEntryCollection coll = new POSSalesReceiptQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    //   DateTime EmployeeDt = new DateTime();
                    string datevalue = string.Empty;
                    string salesReceipt = string.Empty;
                    DateTime SODate = new DateTime();
                    //Employee Validation

                    POSDataProcessingImportClass.POSSalesReceiptQBEntry SalesReceipt = new POSSalesReceiptQBEntry();
                    if (dt.Columns.Contains("SalesReceiptNumber"))
                    {
                        SalesReceipt = coll.FindpurchaseOrderNumberEntry(dr["SalesReceiptNumber"].ToString());

                        if (SalesReceipt == null)
                        {
                            SalesReceipt = new POSSalesReceiptQBEntry();
                            if (dt.Columns.Contains("Associate"))
                            {
                                #region Validations of Associate
                                if (dr["Associate"].ToString() != string.Empty)
                                {
                                    if (dr["Associate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.Associate = dr["Associate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.Associate = dr["Associate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Associate = dr["Associate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Cashier"))
                            {
                                #region Validations of Cashier
                                if (dr["Cashier"].ToString() != string.Empty)
                                {
                                    if (dr["Cashier"].ToString().Length > 16)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cashier (" + dr["Cashier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.Cashier = dr["Cashier"].ToString().Substring(0, 16);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.Cashier = dr["Cashier"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.Cashier = dr["Cashier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Cashier = dr["Cashier"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Comments"))
                            {
                                #region Validations of Comments
                                if (dr["Comments"].ToString() != string.Empty)
                                {
                                    if (dr["Comments"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.Comments = dr["Comments"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.Comments = dr["Comments"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Comments = dr["Comments"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomerFullName"))
                            {
                                #region Validations of CustomerListID
                                if (dr["CustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer Full Name (" + dr["CustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Discount"))
                            {
                                #region Validations for Discount
                                if (dr["Discount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.Discount = dr["Discount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.Discount = dr["Discount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountPercent"))
                            {
                                #region Validations for DiscountPercent
                                if (dr["DiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("PriceLevelNumber"))
                            {
                                #region Validations of PriceLevelNumber
                                if (dr["PriceLevelNumber"].ToString() != string.Empty)
                                {
                                    if (dr["PriceLevelNumber"].ToString().Length > 5)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PromoCode"))
                            {
                                #region Validations of PromoCode
                                if (dr["PromoCode"].ToString() != string.Empty)
                                {
                                    if (dr["PromoCode"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PromoCode (" + dr["PromoCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.PromoCode = dr["PromoCode"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesOrderNumber"))
                            {
                                #region Validations of SalesOrderNumber
                                if (dr["SalesOrderNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesOrderNumber"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesOrderNumber (" + dr["SalesOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString().Substring(0, 32);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptNumber"))
                            {
                                #region Validations of SalesReceiptNumber
                                if (dr["SalesReceiptNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptNumber"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceiptNumber (" + dr["SalesReceiptNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString().Substring(0, 32);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptType"))
                            {
                                #region Validations of SalesReceiptType
                                if (dr["SalesReceiptType"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptType"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt Type (" + dr["SalesReceiptType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString().Substring(0, 10);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipDate"))
                            {
                                #region validations of ShipDate
                                if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ShipDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesReceipt.ShipDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesReceipt.ShipDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                SalesReceipt.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            SalesReceipt.ShipDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        SalesReceipt.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("StoreNumber"))
                            {
                                #region Validations of StoreNumber
                                if (dr["StoreNumber"].ToString() != string.Empty)
                                {
                                    if (dr["StoreNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TaxCategory"))
                            {
                                #region Validations of TaxCategory
                                if (dr["TaxCategory"].ToString() != string.Empty)
                                {
                                    if (dr["TaxCategory"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["TaxCategory"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.TaxCategory = dr["TaxCategory"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //11.4 451

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    SalesReceipt.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    SalesReceipt.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                SalesReceipt.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            SalesReceipt.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        SalesReceipt.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Workstation"))
                            {
                                #region Validations of Workstation
                                if (dr["Workstation"].ToString() != string.Empty)
                                {
                                    if (dr["Workstation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Tax Category(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.Workstation = dr["Workstation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                #endregion
                            }


                            QBPOSEntities.ShippingInformation ShippingInformationItem = new QBPOSEntities.ShippingInformation();
                            if (dt.Columns.Contains("ShipAddressName"))
                            {
                                #region Validations of ShipAddressName
                                if (dr["ShipAddressName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddressName"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddressName (" + dr["ShipAddressName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipCity"))
                            {
                                #region Validations of ShipCity
                                if (dr["ShipCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCity (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.City = dr["ShipCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipCompanyName"))
                            {
                                #region Validations of ShipCompanyName
                                if (dr["ShipCompanyName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCompanyName"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCompanyName (" + dr["ShipCompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipCountry"))
                            {
                                #region Validations of ShipCountry
                                if (dr["ShipCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCountry"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipCountry (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Country = dr["ShipCountry"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipFullName"))
                            {
                                #region Validations of ShipFullName
                                if (dr["ShipFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipFullName"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipFullName (" + dr["ShipFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.FullName = dr["ShipFullName"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipPhone"))
                            {
                                #region Validations of ShipPhone
                                if (dr["ShipPhone"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone (" + dr["ShipPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone = dr["ShipPhone"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPhone2"))
                            {
                                #region Validations of ShipPhone2
                                if (dr["ShipPhone2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone2 (" + dr["ShipPhone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPhone3"))
                            {
                                #region Validations of ShipPhone3
                                if (dr["ShipPhone3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone3 (" + dr["ShipPhone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipPhone4"))
                            {
                                #region Validations of ShipPhone4
                                if (dr["ShipPhone4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPhone4"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Phone4 (" + dr["ShipPhone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipPostalCode"))
                            {
                                #region Validations of ShipPostalCode
                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPostalCode"].ToString().Length > 10)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipPostalCode (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 10);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipBy"))
                            {
                                #region Validations of ShipBy
                                if (dr["ShipBy"].ToString() != string.Empty)
                                {
                                    if (dr["ShipBy"].ToString().Length > 50)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipBy (" + dr["ShipBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.ShipBy = dr["ShipBy"].ToString().Substring(0, 50);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Shipping"))
                            {
                                #region Validations for Shipping
                                if (dr["Shipping"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Shipping ( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Shipping = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ShipState"))
                            {
                                #region Validations of ShipState
                                if (dr["ShipState"].ToString() != string.Empty)
                                {
                                    if (dr["ShipState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipState (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipStreet"))
                            {
                                #region Validations of ShipStreet
                                if (dr["ShipStreet"].ToString() != string.Empty)
                                {
                                    if (dr["ShipStreet"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipStreet (" + dr["ShipStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Street = dr["ShipStreet"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipStreet2"))
                            {
                                #region Validations of ShipStreet2
                                if (dr["ShipStreet2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipStreet2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipStreet2 (" + dr["ShipStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (ShippingInformationItem.AddressName != null || ShippingInformationItem.City != null || ShippingInformationItem.CompanyName != null || ShippingInformationItem.Country != null || ShippingInformationItem.FullName != null || ShippingInformationItem.Phone != null || ShippingInformationItem.Phone2 != null || ShippingInformationItem.Phone3 != null || ShippingInformationItem.Phone4 != null || ShippingInformationItem.PostalCode != null || ShippingInformationItem.State != null || ShippingInformationItem.Street != null || ShippingInformationItem.Street2 != null)
                                SalesReceipt.ShippingInformation.Add(ShippingInformationItem);

                            QBPOSEntities.SalesReceiptItemAdd SalesReceiptItemAdd = new QBPOSEntities.SalesReceiptItemAdd();


                            if (dt.Columns.Contains("SalesReceiptALU"))
                            {
                                #region Validations of SalesReceiptALU
                                if (dr["SalesReceiptALU"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt ALU (" + dr["SalesReceiptALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            SalesReceiptItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesReceiptItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptAssociate"))
                            {
                                #region Validations of SalesReceiptAssociate
                                if (dr["SalesReceiptAssociate"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptAssociate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt Associate (" + dr["SalesReceiptAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptAttribute"))
                            {
                                #region Validations of SalesReceiptAttribute
                                if (dr["SalesReceiptAttribute"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptAttribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt  Attribute (" + dr["SalesReceiptAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptCommission"))
                            {
                                #region Validations for SalesReceiptCommission
                                if (dr["SalesReceiptCommission"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptCommission"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt Commission( " + dr["SalesReceiptCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptCommission"].ToString()));
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDesc2"))
                            {
                                #region Validations of SalesReceiptDesc2
                                if (dr["SalesReceiptDesc2"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptDesc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Desc2 (" + dr["SalesReceiptDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscount"))
                            {
                                #region Validations for SalesReceiptDiscount
                                if (dr["SalesReceiptDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount( " + dr["SalesReceiptDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscountPercent"))
                            {
                                #region Validations for SalesReceiptDiscountPercent
                                if (dr["SalesReceiptDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount Percent( " + dr["SalesReceiptDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscountType"))
                            {
                                #region Validations of SalesReceiptDiscountType
                                if (dr["SalesReceiptDiscountType"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptDiscountType"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount Type (" + dr["SalesReceiptDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptExtendedPrice"))
                            {
                                #region Validations for SalesReceiptExtendedPrice
                                if (dr["SalesReceiptExtendedPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptExtendedPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Extended Price( " + dr["SalesReceiptExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptExtendedPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptPrice"))
                            {
                                #region Validations for SalesReceiptPrice
                                if (dr["SalesReceiptPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Price( " + dr["SalesReceiptPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptQty"))
                            {
                                #region Validations of SalesReceiptQty
                                if (dr["SalesReceiptQty"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptQty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Qty (" + dr["SalesReceiptQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptSerialNumber"))
                            {
                                #region Validations of SalesReceiptSerialNumber
                                if (dr["SalesReceiptSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptSerialNumber"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Serial Number (" + dr["SalesReceiptSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptSize"))
                            {
                                #region Validations of SalesReceiptSize
                                if (dr["SalesReceiptSize"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptSize"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Size (" + dr["SalesReceiptSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptTaxCode"))
                            {
                                #region Validations of SalesReceiptTaxCode
                                if (dr["SalesReceiptTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptTaxCode"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt TaxCode (" + dr["SalesReceiptTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptUnitOfMeasure"))
                            {
                                #region Validations of SalesReceiptUnitOfMeasure
                                if (dr["SalesReceiptUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptUnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Unit Of Measure (" + dr["SalesReceiptUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptUPC"))
                            {
                                #region Validations of SalesReceiptUPC
                                if (dr["SalesReceiptUPC"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptUPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt UPC (" + dr["SalesReceiptUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            SalesReceiptItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, SalesReceiptItemAdd.UPC, null);
                                            if (SalesReceiptItemAdd.ListID != null)
                                            {
                                                SalesReceiptItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (SalesReceiptItemAdd.ALU != null || SalesReceiptItemAdd.Associate != null || SalesReceiptItemAdd.Attribute != null || SalesReceiptItemAdd.Commission != null || SalesReceiptItemAdd.Desc1 != null || SalesReceiptItemAdd.Desc2 != null || SalesReceiptItemAdd.Discount != null || SalesReceiptItemAdd.DiscountPercent != null || SalesReceiptItemAdd.DiscountType != null || SalesReceiptItemAdd.ExtendedPrice != null || SalesReceiptItemAdd.Price != null || SalesReceiptItemAdd.Qty != null || SalesReceiptItemAdd.SerialNumber != null || SalesReceiptItemAdd.Size != null || SalesReceiptItemAdd.TaxCode != null || SalesReceiptItemAdd.UnitOfMeasure != null || SalesReceiptItemAdd.UPC != null)
                                SalesReceipt.SalesReceiptItemAdd.Add(SalesReceiptItemAdd);


                            QBPOSEntities.TenderAccountAdd TenderAccountAdd = new QBPOSEntities.TenderAccountAdd();

                            if (dt.Columns.Contains("AccountTenderAmount"))
                            {
                                #region Validations for AccountTenderAmount
                                if (dr["AccountTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AccountTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account TenderAmount ( " + dr["AccountTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderAccountAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["AccountTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("AccountTipAmount"))
                            {
                                #region Validations for AccountTipAmount
                                if (dr["AccountTipAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AccountTipAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account Tip Amount ( " + dr["AccountTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderAccountAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["AccountTipAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderAccountAdd.TenderAmount != null || TenderAccountAdd.TipAmount != null)
                                SalesReceipt.TenderAccountAdd.Add(TenderAccountAdd);

                            QBPOSEntities.TenderCashAdd TenderCashAdd = new QBPOSEntities.TenderCashAdd();

                            if (dt.Columns.Contains("CashTenderAmount"))
                            {
                                #region Validations for CashTenderAmount
                                if (dr["CashTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CashTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cash TenderAmount ( " + dr["CashTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCashAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CashTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderCashAdd.TenderAmount != null)
                                SalesReceipt.TenderCashAdd.Add(TenderCashAdd);

                            QBPOSEntities.TenderCheckAdd TenderCheckAdd = new QBPOSEntities.TenderCheckAdd();
                            if (dt.Columns.Contains("CheckNumber"))
                            {
                                #region Validations of CheckNumber
                                if (dr["CheckNumber"].ToString() != string.Empty)
                                {
                                    if (dr["CheckNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Check Number (" + dr["CheckNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CheckTenderAmount"))
                            {
                                #region Validations for CheckTenderAmount
                                if (dr["CheckTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CheckTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Check TenderAmount ( " + dr["CheckTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCheckAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CheckTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderCheckAdd.CheckNumber != null || TenderCheckAdd.TenderAmount != null)
                                SalesReceipt.TenderCheckAdd.Add(TenderCheckAdd);

                            QBPOSEntities.TenderCreditCardAdd TenderCreditCardAdd = new QBPOSEntities.TenderCreditCardAdd();
                            if (dt.Columns.Contains("CreditCardName"))
                            {
                                #region Validations of CreditCardName
                                if (dr["CreditCardName"].ToString() != string.Empty)
                                {
                                    if (dr["CreditCardName"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CreditCard Name (" + dr["CreditCardName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CreditCardTenderAmount"))
                            {
                                #region Validations for CreditCardTenderAmount
                                if (dr["CreditCardTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CreditCardTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CreditCard TenderAmount ( " + dr["CreditCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CreditCardTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("CreditCardTipAmount"))
                            {
                                #region Validations for CreditCardTipAmount
                                if (dr["CreditCardTipAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CreditCardTipAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CreditCard Tip Amount ( " + dr["CreditCardTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CreditCardTipAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderCreditCardAdd.CardName != null || TenderCreditCardAdd.TenderAmount != null || TenderCreditCardAdd.TipAmount != null)
                                SalesReceipt.TenderCreditCardAdd.Add(TenderCreditCardAdd);

                            QBPOSEntities.TenderDebitCardAdd TenderDebitCardAdd = new QBPOSEntities.TenderDebitCardAdd();
                            if (dt.Columns.Contains("DebitCardCashback"))
                            {
                                #region Validations for DebitCardCashback
                                if (dr["DebitCardCashback"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DebitCardCashback"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DebitCard CashBack ( " + dr["DebitCardCashback"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDebitCardAdd.Cashback = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DebitCardCashback"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("DebitCardTenderAmount"))
                            {
                                #region Validations for DebitCardTenderAmount
                                if (dr["DebitCardTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DebitCardTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DebitCard TenderAmount ( " + dr["DebitCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDebitCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DebitCardTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderDebitCardAdd.Cashback != null || TenderDebitCardAdd.TenderAmount != null)
                                SalesReceipt.TenderDebitCardAdd.Add(TenderDebitCardAdd);

                            QBPOSEntities.TenderDepositAdd TenderDepositAdd = new QBPOSEntities.TenderDepositAdd();
                            if (dt.Columns.Contains("DepositTenderAmount"))
                            {
                                #region Validations for DepositTenderAmount
                                if (dr["DepositTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DepositTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Deposit TenderAmount  ( " + dr["DepositTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDepositAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DepositTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (TenderDepositAdd.TenderAmount != null)
                                SalesReceipt.TenderDepositAdd.Add(TenderDepositAdd);

                            QBPOSEntities.TenderGiftAdd TenderGiftAdd = new QBPOSEntities.TenderGiftAdd();
                            if (dt.Columns.Contains("GiftCertificateNumber"))
                            {
                                #region Validations of GiftCertificateNumber
                                if (dr["GiftCertificateNumber"].ToString() != string.Empty)
                                {
                                    if (dr["GiftCertificateNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Gift CertificateNumber (" + dr["GiftCertificateNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("GiftTenderAmount"))
                            {
                                #region Validations for GiftTenderAmount
                                if (dr["GiftTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["GiftTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Gift TenderAmount ( " + dr["GiftTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (TenderGiftAdd.GiftCertificateNumber != null || TenderGiftAdd.TenderAmount != null)
                                SalesReceipt.TenderGiftAdd.Add(TenderGiftAdd);

                            QBPOSEntities.TenderGiftCardAdd TenderGiftCardAdd = new QBPOSEntities.TenderGiftCardAdd();
                            if (dt.Columns.Contains("GiftCardTenderAmount"))
                            {
                                #region Validations for GiftCardTenderAmount
                                if (dr["GiftCardTenderAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["GiftCardTenderAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GiftCard TenderAmount ( " + dr["GiftCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftCardTenderAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("GiftCardTipAmount"))
                            {
                                #region Validations for GiftCardTipAmount
                                if (dr["GiftCardTipAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["GiftCardTipAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Gift Card Tip Amount ( " + dr["GiftCardTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftCardAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftCardTipAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (TenderGiftCardAdd.TenderAmount != null || TenderGiftCardAdd.TipAmount != null)
                                SalesReceipt.TenderGiftCardAdd.Add(TenderGiftCardAdd);

                            coll.Add(SalesReceipt);

                        }
                        else
                        {
                            QBPOSEntities.SalesReceiptItemAdd SalesReceiptItemAdd = new QBPOSEntities.SalesReceiptItemAdd();

                            if (dt.Columns.Contains("SalesReceiptALU"))
                            {
                                #region Validations of SalesReceiptALU
                                if (dr["SalesReceiptALU"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptALU"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt ALU (" + dr["SalesReceiptALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().ALULookup == true)
                                        {
                                            SalesReceiptItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesReceiptItemAdd.ALU);
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptAssociate"))
                            {
                                #region Validations of SalesReceiptAssociate
                                if (dr["SalesReceiptAssociate"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptAssociate"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt Associate (" + dr["SalesReceiptAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptAttribute"))
                            {
                                #region Validations of SalesReceiptAttribute
                                if (dr["SalesReceiptAttribute"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptAttribute"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt  Attribute (" + dr["SalesReceiptAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptCommission"))
                            {
                                #region Validations for SalesReceiptCommission
                                if (dr["SalesReceiptCommission"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptCommission"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Receipt Commission( " + dr["SalesReceiptCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptCommission"].ToString()));
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("ItemName"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemName"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryDepartment"))
                            {
                                #region Validations of Desc1
                                if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDesc2"))
                            {
                                #region Validations of SalesReceiptDesc2
                                if (dr["SalesReceiptDesc2"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptDesc2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Desc2 (" + dr["SalesReceiptDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscount"))
                            {
                                #region Validations for SalesReceiptDiscount
                                if (dr["SalesReceiptDiscount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptDiscount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount( " + dr["SalesReceiptDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscountPercent"))
                            {
                                #region Validations for SalesReceiptDiscountPercent
                                if (dr["SalesReceiptDiscountPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptDiscountPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount Percent( " + dr["SalesReceiptDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscountPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptDiscountType"))
                            {
                                #region Validations of SalesReceiptDiscountType
                                if (dr["SalesReceiptDiscountType"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptDiscountType"].ToString().Length > 32)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Discount Type (" + dr["SalesReceiptDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString().Substring(0, 32);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptExtendedPrice"))
                            {
                                #region Validations for SalesReceiptExtendedPrice
                                if (dr["SalesReceiptExtendedPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptExtendedPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Extended Price( " + dr["SalesReceiptExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptExtendedPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptPrice"))
                            {
                                #region Validations for SalesReceiptPrice
                                if (dr["SalesReceiptPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SalesReceiptPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Price( " + dr["SalesReceiptPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptQty"))
                            {
                                #region Validations of SalesReceiptQty
                                if (dr["SalesReceiptQty"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptQty"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Qty (" + dr["SalesReceiptQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptSerialNumber"))
                            {
                                #region Validations of SalesReceiptSerialNumber
                                if (dr["SalesReceiptSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptSerialNumber"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Serial Number (" + dr["SalesReceiptSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptSize"))
                            {
                                #region Validations of SalesReceiptSize
                                if (dr["SalesReceiptSize"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptSize"].ToString().Length > 12)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Size (" + dr["SalesReceiptSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString().Substring(0, 12);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesReceiptTaxCode"))
                            {
                                #region Validations of SalesReceiptTaxCode
                                if (dr["SalesReceiptTaxCode"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptTaxCode"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt TaxCode (" + dr["SalesReceiptTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptUnitOfMeasure"))
                            {
                                #region Validations of SalesReceiptUnitOfMeasure
                                if (dr["SalesReceiptUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptUnitOfMeasure"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt Unit Of Measure (" + dr["SalesReceiptUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesReceiptUPC"))
                            {
                                #region Validations of SalesReceiptUPC
                                if (dr["SalesReceiptUPC"].ToString() != string.Empty)
                                {
                                    if (dr["SalesReceiptUPC"].ToString().Length > 18)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesReceipt UPC (" + dr["SalesReceiptUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString().Substring(0, 18);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                        //Axis-565
                                        if (CommonUtilities.GetInstance().UPCLookup == true)
                                        {
                                            SalesReceiptItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, SalesReceiptItemAdd.UPC, null);
                                            if (SalesReceiptItemAdd.ListID != null)
                                            {
                                                SalesReceiptItemAdd.Desc1 = null;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (SalesReceiptItemAdd.ALU != null || SalesReceiptItemAdd.Associate != null || SalesReceiptItemAdd.Attribute != null || SalesReceiptItemAdd.Commission != null || SalesReceiptItemAdd.Desc1 != null || SalesReceiptItemAdd.Desc2 != null || SalesReceiptItemAdd.Discount != null || SalesReceiptItemAdd.DiscountPercent != null || SalesReceiptItemAdd.DiscountType != null || SalesReceiptItemAdd.ExtendedPrice != null || SalesReceiptItemAdd.Price != null || SalesReceiptItemAdd.Qty != null || SalesReceiptItemAdd.SerialNumber != null || SalesReceiptItemAdd.Size != null || SalesReceiptItemAdd.TaxCode != null || SalesReceiptItemAdd.UnitOfMeasure != null || SalesReceiptItemAdd.UPC != null)
                                SalesReceipt.SalesReceiptItemAdd.Add(SalesReceiptItemAdd);

                        }
                    }
                    else
                    {
                        SalesReceipt = new POSSalesReceiptQBEntry();
                        if (dt.Columns.Contains("Associate"))
                        {
                            #region Validations of Associate
                            if (dr["Associate"].ToString() != string.Empty)
                            {
                                if (dr["Associate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Associate (" + dr["Associate"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.Associate = dr["Associate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.Associate = dr["Associate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Associate = dr["Associate"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.Associate = dr["Associate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Cashier"))
                        {
                            #region Validations of Cashier
                            if (dr["Cashier"].ToString() != string.Empty)
                            {
                                if (dr["Cashier"].ToString().Length > 16)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Cashier (" + dr["Cashier"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.Cashier = dr["Cashier"].ToString().Substring(0, 16);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.Cashier = dr["Cashier"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Cashier = dr["Cashier"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.Cashier = dr["Cashier"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Comments"))
                        {
                            #region Validations of Comments
                            if (dr["Comments"].ToString() != string.Empty)
                            {
                                if (dr["Comments"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Comments (" + dr["Comments"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.Comments = dr["Comments"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.Comments = dr["Comments"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Comments = dr["Comments"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.Comments = dr["Comments"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomerFullName"))
                        {
                            #region Validations of CustomerListID
                            if (dr["CustomerFullName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Customer Full Name (" + dr["CustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.CustomerListID = dr["CustomerFullName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Discount"))
                        {
                            #region Validations for Discount
                            if (dr["Discount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Discount( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.Discount = dr["Discount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.Discount = dr["Discount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Discount = dr["Discount"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Discount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountPercent"))
                        {
                            #region Validations for DiscountPercent
                            if (dr["DiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountPercent( " + dr["DiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.DiscountPercent = dr["DiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("PriceLevelNumber"))
                        {
                            #region Validations of PriceLevelNumber
                            if (dr["PriceLevelNumber"].ToString() != string.Empty)
                            {
                                if (dr["PriceLevelNumber"].ToString().Length > 5)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceLevelNumber(" + dr["PriceLevelNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString().Substring(0, 5);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.PriceLevelNumber = dr["PriceLevelNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PromoCode"))
                        {
                            #region Validations of PromoCode
                            if (dr["PromoCode"].ToString() != string.Empty)
                            {
                                if (dr["PromoCode"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PromoCode (" + dr["PromoCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.PromoCode = dr["PromoCode"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.PromoCode = dr["PromoCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesOrderNumber"))
                        {
                            #region Validations of SalesOrderNumber
                            if (dr["SalesOrderNumber"].ToString() != string.Empty)
                            {
                                if (dr["SalesOrderNumber"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesOrderNumber (" + dr["SalesOrderNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString().Substring(0, 32);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.SalesOrderTxnID = dr["SalesOrderNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptNumber"))
                        {
                            #region Validations of SalesReceiptNumber
                            if (dr["SalesReceiptNumber"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptNumber"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceiptNumber (" + dr["SalesReceiptNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString().Substring(0, 32);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.SalesReceiptNumber = dr["SalesReceiptNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptType"))
                        {
                            #region Validations of SalesReceiptType
                            if (dr["SalesReceiptType"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptType"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Receipt Type (" + dr["SalesReceiptType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString().Substring(0, 10);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.SalesReceiptType = dr["SalesReceiptType"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipDate"))
                        {
                            #region validations of ShipDate
                            if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["ShipDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.ShipDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.ShipDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        SalesReceipt.ShipDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    SalesReceipt.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("StoreNumber"))
                        {
                            #region Validations of StoreNumber
                            if (dr["StoreNumber"].ToString() != string.Empty)
                            {
                                if (dr["StoreNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["StoreNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.StoreNumber = dr["StoreNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.StoreNumber = dr["StoreNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TaxCategory"))
                        {
                            #region Validations of TaxCategory
                            if (dr["TaxCategory"].ToString() != string.Empty)
                            {
                                if (dr["TaxCategory"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["TaxCategory"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.TaxCategory = dr["TaxCategory"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.TaxCategory = dr["TaxCategory"].ToString();
                                }
                            }
                            #endregion
                        }

                        //11.4 451

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesReceipt.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesReceipt.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SalesReceipt.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        SalesReceipt.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    SalesReceipt.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Workstation"))
                        {
                            #region Validations of Workstation
                            if (dr["Workstation"].ToString() != string.Empty)
                            {
                                if (dr["Workstation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Tax Category(" + dr["Workstation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceipt.Workstation = dr["Workstation"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceipt.Workstation = dr["Workstation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceipt.Workstation = dr["Workstation"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceipt.Workstation = dr["Workstation"].ToString();
                                }
                            }
                            #endregion
                        }


                        QBPOSEntities.ShippingInformation ShippingInformationItem = new QBPOSEntities.ShippingInformation();
                        if (dt.Columns.Contains("ShipAddressName"))
                        {
                            #region Validations of ShipAddressName
                            if (dr["ShipAddressName"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddressName"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddressName (" + dr["ShipAddressName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.AddressName = dr["ShipAddressName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipCity"))
                        {
                            #region Validations of ShipCity
                            if (dr["ShipCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCity (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.City = dr["ShipCity"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipCompanyName"))
                        {
                            #region Validations of ShipCompanyName
                            if (dr["ShipCompanyName"].ToString() != string.Empty)
                            {
                                if (dr["ShipCompanyName"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCompanyName (" + dr["ShipCompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.CompanyName = dr["ShipCompanyName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipCountry"))
                        {
                            #region Validations of ShipCountry
                            if (dr["ShipCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipCountry"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipCountry (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Country = dr["ShipCountry"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipFullName"))
                        {
                            #region Validations of ShipFullName
                            if (dr["ShipFullName"].ToString() != string.Empty)
                            {
                                if (dr["ShipFullName"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipFullName (" + dr["ShipFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.FullName = dr["ShipFullName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipPhone"))
                        {
                            #region Validations of ShipPhone
                            if (dr["ShipPhone"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone (" + dr["ShipPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone = dr["ShipPhone"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPhone2"))
                        {
                            #region Validations of ShipPhone2
                            if (dr["ShipPhone2"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone2 (" + dr["ShipPhone2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone2 = dr["ShipPhone2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPhone3"))
                        {
                            #region Validations of ShipPhone3
                            if (dr["ShipPhone3"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone3 (" + dr["ShipPhone3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone3 = dr["ShipPhone3"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipPhone4"))
                        {
                            #region Validations of ShipPhone4
                            if (dr["ShipPhone4"].ToString() != string.Empty)
                            {
                                if (dr["ShipPhone4"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Phone4 (" + dr["ShipPhone4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Phone4 = dr["ShipPhone4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipPostalCode"))
                        {
                            #region Validations of ShipPostalCode
                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipPostalCode"].ToString().Length > 10)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipPostalCode (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 10);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.PostalCode = dr["ShipPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipBy"))
                        {
                            #region Validations of ShipBy
                            if (dr["ShipBy"].ToString() != string.Empty)
                            {
                                if (dr["ShipBy"].ToString().Length > 50)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipBy (" + dr["ShipBy"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString().Substring(0, 50);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.ShipBy = dr["ShipBy"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Shipping"))
                        {
                            #region Validations for Shipping
                            if (dr["Shipping"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Shipping"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Shipping ( " + dr["Shipping"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Shipping = dr["Shipping"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Shipping = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["Shipping"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ShipState"))
                        {
                            #region Validations of ShipState
                            if (dr["ShipState"].ToString() != string.Empty)
                            {
                                if (dr["ShipState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipState (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.State = dr["ShipState"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipStreet"))
                        {
                            #region Validations of ShipStreet
                            if (dr["ShipStreet"].ToString() != string.Empty)
                            {
                                if (dr["ShipStreet"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipStreet (" + dr["ShipStreet"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Street = dr["ShipStreet"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipStreet2"))
                        {
                            #region Validations of ShipStreet2
                            if (dr["ShipStreet2"].ToString() != string.Empty)
                            {
                                if (dr["ShipStreet2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipStreet2 (" + dr["ShipStreet2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShippingInformationItem.Street2 = dr["ShipStreet2"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (ShippingInformationItem.AddressName != null || ShippingInformationItem.City != null || ShippingInformationItem.CompanyName != null || ShippingInformationItem.Country != null || ShippingInformationItem.FullName != null || ShippingInformationItem.Phone != null || ShippingInformationItem.Phone2 != null || ShippingInformationItem.Phone3 != null || ShippingInformationItem.Phone4 != null || ShippingInformationItem.PostalCode != null || ShippingInformationItem.State != null || ShippingInformationItem.Street != null || ShippingInformationItem.Street2 != null)
                            SalesReceipt.ShippingInformation.Add(ShippingInformationItem);

                        QBPOSEntities.SalesReceiptItemAdd SalesReceiptItemAdd = new QBPOSEntities.SalesReceiptItemAdd();


                        if (dt.Columns.Contains("SalesReceiptALU"))
                        {
                            #region Validations of SalesReceiptALU
                            if (dr["SalesReceiptALU"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptALU"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Receipt ALU (" + dr["SalesReceiptALU"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.ALU = dr["SalesReceiptALU"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().ALULookup == true)
                                    {
                                        SalesReceiptItemAdd.Desc1 = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, null, SalesReceiptItemAdd.ALU);
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptAssociate"))
                        {
                            #region Validations of SalesReceiptAssociate
                            if (dr["SalesReceiptAssociate"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptAssociate"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Receipt Associate (" + dr["SalesReceiptAssociate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Associate = dr["SalesReceiptAssociate"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptAttribute"))
                        {
                            #region Validations of SalesReceiptAttribute
                            if (dr["SalesReceiptAttribute"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptAttribute"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Receipt  Attribute (" + dr["SalesReceiptAttribute"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Attribute = dr["SalesReceiptAttribute"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptCommission"))
                        {
                            #region Validations for SalesReceiptCommission
                            if (dr["SalesReceiptCommission"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesReceiptCommission"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Receipt Commission( " + dr["SalesReceiptCommission"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Commission = dr["SalesReceiptCommission"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Commission = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptCommission"].ToString()));
                                }
                            }

                            #endregion
                        }


                        if (dt.Columns.Contains("ItemName"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemName"].ToString() != string.Empty)
                            {
                                if (dr["ItemName"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemName (" + dr["ItemName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Desc1 = dr["ItemName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemInventoryDepartment"))
                        {
                            #region Validations of Desc1
                            if (dr["ItemInventoryDepartment"].ToString() != string.Empty)
                            {
                                if (dr["ItemInventoryDepartment"].ToString().Length > 30)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  ItemInventory Department (" + dr["ItemInventoryDepartment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString().Substring(0, 30);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                    }
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().Department = dr["ItemInventoryDepartment"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptDesc2"))
                        {
                            #region Validations of SalesReceiptDesc2
                            if (dr["SalesReceiptDesc2"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptDesc2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Desc2 (" + dr["SalesReceiptDesc2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Desc2 = dr["SalesReceiptDesc2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptDiscount"))
                        {
                            #region Validations for SalesReceiptDiscount
                            if (dr["SalesReceiptDiscount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesReceiptDiscount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Discount( " + dr["SalesReceiptDiscount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Discount = dr["SalesReceiptDiscount"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Discount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptDiscountPercent"))
                        {
                            #region Validations for SalesReceiptDiscountPercent
                            if (dr["SalesReceiptDiscountPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesReceiptDiscountPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Discount Percent( " + dr["SalesReceiptDiscountPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountPercent = dr["SalesReceiptDiscountPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.DiscountPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptDiscountPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptDiscountType"))
                        {
                            #region Validations of SalesReceiptDiscountType
                            if (dr["SalesReceiptDiscountType"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptDiscountType"].ToString().Length > 32)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Discount Type (" + dr["SalesReceiptDiscountType"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString().Substring(0, 32);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.DiscountType = dr["SalesReceiptDiscountType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesReceiptExtendedPrice"))
                        {
                            #region Validations for SalesReceiptExtendedPrice
                            if (dr["SalesReceiptExtendedPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesReceiptExtendedPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Extended Price( " + dr["SalesReceiptExtendedPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.ExtendedPrice = dr["SalesReceiptExtendedPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.ExtendedPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptExtendedPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptPrice"))
                        {
                            #region Validations for SalesReceiptPrice
                            if (dr["SalesReceiptPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SalesReceiptPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Price( " + dr["SalesReceiptPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Price = dr["SalesReceiptPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Price = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["SalesReceiptPrice"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("SalesReceiptQty"))
                        {
                            #region Validations of SalesReceiptQty
                            if (dr["SalesReceiptQty"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptQty"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Qty (" + dr["SalesReceiptQty"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Qty = dr["SalesReceiptQty"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesReceiptSerialNumber"))
                        {
                            #region Validations of SalesReceiptSerialNumber
                            if (dr["SalesReceiptSerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptSerialNumber"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Serial Number (" + dr["SalesReceiptSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.SerialNumber = dr["SalesReceiptSerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptSize"))
                        {
                            #region Validations of SalesReceiptSize
                            if (dr["SalesReceiptSize"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptSize"].ToString().Length > 12)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Size (" + dr["SalesReceiptSize"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString().Substring(0, 12);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.Size = dr["SalesReceiptSize"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesReceiptTaxCode"))
                        {
                            #region Validations of SalesReceiptTaxCode
                            if (dr["SalesReceiptTaxCode"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptTaxCode"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt TaxCode (" + dr["SalesReceiptTaxCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.TaxCode = dr["SalesReceiptTaxCode"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesReceiptUnitOfMeasure"))
                        {
                            #region Validations of SalesReceiptUnitOfMeasure
                            if (dr["SalesReceiptUnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptUnitOfMeasure"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt Unit Of Measure (" + dr["SalesReceiptUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.UnitOfMeasure = dr["SalesReceiptUnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesReceiptUPC"))
                        {
                            #region Validations of SalesReceiptUPC
                            if (dr["SalesReceiptUPC"].ToString() != string.Empty)
                            {
                                if (dr["SalesReceiptUPC"].ToString().Length > 18)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesReceipt UPC (" + dr["SalesReceiptUPC"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString().Substring(0, 18);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesReceiptItemAdd.UPC = dr["SalesReceiptUPC"].ToString();
                                    //Axis-565
                                    if (CommonUtilities.GetInstance().UPCLookup == true)
                                    {
                                        SalesReceiptItemAdd.ListID = QBCommonUtilities.GetNameByALUOrUPCFromQuickBookPOS(QBFileName, SalesReceiptItemAdd.UPC, null);
                                        if (SalesReceiptItemAdd.ListID != null)
                                        {
                                            SalesReceiptItemAdd.Desc1 = null;
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (SalesReceiptItemAdd.ALU != null || SalesReceiptItemAdd.Associate != null || SalesReceiptItemAdd.Attribute != null || SalesReceiptItemAdd.Commission != null || SalesReceiptItemAdd.Desc1 != null || SalesReceiptItemAdd.Desc2 != null || SalesReceiptItemAdd.Discount != null || SalesReceiptItemAdd.DiscountPercent != null || SalesReceiptItemAdd.DiscountType != null || SalesReceiptItemAdd.ExtendedPrice != null || SalesReceiptItemAdd.Price != null || SalesReceiptItemAdd.Qty != null || SalesReceiptItemAdd.SerialNumber != null || SalesReceiptItemAdd.Size != null || SalesReceiptItemAdd.TaxCode != null || SalesReceiptItemAdd.UnitOfMeasure != null || SalesReceiptItemAdd.UPC != null)
                            SalesReceipt.SalesReceiptItemAdd.Add(SalesReceiptItemAdd);


                        QBPOSEntities.TenderAccountAdd TenderAccountAdd = new QBPOSEntities.TenderAccountAdd();

                        if (dt.Columns.Contains("AccountTenderAmount"))
                        {
                            #region Validations for AccountTenderAmount
                            if (dr["AccountTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["AccountTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Account TenderAmount ( " + dr["AccountTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderAccountAdd.TenderAmount = dr["AccountTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderAccountAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["AccountTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("AccountTipAmount"))
                        {
                            #region Validations for AccountTipAmount
                            if (dr["AccountTipAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["AccountTipAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Account Tip Amount ( " + dr["AccountTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderAccountAdd.TipAmount = dr["AccountTipAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderAccountAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["AccountTipAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderAccountAdd.TenderAmount != null || TenderAccountAdd.TipAmount != null)
                            SalesReceipt.TenderAccountAdd.Add(TenderAccountAdd);

                        QBPOSEntities.TenderCashAdd TenderCashAdd = new QBPOSEntities.TenderCashAdd();

                        if (dt.Columns.Contains("CashTenderAmount"))
                        {
                            #region Validations for CashTenderAmount
                            if (dr["CashTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CashTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Cash TenderAmount ( " + dr["CashTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCashAdd.TenderAmount = dr["CashTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCashAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CashTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderCashAdd.TenderAmount != null)
                            SalesReceipt.TenderCashAdd.Add(TenderCashAdd);

                        QBPOSEntities.TenderCheckAdd TenderCheckAdd = new QBPOSEntities.TenderCheckAdd();
                        if (dt.Columns.Contains("CheckNumber"))
                        {
                            #region Validations of CheckNumber
                            if (dr["CheckNumber"].ToString() != string.Empty)
                            {
                                if (dr["CheckNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Check Number (" + dr["CheckNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCheckAdd.CheckNumber = dr["CheckNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("CheckTenderAmount"))
                        {
                            #region Validations for CheckTenderAmount
                            if (dr["CheckTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CheckTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Check TenderAmount ( " + dr["CheckTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCheckAdd.TenderAmount = dr["CheckTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCheckAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CheckTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderCheckAdd.CheckNumber != null || TenderCheckAdd.TenderAmount != null)
                            SalesReceipt.TenderCheckAdd.Add(TenderCheckAdd);

                        QBPOSEntities.TenderCreditCardAdd TenderCreditCardAdd = new QBPOSEntities.TenderCreditCardAdd();
                        if (dt.Columns.Contains("CreditCardName"))
                        {
                            #region Validations of CreditCardName
                            if (dr["CreditCardName"].ToString() != string.Empty)
                            {
                                if (dr["CreditCardName"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CreditCard Name (" + dr["CreditCardName"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCreditCardAdd.CardName = dr["CreditCardName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("CreditCardTenderAmount"))
                        {
                            #region Validations for CreditCardTenderAmount
                            if (dr["CreditCardTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CreditCardTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CreditCard TenderAmount ( " + dr["CreditCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.TenderAmount = dr["CreditCardTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCreditCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CreditCardTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("CreditCardTipAmount"))
                        {
                            #region Validations for CreditCardTipAmount
                            if (dr["CreditCardTipAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CreditCardTipAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CreditCard Tip Amount ( " + dr["CreditCardTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderCreditCardAdd.TipAmount = dr["CreditCardTipAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderCreditCardAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CreditCardTipAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderCreditCardAdd.CardName != null || TenderCreditCardAdd.TenderAmount != null || TenderCreditCardAdd.TipAmount != null)
                            SalesReceipt.TenderCreditCardAdd.Add(TenderCreditCardAdd);

                        QBPOSEntities.TenderDebitCardAdd TenderDebitCardAdd = new QBPOSEntities.TenderDebitCardAdd();
                        if (dt.Columns.Contains("DebitCardCashback"))
                        {
                            #region Validations for DebitCardCashback
                            if (dr["DebitCardCashback"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DebitCardCashback"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DebitCard CashBack ( " + dr["DebitCardCashback"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDebitCardAdd.Cashback = dr["DebitCardCashback"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderDebitCardAdd.Cashback = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DebitCardCashback"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("DebitCardTenderAmount"))
                        {
                            #region Validations for DebitCardTenderAmount
                            if (dr["DebitCardTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DebitCardTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DebitCard TenderAmount ( " + dr["DebitCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDebitCardAdd.TenderAmount = dr["DebitCardTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderDebitCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DebitCardTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderDebitCardAdd.Cashback != null || TenderDebitCardAdd.TenderAmount != null)
                            SalesReceipt.TenderDebitCardAdd.Add(TenderDebitCardAdd);

                        QBPOSEntities.TenderDepositAdd TenderDepositAdd = new QBPOSEntities.TenderDepositAdd();
                        if (dt.Columns.Contains("DepositTenderAmount"))
                        {
                            #region Validations for DepositTenderAmount
                            if (dr["DepositTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DepositTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Deposit TenderAmount  ( " + dr["DepositTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderDepositAdd.TenderAmount = dr["DepositTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderDepositAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["DepositTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (TenderDepositAdd.TenderAmount != null)
                            SalesReceipt.TenderDepositAdd.Add(TenderDepositAdd);

                        QBPOSEntities.TenderGiftAdd TenderGiftAdd = new QBPOSEntities.TenderGiftAdd();
                        if (dt.Columns.Contains("GiftCertificateNumber"))
                        {
                            #region Validations of GiftCertificateNumber
                            if (dr["GiftCertificateNumber"].ToString() != string.Empty)
                            {
                                if (dr["GiftCertificateNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Gift CertificateNumber (" + dr["GiftCertificateNumber"].ToString() + ") is exceeded maximum length of quickbooks pos .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderGiftAdd.GiftCertificateNumber = dr["GiftCertificateNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("GiftTenderAmount"))
                        {
                            #region Validations for GiftTenderAmount
                            if (dr["GiftTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["GiftTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Gift TenderAmount ( " + dr["GiftTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftAdd.TenderAmount = dr["GiftTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderGiftAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (TenderGiftAdd.GiftCertificateNumber != null || TenderGiftAdd.TenderAmount != null)
                            SalesReceipt.TenderGiftAdd.Add(TenderGiftAdd);

                        QBPOSEntities.TenderGiftCardAdd TenderGiftCardAdd = new QBPOSEntities.TenderGiftCardAdd();
                        if (dt.Columns.Contains("GiftCardTenderAmount"))
                        {
                            #region Validations for GiftCardTenderAmount
                            if (dr["GiftCardTenderAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["GiftCardTenderAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GiftCard TenderAmount ( " + dr["GiftCardTenderAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftCardAdd.TenderAmount = dr["GiftCardTenderAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderGiftCardAdd.TenderAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftCardTenderAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("GiftCardTipAmount"))
                        {
                            #region Validations for GiftCardTipAmount
                            if (dr["GiftCardTipAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["GiftCardTipAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Gift Card Tip Amount ( " + dr["GiftCardTipAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TenderGiftCardAdd.TipAmount = dr["GiftCardTipAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TenderGiftCardAdd.TipAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["GiftCardTipAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (TenderGiftCardAdd.TenderAmount != null || TenderGiftCardAdd.TipAmount != null)
                            SalesReceipt.TenderGiftCardAdd.Add(TenderGiftCardAdd);

                        coll.Add(SalesReceipt);
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #endregion

            return coll;
        }
    }
}
