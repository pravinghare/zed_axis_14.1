﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;

namespace POSDataProcessingImportClass
{
    [XmlRootAttribute("POSTransferSlipQBEntry", Namespace = "", IsNullable = false)]
    public class POSTransferSlipQBEntry
    {
        #region Private Member Variable
        private string m_RefNumber;
        private string m_Associate;
        private string m_Carrier;
        private string m_Comments;
        private string m_Freight;
        private string m_FromStoreNumber;
        private string m_SlipETA;
        private string m_ToStoreNumber;
        private string m_TxnDate;
        private string m_TxnState;
        private string m_Workstation;
        private Collection<TransferSlipItemAdd> m_TransferSlipItemAdd = new Collection<TransferSlipItemAdd>();

        #endregion

        #region Constructor
        public POSTransferSlipQBEntry()
        {
        }
        #endregion

        #region Properties
        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }
        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }
        public string Carrier
        {
            get { return m_Carrier; }
            set { m_Carrier = value; }
        }
        public string Comments
        {
            get { return m_Comments; }
            set { m_Comments = value; }
        }

        public string Freight
        {
            get { return m_Freight; }
            set { m_Freight = value; }
        }

        public string FromStoreNumber
        {
            get { return m_FromStoreNumber; }
            set { m_FromStoreNumber = value; }
        }

        public string SlipETA
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_SlipETA) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_SlipETA);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_SlipETA = value;
            }
        }


        public string ToStoreNumber
        {
            get { return m_ToStoreNumber; }
            set { m_ToStoreNumber = value; }
        }

        public string TxnDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string TxnState
        {
            get { return m_TxnState; }
            set { m_TxnState = value; }
        }

        public string Workstation
        {
            get { return m_Workstation; }
            set { m_Workstation = value; }
        }

        [XmlArray("TransferSlipItemAddREM")]
        public Collection<TransferSlipItemAdd> TransferSlipItemAdd
        {
            get { return m_TransferSlipItemAdd; }
            set { m_TransferSlipItemAdd = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Creating new customer transaction for pos.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<POSDataProcessingImportClass.POSTransferSlipQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement TransferSlipAddRq = inputXMLDoc.CreateElement("TransferSlipAddRq");
            qbXMLMsgsRq.AppendChild(TransferSlipAddRq);
            TransferSlipAddRq.SetAttribute("requestID", "1");
            XmlElement TransferSlipAdd = inputXMLDoc.CreateElement("TransferSlipAdd");
            TransferSlipAddRq.AppendChild(TransferSlipAdd);

            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<ListID/>", string.Empty);

            requestXML = requestXML.Replace("<TransferSlipItemAddREM />", string.Empty);
            requestXML = requestXML.Replace("<TransferSlipItemAddREM>", string.Empty);
            requestXML = requestXML.Replace("</TransferSlipItemAddREM>", string.Empty);

            TransferSlipAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;

            foreach (System.Xml.XmlNode oNode in inputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRq/TransferSlipAddRq/TransferSlipAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    XmlNode node = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/TransferSlipAddRq/TransferSlipAdd/RefNumber");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();
                }
            }

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    string statusSeverity = string.Empty;
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/TransferSlipAddRs"))
                    {

                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            TransactionImporter.TransactionImporter.testFlag = false;

                        }
                    }
                    if (statusSeverity != "Error")
                    {
                        foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/TransferSlipAddRs/TransferSlipRet"))
                        {
                            CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofTransferSlip(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class POSTransferSlipQBEntryCollection : Collection<POSTransferSlipQBEntry>
    {
        public POSTransferSlipQBEntry FindRefNumberEntry(string refNumber)
        {
            foreach (POSTransferSlipQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }

}
