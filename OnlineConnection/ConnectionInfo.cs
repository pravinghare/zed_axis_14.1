﻿using EDI.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace DataProcessingBlocks.OnlineConnection
{
    public partial class ConnectionInfo : Telerik.WinControls.UI.RadForm
    {

        private static QBOConnection _qboConnection;
        private static ConnectionInfo m_ConnectionInfo;
        public ConnectionDetails connectionDetails;

        private static bool isValidDetails;
        private static string accessToken;
        private static string refreshToken;
        private static string realmId;

        public ConnectionInfo()
        {
            InitializeComponent();
            _qboConnection = new QBOConnection();
            connectionDetails = new ConnectionDetails();
        }

        /// <summary>
        /// This method is used to get static instance.
        /// </summary>
        /// <returns></returns>
        public static ConnectionInfo GetInstance(bool isDestroy = false)
        {
            if (isDestroy)
            {
                m_ConnectionInfo = new ConnectionInfo();

            }
            else
            {
                if (m_ConnectionInfo == null)
                    m_ConnectionInfo = new ConnectionInfo();
            }
            return m_ConnectionInfo;
        }

        private void ConnectionInfo_Load(object sender, EventArgs e)
        {
            ResetValues();
        }

        private void buttonHelp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.ConnectToQuickbookHelpURL);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetValues();
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            accessToken = txtAccessToken.Text;
            refreshToken = txtRefreshToken.Text;
            realmId = txtRealmId.Text;
            if (accessToken == null || accessToken == "")
            {
                MessageBox.Show("Please enter Access Token");
                return;
            }
            else if (refreshToken == null || refreshToken == "")
            {
                MessageBox.Show("Please enter Refresh Token");
                return;
            }
            else if (realmId == null || realmId == "")
            {
                MessageBox.Show("Please enter RealmId");
                return;
            }
            connectionDetails.AccessToken = accessToken;
            connectionDetails.RefreshToken = refreshToken;
            connectionDetails.RealmID = realmId;

            var details = await _qboConnection.validateConnectionDetails(connectionDetails);
            if (details == null || details.CompanyName == null)
            {
                MessageBox.Show("Invalid values, Please try again");
                return;
            }
            else
            {
                lblValidationStatus.Text = "Connected to : " + details.CompanyName;
                var saveConnection = await _qboConnection.SaveConnectionDetailsToXml(connectionDetails);
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void ResetValues()
        {
            txtAccessToken.Clear();
            txtRefreshToken.Clear();
            txtRealmId.Clear();
            lblValidationStatus.Text = "";
            txtAccessToken.Focus();
        }
    }
}
