﻿using EDI.Constant;
using Intuit.Ipp.Core;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DataProcessingBlocks.OnlineConnection
{
    class QBOConnection
    {

        public async System.Threading.Tasks.Task<ConnectionDetails> validateConnectionDetails(ConnectionDetails connectionDetails)
        {
            try
            {
                var details = GetDecryptedConnectionDetails(connectionDetails);
                ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext(details);
                ReadOnlyCollection<Intuit.Ipp.Data.CompanyInfo> dataItem = null;
                QueryService<Intuit.Ipp.Data.CompanyInfo> ItemQueryService = new QueryService<Intuit.Ipp.Data.CompanyInfo>(serviceContext);
                dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.CompanyInfo>(ItemQueryService.ExecuteIdsQuery("select * from CompanyInfo"));
                foreach (var company in dataItem)
                {
                    connectionDetails.CompanyName= company.CompanyName;
                    connectionDetails.CountryVersion= company.Country;
                    return connectionDetails;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        //Get Token Details from ipp.xml.

        public ConnectionDetails GetConnectionDetailsFromXml()
        {
            string ippFilePath = GetIPPFilePath();
            ConnectionDetails connectionDetails = new ConnectionDetails();

            XmlDocument xdoc = new XmlDocument();
            if (!File.Exists(ippFilePath))
            {
                return null;
            }
            else
            {
                try
                {
                    xdoc.Load(ippFilePath);

                    XmlNode root = xdoc.DocumentElement;
                    XmlNode ippNode = root.SelectSingleNode("//ConnectionInfo");
                    connectionDetails.AccessToken = ippNode.SelectSingleNode("./AccessToken").InnerText;
                    connectionDetails.RefreshToken = ippNode.SelectSingleNode("./RefreshToken").InnerText;
                    connectionDetails.RealmID = ippNode.SelectSingleNode("./RealmId").InnerText;
                    connectionDetails.CompanyName = ippNode.SelectSingleNode("./CompanyName").InnerText;
                    connectionDetails.CountryVersion = ippNode.SelectSingleNode("./CountryVersion").InnerText;
                    connectionDetails.ExpiryDate = Convert.ToDateTime(ippNode.SelectSingleNode("./ExpiryDate").InnerText);
                    return GetDecryptedConnectionDetails(connectionDetails);
                }
                catch(Exception ex)
                {
                  //  File.Delete(ippFilePath);
                    return null;
                }
            }
        }

        public async System.Threading.Tasks.Task<ConnectionDetails> SaveConnectionDetailsToXml(ConnectionDetails connectionDetails,bool refreshToken=true)
        {
            if (refreshToken)
            {
                connectionDetails = await CommonUtilities.refreshAccessToken(connectionDetails);
            }
            connectionDetails = GetEncryptedConnectionDetails(connectionDetails);
            string ippFilePath = GetIPPFilePath();
            connectionDetails.ExpiryDate = DateTime.Now;
            XmlDocument xdoc = new XmlDocument();
            if (!File.Exists(ippFilePath))
            {
                xdoc = CreateNewConnectionXml(connectionDetails);
                xdoc.Save(ippFilePath);
            }
            else
            {
                try
                {
                    //Update ipp.xml with latest tokens
                    xdoc.Load(ippFilePath);
                    XmlNode root = xdoc.DocumentElement;
                    XmlNode ippNode = root.SelectSingleNode("//ConnectionInfo");
                    ippNode.SelectSingleNode("./AccessToken").InnerText = connectionDetails.AccessToken;
                    ippNode.SelectSingleNode("./RefreshToken").InnerText = connectionDetails.RefreshToken;
                    ippNode.SelectSingleNode("./RealmId").InnerText = connectionDetails.RealmID;
                    ippNode.SelectSingleNode("./ExpiryDate").InnerText = connectionDetails.ExpiryDate.ToString();
                    ippNode.SelectSingleNode("./CompanyName").InnerText = connectionDetails.CompanyName;
                    ippNode.SelectSingleNode("./CountryVersion").InnerText = connectionDetails.CountryVersion;
                    xdoc.Save(ippFilePath);
                }
                catch(Exception ex)
                {
                    // OAuth 1.0 is having encrypted xml, it will give error, so deleted that file and created new ipp.xml file
                    File.Delete(ippFilePath);
                    xdoc= CreateNewConnectionXml(connectionDetails);
                    xdoc.Save(ippFilePath);
                }
            }

            return GetDecryptedConnectionDetails(connectionDetails);
        }

        public XmlDocument CreateNewConnectionXml(ConnectionDetails connectionDetails)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlElement functionsElement = xdoc.CreateElement("ipp");
            xdoc.AppendChild(functionsElement);

            XmlAttribute attribute = functionsElement.OwnerDocument.CreateAttribute("version");
            attribute.Value = Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);
            functionsElement.Attributes.Append(attribute);

            XmlElement functionElement = xdoc.CreateElement("ConnectionInfo");
            functionsElement.AppendChild(functionElement);
            XmlElement AccessToken = xdoc.CreateElement("AccessToken");
            AccessToken.InnerText = connectionDetails.AccessToken;
            functionElement.AppendChild(AccessToken);

            XmlElement RefreshToken = xdoc.CreateElement("RefreshToken");
            RefreshToken.InnerText = connectionDetails.RefreshToken;
            functionElement.AppendChild(RefreshToken);

            XmlElement RealmId = xdoc.CreateElement("RealmId");
            RealmId.InnerText = connectionDetails.RealmID;
            functionElement.AppendChild(RealmId);

            XmlElement ExpiryDate = xdoc.CreateElement("ExpiryDate");
            ExpiryDate.InnerText = connectionDetails.ExpiryDate.ToString();
            functionElement.AppendChild(ExpiryDate);

            XmlElement CompanyName = xdoc.CreateElement("CompanyName");
            CompanyName.InnerText = connectionDetails.CompanyName;
            functionElement.AppendChild(CompanyName);

            XmlElement CountryVersion = xdoc.CreateElement("CountryVersion");
            CountryVersion.InnerText = connectionDetails.CountryVersion;
            functionElement.AppendChild(CountryVersion);

            return xdoc;
        }

           public ConnectionDetails GetDecryptedConnectionDetails(ConnectionDetails connectionDetails)
        {
            Cryptography cryptography = new Cryptography();
            connectionDetails.AccessToken = cryptography.Decrypt(connectionDetails.AccessToken);
            connectionDetails.RefreshToken = cryptography.Decrypt(connectionDetails.RefreshToken);
            connectionDetails.RealmID = cryptography.Decrypt(connectionDetails.RealmID);
            CommonUtilities.GetInstance().QBOConnectionDetails = connectionDetails;

            return connectionDetails;
        }

        public ConnectionDetails GetEncryptedConnectionDetails(ConnectionDetails connectionDetails)
        {
            Cryptography cryptography = new Cryptography();
            connectionDetails.AccessToken = cryptography.Encrypt(connectionDetails.AccessToken);
            connectionDetails.RefreshToken = cryptography.Encrypt(connectionDetails.RefreshToken);
            connectionDetails.RealmID = cryptography.Encrypt(connectionDetails.RealmID);

            return connectionDetails;
        }

        public string GetIPPFilePath()
        {
            string ippFilePath = string.Empty;

            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                        ippFilePath += @"\Ipp.xml";
                    }
                    else
                    {
                        ippFilePath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                        ippFilePath += @"\Ipp.xml";
                    }
                }
                else
                {
                    ippFilePath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                    ippFilePath += @"\Ipp.xml";
                }
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = ippFilePath.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        ippFilePath = ippFilePath.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = ippFilePath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        ippFilePath = ippFilePath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            return ippFilePath;
        }
    }
    public class ConnectionDetails
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string RealmID { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CompanyName { get; set; }
        public string CountryVersion { get; set; }
    }
}
