﻿using EDI.Constant;
using Intuit.Ipp.Core;
using Intuit.Ipp.Security;

namespace DataProcessingBlocks.OnlineConnection
{
    class QuickbookServiceContext
    {
        public ServiceContext GetServiceContext()
        {
            QBOConnection qBOConnection = new QBOConnection();
            ConnectionDetails connectionDetails = new ConnectionDetails();
            if (CommonUtilities.GetInstance().QBOConnectionDetails!=null)
            {
                connectionDetails = CommonUtilities.GetInstance().QBOConnectionDetails;
            }
            else
            {
                connectionDetails = qBOConnection.GetConnectionDetailsFromXml();
            }
            OAuth2RequestValidator oauthRequestValidator = new OAuth2RequestValidator(connectionDetails.AccessToken);
            ServiceContext serviceContext = new ServiceContext(connectionDetails.RealmID, IntuitServicesType.QBO, oauthRequestValidator);
            serviceContext.IppConfiguration.MinorVersion.Qbo = Constants.QBOMinorVersion;
            serviceContext.IppConfiguration.BaseUrl.Qbo = Constants.QBOBaseUrl;
            serviceContext.IppConfiguration.Logger.RequestLog.EnableRequestResponseLogging = true;
            serviceContext.IppConfiguration.Logger.RequestLog.ServiceRequestLoggingLocation = Constants.QBOLogPath;
            return serviceContext;
        }
    }
}
