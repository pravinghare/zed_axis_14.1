// ===============================================================================
// 
// DepositList.cs
//
// This file contains the implementations of the QuickBooks Deposit request methods and
// Properties.
//
// Developed By : 
// Date : 
// Modified By : 
// Date :
// ==============================================================================


using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    public class QBDepositList : BaseDepositList
    {
        #region Public Properties

        public string TxnID
        {
            get
            { return m_TxnID; }
            set
            {
                m_TxnID = value;
            }
        }
        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string EditSequence
        {
            get { return m_editsequence; }
            set { m_editsequence = value; }
        }

        public string TxnDate
        {
            get { return m_txndate; }
            set { m_txndate = value; }
        }

        public string DepositToAccountRefFullName
        {
            get
            { return m_DepositToAccountRefFullName; }
            set
            {
                m_DepositToAccountRefFullName = value;
            }
        }
        public string Memo
        {
            get
            { return m_Memo; }
            set
            {
                m_Memo = value;
            }
        }
        public string DepositTotal
        {
            get
            { return m_DepositTotal; }
            set
            {
                m_DepositTotal = value;
            }
        }
        public string CurrencyRefFullName
        {
            get
            { return m_CurrencyRefFullName; }
            set
            {
                m_CurrencyRefFullName = value;
            }
        }


        public string ExchangeRate
        {
            get
            { return m_ExchangeRate; }
            set
            {
                m_ExchangeRate = value;
            }
        }

        public string DepositTotalInHomeCurrency
        {
            get
            { return m_DepositTotalInHomeCurrency; }
            set
            {
                m_DepositTotalInHomeCurrency = value;
            }
        }

        public string[] TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public string[] DepositeTxnLineID
        {
            get { return m_DepositTxnLineID; }
            set { m_DepositTxnLineID = value; }
        }

        public string[] ItemFullName
        {
            get { return m_QBitemFullName; }
            set { m_QBitemFullName = value; }
        }
        public string[] CashBackMemo
        {
            get { return m_cashbackmemo; }
            set { m_cashbackmemo = value; }
        }

        public decimal?[] Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string[] EntityRefListID
        {
            get
            { return m_EntityRefListID; }
            set
            {
                m_EntityRefListID = value;
            }
        }

        public string[] EntityRefFullName
        {
            get
            { return m_EntityRefFullName; }
            set
            {
                m_EntityRefFullName = value;
            }
        }

        public string[] AccountRefFullName
        {
            get
            {
                return m_AccountRefFullName;
            }
            set
            {
                m_AccountRefFullName = value;
            }
        }
        public decimal?[] CheckNumber
        {
            get
            {
                return m_CheckNumber;
            }
            set
            {
                m_CheckNumber = value;
            }
        }

        public string[] PaymentMethodRefFullName
        {
            get
            { return m_PaymentMethodRefFullName; }
            set
            {
                m_PaymentMethodRefFullName = value;
            }
        }

        public string[] ClassRefFullName
        {
            get
            { return m_ClassRefFullName; }
            set
            {
                m_ClassRefFullName = value;
            }
        }
        public decimal?[] DepositAmount
        {
            get
            {
                return m_depositAmount;
            }
            set
            {
                m_depositAmount = value;
            }
        }

        #endregion

    }

    public class QBDepositListCollection : Collection<QBDepositList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all deposits in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllDepositList(string QBFileName)
        {
            string depositXmlList = QBCommonUtilities.GetListFromQuickBook("DepositQueryRq", QBFileName);

            List<string> DepositList = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(depositXmlList);
            XmlFileData = depositXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList DepositNodeList = outputXMLDoc.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());

            foreach (XmlNode DepositNodes in DepositNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBDepositList deposit = new QBDepositList();
                    foreach (XmlNode node in DepositNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlDepositList.TxnID.ToString()))
                        {
                            if (!DepositList.Contains(node.InnerText))
                                DepositList.Add(node.InnerText);
                        }
                    }
                }
            }

            return DepositList;

        }

        public Collection<QBDepositList> PopulateDepositList(string QBFileName, DateTime FromDate, string ToDate)
        {            
            string depositXmlList = QBCommonUtilities.GetListFromQuickBookByDate("DepositQueryRq", QBFileName, FromDate, ToDate);          
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(depositXmlList);
            XmlFileData = depositXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            int count = 0;
            int count1 = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList DepositNodeList = outputXMLDoc.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());
            foreach (XmlNode DepositNodes in DepositNodeList)
            {
                //Axis 8.0 Reset value of i to 0.
                #region For Deposit data
                if (bkWorker.CancellationPending != true)
                {
                    QBDepositList deposit = new QBDepositList();
                    foreach (XmlNode node in DepositNodes.ChildNodes)
                    {

                        if (node.Name.Equals(QBXmlDepositList.TxnID.ToString()))
                            deposit.TxnID = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TimeCreated.ToString()))
                            deposit.TimeCreated = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TimeModified.ToString()))
                            deposit.TimeModified = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.EditSequence.ToString()))
                            deposit.EditSequence = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TxnDate.ToString()))
                            deposit.TxnDate = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositToAccountRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                    deposit.DepositToAccountRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlDepositList.Memo.ToString()))
                            deposit.Memo = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositTotal.ToString()))
                            deposit.DepositTotal = node.InnerText;



                        if (node.Name.Equals(QBXmlDepositList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                    deposit.CurrencyRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlDepositList.ExchangeRate.ToString()))
                            deposit.ExchangeRate = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositTotalInHomeCurrency.ToString()))
                            deposit.DepositTotalInHomeCurrency = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.CashBackInfoRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking Txn line Id value.
                                if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        deposit.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        deposit.TxnLineID[count] = childNode.InnerText;

                                }

                                if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.ItemFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.ItemFullName[count] = childNodes.InnerText;
                                        }
                                    }
                                }
                                if (childNode.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.CashBackMemo[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.CashBackMemo[count] = childNodes.InnerText;
                                        }

                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                {
                                    try
                                    {
                                        deposit.Amount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }
                                }

                            }
                            count++;
                        }
                        if (node.Name.Equals(QBXmlDepositList.DepositLineRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {

                                //Checking Txn line Id value.

                                if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        deposit.DepositeTxnLineID[count1] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        deposit.DepositeTxnLineID[count1] = childNode.InnerText;

                                }

                                if (childNode.Name.Equals(QBXmlDepositList.EntityRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.EntityRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.EntityRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.AccountRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.AccountRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }

                                if (childNode.Name.Equals(QBXmlDepositList.CheckNumber.ToString()))
                                {
                                    try
                                    {
                                        deposit.CheckNumber[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }
                                }

                                if (childNode.Name.Equals(QBXmlDepositList.PaymentMethodRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.ClassRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.ClassRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.ClassRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                {
                                    try
                                    {
                                        deposit.DepositAmount[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }

                                }

                            }
                            count1++;
                        }
                    }
                    this.Add(deposit);
                }
                else
                { return null; }
                #endregion
            }
            return this;
        }
       
        public List<string> PopulateDepositList(string QBFileName, string TxnId)
        {
            List<string> depositeTxnLineId = new List<string>();
            string depositXmlList = QBCommonUtilities.GetAllListFromQuickBook("DepositQueryRq", QBFileName, TxnId);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(depositXmlList);
            XmlFileData = depositXmlList;
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList DepositNodeList = outputXMLDoc.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());
            QBDepositList deposiList = new QBDepositList();

            foreach (XmlNode DepositNodes in DepositNodeList)
            {
                #region For Deposit data
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in DepositNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlDepositList.DepositLineRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                {
                                    depositeTxnLineId.Add(childNode.InnerText);
                                }
                            }
                        }
                    }
                }
                else
                { return null; }
             
                #endregion
            }
            return depositeTxnLineId;
        }
        

        /// <summary>
        /// If no filter selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBDepositList> PopulateDepositList(string QBFileName)
        {
            string depositXmlList = QBCommonUtilities.GetListFromQuickBook("DepositQueryRq", QBFileName);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(depositXmlList);
            XmlFileData = depositXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            int count = 0;
            int count1 = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList DepositNodeList = outputXMLDoc.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());
            foreach (XmlNode DepositNodes in DepositNodeList)
            {
                //Axis 8.0 Reset value of i to 0.
                i = 0;
                #region For Deposit data
                if (bkWorker.CancellationPending != true)
                {
                    QBDepositList deposit = new QBDepositList();
                    foreach (XmlNode node in DepositNodes.ChildNodes)
                    {

                        if (node.Name.Equals(QBXmlDepositList.TxnID.ToString()))
                            deposit.TxnID = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TimeCreated.ToString()))
                            deposit.TimeCreated = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TimeModified.ToString()))
                            deposit.TimeModified = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.EditSequence.ToString()))
                            deposit.EditSequence = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.TxnDate.ToString()))
                            deposit.TxnDate = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositToAccountRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                    deposit.DepositToAccountRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlDepositList.Memo.ToString()))
                            deposit.Memo = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositTotal.ToString()))
                            deposit.DepositTotal = node.InnerText;



                        if (node.Name.Equals(QBXmlDepositList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                    deposit.CurrencyRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlDepositList.ExchangeRate.ToString()))
                            deposit.ExchangeRate = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.DepositTotalInHomeCurrency.ToString()))
                            deposit.DepositTotalInHomeCurrency = node.InnerText;

                        if (node.Name.Equals(QBXmlDepositList.CashBackInfoRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking Txn line Id value.
                                if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        deposit.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        deposit.TxnLineID[count] = childNode.InnerText;

                                }

                                if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.ItemFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.ItemFullName[count] = childNodes.InnerText;
                                        }
                                    }
                                }
                                if (childNode.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.CashBackMemo[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.CashBackMemo[count] = childNodes.InnerText;
                                        }

                                    }
                                }
                                if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                {
                                    try
                                    {
                                        deposit.Amount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }
                                }

                            }
                            count++;
                        }

                        if (node.Name.Equals(QBXmlDepositList.DepositLineRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {

                                //Checking Txn line Id value.

                                if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        deposit.DepositeTxnLineID[count1] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        deposit.DepositeTxnLineID[count1] = childNode.InnerText;

                                }

                                if (childNode.Name.Equals(QBXmlDepositList.EntityRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.ListID.ToString()))
                                            deposit.EntityRefListID[count1] = childNodes.InnerText;

                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.EntityRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.EntityRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.AccountRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.AccountRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }

                                if (childNode.Name.Equals(QBXmlDepositList.CheckNumber.ToString()))
                                {
                                    try
                                    {
                                        deposit.CheckNumber[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }
                                }

                                if (childNode.Name.Equals(QBXmlDepositList.PaymentMethodRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.ClassRef.ToString()))
                                {
                                    foreach (XmlNode childNodes in childNode.ChildNodes)
                                    {
                                        if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        {
                                            if (childNodes.InnerText.Length > 159)
                                                deposit.ClassRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                            else
                                                deposit.ClassRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                }


                                if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                {
                                    try
                                    {
                                        deposit.DepositAmount[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    catch
                                    { }

                                }

                            }
                            count1++;
                        }

                    }

                    this.Add(deposit);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }

        public Collection<QBDepositList> PopulateDepositList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {

            #region Getting Deposit List from QuickBooks.

            int i = 0;
            int count = 0;
            int count1 = 0;
            string depositXmlList = string.Empty;
            //string depositXmlList = QBCommonUtilities.GetListFromQuickBookByDate("DepositQueryRq", QBFileName, FromDate, ToDate);
            depositXmlList = QBCommonUtilities.GetListFromQuickBook("DepositQueryRq", QBFileName, FromDate, ToDate);

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlDocument outputXMLDoc = new XmlDocument();

           
            //Axis 8.0; Declare integer i

            if (depositXmlList != null)
            {
                outputXMLDoc.LoadXml(depositXmlList);
                XmlFileData = depositXmlList;

                #region Getting invoices Values from XML
                XmlNodeList DepositNodeList = outputXMLDoc.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());

                foreach (XmlNode DepositNodes in DepositNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    #region For Deposit data
                    if (bkWorker.CancellationPending != true)
                    {
                        QBDepositList deposit = new QBDepositList();
                        foreach (XmlNode node in DepositNodes.ChildNodes)
                        {

                            if (node.Name.Equals(QBXmlDepositList.TxnID.ToString()))
                                deposit.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TimeCreated.ToString()))
                                deposit.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TimeModified.ToString()))
                                deposit.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.EditSequence.ToString()))
                                deposit.EditSequence = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TxnDate.ToString()))
                                deposit.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        deposit.DepositToAccountRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                deposit.Memo = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositTotal.ToString()))
                                deposit.DepositTotal = node.InnerText;



                            if (node.Name.Equals(QBXmlDepositList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        deposit.CurrencyRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlDepositList.ExchangeRate.ToString()))
                                deposit.ExchangeRate = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositTotalInHomeCurrency.ToString()))
                                deposit.DepositTotalInHomeCurrency = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.CashBackInfoRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            deposit.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            deposit.TxnLineID[count] = childNode.InnerText;

                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.ItemFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.ItemFullName[count] = childNodes.InnerText;
                                            }
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.CashBackMemo[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.CashBackMemo[count] = childNodes.InnerText;
                                            }
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            deposit.Amount[count] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }

                                }
                                count++;
                            }

                            if (node.Name.Equals(QBXmlDepositList.DepositLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    //Checking Txn line Id value.

                                    if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            deposit.DepositeTxnLineID[count1] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            deposit.DepositeTxnLineID[count1] = childNode.InnerText;

                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.EntityRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.EntityRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.EntityRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.AccountRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.AccountRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.CheckNumber.ToString()))
                                    {
                                        try
                                        {
                                            deposit.CheckNumber[count1] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.PaymentMethodRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.ClassRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.ClassRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            deposit.DepositAmount[count1] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }

                                    }

                                }
                                count1++;
                            }
                        }

                        this.Add(deposit);
                    }
                    else
                    { return null; }
                    #endregion
                }
                outputXMLDoc.RemoveAll();
                #endregion
            }
            return this;
        }


        public Collection<QBDepositList> ModifiedPopulateDepositList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Deposit List from QuickBooks.
            int count = 0;
            int count1 = 0;
            //Getting item list from QuickBooks.
            string DepositXmlList = string.Empty;            
            DepositXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("DepositQueryRq", QBFileName, FromDate, ToDate);
            QBDepositList deposit;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfDeposit = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (DepositXmlList != string.Empty)
            {
                //Loading Deposit List into XML.
                outputXMLDocOfDeposit.LoadXml(DepositXmlList);
                XmlFileData = DepositXmlList;

                #region Getting Deposit Values from XML

                //Getting Deposit values from QuickBooks response.
                XmlNodeList DepositNodeList = outputXMLDocOfDeposit.GetElementsByTagName(QBXmlDepositList.DepositRet.ToString());

                foreach (XmlNode DepositNodes in DepositNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        deposit = new QBDepositList();
                        foreach (XmlNode node in DepositNodes.ChildNodes)
                        {

                            if (node.Name.Equals(QBXmlDepositList.TxnID.ToString()))
                                deposit.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TimeCreated.ToString()))
                                deposit.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TimeModified.ToString()))
                                deposit.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.EditSequence.ToString()))
                                deposit.EditSequence = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.TxnDate.ToString()))
                                deposit.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        deposit.DepositToAccountRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                deposit.Memo = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositTotal.ToString()))
                                deposit.DepositTotal = node.InnerText;



                            if (node.Name.Equals(QBXmlDepositList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                        deposit.CurrencyRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlDepositList.ExchangeRate.ToString()))
                                deposit.ExchangeRate = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.DepositTotalInHomeCurrency.ToString()))
                                deposit.DepositTotalInHomeCurrency = node.InnerText;

                            if (node.Name.Equals(QBXmlDepositList.CashBackInfoRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            deposit.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            deposit.TxnLineID[count] = childNode.InnerText;

                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.ItemFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.ItemFullName[count] = childNodes.InnerText;
                                            }
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.Memo.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.CashBackMemo[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.CashBackMemo[count] = childNodes.InnerText;
                                            }
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            deposit.Amount[count] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }

                                }
                                count++;
                            }

                            if (node.Name.Equals(QBXmlDepositList.DepositLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    //Checking Txn line Id value.

                                    if (childNode.Name.Equals(QBXmlDepositList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            deposit.DepositeTxnLineID[count1] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            deposit.DepositeTxnLineID[count1] = childNode.InnerText;

                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.EntityRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.EntityRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.EntityRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.AccountRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.AccountRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.CheckNumber.ToString()))
                                    {
                                        try
                                        {
                                            deposit.CheckNumber[count1] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }

                                    if (childNode.Name.Equals(QBXmlDepositList.PaymentMethodRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.PaymentMethodRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlDepositList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    deposit.ClassRefFullName[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    deposit.ClassRefFullName[count1] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    if (childNode.Name.Equals(QBXmlDepositList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            deposit.DepositAmount[count1] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }

                                    }

                                }
                                count1++;
                            }
                        }

                        this.Add(deposit);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfDeposit.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        #endregion        
    }
}
