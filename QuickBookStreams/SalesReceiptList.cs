using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This Class Provides Properties and Method for SalesReceipt.
    /// </summary>
    public class SalesReceiptList: BaseSalesReceiptList
    {
        #region Public Properties

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }      

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string CustomerListID
        {
            get { return m_CustomerListID; }
            set { m_CustomerListID = value; }
        }

        public string CustomerRefFullName
        {
            get { return m_CustomerRefFullName;}
            set { m_CustomerRefFullName = value; }
        }       

        public string ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }
       
        public string TemplateRefFullName
        {
            get { return m_TemplateRefFullName; }
            set { m_TemplateRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Addr1
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        public string Addr2
        {
            get { return m_BillAddress1; }
            set { m_BillAddress1 = value; }
        }
        public string Addr3
        {
            get { return m_BillAddress2; }
            set { m_BillAddress2 = value; }
        }
        public string Addr4
        {
            get { return m_BillAddress3; }
            set { m_BillAddress3 = value; }
        }
        public string Addr5
        {
            get { return m_BillAddress4; }
            set { m_BillAddress4 = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        public string ShipAddr1
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        public string ShipAddr2
        {
            get { return m_ShipAddress1; }
            set { m_ShipAddress1 = value; }
        }

        public string ShipCity
        {
            get { return m_ShipAddress2; }
            set { m_ShipAddress2 = value; }
        }

        public string ShipState
        {
            get { return m_ShipAddress3; }
            set { m_ShipAddress3 = value; }
        }

        public string ShipPostalCode
        {
            get { return m_ShipAddress4; }
            set { m_ShipAddress4 = value; }
        }

        public string ShipCountry
        {
            get { return m_ShipCountry; }
            set { m_ShipCountry = value; }
        }

        public string IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }

        public string CheckNumber
        {
            get { return m_CheckNumber; }
            set { m_CheckNumber = value; }
        }

        public string PaymentMethodRefFullName
        {
            get { return m_PaymentMethodRefFullName; }
            set { m_PaymentMethodRefFullName = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }            

        public string SalesRepRefFullName
        {
            get { return m_SalesRepRefFullName; }
            set { m_SalesRepRefFullName = value; }
        }

        public string ShipDate
        {
            get { return m_Shipdate; }
            set { m_Shipdate = value; }
        }
       
        public string ShipMethodRefFullName
        {
            get { return m_ShipMethodRefFullName; }
            set { m_ShipMethodRefFullName = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        public string ItemSalesTaxFullName
        {
            get { return m_ItemSalesTaxFullName; }
            set { m_ItemSalesTaxFullName = value; }
        }

        public string SalesTaxPercentage
        {
            get { return m_SalesTaxPercentage; }
            set { m_SalesTaxPercentage = value; }
        }      

        public decimal? TotalAmount
        {
            get { return m_MainTotalAmount; }
            set { m_MainTotalAmount = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string CustomerMsgFullName
        {
            get { return m_CutomerMsgFullName; }
            set { m_CutomerMsgFullName = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string CustomerSalesTaxFullName
        {
            get { return m_CustomerSalesTaxFullName; }
            set { m_CustomerSalesTaxFullName = value; }
        }
       
        public string DepositToAccountFullName
        {
            get { return m_DepositToAccountFullName; }
            set { m_DepositToAccountFullName = value; }
        }

        public string Other
        {
            get { return m_Other; }
            set { m_Other = value; }
        }



        //for SalesReceiptLineRet
        
        public List<string> TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }
       
        public List<string> ItemFullName
        {
            get { return m_ItemFullName; }
            set { m_ItemFullName = value; }
        }

        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }      

        public List<string> OverrideUOMFullName
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }

        public List<decimal?> Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }

        public List<string> LineClassRefFullName
        {
            get { return m_LineClassRefFullName; }
            set { m_LineClassRefFullName = value; }
        }

        public List<decimal?> Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public List<string> InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }

        public List<string> ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }
      
        public List<string> SalesTaxCodeFullName
        {
            get { return m_SalesTaxCodeFullName; }
            set { m_SalesTaxCodeFullName = value; }
        }

        public List<string> Other1
        {
            get { return m_Other1; }
            set { m_Other1 = value; }
        }

        public List<string> Other2
        {
            get { return m_Other2; }
            set { m_Other2 = value; }
        }
        // axis 10.0 changes
        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value;}
        }
        // axis 10.0 changes ends

        //For SalesReceiptLineGroupRet

        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }
       
        public List<string> ItemGroupFullName
        {
            get { return m_ItemGroupFullName; }
            set { m_ItemGroupFullName = value; }
        }

        public List<string> GroupDesc
        {
            get { return m_GroupDesc; }
            set { m_GroupDesc = value; }
        }

        public List<decimal?> GroupQuantity
        {
            get { return m_GroupQuantity; }
            set { m_GroupQuantity = value; }
        }

        public List<string> IsPrintItemsInGroup
        {
            get { return m_IsPrintItemsInGroup; }
            set { m_IsPrintItemsInGroup = value; }
        }

        //For SalesReceiptGroupLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }
        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<string> GroupLineOverrideUOM
        {
            get { return m_GroupLineOverrideUOM; }
            set { m_GroupLineOverrideUOM = value; }
        }
        public List<decimal?> GroupLineRate
        {
            get { return m_GroupLineRate; }
            set { m_GroupLineRate = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineServiceDate
        {
            get { return m_GroupLineServiceDate; }
            set { m_GroupLineServiceDate = value; }
        }

        public List<string> GroupLineSalesTaxCodeFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }

        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();

        #endregion
    }

    /// <summary>
    /// This Collection class is used for getting SalesReceipt List from QuickBook.
    /// </summary>
    public class QBSalesReceiptListCollection :  Collection<SalesReceiptList>
    {
        string XmlFileData = string.Empty;
        //Axis 706
        QBItemDetails itemList = null;
        int i = 0;

        public void GetSalesReceiptValuesFromXML(string SalesReceiptXmlList)
        {
                  TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            SalesReceiptList SalesReceiptList;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfSalesReceipt = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (SalesReceiptXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfSalesReceipt.LoadXml(SalesReceiptXmlList);
                XmlFileData = SalesReceiptXmlList;

                #region Getting salesReceipt Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList SalesReceiptNodeList = outputXMLDocOfSalesReceipt.GetElementsByTagName(QBSalesReceiptList.SalesReceiptRet.ToString());

                foreach (XmlNode SalesReceiptNodes in SalesReceiptNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {

                        int count = 0;
                        int count1 = 0;
                        SalesReceiptList = new SalesReceiptList();
                       
                        foreach (XmlNode node in SalesReceiptNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBSalesReceiptList.TxnID.ToString()))
                            {
                                SalesReceiptList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(QBSalesReceiptList.TxnNumber.ToString()))
                            {
                                SalesReceiptList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBSalesReceiptList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.ListID.ToString()))
                                        SalesReceiptList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.CustomerRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBSalesReceiptList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.ClassRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBSalesReceiptList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.TemplateRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBSalesReceiptList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        SalesReceiptList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        SalesReceiptList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    SalesReceiptList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber.
                            if (node.Name.Equals(QBSalesReceiptList.RefNumber.ToString()))
                            {
                                SalesReceiptList.RefNumber = node.InnerText;
                            }

                            //Checking BillAddress.
                            if (node.Name.Equals(QBSalesReceiptList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr1.ToString()))
                                        SalesReceiptList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr2.ToString()))
                                        SalesReceiptList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr3.ToString()))
                                        SalesReceiptList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr4.ToString()))
                                        SalesReceiptList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr5.ToString()))
                                        SalesReceiptList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.City.ToString()))
                                        SalesReceiptList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.State.ToString()))
                                        SalesReceiptList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.PostalCode.ToString()))
                                        SalesReceiptList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Country.ToString()))
                                        SalesReceiptList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress.
                            if (node.Name.Equals(QBSalesReceiptList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr1.ToString()))
                                        SalesReceiptList.ShipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Addr2.ToString()))
                                        SalesReceiptList.ShipAddr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.City.ToString()))
                                        SalesReceiptList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.State.ToString()))
                                        SalesReceiptList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.PostalCode.ToString()))
                                        SalesReceiptList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBSalesReceiptList.Country.ToString()))
                                        SalesReceiptList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBSalesReceiptList.IsPending.ToString()))
                            {
                                SalesReceiptList.IsPending = node.InnerText;
                            }

                            //Checking checkNumber.
                            if (node.Name.Equals(QBSalesReceiptList.CheckNumber.ToString()))
                            {
                                SalesReceiptList.CheckNumber = node.InnerText;
                            }

                            //Checking PaymentMethodRef.
                            if (node.Name.Equals(QBSalesReceiptList.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.PaymentMethodRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking DueDate.
                            if (node.Name.Equals(QBSalesReceiptList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        SalesReceiptList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        SalesReceiptList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    SalesReceiptList.DueDate = node.InnerText;
                                }
                            }
                            //Checking SalesRepRefFullName.
                            if (node.Name.Equals(QBSalesReceiptList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.SalesRepRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ShipDate.
                            if (node.Name.Equals(QBSalesReceiptList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        SalesReceiptList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        SalesReceiptList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    SalesReceiptList.ShipDate = node.InnerText;
                                }
                            }
                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBSalesReceiptList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.ShipMethodRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking FOB
                            if (node.Name.Equals(QBSalesReceiptList.FOB.ToString()))
                            {
                                SalesReceiptList.FOB = node.InnerText;
                            }

                            //Checking ItemSalesTaxRefFullName.
                            if (node.Name.Equals(QBSalesReceiptList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }
                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBSalesReceiptList.SalesTaxPercentage.ToString()))
                            {
                                SalesReceiptList.SalesTaxPercentage = node.InnerText;
                            }

                            //checking Total Amount
                            if (node.Name.Equals(QBSalesReceiptList.TotalAmount.ToString()))
                            {
                                SalesReceiptList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBSalesReceiptList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo
                            if (node.Name.Equals(QBSalesReceiptList.Memo.ToString()))
                            {
                                SalesReceiptList.Memo = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBSalesReceiptList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBSalesReceiptList.IsToBePrinted.ToString()))
                            {
                                SalesReceiptList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBSalesReceiptList.IsToBeEmailed.ToString()))
                            {
                                SalesReceiptList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBSalesReceiptList.IsTaxIncluded.ToString()))
                            {
                                SalesReceiptList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBSalesReceiptList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.CustomerSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking DepositToAccountRefFullName.
                            if (node.Name.Equals(QBSalesReceiptList.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        SalesReceiptList.DepositToAccountFullName = childNode.InnerText;
                                }
                            }

                            //Checking Other
                            if (node.Name.Equals(QBSalesReceiptList.Other.ToString()))
                            {
                                SalesReceiptList.Other = node.InnerText;
                            }

                            //Checking Sales Receipt Line ret values.
                            if (node.Name.Equals(QBSalesReceiptList.SalesReceiptLineRet.ToString()))
                            {
                                checkNullEntries(node, ref SalesReceiptList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBSalesReceiptList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            SalesReceiptList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            SalesReceiptList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBSalesReceiptList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                            {
                                                if (childsNode.InnerText.Length > 209)
                                                    SalesReceiptList.ItemFullName.Add(childsNode.InnerText.Remove(209, (childsNode.InnerText.Length - 209)).Trim());
                                                else
                                                    SalesReceiptList.ItemFullName.Add(childsNode.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Desc
                                    if (childNode.Name.Equals(QBSalesReceiptList.Desc.ToString()))
                                    {
                                        SalesReceiptList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBSalesReceiptList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            SalesReceiptList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBSalesReceiptList.UnitOfMeasure.ToString()))
                                    {
                                        SalesReceiptList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBSalesReceiptList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                SalesReceiptList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBSalesReceiptList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            SalesReceiptList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking LineClassRefFullName
                                    if (childNode.Name.Equals(QBSalesReceiptList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                SalesReceiptList.LineClassRefFullName.Add(childsNode.InnerText);
                                        }
                                    }


                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBSalesReceiptList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            SalesReceiptList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking InventorySiteRefFullName.
                                    if (childNode.Name.Equals(QBSalesReceiptList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                SalesReceiptList.InventorySiteRefFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    // axis 10.0 changes 
                                    if (childNode.Name.Equals(QBSalesReceiptList.SerialNumber.ToString()))
                                    {
                                        SalesReceiptList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBSalesReceiptList.LotNumber.ToString()))
                                    {
                                        SalesReceiptList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 chnages ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBSalesReceiptList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                SalesReceiptList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                SalesReceiptList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            SalesReceiptList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxCodeRefFullName.
                                    if (childNode.Name.Equals(QBSalesReceiptList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                SalesReceiptList.SalesTaxCodeFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBSalesReceiptList.Other1.ToString()))
                                    {
                                        SalesReceiptList.Other1.Add(childNode.InnerText);
                                    }
                                    //Checking Other2
                                    if (childNode.Name.Equals(QBSalesReceiptList.Other2.ToString()))
                                    {
                                        SalesReceiptList.Other2.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBSalesReceiptList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            SalesReceiptList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + SalesReceiptList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                SalesReceiptList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + SalesReceiptList.TxnLineID[count]);
                                        }

                                        i++;
                                    }
                                }
                                count++;
                            }

                            //Checking SalesReceiptGroupLineRet
                            if (node.Name.Equals(QBSalesReceiptList.SalesReceiptLineGroupRet.ToString()))
                            {
                                int count2 = 0;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBSalesReceiptList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            SalesReceiptList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            SalesReceiptList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBSalesReceiptList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                SalesReceiptList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBSalesReceiptList.Desc.ToString()))
                                    {
                                        SalesReceiptList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBSalesReceiptList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            SalesReceiptList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBSalesReceiptList.IsPrintItemsInGroup.ToString()))
                                    {
                                        SalesReceiptList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }
                                    //Axis bug
                                    if (childNode.Name.Equals(QBSalesReceiptList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            SalesReceiptList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + SalesReceiptList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                SalesReceiptList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + SalesReceiptList.TxnLineID[count]);
                                        }

                                        i++;
                                    }
                                    //Checking  sub SalesReceiptLineRet
                                    if (childNode.Name.Equals(QBSalesReceiptList.SalesReceiptLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref SalesReceiptList);
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    SalesReceiptList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    SalesReceiptList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                    {
                                                        if (subChildNodes.InnerText.Length > 209)
                                                            SalesReceiptList.GroupLineItemFullName.Add(subChildNodes.InnerText.Remove(209, (subChildNodes.InnerText.Length - 209)).Trim());
                                                        else
                                                            SalesReceiptList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                    }
                                                }
                                            }
                                            //Checking Desc
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Desc.ToString()))
                                            {
                                                SalesReceiptList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }

                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    SalesReceiptList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.UnitOfMeasure.ToString()))
                                            {
                                                SalesReceiptList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                        SalesReceiptList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    SalesReceiptList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    SalesReceiptList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        SalesReceiptList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        SalesReceiptList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    SalesReceiptList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }
                                            //Checking SalesTaxCodeRefFullName.
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                                        SalesReceiptList.GroupLineSalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Other1.ToString()))
                                            {
                                                SalesReceiptList.Other1.Add(subChildNode.InnerText);
                                            }
                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.Other2.ToString()))
                                            {
                                                SalesReceiptList.Other2.Add(subChildNode.InnerText);
                                            }
                                            // axis 10.0 changes 
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.SerialNumber.ToString()))
                                            {
                                                SalesReceiptList.SerialNumber.Add(childNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.LotNumber.ToString()))
                                            {
                                                SalesReceiptList.LotNumber.Add(childNode.InnerText);
                                            }
                                            //Axis bug
                                            if (subChildNode.Name.Equals(QBSalesReceiptList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    SalesReceiptList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        SalesReceiptList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                }

                                                i++;
                                            }
                                            // axis 10.0 chnages ends

                                        }
                                        count2++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBSalesReceiptList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    SalesReceiptList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        SalesReceiptList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding SalesReceipt list.
                        this.Add(SalesReceiptList);
                    }
                    else
                    { }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfSalesReceipt.RemoveAll();
                #endregion
            }
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref SalesReceiptList salesReceiptList)
        {
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.TxnLineID.ToString()) == null)
            {
                salesReceiptList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.ItemRef.ToString()) == null)
            {
                salesReceiptList.GroupLineItemFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Desc.ToString()) == null)
            {
                salesReceiptList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Quantity.ToString()) == null)
            {
                salesReceiptList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.UnitOfMeasure.ToString()) == null)
            {
                salesReceiptList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.OverrideUOMSetRef.ToString()) == null)
            {
                salesReceiptList.GroupLineOverrideUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Rate.ToString()) == null)
            {
                salesReceiptList.GroupLineRate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Amount.ToString()) == null)
            {
                salesReceiptList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.ServiceDate.ToString()) == null)
            {
                salesReceiptList.GroupLineServiceDate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.SalesTaxCodeRef.ToString()) == null)
            {
                salesReceiptList.GroupLineSalesTaxCodeFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Other1.ToString()) == null)
            {
                salesReceiptList.Other1.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.Other2.ToString()) == null)
            {
                salesReceiptList.Other2.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.SerialNumber.ToString()) == null)
            {
                salesReceiptList.SerialNumber.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.LotNumber.ToString()) == null)
            {
                salesReceiptList.LotNumber.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBSalesReceiptList.DataExtRet.ToString()) == null)
            {
                salesReceiptList.DataExtName.Add(null);
                salesReceiptList.DataExtValue.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref SalesReceiptList salesReceiptList)
        {
            if (node.SelectSingleNode("./" + QBSalesReceiptList.TxnLineID.ToString()) == null)
            {
                salesReceiptList.TxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.ItemRef.ToString()) == null)
            {
                salesReceiptList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Desc.ToString()) == null)
            {
                salesReceiptList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Quantity.ToString()) == null)
            {
                salesReceiptList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.UnitOfMeasure.ToString()) == null)
            {
                salesReceiptList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.OverrideUOMSetRef.ToString()) == null)
            {
                salesReceiptList.OverrideUOMFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Rate.ToString()) == null)
            {
                salesReceiptList.Rate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.ClassRef.ToString()) == null)
            {
                salesReceiptList.LineClassRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Amount.ToString()) == null)
            {
                salesReceiptList.Amount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.InventorySiteRef.ToString()) == null)
            {
                salesReceiptList.InventorySiteRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.SerialNumber.ToString()) == null)
            {
                salesReceiptList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.LotNumber.ToString()) == null)
            {
                salesReceiptList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.ServiceDate.ToString()) == null)
            {
                salesReceiptList.ServiceDate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.SalesTaxCodeRef.ToString()) == null)
            {
                salesReceiptList.SalesTaxCodeFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Other1.ToString()) == null)
            {
                salesReceiptList.Other1.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.Other2.ToString()) == null)
            {
                salesReceiptList.Other2.Add(null);
            }
            if (node.SelectSingleNode("./" + QBSalesReceiptList.DataExtRet.ToString()) == null)
            {
                salesReceiptList.DataExtName.Add(null);
                salesReceiptList.DataExtValue.Add(null);
            }
        }


        /// <summary>
        /// This Method is used for getting SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName ,DateTime FromDate, DateTime ToDate)
        {
            #region Getting SalesReceipt List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            //SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            #endregion

          GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="Entity Filter">Entity Filter</param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting SalesReceipt List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;            
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            #endregion
             GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="EntityFilter"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting SalesReceipt List from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            //SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            #endregion

            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="EntityFilter"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting SalesReceipt List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            //SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate,entityFilter);
             #endregion
           GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting SalesReceipt List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            //SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting SalesReceipt List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            //SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate);
            #endregion
            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get SalesReceipt List from QuickBook for Filters
        /// EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("SalesReceiptQueryRq",QBFileName, entityFilter);
            #endregion

            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get SalesReceipt List from QuickBook for Filters
        /// RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromRefnum, ToRefnum,entityFilter);
            #endregion
             GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get SalesReceipt List from QuickBook for Filters
        /// RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName, string FromRefnum, string ToRefnum)
         {
            #region Getting Invoices List from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromRefnum, ToRefnum);
            #endregion
            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used to get SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilter and RefNumberRangeFilter,entityFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="EntityFilter"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);
            #endregion
             GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName);
            #endregion
            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used to get SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> PopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            #endregion
             GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used to get SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilter and RefNumberRangeFilter,Entity Filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);
            #endregion
             GetSalesReceiptValuesFromXML(SalesReceiptXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used to get SalesReceiptList from QuickBook for Filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<SalesReceiptList> ModifiedPopulateSalesReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            #endregion
            GetSalesReceiptValuesFromXML(SalesReceiptXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        //Axis 706
        public List<string> PopulateSalesReceiptTxnIdAndGroupTxnIdInSequence(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting SalesReceipt List from QuickBooks.

            //Getting item list from QuickBooks.
            List<string> SalesReceiptTxnIdAndGroupTxnId = new List<string>();
            string SalesReceiptXmlList = string.Empty;
            SalesReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("SalesReceiptQueryRq", QBFileName, FromRefnum, ToRefnum);

            SalesReceiptList SalesReceiptList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfSalesReceipt = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (SalesReceiptXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfSalesReceipt.LoadXml(SalesReceiptXmlList);
                XmlFileData = SalesReceiptXmlList;

                #region Getting SalesReceipt Values from XML

                //Getting SalesReceipt values from QuickBooks response.
                XmlNodeList SalesReceiptNodeList = outputXMLDocOfSalesReceipt.GetElementsByTagName(QBSalesReceiptList.SalesReceiptRet.ToString());

                foreach (XmlNode SalesReceiptNodes in SalesReceiptNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        SalesReceiptList = new SalesReceiptList();
                        foreach (XmlNode node in SalesReceiptNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBSalesReceiptList.SalesReceiptLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.TxnLineID.ToString()))
                                    {
                                        SalesReceiptList.TxnID = childNode.InnerText;
                                        SalesReceiptTxnIdAndGroupTxnId.Add(SalesReceiptList.TxnID);
                                    }
                                }
                            }



                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBSalesReceiptList.SalesReceiptLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBSalesReceiptList.ItemGroupRef.ToString()) == null)
                                {
                                    SalesReceiptList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBSalesReceiptList.Desc.ToString()) == null)
                                {
                                    SalesReceiptList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBSalesReceiptList.Quantity.ToString()) == null)
                                {
                                    SalesReceiptList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBSalesReceiptList.UnitOfMeasure.ToString()) == null)
                                {
                                    SalesReceiptList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBSalesReceiptList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    SalesReceiptList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBSalesReceiptList.TxnLineID.ToString()))
                                    {
                                        string GroupId = "";
                                        if (childNode.InnerText.Length > 36)
                                            GroupId = (childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            GroupId = childNode.InnerText;

                                        SalesReceiptTxnIdAndGroupTxnId.Add("GroupTxnLineID_" + GroupId);

                                    }
                                }
                            }
                            //

                        }
                    }



                }
                //Adding Invoice list.
                #endregion
            }
            return SalesReceiptTxnIdAndGroupTxnId;
        }

        //Axis 706 ends


    }
}
