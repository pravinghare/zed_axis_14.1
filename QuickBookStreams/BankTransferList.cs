﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using System.Collections;

namespace Streams
{
    public class BankTransferList : BaseBankTransferList
    {
        #region Public Properties
        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string EditSequence
        {
            get { return m_EditSequence; }
            set { m_EditSequence = value; }
        }


        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string[] TransferFromAccountRef
        {
            get { return m_TransferFromAccountRef; }
            set { m_TransferFromAccountRef = value; }
        }


        public decimal?[] FromAccountBalance
        {
            get { return m_FromAccountBalance; }
            set { m_FromAccountBalance = value; }
        }

        public string[] TransferToAccountRef
        {
            get { return m_TransferToAccountRef; }
            set { m_TransferToAccountRef = value; }
        }

        public decimal?[] ToAccountBalance
        {
            get { return m_ToAccountBalance; }
            set { m_ToAccountBalance = value; }
        }

        public string[] ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public decimal?[] Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }



        #endregion

    }

    public class QBBankTransferCollection : Collection<BankTransferList>
    {
        string XmlFileData = string.Empty;

        public List<string> GetAllTransferList(string QBFileName)
        {
            #region Getting transfer Entry List from QuickBooks.

            string TransferEntryXmlList = string.Empty;
            TransferEntryXmlList = QBCommonUtilities.GetListFromQuickBook("TransferQueryRq", QBFileName);
            List<string> BankTransferList = new List<string>();
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(TransferEntryXmlList);
            XmlFileData = TransferEntryXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList TransferNodeList = outputXMLDoc.GetElementsByTagName(QBXmlBankTransfers.TransferRet.ToString());

            foreach (XmlNode TransferNodes in TransferNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in TransferNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlBankTransfers.TxnID.ToString()))
                        {
                            if (!BankTransferList.Contains(node.InnerText))
                                BankTransferList.Add(node.InnerText);
                        }
                    }
                }
            }

            return BankTransferList;
            #endregion
        }


        public Collection<BankTransferList> populateTransferEntryList(string QBFileName, DateTime fromDate, string toDate)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TransferEntryXmlList = string.Empty;
            TransferEntryXmlList = QBCommonUtilities.GetListFromQuickBookByDate("TransferQueryRq", QBFileName, fromDate, toDate);

            BankTransferList BankTransferList;
            #endregion

            XmlDocument outputXMLDocOfTransferEntry = new XmlDocument();

            outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
            XmlFileData = TransferEntryXmlList;
            int count = 0;
            int count1 = 0;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.

            //Getting current version of System. BUG(676)


            if (TransferEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
                XmlFileData = TransferEntryXmlList;

                #region Getting Transfer EntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.


                XmlNodeList TransferNodeList = outputXMLDocOfTransferEntry.GetElementsByTagName(QBXmlBankTransfers.TransferRet.ToString());
                foreach (XmlNode TransferEntriesNodes in TransferNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        BankTransferList = new BankTransferList();
                        foreach (XmlNode node in TransferEntriesNodes.ChildNodes)
                        {
                            //checking txnId
                            if (node.Name.Equals(QBXmlBankTransfers.TxnID.ToString()))
                            {
                                BankTransferList.TxnID = node.InnerText;
                            }
                            //checking TimeCreated
                            if (node.Name.Equals(QBXmlBankTransfers.TimeCreated.ToString()))
                                BankTransferList.TimeCreated = node.InnerText;

                            //checking TimeModified
                            if (node.Name.Equals(QBXmlBankTransfers.TimeModified.ToString()))
                                BankTransferList.TimeModified = node.InnerText;

                            //checking EditSequence
                            if (node.Name.Equals(QBXmlBankTransfers.EditSequence.ToString()))
                                BankTransferList.EditSequence = node.InnerText;

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBXmlBankTransfers.TxnNumber.ToString()))
                            {
                                BankTransferList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBXmlBankTransfers.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);

                                    BankTransferList.TxnDate = dttxn.ToString("MM/dd/yyyy");

                                }
                                catch
                                {
                                    BankTransferList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking TransferFromFullName
                            if (node.Name.Equals(QBXmlBankTransfers.TransferFromAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking FromAccountBalance
                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.FromAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }
                            //checking TransferToAccountRef


                            if (node.Name.Equals(QBXmlBankTransfers.TransferToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking ToAccountBalance

                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.ToAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }

                            //Checking ClassRef.

                            if (node.Name.Equals(QBXmlBankTransfers.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.Amount[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }


                            //Checking Memo 
                            if (node.Name.Equals(QBXmlBankTransfers.Memo.ToString()))
                            {
                                BankTransferList.Memo = node.InnerText;
                            }

                        }
                        //Adding TimeEntry list.
                        this.Add(BankTransferList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTransferEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }
    
        public Collection<BankTransferList> populateTransferEntryList(string QBFileName)
        {
            #region Getting Transfer Entry List from QuickBooks.

            string TransferEntryXmlList = string.Empty;
            TransferEntryXmlList = QBCommonUtilities.GetListFromQuickBook("TransferQueryRq", QBFileName);

            BankTransferList BankTransferList;
            #endregion


            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTransferEntry = new XmlDocument();

            outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
            XmlFileData = TransferEntryXmlList;
            int count = 0;
            int count1 = 0;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.

            //Getting current version of System. BUG(676)


            if (TransferEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
                XmlFileData = TransferEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.


                XmlNodeList TransferNodeList = outputXMLDocOfTransferEntry.GetElementsByTagName(QBXmlBankTransfers.TransferRet.ToString());
                foreach (XmlNode TransferEntriesNodes in TransferNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        BankTransferList = new BankTransferList();
                        foreach (XmlNode node in TransferEntriesNodes.ChildNodes)
                        {
                            //checking txnId
                            if (node.Name.Equals(QBXmlBankTransfers.TxnID.ToString()))
                            {
                                BankTransferList.TxnID = node.InnerText;
                            }
                            //checking TimeCreated
                            if (node.Name.Equals(QBXmlBankTransfers.TimeCreated.ToString()))
                                BankTransferList.TimeCreated = node.InnerText;

                            //checking TimeModified
                            if (node.Name.Equals(QBXmlBankTransfers.TimeModified.ToString()))
                                BankTransferList.TimeModified = node.InnerText;

                            //checking EditSequence
                            if (node.Name.Equals(QBXmlBankTransfers.EditSequence.ToString()))
                                BankTransferList.EditSequence = node.InnerText;

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBXmlBankTransfers.TxnNumber.ToString()))
                            {
                                BankTransferList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBXmlBankTransfers.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);

                                    BankTransferList.TxnDate = dttxn.ToString("yyyy/MM/dd");

                                }
                                catch
                                {
                                    BankTransferList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking TransferFromFullName
                            if (node.Name.Equals(QBXmlBankTransfers.TransferFromAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking FromAccountBalance
                            if (node.Name.Equals(QBXmlBankTransfers.FromAccountBalance.ToString()))
                            {
                                try
                                {
                                    BankTransferList.FromAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }
                            //checking TransferToAccountRef


                            if (node.Name.Equals(QBXmlBankTransfers.TransferToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking ToAccountBalance

                            if (node.Name.Equals(QBXmlBankTransfers.ToAccountBalance.ToString()))
                            {
                                try
                                {
                                    BankTransferList.ToAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }

                            //Checking ClassRef.

                            if (node.Name.Equals(QBXmlBankTransfers.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.Amount[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }


                            //Checking Memo 
                            if (node.Name.Equals(QBXmlBankTransfers.Memo.ToString()))
                            {
                                BankTransferList.Memo = node.InnerText;
                            }

                        }
                        //Adding TimeEntry list.
                        this.Add(BankTransferList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTransferEntry.RemoveAll();

                #endregion
            }

            //Returning object.
            return this;
        }

  
        public Collection<BankTransferList> populateTransferEntryList(string QBFileName, DateTime fromDate, DateTime toDate)
        {
            #region Getting Transfer Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TransferEntryXmlList = string.Empty;
            TransferEntryXmlList = QBCommonUtilities.GetListFromQuickBookByDate("TransferQueryRq", QBFileName, fromDate, toDate);

            BankTransferList BankTransferList;
            #endregion


            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTransferEntry = new XmlDocument();

            outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
            XmlFileData = TransferEntryXmlList;
            int count = 0;
            int count1 = 0;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.

            //Getting current version of System. BUG(676)


            if (TransferEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTransferEntry.LoadXml(TransferEntryXmlList);
                XmlFileData = TransferEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.


                XmlNodeList TransferNodeList = outputXMLDocOfTransferEntry.GetElementsByTagName(QBXmlBankTransfers.TransferRet.ToString());
                foreach (XmlNode TransferEntriesNodes in TransferNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        BankTransferList = new BankTransferList();
                        foreach (XmlNode node in TransferEntriesNodes.ChildNodes)
                        {
                            //checking txnId
                            if (node.Name.Equals(QBXmlBankTransfers.TxnID.ToString()))
                            {
                                BankTransferList.TxnID = node.InnerText;
                            }
                            //checking TimeCreated
                            if (node.Name.Equals(QBXmlBankTransfers.TimeCreated.ToString()))
                                BankTransferList.TimeCreated = node.InnerText;

                            //checking TimeModified
                            if (node.Name.Equals(QBXmlBankTransfers.TimeModified.ToString()))
                                BankTransferList.TimeModified = node.InnerText;

                            //checking EditSequence
                            if (node.Name.Equals(QBXmlBankTransfers.EditSequence.ToString()))
                                BankTransferList.EditSequence = node.InnerText;

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBXmlBankTransfers.TxnNumber.ToString()))
                            {
                                BankTransferList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBXmlBankTransfers.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);

                                    BankTransferList.TxnDate = dttxn.ToString("MM/dd/yyyy");

                                }
                                catch
                                {
                                    BankTransferList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking TransferFromFullName
                            if (node.Name.Equals(QBXmlBankTransfers.TransferFromAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferFromAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking FromAccountBalance
                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.FromAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }
                            //checking TransferToAccountRef


                            if (node.Name.Equals(QBXmlBankTransfers.TransferToAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.TransferToAccountRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking ToAccountBalance

                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.ToAccountBalance[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }

                            //Checking ClassRef.

                            if (node.Name.Equals(QBXmlBankTransfers.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlBankTransfers.FullName.ToString()))
                                    {
                                        if (childNodes.InnerText.Length > 159)
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                        else
                                            BankTransferList.ClassRef[count1] = childNodes.InnerText;
                                    }
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBXmlBankTransfers.Amount.ToString()))
                            {
                                try
                                {
                                    BankTransferList.Amount[count] = Convert.ToDecimal(node.InnerText);
                                }
                                catch { }
                            }


                            //Checking Memo 
                            if (node.Name.Equals(QBXmlBankTransfers.Memo.ToString()))
                            {
                                BankTransferList.Memo = node.InnerText;
                            }

                        }
                        //Adding TimeEntry list.
                        this.Add(BankTransferList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTransferEntry.RemoveAll();

                #endregion
            }

            //Returning object.
            return this;
        }


     



        //This method is used to get the xml response from the QuikBooks.
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

    }
}
