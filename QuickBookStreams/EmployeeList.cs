using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    public class EmployeeList : BaseEmployeeList
    {
        #region Public methods

        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }


        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }


        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        public string Addr1
        {
            get { return m_EmployeeAddress1; }
            set { m_EmployeeAddress1 = value; }
        }


        public string Addr2
        {
            get { return m_EmployeeAddress2; }
            set { m_EmployeeAddress2 = value; }
        }


        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string PrintAs
        {
            get { return m_PrintAs; }
            set { m_PrintAs = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Mobile
        {
            get { return m_Mobile; }
            set { m_Mobile = value; }
        }

        public string Pager
        {
            get { return m_Pager; }
            set { m_Pager = value; }
        }

        public string PagerPin
        {
            get { return m_PagerPin; }
            set { m_PagerPin = value; }
        }

        public string AltPhone
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }


        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }


        public string SSN
        {
            get { return m_SSN; }
            set { m_SSN = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }


        public string EmployeeType
        {
            get { return m_EmployeeType; }
            set { m_EmployeeType = value; }
        }

        public string Gender
        {
            get { return m_Gender; }
            set { m_Gender = value; }
        }

        public string HiredDate
        {
            get { return m_HiredDate; }
            set { m_HiredDate = value; }
        }


        public string ReleasedDate
        {
            get { return m_ReleasedDate; }
            set { m_ReleasedDate = value; }
        }


        public string BirthDate
        {
            get { return m_BirthDate; }
            set { m_BirthDate = value; }
        }

        public string AccountNumber
        {
            get { return m_AccountNumber; }
            set { m_AccountNumber = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string BillingRateFullName
        {
            get { return m_BillingRateFullName; }
            set { m_BillingRateFullName = value; }
        }

        public string PayPeriod
        {
            get { return m_PayPeriod; }
            set { m_PayPeriod = value; }
        }

        public string ClassFullName
        {
            get { return m_ClassFullName; }
            set { m_ClassFullName = value; }
        }

        public string ClearEarnings
        {
            get { return m_ClearEarnings; }
            set { m_ClearEarnings = value; }
        }

        public string PayrollItemWageFullName
        {
            get { return m_PayrollItemWageFullName; }
            set { m_PayrollItemWageFullName = value; }
        }

        public decimal Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }

        public string IsUsingTimeDataToCreatePaychecks
        {
            get { return m_IsUsingTimeDataToCreatePaychecks; }
            set { m_IsUsingTimeDataToCreatePaychecks = value; }
        }

        public string UseTimeDataToCreatePaychecks
        {
            get { return m_UseTimeDataToCreatePaychecks; }
            set { m_UseTimeDataToCreatePaychecks = value; }
        }

        public string SickHoursAvailable
        {
            get { return m_HoursAvailable; }
            set { m_HoursAvailable = value; }
        }


        public string SickAccrualPeriod
        {
            get { return m_AccrualPeriod; }
            set { m_AccrualPeriod = value; }
        }

        public string SickHoursAccrued
        {
            get { return m_HoursAccrued; }
            set { m_HoursAccrued = value; }
        }

        public string SickMaximumHours
        {
            get { return m_MaximumHours; }
            set { m_MaximumHours = value; }
        }

        public string SickIsResettingHoursEachNewYear
        {
            get { return m_IsResettingHoursEachNewYear; }
            set { m_IsResettingHoursEachNewYear = value; }
        }

        public string SickHoursUsed
        {
            get { return m_HoursUsed; }
            set { m_HoursUsed = value; }
        }

        public string SickAccrualStartDate
        {
            get { return m_AccrualStartDate; }
            set { m_AccrualStartDate = value; }
        }

        public string VacationHoursAvailable
        {
            get { return m_VHoursAvailable; }
            set { m_VHoursAvailable = value; }
        }

        public string VacationAccrualPeriod
        {
            get { return m_VAccrualPeriod; }
            set { m_VAccrualPeriod = value; }
        }

        public string VacationHoursAccrued
        {
            get { return m_VHoursAccrued; }
            set { m_VHoursAccrued = value; }
        }

        public string VacationMaximumHours
        {
            get { return m_VMaximumHours; }
            set { m_VMaximumHours = value; }
        }

        public string VacationIsResettingHoursEachNewYear
        {
            get { return m_VIsResettingHoursEachNewYear; }
            set { m_VIsResettingHoursEachNewYear = value; }
        }

        public string VacationHoursUsed
        {
            get { return m_VHoursUsed; }
            set { m_VHoursUsed = value; }
        }

        public string VacationAccrualStartDate
        {
            get { return m_VAccrualStartDate; }
            set { m_VAccrualStartDate = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }


        public string OwnerID
        {
            get { return m_OwnerID; }
            set { m_OwnerID = value; }
        }
               

        //Axis 8.0
        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];

        #endregion
    }

    public class QBEmployeeListCollection : Collection<EmployeeList>
    {
        #region public Method

        string XmlFileData = string.Empty;



        /// <summary>
        /// this method returns all Employee in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllEmployeeList(string QBFileName)
        {
            string employeeXmlList = QBCommonUtilities.GetListFromQuickBook("EmployeeQueryRq", QBFileName);

            List<string> employeeList = new List<string>();
            List<string> employeeList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(employeeXmlList);

            XmlFileData = employeeXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList EmployeeNodeList = outputXMLDoc.GetElementsByTagName(QBEmployeeList.EmployeeRet.ToString());

            foreach (XmlNode empNodes in EmployeeNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in empNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBEmployeeList.Name.ToString()))
                        {
                            if (!employeeList.Contains(node.InnerText))
                            {
                                employeeList.Add(node.InnerText);
                                //issue 593
                                //string name = node.InnerText + "     :     Employee";
                                string name = node.InnerText;

                                employeeList1.Add(name);
                            }
                        }
                    }
                }
            }
            if(employeeList1.Count!=0)employeeList1.Sort();
            CommonUtilities.GetInstance().EmployeeListWithType = employeeList1;

            //463
            employeeXmlList = null;
            EmployeeNodeList = null;
            XmlFileData = null;
            outputXMLDoc = null;
            //463
            return employeeList;

        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<EmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string EmployeeXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("EmployeeQueryRq", QBFileName, FromDate, ToDate);
            }

            EmployeeList EmployeeList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Axis 8.0; declare integer i
            int i = 0;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfEmployee = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (EmployeeXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfEmployee.LoadXml(EmployeeXmlList);
                XmlFileData = EmployeeXmlList;

                #region Getting Employee Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList EmployeeNodeList = outputXMLDocOfEmployee.GetElementsByTagName(QBEmployeeList.EmployeeRet.ToString());

                foreach (XmlNode EmployeeNodes in EmployeeNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        EmployeeList = new EmployeeList();
                        foreach (XmlNode node in EmployeeNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBEmployeeList.ListID.ToString()))
                            {
                                EmployeeList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBEmployeeList.Name.ToString()))
                            {
                                EmployeeList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBEmployeeList.IsActive.ToString()))
                            {
                                EmployeeList.IsActive = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBEmployeeList.Salutation.ToString()))
                            {
                                EmployeeList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBEmployeeList.FirstName.ToString()))
                            {
                                EmployeeList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBEmployeeList.MiddleName.ToString()))
                            {
                                EmployeeList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBEmployeeList.LastName.ToString()))
                            {
                                EmployeeList.LastName = node.InnerText;
                            }



                            //Checking EmployeeAddress
                            if (node.Name.Equals(QBEmployeeList.EmployeeAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.Addr1.ToString()))
                                        EmployeeList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.Addr2.ToString()))
                                        EmployeeList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.City.ToString()))
                                        EmployeeList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.State.ToString()))
                                        EmployeeList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.PostalCode.ToString()))
                                        EmployeeList.PostalCode = childNode.InnerText;

                                }
                            }

                            //Checking PrintAs
                            if (node.Name.Equals(QBEmployeeList.PrintAs.ToString()))
                            {
                                EmployeeList.PrintAs = node.InnerText;
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBEmployeeList.Phone.ToString()))
                            {
                                EmployeeList.Phone = node.InnerText;
                            }

                            //Checking Mobile
                            if (node.Name.Equals(QBEmployeeList.Mobile.ToString()))
                            {
                                EmployeeList.Mobile = node.InnerText;
                            }

                            //Checking Pager
                            if (node.Name.Equals(QBEmployeeList.Pager.ToString()))
                            {
                                EmployeeList.Pager = node.InnerText;
                            }


                            //Checking PagerPin
                            if (node.Name.Equals(QBEmployeeList.PagerPin.ToString()))
                            {
                                EmployeeList.PagerPin = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBEmployeeList.AltPhone.ToString()))
                            {
                                EmployeeList.AltPhone = node.InnerText;
                            }


                            //Checking Fax
                            if (node.Name.Equals(QBEmployeeList.Fax.ToString()))
                            {
                                EmployeeList.Fax = node.InnerText;
                            }


                            //Checking SSN
                            if (node.Name.Equals(QBEmployeeList.SSN.ToString()))
                            {
                                EmployeeList.SSN = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBEmployeeList.Email.ToString()))
                            {
                                EmployeeList.Email = node.InnerText;
                            }

                            //Checking EmployeeType
                            if (node.Name.Equals(QBEmployeeList.EmployeeType.ToString()))
                            {
                                EmployeeList.EmployeeType = node.InnerText;
                            }

                            //Checking Gender
                            if (node.Name.Equals(QBEmployeeList.Gender.ToString()))
                            {
                                EmployeeList.Gender = node.InnerText;
                            }

                            //Checking HiredDate
                            if (node.Name.Equals(QBEmployeeList.HiredDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.HiredDate = node.InnerText;
                                }

                                //EmployeeList.HiredDate = node.InnerText;
                            }

                            //Checking ReleasedDate
                            if (node.Name.Equals(QBEmployeeList.ReleasedDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.ReleasedDate = node.InnerText;
                                }


                                //EmployeeList.ReleasedDate = node.InnerText;
                            }

                            //Checking BirthDate
                            if (node.Name.Equals(QBEmployeeList.BirthDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.BirthDate = node.InnerText;
                                }

                                //EmployeeList.BirthDate = node.InnerText;
                            }


                            //Checking AccountNumber
                            if (node.Name.Equals(QBEmployeeList.AccountNumber.ToString()))
                            {
                                EmployeeList.AccountNumber = node.InnerText;
                            }

                            //Checking Notes
                            if (node.Name.Equals(QBEmployeeList.Notes.ToString()))
                            {
                                EmployeeList.Notes = node.InnerText;
                            }

                            //Checking BillingRate FullName
                            if (node.Name.Equals(QBEmployeeList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.FullName.ToString()))
                                        EmployeeList.BillingRateFullName = childNode.InnerText;
                                }
                            }


                            //Checking Payperiod
                            if (node.Name.Equals(QBEmployeeList.EmployeePayrollInfo.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    //Checking ClassFullName
                                    if (childNode.Name.Equals(QBEmployeeList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                EmployeeList.ClassFullName = childNodes.InnerText;
                                        }
                                    }

                                    //Checking clearEarnings
                                    if (childNode.Name.Equals(QBEmployeeList.PayPeriod.ToString()))
                                    {
                                        EmployeeList.PayPeriod = childNode.InnerText;
                                    }


                                    //Checking PayrollItemWageFullName
                                    if (childNode.Name.Equals(QBEmployeeList.Earnings.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.PayrollItemWageRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes1 in childNodes.ChildNodes)
                                                {
                                                    if (childNodes1.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                        EmployeeList.PayrollItemWageFullName = childNodes1.InnerText;
                                                }
                                            }
                                        }
                                    }
                                    if (childNode.Name.Equals(QBEmployeeList.ClearEarnings.ToString()))
                                    {

                                        EmployeeList.ClearEarnings = childNode.InnerText;
                                    }

                                    //Checking Rate
                                    if (childNode.Name.Equals(QBEmployeeList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            EmployeeList.Rate = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }

                                        //EmployeeList.Rate = node.InnerText;
                                    }


                                    //Checking IsUsingTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.IsUsingTimeDataToCreatePaychecks.ToString()))
                                    {
                                        EmployeeList.IsUsingTimeDataToCreatePaychecks = childNode.InnerText;
                                    }

                                    //Checking UseTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.UseTimeDataToCreatePaychecks.ToString()))
                                    {
                                        EmployeeList.UseTimeDataToCreatePaychecks = childNode.InnerText;
                                    }


                                    //Checking Employee SickHours
                                    if (childNode.Name.Equals(QBEmployeeList.SickHours.ToString()))
                                    {
                                        //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.SickHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.SickAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.SickHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.SickMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.SickIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.SickHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.SickAccrualStartDate = childNodes.InnerText;
                                        }
                                    }


                                    //Checking Employee VacationHours
                                    if (childNode.Name.Equals(QBEmployeeList.VacationHours.ToString()))
                                    {
                                        //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.VacationHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.VacationAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.VacationHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.VacationMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.VacationIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.VacationHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.VacationAccrualStartDate = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //Checking ExternalGUID
                            if (node.Name.Equals(QBEmployeeList.ExternalGUID.ToString()))
                            {
                                EmployeeList.ExternalGUID = node.InnerText;
                            }
                           
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    EmployeeList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        EmployeeList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }

                        }

                        this.Add(EmployeeList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfEmployee.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<EmployeeList> PopulateEmployeeList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string EmployeeXmlList = string.Empty;

            if (inActiveFilter == false)//bug no. 395
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBook("EmployeeQueryRq", QBFileName);
            }
            else
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("EmployeeQueryRq", QBFileName);
            }
            EmployeeList EmployeeList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfEmployee = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (EmployeeXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfEmployee.LoadXml(EmployeeXmlList);
                XmlFileData = EmployeeXmlList;

                #region Getting Employee Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList EmployeeNodeList = outputXMLDocOfEmployee.GetElementsByTagName(QBEmployeeList.EmployeeRet.ToString());

                foreach (XmlNode EmployeeNodes in EmployeeNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        EmployeeList = new EmployeeList();
                        foreach (XmlNode node in EmployeeNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBEmployeeList.ListID.ToString()))
                            {
                                EmployeeList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBEmployeeList.Name.ToString()))
                            {
                                EmployeeList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBEmployeeList.IsActive.ToString()))
                            {
                                EmployeeList.IsActive = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBEmployeeList.Salutation.ToString()))
                            {
                                EmployeeList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBEmployeeList.FirstName.ToString()))
                            {
                                EmployeeList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBEmployeeList.MiddleName.ToString()))
                            {
                                EmployeeList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBEmployeeList.LastName.ToString()))
                            {
                                EmployeeList.LastName = node.InnerText;
                            }



                            //Checking EmployeeAddress
                            if (node.Name.Equals(QBEmployeeList.EmployeeAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.Addr1.ToString()))
                                        EmployeeList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.Addr2.ToString()))
                                        EmployeeList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.City.ToString()))
                                        EmployeeList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.State.ToString()))
                                        EmployeeList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.PostalCode.ToString()))
                                        EmployeeList.PostalCode = childNode.InnerText;

                                }
                            }

                            //Checking PrintAs
                            if (node.Name.Equals(QBEmployeeList.PrintAs.ToString()))
                            {
                                EmployeeList.PrintAs = node.InnerText;
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBEmployeeList.Phone.ToString()))
                            {
                                EmployeeList.Phone = node.InnerText;
                            }

                            //Checking Mobile
                            if (node.Name.Equals(QBEmployeeList.Mobile.ToString()))
                            {
                                EmployeeList.Mobile = node.InnerText;
                            }

                            //Checking Pager
                            if (node.Name.Equals(QBEmployeeList.Pager.ToString()))
                            {
                                EmployeeList.Pager = node.InnerText;
                            }


                            //Checking PagerPin
                            if (node.Name.Equals(QBEmployeeList.PagerPin.ToString()))
                            {
                                EmployeeList.PagerPin = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBEmployeeList.AltPhone.ToString()))
                            {
                                EmployeeList.AltPhone = node.InnerText;
                            }


                            //Checking Fax
                            if (node.Name.Equals(QBEmployeeList.Fax.ToString()))
                            {
                                EmployeeList.Fax = node.InnerText;
                            }


                            //Checking SSN
                            if (node.Name.Equals(QBEmployeeList.SSN.ToString()))
                            {
                                EmployeeList.SSN = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBEmployeeList.Email.ToString()))
                            {
                                EmployeeList.Email = node.InnerText;
                            }

                            //Checking EmployeeType
                            if (node.Name.Equals(QBEmployeeList.EmployeeType.ToString()))
                            {
                                EmployeeList.EmployeeType = node.InnerText;
                            }

                            //Checking Gender
                            if (node.Name.Equals(QBEmployeeList.Gender.ToString()))
                            {
                                EmployeeList.Gender = node.InnerText;
                            }

                            //Checking HiredDate
                            if (node.Name.Equals(QBEmployeeList.HiredDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.HiredDate = node.InnerText;
                                }

                                //EmployeeList.HiredDate = node.InnerText;
                            }

                            //Checking ReleasedDate
                            if (node.Name.Equals(QBEmployeeList.ReleasedDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.ReleasedDate = node.InnerText;
                                }


                                //EmployeeList.ReleasedDate = node.InnerText;
                            }

                            //Checking BirthDate
                            if (node.Name.Equals(QBEmployeeList.BirthDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.BirthDate = node.InnerText;
                                }

                                //EmployeeList.BirthDate = node.InnerText;
                            }


                            //Checking AccountNumber
                            if (node.Name.Equals(QBEmployeeList.AccountNumber.ToString()))
                            {
                                EmployeeList.AccountNumber = node.InnerText;
                            }

                            //Checking Notes
                            if (node.Name.Equals(QBEmployeeList.Notes.ToString()))
                            {
                                EmployeeList.Notes = node.InnerText;
                            }

                            //Checking BillingRate FullName
                            if (node.Name.Equals(QBEmployeeList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.FullName.ToString()))
                                        EmployeeList.BillingRateFullName = childNode.InnerText;
                                }
                            }


                            //Checking Payperiod
                            if (node.Name.Equals(QBEmployeeList.EmployeePayrollInfo.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                                    //Checking ClassFullName
                                    if (childNode.Name.Equals(QBEmployeeList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                EmployeeList.ClassFullName = childNodes.InnerText;
                                        }
                                    }

                                    //Checking clearEarnings
                                    if (childNode.Name.Equals(QBEmployeeList.PayPeriod.ToString()))
                                    {
                                        EmployeeList.PayPeriod = childNode.InnerText;
                                    }


                                    //Checking PayrollItemWageFullName
                                    if (childNode.Name.Equals(QBEmployeeList.Earnings.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.PayrollItemWageRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes1 in childNodes.ChildNodes)
                                                {
                                                    if (childNodes1.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                        EmployeeList.PayrollItemWageFullName = childNodes1.InnerText;
                                                }
                                            }
                                        }
                                    }
                                    if (childNode.Name.Equals(QBEmployeeList.ClearEarnings.ToString()))
                                    {

                                        EmployeeList.ClearEarnings = childNode.InnerText;
                                    }

                                    //Checking Rate
                                    if (childNode.Name.Equals(QBEmployeeList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            EmployeeList.Rate = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }

                                        //EmployeeList.Rate = node.InnerText;
                                    }


                                    //Checking IsUsingTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.IsUsingTimeDataToCreatePaychecks.ToString()))
                                    {
                                        EmployeeList.IsUsingTimeDataToCreatePaychecks = childNode.InnerText;
                                    }

                                    //Checking UseTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.UseTimeDataToCreatePaychecks.ToString()))
                                    {
                                        EmployeeList.UseTimeDataToCreatePaychecks = childNode.InnerText;
                                    }


                                    //Checking Employee SickHours
                                    if (childNode.Name.Equals(QBEmployeeList.SickHours.ToString()))
                                    {
                                        //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.SickHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.SickAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.SickHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.SickMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.SickIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.SickHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.SickAccrualStartDate = childNodes.InnerText;
                                        }
                                    }


                                    //Checking Employee VacationHours
                                    if (childNode.Name.Equals(QBEmployeeList.VacationHours.ToString()))
                                    {
                                        //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.VacationHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.VacationAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.VacationHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.VacationMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.VacationIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.VacationHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.VacationAccrualStartDate = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //Checking ExternalGUID
                            if (node.Name.Equals(QBEmployeeList.ExternalGUID.ToString()))
                            {
                                EmployeeList.ExternalGUID = node.InnerText;
                            }
                            
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    EmployeeList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        EmployeeList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }


                        }

                        this.Add(EmployeeList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfEmployee.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Employee List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<EmployeeList> PopulateEmployeeList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string EmployeeXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookByDate("EmployeeQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                EmployeeXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("EmployeeQueryRq", QBFileName, FromDate, ToDate);
            }
            EmployeeList EmployeeList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfEmployee = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (EmployeeXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfEmployee.LoadXml(EmployeeXmlList);
                XmlFileData = EmployeeXmlList;

                #region Getting Employee Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList EmployeeNodeList = outputXMLDocOfEmployee.GetElementsByTagName(QBEmployeeList.EmployeeRet.ToString());

                foreach (XmlNode EmployeeNodes in EmployeeNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        EmployeeList = new EmployeeList();
                        foreach (XmlNode node in EmployeeNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBEmployeeList.ListID.ToString()))
                            {
                                EmployeeList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBEmployeeList.Name.ToString()))
                            {
                                EmployeeList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBEmployeeList.IsActive.ToString()))
                            {
                                EmployeeList.IsActive = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBEmployeeList.Salutation.ToString()))
                            {
                                EmployeeList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBEmployeeList.FirstName.ToString()))
                            {
                                EmployeeList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBEmployeeList.MiddleName.ToString()))
                            {
                                EmployeeList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBEmployeeList.LastName.ToString()))
                            {
                                EmployeeList.LastName = node.InnerText;
                            }



                            //Checking EmployeeAddress
                            if (node.Name.Equals(QBEmployeeList.EmployeeAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.Addr1.ToString()))
                                        EmployeeList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.Addr2.ToString()))
                                        EmployeeList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.City.ToString()))
                                        EmployeeList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.State.ToString()))
                                        EmployeeList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBEmployeeList.PostalCode.ToString()))
                                        EmployeeList.PostalCode = childNode.InnerText;

                                }
                            }

                            //Checking PrintAs
                            if (node.Name.Equals(QBEmployeeList.PrintAs.ToString()))
                            {
                                EmployeeList.PrintAs = node.InnerText;
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBEmployeeList.Phone.ToString()))
                            {
                                EmployeeList.Phone = node.InnerText;
                            }

                            //Checking Mobile
                            if (node.Name.Equals(QBEmployeeList.Mobile.ToString()))
                            {
                                EmployeeList.Mobile = node.InnerText;
                            }

                            //Checking Pager
                            if (node.Name.Equals(QBEmployeeList.Pager.ToString()))
                            {
                                EmployeeList.Pager = node.InnerText;
                            }


                            //Checking PagerPin
                            if (node.Name.Equals(QBEmployeeList.PagerPin.ToString()))
                            {
                                EmployeeList.PagerPin = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBEmployeeList.AltPhone.ToString()))
                            {
                                EmployeeList.AltPhone = node.InnerText;
                            }


                            //Checking Fax
                            if (node.Name.Equals(QBEmployeeList.Fax.ToString()))
                            {
                                EmployeeList.Fax = node.InnerText;
                            }


                            //Checking SSN
                            if (node.Name.Equals(QBEmployeeList.SSN.ToString()))
                            {
                                EmployeeList.SSN = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBEmployeeList.Email.ToString()))
                            {
                                EmployeeList.Email = node.InnerText;
                            }

                            //Checking EmployeeType
                            if (node.Name.Equals(QBEmployeeList.EmployeeType.ToString()))
                            {
                                EmployeeList.EmployeeType = node.InnerText;
                            }

                            //Checking Gender
                            if (node.Name.Equals(QBEmployeeList.Gender.ToString()))
                            {
                                EmployeeList.Gender = node.InnerText;
                            }

                            //Checking HiredDate
                            if (node.Name.Equals(QBEmployeeList.HiredDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.HiredDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.HiredDate = node.InnerText;
                                }

                                //EmployeeList.HiredDate = node.InnerText;
                            }

                            //Checking ReleasedDate
                            if (node.Name.Equals(QBEmployeeList.ReleasedDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.ReleasedDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.ReleasedDate = node.InnerText;
                                }


                                //EmployeeList.ReleasedDate = node.InnerText;
                            }

                            //Checking BirthDate
                            if (node.Name.Equals(QBEmployeeList.BirthDate.ToString()))
                            {

                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        EmployeeList.BirthDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    EmployeeList.BirthDate = node.InnerText;
                                }

                                //EmployeeList.BirthDate = node.InnerText;
                            }


                            //Checking AccountNumber
                            if (node.Name.Equals(QBEmployeeList.AccountNumber.ToString()))
                            {
                                EmployeeList.AccountNumber = node.InnerText;
                            }

                            //Checking Notes
                            if (node.Name.Equals(QBEmployeeList.Notes.ToString()))
                            {
                                EmployeeList.Notes = node.InnerText;
                            }

                            //Checking BillingRate FullName
                            if (node.Name.Equals(QBEmployeeList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBEmployeeList.FullName.ToString()))
                                        EmployeeList.BillingRateFullName = childNode.InnerText;
                                }
                            }


                            //Checking Payperiod
                            if (node.Name.Equals(QBEmployeeList.EmployeePayrollInfo.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {

                            //Checking ClassFullName
                                    if (childNode.Name.Equals(QBEmployeeList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                EmployeeList.ClassFullName = childNodes.InnerText;
                                }
                            }

                            //Checking clearEarnings
                                    if (childNode.Name.Equals(QBEmployeeList.PayPeriod.ToString()))
                            {
                                        EmployeeList.PayPeriod = childNode.InnerText;
                            }


                            //Checking PayrollItemWageFullName
                                    if (childNode.Name.Equals(QBEmployeeList.Earnings.ToString()))
                            {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                {
                                            if (childNodes.Name.Equals(QBEmployeeList.PayrollItemWageRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes1 in childNodes.ChildNodes)
                                                {
                                                    if (childNodes1.Name.Equals(QBEmployeeList.FullName.ToString()))
                                                        EmployeeList.PayrollItemWageFullName = childNodes1.InnerText;
                                }
                            }
                                        }
                                    }
                                    if (childNode.Name.Equals(QBEmployeeList.ClearEarnings.ToString()))
                                    {

                                        EmployeeList.ClearEarnings = childNode.InnerText;
                                    }

                            //Checking Rate
                                    if (childNode.Name.Equals(QBEmployeeList.Rate.ToString()))
                            {
                                try
                                {
                                            EmployeeList.Rate = Convert.ToDecimal(childNode.InnerText);
                                }
                                catch
                                { }

                                //EmployeeList.Rate = node.InnerText;
                            }


                            //Checking IsUsingTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.IsUsingTimeDataToCreatePaychecks.ToString()))
                            {
                                        EmployeeList.IsUsingTimeDataToCreatePaychecks = childNode.InnerText;
                            }

                            //Checking UseTimeDataToCreatePayChecks
                                    if (childNode.Name.Equals(QBEmployeeList.UseTimeDataToCreatePaychecks.ToString()))
                            {
                                        EmployeeList.UseTimeDataToCreatePaychecks = childNode.InnerText;
                            }


                            //Checking Employee SickHours
                                    if (childNode.Name.Equals(QBEmployeeList.SickHours.ToString()))
                            {
                                //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.SickHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.SickAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.SickHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.SickMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.SickIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.SickHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.SickAccrualStartDate = childNodes.InnerText;
                                }
                            }


                            //Checking Employee VacationHours
                                    if (childNode.Name.Equals(QBEmployeeList.VacationHours.ToString()))
                            {
                                //Getting values of address.
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                {
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAvailable.ToString()))
                                                EmployeeList.VacationHoursAvailable = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualPeriod.ToString()))
                                                EmployeeList.VacationAccrualPeriod = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursAccrued.ToString()))
                                                EmployeeList.VacationHoursAccrued = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.MaximumHours.ToString()))
                                                EmployeeList.VacationMaximumHours = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.IsResettingHoursEachNewYear.ToString()))
                                                EmployeeList.VacationIsResettingHoursEachNewYear = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.HoursUsed.ToString()))
                                                EmployeeList.VacationHoursUsed = childNodes.InnerText;
                                            if (childNodes.Name.Equals(QBEmployeeList.AccrualStartDate.ToString()))
                                                EmployeeList.VacationAccrualStartDate = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //Checking ExternalGUID
                            if (node.Name.Equals(QBEmployeeList.ExternalGUID.ToString()))
                            {
                                EmployeeList.ExternalGUID = node.InnerText;
                            }                           

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    EmployeeList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        EmployeeList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }

                        this.Add(EmployeeList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfEmployee.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }
        




        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
        #endregion


        
    }
}