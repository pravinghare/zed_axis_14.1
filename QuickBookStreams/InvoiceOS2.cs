using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    class QBInvoiceOS2 : BaseInvoiceOS2
    {
        /// <summary>
        /// Product quantity max value 999999999
        /// </summary>
        public int Quantity
        {
            get { return m_quantityShipped; }
            set
            {
                if (value <= 999999999)
                {
                    m_quantityShipped = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid Quantity length");
                }
            }
        }

        /// <summary>
        /// Product name max length 25
        /// </summary>
        public string ItemRef_FullName
        {
            get { return m_product; }
            set
            {
                if (value.Length <= 25)
                {
                    m_product = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ItemRef:FullName length");
                }
            }
        }
    }
}
