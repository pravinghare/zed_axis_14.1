﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using System.Windows.Forms;
using System.Collections;

namespace Streams
{
    public class VendorCreditList : BaseVendorCreditList
    {

        string integrated_stringh = string.Empty;

        #region Public Methods

        public string TxnID
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string VendorRefFullName
        {
            get { return m_VendorRefFullName; }
            set { m_VendorRefFullName = value; }
        }

        public string APAccountFullName
        {
            get { return m_APAccountFullName; }
            set { m_APAccountFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public decimal? CreditAmount
        {
            get { return m_CreditAmount; }
            set { m_CreditAmount = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? CreditAmountInHomeCurrency
        {
            get { return m_CreditAmountInHomeCurrency; }
            set { m_CreditAmountInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }


        //For ExpenseLineRet

        public List<string> ExpTxnLineID
        {
            get { return m_ExpTxnLineID; }
            set { m_ExpTxnLineID = value; }
        }

        public List<string> ExpAccountfullName
        {
            get { return m_ExpAccountFullName; }
            set { m_ExpAccountFullName = value; }
        }

        public List<decimal?> ExpAmount
        {
            get { return m_ExpAmount; }
            set { m_ExpAmount = value; }
        }

        public List<string> ExpMemo
        {
            get { return m_ExpMemo; }
            set { m_ExpMemo = value; }
        }

        public List<string> ExpCustomerFullName
        {
            get { return m_ExpCustomerFullname; }
            set { m_ExpCustomerFullname = value; }
        }

        public List<string> ExpClassName
        {
            get { return m_ExpClassFullName; }
            set { m_ExpClassFullName = value; }
        }


        public List<string> ExpBillableStatus
        {
            get { return m_ExpBillableStatus; }
            set { m_ExpBillableStatus = value; }
        }

        public List<string> ExpSalesRepRefFullName
        {
            get { return m_ExpSalesRepRefFullName; }
            set { m_ExpSalesRepRefFullName = value; }
        }

        //For ItelLineRet

        public List<string> ItemTxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_ItemFullName; }
            set { m_ItemFullName = value; }
        }
        // axis 10.0 changes
        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        // axis 10.0 changes ends
        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<decimal?> Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public List<decimal?> ItemAmount
        {
            get { return m_ItemAmount; }
            set { m_ItemAmount = value; }
        }

        public List<string> ItemCustomerFullName
        {
            get { return m_ItemCustomerFullName; }
            set { m_ItemCustomerFullName = value; }
        }

        public List<string> ItemClassFullName
        {
            get { return m_ItemClassName; }
            set { m_ItemClassName = value; }
        }


        public List<string> ItemBillableStatus
        {
            get { return m_ItemBillableStatus; }
            set { m_ItemBillableStatus = value; }
        }

        public List<string> ItemSalesRepRefFullName
        {
            get { return m_ItemSalesRepRefFullName; }
            set { m_ItemSalesRepRefFullName = value; }
        }

        //For GoupItemLineRet

        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineId; }
            set { m_GroupTxnLineId = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemgroupFullName; }
            set { m_ItemgroupFullName = value; }
        }

        //for sub ItemLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<decimal?> GroupLineCost
        {
            get { return m_GroupLineCost; }
            set { m_GroupLineCost = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineCustomerFullName
        {
            get { return m_GroupLineCustomerFullName; }
            set { m_GroupLineCustomerFullName = value; }
        }

        public List<string> GroupLineClassFullName
        {
            get { return m_GroupLineClassName; }
            set { m_GroupLineClassName = value; }
        }


        public List<string> GroupLineBillableStatus
        {
            get { return m_GroupLineBillableStatus; }
            set { m_GroupLineBillableStatus = value; }
        }


        public List<string> GroupLineSalesRepRefFullName
        {
            get { return m_GroupLineSalesRepRefFullName; }
            set { m_GroupLineSalesRepRefFullName = value; }
        }
        #endregion
    }

    

    public class QBVendorCreditListCollection : Collection<VendorCreditList>
    {

       

        string XmlFileData = string.Empty;
        public Collection<VendorCreditList> PopulateVendorCreditAllDataList(string QBFileName)
        {

            #region Getting VendorCredit List from QuickBooks.

            //Getting item list from QuickBooks.
            string VendorCreditXmlList = string.Empty;
            VendorCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName);
           
            GetVendorCreditList(VendorCreditXmlList);
            #endregion
            //Returning object.
            return this;
        }
        #region private method
        /// <summary>
        /// Assigning the xml value to the class objects
        /// </summary>
        /// <param name="purchaseOrderXmlList"></param>
        /// <returns></returns>
        private Collection<VendorCreditList> GetVendorCreditList(string VendorCreditXmlList)
        {
            #region
            VendorCreditList VendorCreditList;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLVendorCredit = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (VendorCreditXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLVendorCredit.LoadXml(VendorCreditXmlList);
                XmlFileData = VendorCreditXmlList;


                #region Getting VendorCredit Values from XML

                //Getting CreditCardChargeList values from QuickBooks response.
                XmlNodeList VendorCreditNodeList = outputXMLVendorCredit.GetElementsByTagName(QBVendorCreditList.VendorCreditRet.ToString());

                foreach (XmlNode VendorCreditNodes in VendorCreditNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int i = 0;
                        int count = 0;
                        int count1 = 0;
                        int count2 = 0;
                        VendorCreditList = new VendorCreditList();

                        // Axis 10.0 changes ends
                        foreach (XmlNode node in VendorCreditNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBVendorCreditList.TxnID.ToString()))
                            {
                                VendorCreditList.TxnID = node.InnerText;
                            }

                            //Checking Account Ref list.
                            if (node.Name.Equals(QBVendorCreditList.VendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                        VendorCreditList.VendorRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayeeEntityRef List.
                            if (node.Name.Equals(QBVendorCreditList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                        VendorCreditList.APAccountFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBVendorCreditList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        VendorCreditList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        VendorCreditList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    VendorCreditList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBVendorCreditList.CreditAmount.ToString()))
                            {
                                VendorCreditList.CreditAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Chekcing CurrencyRefFullName
                            if (node.Name.Equals(QBVendorCreditList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                        VendorCreditList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ExchangeRate
                            if (node.Name.Equals(QBVendorCreditList.ExchangeRate.ToString()))
                            {
                                VendorCreditList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking AmountInHomeCurrency
                            if (node.Name.Equals(QBVendorCreditList.CreditAmountInHomeCurrency.ToString()))
                            {
                                VendorCreditList.CreditAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }


                            //Checking RefNumber
                            if (node.Name.Equals(QBVendorCreditList.RefNumber.ToString()))
                            {
                                VendorCreditList.RefNumber = node.InnerText;
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBVendorCreditList.Memo.ToString()))
                            {
                                VendorCreditList.Memo = node.InnerText;
                            }

                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBVendorCreditList.ExternalGUID.ToString()))
                            {
                                VendorCreditList.ExternalGUID = node.InnerText;
                            }

                            //Checking Expense Line ret values.(repeated)
                            if (node.Name.Equals(QBVendorCreditList.ExpenseLineRet.ToString()))
                            {
                                checkExpNullEntries(node, ref VendorCreditList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBVendorCreditList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            VendorCreditList.ExpTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            VendorCreditList.ExpTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Chekcing AccountRefFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                VendorCreditList.ExpAccountfullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //checking Expense Amount
                                    if (childNode.Name.Equals(QBVendorCreditList.Amount.ToString()))
                                    {
                                        VendorCreditList.ExpAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing Expense Memo
                                    if (childNode.Name.Equals(QBVendorCreditList.Memo.ToString()))
                                    {
                                        VendorCreditList.ExpMemo.Add(childNode.InnerText);
                                    }
                                    //Checking CustomerRefFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ExpCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking classRefFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ExpClassName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing BillableStatus
                                    if (childNode.Name.Equals(QBVendorCreditList.BillableStatus.ToString()))
                                    {
                                        VendorCreditList.ExpBillableStatus.Add(childNode.InnerText);
                                    }

                                    //Checking SalesRepRefFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ExpSalesRepRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                }
                                count++;
                            }

                            //Chekcing ItemLineRet
                            if (node.Name.Equals(QBVendorCreditList.ItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref VendorCreditList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBVendorCreditList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            VendorCreditList.ItemTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            VendorCreditList.ItemTxnLineID.Add(childNode.InnerText);
                                    }
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ItemFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    // Axis 10.0 changes
                                    //Chekcing Serial Number
                                    if (childNode.Name.Equals(QBVendorCreditList.SerialNumber.ToString()))
                                    {
                                        VendorCreditList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    // Checking Lot Number
                                    if (childNode.Name.Equals(QBVendorCreditList.LotNumber.ToString()))
                                    {
                                        VendorCreditList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends


                                    //Checking Desc
                                    if (childNode.Name.Equals(QBVendorCreditList.Desc.ToString()))
                                    {
                                        VendorCreditList.Desc.Add(childNode.InnerText);
                                    }
                                    //Checking Quantiy
                                    if (childNode.Name.Equals(QBVendorCreditList.Quantity.ToString()))
                                    {
                                        VendorCreditList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBVendorCreditList.UnitOfMeasure.ToString()))
                                    {
                                        VendorCreditList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking Cost
                                    if (childNode.Name.Equals(QBVendorCreditList.Cost.ToString()))
                                    {
                                        VendorCreditList.Cost.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBVendorCreditList.Amount.ToString()))
                                    {
                                        VendorCreditList.ItemAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing CustomerFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ItemCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing ClassFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ItemClassFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking BillableStatus
                                    if (childNode.Name.Equals(QBVendorCreditList.BillableStatus.ToString()))
                                    {
                                        VendorCreditList.ItemBillableStatus.Add(childNode.InnerText);
                                    }

                                    //Checking SalesTaxFullName
                                    if (childNode.Name.Equals(QBVendorCreditList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ItemSalesRepRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                }
                                count1++;
                            }

                            //Checking ItemGroupLineRet
                            if (node.Name.Equals(QBVendorCreditList.ItemGroupLineRet.ToString()))
                            {
                                int count3 = 0;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBVendorCreditList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            VendorCreditList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            VendorCreditList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBVendorCreditList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                VendorCreditList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking  sub ItemLineRet
                                    if (childNode.Name.Equals(QBVendorCreditList.ItemLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref VendorCreditList);

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking TxnLineID
                                            if (subChildNode.Name.Equals(QBVendorCreditList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    VendorCreditList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    VendorCreditList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }
                                            //Chekcing ItemRefFullName
                                            if (subChildNode.Name.Equals(QBVendorCreditList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                        VendorCreditList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc
                                            if (subChildNode.Name.Equals(QBVendorCreditList.Desc.ToString()))
                                            {
                                                VendorCreditList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantiy
                                            if (subChildNode.Name.Equals(QBVendorCreditList.Quantity.ToString()))
                                            {
                                                VendorCreditList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBVendorCreditList.UnitOfMeasure.ToString()))
                                            {
                                                VendorCreditList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }
                                            //Checking Cost
                                            if (subChildNode.Name.Equals(QBVendorCreditList.Cost.ToString()))
                                            {
                                                VendorCreditList.GroupLineCost.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking Amount
                                            if (subChildNode.Name.Equals(QBVendorCreditList.Amount.ToString()))
                                            {
                                                VendorCreditList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Chekcing CustomerFullName
                                            if (subChildNode.Name.Equals(QBVendorCreditList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                        VendorCreditList.GroupLineCustomerFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Chekcing ClassFullName
                                            if (subChildNode.Name.Equals(QBVendorCreditList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                        VendorCreditList.GroupLineClassFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking BillableStatus
                                            if (subChildNode.Name.Equals(QBVendorCreditList.BillableStatus.ToString()))
                                            {
                                                VendorCreditList.GroupLineBillableStatus.Add(subChildNode.InnerText);
                                            }

                                            //Checking SalesTaxFullName
                                            if (subChildNode.Name.Equals(QBVendorCreditList.SalesRepRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBVendorCreditList.FullName.ToString()))
                                                        VendorCreditList.GroupLineSalesRepRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                        }
                                        count3++;
                                    }
                                }
                                count2++;
                            }
                        }
                        //Adding CreditCardCharge list.
                        this.Add(VendorCreditList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLVendorCredit.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
            #endregion


        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref VendorCreditList vendorCreditList)
        {
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.TxnLineID.ToString()) == null)
            {
                vendorCreditList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.ItemRef.ToString()) == null)
            {
                vendorCreditList.GroupLineItemFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.Desc.ToString()) == null)
            {
                vendorCreditList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.Quantity.ToString()) == null)
            {
                vendorCreditList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.UnitOfMeasure.ToString()) == null)
            {
                vendorCreditList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.Cost.ToString()) == null)
            {
                vendorCreditList.GroupLineCost.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.Amount.ToString()) == null)
            {
                vendorCreditList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.CustomerRef.ToString()) == null)
            {
                vendorCreditList.GroupLineCustomerFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.ClassRef.ToString()) == null)
            {
                vendorCreditList.GroupLineClassFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.BillableStatus.ToString()) == null)
            {
                vendorCreditList.GroupLineBillableStatus.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBVendorCreditList.SalesRepRef.ToString()) == null)
            {
                vendorCreditList.GroupLineSalesRepRefFullName.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref VendorCreditList vendorCreditList)
        {
            if (node.SelectSingleNode("./" + QBVendorCreditList.TxnLineID.ToString()) == null)
            {
                vendorCreditList.ItemTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.ItemRef.ToString()) == null)
            {
                vendorCreditList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.SerialNumber.ToString()) == null)
            {
                vendorCreditList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.LotNumber.ToString()) == null)
            {
                vendorCreditList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Desc.ToString()) == null)
            {
                vendorCreditList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Quantity.ToString()) == null)
            {
                vendorCreditList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.UnitOfMeasure.ToString()) == null)
            {
                vendorCreditList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Cost.ToString()) == null)
            {
                vendorCreditList.Cost.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Amount.ToString()) == null)
            {
                vendorCreditList.ItemAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.CustomerRef.ToString()) == null)
            {
                vendorCreditList.ItemCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.ClassRef.ToString()) == null)
            {
                vendorCreditList.ItemClassFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.BillableStatus.ToString()) == null)
            {
                vendorCreditList.ItemBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.SalesRepRef.ToString()) == null)
            {
                vendorCreditList.ItemSalesRepRefFullName.Add(null);
            }
        }

        private void checkExpNullEntries(XmlNode node, ref VendorCreditList vendorCreditList)
        {
            if (node.SelectSingleNode("./" + QBVendorCreditList.TxnLineID.ToString()) == null)
            {
                vendorCreditList.ExpTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.TxnLineID.ToString()) == null)
            {
                vendorCreditList.ExpTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.AccountRef.ToString()) == null)
            {
                vendorCreditList.ExpAccountfullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Amount.ToString()) == null)
            {
                vendorCreditList.ExpAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.Memo.ToString()) == null)
            {
                vendorCreditList.ExpMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.CustomerRef.ToString()) == null)
            {
                vendorCreditList.ExpCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.ClassRef.ToString()) == null)
            {
                vendorCreditList.ExpClassName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.BillableStatus.ToString()) == null)
            {
                vendorCreditList.ExpBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBVendorCreditList.SalesRepRef.ToString()) == null)
            {
                vendorCreditList.ExpSalesRepRefFullName.Add(null);
            }
        }
        #endregion


        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion

        }

        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }


        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }


        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }

        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromDate, ToDate);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }

        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }
        public Collection<VendorCreditList> PopulateVendorCreditList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromRefnum, ToRefnum);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }
        public Collection<VendorCreditList> PopulateVendorCreditEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string vendorcreditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("VendorCreditQueryRq", QBFileName, entityFilter);
            return GetVendorCreditList(vendorcreditCardCreditXmlList);

            #endregion
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VendorCreditList> ModifiedPopulateVendorCreditList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.

            string vendorcreditCardCreditXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("VendorCreditQueryRq", QBFileName, FromDate, ToDate);
          
            return GetVendorCreditList(vendorcreditCardCreditXmlList);
            #endregion
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


    }
}
