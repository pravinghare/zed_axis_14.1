// ===============================================================================
// 
// Sale3sOrderList.cs
//
// This file contains the implementations of the QuickBooks Customer request methods and
// Properties.
//
// Developed By : K.Gouraw
// Date : 30/02/2009
// Modified By : Modify by K. Gouraw
// Date : 30/03/2009
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    public class VendorList : BaseVendorList
    {
        #region Public Properties
        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }


        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }


        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        public string Addr1
        {
            get { return m_VendorAddress1; }
            set { m_VendorAddress1 = value; }
        }

        public string Addr2
        {
            get { return m_VendorAddress2; }
            set { m_VendorAddress2 = value; }
        }

        public string Addr3
        {
            get { return m_VendorAddress3; }
            set { m_VendorAddress3 = value; }
        }

        public string Addr4
        {
            get { return m_VendorAddress4; }
            set { m_VendorAddress4 = value; }
        }

        public string Addr5
        {
            get { return m_VendorAddress5; }
            set { m_VendorAddress5 = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }


        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }


        public string Note
        {
            get { return m_Note; }
            set { m_Note = value; }
        }


        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string AltPhone
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }

        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string AltContact
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public string NameOnCheck
        {
            get { return m_NameOnCheck; }
            set { m_NameOnCheck = value; }
        }

        public string AccountNumber
        {
            get { return m_AccountNumber; }
            set { m_AccountNumber = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string VendorTypeFullName
        {
            get { return m_VendorTypeFullName; }
            set { m_VendorTypeFullName = value; }
        }

        public string TermFullName
        {
            get { return m_TermFullName; }
            set { m_TermFullName = value; }
        }

        public string CreditLimit
        {
            get { return m_CreditLimit; }
            set { m_CreditLimit = value; }
        }

        public string VendorTaxIndent
        {
            get { return m_VendorTaxIndent; }
            set { m_VendorTaxIndent = value; }
        }

        public string IsVendorEligibleFor1099
        {
            get { return m_IsVendorEligibleFor1099; }
            set { m_IsVendorEligibleFor1099 = value; }
        }

        public string Balance
        {
            get { return m_Balance; }
            set { m_Balance = value; }
        }

        public string BillingRateFullName
        {
            get { return m_BillingRateFullName; }
            set { m_BillingRateFullName = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        public string PrefillAccountRefFullName
        {
            get { return m_PrefillAccountRefFullName; }
            set { m_PrefillAccountRefFullName = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public string OwnerID
        {
            get { return m_OwnerID; }
            set { m_OwnerID = value; }
        }

        //public string DataExtName
        //{
        //    get { return m_DataExtName; }
        //    set { m_DataExtName = value; }
        //}

        public string DataExtType
        {
            get { return m_DataExtType; }
            set { m_DataExtType = value; }
        }

        //Axis 8.0
        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];

        #endregion
    }

    public class QBVendorListCollection : Collection<VendorList>
    {
        #region public Method

        string XmlFileData = string.Empty;

    
        /// <summary>
        /// this method returns all Vendors in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllVendorList(string QBFileName)
        {
            string vendorXmlList = QBCommonUtilities.GetListFromQuickBook("VendorQueryRq", QBFileName);

            List<string> vendorList = new List<string>();
            List<string> vendorList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(vendorXmlList);

            XmlFileData = vendorXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList VendorNodeList = outputXMLDoc.GetElementsByTagName(QBVendorList.VendorRet.ToString());

            foreach (XmlNode vendNodes in VendorNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in vendNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBVendorList.Name.ToString()))
                        {
                            if (!vendorList.Contains(node.InnerText))
                            {
                                vendorList.Add(node.InnerText);
                                string name = node.InnerText + "     :     Vendor";
                                vendorList1.Add(name);
                            }
                        }
                    }
                }
            }
            CommonUtilities.GetInstance().VendorListWithType = vendorList1;
			
			//463
            XmlFileData = null;
            outputXMLDoc = null;
            vendorXmlList = null;
            VendorNodeList = null;
            return vendorList;

        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VendorList> PopulateVendorList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string VendorXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBookByDate("VendorQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("VendorQueryRq", QBFileName, FromDate, ToDate);
            }           
            VendorList VendorList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfVendor = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (VendorXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfVendor.LoadXml(VendorXmlList);
                XmlFileData = VendorXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList VendorNodeList = outputXMLDocOfVendor.GetElementsByTagName(QBVendorList.VendorRet.ToString());

                foreach (XmlNode VendorNodes in VendorNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        VendorList = new VendorList();
                        foreach (XmlNode node in VendorNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBVendorList.ListID.ToString()))
                            {
                                VendorList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBVendorList.Name.ToString()))
                            {
                                VendorList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBVendorList.IsActive.ToString()))
                            {
                                VendorList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBVendorList.CompanyName.ToString()))
                            {
                                VendorList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBVendorList.Salutation.ToString()))
                            {
                                VendorList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBVendorList.FirstName.ToString()))
                            {
                                VendorList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBVendorList.MiddleName.ToString()))
                            {
                                VendorList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBVendorList.LastName.ToString()))
                            {
                                VendorList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBVendorList.VendorAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.Addr1.ToString()))
                                        VendorList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr2.ToString()))
                                        VendorList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr3.ToString()))
                                        VendorList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr4.ToString()))
                                        VendorList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr5.ToString()))
                                        VendorList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBVendorList.City.ToString()))
                                        VendorList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.State.ToString()))
                                        VendorList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.PostalCode.ToString()))
                                        VendorList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Country.ToString()))
                                        VendorList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Note.ToString()))
                                        VendorList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBVendorList.Phone.ToString()))
                            {
                                VendorList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBVendorList.AltPhone.ToString()))
                            {
                                VendorList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBVendorList.Fax.ToString()))
                            {
                                VendorList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBVendorList.Email.ToString()))
                            {
                                VendorList.Email = node.InnerText;
                            }



                            //Checking Contact
                            if (node.Name.Equals(QBVendorList.Contact.ToString()))
                            {
                                VendorList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBVendorList.AltContact.ToString()))
                            {
                                VendorList.AltContact = node.InnerText;
                            }

                            //Checking NameOnCheck
                            if (node.Name.Equals(QBVendorList.NameOnCheck.ToString()))
                            {
                                VendorList.NameOnCheck = node.InnerText;
                            }

                            //Checking AccountNumber
                            if (node.Name.Equals(QBVendorList.AccountNumber.ToString()))
                            {
                                VendorList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBVendorList.Notes.ToString()))
                            {
                                VendorList.Notes = node.InnerText;
                            }


                            //Checking VendorTypeRef
                            if (node.Name.Equals(QBVendorList.VendorTypeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.VendorTypeFullName = childNode.InnerText;
                                }
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBVendorList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.TermFullName = childNode.InnerText;
                                }
                            }

                            //Checking CreditLimit
                            if (node.Name.Equals(QBVendorList.CreditLimit.ToString()))
                            {
                                VendorList.CreditLimit = node.InnerText;
                            }
                                                        

                            //Bug No.396
                            if (node.Name.Equals(QBVendorList.VendorTaxIdent.ToString()))
                            {
                                VendorList.VendorTaxIndent = node.InnerText;
                            }
                            //
                            //Checking IsVendorEligibleFor1099
                            if (node.Name.Equals(QBVendorList.IsVendorEligibleFor1099.ToString()))
                            {
                                VendorList.IsVendorEligibleFor1099 = node.InnerText;
                            }

                            //Checking Balance
                            if (node.Name.Equals(QBVendorList.Balance.ToString()))
                            {
                                VendorList.Balance = node.InnerText;
                            }

                            //Checking BillingRateRef
                            if (node.Name.Equals(QBVendorList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        //VendorList.TermFullName = childNode.InnerText;
                                        //Bug No.396
                                        VendorList.BillingRateFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBVendorList.ExternalGUID.ToString()))
                            {
                                VendorList.ExternalGUID = node.InnerText;
                            }

                            //Checking PrefillAccountRef
                            if (node.Name.Equals(QBVendorList.PrefillAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.PrefillAccountRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking CurrencyRef
                            if (node.Name.Equals(QBVendorList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }
                                                      

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBVendorList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    VendorList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        VendorList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }


                        }

                        this.Add(VendorList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfVendor.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter is selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<VendorList> PopulateVendorList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Vendor List from QuickBooks.

            //Getting item list from QuickBooks.
            string VendorXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBook("VendorQueryRq", QBFileName);
            }
            else
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("VendorQueryRq", QBFileName);
            }
            VendorList VendorList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfVendor = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (VendorXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfVendor.LoadXml(VendorXmlList);
                XmlFileData = VendorXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList VendorNodeList = outputXMLDocOfVendor.GetElementsByTagName(QBVendorList.VendorRet.ToString());

                foreach (XmlNode VendorNodes in VendorNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        VendorList = new VendorList();
                        foreach (XmlNode node in VendorNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBVendorList.ListID.ToString()))
                            {
                                VendorList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBVendorList.Name.ToString()))
                            {
                                VendorList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBVendorList.IsActive.ToString()))
                            {
                                VendorList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBVendorList.CompanyName.ToString()))
                            {
                                VendorList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBVendorList.Salutation.ToString()))
                            {
                                VendorList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBVendorList.FirstName.ToString()))
                            {
                                VendorList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBVendorList.MiddleName.ToString()))
                            {
                                VendorList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBVendorList.LastName.ToString()))
                            {
                                VendorList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBVendorList.VendorAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.Addr1.ToString()))
                                        VendorList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr2.ToString()))
                                        VendorList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr3.ToString()))
                                        VendorList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr4.ToString()))
                                        VendorList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr5.ToString()))
                                        VendorList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBVendorList.City.ToString()))
                                        VendorList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.State.ToString()))
                                        VendorList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.PostalCode.ToString()))
                                        VendorList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Country.ToString()))
                                        VendorList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Note.ToString()))
                                        VendorList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBVendorList.Phone.ToString()))
                            {
                                VendorList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBVendorList.AltPhone.ToString()))
                            {
                                VendorList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBVendorList.Fax.ToString()))
                            {
                                VendorList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBVendorList.Email.ToString()))
                            {
                                VendorList.Email = node.InnerText;
                            }



                            //Checking Contact
                            if (node.Name.Equals(QBVendorList.Contact.ToString()))
                            {
                                VendorList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBVendorList.AltContact.ToString()))
                            {
                                VendorList.AltContact = node.InnerText;
                            }

                            //Checking NameOnCheck
                            if (node.Name.Equals(QBVendorList.NameOnCheck.ToString()))
                            {
                                VendorList.NameOnCheck = node.InnerText;
                            }

                            //Checking AccountNumber
                            if (node.Name.Equals(QBVendorList.AccountNumber.ToString()))
                            {
                                VendorList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBVendorList.Notes.ToString()))
                            {
                                VendorList.Notes = node.InnerText;
                            }


                            //Checking VendorTypeRef
                            if (node.Name.Equals(QBVendorList.VendorTypeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.VendorTypeFullName = childNode.InnerText;
                                }
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBVendorList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.TermFullName = childNode.InnerText;
                                }
                            }

                            //Checking CreditLimit
                            if (node.Name.Equals(QBVendorList.CreditLimit.ToString()))
                            {
                                VendorList.CreditLimit = node.InnerText;
                            }

                            
                            //Bug No. 396
                            if (node.Name.Equals(QBVendorList.VendorTaxIdent.ToString()))
                            {
                                VendorList.VendorTaxIndent = node.InnerText;
                            }
                            //

                            //Checking IsVendorEligibleFor1099
                            if (node.Name.Equals(QBVendorList.IsVendorEligibleFor1099.ToString()))
                            {
                                VendorList.IsVendorEligibleFor1099 = node.InnerText;
                            }

                            //Checking Balance
                            if (node.Name.Equals(QBVendorList.Balance.ToString()))
                            {
                                VendorList.Balance = node.InnerText;
                            }

                            //Checking BillingRateRef
                            if (node.Name.Equals(QBVendorList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        //Bug No.396
                                        VendorList.BillingRateFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBVendorList.ExternalGUID.ToString()))
                            {
                                VendorList.ExternalGUID = node.InnerText;
                            }

                            //Checking PrefillAccountRef
                            if (node.Name.Equals(QBVendorList.PrefillAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.PrefillAccountRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking CurrencyRef
                            if (node.Name.Equals(QBVendorList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }
                                                        

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    VendorList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        VendorList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }


                        }

                        this.Add(VendorList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfVendor.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Vendor List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VendorList> PopulateVendorList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string VendorXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBookByDate("VendorQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                VendorXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("VendorQueryRq", QBFileName, FromDate, ToDate);
            }
            VendorList VendorList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfVendor = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (VendorXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfVendor.LoadXml(VendorXmlList);
                XmlFileData = VendorXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList VendorNodeList = outputXMLDocOfVendor.GetElementsByTagName(QBVendorList.VendorRet.ToString());

                foreach (XmlNode VendorNodes in VendorNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        VendorList = new VendorList();
                        foreach (XmlNode node in VendorNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBVendorList.ListID.ToString()))
                            {
                                VendorList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBVendorList.Name.ToString()))
                            {
                                VendorList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBVendorList.IsActive.ToString()))
                            {
                                VendorList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBVendorList.CompanyName.ToString()))
                            {
                                VendorList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBVendorList.Salutation.ToString()))
                            {
                                VendorList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBVendorList.FirstName.ToString()))
                            {
                                VendorList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBVendorList.MiddleName.ToString()))
                            {
                                VendorList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBVendorList.LastName.ToString()))
                            {
                                VendorList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBVendorList.VendorAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.Addr1.ToString()))
                                        VendorList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr2.ToString()))
                                        VendorList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr3.ToString()))
                                        VendorList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr4.ToString()))
                                        VendorList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Addr5.ToString()))
                                        VendorList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBVendorList.City.ToString()))
                                        VendorList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.State.ToString()))
                                        VendorList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.PostalCode.ToString()))
                                        VendorList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Country.ToString()))
                                        VendorList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBVendorList.Note.ToString()))
                                        VendorList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBVendorList.Phone.ToString()))
                            {
                                VendorList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBVendorList.AltPhone.ToString()))
                            {
                                VendorList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBVendorList.Fax.ToString()))
                            {
                                VendorList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBVendorList.Email.ToString()))
                            {
                                VendorList.Email = node.InnerText;
                            }

                            

                            //Checking Contact
                            if (node.Name.Equals(QBVendorList.Contact.ToString()))
                            {
                                VendorList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBVendorList.AltContact.ToString()))
                            {
                                VendorList.AltContact = node.InnerText;
                            }

                            //Checking NameOnCheck
                            if (node.Name.Equals(QBVendorList.NameOnCheck.ToString()))
                            {
                                VendorList.NameOnCheck = node.InnerText;
                            }

                            //Checking AccountNumber
                            if (node.Name.Equals(QBVendorList.AccountNumber.ToString()))
                            {
                                VendorList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBVendorList.Notes.ToString()))
                            {
                                VendorList.Notes = node.InnerText;
                            }


                            //Checking VendorTypeRef
                            if (node.Name.Equals(QBVendorList.VendorTypeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.VendorTypeFullName = childNode.InnerText;
                                }
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBVendorList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.TermFullName = childNode.InnerText;
                                }
                            }

                            //Checking CreditLimit
                            if (node.Name.Equals(QBVendorList.CreditLimit.ToString()))
                            {
                                VendorList.CreditLimit = node.InnerText;
                            }

                            //Checking VendorTaxIndent
                            if (node.Name.Equals(QBVendorList.VendorTaxIdent.ToString()))
                            {
                                VendorList.VendorTaxIndent = node.InnerText;
                            }


                            //Checking IsVendorEligibleFor1099
                            if (node.Name.Equals(QBVendorList.IsVendorEligibleFor1099.ToString()))
                            {
                                VendorList.IsVendorEligibleFor1099 = node.InnerText;
                            }

                            //Checking Balance
                            if (node.Name.Equals(QBVendorList.Balance.ToString()))
                            {
                                VendorList.Balance = node.InnerText;
                            }

                            //Checking BillingRateRef
                            if (node.Name.Equals(QBVendorList.BillingRateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        //VendorList.TermFullName = childNode.InnerText;
                                        //Bug No.396
                                        VendorList.BillingRateFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBVendorList.ExternalGUID.ToString()))
                            {
                                VendorList.ExternalGUID = node.InnerText;
                            }

                            //Checking PrefillAccountRef
                            if (node.Name.Equals(QBVendorList.PrefillAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.PrefillAccountRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking CurrencyRef
                            if (node.Name.Equals(QBVendorList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVendorList.FullName.ToString()))
                                        VendorList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }
                                                      

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBVendorList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    VendorList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        VendorList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                          

                        }

                        this.Add(VendorList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfVendor.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

       

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
        #endregion


    }
 }