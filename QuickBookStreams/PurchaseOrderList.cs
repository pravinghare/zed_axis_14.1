// ===============================================================================
// 
// Sale3sOrderList.cs
//
// This file contains the implementations of the QuickBooks Sales Order request methods and
// Properties.
//
// Developed By : K.Gouraw
// Date : 17/03/2009
// Modified By : Modify by K. Gouraw
// Date : 14/05/2009
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;


namespace Streams
{
    public class QBPurchaseOrderList:BasePurchaseOrderList
    {

        public QBPurchaseOrderList()
        {
            m_QBItemsDetails = new Collection<QBItemDetails>();
        }
       #region Public Properties

        /// <summary>
        /// Get and Set Refnumber of QuickBooks purchase order.
        /// </summary>
        public string RefNumber
        {
            get
            {
                return m_RefNumber;
            }
            set
            {
                if (value.Length > 30)                                    // For bug 595 instead of 30 characters we used 4 char
                {
                    m_RefNumber = value.Remove(30, (value.Length - 30));
                }
                else
                {
                    m_RefNumber = value.Trim();
                }
            }
        }
        /// <summary>
        /// Get or Set VenderListID of QuickBooks purchase order.
        /// </summary>

        public string TxnID                                               //for bug 616
        {
            get { return m_TxnID; }
            set
            {
                if (value.Length > 40)
                {
                    m_TxnID = value.Remove(40, (value.Length - 40));
                }
                else
                {
                    m_TxnID = value.Trim();
                }
            }
        }
        //For POLineRet
        public string[] TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        //For POeLineGroupRet
        public string[] GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

    


        /// <summary>
        /// Get or Set TxnNumber of QuickBooks purchase order.
        /// </summary>

        public string TxnNumber                      //for bug 616
        {
            get { return m_TxnNumber; }
            set
            {
                m_TxnNumber = value.Trim();
            }
        }

        public string VendorListID
        {
            get { return m_VenderListID; }
            set { m_VenderListID = value; }
        }

        public string VendorRefFullName
        {
            get { return m_VendorRefFullName; }
            set { m_VendorRefFullName = value; }
        }
        public string ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }
        public string InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }
        public string ShipToEntityRefFullName
        {
            get { return m_ShipToEntityRefFullName; }
            set { m_ShipToEntityRefFullName = value; }
        }
        public string TemplateRefFullName
        {
            get { return m_TemplateRefFullName; }
            set { m_TemplateRefFullName = value; }
        }
        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        public string VendorAddress1
        {
            get { return m_VendorAddress1; }
            set { m_VendorAddress1 = value; }
        }
        public string VendorAddress2
        {
            get { return m_VendorAddress2; }
            set { m_VendorAddress2 = value; }
        }
        public string VendorAddress3
        {
            get { return m_VendorAddress3; }
            set { m_VendorAddress3 = value; }
        }
        public string VendorCity
        {
            get { return m_VendorCity; }
            set { m_VendorCity = value; }
        }
        public string VendorState
        {
            get { return m_VendorState; }
            set { m_VendorState = value; }
        }
        public string VendorPostalCode
        {
            get { return m_VendorPostalCode; }
            set { m_VendorPostalCode = value; }
        }
        public string VendorCountry
        {
            get { return m_VendorCountry; }
            set { m_VendorCountry = value; }
        }
        public string VendorNote
        {
            get { return m_VendorNote; }
            set { m_VendorNote = value; }
        }

        public string ShipAddress1
        {
            get { return m_ShipAddress1; }
            set { m_ShipAddress1 = value; }
        }
        public string ShipAddress2
        {
            get { return m_ShipAddress2; }
            set { m_ShipAddress2 = value; }
        }
        public string ShipAddress3
        {
            get { return m_ShipAddress3; }
            set { m_ShipAddress3 = value; }
        }
        public string ShipCity
        {
            get { return m_ShipCity; }
            set { m_ShipCity = value; }
        }
        public string ShipState
        {
            get { return m_ShipState; }
            set { m_ShipState = value; }
        }
        public string ShipPostalCode
        {
            get { return m_ShipPostalCode; }
            set { m_ShipPostalCode = value; }
        }
        public string ShipCountry
        {
            get { return m_ShipCountry; }
            set { m_ShipCountry = value; }
        }
        public string ShipNote
        {
            get { return m_ShipNote; }
            set { m_ShipNote = value; }
        }
        public string TermsRefFullName
        {
            get { return m_TermsRefFullName; }
            set { m_TermsRefFullName = value; }
        }
        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }
        public string ShipMethodRefFullName
        {
            get { return m_ShipMethodRefFullName; }
            set { m_ShipMethodRefFullName = value; }
        }
        //Get or Set TotalAmount value.
        public string TotalAmount
        {
            get
            {
                return m_TotalAmount;
            }
            set
            {

                m_TotalAmount = value;
            }
        }
        //Get or Set CurrencyRefFullName value.
        public string CurrencyRefFullName
        {
            get
            {
                return m_CurrencyRefFullName;
            }
            set
            {

                m_CurrencyRefFullName = value;
            }
        }
        //Get or Set ExchangeRate value.
        public string ExchangeRate
        {
            get
            {
                return m_ExchangeRate;
            }
            set
            {

                m_ExchangeRate = value;
            }
        }
        //Get or Set TotalAmountInHomeCurrency value.
        public string TotalAmountInHomeCurrency
        {
            get
            {
                return m_TotalAmountInHomeCurrency;
            }
            set
            {

                m_TotalAmountInHomeCurrency = value;
            }
        }
        //Get or Set IsManuallyClosed value.

        //Get or Set IsFullyInvoiced value.
        public string IsFullyReceived
        {
            get
            {
                return m_IsFullyReceived;
            }
            set
            {

                m_IsFullyReceived = value;
            }
        }
        //Get or Set Memo value.
        public string Memo
        {
            get
            {
                return m_Memo;
            }
            set
            {

                m_Memo = value;
            }
        }
        //Get or Set VendorMsgRefFullName value.
        public string VendorMsgFullName
        {
            get
            {
                return m_VendorMsgRefFullName;
            }
            set
            {

                m_VendorMsgRefFullName = value;
            }
        }

        //Get or Set IsToBePrinted value.
        public string IsToBePrinted
        {
            get
            {
                return m_IsToBePrinted;
            }
            set
            {

                m_IsToBePrinted = value;
            }
        }
        //Get or Set IsToBeEmailed value.
        public string IsToBeEmailed
        {
            get
            {
                return m_IsToBeEmailed;
            }
            set
            {

                m_IsToBeEmailed = value;
            }
        }
        //Get or Set Other1 value.
        public string Other1
        {
            get
            {
                return m_Other1;
            }
            set
            {

                m_Other1 = value;
            }
        }
        //Get or Set Other2 value.
        public string Other2
        {
            get
            {
                return m_Other2;
            }
            set
            {

                m_Other2 = value;
            }
        }


        /// <summary>
        /// Get or Set Expected date of QuickBooks purchase order.
        /// </summary>
        public string ExpectedDate
        {
            get { return m_ExpectedDate; }
            set { m_ExpectedDate = value; }
        }
        public Collection<QBItemDetails> QBItemsDetails
        {
            get { return m_QBItemsDetails; }
            set { m_QBItemsDetails = value; }

        }
       
        public string IsManuallyClosed
        {
            get
            {
                return m_IsManuallyClosed;
            }
            set
            {
                m_IsManuallyClosed = value;
            }
        }
        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }
        public string LinkedTxn
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }
        public string[] ManufacturerPartNumber
        {
            get { return m_ManufacturerPartNumber; }
            set { m_ManufacturerPartNumber = value; }
        }
        public string[] ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }
        public string[] ReceivedQuantity
        {
            get { return m_ReceivedQuantity; }
            set { m_ReceivedQuantity = value; }
        }
        public string[] LineClass
        {
            get { return m_LineClass; }
            set { m_LineClass = value; }
        }
       
        public string[] LineOther1
        {
            get { return m_LineOther1; }
            set { m_LineOther1 = value; }
        }
        public string[] LineOther2
        {
            get { return m_LineOther2; }
            set { m_LineOther2 = value; }
        }

        public string[] LineIsManuallyClosed
        {
            get { return m_LineIsManuallyClosed; }
            set { m_LineIsManuallyClosed = value; }
        }

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];
      #endregion

    }


 
    /// <summary>
    /// Collection class used for getting Purchase Order List from QuickBooks.
    /// </summary>
    public class QBPurchaseOrderCollection : Collection<QBPurchaseOrderList>
    {
        string XmlFileData = string.Empty;

        #region private method
        /// <summary>
        /// Assigning the xml value to the class objects
        /// </summary>
        /// <param name="purchaseOrderXmlList"></param>
        /// <returns></returns>
        private Collection<QBPurchaseOrderList> GetPurchaseOrderList(string purchaseOrderXmlList)
        {
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfPurchaseOrder = new XmlDocument();

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            QBItemDetails itemList = null;
            if (purchaseOrderXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfPurchaseOrder.LoadXml(purchaseOrderXmlList);
                XmlFileData = purchaseOrderXmlList;
                              
                //Getting Sales Order values from QuickBooks response.
                XmlNodeList PurchaseOrderNodeList = outputXMLDocOfPurchaseOrder.GetElementsByTagName(QBXmlPurchaseOrderList.PurchaseOrderRet.ToString());

                foreach (XmlNode PurchaseOrderNodes in PurchaseOrderNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        int j = 0;
                        QBPurchaseOrderList purchaseOrder = new QBPurchaseOrderList();
                        purchaseOrder.GroupTxnLineID = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.LineClass = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.LineIsManuallyClosed = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.LineOther1 = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.LineOther2 = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.ManufacturerPartNumber = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.ReceivedQuantity = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.ServiceDate = new string[PurchaseOrderNodes.ChildNodes.Count];
                        purchaseOrder.TxnLineID = new string[PurchaseOrderNodes.ChildNodes.Count];
                        foreach (XmlNode node in PurchaseOrderNodes.ChildNodes)
                        {
                            //Assinging values to class.
                            if (node.Name.Equals(QBXmlPurchaseOrderList.RefNumber.ToString()))
                                purchaseOrder.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TxnID.ToString()))
                                purchaseOrder.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TxnNumber.ToString()))
                                purchaseOrder.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TxnDate.ToString()))
                                purchaseOrder.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.DueDate.ToString()))
                                purchaseOrder.DueDate = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TotalAmount.ToString()))
                                purchaseOrder.TotalAmount = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.ExchangeRate.ToString()))
                                purchaseOrder.ExchangeRate = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TotalAmountInHomeCurrency.ToString()))
                                purchaseOrder.TotalAmountInHomeCurrency = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.IsManuallyClosed.ToString()))
                                purchaseOrder.IsManuallyClosed = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.IsFullyReceived.ToString()))
                                purchaseOrder.IsFullyReceived = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.Memo.ToString()))
                                purchaseOrder.Memo = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.VendorMsg.ToString()))
                                purchaseOrder.VendorMsgFullName = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.IsToBePrinted.ToString()))
                                purchaseOrder.IsToBePrinted = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.IsToBeEmailed.ToString()))
                                purchaseOrder.IsToBeEmailed = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.Other1.ToString()))
                                purchaseOrder.Other1 = node.InnerText;

                            if (node.Name.Equals(QBXmlPurchaseOrderList.Other2.ToString()))
                                purchaseOrder.Other2 = node.InnerText;
                           

                            if (node.Name.Equals(QBXmlPurchaseOrderList.VendorAddress.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr1.ToString()))
                                        purchaseOrder.VendorAddress1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr2.ToString()))
                                        purchaseOrder.VendorAddress2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr3.ToString()))
                                        purchaseOrder.VendorAddress3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.City.ToString()))
                                        purchaseOrder.VendorCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.State.ToString()))
                                        purchaseOrder.VendorState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Country.ToString()))
                                        purchaseOrder.VendorCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.PostalCode.ToString()))
                                        purchaseOrder.VendorPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Note.ToString()))
                                        purchaseOrder.VendorNote = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlPurchaseOrderList.ShipAddress.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr1.ToString()))
                                        purchaseOrder.ShipAddress1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr2.ToString()))
                                        purchaseOrder.ShipAddress2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Addr3.ToString()))
                                        purchaseOrder.ShipAddress3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.City.ToString()))
                                        purchaseOrder.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.State.ToString()))
                                        purchaseOrder.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Country.ToString()))
                                        purchaseOrder.ShipCountry = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.PostalCode.ToString()))
                                        purchaseOrder.ShipPostalCode = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlPurchaseOrderList.VendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.ListID.ToString()))
                                        purchaseOrder.VendorListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.VendorRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlPurchaseOrderList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.TermsRefFullName = childNode.InnerText;
                                }
                            }


                            if (node.Name.Equals(QBXmlPurchaseOrderList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.ClassRefFullName = childNode.InnerText;
                                }
                            }
                           

                            if (node.Name.Equals(QBXmlPurchaseOrderList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.InventorySiteRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlPurchaseOrderList.ShipToEntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.ShipToEntityRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlPurchaseOrderList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.TemplateRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlPurchaseOrderList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.CurrencyRefFullName = childNode.InnerText;
                                }
                            }


                        if (node.Name.Equals(QBXmlPurchaseOrderList.ExpectedDate.ToString()))
                        {
                            try
                            {
                                DateTime date = Convert.ToDateTime(node.InnerText);
                                purchaseOrder.ExpectedDate = date.ToString("yyyy-MM-dd");
                            }
                            catch
                            {
                                purchaseOrder.ExpectedDate = node.InnerText;
                            }
                        }

                            if (node.Name.Equals(QBXmlPurchaseOrderList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                        purchaseOrder.ShipMethodRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking FOB
                            if (node.Name.Equals(QBXmlPurchaseOrderList.FOB.ToString()))
                            {
                                purchaseOrder.FOB = node.InnerText;
                            }

                            //Checking LinkedTxnID
                            if (node.Name.Equals(QBXmlPurchaseOrderList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.TxnID.ToString()))
                                        purchaseOrder.LinkedTxn = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlPurchaseOrderList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    purchaseOrder.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText + "|" + purchaseOrder.TxnLineID[count];
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        purchaseOrder.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText + "|" + purchaseOrder.TxnLineID[count];
                                }

                                i++;
                            }

                            //Checking Purchase Order Line ret values.
                            if (node.Name.Equals(QBXmlPurchaseOrderList.PurchaseOrderLineRet.ToString()))
                            {
                                purchaseOrder.GroupTxnLineID[count1] = null;
                                count1++;
                                itemList = new QBItemDetails();
                                j = 0;

                                checkNullEntries(node, ref purchaseOrder, ref itemList, count);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            purchaseOrder.TxnLineID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            purchaseOrder.TxnLineID[count] = childNode.InnerText;
                                    }
                                    //Axis 682 

                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.ItemRef.ToString()))
                                        {
                                            foreach (XmlNode childsNode in childNode.ChildNodes)
                                            {
                                                if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                    itemList.ItemFullName.Add(childsNode.InnerText);
                                            }
                                        }
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.Desc.ToString()))
                                        {
                                            itemList.Desc.Add(childNode.InnerText);
                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end

                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            itemList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }

                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.UnitOfMeasure.ToString()))
                                    {
                                        itemList.UOM.Add(childNode.InnerText);

                                    }
                                    //Axis 682 
                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.OverrideUOMSetRef.ToString()))
                                        {
                                            foreach (XmlNode childsNode in childNode.ChildNodes)
                                            {
                                                if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                    itemList.OverrideUOMFullName.Add(childsNode.InnerText);
                                            }
                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end
                                    //Axis 682 
                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.CustomerRef.ToString()))
                                        {
                                            foreach (XmlNode childsNode in childNode.ChildNodes)
                                            {
                                                if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                    itemList.CustomerRefFullName.Add(childsNode.InnerText);
                                            }
                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            itemList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }
                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            itemList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }
                                    }

                                    //ManufacturerPartNumber
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.ManufacturerPartNumber.ToString()))
                                    {
                                        itemList.ManufacturerPartNumber.Add(childNode.InnerText);
                                    }
                                    //Service Date
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime date = Convert.ToDateTime(node.InnerText);
                                            itemList.ServiceDate.Add(date.ToString("yyyy-MM-dd"));
                                        }
                                        catch
                                        {
                                            itemList.ServiceDate.Add(childNode.InnerText);

                                        }
                                    }
                                    //Received Quantity

                                    //Axis 682 
                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.ReceivedQuantity.ToString()))
                                        {
                                            itemList.ReceivedQuantity.Add(childNode.InnerText);
                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end

                                    //LineOther1
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Other1.ToString()))
                                    {
                                        itemList.LineOther1.Add(childNode.InnerText);

                                    }
                                    //LineOther2
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Other2.ToString()))
                                    {
                                        itemList.LineOther2.Add(childNode.InnerText);

                                    }
                                    //LineIsManuallyClosed
                                    //Axis 682 
                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.IsManuallyClosed.ToString()))
                                        {
                                            itemList.LineIsManuallyClosed.Add(childNode.InnerText);

                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end
                                    //LineClass

                                    //Axis 682 
                                    foreach (XmlNode childsNode1 in node.ChildNodes)
                                    {
                                        //Axis 682 end
                                        if (childNode.Name.Equals(QBXmlPurchaseOrderList.ClassRef.ToString()))
                                        {
                                            foreach (XmlNode childsNode in childNode.ChildNodes)
                                            {
                                                if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                    itemList.LineClass.Add(childsNode.InnerText);

                                            }
                                        }
                                        //Axis 682 
                                    }
                                    //Axis 682 end

                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            itemList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + purchaseOrder.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                itemList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + purchaseOrder.TxnLineID[count]);
                                        }

                                        j++;
                                    }
                                }

                                count++;
                                purchaseOrder.QBItemsDetails.Add(itemList);
                            }


                            //Checking Purchase Order Line Group ret values.
                            if (node.Name.Equals(QBXmlPurchaseOrderList.PurchaseOrderLineGroupRet.ToString()))
                            {
                                purchaseOrder.TxnLineID[count] = null;
                                count++;
                                itemList = new QBItemDetails();
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking GroupTxnLineID
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            purchaseOrder.GroupTxnLineID[count1] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            purchaseOrder.GroupTxnLineID[count1] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                itemList.GroupLineItemFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Desc.ToString()))
                                    {
                                        itemList.GroupLineDesc.Add(childNode.InnerText);

                                    }

                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            itemList.GroupLineQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }

                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.UnitOfMeasure.ToString()))
                                    {
                                        itemList.GroupLineUOM.Add(childNode.InnerText);

                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                itemList.GroupLineOverrideUOM.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                itemList.GroupLineCustomerRef.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.TotalAmount.ToString()))
                                    {
                                        try
                                        {
                                            itemList.GroupLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }
                                    }

                                    //Checking Purchase Order Line ret values.
                                    if (childNode.Name.Equals(QBXmlPurchaseOrderList.PurchaseOrderLineRet.ToString()))
                                    {
                                        purchaseOrder.TxnLineID[count] = null;
                                        count++;
                                        int count2 = 0;
                                        checkGroupLineNullEntries(node, ref itemList);

                                        foreach (XmlNode childNode1 in childNode.ChildNodes)
                                        {

                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode childsNode1 in childNode1.ChildNodes)
                                                {
                                                    if (childsNode1.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                        itemList.ItemFullName.Add(childNode1.InnerText);
                                                }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Desc.ToString()))
                                            {
                                                itemList.Desc.Add(childNode1.InnerText);

                                            }

                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    itemList.Quantity.Add(Convert.ToDecimal(childNode1.InnerText));
                                                }
                                                catch { }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.UnitOfMeasure.ToString()))
                                            {
                                                itemList.UOM.Add(childNode1.InnerText);

                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode childsNode in childNode1.ChildNodes)
                                                {
                                                    if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                        itemList.OverrideUOMFullName.Add(childsNode.InnerText);
                                                }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode childsNode in childNode1.ChildNodes)
                                                {
                                                    if (childsNode.Name.Equals(QBXmlPurchaseOrderList.FullName.ToString()))
                                                        itemList.CustomerRefFullName.Add(childsNode.InnerText);
                                                }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    itemList.Rate.Add( Convert.ToDecimal(childNode1.InnerText));
                                                }
                                                catch { }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    itemList.Amount.Add( Convert.ToDecimal(childNode1.InnerText));
                                                }
                                                catch { }
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Other1.ToString()))
                                            {
                                                itemList.LineOther1.Add( childNode1.InnerText);
                                            }
                                            if (childNode1.Name.Equals(QBXmlPurchaseOrderList.Other2.ToString()))
                                            {
                                                itemList.LineOther2.Add(childNode1.InnerText);
                                            }
                                        }
                                        count2++;
                                        purchaseOrder.QBItemsDetails.Add(itemList);
                                    }
                                }
                                count1++;
                                purchaseOrder.QBItemsDetails.Add(itemList);
                            }


                        }

                        this.Add(purchaseOrder);
                    }
                    else
                    { return null; }

                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfPurchaseOrder.RemoveAll();
               
            }

            return this;
        }

        private void checkGroupLineNullEntries(XmlNode node, ref QBItemDetails itemList)
        {
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ItemRef.ToString()) == null)
            {
                itemList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Desc.ToString()) == null)
            {
                itemList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Quantity.ToString()) == null)
            {
                //itemList.Quantity.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.UnitOfMeasure.ToString()) == null)
            {
                itemList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.OverrideUOMSetRef.ToString()) == null)
            {
                itemList.OverrideUOMFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.CustomerRef.ToString()) == null)
            {
                itemList.CustomerRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Rate.ToString()) == null)
            {
                //itemList.Rate.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Amount.ToString()) == null)
            {
                //itemList.Amount.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Other1.ToString()) == null)
            {
                itemList.LineOther1.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Other2.ToString()) == null)
            {
                itemList.LineOther2.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref QBPurchaseOrderList purchaseOrder, ref QBItemDetails itemList, int count)
        {
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.TxnLineID.ToString()) == null)
            {
                purchaseOrder.TxnLineID[count] = null;
            }

            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ItemRef.ToString()) == null)
            {
                itemList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Desc.ToString()) == null)
            {
                itemList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Quantity.ToString()) == null)
            {
                //itemList.Quantity.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.UnitOfMeasure.ToString()) == null)
            {
                itemList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.OverrideUOMSetRef.ToString()) == null)
            {
                itemList.OverrideUOMFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.CustomerRef.ToString()) == null)
            {
                itemList.CustomerRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Rate.ToString()) == null)
            {
                //itemList.Rate.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Amount.ToString()) == null)
            {
                //itemList.Amount.Add(Convert.ToDecimal(null));
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ManufacturerPartNumber.ToString()) == null)
            {
                itemList.ManufacturerPartNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ServiceDate.ToString()) == null)
            {
                itemList.ServiceDate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ReceivedQuantity.ToString()) == null)
            {
                itemList.ReceivedQuantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Other1.ToString()) == null)
            {
                itemList.LineOther1.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.Other2.ToString()) == null)
            {
                itemList.LineOther2.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.IsManuallyClosed.ToString()) == null)
            {
                itemList.LineIsManuallyClosed.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.ClassRef.ToString()) == null)
            {
                itemList.LineClass.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlPurchaseOrderList.DataExtRet.ToString()) == null)
            {
                itemList.DataExtName.Add(null);
                itemList.DataExtValue.Add(null);
            }
        }
        #endregion

        #region public Method
        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName)
        {
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }

        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Getting item list from QuickBooks.
            //string purchaseOrderXmlList =QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, string ToDate)
        {
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }

        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Getting item list from QuickBooks.
            //string purchaseOrderXmlList =QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate);

            return GetPurchaseOrderList(purchaseOrderXmlList);
        }

        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("PurchaseOrderQueryRq", QBFileName, entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, string FromRefnum, string ToRefNum, List<string> entityFilter)
        {
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromRefnum, ToRefNum,entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, string FromRefnum, string ToRefNum)
        {
            //Getting item list from QuickBooks.
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromRefnum, ToRefNum);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum,entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> PopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            //Getting item list from QuickBooks.
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate,FromRefNum,ToRefNum);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }
        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum,entityFilter);
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum, entityFilter);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }

        public Collection<QBPurchaseOrderList> ModifiedPopulatePurchaseOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            //Getting item list from QuickBooks.
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate,FromRefNum,ToRefNum);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            //string purchaseOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            //string purchaseOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("PurchaseOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            return GetPurchaseOrderList(purchaseOrderXmlList);
        }

        public string GenerateISISFileFormatEDI(string clientID, string filePath, string QBFileName)
        {
            try
            {
                string ISMFilePath = filePath + "\\" + Constants.ISMPurchaseOrderStream + DateTime.Now.ToString(Constants.ISMFileNameString) + Constants.ISMExtension;
                if (!File.Exists(ISMFilePath))
                {
                    FileStream strm = File.Create(ISMFilePath);
                    strm.Close();
                }
                else
                {
                    File.Delete(ISMFilePath);
                    FileStream strm = File.Create(ISMFilePath);
                    strm.Close();
                }
                StreamWriter wrt = new StreamWriter(ISMFilePath, true);
                wrt.WriteLine(Constants.ISMHeader + Constants.ISMDelimiter.ToString() + clientID + Constants.ISMDelimiter.ToString()
                    + Constants.ISMPurchaseOrderStream);
                string ISMVendorFilePaths = ISMFilePath.Replace(Constants.ISMPurchaseOrderStream, Constants.ISMVendorStream);
                string ISMItemFilePath = ISMFilePath.Replace(Constants.ISMPurchaseOrderStream, Constants.ISMItemStream);
                bool itemFlag = true;
                bool vendorFlag = true;
                foreach (QBPurchaseOrderList QbPurchaseOrderlist in this.Items)
                {
                    wrt.WriteLine(Constants.ISMPurchaseOrderOneStream + Constants.ISMDelimiter.ToString() + Constants.ISMActionIndicaterA +
                        Constants.ISMDelimiter.ToString() + QbPurchaseOrderlist.RefNumber + Constants.ISMDelimiter.ToString()
                        + QbPurchaseOrderlist.TxnID + Constants.ISMDelimiter.ToString()
                        + QbPurchaseOrderlist.VendorListID + Constants.ISMDelimiter.ToString() + QbPurchaseOrderlist.ExpectedDate);
                    if (QbPurchaseOrderlist.VendorListID != null)
                    {
                        if (vendorFlag)
                        {
                            StreamWriter wrtcust = new StreamWriter(ISMVendorFilePaths, true);
                            wrtcust.WriteLine(EDI.Constant.Constants.ISMHeader + Constants.ISMDelimiter
                                          + clientID + Constants.ISMDelimiter
                                          + Constants.ISMVendorStream);
                            wrtcust.Close();
                            vendorFlag = false;
                        }
                        StreamReader strReader = new StreamReader(ISMVendorFilePaths, true);
                        string getAllLines = strReader.ReadToEnd();
                        strReader.Close();
                        if (getAllLines != string.Empty)
                        {
                            if (!(getAllLines.Contains(QbPurchaseOrderlist.VendorListID)))
                            {
                                string response = QBCommonUtilities.GetVendorList(QBFileName, QbPurchaseOrderlist.VendorListID);
                            }
                        }
                    }
                    foreach (QBItemDetails QbItem in QbPurchaseOrderlist.QBItemsDetails)
                    {
                        if (itemFlag)
                        {
                            StreamWriter wrtite = new StreamWriter(ISMItemFilePath, true);
                            wrtite.WriteLine(Constants.ISMHeader + Constants.ISMDelimiter
                                          + clientID + Constants.ISMDelimiter
                                          + Constants.ISMItemStream);
                            wrtite.Close();
                            itemFlag = false;
                        }
                        int count = 0;
                        if (QbItem.ItemFullName[count] != null && QbItem.ItemFullName[count] != "NCG" && QbItem.ItemFullName[count] != "NCF")      // For bug 595
                        {
                            StreamReader strReader = new StreamReader(ISMItemFilePath, true);
                            string getAllLines = strReader.ReadToEnd();
                            strReader.Close();
                            if (getAllLines != string.Empty)
                            {
                                string writeline = Constants.ISMPurchaseOrderTwoStream +
                                    Constants.ISMDelimiter.ToString() + string.Empty +
                                    Constants.ISMDelimiter.ToString() + QbPurchaseOrderlist.RefNumber + Constants.ISMDelimiter.ToString() +
                                    ((string.IsNullOrEmpty(QbItem.ItemFullName[count])) ? (string.Empty) : (QbItem.ItemFullName[count])) +
                                    Constants.ISMDelimiter.ToString() +
                                    ((string.IsNullOrEmpty(QbItem.Quantity[count].ToString())) ? (string.Empty) : (QbItem.Quantity[count].ToString()));
                                wrt.WriteLine(writeline);
                                if (!getAllLines.Contains(QbItem.ItemFullName[count]))
                                {
                                    string response = QBCommonUtilities.GetItemList(QBFileName, QbItem.ItemFullName[count]);
                                }
                            }
                        }
                        count++;
                    }
                }
                wrt.Close();          //Closing streamwriter.
                return ISMFilePath;
            }
            catch (Exception ex)
            {
               
                throw new Exception(ex.Message);
            }
        }
       
        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
        #endregion

        
    }

    
}
