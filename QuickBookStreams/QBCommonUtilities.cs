using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DataProcessingBlocks;
using Interop.QBXMLRP2Lib;
using Interop.QBPOSXMLRPLIB;//Axis 11 pos
using System.IO;
using System.Collections.ObjectModel;
using EDI.Constant;
using System.Windows.Forms;
using TransactionImporter;
using System.Data;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Linq;
//607
using Telerik.WinControls;
using Telerik.WinControls.UI.PropertyGridData;
using System.Runtime.CompilerServices;

namespace Streams
{
    public class QBCommonUtilities
    {
        //Axis 8.0
        private static string m_GeneralDetailReportType;

        //593
        //private static string m_AccountTypeFilter;
        private static List<string> m_AccountTypeFilter;


        public static string GeneralDetailReportValue
        {
            get { return m_GeneralDetailReportType; }
            set { m_GeneralDetailReportType = value; }
        }

        //593

        //public static string AccountTypeFilterValue
        //{
        //    get { return m_AccountTypeFilter; }
        //    set { m_AccountTypeFilter = value; }
        //}

        public static List<string> AccountTypeFilterValue
        {
            get { return m_AccountTypeFilter; }
            set { m_AccountTypeFilter = value; }
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static string GetListFromQuickBookByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            if (TransTypeReq == "TransferQueryRq")
            {
                #region Newly Added Code
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
                queryRequest.AppendChild(ModifiedDateRangeFilter);

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                #endregion
            }
            else
            {
                #region Newly Added Code
                //following code commented for bug no. 395
                // XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                // queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToModifiedDate).InnerText = string.Empty;

                #endregion

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");   //for bug 614
                queryRequest.AppendChild(includeRetItems).InnerText = "";

                if (TransTypeReq != "ClassQueryRq" && TransTypeReq != "InventorySiteQueryRq")
                {
                    XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                    queryRequest.AppendChild(ownerID).InnerText = "0";
                }
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                                foreach (string s in array_msg)
                                {
                                    if (s != "")
                                    { statusMessage += s + ".\n"; }
                                    if (s == "")
                                    { statusMessage += s; }
                                }

                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        //following method is for Active filter- bug no. 395
        public static string GetListFromQuickBookByDateForInActiveFilter(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            if (TransTypeReq == "TransferQueryRq")
            {
                #region Newly Added Code
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
                queryRequest.AppendChild(ModifiedDateRangeFilter);

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                #endregion
            }
            else
            {
                #region Newly Added Code

                XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToModifiedDate).InnerText = string.Empty;

                #endregion

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");   //for bug 614
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                if (TransTypeReq != "ClassQueryRq" && TransTypeReq != "InventorySiteQueryRq")
                {
                    XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                    queryRequest.AppendChild(ownerID).InnerText = "0";
                }
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                                foreach (string s in array_msg)
                                {
                                    if (s != "")
                                    { statusMessage += s + ".\n"; }
                                    if (s == "")
                                    { statusMessage += s; }
                                }

                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        /// <summary>
        /// This method is used for getting item list from quickbooks 
        /// using modified date.
        /// </summary>
        /// <param name="QBFileName">Name of the QuickBooks Company file.</param>
        /// <param name="FromDate">From Modified date.</param>
        /// <param name="ToDate">To Modified date</param>
        /// <returns>Xml data string of item list.</returns>
        public static string GetListFromQuickBookByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            if (TransTypeReq == "TransferQueryRq")
            {
                //Axis 665
                #region Newly Added Code
                //XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
                //XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                //TimeZone localZone = TimeZone.CurrentTimeZone;
                //TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                //string strfromOffset = string.Empty;
                //if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                //{
                //    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                //}
                //else if (fromOffset.ToString().Contains("-"))
                //{
                //    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                //}
                //else if (fromOffset.ToString().Contains("+"))
                //{
                //    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                //}

                //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

                //TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                //string strtoOffset = string.Empty;
                //if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                //{
                //    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                //}
                //else if (toOffset.ToString().Contains("-"))
                //{
                //    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                //}
                //else if (toOffset.ToString().Contains("+"))
                //{
                //    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                //}
                //XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                //queryRequest.AppendChild(ModifiedDateRangeFilter);

                //Axis 665 end
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");

                queryRequest.AppendChild(ModifiedDateRangeFilter);

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                #endregion
            }
            // bug 551
            else if (TransTypeReq == "VehicleMileageQueryRq" || TransTypeReq == "ItemGroupQueryRq")
            {
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }
            else
            {
                #region Newly Added Code
                //following code commented for bug no. 395
                // XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                // queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                #endregion
                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");   //for bug 614
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                if (TransTypeReq != "ClassQueryRq" && TransTypeReq != "InventorySiteQueryRq")
                {
                    XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                    queryRequest.AppendChild(ownerID).InnerText = "0";
                }
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        //following method is for Active filter- bug no. 395
        public static string GetListFromQuickBookByDateForInActiveFilter(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            if (TransTypeReq == "TransferQueryRq")
            {
                #region Newly Added Code
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                queryRequest.AppendChild(ModifiedDateRangeFilter);

                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                #endregion
            }
            else
            {
                #region Newly Added Code

                XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                #endregion
                XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");   //for bug 614
                queryRequest.AppendChild(includeRetItems).InnerText = "";
                if (TransTypeReq != "ClassQueryRq" && TransTypeReq != "InventorySiteQueryRq")
                {
                    XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                    queryRequest.AppendChild(ownerID).InnerText = "0";
                }
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }


        //Axis 11 pos 
        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);

            #endregion           


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetSalesOrderListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, List<string> companyName)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if ((TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesRecieptQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        List<string> customerListId = GetCustomerListIdFromQBPOS(QBFileName, TransTypeReq, name.ToString());

                        if (customerListId.Count > 0)
                        {
                            foreach (var customerId in customerListId)
                            {
                                XmlElement NameFilter = pxmldoc.CreateElement("CustomerListID");
                                NameFilter.InnerText = customerId;
                                queryRequest.AppendChild(NameFilter);
                            }
                        }

                        break;
                    }
                }
            }

            #endregion           


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQBPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, string fromRef, string toRef)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);

            #endregion

            #region refnum
            if (TransTypeReq == "VoucherQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }
            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQBPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string fromRef, string toRef)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            //776
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq"
                || TransTypeReq == "VoucherQueryRq" || TransTypeReq == "InventoryCostAdjustmentQueryRq" || TransTypeReq == "InventoryQtyAdjustmentQueryRq")
            {
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                queryRequest.AppendChild(ModifiedDateRangeFilter);
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }
            else
            {
                #region Newly Added Code

                XmlElement FromTimeModified = pxmldoc.CreateElement("FromTimeModified");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");

                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToTimeModified");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                queryRequest.AppendChild(TimeModifiedRangeFilter);
                #endregion
            }

            #region refnum
            if (TransTypeReq == "VoucherQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }
            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, List<string> companyName)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Vendor
            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }
            if (namefilter.Count > 0)
            {
                if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name;
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);

                        break;
                    }
                }
            }
            #endregion   

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);

            #endregion                    


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, List<string> companyName, string fromRef, string toRef)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";


            #region Vendor
            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name.ToString();
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);
                        break;
                    }
                }
            }
            #endregion

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);

            #endregion           

            if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName, string fromRef, string toRef)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Vendor
            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name.ToString();
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);
                        break;
                    }
                }
            }
            #endregion

            //776
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq"
                || TransTypeReq == "VoucherQueryRq" || TransTypeReq == "InventoryCostAdjustmentQueryRq" || TransTypeReq == "InventoryQtyAdjustmentQueryRq")
            {
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                queryRequest.AppendChild(ModifiedDateRangeFilter);
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }
            else
            {
                #region Newly Added Code

                XmlElement FromTimeModified = pxmldoc.CreateElement("FromTimeModified");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");

                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToTimeModified");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                queryRequest.AppendChild(TimeModifiedRangeFilter);
                #endregion
            }

            #region refnum
            if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }
            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, List<string> firstNameList, List<string> lastNameList)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region Newly Added Code
            XmlElement TimeModifiedFilter = pxmldoc.CreateElement("TimeModifiedFilter");

            XmlElement MatchNumericCriterion = pxmldoc.CreateElement("MatchNumericCriterion");
            TimeModifiedFilter.AppendChild(MatchNumericCriterion).InnerText = "Equal";

            XmlElement TimeModified = pxmldoc.CreateElement("TimeModified");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeModifiedFilter.AppendChild(TimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            queryRequest.AppendChild(TimeModifiedFilter);
            #endregion

            #region Newly Added Code
            if ((TransTypeReq == "CustomerQueryRq" || TransTypeReq == "EmployeeQueryRq" || TransTypeReq == "TimeEntryQueryRq") &&
                (firstNameList.Count != 0) && (lastNameList.Count != 0))
            {
                XmlElement PurchaseOrderNumberFilter = pxmldoc.CreateElement("FirstNameFilter");
                XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchStringCriterion.InnerText = "Equal";
                PurchaseOrderNumberFilter.AppendChild(MatchStringCriterion);

                foreach (var firstName in firstNameList)
                {
                    XmlElement PurchaseOrderNumber = pxmldoc.CreateElement("FirstName");
                    PurchaseOrderNumber.InnerText = firstName;
                    PurchaseOrderNumberFilter.AppendChild(PurchaseOrderNumber);
                    queryRequest.AppendChild(PurchaseOrderNumberFilter);
                }
                //Create aggregate and fill in field values for it
                XmlElement LastFilter = pxmldoc.CreateElement("LastNameFilter");
                XmlElement MatchCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchCriterion.InnerText = "Equal";
                LastFilter.AppendChild(MatchCriterion);
                foreach (var lastName in lastNameList)
                {
                    XmlElement LastName = pxmldoc.CreateElement("LastName");
                    LastName.InnerText = lastName;
                    LastFilter.AppendChild(LastName);
                    queryRequest.AppendChild(LastFilter);
                }
            }

            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;
        }

        //Axis pos 11
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            // Time
            TimeZone localZone = TimeZone.CurrentTimeZone;

            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (TransTypeReq == "TimeEntryQueryRq")
            {
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "-" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = "-" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
            }
            else
            {
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
            }

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (TransTypeReq == "TimeEntryQueryRq")
            {
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "-" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "-" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
            }
            else
            {
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
            }

            //776
            #region Newly Added Code

            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq"
                || TransTypeReq == "VoucherQueryRq" || TransTypeReq == "InventoryCostAdjustmentQueryRq" || TransTypeReq == "InventoryQtyAdjustmentQueryRq")
            {
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                queryRequest.AppendChild(ModifiedDateRangeFilter);
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }
            else if (TransTypeReq == "TimeEntryQueryRq")
            {
                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("ClockInTimeRangeFilter");
                XmlElement FromTimeModified = pxmldoc.CreateElement("FromClockInTime");
                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToClockInTime");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
                queryRequest.AppendChild(TimeModifiedRangeFilter);
            }
            else
            {
                XmlElement FromTimeModified = pxmldoc.CreateElement("FromTimeModified");
                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");
                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToTimeModified");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
                queryRequest.AppendChild(TimeModifiedRangeFilter);
            }

            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetSalesOrderFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            // Time
            TimeZone localZone = TimeZone.CurrentTimeZone;

            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
            

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
           
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if ((TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesRecieptQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        List<string> customerListId = GetCustomerListIdFromQBPOS(QBFileName, TransTypeReq, name.ToString());

                        if (customerListId.Count > 0)
                        {
                            foreach (var customerId in customerListId)
                            {
                                XmlElement NameFilter = pxmldoc.CreateElement("CustomerListID");
                                NameFilter.InnerText = customerId;
                                queryRequest.AppendChild(NameFilter);
                                
                            }
                        }
                        break;
                    }
                }
            }

            #region Newly Added Code

            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                queryRequest.AppendChild(ModifiedDateRangeFilter);
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }

            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> firstNameList, List<string> lastNameList)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            if ((TransTypeReq == "CustomerQueryRq" || TransTypeReq == "EmployeeQueryRq" || TransTypeReq == "TimeEntryQueryRq") && (firstNameList.Count != 0 || lastNameList.Count != 0))
            {
                XmlElement PurchaseOrderNumberFilter = pxmldoc.CreateElement("FirstNameFilter");
                XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchStringCriterion.InnerText = "Equal";
                PurchaseOrderNumberFilter.AppendChild(MatchStringCriterion);
                foreach (var item in firstNameList)
                {
                    XmlElement PurchaseOrderNumber = pxmldoc.CreateElement("FirstName");
                    PurchaseOrderNumber.InnerText = item;
                    PurchaseOrderNumberFilter.AppendChild(PurchaseOrderNumber);
                    queryRequest.AppendChild(PurchaseOrderNumberFilter);
                }


                //Create aggregate and fill in field values for it
                XmlElement LastFilter = pxmldoc.CreateElement("LastNameFilter");
                XmlElement MatchCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchCriterion.InnerText = "Equal";
                LastFilter.AppendChild(MatchCriterion);
                foreach (var item in lastNameList)
                {
                    XmlElement LastName = pxmldoc.CreateElement("LastName");
                    LastName.InnerText = item;
                    LastFilter.AppendChild(LastName);
                    queryRequest.AppendChild(LastFilter);
                }
            }

            #region Newly Added Code

            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }

            if (TransTypeReq == "TimeEntryQueryRq")
            {
                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("ClockInTimeRangeFilter");
                XmlElement FromTimeModified = pxmldoc.CreateElement("FromClockInTime");
                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToClockInTime");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
                queryRequest.AppendChild(TimeModifiedRangeFilter);
            }
            else
            {
                XmlElement FromTimeModified = pxmldoc.CreateElement("FromTimeModified");
                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");
                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToTimeModified");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
                queryRequest.AppendChild(TimeModifiedRangeFilter);
            }


            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, List<string> firstNameList, List<string> lastNameList)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            if ((TransTypeReq == "CustomerQueryRq" || TransTypeReq == "EmployeeQueryRq" || TransTypeReq == "TimeEntryQueryRq") &&
                (firstNameList.Count != 0) && (lastNameList.Count !=0))
            {
                XmlElement PurchaseOrderNumberFilter = pxmldoc.CreateElement("FirstNameFilter");
                XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchStringCriterion.InnerText = "Equal";
                PurchaseOrderNumberFilter.AppendChild(MatchStringCriterion);
                string firstFullName = ""; string lastFullName = "";
                if (TransTypeReq == "TimeEntryQueryRq")
                {
                    foreach (var firstname in firstNameList)
                    {
                        firstFullName = firstFullName + firstname + " ";
                    }
                    foreach (var lastname in lastNameList)
                    {
                        lastFullName = lastFullName + lastname + " ";
                    }

                    XmlElement PurchaseOrderNumber = pxmldoc.CreateElement("FirstName");
                    PurchaseOrderNumber.InnerText = firstFullName.TrimEnd();
                    PurchaseOrderNumberFilter.AppendChild(PurchaseOrderNumber);
                    queryRequest.AppendChild(PurchaseOrderNumberFilter);
                }
                else
                {
                    foreach (var firstname in firstNameList)
                    {
                        XmlElement PurchaseOrderNumber = pxmldoc.CreateElement("FirstName");
                        PurchaseOrderNumber.InnerText = firstname;
                        PurchaseOrderNumberFilter.AppendChild(PurchaseOrderNumber);
                        queryRequest.AppendChild(PurchaseOrderNumberFilter);
                    }
                }


                //Create aggregate and fill in field values for it
                XmlElement LastFilter = pxmldoc.CreateElement("LastNameFilter");
                XmlElement MatchCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchCriterion.InnerText = "Equal";
                LastFilter.AppendChild(MatchCriterion);

                if (TransTypeReq == "TimeEntryQueryRq")
                {
                    XmlElement LastName = pxmldoc.CreateElement("LastName");
                    LastName.InnerText = lastFullName.TrimEnd();
                    LastFilter.AppendChild(LastName);
                    queryRequest.AppendChild(LastFilter);
                }
                else
                {
                    if (lastNameList.Count != 0)
                    {
                        foreach (var lastname in lastNameList)
                        {
                            XmlElement LastName = pxmldoc.CreateElement("LastName");
                            LastName.InnerText = lastname;
                            LastFilter.AppendChild(LastName);
                            queryRequest.AppendChild(LastFilter);
                        }
                    }
                }
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, List<string> companyName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if ((TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name.ToString();
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);

                        break;
                    }
                }
                else if ((TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesRecieptQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        List<string> customerListId = GetCustomerListIdFromQBPOS(QBFileName, TransTypeReq, name.ToString());

                        if (customerListId.Count > 0)
                        {
                            foreach (var customerId in customerListId)
                            {
                                XmlElement NameFilter = pxmldoc.CreateElement("CustomerListID");
                                NameFilter.InnerText = customerId;
                                queryRequest.AppendChild(NameFilter);
                            }
                        }

                        break;
                    }
                }
            }
            

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public static List<string> GetCustomerListIdFromQBPOS(string QBFileName, string TransTypeReq, string customerName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement("CustomerQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            List<string> firstNameList = new List<string>();
            List<string> lastNameList = new List<string>();
            List<string> customerIdList = new List<string>();
            
            string[] array_msg = customerName.Split(' ');

            for (int listCount = 0; listCount < array_msg.Length; listCount++)
            {
                if (listCount == (array_msg.Length - 1))
                {
                    lastNameList.Add(array_msg[listCount]);
                }
                else
                {
                    firstNameList.Add(array_msg[listCount]);
                }
            }

            XmlElement PurchaseOrderNumberFilter = pxmldoc.CreateElement("FirstNameFilter");
            XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
            MatchStringCriterion.InnerText = "Equal";
            PurchaseOrderNumberFilter.AppendChild(MatchStringCriterion);

            foreach (var firstname in firstNameList)
            {
                XmlElement PurchaseOrderNumber = pxmldoc.CreateElement("FirstName");
                PurchaseOrderNumber.InnerText = firstname;
                PurchaseOrderNumberFilter.AppendChild(PurchaseOrderNumber);
                queryRequest.AppendChild(PurchaseOrderNumberFilter);
            }

            XmlElement LastFilter = pxmldoc.CreateElement("LastNameFilter");
            XmlElement MatchCriterion = pxmldoc.CreateElement("MatchStringCriterion");
            MatchCriterion.InnerText = "Equal";
            LastFilter.AppendChild(MatchCriterion);

            if (lastNameList.Count != 0)
            {
                foreach (var lastname in lastNameList)
                {
                    XmlElement LastName = pxmldoc.CreateElement("LastName");
                    LastName.InnerText = lastname;
                    LastFilter.AppendChild(LastName);
                    queryRequest.AppendChild(LastFilter);
                }
            }


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    XmlDocument outputXMLDocOfItemService = new XmlDocument();

                    if (responseOfItem != string.Empty)
                    {
                        outputXMLDocOfItemService.LoadXml(responseOfItem);

                        XmlNodeList custList = outputXMLDocOfItemService.GetElementsByTagName("ListID");

                        foreach (XmlNode customereNodes in custList)
                        {
                            customerIdList.Add(customereNodes.InnerText);
                        }
                    }
                }
                #endregion
            }
            catch
            {
                return customerIdList;
            }

            return customerIdList;
        }


        public static string GetListFromQPOSByDate(string TransTypeReq, string QBFileName, string fromRef, string toRef)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            #region refnum
            if (TransTypeReq == "VoucherQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }
            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, List<string> companyName, string fromRef, string toRef)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name;
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);

                        break;
                    }
                }
            }

            #region refnum
            if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = fromRef;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = toRef;
            }
            #endregion
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }

        public static string GetListFromQuickBookPOSByDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyname)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            queryRequest.AppendChild(ownerID).InnerText = "0";

            //776
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq"
                || TransTypeReq == "VoucherQueryRq" || TransTypeReq == "InventoryCostAdjustmentQueryRq" || TransTypeReq == "InventoryQtyAdjustmentQueryRq")
            {
                XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
                queryRequest.AppendChild(ModifiedDateRangeFilter);
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            }
            else
            {
                #region Newly Added Code

                XmlElement FromTimeModified = pxmldoc.CreateElement("FromTimeModified");
                TimeZone localZone = TimeZone.CurrentTimeZone;
                TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
                string strfromOffset = string.Empty;
                if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("-"))
                {
                    strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }
                else if (fromOffset.ToString().Contains("+"))
                {
                    strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
                }

                XmlElement TimeModifiedRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");

                TimeModifiedRangeFilter.AppendChild(FromTimeModified).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

                TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
                string strtoOffset = string.Empty;
                if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("-"))
                {
                    strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                else if (toOffset.ToString().Contains("+"))
                {
                    strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
                }
                XmlElement ToTimeModified = pxmldoc.CreateElement("ToTimeModified");
                TimeModifiedRangeFilter.AppendChild(ToTimeModified).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

                queryRequest.AppendChild(TimeModifiedRangeFilter);
                #endregion
            }

            #region Vendor & purchase
            List<string> namefilter = new List<string>();
            foreach (var name in companyname)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if (TransTypeReq == "VoucherQueryRq" || TransTypeReq == "PurchaseOrderQueryRq")
                {
                    foreach (var name in namefilter)
                    {
                        XmlElement CompanyNameFilter = pxmldoc.CreateElement("CompanyNameFilter");
                        XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                        MatchStringCriterion.InnerText = "Equal";
                        CompanyNameFilter.AppendChild(MatchStringCriterion);

                        XmlElement CompanyName = pxmldoc.CreateElement("CompanyName");
                        CompanyName.InnerText = name.ToString();
                        CompanyNameFilter.AppendChild(CompanyName);
                        queryRequest.AppendChild(CompanyNameFilter);

                        break;
                    }
                }
            }
            #endregion
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseOfItem;

        }
        //Axsis 11 POS
        public static string GetListFromQBPOS(string TransTypeReq, string QBFileName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            switch (TransTypeReq)
            {

                case "ItemInventoryQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;

                case "PurchaseOrderQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "SalesOrderQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "PriceAdjustmentQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "SalesReceiptQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "VoucherQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ClassQueryRq":
                    {
                        //For the Export Customer data with CustomFields (Axis 8.0)
                        XmlElement ownerID = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(ownerID).InnerText = "";
                    }
                    break;
                case "CustomerQueryRq":
                case "VendorQueryRq":
                case "EmployeeQueryRq":
                    {
                        //For the Export Customer data with CustomFields (Axis 8.0)
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;

                default:
                    break;
            }


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);

                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {
                //DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.CloseConnection();
                MessageBox.Show("Please disconnect online connection.", "Zed Axis");
            }
            return responseList;
        }

        // Axis 11  pos
        public static string GetListByTxnIDAndListIDFromQuickBookPOS(string TransTypeReq, string QBFileName, List<string> ListAndTxnId)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            string ticket = string.Empty;
            string responseList = string.Empty;



            if (TransTypeReq == "CustomerQueryRq" || TransTypeReq == "EmployeeQueryRq" || TransTypeReq == "TimeEntryQueryRq" || TransTypeReq == "ItemInventoryQueryRq")
            {
                if (ListAndTxnId.Count > 0)
                {
                    foreach (string id in ListAndTxnId)
                    {
                        XmlElement qbXMLlistId = pxmldoc.CreateElement("ListID");
                        qbXMLlistId.InnerText = id;
                        queryRequest.AppendChild(qbXMLlistId);
                    }
                }
            }
            else
            {
                if (ListAndTxnId.Count > 0)
                {
                    foreach (string id in ListAndTxnId)
                    {
                        XmlElement qbXMLlistId = pxmldoc.CreateElement("TxnID");
                        qbXMLlistId.InnerText = id;
                        queryRequest.AppendChild(qbXMLlistId);
                    }
                }
            }
            string pinput = pxmldoc.OuterXml;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch { }
            return responseList;
        }

        //Axis-565
        public static string GetNameByALUOrUPCFromQuickBookPOS(string QBFileName, string UPCId = null, string ALUId = null)
        {
            string ItemName = null;

            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement("ItemInventoryQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            string ticket = string.Empty;
            string responseList = string.Empty;




            if (UPCId != null)
            {

                XmlElement qbXMLlistId = pxmldoc.CreateElement("UPCFilter");
                XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchStringCriterion.InnerText = "Equal";
                XmlElement UPC = pxmldoc.CreateElement("UPC");

                UPC.InnerXml = UPCId;

                qbXMLlistId.AppendChild(MatchStringCriterion);
                qbXMLlistId.AppendChild(UPC);
                queryRequest.AppendChild(qbXMLlistId);

            }
            if (ALUId != null)
            {

                XmlElement qbXMLlistId = pxmldoc.CreateElement("ALUFilter");
                XmlElement MatchStringCriterion = pxmldoc.CreateElement("MatchStringCriterion");
                MatchStringCriterion.InnerText = "Equal";
                XmlElement ALU = pxmldoc.CreateElement("ALU");

                ALU.InnerXml = ALUId;

                qbXMLlistId.AppendChild(MatchStringCriterion);
                qbXMLlistId.AppendChild(ALU);
                queryRequest.AppendChild(qbXMLlistId);
            }

            string pinput = pxmldoc.OuterXml;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                //try
                //{
                ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket;
                //DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                //ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                //}
                //catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {

                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    //Axis 565 reopen
                    if (responseList.Contains("<UPC>"))
                    {
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryQueryRs/ItemInventoryRet/ListID"))
                        {
                            ItemName = oNode.InnerXml;
                        }
                    }
                    else
                    {
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/ItemInventoryQueryRs/ItemInventoryRet/Desc1"))
                        {
                            ItemName = oNode.InnerXml;
                        }
                    }
                    //Axis 565 reopen ends
                }
                #endregion
            }
            catch { }
            return ItemName;
        }

        #region GeneralReport
        public static string GetListFromQuickBookByDateGeneralReport(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {

            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (TransTypeReq == "GeneralDetailReportQueryRq")
            {
                XmlElement reportType = pxmldoc.CreateElement("GeneralDetailReportType");
                queryRequest.AppendChild(reportType).InnerText = GeneralDetailReportValue;
            }
            else if (TransTypeReq == "GeneralSummaryReportQueryRq")
            {
                XmlElement reportType = pxmldoc.CreateElement("GeneralSummaryReportType");
                queryRequest.AppendChild(reportType).InnerText = GeneralDetailReportValue;
            }

            XmlElement reportperiod = pxmldoc.CreateElement("ReportPeriod");
            queryRequest.AppendChild(reportperiod);


            #region Newly Added Code

            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromReportDate");

            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            reportperiod.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToReportDate");
            reportperiod.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            // queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();

            //Axis 671 DESKTOP
            if (TransTypeReq == "GeneralDetailReportQueryRq")
            {
                XmlElement ReportAccountFilter = pxmldoc.CreateElement("ReportAccountFilter");
                queryRequest.AppendChild(ReportAccountFilter);

                foreach (var item in AccountTypeFilterValue)
                {
                    XmlElement AccountTypeFilter = pxmldoc.CreateElement("FullName");
                    ReportAccountFilter.AppendChild(AccountTypeFilter).InnerText = item;
                }
                //XmlElement reportAccountFilter = pxmldoc.CreateElement("ReportAccountFilter");
                //queryRequest.AppendChild(reportAccountFilter);
                //XmlElement accountTypeFilter = pxmldoc.CreateElement("AccountTypeFilter");
                //reportperiod.AppendChild(accountTypeFilter).InnerText = FromRefNum;
            }

            #endregion

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            try
            {
                DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch
            { }

            //Getting ticket for request.
            ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseOfItem != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseOfItem);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "The Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }


            return responseOfItem;

        }

        #endregion




        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Newly added code

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq == "BillingRateQueryRq")
                {
                    foreach (var item in entityFilter)
                    {
                        XmlElement NameFilter = pxmldoc.CreateElement("FullName");
                        NameFilter.InnerText = item.Replace("     :     BillingRate", string.Empty);
                        queryRequest.AppendChild(NameFilter);
                    }
                }
            }

            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            //bug 485 
            if (TransTypeReq == "VehicleMileageQueryRq")
            {
                XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            }
            else
            {
                if (TransTypeReq == "ItemGroupQueryRq" || TransTypeReq == "BillingRateQueryRq")
                {
                    queryRequest.RemoveChild(ModifiedDateRangeFilter);
                    XmlElement FromMDate = pxmldoc.CreateElement("FromModifiedDate");
                    queryRequest.AppendChild(FromMDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                    XmlElement ToMDate = pxmldoc.CreateElement("ToModifiedDate");
                    queryRequest.AppendChild(ToMDate).InnerText = string.Empty;
                }
                else
                {
                    XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
                    ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                    XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                    ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
                }
            }

            #endregion

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq != "BillingRateQueryRq")
                {
                    if (TransTypeReq != "TimeTrackingQueryRq")
                    {
                        XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                        queryRequest.AppendChild(EntityFilter);
                        foreach (var item in entityFilter)
                        {
                            XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                            EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                        }
                    }
                    else
                    {
                        XmlElement EntityFilter = pxmldoc.CreateElement("TimeTrackingEntityFilter");
                        queryRequest.AppendChild(EntityFilter);
                        foreach (var item in entityFilter)
                        {
                            XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                            EntityFilter.AppendChild(entityFilterFullName).InnerText = item;
                        }
                    }
                }
            }
            // bug 485
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "VehicleMileageQueryRq" && TransTypeReq != "ItemGroupQueryRq"
                && TransTypeReq != "BillingRateQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485

            //bug no 126 Axis 11 //bug 147 Axis 12.0
            if (TransTypeReq == "ItemInventoryQueryRq" || TransTypeReq == "ItemNonInventoryQueryRq" || TransTypeReq == "ItemServiceQueryRq" || TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "ItemGroupQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
               
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {
            }
            return responseList;
        }

        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region Newly added code

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq == "BillingRateQueryRq")
                {
                    foreach (var item in entityFilter)
                    {
                        XmlElement NameFilter = pxmldoc.CreateElement("FullName");
                        NameFilter.InnerText = item.Replace("     :     BillingRate", string.Empty);
                        queryRequest.AppendChild(NameFilter);
                    }
                }
            }

            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion

            if (TransTypeReq == "BillingRateQueryRq")
            {
                queryRequest.RemoveChild(ModifiedDateRangeFilter);
                XmlElement FromMDate = pxmldoc.CreateElement("FromModifiedDate");
                queryRequest.AppendChild(FromMDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
                XmlElement ToMDate = pxmldoc.CreateElement("ToModifiedDate");
                queryRequest.AppendChild(ToMDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            }

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq != "BillingRateQueryRq")
                {
                    if (TransTypeReq != "TimeTrackingQueryRq")
                    {
                        XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                        queryRequest.AppendChild(EntityFilter);
                        foreach (var item in entityFilter)
                        {
                            XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                            EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                        }
                    }
                    else
                    {
                        XmlElement EntityFilter = pxmldoc.CreateElement("TimeTrackingEntityFilter");
                        queryRequest.AppendChild(EntityFilter);
                        foreach (var item in entityFilter)
                        {
                            XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                            EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                        }
                    }
                }
            }
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BillingRateQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }


            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            //End Axis bug no 144
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        //last export
        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BuildAssemblyQueryRq" && TransTypeReq != "VehicleMileageQueryRq" && TransTypeReq != "ItemGroupQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            //402
            if (TransTypeReq == "BuildAssemblyQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {
            }
            return responseList;
        }
        /// <summary>
        /// This method is used for getting the list from quickbooks 
        /// using modified date.
        /// </summary>
        /// <param name="QBFileName">Name of the QuickBooks Company file.</param>
        /// <param name="FromDate">From Modified date.</param>
        /// <param name="ToDate">To Modified date</param>
        /// <returns>Xml data string of sales order list.</returns>
        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region For Modified Date
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion


            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";

            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq" || TransTypeReq == "ItemGruopQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //bug 485
            if (TransTypeReq == "ItemReceiptQueryRq")
            {


                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            //End Axis bug no 144
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            switch (TransTypeReq)
            {
                case "ARRefundCreditCardQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "TransferQueryRq":
                    {

                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeRetElement = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement).InnerText = "TxnID";
                        XmlElement IncludeRetElement1 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement1).InnerText = "TimeCreated";
                        XmlElement IncludeRetElement2 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement2).InnerText = "TimeModified";
                        XmlElement IncludeRetElement3 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement3).InnerText = "EditSequence";
                        XmlElement IncludeRetElement4 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement4).InnerText = "TxnNumber";
                        XmlElement IncludeRetElement5 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement5).InnerText = "TxnDate";
                        XmlElement IncludeRetElement6 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement6).InnerText = "TransferFromAccountRef";
                        XmlElement IncludeRetElement7 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement7).InnerText = "FromAccountBalance";
                        XmlElement IncludeRetElement8 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement8).InnerText = "TransferToAccountRef";
                        XmlElement IncludeRetElement9 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement9).InnerText = "ToAccountBalance";
                        XmlElement IncludeRetElement10 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement10).InnerText = "ClassRef";
                        XmlElement IncludeRetElement11 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement11).InnerText = "Amount";
                        XmlElement IncludeRetElement12 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement12).InnerText = "Memo";
                    }
                    break;
                case "BillPaymentCheckQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "BillPaymentCreditCardQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "BillQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Axis Bug No 144                       
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                        //End Axis bug no 144

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CheckQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CreditCardChargeQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CreditCardCreditQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "CreditMemoQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "DepositQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "EstimateQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "InventoryAdjustmentQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "InvoiceQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)                       
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //bug 147 Axis 12.0
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "JournalEntryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "PurchaseOrderQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        //P Axis 13.1 : issue 145
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                        //P Axis 13.1 : issue 145 END
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ReceivePaymentQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "SalesOrderQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;

                case "SalesReceiptQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "VendorCreditQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        //Axis Bug No 144                       
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                    }
                    break;
                case "TransferInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "ItemInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemNonInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemInventoryAssemblyQueryRq":
                    {

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemServiceQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ClassQueryRq":
                case "InventorySiteQueryRq":
                case "BillingRateQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(ownerID).InnerText = "";
                    }
                    break;
                case "CustomerQueryRq":
                case "VendorQueryRq":
                case "EmployeeQueryRq":
                    {
                        //For the Export Customer data with CustomFields (Axis 8.0)
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "GeneralDetailReportQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)                        
                        XmlElement GeneralDetailReportType = pxmldoc.CreateElement("GeneralDetailReportType");
                        queryRequest.AppendChild(GeneralDetailReportType).InnerText = GeneralDetailReportValue.ToString();

                        if (GeneralDetailReportValue.ToString() == "MissingChecks")
                        {
                            //Axis 671 DESKTOP
                            if (AccountTypeFilterValue == null)
                            {
                                MessageBox.Show("Please select Account type.");
                            }
                            else
                            {
                                XmlElement ReportAccountFilter = pxmldoc.CreateElement("ReportAccountFilter");
                                queryRequest.AppendChild(ReportAccountFilter);

                                foreach (var item in AccountTypeFilterValue)
                                {
                                    XmlElement AccountTypeFilter = pxmldoc.CreateElement("FullName");
                                    ReportAccountFilter.AppendChild(AccountTypeFilter).InnerText = item;
                                }
                            }
                        }
                    }
                    break;
                case "GeneralSummaryReportQueryRq":
                    {
                        XmlElement GeneralSummaryReportType = pxmldoc.CreateElement("GeneralSummaryReportType");
                        queryRequest.AppendChild(GeneralSummaryReportType).InnerText = GeneralDetailReportValue.ToString();
                    }
                    break;
                //402
                case "BuildAssemblyQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                //bug 485 axis 12
                case "VehicleMileageQueryRq":
                    {
                        //XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        //queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                // Bug 485 axis 12 item receipt
                case "ItemReceiptQueryRq":
                    {
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }

                    break;
                //issue 551 required for  DataExt Values
                case "ItemGroupQueryRq":
                    {
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                default:
                    break;
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {
                MessageBox.Show("Please disconnect online connection.", "Zed Axis");
            }
            //463
            pinput = null;

            //463

            return responseList;
        }

        //bug no. 395- method for include inactive filter
        public static string GetListFromQuickBookForInactive(string TransTypeReq, string QBFileName)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            switch (TransTypeReq)
            {
                case "ARRefundCreditCardQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "TransferQueryRq":
                    {

                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeRetElement = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement).InnerText = "TxnID";
                        XmlElement IncludeRetElement1 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement1).InnerText = "TimeCreated";
                        XmlElement IncludeRetElement2 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement2).InnerText = "TimeModified";
                        XmlElement IncludeRetElement3 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement3).InnerText = "EditSequence";
                        XmlElement IncludeRetElement4 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement4).InnerText = "TxnNumber";
                        XmlElement IncludeRetElement5 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement5).InnerText = "TxnDate";
                        XmlElement IncludeRetElement6 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement6).InnerText = "TransferFromAccountRef";
                        XmlElement IncludeRetElement7 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement7).InnerText = "FromAccountBalance";
                        XmlElement IncludeRetElement8 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement8).InnerText = "TransferToAccountRef";
                        XmlElement IncludeRetElement9 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement9).InnerText = "ToAccountBalance";
                        XmlElement IncludeRetElement10 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement10).InnerText = "ClassRef";
                        XmlElement IncludeRetElement11 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement11).InnerText = "Amount";
                        XmlElement IncludeRetElement12 = pxmldoc.CreateElement("IncludeRetElement");
                        queryRequest.AppendChild(IncludeRetElement12).InnerText = "Memo";
                    }
                    break;
                case "BillPaymentCheckQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "BillPaymentCreditCardQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "BillQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Axis Bug No 144                       
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                        //End Axis bug no 144

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CheckQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CreditCardChargeQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Adding for custome field
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CreditCardCreditQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Adding for custome field  Axis 12.0
                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "CreditMemoQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                case "DepositQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "EstimateQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "InventoryAdjustmentQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "InvoiceQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        //bug 147 Axis 12.0
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "JournalEntryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "PurchaseOrderQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ReceivePaymentQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "SalesOrderQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;

                case "SalesReceiptQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "VendorCreditQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)                       
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                        //Axis Bug No 485 axis 12.0                      
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                        //End Axis bug no 485
                    }
                    break;
                case "TransferInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                        queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
                    }
                    break;
                case "ItemInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemNonInventoryQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemInventoryAssemblyQueryRq":
                    {
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ItemServiceQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "ClassQueryRq":
                case "InventorySiteQueryRq":
                    {
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";
                    }
                    break;
                case "PriceLevelQueryRq":
                    {
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";
                    }
                    break;
                case "OtherNameQueryRq":
                    {
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";
                    }
                    break;
                case "CustomerQueryRq":
                case "VendorQueryRq":
                case "EmployeeQueryRq":
                    {
                        //For the Export Customer data with CustomFields (Axis 8.0)
                        //include inactive is checked
                        XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                        queryRequest.AppendChild(ActiveStatus).InnerText = "All";

                        XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(ownerID).InnerText = "0";
                    }
                    break;
                case "GeneralDetailReportQueryRq":
                    {
                        //For the bug 1492 (Axis 6.0)                        
                        XmlElement GeneralDetailReportType = pxmldoc.CreateElement("GeneralDetailReportType");
                        queryRequest.AppendChild(GeneralDetailReportType).InnerText = GeneralDetailReportValue.ToString();

                        if (GeneralDetailReportValue.ToString() == "MissingChecks")
                        {
                            XmlElement ReportAccountFilter = pxmldoc.CreateElement("ReportAccountFilter");
                            queryRequest.AppendChild(ReportAccountFilter);
                            XmlElement AccountTypeFilter = pxmldoc.CreateElement("FullName");
                            ReportAccountFilter.AppendChild(AccountTypeFilter).InnerText = AccountTypeFilterValue.ToString();
                        }
                    }
                    break;
                case "GeneralSummaryReportQueryRq":
                    {
                        XmlElement GeneralSummaryReportType = pxmldoc.CreateElement("GeneralSummaryReportType");
                        queryRequest.AppendChild(GeneralSummaryReportType).InnerText = GeneralDetailReportValue.ToString();
                    }
                    break;
                case "vehiclemileageRq":
                    {

                    }
                    break;
                // Bug 485 axis 12 item receipt
                case "ItemReceiptRq":
                    {
                        XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                        queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                        XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                        queryRequest.AppendChild(OwnerID).InnerText = "0";
                    }
                    break;
                default:
                    break;
            }




            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
                
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {
                //DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.CloseConnection();
                MessageBox.Show("Please disconnect online connection.", "Zed Axis");
            }
            return responseList;
        }

        /// <summary>
        /// This is used to get TxnID and ListID of transactions.
        /// </summary>
        /// <param name="TransTypeReq">Returns Transaction type</param>
        /// <param name="QBFileName"></param>
        /// <param name="ListAndTxnId"></param>
        /// <returns></returns>
        public static string GetListByTxnIDAndListIDFromQuickBook(string TransTypeReq, string QBFileName, List<string> ListAndTxnId)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransTypeReq == "GeneralDetailReportQueryRq" || TransTypeReq == "GeneralSummaryReportQueryRq")
            {

            }

            else if (TransTypeReq == "CustomerQueryRq" || TransTypeReq == "EmployeeQueryRq" || TransTypeReq == "VendorQueryRq" || TransTypeReq == "ClassQueryRq"
                || TransTypeReq == "ItemQueryRq" || TransTypeReq == "OtherNameQueryRq" || TransTypeReq == "ItemQueryRq"
                || TransTypeReq == "PriceLevelQueryRq" || TransTypeReq == "ItemNonInventoryQueryRq" || TransTypeReq == "ItemInventoryAssemblyQueryRq"
                || TransTypeReq == "ItemServiceQueryRq" || TransTypeReq == "ItemGroupQueryRq" || TransTypeReq == "BillingRateQueryRq")
            {

                if (ListAndTxnId.Count > 0)
                {
                    foreach (string id in ListAndTxnId)
                    {
                        XmlElement qbXMLlistId = pxmldoc.CreateElement("ListID");
                        qbXMLlistId.InnerText = id;
                        queryRequest.AppendChild(qbXMLlistId);
                    }
                }

            }
            else
            {

                if (ListAndTxnId.Count > 0)
                {
                    foreach (string id in ListAndTxnId)
                    {
                        XmlElement qbXMLlistId = pxmldoc.CreateElement("TxnID");
                        qbXMLlistId.InnerText = id;
                        queryRequest.AppendChild(qbXMLlistId);
                    }
                }
            }
            string pinput = pxmldoc.OuterXml;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq != "TimeTrackingQueryRq")
                {
                    XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                    queryRequest.AppendChild(EntityFilter);
                    foreach (var item in entityFilter)
                    {
                        XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                        EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                    }

                }
                else
                {
                    XmlElement EntityFilter = pxmldoc.CreateElement("TimeTrackingEntityFilter");
                    queryRequest.AppendChild(EntityFilter);
                    foreach (var item in entityFilter)
                    {
                        XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                        EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                    }
                }
            }
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "VehicleMileageQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                //Axis Bug No 144                       
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                //End Axis bug no 144

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                //XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                //queryRequest.AppendChild(OwnerID).InnerText = "0";

            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;

        }

        /// <summary>
        /// This method is used for getting the list from quickbooks 
        /// using modified date.
        /// </summary>
        /// <param name="QBFileName">Name of the QuickBooks Company file.</param>
        /// <param name="FromDate">From Modified date.</param>
        /// <param name="ToDate">To Modified date</param>
        /// <returns>Xml data string of sales order list.</returns>
        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            ////Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion

            //bug 485 axis 12.0
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BuildAssemblyQueryRq" && TransTypeReq != "VehicleMileageQueryRq" && TransTypeReq != "ItemGroupQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144


            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }


            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
               
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;

        }


        /// <summary>
        /// This method is used for getting the list from quickbooks 
        /// using Transaction Date.
        /// </summary>
        /// <param name="TransTypeReq">Type of transaction</param>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="FromDate">From Txn Date</param>
        /// <param name="ToDate">To Txn date</param>
        public static string GetListFromQuickBookTxnDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");



            if (entityFilter != "All")
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                EntityFilter.AppendChild(entityFilterFullName).InnerText = entityFilter;
            }
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                //XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                //queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            try
            {
                // DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch
            { }
            // ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        /// <returns></returns>
        public static string GetListFromQuickBookTxnDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            //XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(responseList);
            //xmldoc.Save("C://PurchaseOrder.xml");
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;

        }

        /// <summary>
        ///This method is used for getting Time Entry list from Quickbooks using RefNumber. 
        /// </summary>
        /// <returns></returns>
        public static string GetListFromQuickBookForTimeEntry(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("SalesOrderQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;

        }

        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        /// <summary>
        /// This method is used for getting sales order list from quickbooks
        /// using Ref Number.
        /// </summary>
        /// <param name="QBFileName">Name of the QuickBooks Company file.</param>
        /// <param name="FromRefNumber">From Ref Number</param>
        /// <param name="ToRefNumber">To Ref Number</param>
        /// <returns>Xml data string of sales order list.</returns>
        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, string FromRefNum, string ToRefNum)
        {

            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BuildAssemblyQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            if (TransTypeReq == "BuildAssemblyQueryRq")
            {

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {


                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;
        }


        public static string GetAllListFromQuickBook(string TransTypeReq, string QBFileName, string txnId)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement(TransTypeReq);
            inner.AppendChild(QueryRq);

            XmlElement txnIdNodes = requestXmlDoc.CreateElement("TxnID");
            QueryRq.AppendChild(txnIdNodes).InnerText = txnId;

            XmlElement IncludeLineItems = requestXmlDoc.CreateElement("IncludeLineItems");
            QueryRq.AppendChild(IncludeLineItems).InnerText = "1";

            string pinput = requestXmlDoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                requestXmlDoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;
        }

        //Axis pos 11
        public static string GetListFromQBPOS(string TransTypeReq, string QBFileName, string FromRefNum, string ToRefNum)
        {

            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (TransTypeReq == "SalesOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("SalesOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromSalesOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToSalesOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }
            if (TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;
        }

        public static string GetSalesOrderListFromQBPOS(string TransTypeReq, string QBFileName, string FromRefNum, string ToRefNum, List<string> companyName)
        {

            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
                if ((TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesRecieptQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        List<string> customerListId = GetCustomerListIdFromQBPOS(QBFileName, TransTypeReq, name.ToString());

                        if (customerListId.Count > 0)
                        {
                            foreach (var customerId in customerListId)
                            {
                                XmlElement NameFilter = pxmldoc.CreateElement("CustomerListID");
                                NameFilter.InnerText = customerId;
                                queryRequest.AppendChild(NameFilter);
                            }
                        }
                        break;
                    }
                }
            }

            if (TransTypeReq == "SalesOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("SalesOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromSalesOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToSalesOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;
        }

        /// <summary>
        /// This method is used for getting sales order list from quickbooks
        /// using Date filter and ref number filter.
        /// </summary>
        /// <param name="QBFileName">Name of the company file.</param>
        /// <param name="FromDate">Passing From date</param>
        /// <param name="ToDate">Passing To Date</param>
        /// <param name="FromRefNumber">Passing From Ref Number</param>
        /// <param name="ToRefNumber">Passing To ref Number</param>
        /// <returns>Xml data of sales order list</returns>
        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            ////Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;

            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BuildAssemblyQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                //XmlElement IncludeRetElement = pxmldoc.CreateElement("IncludeRetElement");
                //queryRequest.AppendChild(IncludeRetElement).InnerText = "1";

                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

            }//End Axis bug no 485

            if (TransTypeReq == "ReceivePaymentQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        //Axis pos 11
        public static string GetListFromQBPOS(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            if (TransTypeReq == "SalesOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("SalesOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromSalesOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToSalesOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }
            if (TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }


            //Axis 11.0  bug 126 
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Newly added code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();
            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }

            //Axis 671 DESKTOP
            if (TransTypeReq == "InvoiceQueryRq" || TransTypeReq == "SalesReceiptQueryRq" || TransTypeReq == "ReceivePaymentQueryRq" || TransTypeReq == "BillQueryRq" || TransTypeReq == "CreditMemoQueryRq")
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            }
            else
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            }
            #endregion
            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getting sales order list from quickbooks
        /// using Date filter and ref number filter.
        /// </summary>
        /// <param name="QBFileName">Name of the company file.</param>
        /// <param name="FromDate">Passing From date</param>
        /// <param name="ToDate">Passing To Date</param>
        /// <param name="FromRefNumber">Passing From Ref Number</param>
        /// <param name="ToRefNumber">Passing To ref Number</param>
        /// <returns>Xml data of sales order list</returns>
        public static string ModifiedGetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region Newly added
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion

            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        //Axis 11
        public static string ModifiedGetListFromQuickBookPOS(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Newly added
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTimeModified");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTimeModified");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion

            if (TransTypeReq == "SalesOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("SalesOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromSalesOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToSalesOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }

            if (TransTypeReq == "PurchaseOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("PurchaseOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromPurchaseOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToPurchaseOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }

            //Axis 11.0  bug 126 
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        public static string ModifiedListFromQuickBookPOS(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, List<string> companyName, string FromRefNum, string ToRefNum)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBPOSXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            #region Newly added 
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TimeModifiedRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTimeModified");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTimeModified");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion

            List<string> namefilter = new List<string>();
            foreach (var name in companyName)
            {
                if (name.ToString() == "All")
                {
                    namefilter.Clear();
                    break;
                }
                else
                {
                    namefilter.Add(name);
                }
            }

            if (namefilter.Count > 0)
            {
               if ((TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "SalesRecieptQueryRq"))
                {
                    foreach (var name in namefilter)
                    {
                        List<string> customerListId = GetCustomerListIdFromQBPOS(QBFileName, TransTypeReq, name.ToString());

                        if (customerListId.Count > 0)
                        {
                            foreach (var customerId in customerListId)
                            {
                                XmlElement NameFilter = pxmldoc.CreateElement("CustomerListID");
                                NameFilter.InnerText = customerId;
                                queryRequest.AppendChild(NameFilter);

                            }
                        }

                        break;
                    }
                }
            }

            if (TransTypeReq == "SalesOrderQueryRq")
            {
                XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("SalesOrderNumberRangeFilter");
                queryRequest.AppendChild(RefNumberRangeFilter);
                //Adding modified date filters.
                XmlElement FromRefNumber = pxmldoc.CreateElement("FromSalesOrderNumber");

                RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

                XmlElement ToRefNumber = pxmldoc.CreateElement("ToSalesOrderNumber");
                RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            }

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.BeginSession(QBFileName);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }



        /// <summary>
        /// This method is used for getting QuickBooks list 
        /// using Txndate and ref number filter.
        /// </summary>
        /// <param name="TransTypeReq">Type of Transaction</param>
        /// <param name="QBFileName">QuickBooks company file</param>
        /// <param name="FromDate">From Txn date</param>
        /// <param name="ToDate">To Txn Date</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number.</param>
        /// <returns></returns>
        public static string GetListFromQuickBookTxnDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, string entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            if (entityFilter != "All")
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                EntityFilter.AppendChild(entityFilterFullName).InnerText = entityFilter;
            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string GetListFromQuickBookTxnDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");

            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemReceiptQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getting QuickBooks List
        public static string GetListFromQuickBookEntityFilter(string TransTypeReq, string QBFileName, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                if (TransTypeReq == "BillingRateQueryRq")
                {
                    foreach (var item in entityFilter)
                    {
                        XmlElement NameFilter = pxmldoc.CreateElement("FullName");
                        NameFilter.InnerText = item.Replace("     :     BillingRate", string.Empty);
                        queryRequest.AppendChild(NameFilter);
                    }
                }
                else if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "ClassQueryRq")
                {
                    XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                    queryRequest.AppendChild(EntityFilter);
                    foreach (var item in entityFilter)
                    {
                        XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                        EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                    }
                }
                else if (TransTypeReq == "ClassQueryRq")
                {
                    XmlElement EntityFilter = pxmldoc.CreateElement("NameFilter");
                    queryRequest.AppendChild(EntityFilter);
                    foreach (var item in entityFilter)
                    {
                        XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                        EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                    }
                }
                else
                {
                    XmlElement EntityFilter = pxmldoc.CreateElement("TimeTrackingEntityFilter");
                    queryRequest.AppendChild(EntityFilter);
                    foreach (var item in entityFilter)
                    {
                        XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                        EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                    }
                }
            }
            //bug 485 axis 12
            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "VehicleMileageQueryRq" &&
                TransTypeReq != "BillingRateQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }


            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq" || TransTypeReq == "CheckQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144


            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string GetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, string paidStatus, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        /// using Paid Status filter.
        /// </summary>
        /// <param name="TransTypeReq"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string GetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, string paidStatus)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            //For Paid Status
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }

            return responseList;
        }

        public static string GetListFromQuickBookDatePaid(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string paidStatus, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
               
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        /// <summary>
        /// This method is used for getting QuickBooks List
        /// using TxnDateRangeFilter and paidStatus
        /// </summary>
        /// <param name="TransTypeReq"></param>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string GetListFromQuickBookDatePaid(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string paidStatus)
        {
            //Create a xml document
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            ////Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            //Adding PaidStatus
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144

            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;

                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string ModifiedGetListFromQuickBookDatePaid(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string paidStatus, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            //XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            //queryRequest.AppendChild(ModifiedDateRangeFilter);
            //XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss");
            //XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss");
            #endregion

            #region For Modified Date
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }

            if (TransTypeReq == "InvoiceQueryRq" || TransTypeReq == "BillQueryRq")
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            }
            else
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            }
            #endregion


            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getting QuickBooks List
        /// using TxnDateRangeFilter and paidStatus
        /// </summary>
        /// <param name="TransTypeReq"></param>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string ModifiedGetListFromQuickBookDatePaid(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string paidStatus)
        {
            //Create a xml document
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion


            //Adding PaidStatus
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string GetListFromQuickBookRefPaid(string TransTypeReq, string QBFileName, string FromRefnum, string ToRefnum, string paidStatus, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getiing QuickBooks List
        /// using RefNumberRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="TransTypeReq"></param>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string GetListFromQuickBookRefPaid(string TransTypeReq, string QBFileName, string FromRefnum, string ToRefnum, string paidStatus)
        {
            //Create a xml document 
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;

            //Adding PaidStatus
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getting QuickBooks List
        /// using TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="TrnasTypeReq"></param>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string GetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string paidStatus, List<string> entityFilter)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            ////Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;

            //Adding PaidStatus
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        /// <summary>
        /// This method is used for getting QuickBooks List
        /// using TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="TrnasTypeReq"></param>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="paidStatus"></param>
        /// <returns></returns>
        public static string GetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string paidStatus)
        {
            //Create a xml document for Sales Order List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            ////Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");

            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");

            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            #endregion


            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;

            //Adding PaidStatus
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            if (TransTypeReq != "TimeTrackingQueryRq")
            {
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }

        public static string ModifiedGetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string paidStatus, List<string> entityFilter)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region For Modified Date

            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromdDate = pxmldoc.CreateElement("FromModifiedDate");

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(FromdDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }

            //Axis 671 DESKTOP
            if (TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            }
            else
            {
                XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
                //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
                ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            }
            #endregion

            //#region Commented Code
            //XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            //queryRequest.AppendChild(ModifiedDateRangeFilter);
            //XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            //XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-dd");
            //#endregion

            if (entityFilter.Count != 0 && !entityFilter.Contains("All"))
            {
                XmlElement EntityFilter = pxmldoc.CreateElement("EntityFilter");
                queryRequest.AppendChild(EntityFilter);
                foreach (var item in entityFilter)
                {
                    XmlElement entityFilterFullName = pxmldoc.CreateElement("FullName");
                    EntityFilter.AppendChild(entityFilterFullName).InnerText = item.Replace("     :     Employee", string.Empty);
                }

            }

            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;

            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;

            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq" || TransTypeReq == "BillQueryRq" || TransTypeReq == "ItemReceiptQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            //if (TransTypeReq == "BillQueryRq")
            //{
            //    XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
            //    queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

            //    XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            //    queryRequest.AppendChild(ownerID).InnerText = "0";
            //}
            //End Axis bug no 144

            // bug 485 item receipt
            //if (TransTypeReq == "ItemReceiptQueryRq")
            //{
            //    XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
            //    queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            //    XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
            //    queryRequest.AppendChild(OwnerID).InnerText = "0";
            //}
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485




            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        public static string ModifiedGetListFromQuickBookPaidStatus(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string paidStatus)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion
            //Axis 11.0  bug 126 
            if (TransTypeReq == "InvoiceQueryRq")
            {
                //bug 147 Axis 12.0
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  
            if (TransTypeReq == "BillQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");
            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefnum;
            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefnum;
            XmlElement ValuePaidStatus = pxmldoc.CreateElement("PaidStatus");
            queryRequest.AppendChild(ValuePaidStatus).InnerText = paidStatus;
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
               
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
        /// <summary>
        /// This method is used for getting Item list by passing
        /// Item full name.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="fullName">Item Full name</param>
        /// <returns></returns>
        public static string GetItemList(string QBFileName, string fullName)
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemQueryRq);
            ItemQueryRq.SetAttribute("requestID", "1");
            //Adding modified date filters.
            XmlElement FullName = pxmldoc.CreateElement("FullName");
            ItemQueryRq.AppendChild(FullName).InnerText = fullName;

            XmlElement includeRetItems = pxmldoc.CreateElement("IncludeRetElement");   //for bug 608 and 612
            ItemQueryRq.AppendChild(includeRetItems).InnerText = "";
            XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
            ItemQueryRq.AppendChild(ownerID).InnerText = "0";
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfItem = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseOfItem = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseOfItem != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseOfItem);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
            }
            catch
            {

            }
            return responseOfItem;
        }

        /// <summary>
        /// This method is used for getting Customer list 
        /// by passing customer name.
        /// </summary>
        /// <returns></returns>
        public static string GetCustomerListByName(string QBFileName, string name)
        {
            //Executing Customer Query
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
            qbXMLMsgsRq.AppendChild(CustomerQueryRq);
            CustomerQueryRq.SetAttribute("requestID", "1");
            XmlElement FullName = pxmldoc.CreateElement("FullName");
            CustomerQueryRq.AppendChild(FullName).InnerText = name;

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }


            response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (response != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(response);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
            }
            catch
            {

            }
            return response;
        }

        /// <summary>
        /// This method is used for getting Customer list 
        /// by passing customer id.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="listID">Customer list id.</param>
        /// <returns></returns>
        public static string GetCustomerList(string QBFileName, string listID)
        {
            //Executing Customer Query
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
            qbXMLMsgsRq.AppendChild(CustomerQueryRq);
            CustomerQueryRq.SetAttribute("requestID", "1");
            XmlElement ListID = pxmldoc.CreateElement("ListID");
            CustomerQueryRq.AppendChild(ListID).InnerText = listID;

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }


            response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (response != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(response);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
            }
            catch
            {

            }
            return response;
        }

        /// <summary>
        /// This method is used for getting Vendor list 
        /// by passing customer id.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="listID">Customer list id.</param>
        /// <returns></returns>
        public static string GetVendorList(string QBFileName, string listID)
        {
            //Executing Customer Query
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement CustomerQueryRq = pxmldoc.CreateElement("VendorQueryRq");
            qbXMLMsgsRq.AppendChild(CustomerQueryRq);
            CustomerQueryRq.SetAttribute("requestID", "1");
            XmlElement ListID = pxmldoc.CreateElement("ListID");
            CustomerQueryRq.AppendChild(ListID).InnerText = listID;

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (response != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(response);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
            }
            catch
            {

            }
            response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            return response;
        }

        public static string GetPriceFromItemSerivceQuery(string QBFileName, string ItemServiceName)
        {
            //Create a xml document for Item List
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmldoc.CreateElement("QBXML");
            xmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement EmployeeQueryRq = xmldoc.CreateElement("ItemServiceQueryRq");
            qbXMLMsgsRq.AppendChild(EmployeeQueryRq);
            EmployeeQueryRq.SetAttribute("requestID", "1");

            //Adding EntityFullName as FullName to the Request.
            XmlElement FullName = xmldoc.CreateElement("FullName");
            EmployeeQueryRq.AppendChild(FullName).InnerText = ItemServiceName;

            //Adding IncludeRetElement to get only BillingRateRef.
            //XmlElement IncludeRetElement = xmldoc.CreateElement("IncludeRetElement");
            //EmployeeQueryRq.AppendChild(IncludeRetElement).InnerText = "BillingRateRef";

            string pinput = xmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;
            string ItemServicePrice = string.Empty;

            try
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {

                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);

                }
                else
                {
                    ticket = CommonUtilities.GetInstance().ticketvalue;

                }

                //Processing the request.
                response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return ItemServicePrice;
            }
            XmlDocument outputXMLDocOfItemService = new XmlDocument();

            if (response != string.Empty)
            {
                //Loading Employee List into XML.
                outputXMLDocOfItemService.LoadXml(response);

                //Getting BillingRateName from QuickBooks response.
                XmlNodeList ItemServiceList = outputXMLDocOfItemService.GetElementsByTagName("ItemServiceRet");

                foreach (XmlNode ItemServiceNodes in ItemServiceList)
                {
                    foreach (XmlNode node in ItemServiceNodes.ChildNodes)
                    {
                        //Checking BillingRateName.
                        if (node.Name.Equals("SalesOrPurchase"))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals("Price"))
                                    ItemServicePrice = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals("SalesAndPurchase"))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals("SalesPrice"))
                                    ItemServicePrice = childNode.InnerText;
                            }
                        }
                    }
                }
            }
            return ItemServicePrice;
        }



        /// <summary>
        /// Get SalesTaxCodeRet.ItemSalesTaxRef.FullName
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public static string GetItemSaleTaxFullName(string QBFileName, string FullName)
        {
            string SaleTaxFullName = string.Empty;

            //Executing SalesTaxCodeQuery
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
            xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmldoc.CreateElement("QBXML");
            xmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

            XmlElement SalesTaxCodeQueryRq = xmldoc.CreateElement("SalesTaxCodeQueryRq");
            qbXMLMsgsRq.AppendChild(SalesTaxCodeQueryRq);

            SalesTaxCodeQueryRq.SetAttribute("requestID", "1");

            XmlElement fullname = xmldoc.CreateElement("FullName");
            fullname.InnerText = FullName;
            SalesTaxCodeQueryRq.AppendChild(fullname);

            string input = xmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                //ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);

                response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, input);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception(ex.Message);
            }

            XmlDocument outputXMLDoc = new XmlDocument();

            if (response != string.Empty)
            {
                //Loading ItemSalesTaxCode List into XML.
                outputXMLDoc.LoadXml(response);

                #region Getting Values from XML

                //Getting values from QuickBooks response.
                XmlNodeList nodeList = outputXMLDoc.GetElementsByTagName("SalesTaxCodeRet");
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode nodes in node.ChildNodes)
                    {
                        if (nodes.Name.Equals("ItemSalesTaxRef"))
                        {
                            foreach (XmlNode nodeFullName in nodes.ChildNodes)
                            {
                                if (nodeFullName.Name.Equals("FullName"))
                                {
                                    SaleTaxFullName = nodeFullName.InnerText;
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            return SaleTaxFullName;


        }




        /// <summary>
        /// Get SalesTaxCodeRet.ItemPurchaseTaxRef.FullName
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public static string GetItemPurchaseTaxFullName(string QBFileName, string FullName)
        {
            string SaleTaxFullName = string.Empty;

            //Executing SalesTaxCodeQuery
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
            xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmldoc.CreateElement("QBXML");
            xmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

            XmlElement SalesTaxCodeQueryRq = xmldoc.CreateElement("SalesTaxCodeQueryRq");
            qbXMLMsgsRq.AppendChild(SalesTaxCodeQueryRq);

            SalesTaxCodeQueryRq.SetAttribute("requestID", "1");

            XmlElement fullname = xmldoc.CreateElement("FullName");
            fullname.InnerText = FullName;
            SalesTaxCodeQueryRq.AppendChild(fullname);

            string input = xmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            try
            {
                // ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, input);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception(ex.Message);
            }

            XmlDocument outputXMLDoc = new XmlDocument();

            if (response != string.Empty)
            {
                //Loading ItemSalesTaxCode List into XML.
                outputXMLDoc.LoadXml(response);

                #region Getting Values from XML

                //Getting values from QuickBooks response.
                XmlNodeList nodeList = outputXMLDoc.GetElementsByTagName("SalesTaxCodeRet");
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode nodes in node.ChildNodes)
                    {
                        if (nodes.Name.Equals("ItemPurchaseTaxRef"))
                        {
                            foreach (XmlNode nodeFullName in nodes.ChildNodes)
                            {
                                if (nodeFullName.Name.Equals("FullName"))
                                {
                                    SaleTaxFullName = nodeFullName.InnerText;
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            return SaleTaxFullName;


        }




        /// <summary>
        /// This method is used to get "TaxRate" from "SalesTaxCode".
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fullname"></param>
        /// <returns></returns>
        public static string GetTaxRateFromSalesTaxCode(string QBFileName, string name)
        {
            string TaxRate = string.Empty;

            //Executing SalesTaxCodeQuery
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
            xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmldoc.CreateElement("QBXML");
            xmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemSalesTaxQueryRq = xmldoc.CreateElement("ItemSalesTaxQueryRq");
            qbXMLMsgsRq.AppendChild(ItemSalesTaxQueryRq);
            ItemSalesTaxQueryRq.SetAttribute("requestID", "1");
            XmlElement nameFilter = xmldoc.CreateElement("NameFilter");
            ItemSalesTaxQueryRq.AppendChild(nameFilter);

            //Creating child for NameFilter.
            XmlElement matchCriteria = xmldoc.CreateElement("MatchCriterion");
            matchCriteria.InnerText = "Contains";
            nameFilter.AppendChild(matchCriteria);

            XmlElement Name = xmldoc.CreateElement("Name");
            Name.InnerText = name;
            nameFilter.AppendChild(Name);

            string input = xmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }


            try
            {
                //ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, input);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception(ex.Message);
            }

            XmlDocument outputXMLDoc = new XmlDocument();

            if (response != string.Empty)
            {
                //Loading ItemSalesTaxCode List into XML.
                outputXMLDoc.LoadXml(response);

                #region Getting Values from XML

                //Getting values from QuickBooks response.
                XmlNodeList nodeList = outputXMLDoc.GetElementsByTagName("ItemSalesTaxRet");
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode nodes in node.ChildNodes)
                    {
                        if (nodes.Name.Equals("TaxRate"))
                        {
                            TaxRate = nodes.InnerText;
                        }
                    }
                }
                #endregion
            }

            return TaxRate;
        }


        /// <summary>
        ///Only Since Last Export overload
        /// </summary>
        /// <param name="TransTypeReq">Type of transaction</param>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="FromDate">From Modified Date</param>
        /// <param name="ToDate">To Modified date</param>
        /// <returns></returns>
        public static string GetListFromQuickBookModifiedDate(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            //Create a xml document for Price Level List.
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("PriceLevelQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");


            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");

            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            #endregion




            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "PriceLevelQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
            }//End Axis bug no 485


            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            //XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(responseList);
            //xmldoc.Save("C://PurchaseOrder.xml");
            return responseList;
        }

        //for bug no. 395
        public static string GetListFromQuickBookModifiedDateForInActiveFilter(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate)
        {
            //Create a xml document for Price Level List.
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("PriceLevelQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            //bug no. 395
            XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
            queryRequest.AppendChild(ActiveStatus).InnerText = "All";

            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("ModifiedDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            //Adding modified date filters.
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");


            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();


            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");

            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            #endregion




            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "PriceLevelQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }
            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }

            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            //XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(responseList);
            //xmldoc.Save("C://PurchaseOrder.xml");
            return responseList;
        }



        /// <summary>
        /// This method is used for getting the list from quickbooks 
        /// using Modified Date.
        /// </summary>
        /// <param name="TransTypeReq">Type of transaction</param>
        /// <param name="QBFileName">QuickBooks company file name</param>
        /// <param name="FromDate">From Modified Date</param>
        /// <param name="ToDate">To Modified date</param>
        /// <returns></returns>
        public static string GetListFromQuickBookModifiedDate(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Price Level List.
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("PriceLevelQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            //following code commented for inactive filter- bug no. 395
            // XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
            // queryRequest.AppendChild(ActiveStatus).InnerText = "All";
            //Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion
            #region For Modified Date
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion




            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "PriceLevelQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            //XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(responseList);
            //xmldoc.Save("C://PurchaseOrder.xml");
            return responseList;
        }

        //for bug no. 395
        public static string GetListFromQuickBookModifiedDateForInActiveFilter(string TransTypeReq, string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //Create a xml document for Price Level List.
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);//("PriceLevelQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");


            #region For Modified Date
            //bug no. 395
            XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
            queryRequest.AppendChild(ActiveStatus).InnerText = "All";
            //Adding modified date filters.
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromModifiedDate");
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeSpan fromOffset = localZone.GetUtcOffset(FromDate);
            string strfromOffset = string.Empty;
            if (!fromOffset.ToString().Contains("-") && !fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("-"))
            {
                strfromOffset = fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }
            else if (fromOffset.ToString().Contains("+"))
            {
                strfromOffset = "+" + fromOffset.ToString().Substring(0, fromOffset.ToString().Length - 3);
            }

            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            queryRequest.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss") + strfromOffset.ToString();

            TimeSpan toOffset = localZone.GetUtcOffset(ToDate);
            string strtoOffset = string.Empty;
            if (!toOffset.ToString().Contains("-") && !toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("-"))
            {
                strtoOffset = toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            else if (toOffset.ToString().Contains("+"))
            {
                strtoOffset = "+" + toOffset.ToString().Substring(0, toOffset.ToString().Length - 3);
            }
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToModifiedDate");
            queryRequest.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion
            #region For Modified Date
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + "-00:60";
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = ToDate.ToString("yyyy-MM-ddTHH:mm:ss") + strtoOffset.ToString();
            #endregion




            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "PriceLevelQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            // bug 485 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";
                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";


            }//End Axis bug no 485
            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            //XmlDocument xmldoc = new XmlDocument();
            //xmldoc.LoadXml(responseList);
            //xmldoc.Save("C://PurchaseOrder.xml");
            return responseList;
        }


        public static async System.Threading.Tasks.Task GetQBOnlineData()
        {

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            DataService dataService = new DataService(serviceContext);

            QueryService<SalesReceipt> customerQueryService = new QueryService<SalesReceipt>(serviceContext);
            TransactionImporter.TransactionImporter frmImporter = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            // ComboBox cbTemplates = (ComboBox)frmImporter.comboBoxCodingTemplate;
            //607
            //DataGridView datagrid = (DataGridView)frmImporter.dataGridViewResult;
            //datagrid.DataSource = customerQueryService.ExecuteIdsQuery("Select * From SalesReceipt").ToList();
        }

        //Axis 671 DESKTOP 
        public static string GetListFromQuickBook(string TransTypeReq, string QBFileName, DateTime FromDate, string ToDate, string FromRefNum, string ToRefNum)
        {
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = pxmldoc.CreateElement(TransTypeReq);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            #region Commented Code
            XmlElement ModifiedDateRangeFilter = pxmldoc.CreateElement("TxnDateRangeFilter");
            queryRequest.AppendChild(ModifiedDateRangeFilter);
            XmlElement FromModifiedDate = pxmldoc.CreateElement("FromTxnDate");
            ModifiedDateRangeFilter.AppendChild(FromModifiedDate).InnerText = FromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = pxmldoc.CreateElement("ToTxnDate");
            ModifiedDateRangeFilter.AppendChild(ToModifiedDate).InnerText = string.Empty;
            #endregion


            XmlElement RefNumberRangeFilter = pxmldoc.CreateElement("RefNumberRangeFilter");
            queryRequest.AppendChild(RefNumberRangeFilter);
            //Adding modified date filters.
            XmlElement FromRefNumber = pxmldoc.CreateElement("FromRefNumber");

            RefNumberRangeFilter.AppendChild(FromRefNumber).InnerText = FromRefNum;

            XmlElement ToRefNumber = pxmldoc.CreateElement("ToRefNumber");
            RefNumberRangeFilter.AppendChild(ToRefNumber).InnerText = ToRefNum;

            if (TransTypeReq != "TimeTrackingQueryRq" && TransTypeReq != "BuildAssemblyQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement IncludeLineItems = pxmldoc.CreateElement("IncludeLineItems");
                queryRequest.AppendChild(IncludeLineItems).InnerText = "1";
            }

            if (TransTypeReq == "ItemInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemNonInventoryQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            if (TransTypeReq == "ItemServiceQueryRq")
            {
                //For the bug 1492 (Axis 6.0)
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //Axis 11.0  bug 126 //bug 147 Axis 12.0
            if (TransTypeReq == "SalesOrderQueryRq" || TransTypeReq == "CreditMemoQueryRq" || TransTypeReq == "BillPaymentCreditCardQueryRq" || TransTypeReq == "CreditCardChargeQueryRq" || TransTypeReq == "CreditCardCreditQueryRq" || TransTypeReq == "CheckQueryRq" || TransTypeReq == "PurchaseOrderQueryRq" || TransTypeReq == "EstimateQueryRq" || TransTypeReq == "SalesReceiptQueryRq")
            {
                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }

            //Axis Bug No 144  //bug 147 Axis 12.0
            if (TransTypeReq == "BillQueryRq" || TransTypeReq == "InvoiceQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                XmlElement ownerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(ownerID).InnerText = "0";
            }
            //End Axis bug no 144
            // Bug 485 axis 12 item receipt
            if (TransTypeReq == "ItemReceiptQueryRq")
            {

                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

                //XmlElement IncludeRetElement = pxmldoc.CreateElement("IncludeRetElement");
                //queryRequest.AppendChild(IncludeRetElement).InnerText = "1";

                XmlElement OwnerID = pxmldoc.CreateElement("OwnerID");
                queryRequest.AppendChild(OwnerID).InnerText = "0";
            }
            //Axis Bug No 485 axis 12.0  
            if (TransTypeReq == "VendorCreditQueryRq")
            {
                XmlElement IncludeLinkedTxns = pxmldoc.CreateElement("IncludeLinkedTxns");
                queryRequest.AppendChild(IncludeLinkedTxns).InnerText = "1";

            }//End Axis bug no 485

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseList = string.Empty;
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }

            //Processing the request.
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            try
            {
                string logDirectory = CommonUtilities.GetInstance().logDirectoryPath;
                
                pxmldoc.Save(logDirectory + @"\ExportRequest.xml");
                if (responseList != string.Empty)
                {
                    XmlDocument responseDoc = new XmlDocument();
                    responseDoc.LoadXml(responseList);
                    responseDoc.Save(logDirectory + @"\ExportResponse.xml");
                }
                #region Code for Export Data Error Summary
                string statusMessage = string.Empty;
                string responseFile = string.Empty;
                if (responseList != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(responseList);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TransTypeReq.Replace("Rq", "Rs")))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Info" || statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                            if (!statusMessage.Contains("Status OK"))
                            {
                                statusMessage = "\nThe Export Error is : ";
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            }
                            else
                            {
                                statusMessage = string.Empty;
                            }
                            CommonUtilities.GetInstance().sb.AppendLine(statusMessage.ToString());
                        }
                    }
                }
                #endregion
            }
            catch
            {

            }
            return responseList;
        }
    }

    public class QBItemDetails
    {

        private List<string> m_GroupCustomerFullName = new List<string>();
        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();

        //for export PurchaseOrderLineRet;    

        protected List<string> m_QBitemFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected List<string> m_UOM = new List<string>();
        protected List<string> m_OverrideUOMFullName = new List<string>();
        protected List<decimal> m_Rate = new List<decimal>();
        protected List<decimal> m_Amount = new List<decimal>();
        protected List<string> m_SalesTaxCodeFullName = new List<string>();
        protected List<string> m_ManufacturerPartNumber = new List<string>();
        protected List<string> m_ServiceDate = new List<string>();
        protected List<string> m_LineClass = new List<string>();
        protected List<string> m_LineIsManuallyClosed = new List<string>();
        protected List<string> m_ReceivedQuantity = new List<string>();
        protected List<string> m_LineOther1 = new List<string>();
        protected List<string> m_LineOther2 = new List<string>();

        //for export PurchaseOrderLineGroupRet;
        protected List<string> m_GroupTxnLineID = new List<string>();
        protected List<string> m_ItemGroupFullName = new List<string>();
        protected List<string> m_GroupDesc = new List<string>();
        protected List<decimal> m_GroupQuantity = new List<decimal>();
        protected List<string> m_IsPrintItemsInGroup = new List<string>();
        protected List<string> m_CustomerRefFullName = new List<string>();
        //Sub PurchaseOrderLineRet;
        protected List<string> m_GroupQBTxnLineNumber = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<decimal> m_GroupLineQuantity = new List<decimal>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<string> m_GroupLineOverrideUOM = new List<string>();
        protected List<decimal> m_GroupLineRate = new List<decimal>();
        protected List<string> m_GroupLineClassRefFullName = new List<string>();
        protected List<decimal> m_GroupLineAmount = new List<decimal>();
        protected List<string> m_GroupLineServiceDate = new List<string>();
        protected List<string> m_GroupLineSalesTaxCodeFullName = new List<string>();
        protected List<string> m_QBTxnLineNumber = new List<string>();
        protected List<decimal> m_QBquantity = new List<decimal>();



        public List<string> LineOther1
        {
            get { return m_LineOther1; }
            set { m_LineOther1 = value; }
        }

        public List<string> LineOther2
        {
            get { return m_LineOther2; }
            set { m_LineOther2 = value; }
        }


        //For InvoiceLineRet
        public List<string> TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_QBitemFullName; }
            set { m_QBitemFullName = value; }
        }

        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal> Quantity
        {
            get { return m_QBquantity; }
            set { m_QBquantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<string> OverrideUOMFullName
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }

        public List<decimal> Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }
        public List<string> SalesTaxCodeFullName
        {
            get { return m_SalesTaxCodeFullName; }
            set { m_SalesTaxCodeFullName = value; }
        }
        public List<decimal> Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public List<string> ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }

        public List<string> ReceivedQuantity
        {
            get { return m_ReceivedQuantity; }
            set { m_ReceivedQuantity = value; }
        }

        public List<string> LineIsManuallyClosed
        {
            get { return m_LineIsManuallyClosed; }
            set { m_LineIsManuallyClosed = value; }
        }
        //For PurchaseOrderLineGroupRet
        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemGroupFullName; }
            set { m_ItemGroupFullName = value; }
        }

        public List<string> GroupDesc
        {
            get { return m_GroupDesc; }
            set { m_GroupDesc = value; }
        }

        public List<decimal> GroupQuantity
        {
            get { return m_GroupQuantity; }
            set { m_GroupQuantity = value; }
        }

        public List<string> IsPrintItemsInGroup
        {
            get { return m_IsPrintItemsInGroup; }
            set { m_IsPrintItemsInGroup = value; }
        }


        //For Sub InvoiceLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupQBTxnLineNumber; }
            set { m_GroupQBTxnLineNumber = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<string> GroupLineOverrideUOM
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }
        public List<string> GroupLineCustomerRef
        {
            get { return m_GroupCustomerFullName; }
            set { m_GroupCustomerFullName = value; }
        }


        public List<decimal> GroupLineRate
        {
            get { return m_GroupLineRate; }
            set { m_GroupLineRate = value; }
        }
        public List<string> GroupLineClassRefFullName
        {
            get { return m_GroupLineClassRefFullName; }
            set { m_GroupLineClassRefFullName = value; }
        }
        public List<decimal> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineServiceDate
        {
            get { return m_GroupLineServiceDate; }
            set { m_GroupLineServiceDate = value; }
        }
        //Get or Set CustomerRefFullName value.
        public List<string> CustomerRefFullName
        {
            get
            {
                return m_CustomerRefFullName;
            }
            set
            {

                m_CustomerRefFullName = value;
            }
        }



        public List<string> ManufacturerPartNumber
        {
            get { return m_ManufacturerPartNumber; }
            set { m_ManufacturerPartNumber = value; }
        }
        public List<string> LineClass
        {
            get { return m_LineClass; }
            set { m_LineClass = value; }
        }
    }

    public class QBPurchaseOrderItem
    {
        protected string[] m_ListID = new string[30000];
        protected string[] m_ALU = new string[30000];
        protected string[] m_Attribute = new string[30000];
        protected decimal?[] m_Cost = new decimal?[30000];
        protected string[] m_Desc1 = new string[30000];
        protected string[] m_Desc2 = new string[30000];
        protected decimal?[] m_ExtendedCost = new decimal?[30000];
        protected string[] m_NumberOfBaseUnits = new string[30000];
        protected string[] m_ItemNumber = new string[30000];
        protected string[] m_Qty = new string[30000];
        protected string[] m_QtyReceived = new string[30000];
        protected string[] m_Size = new string[30000];
        protected string[] m_UnitOfMeasure = new string[30000];
        protected string[] m_UPC = new string[30000];

        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string[] ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }

        public string[] Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }
        public decimal?[] Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }
        public string[] Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }
        public string[] Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }
        public decimal?[] ExtendedCost
        {
            get { return m_ExtendedCost; }
            set { m_ExtendedCost = value; }
        }
        public string[] NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }
        public string[] ItemNumber
        {
            get { return m_ItemNumber; }
            set { m_ItemNumber = value; }
        }
        public string[] Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }
        public string[] LineQtyReceived
        {
            get { return m_QtyReceived; }
            set { m_QtyReceived = value; }
        }

        public string[] Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }
        public string[] UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
        public string[] UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }


    }

    public class QBSalesOrderItem
    {
        protected string[] m_SalesListID = new string[30000];

        protected string[] m_SalesALU = new string[30000];
        protected string[] m_SalesAssociate = new string[30000];
        protected string[] m_SalesAttribute = new string[30000];
        protected decimal?[] m_SalesCommission = new decimal?[30000];
        protected string[] m_SalesDesc1 = new string[30000];
        protected string[] m_SalesDesc2 = new string[30000];
        protected decimal?[] m_SalesDiscount = new decimal?[30000];
        protected string[] m_SalesDiscountPercent = new string[30000];
        protected string[] m_SalesDiscountType = new string[30000];
        protected string[] m_SalesDiscountSource = new string[30000];
        protected decimal?[] m_SalesExtendedPrice = new decimal?[30000];
        protected decimal?[] m_SalesExtendedTax = new decimal?[30000];
        protected string[] m_SalesItemNumber = new string[30000];
        protected string[] m_SalesNumberOfBaseUnits = new string[30000];
        protected decimal?[] m_SalesPrice = new decimal?[30000];
        protected string[] m_SalesPriceLevelNumber = new string[30000];
        protected string[] m_SalesQty = new string[30000];
        protected string[] m_SalesQtySold = new string[30000];
        protected string[] m_SalesSerialNumber = new string[30000];
        protected string[] m_SalesSize = new string[30000];
        protected decimal?[] m_SalesTaxAmount = new decimal?[30000];
        protected string[] m_SalesTaxCode = new string[30000];
        protected string[] m_SalesTaxPercentage = new string[30000];
        protected string[] m_SalesUnitOfMeasure = new string[30000];
        protected string[] m_SalesUPC = new string[30000];
        protected string[] m_SalesManufacturer = new string[30000];
        protected string[] m_SalesWeight = new string[30000];

        public string[] ListID
        {
            get { return m_SalesListID; }
            set { m_SalesListID = value; }
        }


        public string[] SalesALU
        {
            get { return m_SalesALU; }
            set { m_SalesALU = value; }
        }

        public string[] SalesAssociate
        {
            get { return m_SalesAssociate; }
            set { m_SalesAssociate = value; }
        }

        public string[] SalesAttribute
        {
            get { return m_SalesAttribute; }
            set { m_SalesAttribute = value; }
        }


        public decimal?[] SalesCommission
        {
            get { return m_SalesCommission; }
            set { m_SalesCommission = value; }
        }


        public string[] SalesDesc1
        {
            get { return m_SalesDesc1; }
            set { m_SalesDesc1 = value; }
        }


        public string[] SalesDesc2
        {
            get { return m_SalesDesc2; }
            set { m_SalesDesc2 = value; }
        }


        public decimal?[] SalesDiscount
        {
            get { return m_SalesDiscount; }
            set { m_SalesDiscount = value; }
        }


        public string[] SalesDiscountPercent
        {
            get { return m_SalesDiscountPercent; }
            set { m_SalesDiscountPercent = value; }
        }


        public string[] SalesDiscountType
        {
            get { return m_SalesDiscountType; }
            set { m_SalesDiscountType = value; }
        }


        public string[] SalesDiscountSource
        {
            get { return m_SalesDiscountSource; }
            set { m_SalesDiscountSource = value; }
        }


        public decimal?[] SalesExtendedPrice
        {
            get { return m_SalesExtendedPrice; }
            set { m_SalesExtendedPrice = value; }
        }


        public decimal?[] SalesExtendedTax
        {
            get { return m_SalesExtendedTax; }
            set { m_SalesExtendedTax = value; }
        }


        public string[] SalesItemNumber
        {
            get { return m_SalesItemNumber; }
            set { m_SalesItemNumber = value; }
        }


        public string[] SalesNumberOfBaseUnits
        {
            get { return m_SalesNumberOfBaseUnits; }
            set { m_SalesNumberOfBaseUnits = value; }
        }

        public decimal?[] SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }


        public string[] SalesPriceLevelNumber
        {
            get { return m_SalesPriceLevelNumber; }
            set { m_SalesPriceLevelNumber = value; }
        }

        public string[] SalesQty
        {
            get { return m_SalesQty; }
            set { m_SalesQty = value; }
        }

        public string[] SalesQtySold
        {
            get { return m_SalesQtySold; }
            set { m_SalesQtySold = value; }
        }


        public string[] SalesSerialNumber
        {
            get { return m_SalesSerialNumber; }
            set { m_SalesSerialNumber = value; }
        }

        public string[] SalesSize
        {
            get { return m_SalesSize; }
            set { m_SalesSize = value; }
        }

        public decimal?[] SalesTaxAmount
        {
            get { return m_SalesTaxAmount; }
            set { m_SalesTaxAmount = value; }
        }

        public string[] SalesTaxCode
        {
            get { return m_SalesTaxCode; }
            set { m_SalesTaxCode = value; }
        }


        public string[] SalesTaxPercentage
        {
            get { return m_SalesTaxPercentage; }
            set { m_SalesTaxPercentage = value; }
        }


        public string[] SalesUnitOfMeasure
        {
            get { return m_SalesUnitOfMeasure; }
            set { m_SalesUnitOfMeasure = value; }
        }


        public string[] SalesUPC
        {
            get { return m_SalesUPC; }
            set { m_SalesUPC = value; }
        }


        public string[] SalesManufacturer
        {
            get { return m_SalesManufacturer; }
            set { m_SalesManufacturer = value; }
        }

        public string[] SalesWeight
        {
            get { return m_SalesWeight; }
            set { m_SalesWeight = value; }
        }



    }

    public class QBVoucherItem
    {
        protected string[] m_ListID = new string[30000];
        protected string[] m_ALU = new string[30000];
        protected string[] m_Attribute = new string[30000];
        protected decimal?[] m_Cost = new decimal?[30000];
        protected string[] m_Desc1 = new string[30000];
        protected string[] m_Desc2 = new string[30000];
        protected decimal?[] m_ExtendedCost = new decimal?[30000];
        protected string[] m_ItemNumber = new string[30000];
        protected string[] m_NumberOfBaseUnits = new string[30000];
        protected string[] m_OriginalOrderQty = new string[30000];
        protected string[] m_QtyReceived = new string[30000];
        protected string[] m_SerialNumber = new string[30000];
        protected string[] m_Size = new string[30000];
        protected string[] m_UPC = new string[30000];


        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string[] ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }


        public string[] Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }

        public decimal?[] Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }


        public string[] Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }

        public string[] Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }


        public decimal?[] ExtendedCost
        {
            get { return m_ExtendedCost; }
            set { m_ExtendedCost = value; }
        }


        public string[] ItemNumber
        {
            get { return m_ItemNumber; }
            set { m_ItemNumber = value; }
        }


        public string[] NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }


        public string[] OriginalOrderQty
        {
            get { return m_OriginalOrderQty; }
            set { m_OriginalOrderQty = value; }
        }


        public string[] QtyReceived
        {
            get { return m_QtyReceived; }
            set { m_QtyReceived = value; }
        }


        public string[] SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }


        public string[] Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        public string[] UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }
    }

    public class QBInventoryCostAdjustmentItem
    {
        protected string[] m_ListID = new string[30000];
        protected decimal?[] m_LineCostDifference = new decimal?[30000];
        protected decimal?[] m_LineNewCost = new decimal?[30000];
        protected string[] m_NumberOfBaseUnits = new string[30000];
        protected decimal?[] m_LineOldCost = new decimal?[30000];
        protected string[] m_UnitOfMeasure = new string[30000];



        //InventoryCostAdjustmentRet       

        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public decimal?[] LineCostDifference
        {
            get { return m_LineCostDifference; }
            set { m_LineCostDifference = value; }
        }

        public decimal?[] LineNewCost
        {
            get { return m_LineNewCost; }
            set { m_LineNewCost = value; }
        }

        public string[] NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }


        public decimal?[] LineOldCost
        {
            get { return m_LineOldCost; }
            set { m_LineOldCost = value; }
        }

        public string[] UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
    }

    public class QBInventoryQtyAdjustmentItem
    {
        protected string[] m_ListID = new string[30000];
        protected string[] m_LineNewQuantity = new string[30000];
        protected string[] m_NumberOfBaseUnits = new string[30000];
        protected string[] m_LineOldQuantity = new string[30000];
        protected string[] m_LineQtyDifference = new string[30000];
        protected string[] m_SerialNumber = new string[30000];
        protected string[] m_UnitOfMeasure = new string[30000];

        //InventoryCostAdjustmentRet       

        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string[] LineNewQuantity
        {
            get { return m_LineNewQuantity; }
            set { m_LineNewQuantity = value; }
        }


        public string[] NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }

        public string[] LineOldQuantity
        {
            get { return m_LineOldQuantity; }
            set { m_LineOldQuantity = value; }
        }
        public string[] LineQtyDifference
        {
            get { return m_LineQtyDifference; }
            set { m_LineQtyDifference = value; }
        }

        public string[] SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public string[] UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
    }
}
