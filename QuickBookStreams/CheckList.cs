using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;


namespace Streams
{
    /// <summary>
    /// This class is used to declare public methods and properties.
    /// </summary>
    public class CheckList : BaseCheckList
    {
        #region Public Properties

        public string TxnID
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string AccountFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public string PayeeEntityFullName
        {
            get { return m_PayeeEntityFullName; }
            set { m_PayeeEntityFullName = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public decimal? Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? AmountInHomeCurrency
        {
            get { return m_AmountInHomeCurrency; }
            set { m_AmountInHomeCurrency = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string Address1
        {
            get { return m_Address1; }
            set { m_Address1 = value; }
        }

        public string Address2
        {
            get { return m_Address2; }
            set { m_Address2 = value; }
        }

        public string Address3
        {
            get { return m_Address3; }
            set { m_Address3 = value; }
        }

        public string Address4
        {
            get { return m_Address4; }
            set { m_Address4 = value; }
        }

        public string Address5
        {
            get { return m_Address5; }
            set { m_Address5 = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string LinkedTxnID
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }

        public string LinkedRefNumber
        {
            get { return m_LinkedRefNumber; }
            set { m_LinkedRefNumber = value; }
        }

        //For ExpenseLIneRet

        public List<string> ExpTxnLineID
        {
            get { return m_ExpTxnLineID; }
            set { m_ExpTxnLineID = value; }
        }

        public List<string> ExpAccountfullName
        {
            get { return m_ExpAccountFullName; }
            set { m_ExpAccountFullName = value; }
        }

        public List<decimal?> ExpAmount
        {
            get { return m_ExpAmount; }
            set { m_ExpAmount = value; }
        }

        public List<string> ExpMemo
        {
            get { return m_ExpMemo; }
            set { m_ExpMemo = value; }
        }

        public List<string> ExpCustomerFullName
        {
            get { return m_ExpCustomerFullname; }
            set { m_ExpCustomerFullname = value; }
        }

        public List<string> ExpClassName
        {
            get { return m_ExpClassFullName; }
            set { m_ExpClassFullName = value; }
        }

        public List<string> ExpSalesTaxCodefullName
        {
            get { return m_ExpSalesTaxCodeFullName; }
            set { m_ExpSalesTaxCodeFullName = value; }
        }

        public List<string> ExpBillableStatus
        {
            get { return m_ExpBillableStatus; }
            set { m_ExpBillableStatus = value; }
        }

        public List<string> ExpSalesRepRefFullName
        {
            get { return m_ExpSalesRepRefFullName; }
            set { m_ExpSalesRepRefFullName = value; }
        }

        //For ItemLineRet

        public List<string> ItemTxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_ItemFullName; }
            set { m_ItemFullName = value; }
        }
        // axis 10.0 changes
        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        // axis 10.0 changes ends
        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<decimal?> Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public List<decimal?> ItemAmount
        {
            get { return m_ItemAmount; }
            set { m_ItemAmount = value; }
        }

        public List<string> ItemCustomerFullName
        {
            get { return m_ItemCustomerFullName; }
            set { m_ItemCustomerFullName = value; }
        }

        public List<string> ItemClassFullName
        {
            get { return m_ItemClassName; }
            set { m_ItemClassName = value; }
        }

        public List<string> ItemSalesTaxCodeFullName
        {
            get { return m_ItemSalesTaxCodeFullName; }
            set { m_ItemSalesTaxCodeFullName = value; }
        }

        public List<string> ItemBillableStatus
        {
            get { return m_ItemBillableStatus; }
            set { m_ItemBillableStatus = value; }
        }

        public List<string> ItemSalesRepRefFullName
        {
            get { return m_ItemSalesRepRefFullName; }
            set { m_ItemSalesRepRefFullName = value; }
        }
        //For GoupItemLineRet

        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineId; }
            set { m_GroupTxnLineId = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemgroupFullName; }
            set { m_ItemgroupFullName = value; }
        }

        //for sub ItemLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<decimal?> GroupLineCost
        {
            get { return m_GroupLineCost; }
            set { m_GroupLineCost = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineCustomerFullName
        {
            get { return m_GroupLineCustomerFullName; }
            set { m_GroupLineCustomerFullName = value; }
        }

        public List<string> GroupLineClassFullName
        {
            get { return m_GroupLineClassName; }
            set { m_GroupLineClassName = value; }
        }

        public List<string> GroupLineSalesTaxCodeFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }

        public List<string> GroupLineBillableStatus
        {
            get { return m_GroupLineBillableStatus; }
            set { m_GroupLineBillableStatus = value; }
        }
        public List<string> GroupLineSalesRepRefFullName
        {
            get { return m_GroupLineSalesRepRefFullName; }
            set { m_GroupLineSalesRepRefFullName = value; }
        }
        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue =new List<string>();
        

        #endregion
    }

    /// <summary>
    /// This Class is used to get CheckList from Quickbooks.
    /// </summary>
    public class QBCheckListCollection : Collection<CheckList>
    {
        string XmlFileData = string.Empty;


        public void GetCheckValuesFromXML(string CheckXmlList)
        {
             TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (CheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCheck.LoadXml(CheckXmlList);
                XmlFileData = CheckXmlList;
                CheckList  CheckList ;
                #region Getting Check Values from XML

                //Getting Check values from QuickBooks response.
                XmlNodeList CheckNodeList = outputXMLDocOfCheck.GetElementsByTagName(QBCheckList.CheckRet.ToString());

                foreach (XmlNode CheckNodes in CheckNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int i = 0;
                        int count = 0;
                        int count1 = 0;
                        int count2 = 0;
                       CheckList = new CheckList();
                       
                        // axis 10.0 changes ends
                        foreach (XmlNode node in CheckNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBCheckList.TxnID.ToString()))
                            {
                                CheckList.TxnID = node.InnerText;
                            }

                            //Checking Account Ref list.
                            if (node.Name.Equals(QBCheckList.AccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCheckList.FullName.ToString()))
                                        CheckList.AccountFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayeeEntityRef List.
                            if (node.Name.Equals(QBCheckList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCheckList.FullName.ToString()))
                                        CheckList.PayeeEntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBCheckList.RefNumber.ToString()))
                            {
                                CheckList.RefNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBCheckList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        CheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        CheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    CheckList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBCheckList.Amount.ToString()))
                            {
                                CheckList.Amount = Convert.ToDecimal(node.InnerText);
                            }

                            //Chekcing CurrencyRefFullName
                            if (node.Name.Equals(QBCheckList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCheckList.FullName.ToString()))
                                        CheckList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ExchangeRate
                            if (node.Name.Equals(QBCheckList.ExchangeRate.ToString()))
                            {
                                CheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking AmountInHomeCurrency
                            if (node.Name.Equals(QBCheckList.AmountInHomeCurrency.ToString()))
                            {
                                CheckList.AmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBCheckList.Memo.ToString()))
                            {
                                CheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(QBCheckList.Address.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCheckList.Addr1.ToString()))
                                        CheckList.Address1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCheckList.Addr2.ToString()))
                                        CheckList.Address2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCheckList.Addr3.ToString()))
                                        CheckList.Address3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCheckList.Addr4.ToString()))
                                        CheckList.Address4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCheckList.Addr5.ToString()))
                                        CheckList.Address5 = childNode.InnerText;
                                }
                            }
                            //chekcing IsToBePrinted
                            if (node.Name.Equals(QBCheckList.IsToBePrinted.ToString()))
                            {
                                CheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBCheckList.IsTaxIncluded.ToString()))
                            {
                                CheckList.IsTaxIncluded = node.InnerText;
                            }

                            //Checking LinkedTxnID
                            if (node.Name.Equals(QBCheckList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCheckList.TxnID.ToString()))
                                    {
                                        CheckList.LinkedTxnID = childNode.InnerText;
                                    }
                                    if (childNode.Name.Equals(QBCheckList.RefNumber.ToString()))
                                    {
                                        CheckList.LinkedRefNumber = childNode.InnerText;
                                    }
                                }
                            }

                            //Checking Expense Line ret values.(repeated)
                            if (node.Name.Equals(QBBillList.ExpenseLineRet.ToString()))
                            {
                                checkExpNullEntries(node, ref CheckList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBCheckList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CheckList.ExpTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CheckList.ExpTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Chekcing AccountRefFullName
                                    if (childNode.Name.Equals(QBCheckList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ExpAccountfullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //checking Expense Amount
                                    if (childNode.Name.Equals(QBCheckList.Amount.ToString()))
                                    {
                                        CheckList.ExpAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing Expense Memo
                                    if (childNode.Name.Equals(QBCheckList.Memo.ToString()))
                                    {
                                        CheckList.ExpMemo.Add(childNode.InnerText);
                                    }
                                    //Checking CustomerRefFullName
                                    if (childNode.Name.Equals(QBCheckList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ExpCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking classRefFullName
                                    if (childNode.Name.Equals(QBCheckList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ExpClassName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxCodeFullName
                                    if (childNode.Name.Equals(QBCheckList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ExpSalesTaxCodefullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing BillableStatus
                                    if (childNode.Name.Equals(QBCheckList.BillableStatus.ToString()))
                                    {
                                        CheckList.ExpBillableStatus.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBCheckList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ExpSalesRepRefFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Axis 11 bug 126
                                    if (childNode.Name.Equals(QBCheckList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            CheckList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                CheckList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }

                                        i++;
                                    }
                                }
                                count++;
                            }

                            //Chekcing ItemLineRet
                            if (node.Name.Equals(QBCheckList.ItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref CheckList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBCheckList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CheckList.ItemTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CheckList.ItemTxnLineID.Add(childNode.InnerText);
                                    }
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBCheckList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                CheckList.ItemFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    // axis 10.0 changes
                                    if (childNode.Name.Equals(QBCheckList.SerialNumber.ToString()))
                                    {
                                        CheckList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBCheckList.LotNumber.ToString()))
                                    {
                                        CheckList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Desc
                                    if (childNode.Name.Equals(QBCheckList.Desc.ToString()))
                                    {
                                        CheckList.Desc.Add(childNode.InnerText);
                                    }
                                    //Checking Quantiy
                                    if (childNode.Name.Equals(QBCheckList.Quantity.ToString()))
                                    {
                                        CheckList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBCheckList.UnitOfMeasure.ToString()))
                                    {
                                        CheckList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking Cost
                                    if (childNode.Name.Equals(QBCheckList.Cost.ToString()))
                                    {
                                        CheckList.Cost.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBCheckList.Amount.ToString()))
                                    {
                                        CheckList.ItemAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing CustomerFullName
                                    if (childNode.Name.Equals(QBCheckList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ItemCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing ClassFullName
                                    if (childNode.Name.Equals(QBCheckList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ItemClassFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxFullName
                                    if (childNode.Name.Equals(QBCheckList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ItemSalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking BillableStatus
                                    if (childNode.Name.Equals(QBCheckList.BillableStatus.ToString()))
                                    {
                                        CheckList.ItemBillableStatus.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBCheckList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ItemSalesRepRefFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Axis 11 bug 126
                                    if (childNode.Name.Equals(QBCheckList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            CheckList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                CheckList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }

                                        i++;
                                    }
                                }
                                count1++;
                            }
                            //Axis 11 bug 126
                            if (node.Name.Equals(QBCheckList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CheckList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CheckList.DataExtValue.Add( node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }
                            //Checking ItemGroupLineRet
                            if (node.Name.Equals(QBCheckList.ItemGroupLineRet.ToString()))
                            {
                                int count3 = 0;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBCheckList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CheckList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CheckList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBCheckList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CheckList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Axis 11 bug 126
                                    if (childNode.Name.Equals(QBCheckList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            CheckList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                CheckList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }

                                        i++;
                                    }
                                    //Checking  sub ItemLineRet
                                    if (childNode.Name.Equals(QBCheckList.ItemLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref CheckList);
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking TxnLineID
                                            if (subChildNode.Name.Equals(QBCheckList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    CheckList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    CheckList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }
                                            //Chekcing ItemRefFullName
                                            if (subChildNode.Name.Equals(QBCheckList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                        CheckList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc
                                            if (subChildNode.Name.Equals(QBCheckList.Desc.ToString()))
                                            {
                                                CheckList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantiy
                                            if (subChildNode.Name.Equals(QBCheckList.Quantity.ToString()))
                                            {
                                                CheckList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBCheckList.UnitOfMeasure.ToString()))
                                            {
                                                CheckList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }
                                            //Checking Cost
                                            if (subChildNode.Name.Equals(QBCheckList.Cost.ToString()))
                                            {
                                                CheckList.GroupLineCost.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking Amount
                                            if (subChildNode.Name.Equals(QBCheckList.Amount.ToString()))
                                            {
                                                CheckList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Chekcing CustomerFullName
                                            if (subChildNode.Name.Equals(QBCheckList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                        CheckList.GroupLineCustomerFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Chekcing ClassFullName
                                            if (subChildNode.Name.Equals(QBCheckList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        CheckList.GroupLineClassFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking SalesTaxFullName
                                            if (subChildNode.Name.Equals(QBCheckList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                        CheckList.GroupLineSalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking BillableStatus
                                            if (subChildNode.Name.Equals(QBCheckList.BillableStatus.ToString()))
                                            {
                                                CheckList.GroupLineBillableStatus.Add(subChildNode.InnerText);
                                            }

                                            if (subChildNode.Name.Equals(QBCheckList.SalesRepRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes in subChildNode.ChildNodes)
                                                {
                                                    if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                        CheckList.GroupLineSalesRepRefFullName.Add(childNodes.InnerText);
                                                }
                                            }

                                            //Axis 11 bug 126
                                            if (subChildNode.Name.Equals(QBCheckList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    CheckList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        CheckList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                }

                                                i++;
                                            }
                                        }
                                        count3++;
                                    }
                                }
                                count2++;
                            }
                        }
                        //Adding Check list.
                        this.Add(CheckList);
                    }
                    else
                    {}
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfCheck.RemoveAll();
                #endregion
            }
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref CheckList checkList)
        {
            if (childNode.SelectSingleNode("./" + QBCheckList.TxnLineID.ToString()) == null)
            {
                checkList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.ItemRef.ToString()) == null)
            {
                checkList.GroupLineItemFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.Desc.ToString()) == null)
            {
                checkList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.Quantity.ToString()) == null)
            {
                checkList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.UnitOfMeasure.ToString()) == null)
            {
                checkList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.Cost.ToString()) == null)
            {
                checkList.GroupLineCost.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.Amount.ToString()) == null)
            {
                checkList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.CustomerRef.ToString()) == null)
            {
                checkList.GroupLineCustomerFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.ClassRef.ToString()) == null)
            {
                checkList.GroupLineClassFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.SalesTaxCodeRef.ToString()) == null)
            {
                checkList.GroupLineSalesTaxCodeFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.BillableStatus.ToString()) == null)
            {
                checkList.GroupLineBillableStatus.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.SalesRepRef.ToString()) == null)
            {
                checkList.GroupLineSalesRepRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCheckList.DataExtRet.ToString()) == null)
            {
                checkList.DataExtName.Add(null);
                checkList.DataExtValue.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref CheckList checkList)
        {
            if (node.SelectSingleNode("./" + QBCheckList.TxnLineID.ToString()) == null)
            {
                checkList.ItemTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.ItemRef.ToString()) == null)
            {
                checkList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.SerialNumber.ToString()) == null)
            {
                checkList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.LotNumber.ToString()) == null)
            {
                checkList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Desc.ToString()) == null)
            {
                checkList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Quantity.ToString()) == null)
            {
                checkList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.UnitOfMeasure.ToString()) == null)
            {
                checkList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Cost.ToString()) == null)
            {
                checkList.Cost.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Amount.ToString()) == null)
            {
                checkList.ItemAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.CustomerRef.ToString()) == null)
            {
                checkList.ItemCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.ClassRef.ToString()) == null)
            {
                checkList.ItemClassFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.SalesTaxCodeRef.ToString()) == null)
            {
                checkList.ItemSalesTaxCodeFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.BillableStatus.ToString()) == null)
            {
                checkList.ItemBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.SalesRepRef.ToString()) == null)
            {
                checkList.ItemSalesRepRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.DataExtRet.ToString()) == null)
            {
                checkList.DataExtName.Add(null);
                checkList.DataExtValue.Add(null);
            }
        }

        private void checkExpNullEntries(XmlNode node, ref CheckList checkList)
        {
            if (node.SelectSingleNode("./" + QBCheckList.TxnLineID.ToString()) == null)
            {
                checkList.ExpTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.AccountRef.ToString()) == null)
            {
                checkList.ExpAccountfullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Amount.ToString()) == null)
            {
                checkList.ExpAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.Memo.ToString()) == null)
            {
                checkList.ExpMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.CustomerRef.ToString()) == null)
            {
                checkList.ExpCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.ClassRef.ToString()) == null)
            {
                checkList.ExpClassName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.SalesTaxCodeRef.ToString()) == null)
            {
                checkList.ExpSalesTaxCodefullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.BillableStatus.ToString()) == null)
            {
                checkList.ExpBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.SalesRepRef.ToString()) == null)
            {
                checkList.ExpSalesRepRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCheckList.DataExtRet.ToString()) == null)
            {
                checkList.DataExtName.Add(null);
                checkList.DataExtValue.Add(null);
            }
        }

        /// <summary>
        /// THis method is used to get checkList from QuickBook
        /// for filter entityFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            //CheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("CheckQueryRq", QBFileName,entityFilter);

            CheckList CheckList;
            #endregion
           
            GetCheckValuesFromXML( CheckXmlList);
            //Returning object.
            return this;     
        }



        /// <summary>
        /// THis method is used to get checkList from QuickBook
        /// for filter TxnDateRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            //CheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate,entityFilter);

            CheckList CheckList;
            #endregion
           GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// THis method is used to get checkList from QuickBook
        /// for filter TxnDateRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;            
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckList CheckList;
            #endregion
                     GetCheckValuesFromXML(CheckXmlList);


            //Returning object.
            return this;     
        }


        /// <summary>
        /// Only Since last export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            //CheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            CheckList CheckList;
            #endregion
             GetCheckValuesFromXML(CheckXmlList);
             //Returning object.
            return this;
        }


        /// <summary>
        /// THis method is used to get checkList from QuickBook
        /// for filter TxnDateRangeFilter,Entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;            
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            CheckList CheckList;
            #endregion
                       GetCheckValuesFromXML(CheckXmlList);
            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;            
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckList CheckList;
            #endregion
                     GetCheckValuesFromXML(CheckXmlList);
 //Returning object.
            return this;
        }




        /// <summary>
        /// THis method is used to get checkList from QuickBook
        /// for filter TxnDateRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;            
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate);
            CheckList CheckList;
            #endregion
                      GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method id used to get checkList from QuickBook
        /// for filter RefNumberRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName, FromRefnum, ToRefnum,entityFilter);

            CheckList CheckList;
            #endregion
                     GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method id used to get checkList from QuickBook
        /// for filter RefNumberRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName, FromRefnum, ToRefnum);

            CheckList CheckList;
            #endregion
                      GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get checkList from QuickBook
        /// for Filter TxnDateRangeFilter and RefNumberRangeFilter,Entity Filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);

            CheckList CheckList;
            #endregion
                      GetCheckValuesFromXML(CheckXmlList);


            //Returning object.
            return this;
        }

        /// <summary>
        /// if filter was not selected  
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName);

            CheckList CheckList;
            #endregion
                      GetCheckValuesFromXML(CheckXmlList);


            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get checkList from QuickBook
        /// for Filter TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> PopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.GetListFromQuickBook("CheckQueryRq", QBFileName,FromDate, ToDate, FromRefnum, ToRefnum);

            CheckList CheckList;
            #endregion
                    GetCheckValuesFromXML(CheckXmlList);
        

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get checkList from QuickBook
        /// for Filter TxnDateRangeFilter and RefNumberRangeFilter,Entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);

            CheckList CheckList;
            #endregion
                     GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get checkList from QuickBook
        /// for Filter TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CheckList> ModifiedPopulateCheckList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Check List from QuickBooks.

            //Getting item list from QuickBooks.
            string CheckXmlList = string.Empty;
            CheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            CheckList CheckList;
            #endregion
                      GetCheckValuesFromXML(CheckXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }



        
    }
}
