using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using System.Xml;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;

namespace Streams
{
    /// <summary>
    ///This class is used to declare Public Methods and Properties.
    /// </summary>
    public class BillList : BaseBillList
    {
        #region Public Methods

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }       

        public string VendorRefFullName
        {
            get { return m_VendorFullName; }
            set { m_VendorFullName = value; }
        }

        public string APAccountRefFullName
        {
            get { return m_APAccountRefFullName; }
            set { m_APAccountRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public decimal? AmountDue
        {
            get { return m_AmountDue; }
            set { m_AmountDue = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? AmountDueInHomecurrency
        {
            get { return m_AmountDueInHomeCurrency; }
            set { m_AmountDueInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string TermsRefFullName
        {
            get { return m_TermsRefFullName; }
            set { m_TermsRefFullName = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string SalesTaxCodeRefFullName
        {
            get { return m_SalesTaxCodeRefFullName; }
            set { m_SalesTaxCodeRefFullName = value; }
        }

        public string IsPaid
        {
            get { return m_IsPaid; }
            set { m_IsPaid = value; }
        }


        //Axis Bug No 144                    

        public string LinkedTxnID
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }

        public string LinkRefNumber
        {
            get { return m_LinkRefNumber; }
            set { m_LinkRefNumber = value; }
        }
        public string TxnType
        {
            get { return m_TxnType; }
            set { m_TxnType = value; }
        }
        public string LinkTxnDate
        {
            get { return m_LinkTxnDate; }
            set { m_LinkTxnDate = value; }
        }
        public string LinkType
        {
            get { return m_LinkType; }
            set { m_LinkType = value; }
        }
        public string LinkAmount
        {
            get { return m_LinkAmount; }
            set { m_LinkAmount = value; }
        }

        //End Axis Bug No 144 

        public decimal? OpenAmount
        {
            get { return m_OpenAmount; }
            set { m_OpenAmount = value; }
        }
        
        //For ExpenseLineRet
        public List<string> TxnLineID
        {
            get { return m_ExpTxnLineID; }
            set { m_ExpTxnLineID = value; }
        }
         
        public List<string> AccountRefFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public  List<decimal?> ExpAmount
        {
            get { return m_ExpAmount; }
            set { m_ExpAmount = value; }
        }

        public List<string> ExpMemo
        {
            get { return m_ExpMemo; }
            set { m_ExpMemo = value; }
        }

        public List<string> ExpCustomerRefFullName
        {
            get { return m_ExpCustomerRefFullName; }
            set { m_ExpCustomerRefFullName = value; }
        }

        public List<string> ExpclassRefFullName
        {
            get { return m_ExpClassRefFullName; }
            set { m_ExpClassRefFullName = value; }
        }

        public List<string> ExpSalesTaxFullName
        {
            get { return m_ExpSalesTaxFullName; }
            set { m_ExpSalesTaxFullName = value; }
        }

        public List<string> ExpBillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }
        public List<string> ExpSalesRepRef
        {
            get { return m_ExpSalesRepRefFullName; }
            set { m_ExpSalesRepRefFullName = value; }
        }

        //For ItemLineRet
        public List<string> ItemTxnLineID
        {
            get { return m_ItemTxnLineID; }
            set { m_ItemTxnLineID = value; }
        }

        public List<string> ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }
        // Axis 10.0 changes
        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        //// Axis 10.0 changes ends
        
        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public  List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }       

        public  List<decimal?> Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public  List<decimal?> ItemAmount
        {
            get { return m_ItemAmount; }
            set { m_ItemAmount = value; }
        }

        public List<string> ItemCustomerRefFullName
        {
            get { return m_ItemCustomerRefFullName; }
            set { m_ItemCustomerRefFullName = value; }
        }

        public List<string> ItemClassRefFullName
        {
            get { return m_ItemClassRefFullName; }
            set { m_ItemClassRefFullName = value; }
        }

        public List<string> ItemSalesTaxFullName
        {
            get { return m_ItemSalesTaxFullName; }
            set { m_ItemSalesTaxFullName = value; }
        }

        public List<string> ItemBillableStatus
        {
            get { return m_ItemBillableStatus; }
            set { m_ItemBillableStatus = value; }
        }

       

        public List<string> ItemSalesRepRef
        {
            get { return m_ItemSalesRepRefFullName; }
            set { m_ItemSalesRepRefFullName = value; }
        }

        //For ItemGroupLineRet
        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

        public List<string> ItemGroupRefFullName
        {
            get { return m_ItemGroupRefFullName; }
            set { m_ItemGroupRefFullName = value; }
        }        

        //For Sub ItemLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemRefFullName
        {
            get { return m_GroupLineItemRefFullName; }
            set { m_GroupLineItemRefFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; ; }
            set { m_GroupLineDesc = value; }
        }

        public  List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public  List<decimal?> GroupLineCost
        {
            get { return m_GroupLineCost; }
            set { m_GroupLineCost = value; }
        }

        public  List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineCustomerRefFullName
        {
            get { return m_GroupLineCustomerRefFullName; }
            set { m_GroupLineCustomerRefFullName = value; }
        }

        public List<string> GroupLineClassRefFullName
        {
            get { return m_GroupLineClassRefFullName; }
            set { m_GroupLineClassRefFullName = value; }
        }

        public List<string> GroupLineSalesTaxFullName
        {
            get { return m_GroupLineSalesTaxFullName; }
            set { m_GroupLineSalesTaxFullName = value; }
        }

        public List<string> GroupLineBillableStatus
        {
            get { return m_GroupLineBillableStatus; }
            set { m_GroupLineBillableStatus = value; }
        }

        public List<string> GroupLineSalesRepRef
        {
            get { return m_GroupLineSalesRepRefFullName; }
            set { m_GroupLineSalesRepRefFullName = value; }
        }

        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();
        #endregion
    }


    /// <summary>
    /// This class is used for getting BillList from QuickBooks.
    /// </summary>
    public class QBBillListCollection : Collection<BillList>
    {
        string XmlFileData = string.Empty;
        int i = 0;

        public void GetBillValuesfromXML(string BillXmlList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBills = new XmlDocument();

            BillList BillList;

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBills.LoadXml(BillXmlList);
                XmlFileData = BillXmlList;

                #region Getting Bill Values from XML

                //Getting Bill values from QuickBooks response.
                XmlNodeList BillNodeList = outputXMLDocOfBills.GetElementsByTagName(QBBillList.BillRet.ToString());

                foreach (XmlNode BillNodes in BillNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int count2 = 0;
                        int i = 0;
                        BillList = new BillList();
                      
                        foreach (XmlNode node in BillNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBillList.TxnID.ToString()))
                            {
                                BillList.TxnID = node.InnerText;
                            }

                            //Checking Vendor Ref list.
                            if (node.Name.Equals(QBBillList.VendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBillList.FullName.ToString()))
                                        BillList.VendorRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBBillList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBillList.FullName.ToString()))
                                        BillList.APAccountRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBBillList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBBillList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillList.DueDate = node.InnerText;
                                }
                            }

                            //Checking AmountDue
                            if (node.Name.Equals(QBBillList.AmountDue.ToString()))
                            {
                                BillList.AmountDue = Convert.ToDecimal(node.InnerText);
                            }
                            //Chekcing CurrencyRefFullName
                            if (node.Name.Equals(QBBillList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBillList.FullName.ToString()))
                                        BillList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ExchangeRate
                            if (node.Name.Equals(QBBillList.ExchangeRate.ToString()))
                            {
                                BillList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking AmountDueInHomeCurrency
                            if (node.Name.Equals(QBBillList.AmountDueInHomeCurrency.ToString()))
                            {
                                BillList.AmountDueInHomecurrency = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking RefNumber.
                            if (node.Name.Equals(QBBillList.RefNumber.ToString()))
                            {
                                BillList.RefNumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBBillList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        BillList.TermsRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking Memo values.
                            if (node.Name.Equals(QBBillList.Memo.ToString()))
                            {
                                BillList.Memo = node.InnerText;
                            }

                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBBillList.IsTaxIncluded.ToString()))
                            {
                                BillList.IsTaxIncluded = node.InnerText;
                            }

                            //Checking SalesTaxCodeRefFullName
                            if (node.Name.Equals(QBBillList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBillList.FullName.ToString()))
                                        BillList.SalesTaxCodeRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBBillList.IsPaid.ToString()))
                            {
                                BillList.IsPaid = node.InnerText;
                            }
                            //Checking LinkedTxnID
                            if (node.Name.Equals(QBBillList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBillList.TxnID.ToString()))
                                    {
                                        BillList.LinkedTxnID = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBBillList.TxnType.ToString()))
                                    {
                                        BillList.TxnType = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBBillList.TxnDate.ToString()))
                                    {
                                        BillList.LinkTxnDate = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBBillList.RefNumber.ToString()))
                                    {
                                        BillList.LinkRefNumber = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBBillList.LinkType.ToString()))
                                    {
                                        BillList.LinkType = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBBillList.Amount.ToString()))
                                    {
                                        BillList.LinkAmount = childNode.InnerText;
                                    }
                                }
                            }
                            //Chekcing OpenAmount
                            if (node.Name.Equals(QBBillList.OpenAmount.ToString()))
                            {
                                BillList.OpenAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking Expense Line ret values.(repeated)
                            if (node.Name.Equals(QBBillList.ExpenseLineRet.ToString()))
                            {
                                //Axis 724
                                if (node.SelectSingleNode("./" + QBBillList.AccountRef.ToString()) == null)
                                {
                                    BillList.AccountRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Amount.ToString()) == null)
                                {
                                    BillList.ExpAmount.Add(0);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Memo.ToString()) == null)
                                {
                                    BillList.ExpMemo.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.CustomerRef.ToString()) == null)
                                {
                                    BillList.ExpCustomerRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.ClassRef.ToString()) == null)
                                {
                                    BillList.ExpclassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    BillList.ExpSalesTaxFullName.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.BillableStatus.ToString()) == null)
                                {
                                    BillList.ExpBillableStatus.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.SalesRepRef.ToString()) == null)
                                {
                                    BillList.ExpSalesRepRef.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.DataExtRet.ToString()) == null)
                                {
                                    BillList.DataExtValue.Add("");
                                }
                               
                                bool dataextFlag = false;
                                //Axis 724 ends
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBBillList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            BillList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Chekcing AccountRefFullName
                                    if (childNode.Name.Equals(QBBillList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.AccountRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //checking Expense Amount
                                    if (childNode.Name.Equals(QBBillList.Amount.ToString()))
                                    {
                                        BillList.ExpAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing Expense Memo

                                    if (childNode.Name.Equals(QBBillList.Memo.ToString()))
                                    {
                                        BillList.ExpMemo.Add(childNode.InnerText);
                                    }

                                    //Checking CustomerRefFullName
                                    if (childNode.Name.Equals(QBBillList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ExpCustomerRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking classRefFullName
                                    if (childNode.Name.Equals(QBBillList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ExpclassRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxCodeFullName
                                    if (childNode.Name.Equals(QBBillList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ExpSalesTaxFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing BillableStatus
                                    if (childNode.Name.Equals(QBBillList.BillableStatus.ToString()))
                                    {
                                        BillList.ExpBillableStatus.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBBillList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ExpSalesRepRef.Add(childNodes.InnerText);
                                        }

                                    }

                                    if (childNode.Name.Equals(QBBillList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            BillList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                BillList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        i++;
                                    }
                                }
                                count++;
                            }

                            #region //Chekcing ItemLineRet
                            if (node.Name.Equals(QBBillList.ItemLineRet.ToString()))
                            {
                                //Axis 724
                                if (node.SelectSingleNode("./" + QBBillList.ItemRef.ToString()) == null)
                                {
                                    BillList.ItemRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.SerialNumber.ToString()) == null)
                                {
                                    BillList.SerialNumber.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.LotNumber.ToString()) == null)
                                {
                                    BillList.LotNumber.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Desc.ToString()) == null)
                                {
                                    BillList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Quantity.ToString()) == null)
                                {
                                    BillList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.UnitOfMeasure.ToString()) == null)
                                {
                                    BillList.UOM.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Cost.ToString()) == null)
                                {
                                    BillList.Cost.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.Amount.ToString()) == null)
                                {
                                    BillList.ItemAmount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBBillList.CustomerRef.ToString()) == null)
                                {
                                    BillList.ItemCustomerRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.ClassRef.ToString()) == null)
                                {
                                    BillList.ItemClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    BillList.ItemSalesTaxFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.BillableStatus.ToString()) == null)
                                {
                                    BillList.ItemBillableStatus.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.SalesRepRef.ToString()) == null)
                                {
                                    BillList.ItemSalesRepRef.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBBillList.DataExtRet.ToString()) == null)
                                {
                                    BillList.DataExtValue.Add("");
                                }
                                //Axis 724 ends
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBBillList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillList.ItemTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            BillList.ItemTxnLineID.Add(childNode.InnerText);
                                    }
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBBillList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemRefFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    // axis 10.0 changes
                                    //Checking serial number
                                    if (childNode.Name.Equals(QBBillList.SerialNumber.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 4095)
                                            BillList.SerialNumber.Add(childNode.InnerText.Remove(4095, (childNode.InnerText.Length - 4095)).Trim());
                                        else
                                            BillList.SerialNumber.Add( childNode.InnerText);
                                    }

                                    //Checking lot number
                                    if (childNode.Name.Equals(QBBillList.LotNumber.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillList.LotNumber.Add(childNode.InnerText.Remove(40, (childNode.InnerText.Length - 40)).Trim());
                                        else
                                            BillList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // ais 10,.o changse ends

                                    //Checking Desc
                                    if (childNode.Name.Equals(QBBillList.Desc.ToString()))
                                    {
                                        BillList.Desc.Add(childNode.InnerText);
                                    }
                                    //Checking Quantiy
                                    if (childNode.Name.Equals(QBBillList.Quantity.ToString()))
                                    {
                                        BillList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBBillList.UnitOfMeasure.ToString()))
                                    {
                                        BillList.UOM.Add( childNode.InnerText);
                                    }
                                    //Checking Cost
                                    if (childNode.Name.Equals(QBBillList.Cost.ToString()))
                                    {
                                        BillList.Cost.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBBillList.Amount.ToString()))
                                    {
                                        BillList.ItemAmount.Add( Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing CustomerFullName
                                    if (childNode.Name.Equals(QBBillList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemCustomerRefFullName.Add( childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing ClassFullName
                                    if (childNode.Name.Equals(QBBillList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemClassRefFullName.Add( childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxFullName
                                    if (childNode.Name.Equals(QBBillList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemSalesTaxFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking BillableStatus
                                    if (childNode.Name.Equals(QBBillList.BillableStatus.ToString()))
                                    {
                                        BillList.ItemBillableStatus.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBBillList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemSalesRepRef.Add(childNodes.InnerText);
                                        }

                                    }

                                    if (childNode.Name.Equals(QBBillList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            BillList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                BillList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }

                                        i++;
                                    }
                                    
                                }
                                count1++;
                            }
                            #endregion
                            if (node.Name.Equals(QBBillList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    BillList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        BillList.DataExtValue.Add( node.SelectSingleNode("./DataExtValue").InnerText);
                                }
                                i++;
                            }
                            #region//Checking ItemGroupLineRet
                            if (node.Name.Equals(QBBillList.ItemGroupLineRet.ToString()))
                            {
                                int count3 = 0;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBBillList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            BillList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBBillList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                BillList.ItemGroupRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBillList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            BillList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                BillList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }

                                        i++;
                                    }
                                    //Checking  sub ItemLineRet
                                    if (childNode.Name.Equals(QBBillList.ItemLineRet.ToString()))
                                    {
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Axis 724
                                            if (node.SelectSingleNode("./" + QBBillList.ItemRef.ToString()) == null)
                                            {
                                                BillList.GroupLineItemRefFullName.Add("");
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.Desc.ToString()) == null)
                                            {
                                                BillList.GroupLineDesc.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.Quantity.ToString()) == null)
                                            {
                                                BillList.GroupLineQuantity.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.UnitOfMeasure.ToString()) == null)
                                            {
                                                BillList.GroupLineUOM.Add("");
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.Cost.ToString()) == null)
                                            {
                                                BillList.GroupLineCost.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.Amount.ToString()) == null)
                                            {
                                                BillList.GroupLineAmount.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.CustomerRef.ToString()) == null)
                                            {
                                                BillList.GroupLineCustomerRefFullName.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.ClassRef.ToString()) == null)
                                            {
                                                BillList.GroupLineClassRefFullName.Add(null);
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.SalesTaxCodeRef.ToString()) == null)
                                            {
                                                BillList.GroupLineSalesTaxFullName.Add("");
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.BillableStatus.ToString()) == null)
                                            {
                                                BillList.GroupLineBillableStatus.Add("");
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.SalesRepRef.ToString()) == null)
                                            {
                                                BillList.GroupLineSalesRepRef.Add("");
                                            }
                                            if (node.SelectSingleNode("./" + QBBillList.DataExtRet.ToString()) == null)
                                            {
                                                BillList.DataExtValue.Add("");
                                            }
                                            //Axis 724 ends
                                            //Checking TxnLineID
                                            if (subChildNode.Name.Equals(QBBillList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    BillList.GroupLineTxnLineID.Add( subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    BillList.GroupLineTxnLineID.Add( subChildNode.InnerText);
                                            }
                                            //Chekcing ItemRefFullName
                                            if (subChildNode.Name.Equals(QBBillList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        BillList.GroupLineItemRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc
                                            if (subChildNode.Name.Equals(QBBillList.Desc.ToString()))
                                            {
                                                BillList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantiy
                                            if (subChildNode.Name.Equals(QBBillList.Quantity.ToString()))
                                            {
                                                BillList.GroupLineQuantity.Add( Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBBillList.UnitOfMeasure.ToString()))
                                            {
                                                BillList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }
                                            //Checking Cost
                                            if (subChildNode.Name.Equals(QBBillList.Cost.ToString()))
                                            {
                                                BillList.GroupLineCost.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking Amount
                                            if (subChildNode.Name.Equals(QBBillList.Amount.ToString()))
                                            {
                                                BillList.GroupLineAmount.Add( Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Chekcing CustomerFullName
                                            if (subChildNode.Name.Equals(QBBillList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        BillList.GroupLineCustomerRefFullName.Add( subChildNodes.InnerText);
                                                }
                                            }
                                            //Chekcing ClassFullName
                                            if (subChildNode.Name.Equals(QBBillList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        BillList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking SalesTaxFullName
                                            if (subChildNode.Name.Equals(QBBillList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        BillList.GroupLineSalesTaxFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking BillableStatus
                                            if (subChildNode.Name.Equals(QBBillList.BillableStatus.ToString()))
                                            {
                                                BillList.GroupLineBillableStatus.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBBillList.SalesRepRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes in subChildNode.ChildNodes)
                                                {
                                                    if (childNodes.Name.Equals(QBBillList.FullName.ToString()))
                                                        BillList.GroupLineSalesRepRef.Add(childNodes.InnerText);
                                                }

                                            }

                                            if (subChildNode.Name.Equals(QBBillList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    BillList.DataExtName.Add( subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        BillList.DataExtValue.Add( subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                }

                                                i++;
                                            }

                                        }
                                        count3++;
                                    }
                                }
                                count2++;
                            }
                            #endregion
                        }
                        //Adding Invoice list.
                        this.Add(BillList);
                    }
                    else
                    {  }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBills.RemoveAll();
                #endregion
            }
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate,entityFilter);

            #endregion
            GetBillValuesfromXML(BillXmlList);
         

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            #endregion

            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// ModifieddateRangeFilter,Entity filter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            #endregion

            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }




        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// ModifieddateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// RefNnumberRangeFilter,entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromRefnum, ToRefnum,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// RefNnumberRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName,string FromRefnum, string ToRefnum)
        { 

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromRefnum, ToRefnum);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate,FromRefnum, ToRefnum);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter 
        /// Entity Filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("BillQueryRq", QBFileName, entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting BillList by Filter 
        /// Paid Status.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, string PaidStatus,List<string> entityFilter,bool flag)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("BillQueryRq", QBFileName, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting BillList by Filter 
        /// Paid Status.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("BillQueryRq",QBFileName, PaidStatus);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter and PaidStatus,Entity filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus, List<string> entityFilter,bool flag)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookDatePaid("BillQueryRq", QBFileName, FromDate, ToDate, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookDatePaid("BillQueryRq", QBFileName, FromDate, ToDate, PaidStatus);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// ModifiedDateRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus, List<string> entityFilter, bool flag)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookDatePaid("BillQueryRq", QBFileName, FromDate, ToDate, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting BillList by Filter
        /// ModifiedDateRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookDatePaid("BillQueryRq", QBFileName, FromDate, ToDate, PaidStatus);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// RefNumberRangeFilter nad PaidStatus,entity filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookRefPaid("BillQueryRq", QBFileName, FromRefnum, ToRefnum, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting BillList by Filter
        /// RefNumberRangeFilter nad PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookRefPaid("BillQueryRq", QBFileName, FromRefnum, ToRefnum, PaidStatus);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus,entity filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> PopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.
            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("BillQueryRq", QBFileName, FromDate,ToDate,FromRefnum, ToRefnum, PaidStatus);
          
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus,Entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPaidStatus("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus,entityFilter);
            #endregion
            GetBillValuesfromXML(BillXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter
        /// TxnDateRangeFilter, RefNumberRangeFilter and PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<BillList> ModifiedPopulateBillList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillXmlList = string.Empty;
            BillXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPaidStatus("BillQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus);
            #endregion
            GetBillValuesfromXML(BillXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        
    }
}
