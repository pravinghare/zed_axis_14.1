using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    public class Estimate : BaseEstimateList
    {
        #region Public Properties


        /// <summary>
        /// Get and Set TxnID of Quickbooks.
        /// </summary>
        public string TxnID
        {
            get
            { return m_TxnId; }
            set
            {
                if (value.Length > 40)
                {
                    m_TxnId = value.Remove(40, (value.Length - 40));
                }
                else

                    m_TxnId = value;
            }
        }

        /// <summary>
        /// Get and Set Time Created of QuickBooks.
        /// </summary>
        public string TimeCreated
        {
            get
            { return m_TimeCreated; }
            set
            {
                m_TimeCreated = value;
            }
        }

        /// <summary>
        /// Get and Set Time Modified of QuickBooks.
        /// </summary>
        public string TimeModified
        {
            get
            { return m_TimeModified; }
            set
            {
                m_TimeModified = value;
            }
        }

        /// <summary>
        /// Get and Set Edit Sequence of QuickBooks.
        /// </summary>
        public string EditSequence
        {
            get
            { return m_EditSequence; }
            set
            {
                m_EditSequence = value;
            }
        }

        /// <summary>
        /// Get and Set Txn Number of QuickBooks.
        /// </summary>

        public int TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        //Get or Set CustomerRefFullName value.
        public string CustomerRefFullName
        {
            get
            {
                return m_customerRefFullName;
            }
            set
            {

                m_customerRefFullName = value;
            }
        }

        //Get or Set ClassRefFullName value.
        public string ClassRefFullName
        {
            get
            {
                return m_ClassRefFullName;
            }
            set
            {

                m_ClassRefFullName = value;
            }
        }


        //Get or Set TemplateRefFullName value.
        public string TemplateRefFullName
        {
            get
            {
                return m_TemplateRefFullName;
            }
            set
            {

                m_TemplateRefFullName = value;
            }
        }

        /// <summary>
        /// Get and Set Txn Date of QuickBooks.
        /// </summary>
        public string TxnDate
        {
            get
            {
                return m_TxnDate;
            }
            set
            {
                m_TxnDate = value;
            }
        }
        /// <summary>
        /// Get and Set Refnumber of Quickbooks.
        /// </summary>
        public string RefNumber
        {
            get
            {
                return m_RefNumber;
            }
            set
            {
                if (value.Length > 30)
                {
                    m_RefNumber = value.Remove(30, (value.Length - 30));
                }
                else
                {
                    m_RefNumber = value.Trim();
                }
            }
        }


        /// <summary>
        /// Get or Set Bill Address  of QuickBooks.
        /// </summary>
        public string BillAddress1
        {
            get
            { return m_billAddr1; }
            set
            {
                if (value.Length > 40)
                    m_billAddr1 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddr1 = value;
            }
        }

        public string BillAddress2
        {
            get
            { return m_billAddr2; }
            set
            {
                if (value.Length > 40)
                    m_billAddr2 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddr2 = value;
            }
        }
        public string BillAddress3
        {
            get
            { return m_billAddr3; }
            set
            {
                if (value.Length > 40)
                    m_billAddr3 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddr3 = value;
            }
        }
        public string BillAddress4
        {
            get
            { return m_billAddr4; }
            set
            {
                if (value.Length > 40)
                    m_billAddr4 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddr4 = value;
            }
        }

        public string BillAddress5
        {
            get
            { return m_billAddr5; }
            set
            {
                if (value.Length > 40)
                    m_billAddr5 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddr5 = value;
            }
        }

        public string BillCity
        {
            get
            { return m_billCity; }
            set
            {
                if (value.Length > 40)
                    m_billCity = value.Remove(40, (value.Length - 40));
                else
                    m_billCity = value;
            }
        }

        public string BillState
        {
            get
            { return m_billState; }
            set
            {
                if (value.Length > 40)
                    m_billState = value.Remove(40, (value.Length - 40));
                else
                    m_billState = value;
            }
        }

        public string BillPostalCode
        {
            get
            { return m_billPostalCode; }
            set
            {
                if (value.Length > 40)
                    m_billPostalCode = value.Remove(40, (value.Length - 40));
                else
                    m_billPostalCode = value;
            }
        }

        public string BillCountry
        {
            get
            { return m_billCountry; }
            set
            {
                if (value.Length > 40)
                    m_billCountry = value.Remove(40, (value.Length - 40));
                else
                    m_billCountry = value;
            }
        }
        public string BillNote
        {
            get
            { return m_billNote; }
            set
            {
                if (value.Length > 40)
                    m_billNote = value.Remove(40, (value.Length - 40));
                else
                    m_billNote = value;
            }
        }

        /// <summary>
        /// Get or Set Bill Address Block of QuickBooks.
        /// </summary>
        public string BillAddressBlock1
        {
            get
            { return m_billAddrBlock1; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock1 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock1 = value;
            }
        }

        public string BillAddressBlock2
        {
            get
            { return m_billAddrBlock2; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock2 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock2 = value;
            }
        }
        public string BillAddressBlock3
        {
            get
            { return m_billAddrBlock3; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock3 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock3 = value;
            }
        }
        public string BillAddressBlock4
        {
            get
            { return m_billAddrBlock4; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock4 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock4 = value;
            }
        }

        public string BillAddressBlock5
        {
            get
            { return m_billAddrBlock5; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock5 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock5 = value;
            }
        }

        public string ShipAddress1
        {
            get
            { return m_shipAddress1; }
            set
            {
                if (value.Length > 40)
                    m_shipAddress1 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddress1 = value;
            }
        }


        public string ShipAddress2
        {
            get
            { return m_shipAddress2; }
            set
            {
                if (value.Length > 40)
                    m_shipAddress2 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddress2 = value;
            }
        }


        public string ShipAddress3
        {
            get
            { return m_shipAddress3; }
            set
            {
                if (value.Length > 40)
                    m_shipAddress3 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddress3 = value;
            }
        }




        public string ShipAddress4
        {
            get
            { return m_shipAddress4; }
            set
            {
                if (value.Length > 40)
                    m_shipAddress4 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddress4 = value;
            }
        }
        public string ShipAddress5
        {
            get
            { return m_shipAddress5; }
            set
            {
                if (value.Length > 40)
                    m_shipAddress5 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddress5 = value;
            }
        }


        public string ShipAddressCity
        {
            get
            { return m_shipAddressCity; }
            set
            {
                if (value.Length > 40)
                    m_shipAddressCity = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddressCity = value;
            }
        }

        public string ShipAddressState
        {
            get
            { return m_shipAddressState; }
            set
            {
                if (value.Length > 40)
                    m_shipAddressState = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddressState = value;
            }
        }

        public string ShipAddressPostalCode
        {
            get
            { return m_shipAddressPostalCode; }
            set
            {
                if (value.Length > 40)
                    m_shipAddressPostalCode = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddressPostalCode = value;
            }
        }

        public string ShipAddressCountry
        {
            get
            { return m_shipAddressCountry; }
            set
            {
                if (value.Length > 40)
                    m_shipAddressCountry = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddressCountry = value;
            }
        }
        public string ShipAddressNote
        {
            get
            { return m_shipAddressNote; }
            set
            {
                if (value.Length > 40)
                    m_shipAddressNote = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddressNote = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address Block of QuickBooks.
        /// </summary>
        public string ShipAddressBlock1
        {
            get
            { return m_shipAddrBlock1; }
            set
            {
                if (value.Length > 40)
                    m_shipAddrBlock1 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddrBlock1 = value;
            }
        }

        public string ShipAddressBlock2
        {
            get
            { return m_shipAddrBlock2; }
            set
            {
                if (value.Length > 40)
                    m_shipAddrBlock2 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddrBlock2 = value;
            }
        }
        public string ShipAddressBlock3
        {
            get
            { return m_shipAddrBlock3; }
            set
            {
                if (value.Length > 40)
                    m_shipAddrBlock3 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddrBlock3 = value;
            }
        }
        public string ShipAddressBlock4
        {
            get
            { return m_shipAddrBlock4; }
            set
            {
                if (value.Length > 40)
                    m_billAddrBlock4 = value.Remove(40, (value.Length - 40));
                else
                    m_billAddrBlock4 = value;
            }
        }

        public string ShipAddressBlock5
        {
            get
            { return m_shipAddrBlock5; }
            set
            {
                if (value.Length > 40)
                    m_shipAddrBlock5 = value.Remove(40, (value.Length - 40));
                else
                    m_shipAddrBlock5 = value;
            }
        }
        public string IsActive
        {
            get
            { return m_IsActive; }
            set
            {
                m_IsActive = value;
            }
        }

        /// Get or set PONumber of QuickBooks.

        public string PONumber
        {
            get
            { return m_poNumber; }
            set
            {
                if (value.Length > 30)
                    m_poNumber = value.Remove(30, (value.Length - 30));
                else
                    m_poNumber = value;
            }
        }

        //Get or Set TermsRefFullName value.
        public string TermsRefFullName
        {
            get
            {
                return m_TermsRefFullName;
            }
            set
            {

                m_TermsRefFullName = value;
            }
        }

        //Get or Set DueDate value.
        public string DueDate
        {
            get
            {
                return m_DueDate;
            }
            set
            {

                m_DueDate = value;
            }
        }

        //Get or Set SalesRepRefFullName value.
        public string SalesRepRefFullName
        {
            get
            {
                return m_SalesRepRefFullName;
            }
            set
            {

                m_SalesRepRefFullName = value;
            }
        }

        public string FOB
        {
            get
            {
                return m_FOB;
            }
            set
            {

                m_FOB = value;
            }
        }

        public string SubTotal
        {
            get
            {
                return m_SubTotal;
            }
            set
            {

                m_SubTotal = value;
            }
        }


        //Get or Set ItemSalesTaxRefFullName value.
        public string ItemSalesTaxRefFullName
        {
            get
            {
                return m_ItemSalesTaxRefFullName;
            }
            set
            {

                m_ItemSalesTaxRefFullName = value;
            }
        }

        //Get or Set SalesTaxPercentage value.
        public string SalesTaxPercentage
        {
            get
            {
                return m_SalesTaxPercentage;
            }
            set
            {

                m_SalesTaxPercentage = value;
            }
        }
        //Get or Set SalesTaxTotal value.
        public string SalesTaxTotal
        {
            get
            {
                return m_SalesTaxTotal;
            }
            set
            {

                m_SalesTaxTotal = value;
            }
        }

        //Get or Set TotalAmount value.
        public string TotalAmount
        {
            get
            {
                return m_TotalAmount;
            }
            set
            {

                m_TotalAmount = value;
            }
        }

        public string CurrencyRefFullName
        {
            get
            {
                return m_CurrencyRefFullName;
            }
            set
            {

                m_CurrencyRefFullName = value;
            }
        }



        //Get or Set ExchangeRate value.
        public string ExchangeRate
        {
            get
            {
                return m_ExchangeRate;
            }
            set
            {

                m_ExchangeRate = value;
            }
        }
        //Get or Set TotalAmountInHomeCurrency value.
        public string TotalAmountInHomeCurrency
        {
            get
            {
                return m_TotalAmountInHomeCurrency;
            }
            set
            {

                m_TotalAmountInHomeCurrency = value;
            }
        }

        /// Get or Set Memo of QuickBooks.

        public string Memo
        {
            get
            { return m_memo; }
            set
            {
                if (value.Length > 30)
                    m_memo = value.Remove(30, (value.Length - 30));
                else
                    m_memo = value;
            }
        }

        //Get or Set CustomerMsgRefFullName value.
        public string CustomerMsgRefFullName
        {
            get
            {
                return m_CustomerMsgRefFullName;
            }
            set
            {

                m_CustomerMsgRefFullName = value;
            }
        }
        //Get or Set IsToBeEmailed value.
        public string IsToBeEmailed
        {
            get
            {
                return m_IsToBeEmailed;
            }
            set
            {

                m_IsToBeEmailed = value;
            }
        }

        //Get or Set CustomerSalesTaxCodeRefFullName value.
        public string CustomerSalesTaxCodeRefFullName
        {
            get
            {
                return m_CustomerSalesTaxCodeRefFullName;
            }
            set
            {

                m_CustomerSalesTaxCodeRefFullName = value;
            }
        }

        //Get or Set Other value.
        public string Other
        {
            get
            {
                return m_Other;
            }
            set
            {

                m_Other = value;
            }
        }


        public string Other1
        {
            get
            {
                return m_Other1;
            }
            set
            {

                m_Other1 = value;
            }
        }

        public string Other2
        {
            get
            {
                return m_Other2;
            }
            set
            {

                m_Other2 = value;
            }
        }

        public string ExternalGIUD
        {
            get
            {
                return m_externalGUID;
            }
            set
            {

                m_externalGUID = value;
            }
        }
        public string TxnType
        {
            get
            {
                return m_txnType;
            }
            set
            {

                m_txnType = value;
            }
        }

        public string LinkType
        {
            get
            {
                return m_linkType;
            }
            set
            {

                m_linkType = value;
            }
        }


        public decimal Amount
        {
            get { return m_amount; }
            set { m_amount = value; }
        }


        public string OwnerId
        {
            get
            {
                return m_ownerId;
            }
            set
            {

                m_ownerId = value;
            }
        }

        public string ExtName
        {
            get
            {
                return m_extName;
            }
            set
            {

                m_extName = value;
            }
        }
        public string DataExtType
        {
            get
            {
                return m_dataExtType;
            }
            set
            {

                m_dataExtType = value;
            }
        }


        public string CustomerListId
        {
            get
            {
                return m_customerListID;
            }
            set
            {

                m_customerListID = value;
            }
        }


        public string IsTaxIncluded
        {
            get
            {
                return m_isTaxIncluded;
            }
            set
            {

                m_isTaxIncluded = value;
            }
        }

        /***********Set and Get EsimateLineRet properties***********************/


        public List<string> TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        public List<string> EstLineItemRefFullName
        {
            get { return m_EstItemRefFullName; }
            set { m_EstItemRefFullName = value; }
        }


        public List<string> EstLineDescription
        {
            get { return m_EstDesc; }
            set { m_EstDesc = value; }
        }

        public List<decimal?> EstLineQuantity
        {
            get { return m_EstQuantity; }
            set { m_EstQuantity = value; }
        }

        public List<string> EstLineUOM
        {
            get { return m_EstUOM; }
            set { m_EstUOM = value; }
        }
        public List<string> EstLineOverrideUOMSetRefFullName
        {
            get { return m_EstOverrideUOMSetRefFullName; }
            set { m_EstOverrideUOMSetRefFullName = value; }
        }
        public List<decimal?> EstLineRate
        {
            get { return m_EstRate; }
            set { m_EstRate = value; }
        }
        public List<string> EstLineClassRefFullName
        {
            get { return m_EstClassRefFullName; }
            set { m_EstClassRefFullName = value; }
        }

        public List<decimal?> EstLineAmount
        {
            get { return m_EstAmouunt; }
            set { m_EstAmouunt = value; }
        }

        public List<string> EstLineInventorySiteRefFullName
        {
            get { return m_EstInventorySiteRefFullName; }
            set { m_EstInventorySiteRefFullName = value; }
        }
        public List<string> EstLineSalesTaxCodeRefFullName
        {
            get { return m_EstSalesTaxCodeRefFullName; }
            set { m_EstSalesTaxCodeRefFullName = value; }
        }

        public List<decimal?> EstLineMarkUpRate
        {
            get { return m_EstMarkUpRate; }
            set { m_EstMarkUpRate = value; }
        }

        public List<decimal?> EstLineMarkUpRatePercent
        {
            get { return m_EstMarkUpRatePercent; }
            set { m_EstMarkUpRatePercent = value; }
        }
        public List<string> EstLineOther1
        {
            get { return m_EstOther1; }
            set { m_EstOther1 = value; }
        }
        public List<string> EstLineOther2
        {
            get { return m_EstOther2; }
            set { m_EstOther2 = value; }
        }


        /***********Set and Get EsimateLineGroupRet properties***************/


        public List<string> EstLineGroupTxnLineID
        {
            get { return m_EstGroupTxnLineID; }
            set { m_EstGroupTxnLineID = value; }
        }
        public List<string> EstLineGroupItemRefFullName
        {
            get { return m_EstGroupItemRefFullName; }
            set { m_EstGroupItemRefFullName = value; }
        }

        public List<string> EstLineGroupDesc
        {
            get { return m_EstGroupDesc; }
            set { m_EstGroupDesc = value; }
        }
        public List<decimal?> EstLineGroupQuantity
        {
            get { return m_EstGroupQuantity; }
            set { m_EstGroupQuantity = value; }
        }

        public List<string> EstLineGroupUOM
        {
            get { return m_EstGroupUOM; }
            set { m_EstGroupUOM = value; }
        }

        public List<string> EstLineGroupOverrideUOMSiteRefFullName
        {
            get { return m_EstGroupOverrideUOMSetRefFullName; }
            set { m_EstGroupOverrideUOMSetRefFullName = value; }
        }

        public List<bool> EstLineGroupIsPrintItemInGroup
        {
            get { return m_EstGroupIsPrintItemInGroup; }
            set { m_EstGroupIsPrintItemInGroup = value; }
        }

        public List<decimal?> EstLineGroupTotalAmount
        {
            get { return m_EstGroupTotalAmount; }
            set { m_EstGroupTotalAmount = value; }
        }
        /***********Set and Get Sub EsimateLineRet properties************/
        public List<string> EstGroupLineTxnLineID
        {
            get { return m_EstGroupLineTxnLineID; }
            set { m_EstGroupLineTxnLineID = value; }
        }

        public List<string> EstGroupLineItemRefFullName
        {
            get { return m_EstGroupLineItemRefFullName; }
            set { m_EstGroupLineItemRefFullName = value; }
        }


        public List<string> EstGroupLineDescription
        {
            get { return m_EstGroupLineDesc; }
            set { m_EstGroupLineDesc = value; }
        }

        public List<decimal?> EstGroupLineQuantity
        {
            get { return m_EstGroupLineQuantity; }
            set { m_EstGroupLineQuantity = value; }
        }

        public List<string> EstGroupLineUOM
        {
            get { return m_EstGroupLineUOM; }
            set { m_EstGroupLineUOM = value; }
        }
        public List<string> EstGroupLineOverrideUOMSetRefFullName
        {
            get { return m_EstGroupLineOverrideUOMSetRefFullName; }
            set { m_EstGroupLineOverrideUOMSetRefFullName = value; }
        }
        public List<decimal?> EstGroupLineRate
        {
            get { return m_EstGroupLineRate; }
            set { m_EstGroupLineRate = value; }
        }
        public List<string> EstGroupLineClassRefFullName
        {
            get { return m_EstGroupLineClassRefFullName; }
            set { m_EstGroupLineClassRefFullName = value; }
        }

        public List<decimal?> EstGroupLineAmount
        {
            get { return m_EstGroupLineAmouunt; }
            set { m_EstGroupLineAmouunt = value; }
        }

        public List<string> EstGroupLineInventorySiteRefFullName
        {
            get { return m_EstGroupLineInventorySiteRefFullName; }
            set { m_EstGroupLineInventorySiteRefFullName = value; }
        }
        public List<string> EstGroupLineSalesTaxCodeRefFullName
        {
            get { return m_EstGroupLineSalesTaxCodeRefFullName; }
            set { m_EstGroupLineSalesTaxCodeRefFullName = value; }
        }

        public List<decimal?> EstGroupLineMarkUpRate
        {
            get { return m_EstGroupLineMarkUpRate; }
            set { m_EstGroupLineMarkUpRate = value; }
        }

        public List<decimal?> EstGroupLineMarkUpRatePercent
        {
            get { return m_EstGroupLineMarkUpRatePercent; }
            set { m_EstGroupLineMarkUpRatePercent = value; }
        }

        public List<string> EstGroupLineOther1
        {
            get { return m_EstGroupLineOther1; }
            set { m_EstGroupLineOther1 = value; }
        }

        public List<string> EstGroupLineOther2
        {
            get { return m_EstGroupLineOther2; }
            set { m_EstGroupLineOther2 = value; }
        }

        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();
        #endregion

    }


         /// <summary>
        /// Collection class used for getting Estimate from QuickBooks.
        /// </summary>
        public class QBEstimateCollection : Collection<Estimate>
        {
           public string XmlFileData = string.Empty;
        //Axis 706 
        QBItemDetails itemList = null;

           public void getEstimateDetails(string estimateXmlList)
            {
                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                Estimate esimate;
                //Create a xml document for output result.
                XmlDocument outputXMLDocOfEstimate = new XmlDocument();

                if (estimateXmlList != string.Empty)
                {
                    //Loading Item List into XML.
                    outputXMLDocOfEstimate.LoadXml(estimateXmlList);
                    XmlFileData = estimateXmlList;

                    #region Getting Estimate Values from XML

                    //Getting Estimate values from QuickBooks response.
                    XmlNodeList EsimateNodeList = outputXMLDocOfEstimate.GetElementsByTagName(QBXmlEstimateList.EstimateRet.ToString());

                    foreach (XmlNode EstimateNodes in EsimateNodeList)
                    {
                        if (bkWorker.CancellationPending != true)
                        {
                            esimate = new Estimate();
                            int count = 0;
                            int count1 = 0;
                            int count2 = 0;
                            int i = 0;


                            foreach (XmlNode node in EstimateNodes.ChildNodes)
                            {
                                if (node.Name.Equals(QBXmlEstimateList.TxnID.ToString()))
                                    esimate.TxnID = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.TimeCreated.ToString()))
                                    esimate.TimeCreated = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.TimeModified.ToString()))
                                    esimate.TimeModified = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.EditSequence.ToString()))
                                    esimate.EditSequence = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.TxnNumber.ToString()))
                                    esimate.TxnNumber = int.Parse(node.InnerText);

                                //Checking Customer Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.CustomerRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.ListID.ToString()))
                                            esimate.CustomerListId = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.CustomerRefFullName = childNode.InnerText;
                                    }
                                }
                                //Checking Class Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.ClassRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.ClassRefFullName = childNode.InnerText;

                                    }
                                }

                                //Checking Template Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.TemplateRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.TemplateRefFullName = childNode.InnerText;

                                    }
                                }


                                if (node.Name.Equals(QBXmlEstimateList.TxnDate.ToString()))
                                {
                                    try
                                    {
                                        DateTime dtTxndate = Convert.ToDateTime(node.InnerText);
                                        esimate.TxnDate = dtTxndate.ToString("yyyy-MM-dd");
                                    }
                                    catch
                                    {
                                        esimate.TxnDate = node.InnerText;
                                    }
                                    esimate.TxnDate = node.InnerText;
                                }

                                if (node.Name.Equals(QBXmlEstimateList.RefNumber.ToString()))
                                    esimate.RefNumber = node.InnerText;


                                //Checking values of BillAddress values.
                                if (node.Name.Equals(QBXmlEstimateList.BillAddress.ToString()))
                                {
                                    //Getting values of address.
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr1.ToString()))
                                            esimate.BillAddress1 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr2.ToString()))
                                            esimate.BillAddress2 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr3.ToString()))
                                            esimate.BillAddress3 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr4.ToString()))
                                            esimate.BillAddress4 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr5.ToString()))
                                            esimate.BillAddress5 = childNode.InnerText;

                                        if (childNode.Name.Equals(QBXmlEstimateList.City.ToString()))
                                            esimate.BillCity = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.State.ToString()))
                                            esimate.BillState = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.PostalCode.ToString()))
                                            esimate.BillPostalCode = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Country.ToString()))
                                            esimate.BillCountry = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Note.ToString()))
                                            esimate.BillNote = childNode.InnerText;
                                    }
                                }


                                //Checking values of BillAddress Block values.
                                if (node.Name.Equals(QBXmlEstimateList.BillAddressBlock.ToString()))
                                {
                                    //Getting values of address.
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr1.ToString()))
                                            esimate.BillAddressBlock1 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr2.ToString()))
                                            esimate.BillAddressBlock2 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr3.ToString()))
                                            esimate.BillAddressBlock3 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr4.ToString()))
                                            esimate.BillAddressBlock4 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr5.ToString()))
                                            esimate.BillAddressBlock5 = childNode.InnerText;


                                    }
                                }


                                //Checking values of ShipAddress values.
                                if (node.Name.Equals(QBXmlEstimateList.ShipAddress.ToString()))
                                {
                                    //Getting values of address.
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr1.ToString()))
                                            esimate.ShipAddress1 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr2.ToString()))
                                            esimate.ShipAddress2 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr3.ToString()))
                                            esimate.ShipAddress3 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr4.ToString()))
                                            esimate.ShipAddress4 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr5.ToString()))
                                            esimate.ShipAddress5 = childNode.InnerText;

                                        if (childNode.Name.Equals(QBXmlEstimateList.City.ToString()))
                                            esimate.ShipAddressCity = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.State.ToString()))
                                            esimate.ShipAddressState = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.PostalCode.ToString()))
                                            esimate.ShipAddressPostalCode = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Country.ToString()))
                                            esimate.ShipAddressCountry = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Note.ToString()))
                                            esimate.ShipAddressNote = childNode.InnerText;
                                    }
                                }


                                //Checking values of ShipAddress Block values.
                                if (node.Name.Equals(QBXmlEstimateList.ShipAddressBlock.ToString()))
                                {
                                    //Getting values of address.
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr1.ToString()))
                                            esimate.ShipAddressBlock1 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr2.ToString()))
                                            esimate.ShipAddressBlock2 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr3.ToString()))
                                            esimate.ShipAddressBlock3 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr4.ToString()))
                                            esimate.ShipAddressBlock4 = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Addr5.ToString()))
                                            esimate.ShipAddressBlock5 = childNode.InnerText;


                                    }
                                }

                                if (node.Name.Equals(QBXmlEstimateList.IsActive.ToString()))
                                    esimate.IsActive = node.InnerText;


                                if (node.Name.Equals(QBXmlEstimateList.PONumber.ToString()))
                                    esimate.PONumber = node.InnerText;


                                //Checking Terms Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.TermsRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.TermsRefFullName = childNode.InnerText;

                                    }
                                }

                                if (node.Name.Equals(QBXmlEstimateList.DueDate.ToString()))
                                {
                                    try
                                    {
                                        DateTime dtDuedate = Convert.ToDateTime(node.InnerText);
                                        esimate.DueDate = dtDuedate.ToString("yyyy-MM-dd");
                                    }
                                    catch
                                    {
                                        esimate.DueDate = node.InnerText;
                                    }
                                    esimate.DueDate = node.InnerText;
                                }
                                //Sales Rep Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.SalesRepRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.SalesRepRefFullName = childNode.InnerText;

                                    }
                                }
                                //Checking FOB
                                if (node.Name.Equals(QBXmlEstimateList.FOB.ToString()))
                                    esimate.FOB = node.InnerText;
                                //Checking Sub Total
                                if (node.Name.Equals(QBXmlEstimateList.Subtotal.ToString()))
                                    esimate.SubTotal = node.InnerText;
                                // Item Sales Tax Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.ItemSalesTaxRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.ItemSalesTaxRefFullName = childNode.InnerText;

                                    }
                                }
                                if (node.Name.Equals(QBXmlEstimateList.SalesTaxPercentage.ToString()))
                                    esimate.SalesTaxPercentage = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.SalesTaxTotal.ToString()))
                                    esimate.SalesTaxTotal = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.TotalAmount.ToString()))
                                    esimate.TotalAmount = node.InnerText;
                                // Currency Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.CurrencyRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.CurrencyRefFullName = childNode.InnerText;

                                    }
                                }

                                if (node.Name.Equals(QBXmlEstimateList.ExchangeRate.ToString()))
                                    esimate.ExchangeRate = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.TotalAmountInHomeCurrency.ToString()))
                                    esimate.TotalAmountInHomeCurrency = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.Memo.ToString()))
                                    esimate.Memo = node.InnerText;

                                // Customer Msg Ref list.
                                if (node.Name.Equals(QBXmlEstimateList.CustomerMsgRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.CustomerMsgRefFullName = childNode.InnerText;

                                    }
                                }

                                if (node.Name.Equals(QBXmlEstimateList.IsToBeEmailed.ToString()))
                                    esimate.IsToBeEmailed = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.IsTaxIncluded.ToString()))
                                    esimate.IsTaxIncluded = node.InnerText;

                                //Checking CustomerSalesTaxCodeRef list.
                                if (node.Name.Equals(QBXmlEstimateList.CustomerSalesTaxCodeRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                            esimate.CustomerSalesTaxCodeRefFullName = childNode.InnerText;
                                    }
                                }
                                if (node.Name.Equals(QBXmlEstimateList.Other.ToString()))
                                    esimate.Other = node.InnerText;

                                if (node.Name.Equals(QBXmlEstimateList.ExternalGUID.ToString()))
                                    esimate.ExternalGIUD = node.InnerText;


                                //Checking Linked Txn. list.
                                if (node.Name.Equals(QBXmlEstimateList.LinkedTxn.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBXmlEstimateList.TxnID.ToString()))
                                            esimate.TxnID = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.TxnType.ToString()))
                                            esimate.TxnType = childNode.InnerText;

                                        if (childNode.Name.Equals(QBXmlEstimateList.TxnDate.ToString()))
                                        {
                                            try
                                            {
                                                DateTime dtTxnDate = Convert.ToDateTime(childNode.InnerText);
                                                esimate.TxnDate = dtTxnDate.ToString("yyyy-MM-dd");
                                            }
                                            catch
                                            {
                                                esimate.TxnDate = childNode.InnerText;
                                            }
                                            esimate.TxnDate = childNode.InnerText;
                                        }
                                        if (childNode.Name.Equals(QBXmlEstimateList.RefNumber.ToString()))
                                            esimate.RefNumber = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.LinkType.ToString()))
                                            esimate.LinkType = childNode.InnerText;
                                        if (childNode.Name.Equals(QBXmlEstimateList.Amount.ToString()))
                                        {
                                            try
                                            {
                                                esimate.Amount = Convert.ToDecimal(childNode.InnerText);
                                            }
                                            catch
                                            { }
                                        }
                                    }
                                }

                            if (node.Name.Equals(QBXmlEstimateList.EstimateLineRet.ToString()))
                            {
                                checkNullEntries(node, ref esimate);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlEstimateList.TxnLineID.ToString()))
                                    {
                                        esimate.TxnLineID.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineItemRefFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Desc.ToString()))
                                    {
                                        esimate.EstLineDescription.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBXmlEstimateList.Quantity.ToString()))
                                    {
                                        esimate.EstLineQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }

                                    if (childNode.Name.Equals(QBXmlEstimateList.UnitOfMeasure.ToString()))
                                    {
                                        esimate.EstLineUOM.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.OverrideUOMSiteRef.ToString()))
                                    {
                                        //esimate.EstLineOverrideUOMSetRefFullName[count] = childNode.InnerText;
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineOverrideUOMSetRefFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Rate.ToString()))
                                    {
                                        esimate.EstLineRate.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineClassRefFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Amount.ToString()))
                                    {

                                        esimate.EstLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineInventorySiteRefFullName.Add(childsNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineSalesTaxCodeRefFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlEstimateList.MarkupRate.ToString()))
                                    {
                                        esimate.EstLineMarkUpRate.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.MarkupRatePercent.ToString()))
                                    {
                                        esimate.EstLineMarkUpRatePercent.Add(Convert.ToDecimal(childNode.InnerText));
                                    }


                                    //Checking other1,other2 value.
                                    if (childNode.Name.Equals(QBXmlEstimateList.Other1.ToString()))
                                    {
                                        esimate.EstLineOther1.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Other2.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 29)
                                            esimate.EstLineOther2.Add(childNode.InnerText.Remove(29, (childNode.InnerText.Length - 29)).Trim());
                                        else
                                            esimate.EstLineOther2.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBXmlEstimateList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            esimate.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + esimate.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                esimate.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + esimate.TxnLineID[count]);
                                        }

                                        i++;
                                    }
                                }
                                count++;
                            }
                            if (node.Name.Equals(QBXmlEstimateList.EstimateLineGroupRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlEstimateList.TxnLineID.ToString()))
                                    {
                                        esimate.EstLineGroupTxnLineID.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childsNodes in childNode.ChildNodes)
                                        {
                                            if (childsNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineGroupItemRefFullName.Add(childsNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Desc.ToString()))
                                    {
                                        esimate.EstLineGroupDesc.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.Quantity.ToString()))
                                    {
                                        esimate.EstLineGroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.UnitOfMeasure.ToString()))
                                    {
                                        esimate.EstLineGroupUOM.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.OverrideUOMSiteRef.ToString()))
                                    {
                                        foreach (XmlNode grplineChildNodes in childNode.ChildNodes)
                                        {
                                            if (grplineChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                esimate.EstLineGroupOverrideUOMSiteRefFullName.Add(grplineChildNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.IsPrintItemInGroup.ToString()))
                                    {
                                        esimate.EstLineGroupIsPrintItemInGroup.Add(Convert.ToBoolean(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.TotalAmount.ToString()))
                                    {
                                        esimate.EstLineGroupTotalAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }

                                    if (childNode.Name.Equals(QBXmlEstimateList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            esimate.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + esimate.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                esimate.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + esimate.TxnLineID[count]);
                                        }

                                        i++;
                                    }
                                    if (childNode.Name.Equals(QBXmlEstimateList.EstimateLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref esimate);
                                        foreach (XmlNode childNode2 in childNode.ChildNodes)
                                        {
                                            if (childNode2.Name.Equals(QBXmlEstimateList.TxnLineID.ToString()))
                                            {
                                                esimate.EstGroupLineTxnLineID.Add(childNode2.InnerText);
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode grpChildNodes in childNode2.ChildNodes)
                                                {
                                                    if (grpChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                        esimate.EstGroupLineItemRefFullName.Add(grpChildNodes.InnerText);
                                                }
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.Desc.ToString()))
                                            {
                                                esimate.EstGroupLineDescription.Add(childNode2.InnerText);
                                            }

                                            if (childNode2.Name.Equals(QBXmlEstimateList.Quantity.ToString()))
                                            {
                                                esimate.EstGroupLineQuantity.Add(Convert.ToDecimal(childNode2.InnerText));
                                            }

                                            if (childNode2.Name.Equals(QBXmlEstimateList.UnitOfMeasure.ToString()))
                                            {
                                                esimate.EstGroupLineUOM.Add(childNode2.InnerText);
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.OverrideUOMSiteRef.ToString()))
                                            {
                                                foreach (XmlNode grpChildNodes in childNode2.ChildNodes)
                                                {
                                                    if (grpChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                        esimate.EstGroupLineOverrideUOMSetRefFullName.Add(grpChildNodes.InnerText);
                                                }
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.Rate.ToString()))
                                            {
                                                esimate.EstGroupLineRate.Add(Convert.ToDecimal(childNode2.InnerText));
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode grpChildNodes in childNode2.ChildNodes)
                                                {
                                                    if (grpChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                        esimate.EstGroupLineClassRefFullName.Add(grpChildNodes.InnerText);
                                                }
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.Amount.ToString()))
                                            {
                                                esimate.EstGroupLineAmount.Add(Convert.ToDecimal(childNode2.InnerText));
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.InventorySiteRef.ToString()))
                                            {
                                                foreach (XmlNode grpChildNodes in childNode2.ChildNodes)
                                                {
                                                    if (grpChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                        esimate.EstGroupLineInventorySiteRefFullName.Add(grpChildNodes.InnerText);
                                                }
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode grpChildNodes in childNode2.ChildNodes)
                                                {
                                                    if (grpChildNodes.Name.Equals(QBXmlEstimateList.FullName.ToString()))
                                                        esimate.EstGroupLineSalesTaxCodeRefFullName.Add(grpChildNodes.InnerText);
                                                }
                                            }

                                            if (childNode2.Name.Equals(QBXmlEstimateList.MarkupRate.ToString()))
                                            {
                                                esimate.EstGroupLineMarkUpRate.Add(Convert.ToDecimal(childNode2.InnerText));
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.MarkupRate.ToString()))
                                            {
                                                esimate.EstGroupLineMarkUpRatePercent.Add(Convert.ToDecimal(childNode2.InnerText));
                                            }

                                            //Checking other1,other2 value.
                                            if (childNode2.Name.Equals(QBXmlEstimateList.Other1.ToString()))
                                            {
                                                esimate.EstGroupLineOther1.Add(childNode2.InnerText);
                                            }
                                            if (childNode2.Name.Equals(QBXmlEstimateList.Other2.ToString()))
                                            {
                                                if (childNode2.InnerText.Length > 29)
                                                    esimate.EstGroupLineOther2.Add(childNode2.InnerText.Remove(29, (childNode2.InnerText.Length - 29)).Trim());
                                                else
                                                    esimate.EstGroupLineOther2.Add(childNode2.InnerText);
                                            }

                                            if (childNode2.Name.Equals(QBXmlEstimateList.DataExtRet.ToString()))
                                            {
                                                if (!string.IsNullOrEmpty(childNode2.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    esimate.DataExtName.Add(childNode2.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(childNode2.SelectSingleNode("./DataExtValue").InnerText))
                                                        esimate.DataExtValue.Add(childNode2.SelectSingleNode("./DataExtValue").InnerText);
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                }
                                count1++;
                            }
                                if (node.Name.Equals(QBXmlEstimateList.DataExtRet.ToString()))
                                {
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                    {
                                        esimate.DataExtName.Add( node.SelectSingleNode("./DataExtName").InnerText);
                                        if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                            esimate.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                    }

                                    i++;
                                }
                            }
                            //Adding Invoice list.
                            this.Add(esimate);

                        }
                        else { }
                    }
                    //Removing all the references from OutPut Xml document.
                    outputXMLDocOfEstimate.RemoveAll();

                    #endregion

                }
            }


        private void checkGroupLineNullEntries(XmlNode childNode, ref Estimate esimate)
        {
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.TxnLineID.ToString()) == null)
            {
                esimate.EstGroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.ItemRef.ToString()) == null)
            {
                esimate.EstGroupLineItemRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Desc.ToString()) == null)
            {
                esimate.EstGroupLineDescription.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Quantity.ToString()) == null)
            {
                esimate.EstGroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.UnitOfMeasure.ToString()) == null)
            {
                esimate.EstGroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.OverrideUOMSiteRef.ToString()) == null)
            {
                esimate.EstGroupLineOverrideUOMSetRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Rate.ToString()) == null)
            {
                esimate.EstGroupLineRate.Add(null);
            }         
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.ClassRef.ToString()) == null)
            {
                esimate.EstGroupLineClassRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Amount.ToString()) == null)
            {
                esimate.EstGroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.InventorySiteRef.ToString()) == null)
            {
                esimate.EstGroupLineInventorySiteRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.SalesTaxCodeRef.ToString()) == null)
            {
                esimate.EstGroupLineSalesTaxCodeRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.MarkupRate.ToString()) == null)
            {
                esimate.EstGroupLineMarkUpRate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.MarkupRatePercent.ToString()) == null)
            {
                esimate.EstGroupLineMarkUpRatePercent.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Other1.ToString()) == null)
            {
                esimate.EstGroupLineOther1.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.Other2.ToString()) == null)
            {
                esimate.EstGroupLineOther2.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlEstimateList.DataExtRet.ToString()) == null)
            {
                esimate.DataExtName.Add(null);
                esimate.DataExtValue.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref Estimate esimate)
        {
            if (node.SelectSingleNode("./" + QBXmlEstimateList.TxnLineID.ToString()) == null)
            {
                esimate.TxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.ItemRef.ToString()) == null)
            {
                esimate.EstLineItemRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Desc.ToString()) == null)
            {
                esimate.EstLineDescription.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Quantity.ToString()) == null)
            {
                esimate.EstLineQuantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.UnitOfMeasure.ToString()) == null)
            {
                esimate.EstLineUOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.OverrideUOMSiteRef.ToString()) == null)
            {
                esimate.EstLineOverrideUOMSetRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Rate.ToString()) == null)
            {
                esimate.EstLineRate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.ClassRef.ToString()) == null)
            {
                esimate.EstLineClassRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Amount.ToString()) == null)
            {
                esimate.EstLineAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.InventorySiteRef.ToString()) == null)
            {
                esimate.EstLineInventorySiteRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.SalesTaxCodeRef.ToString()) == null)
            {
                esimate.EstLineSalesTaxCodeRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.MarkupRate.ToString()) == null)
            {
                esimate.EstLineMarkUpRate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.MarkupRatePercent.ToString()) == null)
            {
                esimate.EstLineMarkUpRatePercent.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Other1.ToString()) == null)
            {
                esimate.EstLineOther1.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.Other2.ToString()) == null)
            {
                esimate.EstLineOther2.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlEstimateList.DataExtRet.ToString()) == null)
            {
                esimate.DataExtName.Add(null);
                esimate.DataExtValue.Add(null);
            }
        }


        /// <summary>
        /// This method is used for getting Estimates by passing Company file name
        /// and From and to date.
        /// </summary>
        /// <param name="QBFileName">Passing QuickBooks company file name.</param>
        /// <param name="FromDate">From date of Sales order.</param>
        /// <param name="ToDate">To date of sales order.</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateEntityFilter(string QBFileName, List<string> entityFilter)
            {

                #region Getting Estimate List from QuickBooks.
               
                //Getting item list from QuickBooks.
                string estimateXmlList = string.Empty;
                string countryName = string.Empty;

                estimateXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("EstimateQueryRq", QBFileName, entityFilter);
                getEstimateDetails(estimateXmlList);
               

                #endregion


               

                //Returning object.
                return this;


            } // End of PopulateEstimateEntityFilter Function

             


        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {

            #region Getting Estimate List from QuickBooks.

            //Getting item list from QuickBooks.
           
            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            string countryName = string.Empty;

   
            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        } // End of PopulateEstimateList Function


        public Collection<Estimate> PopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Sales order List from QuickBooks.
            
            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate);
            string countryName = string.Empty;

  
            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);

            //Returning object.
            return this;


        } // End of PopulateEstimateList Function



        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Estimate List from QuickBooks.
            

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            string countryName = string.Empty;

            //salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        } // End of ModifiedPopulateEstimateList Function

        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {

            #region Getting Estimate List from QuickBooks.
            

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            string countryName = string.Empty;

            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of ModifiedPopulateEstimateList Function


        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Estimate List from QuickBooks.
            

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate);
            string countryName = string.Empty;

            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of ModifiedPopulateEstimateList Function

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate);
      
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of ModifiedPopulateEstimateList Function
        

        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName, string FromRefnum, string ToRefNum, List<string> entityFilter)
        {

            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromRefnum, ToRefNum, entityFilter);
        
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of ModifiedPopulateEstimateList Function


        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName, string FromRefnum, string ToRefNum)
        {

            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromRefnum, ToRefNum);
         
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of PopulateEstimateList Function

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <param name="ToRefNum">Entity filter</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {
            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum, entityFilter);
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of PopulateEstimateList Function

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName)
        {
            #region Getting Estimate List from QuickBooks.
            

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName);
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;


            Estimate esimate;
            #endregion

            getEstimateDetails(estimateXmlList);

            //Returning object.
            return this;


        }// End of PopulateEstimateList Function

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> PopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {

            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
           
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of PopulateEstimateList Function


        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <param name="ToRefNum">Entity Filter</param>
        /// <returns></returns>
        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {

            #region Getting Estimate List from QuickBooks.
            

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum, entityFilter);
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);


            //Returning object.
            return this;


        }// End of PopulateEstimateList Function

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<Estimate> ModifiedPopulateEstimateList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {

            #region Getting Estimate List from QuickBooks.
           

            //Getting item list from QuickBooks.
            string estimateXmlList = string.Empty;
            estimateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("EstimateQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;


            Estimate esimate;
            #endregion


            getEstimateDetails(estimateXmlList);

            //Returning object.
            return this;


        }// End of PopulateEstimateList Function

        //Axis 706
        public List<string> PopulateEstimateTxnIdAndGroupTxnIdInSequence(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Estimate List from QuickBooks.

            //Getting item list from QuickBooks.
            List<string> EstimateTxnIdAndGroupTxnId = new List<string>();
            string EstimateXmlList = string.Empty;
            EstimateXmlList = QBCommonUtilities.GetListFromQuickBook("EstimateQueryRq", QBFileName, FromRefnum, ToRefnum);

            Estimate EstimateList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfEstimate = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (EstimateXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfEstimate.LoadXml(EstimateXmlList);
                XmlFileData = EstimateXmlList;

                #region Getting estimate Values from XML

                //Getting estimate values from QuickBooks response.
                XmlNodeList EstimateNodeList = outputXMLDocOfEstimate.GetElementsByTagName(QBXmlEstimateList.EstimateRet.ToString());

                foreach (XmlNode EstimatesNodes in EstimateNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        EstimateList = new Estimate();
                        foreach (XmlNode node in EstimatesNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBXmlEstimateList.EstimateLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlEstimateList.TxnLineID.ToString()))
                                    {
                                        EstimateList.TxnID = childNode.InnerText;
                                        EstimateTxnIdAndGroupTxnId.Add(EstimateList.TxnID);
                                    }
                                }
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBXmlEstimateList.EstimateLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBXmlEstimateList.ItemGroupRef.ToString()) == null)
                                {
                                    EstimateList.EstLineGroupItemRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlEstimateList.Desc.ToString()) == null)
                                {
                                    EstimateList.EstGroupLineDescription.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlEstimateList.Quantity.ToString()) == null)
                                {
                                    EstimateList.EstGroupLineQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBXmlEstimateList.UnitOfMeasure.ToString()) == null)
                                {
                                    EstimateList.EstGroupLineUOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlEstimateList.IsPrintItemInGroup.ToString()) == null)
                                {
                                    //EstimateList.EstLineGroupIsPrintItemInGroup.Add();
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        string GroupId = "";
                                        if (childNode.InnerText.Length > 36)
                                            GroupId = (childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            GroupId = childNode.InnerText;

                                        EstimateTxnIdAndGroupTxnId.Add("GroupTxnLineID_" + GroupId);

                                    }
                                }
                            }
                            //

                        }
                    }
                }
                //Adding Invoice list.
                #endregion
            }
            return EstimateTxnIdAndGroupTxnId;
        }
        //Axis 706 ends

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


      
    }

    }
