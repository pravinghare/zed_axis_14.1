using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    #region Public properties
    public class OtherList : BaseOtherList
    {
        #region Public Properties
        public string[] ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }


        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }


        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        public string Addr1
        {
            get { return m_OtherNameAddress1; }
            set { m_OtherNameAddress1 = value; }
        }

        public string Addr2
        {
            get { return m_OtherNameAddress2; }
            set { m_OtherNameAddress2 = value; }
        }

        public string Addr3
        {
            get { return m_OtherNameAddress3; }
            set { m_OtherNameAddress3 = value; }
        }

        public string Addr4
        {
            get { return m_OtherNameAddress4; }
            set { m_OtherNameAddress4 = value; }
        }

        public string Addr5
        {
            get { return m_OtherNameAddress5; }
            set { m_OtherNameAddress5 = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }


        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }


        public string Note
        {
            get { return m_Note; }
            set { m_Note = value; }
        }


        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string AltPhone
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }

        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string AltContact
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

       
        public string AccountNumber
        {
            get { return m_AccountNumber; }
            set { m_AccountNumber = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        public string OwnerID
        {
            get { return m_OwnerID; }
            set { m_OwnerID = value; }
        }

        

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];


        #endregion
    }

    public class QBOtherListCollection : Collection<OtherList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        public List<string> GetAllOtherNameList(string QBFileName)
        {
            string otherNameXmlList = QBCommonUtilities.GetListFromQuickBook("OtherNameQueryRq", QBFileName);
            List<string> otherNameList = new List<string>();
            List<string> otherNameList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(otherNameXmlList);
            XmlFileData = otherNameXmlList;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList OtherNameNodeList = outputXMLDoc.GetElementsByTagName(QBOtherList.OtherNameRet.ToString());
            foreach (XmlNode othNodes in OtherNameNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in othNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBOtherList.Name.ToString()))
                        {
                            if (!otherNameList.Contains(node.InnerText))
                            {
                                otherNameList.Add(node.InnerText);
                                string name = node.InnerText + "     :     Other";
                                otherNameList1.Add(name);

                            }
                        }
                    }
                }
            }
            CommonUtilities.GetInstance().OtherListWithType = otherNameList1;
            //463
            XmlFileData = null;
            outputXMLDoc = null;
            otherNameXmlList = null;
            OtherNameNodeList = null;
            //463
            return otherNameList;
        }


        /// <summary>
        /// Only Since LastExport
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<OtherList> PopulateOtherList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.
            int i = 0;
            //Getting item list from QuickBooks.
            string OtherXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBookByDate("OtherNameQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("OtherNameQueryRq", QBFileName, FromDate, ToDate);
            }

            OtherList OtherList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfOther = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (OtherXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfOther.LoadXml(OtherXmlList);
                XmlFileData = OtherXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList OtherNodeList = outputXMLDocOfOther.GetElementsByTagName(QBOtherList.OtherNameRet.ToString());

                foreach (XmlNode OtherNodes in OtherNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        OtherList = new OtherList();
                        foreach (XmlNode node in OtherNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBOtherList.ListID.ToString()))
                            {
                                OtherList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBOtherList.Name.ToString()))
                            {
                                OtherList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBOtherList.IsActive.ToString()))
                            {
                                OtherList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBOtherList.CompanyName.ToString()))
                            {
                                OtherList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBOtherList.Salutation.ToString()))
                            {
                                OtherList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBOtherList.FirstName.ToString()))
                            {
                                OtherList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBOtherList.MiddleName.ToString()))
                            {
                                OtherList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBOtherList.LastName.ToString()))
                            {
                                OtherList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBOtherList.OtherNameAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBOtherList.Addr1.ToString()))
                                        OtherList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr2.ToString()))
                                        OtherList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr3.ToString()))
                                        OtherList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr4.ToString()))
                                        OtherList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr5.ToString()))
                                        OtherList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBOtherList.City.ToString()))
                                        OtherList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.State.ToString()))
                                        OtherList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.PostalCode.ToString()))
                                        OtherList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Country.ToString()))
                                        OtherList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Note.ToString()))
                                        OtherList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBOtherList.Phone.ToString()))
                            {
                                OtherList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBOtherList.AltPhone.ToString()))
                            {
                                OtherList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBOtherList.Fax.ToString()))
                            {
                                OtherList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBOtherList.Email.ToString()))
                            {
                                OtherList.Email = node.InnerText;
                            }



                            //Checking Contact
                            if (node.Name.Equals(QBOtherList.Contact.ToString()))
                            {
                                OtherList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBOtherList.AltContact.ToString()))
                            {
                                OtherList.AltContact = node.InnerText;
                            }



                            //Checking AccountNumber
                            if (node.Name.Equals(QBOtherList.AccountNumber.ToString()))
                            {
                                OtherList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBOtherList.Notes.ToString()))
                            {
                                OtherList.Notes = node.InnerText;
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBOtherList.ExternalGUID.ToString()))
                            {
                                OtherList.ExternalGUID = node.InnerText;
                            }

                           
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    OtherList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        OtherList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }



                        }

                        this.Add(OtherList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfOther.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }


        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<OtherList> PopulateOtherList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Other List from QuickBooks.
            int i = 0;

            //Getting item list from QuickBooks.
            string OtherXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBook("OtherNameQueryRq", QBFileName);
            }
            else
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("OtherNameQueryRq", QBFileName);
            }

            OtherList OtherList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfOther = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (OtherXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfOther.LoadXml(OtherXmlList);
                XmlFileData = OtherXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList OtherNodeList = outputXMLDocOfOther.GetElementsByTagName(QBOtherList.OtherNameRet.ToString());

                foreach (XmlNode OtherNodes in OtherNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        OtherList = new OtherList();
                        foreach (XmlNode node in OtherNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBOtherList.ListID.ToString()))
                            {
                                OtherList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBOtherList.Name.ToString()))
                            {
                                OtherList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBOtherList.IsActive.ToString()))
                            {
                                OtherList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBOtherList.CompanyName.ToString()))
                            {
                                OtherList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBOtherList.Salutation.ToString()))
                            {
                                OtherList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBOtherList.FirstName.ToString()))
                            {
                                OtherList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBOtherList.MiddleName.ToString()))
                            {
                                OtherList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBOtherList.LastName.ToString()))
                            {
                                OtherList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBOtherList.OtherNameAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBOtherList.Addr1.ToString()))
                                        OtherList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr2.ToString()))
                                        OtherList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr3.ToString()))
                                        OtherList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr4.ToString()))
                                        OtherList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr5.ToString()))
                                        OtherList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBOtherList.City.ToString()))
                                        OtherList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.State.ToString()))
                                        OtherList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.PostalCode.ToString()))
                                        OtherList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Country.ToString()))
                                        OtherList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Note.ToString()))
                                        OtherList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBOtherList.Phone.ToString()))
                            {
                                OtherList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBOtherList.AltPhone.ToString()))
                            {
                                OtherList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBOtherList.Fax.ToString()))
                            {
                                OtherList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBOtherList.Email.ToString()))
                            {
                                OtherList.Email = node.InnerText;
                            }



                            //Checking Contact
                            if (node.Name.Equals(QBOtherList.Contact.ToString()))
                            {
                                OtherList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBOtherList.AltContact.ToString()))
                            {
                                OtherList.AltContact = node.InnerText;
                            }



                            //Checking AccountNumber
                            if (node.Name.Equals(QBOtherList.AccountNumber.ToString()))
                            {
                                OtherList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBOtherList.Notes.ToString()))
                            {
                                OtherList.Notes = node.InnerText;
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBOtherList.ExternalGUID.ToString()))
                            {
                                OtherList.ExternalGUID = node.InnerText;
                            }

                            
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    OtherList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        OtherList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }





                        }

                        this.Add(OtherList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfOther.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// This method is used for getting OtherName List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<OtherList> PopulateOtherList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.
            int i = 0;
            //Getting item list from QuickBooks.
            string OtherXmlList = string.Empty;           
            if (inActiveFilter == false)//bug no. 395
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBookByDate("OtherNameQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                OtherXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("OtherNameQueryRq", QBFileName, FromDate, ToDate);
            }

            OtherList OtherList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfOther = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (OtherXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfOther.LoadXml(OtherXmlList);
                XmlFileData = OtherXmlList;

                #region Getting Vendor Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList OtherNodeList = outputXMLDocOfOther.GetElementsByTagName(QBOtherList.OtherNameRet.ToString());

                foreach (XmlNode OtherNodes in OtherNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        OtherList = new OtherList();
                        foreach (XmlNode node in OtherNodes.ChildNodes)
                        {

                            //Checking ListID. 
                            if (node.Name.Equals(QBOtherList.ListID.ToString()))
                            {
                                OtherList.ListID[0] = node.InnerText;
                            }


                            //Checking Name
                            if (node.Name.Equals(QBOtherList.Name.ToString()))
                            {
                                OtherList.Name = node.InnerText;
                            }

                            //Checking IsActive
                            if (node.Name.Equals(QBOtherList.IsActive.ToString()))
                            {
                                OtherList.IsActive = node.InnerText;
                            }

                            //Checking CompanyName
                            if (node.Name.Equals(QBOtherList.CompanyName.ToString()))
                            {
                                OtherList.CompanyName = node.InnerText;
                            }

                            //Checking Salutation
                            if (node.Name.Equals(QBOtherList.Salutation.ToString()))
                            {
                                OtherList.Salutation = node.InnerText;
                            }

                            //Checking FirstName
                            if (node.Name.Equals(QBOtherList.FirstName.ToString()))
                            {
                                OtherList.FirstName = node.InnerText;
                            }

                            //Checking MiddleName
                            if (node.Name.Equals(QBOtherList.MiddleName.ToString()))
                            {
                                OtherList.MiddleName = node.InnerText;
                            }

                            //Checking LastName
                            if (node.Name.Equals(QBOtherList.LastName.ToString()))
                            {
                                OtherList.LastName = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBOtherList.OtherNameAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBOtherList.Addr1.ToString()))
                                        OtherList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr2.ToString()))
                                        OtherList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr3.ToString()))
                                        OtherList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr4.ToString()))
                                        OtherList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Addr5.ToString()))
                                        OtherList.Addr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBOtherList.City.ToString()))
                                        OtherList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.State.ToString()))
                                        OtherList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.PostalCode.ToString()))
                                        OtherList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Country.ToString()))
                                        OtherList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBOtherList.Note.ToString()))
                                        OtherList.Note = childNode.InnerText;

                                }
                            }

                            //Checking Phone
                            if (node.Name.Equals(QBOtherList.Phone.ToString()))
                            {
                                OtherList.Phone = node.InnerText;
                            }

                            //Checking AltPhone
                            if (node.Name.Equals(QBOtherList.AltPhone.ToString()))
                            {
                                OtherList.AltPhone = node.InnerText;
                            }

                            //Checking Fax
                            if (node.Name.Equals(QBOtherList.Fax.ToString()))
                            {
                                OtherList.Fax = node.InnerText;
                            }

                            //Checking Email
                            if (node.Name.Equals(QBOtherList.Email.ToString()))
                            {
                                OtherList.Email = node.InnerText;
                            }



                            //Checking Contact
                            if (node.Name.Equals(QBOtherList.Contact.ToString()))
                            {
                                OtherList.Contact = node.InnerText;
                            }

                            //Checking AltContact
                            if (node.Name.Equals(QBOtherList.AltContact.ToString()))
                            {
                                OtherList.AltContact = node.InnerText;
                            }

                            

                            //Checking AccountNumber
                            if (node.Name.Equals(QBOtherList.AccountNumber.ToString()))
                            {
                                OtherList.AccountNumber = node.InnerText;
                            }

                            //Checking Note
                            if (node.Name.Equals(QBOtherList.Notes.ToString()))
                            {
                                OtherList.Notes = node.InnerText;
                            }

                            //Checking ExternalGUID
                            if (node.Name.Equals(QBOtherList.ExternalGUID.ToString()))
                            {
                                OtherList.ExternalGUID = node.InnerText;
                            }

                       
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    OtherList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        OtherList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }




                        }

                        this.Add(OtherList);
                    }
                    else
                    {
                        return null;
                    }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfOther.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
        #endregion


        
    }
    #endregion
}