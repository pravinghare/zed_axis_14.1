using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using DataProcessingBlocks;
using Interop.QBXMLRP2Lib;
using EDI.Constant;

namespace Streams
{
    // ===============================================================================
    // 
    // InventoryAdjusttReceive.cs
    //
    // This file is used to export the Inventory Adjustment Receive file (.ism) file into QuickBooks.
    //
    // Developed By : K.Gouraw.
    // Date : 25-3-2009
    // ==============================================================================


    [XmlRootAttribute("InventoryAdjusttReceive", Namespace = "", IsNullable = false)]
    public class InventoryAdjusttReceive
    {
        # region Private Member vaiable
        private AccountRef m_AccountRef=new AccountRef();
        private string m_TxnDate;
        private string m_Memo;
        private Collection<InventoryAdjustmentLineAdd> m_InventoryAdjustmentLineAdd = new Collection<InventoryAdjustmentLineAdd>();
       
        #endregion

        #region Public Properties
        public AccountRef AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {

            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }
               
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        [XmlArray("InventoryAdjustmentLineAddREM")]
        public Collection<InventoryAdjustmentLineAdd> InventoryAdjustmentLineAdd
        {
            get { return m_InventoryAdjustmentLineAdd; }
            set { m_InventoryAdjustmentLineAdd = value; }
        }

       
        #endregion

        #region private Method
        private string validateMessage()
        {
            //create an instance of valid message.
            string strValidMsg = string.Empty;
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<InventoryAdjusttReceive>.Save(this, fileName);
            }
            catch { }
            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);
            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillAddRq aggregate and fill in field values for it
            System.Xml.XmlElement inventoryAdjustmentAddRq = requestXmlDoc.CreateElement("InventoryAdjustmentAddRq");
            inner.AppendChild(inventoryAdjustmentAddRq);
            XmlElement inventAdjustAdd = requestXmlDoc.CreateElement("InventoryAdjustmentAdd");
            inventoryAdjustmentAddRq.AppendChild(inventAdjustAdd);

            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAddREM>", string.Empty);

            inventAdjustAdd.InnerXml = requestXML;
            strValidMsg = DataProcessingBlocks.CommonUtilities.GetInstance().CheckXml(requestXmlDoc);
            return strValidMsg;
        }

        #endregion

        #region Public Method
        public bool ExportToQuickBooks(ref string statusMessage, string QBFileName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                //Serialising the class.
                DataProcessingBlocks.ObjectXMLSerializer<InventoryAdjusttReceive>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillAddRq aggregate and fill in field values for it
            System.Xml.XmlElement inventoryAdjustmentAddRq = requestXmlDoc.CreateElement("InventoryAdjustmentAddRq");
            inner.AppendChild(inventoryAdjustmentAddRq);
            XmlElement inventAdjustAdd = requestXmlDoc.CreateElement("InventoryAdjustmentAdd");
            inventoryAdjustmentAddRq.AppendChild(inventAdjustAdd);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAddREM>", string.Empty);

            inventAdjustAdd.InnerXml = requestXML;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {

                //for saving request qbxml file
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
               
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentAddRs"))
                    {
                        string statusSeverity=string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                            if (statusSeverity == "Error")
                            {
                                statusMessage += "\n ";
                                statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                                statusMessage += "\n ";
                            }
                        }
                        catch { }
                    }

                    //for saving response qbxml file
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += this.validateMessage();
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
      

    }

    /// <summary>
    /// This class is used to store object of InventoryAdjusttReceive.cs.
    /// </summary>
    public class InventoryAdjustmentReceiveCollection : Collection<InventoryAdjusttReceive>
    {

    }


    /// <summary>
    /// This class is used as subclass of InventoryAdustmentReceive
    /// </summary>
    [XmlRootAttribute("InventoryAdjustmentLineAdd", Namespace = "", IsNullable = false)]
    public class InventoryAdjustmentLineAdd
    {
        private ItemRef m_ItemRef=new ItemRef();
        private QuantityAdjustment m_QuantityAdjustment=new QuantityAdjustment();
        public InventoryAdjustmentLineAdd()
        {
        }

        public ItemRef ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }

        public QuantityAdjustment QuantityAdjustment
        {
            get { return m_QuantityAdjustment; }
            set { m_QuantityAdjustment = value; }
        }
        
    }

    /// <summary>
    /// This class is used as subclass of InventoryAdjustmentLineAdd.
    /// </summary>
    [XmlRootAttribute("QuantityAdjustment", Namespace = "", IsNullable = false)]
    public class QuantityAdjustment 
    {
        private string m_QuantityDifference;
        public QuantityAdjustment()
        {
        }
        public string QuantityDifference
        {
            get { return m_QuantityDifference; }
            set { m_QuantityDifference = value; }
        }
    }
}
