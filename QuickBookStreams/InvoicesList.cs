using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class provides Properties and methods for Invoices.
    /// </summary>
    public class InvoicesList : BaseInvoicesList
    {
        public InvoicesList()
        {
            m_ItemDetails = new Collection<QBItemDetails>();
        }

        #region Public Mehtods

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string CustomerListID
        {
            get { return m_CustomerListID; }
            set { m_CustomerListID = value; }
        }


        public string CustomerFullName
        {
            get { return m_CustomerFullName; }
            set { m_CustomerFullName = value; }
        }

        public string ClassFullName
        {
            get { return m_ClassFullName; }
            set { m_ClassFullName = value; }
        }

        public string ARAccountFullName
        {
            get { return m_ARAccountFullName; }
            set { m_ARAccountFullName = value; }
        }

        public string TemplateFullName
        {
            get { return m_TemplateFullName; }
            set { m_TemplateFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        //Bill Address
        public string Addr1
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }


        public string Addr2
        {
            get { return m_BillAddress1; }
            set { m_BillAddress1 = value; }
        }
        public string Addr3
        {
            get { return this.m_BillAddress2; }
            set { this.m_BillAddress2 = value; }
        }
        public string Addr4
        {
            get { return this.m_BillAddress3; }
            set { this.m_BillAddress3 = value; }
        }
        public string Addr5
        {
            get { return this.m_BillAddress4; }
            set { this.m_BillAddress4 = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        //Ship Address
        public string shipAddr1
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        public string ShipAddr2
        {
            get { return m_ShipAddress1; }
            set { m_ShipAddress1 = value; }
        }


        //Axis 10.0 
        // Issue No. 417
        public string ShipAddr3
        {
            get { return m_ShipAddress2; }
            set { m_ShipAddress2 = value; }
        }


        public string ShipAddr4
        {
            get { return m_ShipAddress3; }
            set { m_ShipAddress3 = value; }
        }


        public string ShipAddr5
        {
            get { return m_ShipAddress4; }
            set { m_ShipAddress4 = value; }
        }




        public string ShipCity
        {
            get { return m_ShipCity; }
            set { m_ShipCity = value; }
        }

        public string ShipState
        {
            get { return m_ShipState; }
            set { m_ShipState = value; }
        }

        public string ShipPostalCode
        {
            get { return m_ShipPostalCode; }
            set { m_ShipPostalCode = value; }
        }

        public string ShipCountry
        {
            get { return m_ShipCountry; }
            set { m_ShipCountry = value; }
        }

        public string ISPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }

        public string IsFinanceCharge
        {
            get { return m_IsFinanceCharge; }
            set { m_IsFinanceCharge = value; }
        }

        public string PONumber
        {
            get { return m_PONumber; }
            set { m_PONumber = value; }
        }

        public string TermsFullName
        {
            get { return m_TermsFullName; }
            set { m_TermsFullName = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public string SalesRepFullName
        {
            get { return m_SalesRepFullName; }
            set { m_SalesRepFullName = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }
        }

        public string ShipMethodFullName
        {
            get { return m_ShipMethodFullName; }
            set { m_ShipMethodFullName = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ItemSalesTaxFullName
        {
            get { return m_ItemSalesTaxFullName; }
            set { m_ItemSalesTaxFullName = value; }
        }

        public string SalesTaxPercentage
        {
            get { return m_SalesTaxPercentage; }
            set { m_SalesTaxPercentage = value; }
        }

        public decimal AppliedAmount
        {
            get { return m_AppliedAmount; }
            set { m_AppliedAmount = value; }
        }

        public decimal BalanceRemaining
        {
            get { return m_BalanceRemaining; }
            set { m_BalanceRemaining = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value; }
        }

        public decimal ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal BalanceRemainingInHomeCurrency
        {
            get { return m_BalanceRemainingInHomeCurrency; }
            set { m_BalanceRemainingInHomeCurrency = value; }
        }

        public string IsPaid
        {
            get { return m_IsPaid; }
            set { m_IsPaid = value; }
        }

        public string CustomerMsgFullName
        {
            get { return m_CustomerMsgFullName; }
            set { m_CustomerMsgFullName = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string CustomerSalesTaxCodeFullName
        {
            get { return m_CustomerSalesTaxCodeFullName; }
            set { m_CustomerSalesTaxCodeFullName = value; }
        }

        public decimal SuggestedDiscountAmount
        {
            get { return m_SuggestedDiscountAmount; }
            set { m_SuggestedDiscountAmount = value; }
        }

        public string SuggestedDiscountDate
        {
            get { return m_SuggestedDiscountDate; }
            set { m_SuggestedDiscountDate = value; }
        }

        public string Other
        {
            get { return m_Other; }
            set { m_Other = value; }
        }

        public List<string> Other1
        {
            get { return m_Other1; }
            set { m_Other1 = value; }
        }

        public List<string> Other2
        {
            get { return m_Other2; }
            set { m_Other2 = value; }
        }

        public string LinkTxnID
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }

        //For InvoiceLineRet
        public List<string> TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_QBitemFullName; }
            set { m_QBitemFullName = value; }
        }

        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_QBquantity; }
            set { m_QBquantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<string> OverrideUOMFullName
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }
        public List<string> ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }
        public List<decimal?> Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }

        public List<decimal?> Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public List<string> InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }

        // axis 10.0 changes

        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }


        // axis 10.0 changes ends

        public List<string> ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }

        public List<string> SalesTaxCodeFullName
        {
            get { return m_SalesTaxCodeFullName; }
            set { m_SalesTaxCodeFullName = value; }
        }

        //For InvoiceLineGroupRet
        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemGroupFullName; }
            set { m_ItemGroupFullName = value; }
        }

        public List<string> GroupDesc
        {
            get { return m_GroupDesc; }
            set { m_GroupDesc = value; }
        }

        public List<decimal?> GroupQuantity
        {
            get { return m_GroupQuantity; }
            set { m_GroupQuantity = value; }
        }

        public List<string> GroupUOM
        {
            get { return m_GroupUOM; }
            set { m_GroupUOM = value; }
        }
        public List<string> IsPrintItemsInGroup
        {
            get { return m_IsPrintItemsInGroup; }
            set { m_IsPrintItemsInGroup = value; }
        }


        //For Sub InvoiceLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupQBTxnLineNumber; }
            set { m_GroupQBTxnLineNumber = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<string> GroupLineOverrideUOM
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }

        public List<string> GroupLineClassRefFullName
        {
            get { return m_GroupLineClassRefFullName; }
            set { m_GroupLineClassRefFullName = value; }
        }

        public List<decimal?> GroupLineRate
        {
            get { return m_GroupLineRate; }
            set { m_GroupLineRate = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineServiceDate
        {
            get { return m_GroupLineServiceDate; }
            set { m_GroupLineServiceDate = value; }
        }

        public List<string> GroupLineSalesTaxCodeFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }

        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();

        public Collection<QBItemDetails> QBItemDetails
        {
            get { return m_ItemDetails; }
            set { m_ItemDetails = value; }

        }

        //616
        public List<string> LineOther1
        {
            get { return m_LineOther1; }
            set { m_LineOther1 = value; }
        }

        public List<string> LineOther2
        {
            get { return m_LineOther2; }
            set { m_LineOther2 = value; }
        }

        #endregion
    }

    /// <summary>
    /// Collection class used for getting invoices list from QuickBook.
    /// </summary>
    public class QBInvoicesListCollection : Collection<InvoicesList>
    {
        string XmlFileData = string.Empty;

        QBItemDetails itemList = null;


        /// <summary>
        /// This method is used for getting invoices List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;

            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting invoices List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;

            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }


                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            //InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("InvoiceQueryRq", QBFileName, FromDate, ToDate);
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            //InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("InvoiceQueryRq", QBFileName, FromDate, ToDate);
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            //InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("InvoiceQueryRq", QBFileName, FromDate, ToDate);
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting invoices List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Invoices List from QuickBooks.
            // int count = 0;
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            //InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("InvoiceQueryRq", QBFileName, FromDate, ToDate);
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getiing invoices list by passing Comapany file Name,
        /// FromRefnumber and ToRefNumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.

            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }
                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }
                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }


            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getiing invoices list by passing Comapany file Name,
        /// FromRefnumber and ToRefNumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.

            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromRefnum, ToRefnum);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                {
                                    InvoicesList.ItemFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.Desc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.Quantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                {
                                    InvoicesList.OverrideUOMFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                {
                                    InvoicesList.Rate.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                {
                                    InvoicesList.ClassRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                {
                                    InvoicesList.Amount.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
                                {
                                    InvoicesList.InventorySiteRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
                                {
                                    InvoicesList.SerialNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
                                {
                                    InvoicesList.LotNumber.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                {
                                    InvoicesList.SalesTaxCodeFullName.Add("");
                                }

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        //P Axis 13.2 : issue 695
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineItemFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineDesc.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineQuantity.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineOverrideUOM.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineRate.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineClassRefFullName.Add("");
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
                                        {
                                            InvoicesList.GroupLineAmount.Add(null);
                                        }
                                        if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
                                        {
                                            InvoicesList.SalesTaxCodeFullName.Add("");
                                        }

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }


            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting invoices list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }




        /// <summary>
        /// This method is used for getting invoices list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting invoices list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting invoices list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            return this;
        }


        /// <summary>
        /// This method is used for getting Invoices List by Passing company filename,
        /// Entityfilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("InvoiceQueryRq", QBFileName, entityFilter);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Invoices List by Passing company filename,
        /// PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string PaidStatus, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, PaidStatus, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Invoices List by Passing company filename,
        /// PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, PaidStatus);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Invoices List by passing comapny FileName,
        /// TxnDateRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookDatePaid("InvoiceQueryRq", QBFileName, FromDate, ToDate, PaidStatus, entityFilter);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Invoices List by passing comapny FileName,
        /// TxnDateRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookDatePaid("InvoiceQueryRq", QBFileName, FromDate, ToDate, PaidStatus);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Invoices List by passing comapny FileName,
        /// TxnDateRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus, List<string> entityFilter, bool flag)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookDatePaid("InvoiceQueryRq", QBFileName, FromDate, ToDate, PaidStatus, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Invoices List by passing comapny FileName,
        /// TxnDateRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookDatePaid("InvoiceQueryRq", QBFileName, FromDate, ToDate, PaidStatus);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Invioces List by passing company FileName,
        /// RefNumberRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookRefPaid("InvoiceQueryRq", QBFileName, FromRefnum, ToRefnum, PaidStatus, entityFilter);

            InvoicesList InvoicesList;
            #endregion
            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used for getting Invioces List by passing company FileName,
        /// RefNumberRangeFilter, PaidStatus
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookRefPaid("InvoiceQueryRq", QBFileName, FromRefnum, ToRefnum, PaidStatus);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// THis method is used for getiing Invoices lSIt by passing company FileName
        /// TxnDateRangeFilter, RefNumberRangeFilter, PaidStatus,entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus, entityFilter);

            InvoicesList InvoicesList;
            #endregion

            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName);
            #endregion
            getInvoiceData(InvoicesXmlList);
            return this;
        }

        /// <summary>
        /// THis method is used for getiing Invoices lSIt by passing company FileName
        /// TxnDateRangeFilter, RefNumberRangeFilter, PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<InvoicesList> PopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus);

            InvoicesList InvoicesList;
            #endregion


            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// THis method is used for getiing Invoices lSIt by passing company FileName
        /// TxnDateRangeFilter, RefNumberRangeFilter, PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus, List<string> entityFilter)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus, entityFilter);


            #endregion
            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }





        /// <summary>
        /// THis method is used for getiing Invoices lSIt by passing company FileName
        /// TxnDateRangeFilter, RefNumberRangeFilter, PaidStatus.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<InvoicesList> ModifiedPopulateInvoicesList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, string PaidStatus)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.ModifiedGetListFromQuickBookPaidStatus("InvoiceQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, PaidStatus);

            InvoicesList InvoicesList;
            #endregion


            getInvoiceData(InvoicesXmlList);

            //Returning object.
            return this;
        }


        //Axis 706
        public List<string> PopulateInvoicesTxnIdAndGroupTxnIdInSequence(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.
            List<string> InvoicesTxnIdAndGroupTxnId = new List<string>();
            string InvoicesXmlList = string.Empty;
            InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromRefnum, ToRefnum);

            InvoicesList InvoicesList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        InvoicesList.TxnID = childNode.InnerText;
                                        InvoicesTxnIdAndGroupTxnId.Add(InvoicesList.TxnID);
                                    }
                                }
                            }



                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBInvoicesList.ItemGroupRef.ToString()) == null)
                                {
                                    InvoicesList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
                                {
                                    InvoicesList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
                                {
                                    InvoicesList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
                                {
                                    InvoicesList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBInvoicesList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    InvoicesList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        string GroupId = "";
                                        if (childNode.InnerText.Length > 36)
                                            GroupId = (childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            GroupId = childNode.InnerText;

                                        InvoicesTxnIdAndGroupTxnId.Add("GroupTxnLineID_" + GroupId);

                                    }
                                }
                            }
                            //

                        }
                    }



                }
                //Adding Invoice list.
                #endregion
            }
            return InvoicesTxnIdAndGroupTxnId;
        }
        //Axis 706 ends

        public void getInvoiceData(string InvoicesXmlList)
        {
            InvoicesList InvoicesList;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInvoices = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (InvoicesXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfInvoices.LoadXml(InvoicesXmlList);
                XmlFileData = InvoicesXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList InvoicesNodeList = outputXMLDocOfInvoices.GetElementsByTagName(QBInvoicesList.InvoiceRet.ToString());

                foreach (XmlNode InvoicesNodes in InvoicesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        InvoicesList = new InvoicesList();
                        foreach (XmlNode node in InvoicesNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBInvoicesList.TxnID.ToString()))
                            {
                                InvoicesList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.EndsWith(QBInvoicesList.TxnNumber.ToString()))
                            {
                                InvoicesList.TxnNumber = node.InnerText;
                            }
                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBInvoicesList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.ListID.ToString()))
                                        InvoicesList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRef List.
                            if (node.Name.Equals(QBInvoicesList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ARAccountFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBInvoicesList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TemplateFullName = childNode.InnerText;
                                }

                            }
                            //Checking TxnDate.
                            if (node.Name.Equals(QBInvoicesList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking RefNumber.
                            if (node.Name.Equals(QBInvoicesList.RefNumber.ToString()))
                                InvoicesList.RefNumber = node.InnerText;


                            //Checking BillAddress.
                            if (node.Name.Equals(QBInvoicesList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.Addr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.Addr5 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.Country = childNode.InnerText;
                                }
                            }

                            //Checking ShipAddress
                            if (node.Name.Equals(QBInvoicesList.ShipAddress.ToString()))
                            {
                                //Getting Values of Address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.Addr1.ToString()))
                                        InvoicesList.shipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr2.ToString()))
                                        InvoicesList.ShipAddr2 = childNode.InnerText;

                                    //Axis 10.0
                                    if (childNode.Name.Equals(QBInvoicesList.Addr3.ToString()))
                                        InvoicesList.ShipAddr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr4.ToString()))
                                        InvoicesList.ShipAddr4 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Addr5.ToString()))
                                        InvoicesList.ShipAddr5 = childNode.InnerText;

                                    if (childNode.Name.Equals(QBInvoicesList.City.ToString()))
                                        InvoicesList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.State.ToString()))
                                        InvoicesList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.PostalCode.ToString()))
                                        InvoicesList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBInvoicesList.Country.ToString()))
                                        InvoicesList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking IsPending
                            if (node.Name.Equals(QBInvoicesList.IsPending.ToString()))
                            {
                                InvoicesList.ISPending = node.InnerText;
                            }

                            //Checking IsFinanceCharge                  
                            if (node.Name.Equals(QBInvoicesList.IsFinanceCharge.ToString()))
                            {
                                InvoicesList.IsFinanceCharge = node.InnerText;
                            }

                            //Checking PONumber values.
                            if (node.Name.Equals(QBInvoicesList.PONumber.ToString()))
                            {
                                InvoicesList.PONumber = node.InnerText;
                            }

                            //Checking TermsRef
                            if (node.Name.Equals(QBInvoicesList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.TermsFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBInvoicesList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.DueDate = node.InnerText;
                                }
                            }

                            //Checking SalesRepRef
                            if (node.Name.Equals(QBInvoicesList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.SalesRepFullName = childNode.InnerText;
                                }
                            }

                            //FOB
                            if (node.Name.Equals(QBInvoicesList.FOB.ToString()))
                            {
                                InvoicesList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBInvoicesList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.ShipDate = node.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBInvoicesList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ShipMethodFullName = childNode.InnerText;
                                }
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInvoicesList.Memo.ToString()))
                            {
                                InvoicesList.Memo = node.InnerText;
                            }

                            //Checking ItemSalesTaxRef
                            if (node.Name.Equals(QBInvoicesList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.ItemSalesTaxFullName = childNode.InnerText;
                                }
                            }

                            //Checking SalesTAxPercentage
                            if (node.Name.Equals(QBInvoicesList.SalesTaxPercentage.ToString()))
                            {
                                InvoicesList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking AppliedAmount
                            if (node.Name.Equals(QBInvoicesList.AppliedAmount.ToString()))
                            {
                                InvoicesList.AppliedAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking BalanceRemaining
                            if (node.Name.Equals(QBInvoicesList.BalanceRemaining.ToString()))
                            {
                                InvoicesList.BalanceRemaining = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CurrencyRef
                            if (node.Name.Equals(QBInvoicesList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBInvoicesList.ExchangeRate.ToString()))
                            {
                                InvoicesList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking BalanceRemaningInHomeCurrency
                            if (node.Name.Equals(QBInvoicesList.BalanceRemainingInHomeCurrency.ToString()))
                            {
                                InvoicesList.BalanceRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking IsPaid
                            if (node.Name.Equals(QBInvoicesList.IsPaid.ToString()))
                            {
                                InvoicesList.IsPaid = node.InnerText;
                            }
                            //Checking CustomerMsgRef
                            if (node.Name.Equals(QBInvoicesList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerMsgFullName = childNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBInvoicesList.IsToBePrinted.ToString()))
                            {
                                InvoicesList.IsToBePrinted = node.InnerText;
                            }
                            //Checking IsToBeEmailed
                            if (node.Name.Equals(QBInvoicesList.IsToBeEmailed.ToString()))
                            {
                                InvoicesList.IsToBeEmailed = node.InnerText;
                            }
                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBInvoicesList.IsTaxIncluded.ToString()))
                            {
                                InvoicesList.IsTaxIncluded = node.InnerText;
                            }
                            //Checking CustomerSalesTaxCode
                            if (node.Name.Equals(QBInvoicesList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.FullName.ToString()))
                                        InvoicesList.CustomerSalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Suggested DiscountAmount
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountAmount.ToString()))
                            {
                                InvoicesList.SuggestedDiscountAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking SuggestedDiscountDate
                            if (node.Name.Equals(QBInvoicesList.SuggestedDiscountDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDiscountDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InvoicesList.SuggestedDiscountDate = dtDiscountDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InvoicesList.SuggestedDiscountDate = node.InnerText;
                                }
                            }
                            //Checking Other
                            if (node.Name.Equals(QBInvoicesList.Other.ToString()))
                            {
                                InvoicesList.Other = node.InnerText;
                            }

                            //Checking LinkTxn
                            if (node.Name.Equals(QBInvoicesList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInvoicesList.TxnID.ToString()))
                                        InvoicesList.LinkTxnID = childNode.InnerText;
                                }
                            }

                            //Checking Invoice Line ret values.


                            if (node.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                            {
                                checkNullEntries(node, ref InvoicesList);

                                bool dataextFlag = false;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        InvoicesList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Quantity.Add(null); }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Rate.Add(null); }
                                    }
                                    //Checking ClassRef value.
                                    if (childNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    InvoicesList.ClassRefFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            InvoicesList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.Amount.Add(null); }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBInvoicesList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }


                                    // axis 10.0 changes
                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBInvoicesList.SerialNumber.ToString()))
                                    {
                                        InvoicesList.SerialNumber.Add(childNode.InnerText);
                                    }

                                    //Checking LotNumber
                                    if (childNode.Name.Equals(QBInvoicesList.LotNumber.ToString()))
                                    {
                                        InvoicesList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                InvoicesList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            InvoicesList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                InvoicesList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                    {
                                        InvoicesList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                    {
                                        InvoicesList.Other2.Add(childNode.InnerText);
                                    }

                                    dataextFlag = false;
                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + InvoicesList.TxnLineID[count]);
                                        }
                                        dataextFlag = true;
                                        i++;
                                    }
                                }
                                if (dataextFlag == false)
                                {
                                    //if (InvoicesList.DataExtName[0] != null)
                                    //{
                                    //    InvoicesList.DataExtValue[i] = "";
                                    i++;
                                    //}

                                }
                                count++;
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBInvoicesList.InvoiceLineGroupRet.ToString()))
                            {
                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InvoicesList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBInvoicesList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                //itemList.ItemGroupFullName.Add (childNodes.InnerText);
                                                //616
                                                InvoicesList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                    {
                                        //itemList.GroupDesc.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            //itemList.GroupQuantity.Add (Convert.ToDecimal(childNode.InnerText));
                                            //616
                                            InvoicesList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { InvoicesList.GroupQuantity.Add(null); }
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                    {
                                        //itemList.UOM.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBInvoicesList.IsPrintItemsInGroup.ToString()))
                                    {
                                        //itemList.IsPrintItemsInGroup.Add (childNode.InnerText);
                                        //616
                                        InvoicesList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            InvoicesList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                InvoicesList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                        }
                                        dataextFlag = true;

                                        i++;
                                    }
                                    //Checking  sub InvoiceLineRet
                                    if (childNode.Name.Equals(QBInvoicesList.InvoiceLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref InvoicesList);
                                        
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    InvoicesList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineItemFullName.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Desc.ToString()))
                                            {
                                                //itemList.GroupLineDesc.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineQuantity.Add ( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                {
                                                    InvoicesList.GroupLineQuantity.Add(null);
                                                }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.UnitOfMeasure.ToString()))
                                            {
                                                //itemList.GroupLineUOM.Add (subChildNode.InnerText);
                                                //616
                                                InvoicesList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBInvoicesList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineOverrideUOM.Add (subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineRate.Add( Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineRate.Add(null); }
                                            }
                                            //Checking ClassRef value.
                                            if (subChildNode.Name.Equals(QBInvoicesList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.GroupLineClassRefFullName.Add( subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.GroupLineClassRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBInvoicesList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    //itemList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                    //616
                                                    InvoicesList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { InvoicesList.GroupLineAmount.Add(null); }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBInvoicesList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        //itemList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                        //616
                                                        InvoicesList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    //itemList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                    //616
                                                    InvoicesList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBInvoicesList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        //itemList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                        //616
                                                        InvoicesList.SalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other1.ToString()))
                                            {
                                                //itemList.LineOther1.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBInvoicesList.Other2.ToString()))
                                            {
                                                //itemList.LineOther2.Add(subChildNode.InnerText);
                                                //616
                                                InvoicesList.LineOther2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    InvoicesList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        InvoicesList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    dataextFlag = true;
                                                }

                                                i++;
                                            }
                                        }
                                        count2++;
                                    }
                                    //
                                    if (dataextFlag == false)
                                    {
                                        //if (InvoicesList.DataExtName[0] != null)
                                        //{

                                        //    InvoicesList.DataExtValue.Add(" ");
                                        //    i++;
                                        //}
                                        i++;
                                    }
                                }
                                count1++;
                            }

                            if (node.Name.Equals(QBInvoicesList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    InvoicesList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        InvoicesList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        //Adding Invoice list.
                        this.Add(InvoicesList);

                    }
                    else
                    { }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInvoices.RemoveAll();
                #endregion
            }
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref InvoicesList invoicesList)
        {
            if (childNode.SelectSingleNode("./" + QBInvoicesList.TxnLineID.ToString()) == null)
            {
                invoicesList.GroupLineTxnLineID.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
            {
                invoicesList.GroupLineItemFullName.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
            {
                invoicesList.GroupLineDesc.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
            {
                invoicesList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
            {
                invoicesList.GroupLineUOM.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
            {
                invoicesList.GroupLineOverrideUOM.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
            {
                invoicesList.GroupLineRate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
            {
                invoicesList.GroupLineClassRefFullName.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
            {
                invoicesList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.ServiceDate.ToString()) == null)
            {
                invoicesList.GroupLineServiceDate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
            {
                invoicesList.SalesTaxCodeFullName.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Other1.ToString()) == null)
            {
                invoicesList.LineOther1.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.Other2.ToString()) == null)
            {
                invoicesList.LineOther2.Add("");
            }
            if (childNode.SelectSingleNode("./" + QBInvoicesList.DataExtRet.ToString()) == null)
            {
                invoicesList.DataExtName.Add("");
                invoicesList.DataExtValue.Add("");
            }
        }

        private void checkNullEntries(XmlNode node, ref InvoicesList invoicesList)
        {

            if (node.SelectSingleNode("./" + QBInvoicesList.TxnLineID.ToString()) == null)
            {
                invoicesList.TxnLineID.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.ItemRef.ToString()) == null)
            {
                invoicesList.ItemFullName.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Desc.ToString()) == null)
            {
                invoicesList.Desc.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Quantity.ToString()) == null)
            {
                invoicesList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.UnitOfMeasure.ToString()) == null)
            {
                invoicesList.UOM.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.OverrideUOMSetRef.ToString()) == null)
            {
                invoicesList.OverrideUOMFullName.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Rate.ToString()) == null)
            {
                invoicesList.Rate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.ClassRef.ToString()) == null)
            {
                invoicesList.ClassRefFullName.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Amount.ToString()) == null)
            {
                invoicesList.Amount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.InventorySiteRef.ToString()) == null)
            {
                invoicesList.InventorySiteRefFullName.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.SerialNumber.ToString()) == null)
            {
                invoicesList.SerialNumber.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.LotNumber.ToString()) == null)
            {
                invoicesList.LotNumber.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.ServiceDate.ToString()) == null)
            {
                invoicesList.ServiceDate.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.SalesTaxCodeRef.ToString()) == null)
            {
                invoicesList.SalesTaxCodeFullName.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Other1.ToString()) == null)
            {
                invoicesList.Other1.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.Other2.ToString()) == null)
            {
                invoicesList.Other2.Add("");
            }
            if (node.SelectSingleNode("./" + QBInvoicesList.DataExtRet.ToString()) == null)
            {
                invoicesList.DataExtName.Add("");
                invoicesList.DataExtValue.Add("");
            }
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
    }
}
