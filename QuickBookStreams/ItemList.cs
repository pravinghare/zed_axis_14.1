// ===============================================================================
// 
// ItemList.cs
//
// This file contains the implementations of the QuickBooks Item request methods and
// Properties.
//
// Developed By : Sandeep Patil.
// Date : 27/02/2009
// Modified By :K.Gouraw
// Date : 27/06/2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class provides properties and methods of QuickBooks Item List.
    /// </summary>
    public class QBItemList : BaseItemList
    {
         #region Public Properties

        /// <summary>
        /// Get and Set Name 
        /// </summary>
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }

        }
        /// <summary>
        /// Get and Set BarCodeValue
        /// </summary>
        public string BarCodeValue
        {
            get { return m_BarCodeValue; }
            set { m_BarCodeValue = value; }
        }

        /// <summary>
        /// Get and Set ListId
        /// </summary>
        public string ListId
        {
            get { return m_ListId; }
            set { m_ListId = value; }

        }

        /// <summary>
        /// Get and Set TimeCreated
        /// </summary>
        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }

        }

        /// <summary>
        /// Get and Set TimeModified
        /// </summary>
        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }

        }

        

        /// <summary>
        /// Get and Set Fullname of Quickbooks.
        /// </summary>
        public string FullName
        {
            get
            {
                return m_itemnumber;
            }
            set
            {
                //if (value.Length > 25)
                //{
                //    m_itemnumber = value.Remove(25, (value.Length - 25)).Trim();
                //}
                //else
                //{
                    m_itemnumber = value.Trim();
                //}
            }
        }

        /// <summary>
        /// Get and Set Sales Desctiption of Quickbooks.
        /// </summary>
        public string SalesDesc
        {
            get
            { return m_itemname; }
            set
            {
                //if (value.Length > 60)
                //    m_itemname = value.Remove(60, (value.Length - 60));
                //else
                    m_itemname = value;
            }
        }

        /// <summary>
        /// Get and Set Purchase Description of QuickBooks.
        /// </summary>
        public string PurchaseDesc
        {
            get
            { return m_supplieritemnumber; }
            set
            { 
                //if (value.Length > 60)
                //    m_supplieritemnumber = value.Remove(60, (value.Length - 60));
                //else
                    m_supplieritemnumber = value;
            }
        }
        /// <summary>
        /// Get and Set Sales Price of QuickBooks.
        /// </summary>
        public string SalesPrice
        {
            get
            {
                return m_basesellingprice;
            }
            set
            {
                m_basesellingprice = value;
            }
        }

        /// <summary>
        /// Get and Set Unit of Measure of Quickbooks.
        /// </summary>
        public string UnitMeasureFullName
        {
            get
            { return m_unitmeasure; }
            set
            {
                if (value.Length > 20)
                    m_unitmeasure = value.Remove(20, (value.Length - 20));
                else
                    m_unitmeasure = value;
            }
        }

   
        /// <summary>
        /// Get and Set ParentRefFullName of QuickBooks.
        /// </summary>
        public string ParentRefFullName
        {
            get
            {
                return m_ParentRefFullName;
            }
            set
            {
                m_ParentRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set SublevelFullNameof QuickBooks.
        /// </summary>
        public string SublevelFullName
        {
            get
            {
                return m_SublevelFullName;
            }
            set
            {
                m_SublevelFullName = value;
            }
        }
        /// <summary>
        /// Get and Set ManufacturerPartNumber of QuickBooks.
        /// </summary>
        public string ManufacturerPartNumber
        {
            get
            {
                return m_ManufacturerPartNumber;
            }
            set
            {
                m_ManufacturerPartNumber = value;
            }
        }
        /// <summary>
        /// Get and Set UnitOfMeasureSetRefFullName of QuickBooks.
        /// </summary>
        public string UnitOfMeasureSetRefFullName
        {
            get
            {
                return m_UnitOfMeasureSetRefFullName;
            }
            set
            {
                if (value.Length > 20)
                    m_UnitOfMeasureSetRefFullName = value.Remove(20, (value.Length - 20));
                else
                m_UnitOfMeasureSetRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set SalesTaxCodeRefFullName of QuickBooks.
        /// </summary>
        public string SalesTaxCodeRefFullName
        {
            get
            {
                return m_SalesTaxCodeRefFullName;
            }
            set
            {
                m_SalesTaxCodeRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set IncomeAccountRefFullName of QuickBooks.
        /// </summary>
        public string IncomeAccountRefFullName
        {
            get
            {
                return m_IncomeAccountRefFullName;
            }
            set
            {
                m_IncomeAccountRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set PurchaseCost of QuickBooks.
        /// </summary>
        public string PurchaseCost
        {
            get
            {
                return m_PurchaseCost;
            }
            set
            {
                m_PurchaseCost = value;
            }
        }
        /// <summary>
        /// Get and Set COGSAccountRefFullName of QuickBooks.
        /// </summary>
        public string COGSAccountRefFullName
        {
            get
            {
                return m_COGSAccountRefFullName;
            }
            set
            {
                m_COGSAccountRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set PrefVendorRefFullName of QuickBooks.
        /// </summary>
        public string PrefVendorRefFullName
        {
            get
            {
                return m_PrefVendorRefFullName;
            }
            set
            {
                m_PrefVendorRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set AssetAccountRefFullName of QuickBooks.
        /// </summary>
        public string AssetAccountRefFullName
        {
            get
            {
                return m_AssetAccountRefFullName;
            }
            set
            {
                m_AssetAccountRefFullName = value;
            }
        }
        /// <summary>
        /// Get and Set ReorderPoint of QuickBooks.
        /// </summary>
        public string ReorderPoint
        {
            get
            {
                return m_ReorderPoint;
            }
            set
            {
                m_ReorderPoint = value;
            }
        }

        /// <summary>
        /// Get and Set QuantityOnOrder of QuickBooks.
        /// </summary>
        public string QuantityOnOrder
        {
            get
            {
                return m_QuantityOnOrder;
            }
            set
            {
                m_QuantityOnOrder = value;
            }
        }
        /// <summary>
        /// Get and Set QuantityOnHand of QuickBooks.
        /// </summary>
        public string QuantityOnHand
        {
            get
            {
                return m_QuantityOnHand;
            }
            set
            {
                m_QuantityOnHand = value;
            }
        }
        /// <summary>
        /// Get and Set AverageCost of QuickBooks.
        /// </summary>
        public string AverageCost
        {
            get
            {
                return m_AverageCost;
            }
            set
            {
                m_AverageCost = value;
            }
        }
        /// <summary>
        /// Get and Set QuantityOnSalesOrder of QuickBooks.
        /// </summary>
        public string QuantityOnSalesOrder
        {
            get
            {
                return m_QuantityOnSalesOrder;
            }
            set
            {
                m_QuantityOnSalesOrder = value;
            }
        }
        public string IsActive
        {
            get
            {
                return m_isactive;
            }
            set
            {
                m_isactive = value;
            }
        }

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];
       
        #endregion

    }

    

    /// <summary>
    /// Collection class used for getting Item List from QuickBooks.
    /// </summary>
    public class QBItemListCollection : Collection<QBItemList>
    {
       
        List<string> serviceItemList = new List<string>();
        List<string> inventoryItemList = new List<string>();
        List<string> nonInventoryItemList = new List<string>();
        List<string> otherItemList = new List<string>();


        string XmlFileData = string.Empty;

        #region To get item list by item type.

        /// <summary>
        /// This method used to get Item List by Item type like Service, Inventory, Non-Inventory etc.
        /// </summary>
        /// <param name="bkWorker"></param>
        /// <param name="ItemNodeList"></param>
        /// <param name="itemList"></param>
        /// <returns></returns>

        private List<string> GetItemList(BackgroundWorker bkWorker,XmlNodeList ItemNodeList, List<string> itemList)
        {
            foreach (XmlNode itemNodes in ItemNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBItemList item = new QBItemList();
                    foreach (XmlNode node in itemNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                        {
                            if (!itemList.Contains(node.InnerText))
                            {
                                
                                itemList.Add(node.InnerText);
                            }
                        }
                    }
                }
            }
            return itemList;
        }

        private List<string> GetServiceItemList(BackgroundWorker bkWorker, XmlNodeList ItemNodeList, List<string> itemList)
        {
            

            foreach (XmlNode itemNodes in ItemNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBItemList item = new QBItemList();
                    foreach (XmlNode node in itemNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                        {
                            if (!itemList.Contains(node.InnerText))
                            {
                                string name = node.InnerText + "     :     Service";
                                itemList.Add(name);
                               
                            }
                        }
                    }
                }
            }
            return itemList;
        }

        private List<string> GetInventoryItemList(BackgroundWorker bkWorker, XmlNodeList ItemNodeList, List<string> itemList)
        {
            foreach (XmlNode itemNodes in ItemNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBItemList item = new QBItemList();
                    foreach (XmlNode node in itemNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                        {
                            if (!itemList.Contains(node.InnerText))
                            {
                                string name = node.InnerText + "     :     Inventory Part";
                                itemList.Add(name);

                            }
                        }
                    }
                }
            }
            return itemList;
        }

        private List<string> GetNonInventoryItemList(BackgroundWorker bkWorker, XmlNodeList ItemNodeList, List<string> itemList)
        {
             foreach (XmlNode itemNodes in ItemNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBItemList item = new QBItemList();
                    foreach (XmlNode node in itemNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                        {
                            if (!itemList.Contains(node.InnerText))
                            {
                                string name = node.InnerText + "     :     Non-inventory Part";
                                itemList.Add(name);

                            }
                        }
                    }
                }
            }
            return itemList;
        }

        private List<string> GetOtherItemList(BackgroundWorker bkWorker, XmlNodeList ItemNodeList, List<string> itemList)
        {


            foreach (XmlNode itemNodes in ItemNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBItemList item = new QBItemList();
                    foreach (XmlNode node in itemNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                        {
                            if (!itemList.Contains(node.InnerText))
                            {
                                string name = node.InnerText + "     :     Other Charge";
                                itemList.Add(name);

                            }
                        }
                    }
                }
            }
            return itemList;
        }

        #endregion 

        /// <summary>
        /// This method returns all items from quickbooks
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllItemsList(string QBFileName)
        {
            
       
            string accountXmlList = QBCommonUtilities.GetListFromQuickBook("AccountQueryRq", QBFileName);


            List<string> itemList = new List<string>();
            List<string> itemList1 = new List<string>();
            List<string> itemList2 = new List<string>();
            List<string> itemList3 = new List<string>();
            List<string> itemList4 = new List<string>();


            XmlDocument outputXMLDoc = new XmlDocument();
            XmlDocument outputXMLDoc1 = new XmlDocument();


            //  comm for 324     outputXMLDoc.LoadXml(itemXmlList);
            outputXMLDoc1.LoadXml(accountXmlList);

            //   comm for 324     XmlFileData = itemXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Get Service item list
            XmlNodeList ItemServiceNodeList = outputXMLDoc.GetElementsByTagName(QBXmlItemList.ItemServiceRet.ToString());
            GetItemList(bkWorker, ItemServiceNodeList, itemList);

             serviceItemList= GetServiceItemList(bkWorker, ItemServiceNodeList, itemList1);

            //Get Inventory Item list
            XmlNodeList ItemInventoryNodeList = outputXMLDoc.GetElementsByTagName(QBXmlItemList.ItemInventoryRet.ToString());
            GetItemList(bkWorker, ItemInventoryNodeList, itemList);

            inventoryItemList = GetInventoryItemList(bkWorker, ItemInventoryNodeList, itemList2);

            //Get Non INventory Item List
            XmlNodeList ItemNonInventoryNodeList = outputXMLDoc.GetElementsByTagName(QBXmlItemList.ItemNonInventoryRet.ToString());
            GetItemList(bkWorker, ItemNonInventoryNodeList, itemList);

            nonInventoryItemList = GetNonInventoryItemList(bkWorker, ItemNonInventoryNodeList, itemList3);

            // Axis 10.0 
            // Get Other Charge Item List
            XmlNodeList ItemOtherChargeList = outputXMLDoc.GetElementsByTagName(QBXmlItemList.ItemOtherChargeRet.ToString());
            GetItemList(bkWorker, ItemOtherChargeList, itemList);

            otherItemList = GetOtherItemList(bkWorker, ItemOtherChargeList, itemList4);

            List<string> test = new List<string>();
            test.AddRange(serviceItemList);
            test.AddRange(inventoryItemList);
            test.AddRange(nonInventoryItemList);
            test.AddRange(otherItemList);
            test.Insert(0, "<None>");
            CommonUtilities.GetInstance().CombinedItemList = test;
            string[,] testarray =new string[100,2];
        
            CommonUtilities.GetInstance().CombinedItemListArray = testarray;
            return itemList;

        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemList> PopulateItemList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Item List from QuickBooks.
            int i = 0;
            //Getting item list from QuickBooks.
            string itemList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                itemList = QBCommonUtilities.GetListFromQuickBookByDate("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                itemList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemList item;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItem = new XmlDocument();

            if (itemList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfItem.LoadXml(itemList);
                XmlFileData = itemList;

                #region Getting Item Inventory Values from XML

                //Getting Item Inventory list from XML.
                XmlNodeList ItemInventoryList = outputXMLDocOfItem.GetElementsByTagName(QBXmlItemList.ItemInventoryRet.ToString());

                foreach (XmlNode ItemInventoryNodes in ItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        item = new QBItemList();
                        //Getting node list of Item Inventory.
                        foreach (XmlNode node in ItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemList.Name.ToString()))
                                item.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.BarCodeValue.ToString()))
                                item.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.ListID.ToString()))
                                item.ListId = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeCreated.ToString()))
                                item.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeModified.ToString()))
                                item.TimeModified = node.InnerText;

                             if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                                item.FullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesDesc.ToString()))
                                item.SalesDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesPrice.ToString()))
                                item.SalesPrice = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseDesc.ToString()))
                                item.PurchaseDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.Sublevel.ToString()))
                                item.SublevelFullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseCost.ToString()))
                                item.PurchaseCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ReorderPoint.ToString()))
                                item.ReorderPoint = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnHand.ToString()))
                                item.QuantityOnHand = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.AverageCost.ToString()))
                                item.AverageCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnOrder.ToString()))
                                item.QuantityOnOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnSalesOrder.ToString()))
                                item.QuantityOnSalesOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.IsActive.ToString()))
                                item.IsActive = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ManufacturerPartNumber.ToString()))
                                item.ManufacturerPartNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ParentRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.ParentRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking UnitOfMeasureSetRefFullName node value into xml.

                            //Checking SalesTaxCodeRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking IncomeAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking COGSAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.COGSAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking PrefVendorRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.PrefVendorRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking AssetAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.AssetAccountRefFullName = childNode.InnerText;
                                }

                            }
                          
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    item.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        item.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(item);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfItem.RemoveAll();
            }
            //Returning object.
            return this;

        }


        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBItemList> PopulateItemList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Item List from QuickBooks.
            int i = 0;
            //Getting item list from QuickBooks.
            string itemList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                itemList = QBCommonUtilities.GetListFromQuickBook("ItemInventoryQueryRq", QBFileName);
            }
            else
            {
                itemList = QBCommonUtilities.GetListFromQuickBookForInactive("ItemInventoryQueryRq", QBFileName);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemList item;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItem = new XmlDocument();

            if (itemList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfItem.LoadXml(itemList);
                XmlFileData = itemList;

                #region Getting Item Inventory Values from XML

                //Getting Item Inventory list from XML.
                XmlNodeList ItemInventoryList = outputXMLDocOfItem.GetElementsByTagName(QBXmlItemList.ItemInventoryRet.ToString());

                foreach (XmlNode ItemInventoryNodes in ItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        item = new QBItemList();
                        //Getting node list of Item Inventory.
                        foreach (XmlNode node in ItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemList.Name.ToString()))
                                item.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.BarCodeValue.ToString()))
                                item.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.ListID.ToString()))
                                item.ListId = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeCreated.ToString()))
                                item.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeModified.ToString()))
                                item.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                                item.FullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesDesc.ToString()))
                                item.SalesDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesPrice.ToString()))
                                item.SalesPrice = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseDesc.ToString()))
                                item.PurchaseDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.Sublevel.ToString()))
                                item.SublevelFullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseCost.ToString()))
                                item.PurchaseCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ReorderPoint.ToString()))
                                item.ReorderPoint = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnHand.ToString()))
                                item.QuantityOnHand = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.AverageCost.ToString()))
                                item.AverageCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnOrder.ToString()))
                                item.QuantityOnOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnSalesOrder.ToString()))
                                item.QuantityOnSalesOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.IsActive.ToString()))
                                item.IsActive = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ManufacturerPartNumber.ToString()))
                                item.ManufacturerPartNumber = node.InnerText;

                           
                            if (node.Name.Equals(QBXmlItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ParentRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.ParentRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking UnitOfMeasureSetRefFullName node value into xml.

                            //Checking SalesTaxCodeRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking IncomeAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking COGSAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.COGSAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking PrefVendorRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.PrefVendorRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking AssetAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.AssetAccountRefFullName = childNode.InnerText;
                                }

                            }
                         
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    item.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        item.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }
                                i++;
                            }
                        }
                        this.Add(item);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfItem.RemoveAll();
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for adding various item list into QuickBooks item class.
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemList> PopulateItemList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)

        {
            #region Getting Item List from QuickBooks.
            int i = 0;
            //Getting item list from QuickBooks.
            string itemList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                itemList = QBCommonUtilities.GetListFromQuickBookByDate("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                itemList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemInventoryQueryRq", QBFileName, FromDate, ToDate);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemList item;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItem = new XmlDocument();

            if (itemList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfItem.LoadXml(itemList);
                XmlFileData = itemList;

                #region Getting Item Inventory Values from XML

                //Getting Item Inventory list from XML.
                XmlNodeList ItemInventoryList = outputXMLDocOfItem.GetElementsByTagName(QBXmlItemList.ItemInventoryRet.ToString());

                foreach (XmlNode ItemInventoryNodes in ItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        item = new QBItemList();
                        //Getting node list of Item Inventory.
                        foreach (XmlNode node in ItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemList.Name.ToString()))
                                item.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.BarCodeValue.ToString()))
                                item.BarCodeValue = node.InnerText;


                            if (node.Name.Equals(QBXmlItemList.ListID.ToString()))
                                item.ListId = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeCreated.ToString()))
                                item.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.TimeModified.ToString()))
                                item.TimeModified = node.InnerText;





                            if (node.Name.Equals(QBXmlItemList.FullName.ToString()))
                                item.FullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesDesc.ToString()))
                                item.SalesDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.SalesPrice.ToString()))
                                item.SalesPrice = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseDesc.ToString()))
                                item.PurchaseDesc = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.Sublevel.ToString()))
                                item.SublevelFullName = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.PurchaseCost.ToString()))
                                item.PurchaseCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ReorderPoint.ToString()))
                                item.ReorderPoint = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnHand.ToString()))
                                item.QuantityOnHand = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.AverageCost.ToString()))
                                item.AverageCost = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnOrder.ToString()))
                                item.QuantityOnOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.QuantityOnSalesOrder.ToString()))
                                item.QuantityOnSalesOrder = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.IsActive.ToString()))
                                item.IsActive = node.InnerText;
                            if (node.Name.Equals(QBXmlItemList.ManufacturerPartNumber.ToString()))
                                item.ManufacturerPartNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ParentRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.ParentRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking UnitOfMeasureSetRefFullName node value into xml.

                            //Checking SalesTaxCodeRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking IncomeAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking COGSAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.COGSAccountRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking PrefVendorRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.PrefVendorRefFullName = childNode.InnerText;
                                }

                            }
                            //Checking AssetAccountRefFullName node value into xml.
                            if (node.Name.Equals(QBXmlItemList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemList.FullName.ToString()))
                                        item.AssetAccountRefFullName = childNode.InnerText;
                                }

                            }
                           
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    item.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        item.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }

                        }
                        this.Add(item);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
               // outputXMLDocOfItem.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        
    }
}
