﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using System.Collections;

namespace Streams
{
    /// <summary>
    /// This class provide properties nad methods for Time Entries.
    /// </summary>
    public class TimeEntryList :BaseTimeEntry
    {
        #region Public Properties
        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string EntityFullName
        {
            get { return m_EntityFullName; }
            set { m_EntityFullName = value; }
        }

        public string CustomerFullName
        {
            get { return m_CustomerFullName; }
            set { m_CustomerFullName = value; }
        }

        public string ItemServiceFullName
        {
            get { return m_ItemServiceFullName; }
            set { m_ItemServiceFullName = value; }
        }

        public string Duration
        {
            get { return m_Duration; }
            set { m_Duration = value; }
        }

        public string ClassFullName
        {
            get { return m_ClassFullName; }
            set { m_ClassFullName = value; }
        }

        public string PayrollItemWageFullName
        {
            get { return m_PayrollItemWageFullName; }
            set { m_PayrollItemWageFullName = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }

        //EmployeeQuery.BillingRateName.
        public decimal? ServiceItemPrice
        {
            get { return m_ServiceItemPrice; }
            set { m_ServiceItemPrice = value; }
        }
        
        #endregion
    }

    /// <summary>
    /// This collection class is used for getting Time Entry List from QuickBooks.
    /// </summary>
    public class QBTimeEntryListCollection : Collection<TimeEntryList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// This method is used to get TimeEntryList From QuickBook for filters
        /// EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> populateTimeEntryListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("TimeTrackingQueryRq", QBFileName,entityFilter);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML

                Hashtable itemServiceFullName = new Hashtable();
                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get TimeEntryList From QuickBook for filters
        /// TxnDateRangeFilters,Entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> populateTimeEntryList(string QBFileName, DateTime fromDate, DateTime toDate, List<string> entityFilter)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate,entityFilter);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID added 
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> populateTimeEntryList(string QBFileName)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBook("TimeTrackingQueryRq", QBFileName);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this; 
        }


        /// <summary>
        /// This method is used to get TimeEntryList From QuickBook for filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> populateTimeEntryList(string QBFileName,DateTime fromDate, DateTime toDate)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.GetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID  
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> ModifiedpopulateTimeEntryList(string QBFileName, DateTime fromDate, string toDate, List<string> entityFilter)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate, entityFilter);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID 
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get TimeEntryList From QuickBook for filters
        /// TxnDateRangeFilters,entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> ModifiedpopulateTimeEntryList(string QBFileName, DateTime fromDate, DateTime toDate, List<string> entityFilter)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate,entityFilter);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID 
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        ///Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> ModifiedpopulateTimeEntryList(string QBFileName, DateTime fromDate, string toDate)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get TimeEntryList From QuickBook for filters
        /// TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Collection<TimeEntryList> ModifiedpopulateTimeEntryList(string QBFileName, DateTime fromDate, DateTime toDate)
        {
            #region Getting Time Entry List from QuickBooks.

            //Getting item list from QuickBooks.

            string TimeEntryXmlList = string.Empty;
            TimeEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TimeTrackingQueryRq", QBFileName, fromDate, toDate);

            TimeEntryList TimeEntryList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfTimeEntry = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TimeEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfTimeEntry.LoadXml(TimeEntryXmlList);
                XmlFileData = TimeEntryXmlList;

                #region Getting TimeEntryList Values from XML
                Hashtable itemServiceFullName = new Hashtable();

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TimeEntriesNodeList = outputXMLDocOfTimeEntry.GetElementsByTagName(QBTimeEntry.TimeTrackingRet.ToString());

                foreach (XmlNode TimeEntriesNodes in TimeEntriesNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        TimeEntryList = new TimeEntryList();
                        foreach (XmlNode node in TimeEntriesNodes.ChildNodes)
                        {
                            // Checking TxnID 
                            if (node.Name.Equals(QBTimeEntry.TxnID.ToString()))
                            {
                                TimeEntryList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBTimeEntry.TxnNumber.ToString()))
                            {
                                TimeEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBTimeEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        TimeEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    TimeEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking EntityFullName
                            if (node.Name.Equals(QBTimeEntry.EntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.EntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking CustomerFullName.
                            if (node.Name.Equals(QBTimeEntry.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.CustomerFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSerivceFullName
                            if (node.Name.Equals(QBTimeEntry.ItemServiceRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ItemServiceFullName = childNode.InnerText;
                                }
                            }
                            //Checking Duration
                            if (node.Name.Equals(QBTimeEntry.Duration.ToString()))
                            {
                                try
                                {
                                    string[] type = { "PT", "H", "M" };
                                    string[] duration = node.InnerText.Split(type, StringSplitOptions.None);
                                    //Convert to decimal format
                                    decimal time = ((Convert.ToDecimal(duration[1]) * 60) + Convert.ToDecimal(duration[2])) / 60;
                                    string sTime = Convert.ToString(time);
                                    if (!sTime.Contains("."))
                                    {
                                        TimeEntryList.Duration = sTime + ".0";
                                    }
                                    else
                                    {
                                        TimeEntryList.Duration = sTime;
                                    }

                                }
                                catch
                                {
                                    TimeEntryList.Duration = node.InnerText;
                                }
                            }

                            //Checking ClassRef List.
                            if (node.Name.Equals(QBTimeEntry.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.ClassFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayrollItemWageFullName
                            if (node.Name.Equals(QBTimeEntry.PayrollItemWageRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBTimeEntry.FullName.ToString()))
                                        TimeEntryList.PayrollItemWageFullName = childNode.InnerText;
                                }
                            }
                            //Checking Notes
                            if (node.Name.Equals(QBTimeEntry.Notes.ToString()))
                            {
                                TimeEntryList.Notes = node.InnerText;
                            }

                            //Checking Billable Status
                            if (node.Name.Equals(QBTimeEntry.BillableStatus.ToString()))
                            {
                                TimeEntryList.BillableStatus = node.InnerText;
                            }

                            //Get ItemServicePrice from query to ItemServiceQuery.
                            if (!string.IsNullOrEmpty(TimeEntryList.ItemServiceFullName))
                            {
                                //string serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                string serviceItemPrice = string.Empty;
                                if (!itemServiceFullName.ContainsKey(TimeEntryList.ItemServiceFullName))
                                {
                                    serviceItemPrice = QBCommonUtilities.GetPriceFromItemSerivceQuery(QBFileName, TimeEntryList.ItemServiceFullName);
                                    itemServiceFullName.Add(TimeEntryList.ItemServiceFullName, serviceItemPrice);
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                                else
                                {
                                    serviceItemPrice = itemServiceFullName[TimeEntryList.ItemServiceFullName].ToString();
                                    TimeEntryList.ServiceItemPrice = Convert.ToDecimal(serviceItemPrice);
                                }
                            }
                        }
                        //Adding TimeEntry list.
                        this.Add(TimeEntryList);
                    }
                    else
                    {
                        return null;
                    }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfTimeEntry.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        
    }
}
