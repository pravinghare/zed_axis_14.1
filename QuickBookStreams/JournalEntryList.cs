﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This class provide properties and methods for JournalEntries.
    /// </summary>
    public class JournalEntryList: BaseJournalEntry
    {
        #region Public Properties

        public string TxnID
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string IsAdjustment
        {
            get { return m_IsAdjustment; }
            set { m_IsAdjustment = value; }
        }

        public string IsHomeCurrencyAdjustment
        {
            get { return m_IsHomeCurrencyAdjustment; }
            set { m_IsHomeCurrencyAdjustment = value; }
        }

        public string IsAmountEnteredInHomeCurrency
        {
            get { return m_IsAmountEnteredinHomeCurrency; }
            set { m_IsAmountEnteredinHomeCurrency = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value;}
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        //JournalDebitLine
        //D = Debit
        public List<string> DLineTxnLineId
        {
            get { return m_DLineTxnLineId; }
            set { m_DLineTxnLineId = value; }
        }

        public List<string> DLineAccountFullName
        {
            get { return m_DLineAccountFullName; }
            set { m_DLineAccountFullName = value; }
        }

        public List<decimal?> DLineAmount
        {
            get { return m_DLineAmount; }
            set { m_DLineAmount= value;}
        }

        public List<string> DLineMemo
        {
            get { return m_DLineMemo; }
            set { m_DLineMemo = value; }
        }

        public List<string> DLineEntityFullName
        {
            get { return m_DLineEntityFullName; }
            set { m_DLineEntityFullName = value; }
        }

        public List<string> DLineClassFullName
        {
            get { return m_DLineClassFullName; }
            set { m_DLineClassFullName = value; }
        }

        public List<string> DLineBillableStatus 
        {
            get { return m_DLineBillableStatus; }
            set { m_DLineBillableStatus = value; }
        }

        public List<string> DLineItemSalesTaxRefFullName
        {
            get { return m_DLineItemSalesTaxRefFullName; }
            set { m_DLineItemSalesTaxRefFullName = value; }
        }
        //JournalCreditLine
        //C- Credit
        public List<string> CLineTxnLineId
        {
            get { return m_CLineTxnlineId; }
            set { m_CLineTxnlineId = value; }
        }

        public List<string> CLineAccountFullName
        {
            get { return m_ClineAccountFullName; }
            set { m_ClineAccountFullName = value; }
        }

        public List<decimal?> CLineAmount
        {
            get { return m_CLineAmount; }
            set { m_CLineAmount = value; }
        }

        public List<string> CLineMemo
        {
            get { return m_CLineMemo; }
            set { m_CLineMemo = value; }
        }

        public List<string> CLineEntityFullName
        {
            get { return m_CLineEntityFullName; }
            set { m_CLineEntityFullName = value; }
        }

        public List<string> CLineClassFullName
        {
            get { return m_CLineClassFullName; }
            set { m_CLineClassFullName = value; }
        }

        public List<string> CLineBillableStatus
        {
            get { return m_CLineBillableStatus; }
            set { m_CLineBillableStatus = value; }
        }

        public List<string> CLineItemSalesTaxRefFullName
        {
            get { return m_CLineItemSalesTaxRefFullName; }
            set { m_CLineItemSalesTaxRefFullName = value; }
        }
        #endregion
    }


    /// <summary>
    /// This collection class is used for gettting Journal Entry List from Quickbook.
    /// </summary>
    public class QBJournalEntryListCollection : Collection<JournalEntryList>
    {
        string XmlFileData = string.Empty;

        public void GetJournalEntryValuesFromXML(string JournalEntryXmlList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfJournalEntry = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (JournalEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfJournalEntry.LoadXml(JournalEntryXmlList);
                XmlFileData = JournalEntryXmlList;
                JournalEntryList journalEntryList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList JournalEntryNodeList = outputXMLDocOfJournalEntry.GetElementsByTagName(QBJournalEntry.JournalEntryRet.ToString());

                foreach (XmlNode JournalEntryNodes in JournalEntryNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        journalEntryList = new JournalEntryList();
                        
                        foreach (XmlNode node in JournalEntryNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBJournalEntry.TxnID.ToString()))
                            {
                                journalEntryList.TxnID = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(QBJournalEntry.TxnNumber.ToString()))
                            {
                                journalEntryList.TxnNumber = node.InnerText;
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBJournalEntry.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        journalEntryList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        journalEntryList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    journalEntryList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber.
                            if (node.Name.Equals(QBJournalEntry.RefNumber.ToString()))
                            {
                                journalEntryList.RefNumber = node.InnerText;
                            }

                            //Checking IsAdjustment
                            if (node.Name.Equals(QBJournalEntry.IsAdjustment.ToString()))
                            {
                                journalEntryList.IsAdjustment = node.InnerText;
                            }

                            //Checking IsHomeCurrencyAdjsutment.
                            if (node.Name.Equals(QBJournalEntry.IsHomeCurrencyAdjustment.ToString()))
                            {
                                journalEntryList.IsHomeCurrencyAdjustment = node.InnerText;
                            }

                            //Checking IsAmountEnteredInHomeCurrency.
                            if (node.Name.Equals(QBJournalEntry.IsAmountEnteredInHomeCurrecny.ToString()))
                            {
                                journalEntryList.IsAmountEnteredInHomeCurrency = node.InnerText;
                            }

                            //Checking CurrencyRefFullName.
                            if (node.Name.Equals(QBJournalEntry.IsAmountEnteredInHomeCurrecny.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBJournalEntry.FullName.ToString()))
                                        journalEntryList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate.
                            if (node.Name.Equals(QBJournalEntry.ExchangeRate.ToString()))
                            {
                                journalEntryList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking JOurnalDebitLine Line values.
                            if (node.Name.Equals(QBJournalEntry.JournalDebitLine.ToString()))
                            {
                                checkDebitNullEntries(node, ref journalEntryList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBJournalEntry.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            journalEntryList.DLineTxnLineId.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            journalEntryList.DLineTxnLineId.Add(childNode.InnerText);
                                    }

                                    //Checking Account ref value.
                                    if (childNode.Name.Equals(QBJournalEntry.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBJournalEntry.FullName.ToString()))
                                            {
                                                journalEntryList.DLineAccountFullName.Add(childsNode.InnerText);

                                            }
                                        }
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBJournalEntry.Amount.ToString()))
                                    {
                                        try
                                        {
                                            journalEntryList.DLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }
                                    }

                                    //Checking Memo
                                    if (childNode.Name.Equals(QBJournalEntry.Memo.ToString()))
                                    {
                                        journalEntryList.DLineMemo.Add(childNode.InnerText);
                                    }
                                    //Checking Entity FullName
                                    if (childNode.Name.Equals(QBJournalEntry.EntityRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.DLineEntityFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    //Checking Class FullName
                                    if (childNode.Name.Equals(QBJournalEntry.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.DLineClassFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBJournalEntry.ItemSalesTaxRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.DLineItemSalesTaxRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Billable Status
                                    if (childNode.Name.Equals(QBJournalEntry.BillableStatus.ToString()))
                                    {
                                        journalEntryList.DLineBillableStatus.Add(childNode.InnerText);
                                    }

                                }//End of foreach
                                count++;
                            }

                            //Checking JournalCreditLine
                            if (node.Name.Equals(QBJournalEntry.JournalCreditLine.ToString()))
                            {
                                checkCreditNullEntries(node, ref journalEntryList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBJournalEntry.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            journalEntryList.CLineTxnLineId.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            journalEntryList.CLineTxnLineId.Add(childNode.InnerText);
                                    }

                                    //Checking Account ref value.
                                    if (childNode.Name.Equals(QBJournalEntry.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBJournalEntry.FullName.ToString()))
                                            {
                                                journalEntryList.CLineAccountFullName.Add(childsNode.InnerText);

                                            }
                                        }
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBJournalEntry.Amount.ToString()))
                                    {
                                        try
                                        {
                                            journalEntryList.CLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch { }
                                    }

                                    //Checking Memo
                                    if (childNode.Name.Equals(QBJournalEntry.Memo.ToString()))
                                    {
                                        journalEntryList.CLineMemo.Add(childNode.InnerText);
                                    }
                                    //Checking Entity FullName
                                    if (childNode.Name.Equals(QBJournalEntry.EntityRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.CLineEntityFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    //Checking Class FullName
                                    if (childNode.Name.Equals(QBJournalEntry.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.CLineClassFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBJournalEntry.ItemSalesTaxRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBJournalEntry.FullName.ToString()))
                                                journalEntryList.CLineItemSalesTaxRefFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Billable Status
                                    if (childNode.Name.Equals(QBJournalEntry.BillableStatus.ToString()))
                                    {
                                        journalEntryList.CLineBillableStatus.Add(childNode.InnerText);
                                    }
                                }
                                count1++;
                            }

                        }
                        //Adding SalesReceipt list.
                        this.Add(journalEntryList);
                    }
                    else
                    { }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfJournalEntry.RemoveAll();
                #endregion
            }
        }

        private void checkCreditNullEntries(XmlNode node, ref JournalEntryList journalEntryList)
        {
            if (node.SelectSingleNode("./" + QBJournalEntry.TxnLineID.ToString()) == null)
            {
                journalEntryList.CLineTxnLineId.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.AccountRef.ToString()) == null)
            {
                journalEntryList.CLineAccountFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.Amount.ToString()) == null)
            {
                journalEntryList.CLineAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.Memo.ToString()) == null)
            {
                journalEntryList.CLineMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.EntityRef.ToString()) == null)
            {
                journalEntryList.CLineEntityFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.ClassRef.ToString()) == null)
            {
                journalEntryList.CLineClassFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.ItemSalesTaxRef.ToString()) == null)
            {
                journalEntryList.CLineItemSalesTaxRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.BillableStatus.ToString()) == null)
            {
                journalEntryList.CLineBillableStatus.Add(null);
            }
        }

        private void checkDebitNullEntries(XmlNode node, ref JournalEntryList journalEntryList)
        {
            if (node.SelectSingleNode("./" + QBJournalEntry.TxnLineID.ToString()) == null)
            {
                journalEntryList.DLineTxnLineId.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.AccountRef.ToString()) == null)
            {
                journalEntryList.DLineAccountFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.Amount.ToString()) == null)
            {
                journalEntryList.DLineAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.Memo.ToString()) == null)
            {
                journalEntryList.DLineMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.EntityRef.ToString()) == null)
            {
                journalEntryList.DLineEntityFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.ClassRef.ToString()) == null)
            {
                journalEntryList.DLineClassFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.ItemSalesTaxRef.ToString()) == null)
            {
                journalEntryList.DLineItemSalesTaxRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBJournalEntry.BillableStatus.ToString()) == null)
            {
                journalEntryList.DLineBillableStatus.Add(null);
            }
        }

        /// <summary>
        /// This method is used for getting JournalEntry from Quickbook for filter
        /// TxnDateRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>

        public Collection<JournalEntryList> PopulateJournalEntryList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting JournalEntry List from QuickBooks.
           

            string JournalEntryXmlList = string.Empty;
          
            JournalEntryXmlList = QBCommonUtilities.GetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromDate, ToDate);

            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> ModifiedPopulateJournalEntryList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
 
            JournalEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromDate, ToDate);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting JournalEntry from Quickbook for filter
        /// TxnDateRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> ModifiedPopulateJournalEntryList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
            //JournalEntryXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("JournalEntryQueryRq", QBFileName, FromDate, ToDate);
            JournalEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromDate, ToDate);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting JournalEntry from QuickBoos for filter
        /// RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> PopulateJournalEntryList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
            JournalEntryXmlList = QBCommonUtilities.GetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromRefnum, ToRefnum);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);

            //Returning object.
            return this;        
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> PopulateJournalEntryList(string QBFileName)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
            JournalEntryXmlList = QBCommonUtilities.GetListFromQuickBook("JournalEntryQueryRq", QBFileName);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting JournalEntry from Quickbooks for filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> PopulateJournalEntryList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
            JournalEntryXmlList = QBCommonUtilities.GetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting JournalEntry from Quickbooks for filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<JournalEntryList> ModifiedPopulateJournalEntryList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string JournalEntryXmlList = string.Empty;
            JournalEntryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("JournalEntryQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            JournalEntryList journalEntryList;
            #endregion
            GetJournalEntryValuesFromXML(JournalEntryXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        
    }
}
