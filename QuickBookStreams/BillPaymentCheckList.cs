﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class is used to implement methods and properties for the baill payment check.
    /// </summary>
    public class BillPaymentsCheckList : BaseBillPaymentCheckList
    {
        #region Public Properties

        public string TxnId
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string PayEntRefFullName
        {
            get { return m_PayEntityRefFullName; }
            set { m_PayEntityRefFullName = value; }
        }

        public string AccountRefFullName
        {
            get { return m_APAccountRefFullName; }
            set { m_APAccountRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string BankAccountRefNumber
        {
            get { return m_BankAccountRefFullName; }
            set { m_BankAccountRefFullName = value; }
        }

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? AmountHomeCurrency
        {
            get { return m_AmountInHomeCurrency; }
            set { m_AmountInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string Addr1
        {
            get { return m_Addr1; }
            set { m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return m_Addr2; }
            set { m_Addr2 = value; }
        }

        public string City
        {
            get { return m_City; }
            set { m_City = value; }
        }

        public string State
        {
            get { return m_State; }
            set { m_State = value; }
        }

        public string PostalCode
        {
            get { return m_PostalCode; }
            set { m_PostalCode = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string[] ATRTxnID
        {
            get { return m_ATRTxnID; }
            set { m_ATRTxnID = value; }
        }

        public string[] ATRRefNumber
        {
            get { return m_ATRRefNumber; }
            set { m_ATRRefNumber = value; }
        }

        public string[] ATRTxnAmount
        {
            get { return m_ATRTxnAmount; }
            set { m_ATRTxnAmount = value; }
        }
        #endregion
    }


    /// <summary>
    /// This collection class is used for getting Bill Payment Check List from QuickBook.
    /// </summary>
    public class QBBillPaymentCheckListCollection : Collection<BillPaymentsCheckList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// This method is used to get Bill Payment Check List for fiters
        /// EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Bill Payment Check List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            //BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("BillPaymentCheckQueryRq", QBFileName, entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get Bill Payment Check List for fiters
        /// TxnDateRangeFilter,entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Check List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            //BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate,entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Bill Payment Check List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payment Check List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            //BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);

            BillPaymentsCheckList BillPaymentCheckList;
            
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCheckList.TxnDate = node.InnerText;
                            }
                        }
                        //Checking BankAccountRef
                        if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                            }
                        }
                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                        {
                            BillPaymentCheckList.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                        {
                            BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                        {
                            BillPaymentCheckList.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                        {
                            BillPaymentCheckList.Memo = node.InnerText;
                        }

                        //Checking Address
                        if (node.Name.Equals(BillPayCheck.Address.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                    BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                    BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                    BillPaymentCheckList.City = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                    BillPaymentCheckList.State = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                    BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                    BillPaymentCheckList.Country = ChildNode.InnerText;
                            }
                        }

                        //Checking IsToBePrinted.
                        if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                        {
                            BillPaymentCheckList.IsToBePrinted = node.InnerText;
                        }

                        //Checking ReceivePayment Line ret values.
                        if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //bug no 65
                                if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    {
                                    }
                                }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Check List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            //BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Bill Payment Check List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Check List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            //BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate,entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payment Check List from QuickBooks.           
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;            
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);

            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Bill Payment Check List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payment Check List from QuickBooks.
         
            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
           
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentsCheckList BillPaymentCheckList;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        ///This method is used to get Bill Payment List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromRefnum, ToRefnum,entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        ///This method is used to get Bill Payment List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromRefnum, ToRefnum);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCheck = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCheck.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCheck.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCheckList.TxnDate = node.InnerText;
                            }
                        }
                        //Checking BankAccountRef
                        if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                            }
                        }
                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                        {
                            BillPaymentCheckList.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                        {
                            BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                        {
                            BillPaymentCheckList.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                        {
                            BillPaymentCheckList.Memo = node.InnerText;
                        }

                        //Checking Address
                        if (node.Name.Equals(BillPayCheck.Address.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                    BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                    BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                    BillPaymentCheckList.City = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                    BillPaymentCheckList.State = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                    BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                    BillPaymentCheckList.Country = ChildNode.InnerText;
                            }
                        }

                        //Checking IsToBePrinted.
                        if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                        {
                            BillPaymentCheckList.IsToBePrinted = node.InnerText;
                        }

                        //Checking ReceivePayment Line ret values.
                        if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //bug no 65
                                if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    {
                                    }
                                }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCheck.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter,Entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOBillPayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOBillPayment.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOBillPayment.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOBillPayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOBillPayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOBillPayment.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOBillPayment.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];                        
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;                                             
                                        }
                                        catch
                                        {
                                        }
                                    }

                                }
                                count++;
                            }
                           
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOBillPayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> PopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOBillPayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOBillPayment.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOBillPayment.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCheckList.TxnDate = node.InnerText;
                            }
                        }
                        //Checking BankAccountRef
                        if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                            }
                        }
                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                        {
                            BillPaymentCheckList.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                        {
                            BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                        {
                            BillPaymentCheckList.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                        {
                            BillPaymentCheckList.Memo = node.InnerText;
                        }

                        //Checking Address
                        if (node.Name.Equals(BillPayCheck.Address.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                    BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                    BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                    BillPaymentCheckList.City = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                    BillPaymentCheckList.State = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                    BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                    BillPaymentCheckList.Country = ChildNode.InnerText;
                            }
                        }

                        //Checking IsToBePrinted.
                        if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                        {
                            BillPaymentCheckList.IsToBePrinted = node.InnerText;
                        }

                        //Checking ReceivePayment Line ret values.
                        if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //bug no 65
                                if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    {
                                    }
                                }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOBillPayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOBillPayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOBillPayment.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOBillPayment.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOBillPayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentsCheckList> ModifiedPopulateBillPaymentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCheckXmlList = string.Empty;
            BillPaymentCheckXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCheckQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BillPaymentsCheckList BillPaymentCheckList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOBillPayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCheckXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOBillPayment.LoadXml(BillPaymentCheckXmlList);
                XmlFileData = BillPaymentCheckXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOBillPayment.GetElementsByTagName(BillPayCheck.BillPaymentCheckRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCheckList = new BillPaymentsCheckList();
                        BillPaymentCheckList.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCheckList.ATRTxnAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCheck.TxnID.ToString()))
                            {
                                BillPaymentCheckList.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCheck.TxnNumber.ToString()))
                            {
                                BillPaymentCheckList.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCheck.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCheck.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCheck.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCheckList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCheckList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking BankAccountRef
                            if (node.Name.Equals(BillPayCheck.BankAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.BankAccountRefNumber = childNode.InnerText;
                                }
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCheck.Amount.ToString()))
                            {
                                BillPaymentCheckList.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCheck.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCheckList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCheck.ExchangeRate.ToString()))
                            {
                                BillPaymentCheckList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCheck.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCheckList.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCheck.RefNumber.ToString()))
                            {
                                BillPaymentCheckList.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCheck.Memo.ToString()))
                            {
                                BillPaymentCheckList.Memo = node.InnerText;
                            }

                            //Checking Address
                            if (node.Name.Equals(BillPayCheck.Address.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr1.ToString()))
                                        BillPaymentCheckList.Addr1 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Addr2.ToString()))
                                        BillPaymentCheckList.Addr2 = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.City.ToString()))
                                        BillPaymentCheckList.City = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.State.ToString()))
                                        BillPaymentCheckList.State = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.PostalCode.ToString()))
                                        BillPaymentCheckList.PostalCode = ChildNode.InnerText;
                                    if (ChildNode.Name.Equals(BillPayCheck.Country.ToString()))
                                        BillPaymentCheckList.Country = ChildNode.InnerText;
                                }
                            }

                            //Checking IsToBePrinted.
                            if (node.Name.Equals(BillPayCheck.IsToBePrinted.ToString()))
                            {
                                BillPaymentCheckList.IsToBePrinted = node.InnerText;
                            }

                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(BillPayCheck.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCheck.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCheckList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCheck.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //bug no 65
                                    if (childNode.Name.Equals(BillPayCheck.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCheckList.ATRTxnAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCheckList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOBillPayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        
    }

}
            