﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections;

namespace Streams
{
    /// <summary>
    /// This class is used to declare  public methods and Properties.
    /// </summary>
    public class CreditCardCreditList : BaseCreditCardCreditList
    {
        string integrated_stringh = string.Empty;
       

        #region Public Methods

        public string TxnID
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string AccountFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public string PayeeEntityFullName
        {
            get { return m_PayeeEntityFullName; }
            set { m_PayeeEntityFullName = value; }
        }       

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public decimal? Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string CurrencyFullName
        {
            get { return m_CurrencyFullName; }
            set { m_CurrencyFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? AmountInHomeCurrency
        {
            get { return m_AmountInHomeCurrency; }
            set { m_AmountInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }
        
        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string SalesTaxCodeFullName
        {
            get { return m_SalesTaxCodeFullName; }
            set { m_SalesTaxCodeFullName = value; }
        }

        
        //For ExpenseLineRet

        public List<string> ExpTxnLineID
        {
            get { return m_ExpTxnLineID; }
            set { m_ExpTxnLineID = value; }
        }

        public List<string> ExpAccountfullName
        {
            get { return m_ExpAccountFullName; }
            set { m_ExpAccountFullName = value; }
        }

        public List<decimal?> ExpAmount
        {
            get { return m_ExpAmount; }
            set { m_ExpAmount = value; }
        }

        public List<string> ExpMemo
        {
            get { return m_ExpMemo; }
            set { m_ExpMemo = value; }
        }

        public List<string> ExpCustomerFullName
        {
            get { return m_ExpCustomerFullname; }
            set { m_ExpCustomerFullname = value; }
        }

        public List<string> ExpClassName
        {
            get { return m_ExpClassFullName; }
            set { m_ExpClassFullName = value; }
        }

        public List<string> ExpSalesTaxCodefullName
        {
            get { return m_ExpSalesTaxCodeFullName; }
            set { m_ExpSalesTaxCodeFullName = value; }
        }

        public List<string> ExpBillableStatus
        {
            get { return m_ExpBillableStatus; }
            set { m_ExpBillableStatus = value; }
        }

        public List<string> ExpSalesRepRef
        {
            get { return m_ExpSalesRepRef; }
            set { m_ExpSalesRepRef = value; }
        }


        //For ItelLineRet

        public List<string> ItemTxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_ItemFullName; }
            set { m_ItemFullName = value; }
        }
        // axis 10.0 changes
        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        // axis 10.0 changes ends
        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<decimal?> Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public List<decimal?> ItemAmount
        {
            get { return m_ItemAmount; }
            set { m_ItemAmount = value; }
        }

        public List<string> ItemCustomerFullName
        {
            get { return m_ItemCustomerFullName; }
            set { m_ItemCustomerFullName = value; }
        }

        public List<string> ItemClassFullName
        {
            get { return m_ItemClassName; }
            set { m_ItemClassName = value; }
        }

        public List<string> ItemSalesTaxCodeFullName
        {
            get { return m_ItemSalesTaxCodeFullName; }
            set { m_ItemSalesTaxCodeFullName = value; }
        }

        public List<string> ItemBillableStatus
        {
            get { return m_ItemBillableStatus; }
            set { m_ItemBillableStatus = value; }
        }

        public List<string> ItemSalesRepRef
        {
            get { return m_ItemSalesRepRef; }
            set { m_ItemSalesRepRef = value; }
        }
        //For GoupItemLineRet

        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineId; }
            set { m_GroupTxnLineId = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemgroupFullName; }
            set { m_ItemgroupFullName = value; }
        }

        //for sub ItemLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<decimal?> GroupLineCost
        {
            get { return m_GroupLineCost; }
            set { m_GroupLineCost = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineCustomerFullName
        {
            get { return m_GroupLineCustomerFullName; }
            set { m_GroupLineCustomerFullName = value; }
        }

        public List<string> GroupLineClassFullName
        {
            get { return m_GroupLineClassName; }
            set { m_GroupLineClassName = value; }
        }

        public List<string> GroupLineSalesTaxCodeFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }

        public List<string> GroupLineBillableStatus
        {
            get { return m_GroupLineBillableStatus; }
            set { m_GroupLineBillableStatus = value; }
        }
        public List<string> GroupLineSalesRepRef
        {
            get { return m_GroupLineSalesRepRef; }
            set { m_GroupLineSalesRepRef = value; }
        }
        #endregion
    }

    /// <summary>
    /// This class is used to get CreditCardCharge from QuickBook.
    /// </summary>
    public class QBCreditCardCreditListCollection : Collection<CreditCardCreditList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Collection<CreditCardCreditList> PopulateCreditCardCreditAllDataList(string QBFileName)
        {
            
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.
            string creditCardCreditXmlList = string.Empty;
            creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName);
            return GetCreditCardCreditList(creditCardCreditXmlList);
            #endregion
        }
        #region private method
        
        private Collection<CreditCardCreditList> GetCreditCardCreditList(string creditCardCreditXmlList)
        {
            #region  Get CreditCard Credit List
            CreditCardCreditList CreditCardChargeList;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLCreditCardCredit = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (creditCardCreditXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLCreditCardCredit.LoadXml(creditCardCreditXmlList);
                XmlFileData = creditCardCreditXmlList;


                #region Getting CreditCardCahrge Values from XML

                //Getting CreditCardChargeList values from QuickBooks response.
                XmlNodeList CreditCardCreditNodeList = outputXMLCreditCardCredit.GetElementsByTagName(QBCreditCardCreditList.CreditCardCreditRet.ToString());

                foreach (XmlNode CreditCardCreditNodes in CreditCardCreditNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int i = 0;
                        int count = 0;
                        int count1 = 0;
                        int count2 = 0;
                        CreditCardChargeList = new CreditCardCreditList();
                       
                        // Axis 10.0 changes ends
                        foreach (XmlNode node in CreditCardCreditNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBCreditCardChargeList.TxnID.ToString()))
                            {
                                CreditCardChargeList.TxnID = node.InnerText;
                            }

                            //Checking Account Ref list.
                            if (node.Name.Equals(QBCreditCardChargeList.AccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                        CreditCardChargeList.AccountFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayeeEntityRef List.
                            if (node.Name.Equals(QBCreditCardChargeList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                        CreditCardChargeList.PayeeEntityFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBCreditCardChargeList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        CreditCardChargeList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        CreditCardChargeList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    CreditCardChargeList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking Amount
                            if (node.Name.Equals(QBCreditCardChargeList.Amount.ToString()))
                            {
                                CreditCardChargeList.Amount = Convert.ToDecimal(node.InnerText);
                            }

                            //Chekcing CurrencyRefFullName
                            if (node.Name.Equals(QBCreditCardChargeList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                        CreditCardChargeList.CurrencyFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ExchangeRate
                            if (node.Name.Equals(QBCreditCardChargeList.ExchangeRate.ToString()))
                            {
                                CreditCardChargeList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking AmountInHomeCurrency
                            if (node.Name.Equals(QBCreditCardChargeList.AmountInHomeCurrency.ToString()))
                            {
                                CreditCardChargeList.AmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }


                            //Checking RefNumber
                            if (node.Name.Equals(QBCreditCardChargeList.RefNumber.ToString()))
                            {
                                CreditCardChargeList.RefNumber = node.InnerText;
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBCreditCardChargeList.Memo.ToString()))
                            {
                                CreditCardChargeList.Memo = node.InnerText;
                            }

                            //Checking IsTaxIncluded
                            if (node.Name.Equals(QBCreditCardChargeList.IsTaxIncluded.ToString()))
                            {
                                CreditCardChargeList.IsTaxIncluded = node.InnerText;
                            }


                            //Checking SalesTaxCodeName
                            if (node.Name.Equals(QBCreditCardChargeList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                        CreditCardChargeList.SalesTaxCodeFullName = childNode.InnerText;
                                }
                            }

                            //Checking Expense Line ret values.(repeated)
                            if (node.Name.Equals(QBCreditCardChargeList.ExpenseLineRet.ToString()))
                            {
                                checkExpNullEntries(node, ref CreditCardChargeList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBCreditCardChargeList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CreditCardChargeList.ExpTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CreditCardChargeList.ExpTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Chekcing AccountRefFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCheckList.FullName.ToString()))
                                                CreditCardChargeList.ExpAccountfullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //checking Expense Amount
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Amount.ToString()))
                                    {
                                        CreditCardChargeList.ExpAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing Expense Memo
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Memo.ToString()))
                                    {
                                        CreditCardChargeList.ExpMemo.Add(childNode.InnerText);
                                    }
                                    //Checking CustomerRefFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ExpCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking classRefFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ExpClassName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxCodeFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ExpSalesTaxCodefullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing BillableStatus
                                    if (childNode.Name.Equals(QBCreditCardChargeList.BillableStatus.ToString()))
                                    {
                                        CreditCardChargeList.ExpBillableStatus.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBCreditCardChargeList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ExpSalesRepRef.Add(childNodes.InnerText);
                                        }
                                    }

                                    ////Axis 11 bug 126
                                    //if (childNode.Name.Equals(QBCreditCardChargeList.DataExtRet.ToString()))
                                    //{

                                    //    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                    //    {
                                    //        CreditCardChargeList.DataExtName[i] = childNode.SelectSingleNode("./DataExtName").InnerText;
                                    //        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                    //            CreditCardChargeList.DataExtValue[i] = childNode.SelectSingleNode("./DataExtValue").InnerText;
                                    //    }

                                    //    i++;
                                    //}

                                }
                                count++;
                            }

                            //Chekcing ItemLineRet
                            if (node.Name.Equals(QBCreditCardChargeList.ItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref CreditCardChargeList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBCreditCardChargeList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CreditCardChargeList.ItemTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CreditCardChargeList.ItemTxnLineID.Add(childNode.InnerText);
                                    }
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    // Axis 10.0 changes
                                    //Chekcing Serial Number
                                    if (childNode.Name.Equals(QBCreditCardChargeList.SerialNumber.ToString()))
                                    {
                                        CreditCardChargeList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    // Checking Lot Number
                                    if (childNode.Name.Equals(QBCreditCardChargeList.LotNumber.ToString()))
                                    {
                                        CreditCardChargeList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends


                                    //Checking Desc
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Desc.ToString()))
                                    {
                                        CreditCardChargeList.Desc.Add(childNode.InnerText);
                                    }
                                    //Checking Quantiy
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Quantity.ToString()))
                                    {
                                        CreditCardChargeList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBCreditCardChargeList.UnitOfMeasure.ToString()))
                                    {
                                        CreditCardChargeList.UOM.Add(childNode.InnerText);
                                    }
                                    //Checking Cost
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Cost.ToString()))
                                    {
                                        CreditCardChargeList.Cost.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBCreditCardChargeList.Amount.ToString()))
                                    {
                                        CreditCardChargeList.ItemAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    //Chekcing CustomerFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemCustomerFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Chekcing ClassFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemClassFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking SalesTaxFullName
                                    if (childNode.Name.Equals(QBCreditCardChargeList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemSalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking BillableStatus
                                    if (childNode.Name.Equals(QBCreditCardChargeList.BillableStatus.ToString()))
                                    {
                                        CreditCardChargeList.ItemBillableStatus.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBCreditCardChargeList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemSalesRepRef.Add(childNodes.InnerText);
                                        }
                                    }

                                    ////Axis 11 bug 126
                                    //if (childNode.Name.Equals(QBCreditCardChargeList.DataExtRet.ToString()))
                                    //{

                                    //    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                    //    {
                                    //        CreditCardChargeList.DataExtName[i] = childNode.SelectSingleNode("./DataExtName").InnerText;
                                    //        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                    //            CreditCardChargeList.DataExtValue[i] = childNode.SelectSingleNode("./DataExtValue").InnerText;
                                    //    }

                                    //    i++;
                                    //}


                                }
                                count1++;
                            }
                            ////Axis 11 bug 126
                            //if (node.Name.Equals(QBCreditCardChargeList.DataExtRet.ToString()))
                            //{

                            //    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            //    {
                            //        CreditCardChargeList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                            //        if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                            //            CreditCardChargeList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            //    }

                            //    i++;
                            //}

                            //Checking ItemGroupLineRet
                            if (node.Name.Equals(QBCreditCardChargeList.ItemGroupLineRet.ToString()))
                            {
                                int count3 = 0;
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBCreditCardChargeList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CreditCardChargeList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CreditCardChargeList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBCreditCardChargeList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                CreditCardChargeList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //if (childNode.Name.Equals(QBCreditCardChargeList.DataExtRet.ToString()))
                                    //{

                                    //    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                    //    {
                                    //        CreditCardChargeList.DataExtName[i] = childNode.SelectSingleNode("./DataExtName").InnerText;
                                    //        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                    //            CreditCardChargeList.DataExtValue[i] = childNode.SelectSingleNode("./DataExtValue").InnerText;
                                    //    }

                                    //    i++;
                                    //}
                                    //Checking  sub ItemLineRet
                                    if (childNode.Name.Equals(QBCreditCardChargeList.ItemLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref CreditCardChargeList);

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking TxnLineID
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    CreditCardChargeList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    CreditCardChargeList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }
                                            //Chekcing ItemRefFullName
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                        CreditCardChargeList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.Desc.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantiy
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.Quantity.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.UnitOfMeasure.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }
                                            //Checking Cost
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.Cost.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineCost.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Checking Amount
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.Amount.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                            }
                                            //Chekcing CustomerFullName
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                        CreditCardChargeList.GroupLineCustomerFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Chekcing ClassFullName
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                        CreditCardChargeList.GroupLineClassFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking SalesTaxFullName
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                        CreditCardChargeList.GroupLineSalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking BillableStatus
                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.BillableStatus.ToString()))
                                            {
                                                CreditCardChargeList.GroupLineBillableStatus.Add(subChildNode.InnerText);
                                            }

                                            if (subChildNode.Name.Equals(QBCreditCardChargeList.SalesRepRef.ToString()))
                                            {
                                                foreach (XmlNode childNodes in subChildNode.ChildNodes)
                                                {
                                                    if (childNodes.Name.Equals(QBCreditCardChargeList.FullName.ToString()))
                                                        CreditCardChargeList.GroupLineSalesRepRef.Add(childNodes.InnerText);
                                                }
                                            }
                                            ////Axis 11 bug 126
                                            //if (subChildNode.Name.Equals(QBCreditCardChargeList.DataExtRet.ToString()))
                                            //{

                                            //    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                            //    {
                                            //        CreditCardChargeList.DataExtName[i] = subChildNode.SelectSingleNode("./DataExtName").InnerText;
                                            //        if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                            //            CreditCardChargeList.DataExtValue[i] = subChildNode.SelectSingleNode("./DataExtValue").InnerText;
                                            //    }

                                            //    i++;
                                            //}
                                        }
                                        count3++;
                                    }
                                }
                                count2++;
                            }

                        }
                        //Adding CreditCardCharge list.
                        this.Add(CreditCardChargeList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLCreditCardCredit.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
            #endregion
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref CreditCardCreditList creditCardChargeList)
        {
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.TxnLineID.ToString()) == null)
            {
                creditCardChargeList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.ItemRef.ToString()) == null)
            {
                creditCardChargeList.GroupLineItemFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.Desc.ToString()) == null)
            {
                creditCardChargeList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.Quantity.ToString()) == null)
            {
                creditCardChargeList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.UnitOfMeasure.ToString()) == null)
            {
                creditCardChargeList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.Cost.ToString()) == null)
            {
                creditCardChargeList.GroupLineCost.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.Amount.ToString()) == null)
            {
                creditCardChargeList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.CustomerRef.ToString()) == null)
            {
                creditCardChargeList.GroupLineCustomerFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.ClassRef.ToString()) == null)
            {
                creditCardChargeList.GroupLineClassFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.SalesTaxCodeRef.ToString()) == null)
            {
                creditCardChargeList.GroupLineSalesTaxCodeFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.BillableStatus.ToString()) == null)
            {
                creditCardChargeList.GroupLineBillableStatus.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditCardChargeList.SalesRepRef.ToString()) == null)
            {
                creditCardChargeList.GroupLineSalesRepRef.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref CreditCardCreditList creditCardChargeList)
        {
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.TxnLineID.ToString()) == null)
            {
                creditCardChargeList.ItemTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.ItemRef.ToString()) == null)
            {
                creditCardChargeList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.SerialNumber.ToString()) == null)
            {
                creditCardChargeList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.LotNumber.ToString()) == null)
            {
                creditCardChargeList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Desc.ToString()) == null)
            {
                creditCardChargeList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Quantity.ToString()) == null)
            {
                creditCardChargeList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.UnitOfMeasure.ToString()) == null)
            {
                creditCardChargeList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Cost.ToString()) == null)
            {
                creditCardChargeList.Cost.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Amount.ToString()) == null)
            {
                creditCardChargeList.ItemAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.CustomerRef.ToString()) == null)
            {
                creditCardChargeList.ItemCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.ClassRef.ToString()) == null)
            {
                creditCardChargeList.ItemClassFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.SalesTaxCodeRef.ToString()) == null)
            {
                creditCardChargeList.ItemSalesTaxCodeFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.BillableStatus.ToString()) == null)
            {
                creditCardChargeList.ItemBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.SalesRepRef.ToString()) == null)
            {
                creditCardChargeList.ItemSalesRepRef.Add(null);
            }
        }

        private void checkExpNullEntries(XmlNode node, ref CreditCardCreditList creditCardChargeList)
        {
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.TxnLineID.ToString()) == null)
            {
                creditCardChargeList.ExpTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.AccountRef.ToString()) == null)
            {
                creditCardChargeList.ExpAccountfullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Amount.ToString()) == null)
            {
                creditCardChargeList.ExpAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.Memo.ToString()) == null)
            {
                creditCardChargeList.ExpMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.CustomerRef.ToString()) == null)
            {
                creditCardChargeList.ExpCustomerFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.ClassRef.ToString()) == null)
            {
                creditCardChargeList.ExpClassName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.SalesTaxCodeRef.ToString()) == null)
            {
                creditCardChargeList.ExpSalesTaxCodefullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.BillableStatus.ToString()) == null)
            {
                creditCardChargeList.ExpBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditCardChargeList.SalesRepRef.ToString()) == null)
            {
                creditCardChargeList.ExpSalesRepRef.Add(null);
            }
        }

        #endregion


        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName)
        {

            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName);
            return GetCreditCardCreditList(creditCardCreditXmlList);
        
            #endregion
            
        }

        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }


        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }


        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }

        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromDate, ToDate);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }

        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }
        public Collection<CreditCardCreditList> PopulateCreditCardCreditList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromRefnum, ToRefnum);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }
        public Collection<CreditCardCreditList> PopulateCreditCardCreditEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string creditCardCreditXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("CreditCardCreditQueryRq", QBFileName, entityFilter);
            return GetCreditCardCreditList(creditCardCreditXmlList);

            #endregion
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditCardCreditList> ModifiedPopulateCreditCardCreditList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.

            string vendorcreditCardCreditXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditCardCreditQueryRq", QBFileName, FromDate, ToDate);

            return GetCreditCardCreditList(vendorcreditCardCreditXmlList);
            #endregion
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        
    }
}
