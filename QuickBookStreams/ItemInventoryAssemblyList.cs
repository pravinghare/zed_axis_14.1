// ===============================================================================
//Axis 9.0 
// ItemInventoryAssemblyList.cs
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class provides properties and methods of QuickBooks Item List.
    /// </summary>
    public class QBItemInventoryAssemblyList : BaseItemInventoryAssemblylist
    {
        #region Public Properties

        public string ListID
        {
            get { return m_ListId; }
            set { m_ListId = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }

        }

        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }

        }

        /// <summary>
        /// Get and Set BarCodeValue
        /// </summary>
        public string BarCodeValue
        {
            get { return m_BarCodeValue; }
            set { m_BarCodeValue = value; }
        }


        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }

        }

        public string ParentRefFullName
        {
            get
            {
                return m_ParentRefFullName;
            }
            set
            {
                m_ParentRefFullName = value;
            }
        }


        public string Sublevel
        {
            get { return m_Sublevel; }
            set { m_Sublevel = value; }
        }


       public string UnitOfMeasureSetRefFullName
        {
            get
            { return m_UnitOfMeasureSetRefFullName; }
            set
            {
                if (value.Length > 20)
                    m_UnitOfMeasureSetRefFullName = value.Remove(20, (value.Length - 20));
                else
                    m_UnitOfMeasureSetRefFullName = value;
            }
        }

        public string SalesTaxCodeRefFullName
        {
            get
            {
                return m_SalesTaxCodeRefFullName;
            }
            set
            {
                m_SalesTaxCodeRefFullName = value;
            }
        }
        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }

       public string IncomeAccountRefFullName
        {
            get { return m_IncomeAccountRefFullName; }
            set { m_IncomeAccountRefFullName = value; }
        }

        public string PurchaseDesc
        {
            get { return m_PurchaseDesc; }
            set { m_PurchaseDesc = value; }
        }

        public string PurchaseCost
        {
            get { return m_PurchaseCost; }
            set { m_PurchaseCost = value; }

        }

        public string COGSAccountRefFullName
        {
            get { return m_COGSAcoountRefFullName; }
            set { m_COGSAcoountRefFullName = value; }
        }

        public string PrefVendorRefFullName
        {
            get { return m_PrefVendorRefFullName; }
            set { m_PrefVendorRefFullName = value; }
        }

        public string AssetAccountRefFullName
        {
            get { return m_AssetAccountRefFullName; }
            set { m_AssetAccountRefFullName = value; }
        }


        public string BuildPoint
        {
            get { return m_BuildPoint; }
            set { m_BuildPoint = value; }

        }

        public string QunatityOnHand
        {
            get { return m_QunatityOnHand; }
            set { m_QunatityOnHand = value; }

        }
        public string AverageCost
        {
            get { return m_AverageCost; }
            set { m_AverageCost = value; }
        }
        public string QuantityOnOrder
        {
            get { return m_QuantityOnOrder; }
            set { m_QuantityOnOrder = value; }
        }
        public string QuantityOnSalesOrder
        {
            get { return m_QuantityOnSalesOrder; }
            set { m_QuantityOnSalesOrder = value; }
        }
        public string[] ItemInventoryListID
        {
            get { return m_QBitemInventoryListID; }
            set { m_QBitemInventoryListID = value; }
        }
        public string[] ItemInventoryFullName
        {
            get { return m_QBitemInventoryFullName; }
            set { m_QBitemInventoryFullName = value; }
        }

        public decimal?[] Quantity
        {
            get { return m_QBquantity; }
            set { m_QBquantity = value; }
        }
       
        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];





        #endregion
    }

    /// <summary>
    /// Collection class used for getting NonInventoryItem List from QuickBooks.
    /// </summary>
    public class QBItemInventoryAssemblyCollection : Collection<QBItemInventoryAssemblyList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemInventoryAssemblyList> PopulateInventoryAssemblyItemList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting InventoryItemAssembly List from QuickBooks.
            int i = 0;
            //Getting InventoryItemAssembly list from QuickBooks.
            string InventoryItemAssemblyList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBookByDate("ItemInventoryAssemblyQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemInventoryAssemblyQueryRq", QBFileName, FromDate, ToDate);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemInventoryAssemblyList QBItemInventoryAssembly;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInventoryAssemblyItem = new XmlDocument();

            if (InventoryItemAssemblyList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfInventoryAssemblyItem.LoadXml(InventoryItemAssemblyList);
                XmlFileData = InventoryItemAssemblyList;

                #region Getting NonInventoryItem Inventory Values from XML

                //Getting InventoryItemAssembly  list from XML.
                XmlNodeList ItemInventoryAssemblyList = outputXMLDocOfInventoryAssemblyItem.GetElementsByTagName(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyRet.ToString());

                foreach (XmlNode InventoryItemAssemblyNodes in ItemInventoryAssemblyList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        QBItemInventoryAssembly = new QBItemInventoryAssemblyList();
                        int count = 0;

                        QBItemInventoryAssembly.ItemInventoryFullName = new string[InventoryItemAssemblyNodes.ChildNodes.Count];

                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in InventoryItemAssemblyNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Name.ToString()))
                                QBItemInventoryAssembly.Name= node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ListID.ToString()))
                                QBItemInventoryAssembly.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeCreated.ToString()))
                                QBItemInventoryAssembly.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeModified.ToString()))
                                QBItemInventoryAssembly.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                QBItemInventoryAssembly.FullName = node.InnerText;

                            if(node.Name.Equals(QBXmlItemInventoryAssemblyList.BarCodeValue.ToString()))
                                QBItemInventoryAssembly.BarCodeValue = node.InnerText;

                            if(node.Name.Equals(QBXmlItemInventoryAssemblyList.IsActive.ToString()))
                                QBItemInventoryAssembly.IsActive = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.ParentRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Sublevel.ToString()))
                                QBItemInventoryAssembly.Sublevel = node.InnerText;

                                                       
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesDesc.ToString()))
                                QBItemInventoryAssembly.SalesDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesPrice.ToString()))
                                QBItemInventoryAssembly.SalesPrice = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseDesc.ToString()))
                                QBItemInventoryAssembly.PurchaseDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseCost.ToString()))
                                QBItemInventoryAssembly.PurchaseCost = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.COGSAccountRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.PrefVendorRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.AssetAccountRefFullName = childNodes.InnerText;
                                }
                            }
                           
                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.BuildPoint.ToString()))
                           QBItemInventoryAssembly.BuildPoint = node.InnerText;
                            
                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnHand.ToString()))
                           QBItemInventoryAssembly.QunatityOnHand = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AverageCost.ToString()))
                           QBItemInventoryAssembly.AverageCost = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnOrder.ToString()))
                           QBItemInventoryAssembly.QuantityOnOrder = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnSalesOrder.ToString()))
                           QBItemInventoryAssembly.QuantityOnSalesOrder = node.InnerText;

                                                                         
                           
                            // For checking Item Inventory Assembly Line 

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyLine.ToString()))
                           {

                               foreach (XmlNode childNode in node.ChildNodes)
                               {
                                   //Checking Item Inventory FullName value.
                                   if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryRef.ToString()))
                                   {
                                       foreach (XmlNode childNodes in childNode.ChildNodes)
                                       {
                                           if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                           {
                                               if (childNodes.InnerText.Length > 159)
                                                   QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                               else
                                                   QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText;
                                           }
                                       }
                                   }


                                   //Checking Quantity values.
                                   if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.Quantity.ToString()))
                                   {
                                       try
                                       {
                                           QBItemInventoryAssembly.Quantity[count] = Convert.ToDecimal(childNode.InnerText);
                                       }
                                       catch
                                       { }
                                   }
                               }
                               count++;
                           }
                           
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    QBItemInventoryAssembly.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        QBItemInventoryAssembly.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(QBItemInventoryAssembly);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInventoryAssemblyItem.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBItemInventoryAssemblyList> PopulateInventoryAssemblyItemList(string QBFileName, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting InventoryItemAssemblyList list from QuickBooks.
            string InventoryItemAssemblyList = string.Empty;
            
            if (inActiveFilter == false)//bug no. 395
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBook("ItemInventoryAssemblyQueryRq", QBFileName);
            }
            else
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBookForInactive("ItemInventoryAssemblyQueryRq", QBFileName);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemInventoryAssemblyList QBItemInventoryAssembly;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInventoryAssemblyItem = new XmlDocument();

            if (InventoryItemAssemblyList != string.Empty)
            {
                //Loading InventoryItemAssembly List into XML.
                outputXMLDocOfInventoryAssemblyItem.LoadXml(InventoryItemAssemblyList);
                XmlFileData = InventoryItemAssemblyList;

                #region Getting NonInventoryItem Inventory Values from XML

                //Getting InventoryItemAssembly  list from XML.
                XmlNodeList ItemInventoryAssemblyList = outputXMLDocOfInventoryAssemblyItem.GetElementsByTagName(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyRet.ToString());

                foreach (XmlNode InventoryItemAssemblyNodes in ItemInventoryAssemblyList)
                {
                    i = 0;
                    int count = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        QBItemInventoryAssembly = new QBItemInventoryAssemblyList();
                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in InventoryItemAssemblyNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Name.ToString()))
                                QBItemInventoryAssembly.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ListID.ToString()))
                                QBItemInventoryAssembly.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeCreated.ToString()))
                                QBItemInventoryAssembly.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeModified.ToString()))
                                QBItemInventoryAssembly.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                QBItemInventoryAssembly.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.BarCodeValue.ToString()))
                                QBItemInventoryAssembly.BarCodeValue = node.InnerText;

                            if(node.Name.Equals(QBXmlItemInventoryAssemblyList.IsActive.ToString()))
                                QBItemInventoryAssembly.IsActive = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.ParentRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Sublevel.ToString()))
                                QBItemInventoryAssembly.Sublevel = node.InnerText;


                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesDesc.ToString()))
                                QBItemInventoryAssembly.SalesDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesPrice.ToString()))
                                QBItemInventoryAssembly.SalesPrice = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseDesc.ToString()))
                                QBItemInventoryAssembly.PurchaseDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseCost.ToString()))
                                QBItemInventoryAssembly.PurchaseCost = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.COGSAccountRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.PrefVendorRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.AssetAccountRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.BuildPoint.ToString()))
                                QBItemInventoryAssembly.BuildPoint = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnHand.ToString()))
                                QBItemInventoryAssembly.QunatityOnHand = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AverageCost.ToString()))
                                QBItemInventoryAssembly.AverageCost = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnOrder.ToString()))
                                QBItemInventoryAssembly.QuantityOnOrder = node.InnerText;

                           if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnSalesOrder.ToString()))
                                QBItemInventoryAssembly.QuantityOnSalesOrder = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyLine.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Item Inventory FullName value.
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            QBItemInventoryAssembly.Quantity[count] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }
                                }
                                count++;
                            }
                           
                           
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    QBItemInventoryAssembly.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        QBItemInventoryAssembly.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(QBItemInventoryAssembly);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInventoryAssemblyItem.RemoveAll();
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for adding various NonInventoryItem list into QuickBooks NonInventoryItem class.
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemInventoryAssemblyList> PopulateInventoryAssemblyItemList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting NonInventoryItem list from QuickBooks.
            string InventoryItemAssemblyList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBookByDate("ItemInventoryAssemblyQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                InventoryItemAssemblyList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemInventoryAssemblyQueryRq", QBFileName, FromDate, ToDate);
            }
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemInventoryAssemblyList QBItemInventoryAssembly;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfInventoryAssemblyItem = new XmlDocument();

            if (InventoryItemAssemblyList != string.Empty)
            {
                //Loading InventoryItemAssembly List into XML.
                outputXMLDocOfInventoryAssemblyItem.LoadXml(InventoryItemAssemblyList);
                XmlFileData = InventoryItemAssemblyList;

                #region Getting InventoryAssemblyItem Values from XML

                //Getting InventoryItemAssembly  list from XML.
                XmlNodeList ItemInventoryAssemblyList = outputXMLDocOfInventoryAssemblyItem.GetElementsByTagName(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyRet.ToString());

                foreach (XmlNode InventoryItemAssemblyNodes in ItemInventoryAssemblyList)
                {
                    i = 0;
                    int count = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        QBItemInventoryAssembly = new QBItemInventoryAssemblyList();
                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in InventoryItemAssemblyNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Name.ToString()))
                                QBItemInventoryAssembly.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ListID.ToString()))
                                QBItemInventoryAssembly.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeCreated.ToString()))
                                QBItemInventoryAssembly.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.TimeModified.ToString()))
                                QBItemInventoryAssembly.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                QBItemInventoryAssembly.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.BarCodeValue.ToString()))
                                QBItemInventoryAssembly.BarCodeValue = node.InnerText;

                            if(node.Name.Equals(QBXmlItemInventoryAssemblyList.IsActive.ToString()))
                                QBItemInventoryAssembly.IsActive = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.ParentRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.Sublevel.ToString()))
                                QBItemInventoryAssembly.Sublevel = node.InnerText;


                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesDesc.ToString()))
                                QBItemInventoryAssembly.SalesDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.SalesPrice.ToString()))
                                QBItemInventoryAssembly.SalesPrice = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.IncomeAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.IncomeAccountRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseDesc.ToString()))
                                QBItemInventoryAssembly.PurchaseDesc = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PurchaseCost.ToString()))
                                QBItemInventoryAssembly.PurchaseCost = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.COGSAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.COGSAccountRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.PrefVendorRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.PrefVendorRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AssetAccountRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                        QBItemInventoryAssembly.AssetAccountRefFullName = childNodes.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.BuildPoint.ToString()))
                                QBItemInventoryAssembly.BuildPoint = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnHand.ToString()))
                                QBItemInventoryAssembly.QunatityOnHand = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.AverageCost.ToString()))
                                QBItemInventoryAssembly.AverageCost = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnOrder.ToString()))
                                QBItemInventoryAssembly.QuantityOnOrder = node.InnerText;

                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.QuantityOnSalesOrder.ToString()))
                                QBItemInventoryAssembly.QuantityOnSalesOrder = node.InnerText;

                      
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryAssemblyLine.ToString()))
                            {
                                //Checking Item Inventory FullName value.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.ItemInventoryRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemInventoryAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim();
                                                else
                                                    QBItemInventoryAssembly.ItemInventoryFullName[count] = childNodes.InnerText;
                                            }
                                        }
                                    }


                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBXmlItemInventoryAssemblyList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            QBItemInventoryAssembly.Quantity[count] = Convert.ToDecimal(childNode.InnerText);
                                        }
                                        catch
                                        { }
                                    }
                                }
                                count++;

                            }
                           
                            if (node.Name.Equals(QBXmlItemInventoryAssemblyList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    QBItemInventoryAssembly.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        QBItemInventoryAssembly.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(QBItemInventoryAssembly);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfInventoryAssemblyItem.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

    }
}
