using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace Streams
{
    /// <summary>
    /// This class is used to declare Methods and Properties.
    /// </summary>
    public class CreditMemoList : BaseCreditMemoList
    {

        public CreditMemoList()
        {
            m_QBItemsDetails = new Collection<QBItemDetails>();
        }
        #region Public Properties

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }               

        public string CustomerRefFullName
        {
            get { return m_CustomerRefFullName; }
            set { m_CustomerRefFullName = value; }
        }

        public string ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }

        public string ARAccountRefFullName
        {
            get { return m_ARAccountRefFullName; }
            set { m_ARAccountRefFullName = value; }
        }

        public string TemplateRefFullName
        {
            get { return m_TemplateRefFullName; }
            set { m_TemplateRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Addr1
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        public string Addr2
        {
            get { return m_BillAddress1; }
            set { m_BillAddress1 = value; }
        }

        public string City
        {
            get { return m_BillAddress2; }
            set { m_BillAddress2 = value; }
        }

        public string State
        {
            get { return m_BillAddress3; }
            set { m_BillAddress3 = value; }
        }

        public string PostalCode
        {
            get { return m_BillAddress4; }
            set { m_BillAddress4 = value; }
        }

        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }

        public string ShipAddr1
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        public string ShipAddr2
        {
            get { return m_ShipAddress1; }
            set { m_ShipAddress1 = value; }
        }

        public string ShipCity
        {
            get { return m_ShipAddress2; }
            set { m_ShipAddress2 = value; }
        }

        public string ShipState
        {
            get { return m_ShipAddress3; }
            set { m_ShipAddress3 = value; }
        }

        public string ShipPostalCode
        {
            get { return m_ShipAddress4; }
            set { m_ShipAddress4 = value; }
        }

        public string ShipCountry
        {
            get { return m_ShipCountry; }
            set { m_ShipCountry = value; }
        }

        public string IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }

        public string PONumber
        {
            get { return m_PONumber; }
            set { m_PONumber = value; }
        }

        public string TermsRefFullName
        {
            get { return m_TermsRefFullName; }
            set { m_TermsRefFullName = value; }
        }

        public string DueDate
        {
            get { return m_DueDate; }
            set { m_DueDate = value; }
        }

        public string SalesRepRefFullName
        {
            get { return m_SalesRepRefFullName; }
            set { m_SalesRepRefFullName = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        public string ShipDate
        {
            get { return m_Shipdate; }
            set { m_Shipdate = value; }
        }

        public string ShipMethodRefFullName
        {
            get { return m_ShipMethodRefFullName; }
            set { m_ShipMethodRefFullName = value; }
        }

        public string ItemSalesTaxRefFullName
        {
            get { return m_ItemSalesTaxRefFullName; }
            set { m_ItemSalesTaxRefFullName = value; }
        }

        public string SalesTaxPercentage
        {
            get { return m_SalesTaxPercentage; }
            set { m_SalesTaxPercentage = value; }
        }

        public decimal? TotalAmount
        {
            get { return m_TotalAmount; }
            set { m_TotalAmount = value; }
        }

        public decimal? CreditRemaining
        {
            get { return m_CreditRemaining; }
            set { m_CreditRemaining = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? CreditRemainingInHomeCurrency
        {
            get { return m_CreditRemainingInHomeCurrency; }
            set { m_CreditRemainingInHomeCurrency = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string CustomerMsgRefFullName
        {
            get { return m_CustomerMsgRefFullName; }
            set { m_CustomerMsgRefFullName = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public string CustomerSalesTaxCodeRefFullName
        {
            get { return m_CustomerSalesTaxCodeRefFullName; }
            set { m_CustomerSalesTaxCodeRefFullName = value; }
        }

        public string Other
        {
            get { return m_Other; }
            set { m_Other = value; }
        }

        public string LinkedTxnID
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }

        //For CreditMemoLineRet
        
        public List<string> TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public List<string> ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }

        public List<string> Desc
        {
            get { return m_desc; }
            set { m_desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<decimal?> Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }

        public List<decimal?> Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public List<string> InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }
        //axis 10.0 changes

        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        // axis 10.0 changes ends

        public List<string> ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }
        
        public List<string> SalesTaxCodeRefFullName
        {
            get { return m_SalesTaxCodeRefFullName; }
            set { m_SalesTaxCodeRefFullName = value; }
        }

       

        //For CreditMemoLineGroupRet

        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

        public List<string> GroupItemRefFullName
        {
            get { return m_GroupItemFullName; }
            set { m_GroupItemFullName = value; }
        }       

        //For SubCreditMemoLineRet

        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }

        public List<string> GroupLineItemRefFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<decimal?> GroupLineRate
        {
            get { return m_GroupLineRate; }
            set { m_GroupLineRate = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineServiceDate
        {
            get { return m_GroupLineServiceDate; }
            set { m_GroupLineServiceDate = value; }
        }

        public List<string> GroupLineSalesTaxCodeRefFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }

        //For Others Fields
        public string Other1
        {
            get { return m_Other1; }
            set { m_Other1 = value; }
        }

        public string Other2
        {
            get { return m_Other2; }
            set { m_Other2 = value; }
        }

        //GroupLineOther
        public List<string> GroupLineOther1
        {
            get { return m_GroupLineOther1; }
            set { m_GroupLineOther1 = value; }
        }

        public List<string> GroupLineOther2
        {
            get { return m_GroupLineOther2; }
            set { m_GroupLineOther2 = value; }
        }

        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();


        public Collection<QBItemDetails> QBItemsDetails
        {
            get { return m_QBItemsDetails; }
            set { m_QBItemsDetails = value; }

        }
        #endregion
    }

    /// <summary>
    /// This class is used for getting credit Memo List from QuickBooks.
    /// </summary>
    public class QBCreditMemoListCollection : Collection<CreditMemoList>
    {
        string XmlFileData = string.Empty;

        public void GetCreditMemoValuesFromXML(string CreditMemoXmlList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfCreditMemo = new XmlDocument();
            CreditMemoList CreditMemoList;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (CreditMemoXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfCreditMemo.LoadXml(CreditMemoXmlList);
                XmlFileData = CreditMemoXmlList;

                #region Getting creditMemo Values from XML

                //Getting CreditMemo values from QuickBooks response.
                XmlNodeList CreditMemoNodeList = outputXMLDocOfCreditMemo.GetElementsByTagName(QBCreditMemoList.CreditMemoRet.ToString());

                foreach (XmlNode CreditMemoNodes in CreditMemoNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        CreditMemoList = new CreditMemoList();
                        foreach (XmlNode node in CreditMemoNodes.ChildNodes)
                        {

                            //Checking TxxID. 
                            if (node.Name.Equals(QBCreditMemoList.TxnID.ToString()))
                            {
                                CreditMemoList.TxnID = node.InnerText;
                            }

                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBCreditMemoList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.CustomerRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRef List.
                            if (node.Name.Equals(QBCreditMemoList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.ClassRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBCreditMemoList.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.ARAccountRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking TemplateRef List.
                            if (node.Name.Equals(QBCreditMemoList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.TemplateRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBCreditMemoList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        CreditMemoList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        CreditMemoList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    CreditMemoList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber.
                            if (node.Name.Equals(QBCreditMemoList.RefNumber.ToString()))
                                CreditMemoList.RefNumber = node.InnerText;

                            //Checking BillAddress.
                            if (node.Name.Equals(QBCreditMemoList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.Addr1.ToString()))
                                        CreditMemoList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.Addr2.ToString()))
                                        CreditMemoList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.City.ToString()))
                                        CreditMemoList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.State.ToString()))
                                        CreditMemoList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.PostalCode.ToString()))
                                        CreditMemoList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.Country.ToString()))
                                        CreditMemoList.Country = childNode.InnerText;
                                }
                            }


                            //Checking ShipAddress.
                            if (node.Name.Equals(QBCreditMemoList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.Addr1.ToString()))
                                        CreditMemoList.ShipAddr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.Addr2.ToString()))
                                        CreditMemoList.ShipAddr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.City.ToString()))
                                        CreditMemoList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.State.ToString()))
                                        CreditMemoList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.PostalCode.ToString()))
                                        CreditMemoList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBCreditMemoList.Country.ToString()))
                                        CreditMemoList.ShipCountry = childNode.InnerText;
                                }
                            }
                            //Checking ISPending
                            if (node.Name.Equals(QBCreditMemoList.IsPending.ToString()))
                            {
                                CreditMemoList.IsPending = node.InnerText;
                            }

                            //Checking PoNUmber
                            if (node.Name.Equals(QBCreditMemoList.PONumber.ToString()))
                            {
                                CreditMemoList.PONumber = node.InnerText;
                            }

                            //Checking TermsrefFullName
                            if (node.Name.Equals(QBCreditMemoList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.TermsRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking DueDate.
                            if (node.Name.Equals(QBCreditMemoList.DueDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtDueDate = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        CreditMemoList.DueDate = dtDueDate.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        CreditMemoList.DueDate = dtDueDate.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    CreditMemoList.DueDate = node.InnerText;
                                }
                            }
                            //Checking SalesRepRefFullName.
                            if (node.Name.Equals(QBCreditMemoList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBSalesReceiptList.FullName.ToString()))
                                        CreditMemoList.SalesRepRefFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing FOB
                            if (node.Name.Equals(QBCreditMemoList.FOB.ToString()))
                            {
                                CreditMemoList.FOB = node.InnerText;
                            }

                            //Checking ShipDate.
                            if (node.Name.Equals(QBCreditMemoList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        CreditMemoList.ShipDate = dtship.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        CreditMemoList.ShipDate = dtship.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    CreditMemoList.ShipDate = node.InnerText;
                                }
                            }
                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBCreditMemoList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.ShipMethodRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ItemSalesTaxRefFullName.
                            if (node.Name.Equals(QBCreditMemoList.ItemSalesTaxRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.ItemSalesTaxRefFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing SalesTaxPercentage
                            if (node.Name.Equals(QBCreditMemoList.SalesTaxPercentage.ToString()))
                            {
                                CreditMemoList.SalesTaxPercentage = node.InnerText;
                            }
                            //Checking Totalamount
                            if (node.Name.Equals(QBCreditMemoList.TotalAmount.ToString()))
                            {
                                CreditMemoList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CreditRemaining.
                            if (node.Name.Equals(QBCreditMemoList.CreditRemaining.ToString()))
                            {
                                CreditMemoList.CreditRemaining = Convert.ToDecimal(node.InnerText);
                            }

                            //Chekcing currencyRef
                            if (node.Name.Equals(QBCreditMemoList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ExchangeRate
                            if (node.Name.Equals(QBCreditMemoList.ExchangeRate.ToString()))
                            {
                                CreditMemoList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking CreditRemainingInHomeCurrency
                            if (node.Name.Equals(QBCreditMemoList.CreditRemainingInHomeCurrency.ToString()))
                            {
                                CreditMemoList.CreditRemainingInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBCreditMemoList.Memo.ToString()))
                            {
                                CreditMemoList.Memo = node.InnerText;
                            }

                            //Checking CustomerMsgFullName
                            if (node.Name.Equals(QBCreditMemoList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.CustomerMsgRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking IsToBePrinted
                            if (node.Name.Equals(QBCreditMemoList.IsToBePrinted.ToString()))
                            {
                                CreditMemoList.IsToBePrinted = node.InnerText;
                            }

                            //Chekcing IsToBeEmailed
                            if (node.Name.Equals(QBCreditMemoList.IsToBeEmailed.ToString()))
                            {
                                CreditMemoList.IsToBeEmailed = node.InnerText;
                            }

                            //Chekcing IsTaxIncluded
                            if (node.Name.Equals(QBCreditMemoList.IsTaxIncluded.ToString()))
                            {
                                CreditMemoList.IsTaxIncluded = node.InnerText;
                            }

                            //Checking CusotmerSalesTaxCodeRefFullName.
                            if (node.Name.Equals(QBCreditMemoList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                        CreditMemoList.CustomerSalesTaxCodeRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking Other
                            if (node.Name.Equals(QBCreditMemoList.Other.ToString()))
                            {
                                CreditMemoList.Other = node.InnerText;
                            }

                            //Checking LinkedTxnID
                            if (node.Name.Equals(QBCreditMemoList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBCreditMemoList.TxnID.ToString()))
                                        CreditMemoList.LinkedTxnID = childNode.InnerText;
                                }
                            }

                            // Checking Custom
                            if (node.Name.Equals(QBCreditMemoList.DataExtRet.ToString()))
                            {
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    CreditMemoList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText + "|" + CreditMemoList.TxnLineID[count]);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        CreditMemoList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText + "|" + CreditMemoList.TxnLineID[count]);
                                }
                                i++;
                            }

                            //Checking CreditMemo Line ret values.
                            if (node.Name.Equals(QBCreditMemoList.CreditMemoLineRet.ToString()))
                            {
                                checkNullEntries(node, ref CreditMemoList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBCreditMemoList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CreditMemoList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CreditMemoList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    //Axis bug No 239
                                    if (childNode.Name.Equals(QBCreditMemoList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                            {
                                                if (childsNode.InnerText.Length > 59)
                                                    CreditMemoList.ItemRefFullName.Add(childsNode.InnerText.Remove(59, (childsNode.InnerText.Length - 59)).Trim());
                                                else
                                                    CreditMemoList.ItemRefFullName.Add(childsNode.InnerText);
                                            }
                                        }
                                    }
                                    //End bug No 239
                                    //Checking Desc
                                    if (childNode.Name.Equals(QBCreditMemoList.Desc.ToString()))
                                    {
                                        CreditMemoList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBCreditMemoList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            CreditMemoList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBCreditMemoList.UnitOfMeasure.ToString()))
                                    {
                                        CreditMemoList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBCreditMemoList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            CreditMemoList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBCreditMemoList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            CreditMemoList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking InventorySiteRefFullName.
                                    if (childNode.Name.Equals(QBCreditMemoList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                                CreditMemoList.InventorySiteRefFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    // axis 10.0 changes

                                    //Checking SerialNumber
                                    if (childNode.Name.Equals(QBCreditMemoList.SerialNumber.ToString()))
                                    {
                                        CreditMemoList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    // checking lotnumber
                                    if (childNode.Name.Equals(QBCreditMemoList.LotNumber.ToString()))
                                    {
                                        CreditMemoList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //checking Service Date                                
                                    if (childNode.Name.Equals(QBCreditMemoList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtService = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                CreditMemoList.ServiceDate.Add(dtService.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                CreditMemoList.ServiceDate.Add(dtService.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            CreditMemoList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRefFullName.
                                    if (childNode.Name.Equals(QBCreditMemoList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childsNode in childNode.ChildNodes)
                                        {
                                            if (childsNode.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                                CreditMemoList.SalesTaxCodeRefFullName.Add(childsNode.InnerText);
                                        }
                                    }

                                    //Checking Other1.

                                    if (childNode.Name.Equals(QBCreditMemoList.Other1.ToString()))
                                    {
                                        CreditMemoList.Other1 = childNode.InnerText;
                                    }
                                    //Checking Other2.
                                    if (childNode.Name.Equals(QBCreditMemoList.Other2.ToString()))
                                    {
                                        CreditMemoList.Other2 = childNode.InnerText;
                                    }

                                    // Checking Custom
                                    if (childNode.Name.Equals(QBCreditMemoList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            CreditMemoList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + CreditMemoList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                CreditMemoList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + CreditMemoList.TxnLineID[count]);
                                        }

                                        i++;
                                    }
                                }
                            }
                            //Checking CreditMemoGroupLineRet
                            if (node.Name.Equals(QBCreditMemoList.CreditMemoLineGroupRet.ToString()))
                            {
                                int count2 = 0;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBCreditMemoList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            CreditMemoList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            CreditMemoList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBCreditMemoList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                                CreditMemoList.GroupItemRefFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking  sub CreditMemoLineRet.
                                    if (childNode.Name.Equals(QBCreditMemoList.CreditMemoLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref CreditMemoList);

                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    CreditMemoList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                                else
                                                    CreditMemoList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBInvoicesList.FullName.ToString()))
                                                        CreditMemoList.GroupLineItemRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.Desc.ToString()))
                                            {
                                                CreditMemoList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    CreditMemoList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking UOM
                                            if (childNode.Name.Equals(QBCreditMemoList.UnitOfMeasure.ToString()))
                                            {
                                                CreditMemoList.GroupLineUOM.Add(childNode.InnerText);
                                            }

                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    CreditMemoList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBCreditMemoList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    CreditMemoList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBCreditMemoList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        CreditMemoList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        CreditMemoList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    CreditMemoList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBCreditMemoList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBCreditMemoList.FullName.ToString()))
                                                        CreditMemoList.GroupLineSalesTaxCodeRefFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                        }
                                        count2++;
                                    }
                                }
                                count1++;
                            }


                        }
                        //Adding CreditMemo list.
                        this.Add(CreditMemoList);
                    }
                    else
                    {  }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfCreditMemo.RemoveAll();
                #endregion
            }
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref CreditMemoList creditMemoList)
        {
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.TxnLineID.ToString()) == null)
            {
                creditMemoList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.ItemRef.ToString()) == null)
            {
                creditMemoList.GroupLineItemRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.Desc.ToString()) == null)
            {
                creditMemoList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.Quantity.ToString()) == null)
            {
                creditMemoList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.UnitOfMeasure.ToString()) == null)
            {
                creditMemoList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.Rate.ToString()) == null)
            {
                creditMemoList.GroupLineRate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.Amount.ToString()) == null)
            {
                creditMemoList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.ServiceDate.ToString()) == null)
            {
                creditMemoList.GroupLineServiceDate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBCreditMemoList.SalesTaxCodeRef.ToString()) == null)
            {
                creditMemoList.GroupLineSalesTaxCodeRefFullName.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref CreditMemoList creditMemoList)
        {
            if (node.SelectSingleNode("./" + QBCreditMemoList.TxnLineID.ToString()) == null)
            {
                creditMemoList.TxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.ItemRef.ToString()) == null)
            {
                creditMemoList.ItemRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Desc.ToString()) == null)
            {
                creditMemoList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Quantity.ToString()) == null)
            {
                creditMemoList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.UnitOfMeasure.ToString()) == null)
            {
                creditMemoList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Rate.ToString()) == null)
            {
                creditMemoList.Rate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Amount.ToString()) == null)
            {
                creditMemoList.Amount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.InventorySiteRef.ToString()) == null)
            {
                creditMemoList.InventorySiteRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.SerialNumber.ToString()) == null)
            {
                creditMemoList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.LotNumber.ToString()) == null)
            {
                creditMemoList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.ServiceDate.ToString()) == null)
            {
                creditMemoList.ServiceDate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.SalesTaxCodeRef.ToString()) == null)
            {
                creditMemoList.SalesTaxCodeRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Other1.ToString()) == null)
            {
                creditMemoList.Other1 = null;
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.Other2.ToString()) == null)
            {
                creditMemoList.Other2 = null;
            }
            if (node.SelectSingleNode("./" + QBCreditMemoList.DataExtRet.ToString()) == null)
            {
                creditMemoList.DataExtName.Add(null);
                creditMemoList.DataExtValue.Add(null);
            }
        }

        /// <summary>
        /// This method is used for getting creditMemo List from QuickBook for 
        /// Filters TxnDateRangeFilters,entity filter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Credit Memo from QuickBooks.
            //Getting item list from QuickBooks.
            string CreditMemoXmlList = string.Empty;
            //CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("CreditMemoQueryRq", QBFileName, FromDate, ToDate);
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
           

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting creditMemo List from QuickBook for 
        /// Filters TxnDateRangeFilters. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            //CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("CreditMemoQueryRq", QBFileName, FromDate, ToDate);
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate);

          
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;            
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used for getting creditMemo List from QuickBook for 
        /// Filters TxnDateRangeFilters,entity filter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;            
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);

            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;            
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used for getting creditMemo List from QuickBook for 
        /// Filters TxnDateRangeFilters. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;            
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Credit memo List from  QuickBooks for
        /// filters EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("CreditMemoQueryRq", QBFileName,entityFilter);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Credit memo List from  QuickBooks for
        /// filters RefNumberRangeFilter,entityFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromRefnum, ToRefnum,entityFilter);
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Credit memo List from  QuickBooks for
        /// filters RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, string FromRefnum, string ToRefnum)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromRefnum, ToRefnum);
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Credit Memo list for filters
        /// TxnDateRangeFilter and RefRangeFilter,entity filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName);
            #endregion

            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Credit Memo list for filters
        /// TxnDateRangeFilter and RefRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> PopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.GetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Credit Memo list for filters
        /// TxnDateRangeFilter and RefRangeFilter,entity filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum,entityFilter);

            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting Credit Memo list for filters
        /// TxnDateRangeFilter and RefRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<CreditMemoList> ModifiedPopulateCreditMemoList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {

            #region Getting Credit Memo from QuickBooks.

            //Getting item list from QuickBooks.

            string CreditMemoXmlList = string.Empty;
            CreditMemoXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("CreditMemoQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            #endregion
            GetCreditMemoValuesFromXML(CreditMemoXmlList);
            //Returning object.
            return this;
        } //Done12

        public List<string> GetTxnLineIdList(string QBFileName, string TxnId)
        {
            List<string> creditMemoTxnLineId = new List<string>();
            string creditMemoXmlList = QBCommonUtilities.GetAllListFromQuickBook("CreditMemoQueryRq", QBFileName, TxnId);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(creditMemoXmlList);
            XmlFileData = creditMemoXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CreditMemoNodeList = outputXMLDoc.GetElementsByTagName(QBCreditMemoList.CreditMemoRet.ToString());

            foreach (XmlNode creditMemoNodes in CreditMemoNodeList)
            {
                #region For creditMemo data
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in creditMemoNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBCreditMemoList.CreditMemoLineRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBCreditMemoList.TxnLineID.ToString()))
                                {
                                    creditMemoTxnLineId.Add(childNode.InnerText);
                                }
                            }
                        }
                    }
                }
                else
                { return null; }

                #endregion
            }
            return creditMemoTxnLineId;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        
    }
}

