using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;
using DataProcessingBlocks;
using System.Xml.Serialization;
using QuickBookEntities;
using TransactionImporter;
using System.Windows.Forms;
using EDI.Message;

namespace Streams
{
    [XmlRootAttribute("ItemReceiptAddRq", Namespace = "", IsNullable = false)]
    public class ItemReceiptAddRq
    {
        #region Private Members

        private int m_messageId;
        private ItemReceiptAdd m_itemReceiptAdd;
        private string m_companyFileName;
        private string m_response;
        private int m_failureCount;
        private int m_successCount;
        private bool m_requestCancel;
        private bool m_refExists;

        #endregion

        #region Public Properties

        public ItemReceiptAdd ItemReceiptAdd
        {
            get { return m_itemReceiptAdd; }
            set { m_itemReceiptAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public string CompanyFileName
        {
            get { return m_companyFileName; }
            set { m_companyFileName = value; }
        }

        #endregion

        #region Private Methods

        private bool AddTP(string[] stream)
        {
            ItemLineAdd itemLineAdd = new ItemLineAdd();

            if (m_itemReceiptAdd.RefNumber == stream[1].Trim())
            {
                if (stream[5].Trim() == string.Empty)
                {
                    throw new Exception("Item name field missing");
                }
                else
                {
                    itemLineAdd.ItemRef.FullName = stream[5].Trim();
                }
                if (stream[6].Trim() == string.Empty)
                {
                    throw new Exception("Item quantity field missing");
                }
                else
                {
                    itemLineAdd.Quantity = Convert.ToInt32(stream[6].Trim());
                }
            }
            else
            {
                if (m_refExists)
                {
                    ExportToQuickBooks();
                    m_itemReceiptAdd.ItemLineAdd.Clear();
                }
                m_itemReceiptAdd.RefNumber = stream[1].Trim();

               

                if (stream[3].Trim() == string.Empty)
                {
                    throw new Exception("Vendor Id field missing");
                }
                else
                {
                    m_itemReceiptAdd.VendorRef.ListID = stream[3].Trim();
                }
                
                m_itemReceiptAdd.TxnDate = stream[4].Trim();


                if (stream[5].Trim() == string.Empty)
                {
                    throw new Exception("Item name field missing");
                }
                else
                {
                    itemLineAdd.ItemRef.FullName = stream[5].Trim();
                }
                if (stream[6].Trim() == string.Empty)
                {
                    throw new Exception("Item quantity field missing");
                }
                else
                {
                    itemLineAdd.Quantity = Convert.ToInt32(stream[6].Trim());
                }
                m_refExists = true;

            }
            if (stream.Length == 8)
            {
                itemLineAdd.Desc = stream[7].Trim();
            }
            else
            {
                itemLineAdd.Desc = string.Empty;
            }
            m_itemReceiptAdd.ItemLineAdd.Add(itemLineAdd);
            
            if (stream.Length == 9)
            {
                m_itemReceiptAdd.Memo = stream[8].Trim();
            }
            else
            {
                m_itemReceiptAdd.Memo = string.Empty;
            }
            return true;
        }

        #endregion

        #region Constructors

        public ItemReceiptAddRq(int messageId)
        {
            m_messageId = messageId;
            m_requestCancel = false;
            m_response = string.Empty;
            m_failureCount = 0;
            m_successCount = 0;
            m_itemReceiptAdd = new ItemReceiptAdd();

        }

        public ItemReceiptAddRq()
        { }

        #endregion

        #region Public Method

        public void Parse(string attachmentContent)
        {
            int startIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;

            while (endIndex <= (attachmentContent.Length))
            {
                string[] stream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                if (stream[0].Trim().Equals("TP"))
                {
                    if (!AddTP(stream))
                    {
                        throw new Exception("Error in parsing TP stream in the attachment");
                    }
                }
                else
                {
                    throw new Exception("Attachment is not in correct format");
                }

                startIndex = endIndex + Constants.NEWLINE.Length;
                if (startIndex < attachmentContent.Length)
                {
                    endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                }
                else
                {
                    break;
                }
            }
            ExportToQuickBooks();

            if (CommonUtilities.GetInstance().IsInboundFlag == true)
            {
                CommonUtilities.WriteErrorLog(m_response);

                if (m_failureCount > 0)
                    CommonUtilities.GetInstance().FailureCount = CommonUtilities.GetInstance().FailureCount + 1;
                else if (m_successCount > 0)
                    CommonUtilities.GetInstance().ProcessedCount = CommonUtilities.GetInstance().ProcessedCount + 1;
            }
            else
            {
                ImportSummary importSummary = new ImportSummary();
                importSummary.SDKMSG = m_response;
                importSummary.FailMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG020"), Convert.ToString(m_failureCount));
                importSummary.SuccesMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG019"), Convert.ToString(m_successCount));
                importSummary.ShowDialog();
                importSummary.Dispose();
            }
            
            if (m_failureCount > 0)
            {
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Failed);
                new MessageController().ChangeAttachmentStatus(m_messageId, (int)MessageStatus.Failed);
                CommonUtilities.WriteErrorLog(m_response);
            }
            else
            {
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Processed);
                new MessageController().ChangeAttachmentStatus(m_messageId, (int)MessageStatus.Processed);
            }
        }

        private bool ExportToQuickBooks()
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<ItemReceiptAddRq>.Save(this, fileName);
            }
            catch
            {
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            requestXmlDoc = new System.Xml.XmlDocument();
            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptAddRq = requestXmlDoc.CreateElement("ItemReceiptAddRq");
            inner.AppendChild(ItemReceiptAddRq);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);

            ItemReceiptAddRq.InnerXml = requestXML;
            //For add request id to track error message
            string resp = string.Empty;
            string responseFile = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(m_companyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            m_response = m_response + oNode.Attributes.Item(2).InnerText + "\r\n";
                            m_failureCount++;
                            DialogResult result = CommonUtilities.ShowMessage(oNode.Attributes.Item(2).InnerText, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                m_requestCancel = true;
                            }
                        }
                        else if (statusSeverity == "Info")
                        {
                            m_successCount++;
                        }
                        else if (statusSeverity.ToLower() == "warn")
                        {
                            m_response = m_response + oNode.Attributes.Item(2).InnerText + "\r\n";
                            m_failureCount++;

                        }
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty || m_requestCancel)
            {
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;
                }
                else
                    return true;
            }
        }
        #endregion
    }
}
