using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class provide properties and methods for TransferInventory
    /// </summary>
    public class QBTransferInventoryList : BaseTransferInventoryList
    {
        #region Public Properties


        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string FromInventorySiteRefListID
        {
            get { return m_FromInventorySiteRefListID; }
            set { m_FromInventorySiteRefListID = value; }
        }

        public string FromInventorySiteRefFullName
        {
            get { return m_FromInventorySiteRefFullName; }
            set { m_FromInventorySiteRefFullName = value; }
        }

        public string ToInventorySiteRefListID
        {
            get { return m_ToInventorySiteRefListID; }
            set { m_ToInventorySiteRefListID = value; }
        }

        public string ToInventorySiteRefFullName
        {
            get { return m_ToInventorySiteRefFullName; }
            set { m_ToInventorySiteRefFullName = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }


        //Line Add

        public string[] ItemRefListID
        {
            get { return m_ItemRefListID; }
            set { m_ItemRefListID = value; }
        }

        public string[] TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public string[] ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }

        // axis 10.0 changes
        public string[] FromInventorySiteLineRefFullName
        {
            get { return m_FromInventorySiteLineRefFullName; }
            set { m_FromInventorySiteLineRefFullName = value; }
        }
        public string[] ToInventorySiteLineRefFullName
        {
            get { return m_ToInventorySiteLineRefFullName; }
            set { m_ToInventorySiteLineRefFullName = value; }
        }

        public string[] SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public string[] LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        // axis 10.0 changes ends
        public string[] QuantityToTransfer
        {
            get { return m_QuantityToTransfer; }
            set { m_QuantityToTransfer = value; }
        }

        #endregion
    }

    /// <summary>
    /// This collection class is used for getting TransferInventory List from QuickBook 
    /// </summary>
    public class QBTransferInventoryListCollection : Collection<QBTransferInventoryList>
    {
        string XmlFileData = string.Empty;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> PopulateTransferInventoryList(string QBFileName, DateTime FromDate, string ToDate)
        {

            string tranInvXmlList = QBCommonUtilities.GetListFromQuickBookByDate("TransferInventoryQueryRq", QBFileName, FromDate, ToDate);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(tranInvXmlList);
            XmlFileData = tranInvXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

            foreach (XmlNode TransNodes in TransNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    int count = 0;
                    QBTransferInventoryList transfer = new QBTransferInventoryList();
                    foreach (XmlNode node in TransNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                            transfer.TxnID = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                            transfer.TxnNumber = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                            transfer.TxnDate = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                            transfer.RefNumber = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                    transfer.FromInventorySiteRefListID = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                    transfer.FromInventorySiteRefFullName = childNode.InnerText;

                            }
                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                    transfer.ToInventorySiteRefListID = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                    transfer.ToInventorySiteRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                        {
                            checkNullEntries(node, ref transfer, count);

                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                    transfer.TxnLineID[count] = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                    }
                                }
                                // Axis -7(Axis 10.1changes)
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        //    transfer.FromInventorySiteRefListID = subchildNode.InnerText;

                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                    }
                                }

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        //    transfer.ToInventorySiteRefListID = subchildNode.InnerText;

                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                    }
                                }

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                    transfer.SerialNumber[count] = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                    transfer.LotNumber[count] = childNode.InnerText;

                                //End Of Axis -7(Axis 10.1changes) changes.
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                    transfer.QuantityToTransfer[count] = childNode.InnerText;

                            }
                            count++;

                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                            transfer.Memo = node.InnerText;
                    }
                    this.Add(transfer);
                }
            }


            return this;
        }

        private void checkNullEntries(XmlNode node, ref QBTransferInventoryList transfer, int count)
        {
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.TxnLineID.ToString()) == null)
            {
                transfer.TxnLineID[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.ItemRef.ToString()) == null)
            {
                transfer.ItemRefFullName[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()) == null)
            {
                transfer.FromInventorySiteLineRefFullName[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()) == null)
            {
                transfer.ToInventorySiteLineRefFullName[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.SerialNumber.ToString()) == null)
            {
                transfer.SerialNumber[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.LotNumber.ToString()) == null)
            {
                transfer.LotNumber[count] = null;
            }
            if (node.SelectSingleNode("./" + QBXmlTransferInventoryList.QuantityTransferred.ToString()) == null)
            {
                transfer.QuantityToTransfer[count] = null;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> PopulateTransferInventoryList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            //InvoicesXmlList = QBCommonUtilities.GetListFromQuickBook("InvoiceQueryRq", QBFileName, FromDate, ToDate);
            string tranInvXmlList = QBCommonUtilities.GetListFromQuickBook("TransferInventoryQueryRq", QBFileName, FromDate, ToDate);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(tranInvXmlList);
            XmlFileData = tranInvXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

            foreach (XmlNode TransNodes in TransNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBTransferInventoryList transfer = new QBTransferInventoryList();
                    int count = 0;
                    foreach (XmlNode node in TransNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                            transfer.TxnID = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                            transfer.TxnNumber = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                            transfer.TxnDate = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                            transfer.RefNumber = node.InnerText;

                        if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                    transfer.FromInventorySiteRefListID = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                    transfer.FromInventorySiteRefFullName = childNode.InnerText;

                            }
                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                    transfer.ToInventorySiteRefListID = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                    transfer.ToInventorySiteRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                        {
                            checkNullEntries(node, ref transfer, count);

                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                    transfer.TxnLineID[count] = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                    }
                                }

                                // Axis -7(Axis 10.1changes)
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        //    transfer.FromInventorySiteRefListID = subchildNode.InnerText;

                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                    }
                                }

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        //    transfer.ToInventorySiteRefListID = subchildNode.InnerText;

                                        if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                            transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                    }
                                }
                                if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                    transfer.SerialNumber[count] = childNode.InnerText;

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                    transfer.LotNumber[count] = childNode.InnerText;
                                //End Of Axis -7(Axis 10.1changes) changes.

                                if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                    transfer.QuantityToTransfer[count] = node.InnerText;

                            }
                            count++;

                        }

                        if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                            transfer.Memo = node.InnerText;
                    }
                    this.Add(transfer);
                }
            }

            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> ModifiedPopulateTransferInventoryList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.
            string TransferInventoryXmlList = string.Empty;

            TransferInventoryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TransferInventoryQueryRq", QBFileName, FromDate, ToDate);

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TransferInventoryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(TransferInventoryXmlList);
                XmlFileData = TransferInventoryXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

                foreach (XmlNode TransNodes in TransNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        QBTransferInventoryList transfer = new QBTransferInventoryList();
                        int count = 0;
                        foreach (XmlNode node in TransNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                                transfer.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                                transfer.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                                transfer.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                                transfer.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.FromInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.FromInventorySiteRefFullName = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.ToInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.ToInventorySiteRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                            {
                                checkNullEntries(node, ref transfer, count);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                        transfer.TxnLineID[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                            //    transfer.ToInventorySiteRefListID = subchildNode.InnerText;

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                        transfer.SerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                        transfer.LotNumber[count] = childNode.InnerText;
                                    //End Of Axis -7(Axis 10.1changes) changes.
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                        transfer.QuantityToTransfer[count] = childNode.InnerText;
                                }
                                count++;

                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                                transfer.Memo = node.InnerText;
                        }
                        this.Add(transfer);
                    }
                }
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting TransferInventory list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> ModifiedPopulateTransferInventoryList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.
            //Getting item list from QuickBooks.
            string TransferInventoryXmlList = string.Empty;
            TransferInventoryXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("TransferInventoryQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TransferInventoryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(TransferInventoryXmlList);
                XmlFileData = TransferInventoryXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

                foreach (XmlNode TransNodes in TransNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        QBTransferInventoryList transfer = new QBTransferInventoryList();
                        int count = 0;
                        foreach (XmlNode node in TransNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                                transfer.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                                transfer.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                                transfer.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                                transfer.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.FromInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.FromInventorySiteRefFullName = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.ToInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.ToInventorySiteRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                            {
                                checkNullEntries(node, ref transfer, count);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                        transfer.TxnLineID[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                            //    transfer.ToInventorySiteRefListID = subchildNode.InnerText;

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                        transfer.SerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                        transfer.LotNumber[count] = childNode.InnerText;
                                    //End Of Axis -7(Axis 10.1changes) changes.
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                        transfer.QuantityToTransfer[count] = childNode.InnerText;
                                }
                                count++;

                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                                transfer.Memo = node.InnerText;
                        }
                        this.Add(transfer);
                    }
                }
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getiing TransferInventory list by passing Comapany file Name,
        /// FromRefnumber and ToRefNumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> PopulateTransferInventoryList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.

            string TransferInventoryXmlList = string.Empty;
            TransferInventoryXmlList = QBCommonUtilities.GetListFromQuickBook("TransferInventoryQueryRq", QBFileName, FromRefnum, ToRefnum);


            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (TransferInventoryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(TransferInventoryXmlList);
                XmlFileData = TransferInventoryXmlList;

                #region Getting invoices Values from XML

                //Getting Transfer Inventory values from QuickBooks response.
                XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

                foreach (XmlNode TransNodes in TransNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        QBTransferInventoryList transfer = new QBTransferInventoryList();
                        int count = 0;
                        foreach (XmlNode node in TransNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                                transfer.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                                transfer.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                                transfer.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                                transfer.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.FromInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.FromInventorySiteRefFullName = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.ToInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.ToInventorySiteRefFullName = childNode.InnerText;
                                }
                            }
                            if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                            {
                                checkNullEntries(node, ref transfer, count);
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                        transfer.TxnLineID[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                            //    transfer.FromInventorySiteRefListID = subchildNode.InnerText;

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    //End Of Axis -7(Axis 10.1changes) changes. // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                            //    transfer.ToInventorySiteRefListID = subchildNode.InnerText;

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                        transfer.SerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                        transfer.LotNumber[count] = childNode.InnerText;

                                    //End Of Axis -7(Axis 10.1changes) changes.
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                        transfer.QuantityToTransfer[count] = childNode.InnerText;

                                }
                                count++;
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                                transfer.Memo = node.InnerText;
                        }
                        this.Add(transfer);
                    }
                }
                #endregion
            }


            //Returning object.
            return this;
        }

        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> PopulateTransferInventoryList(string QBFileName)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.
            string TransferInventoryXmlList = string.Empty;
            TransferInventoryXmlList = QBCommonUtilities.GetListFromQuickBook("TransferInventoryQueryRq", QBFileName);

            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (TransferInventoryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(TransferInventoryXmlList);
                XmlFileData = TransferInventoryXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

                foreach (XmlNode TransNodes in TransNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        QBTransferInventoryList transfer = new QBTransferInventoryList();
                        int count = 0;
                        foreach (XmlNode node in TransNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                                transfer.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                                transfer.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                                transfer.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                                transfer.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.FromInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.FromInventorySiteRefFullName = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.ToInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.ToInventorySiteRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                            {
                                checkNullEntries(node, ref transfer, count);
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                        transfer.TxnLineID[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            //if (subchildNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                            //    transfer.FromInventorySiteRefListID = subchildNode.InnerText;

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                        transfer.SerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                        transfer.LotNumber[count] = childNode.InnerText;
                                    //End Of Axis -7(Axis 10.1changes) changes.
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                        transfer.QuantityToTransfer[count] = childNode.InnerText;
                                }
                                count++;

                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                                transfer.Memo = node.InnerText;
                        }
                        this.Add(transfer);
                    }
                }
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting invoices list by passing company filename,
        /// fromdate, todate, fromRefnumber, toRefnumber.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<QBTransferInventoryList> PopulateTransferInventoryList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Invoices List from QuickBooks.

            //Getting item list from QuickBooks.
            string TransferInventoryXmlList = string.Empty;
            TransferInventoryXmlList = QBCommonUtilities.GetListFromQuickBook("TransferInventoryQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (TransferInventoryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(TransferInventoryXmlList);
                XmlFileData = TransferInventoryXmlList;

                #region Getting invoices Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList TransNodeList = outputXMLDoc.GetElementsByTagName(QBXmlTransferInventoryList.TransferInventoryRet.ToString());

                foreach (XmlNode TransNodes in TransNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        QBTransferInventoryList transfer = new QBTransferInventoryList();
                        int count = 0;
                        foreach (XmlNode node in TransNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnID.ToString()))
                                transfer.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnNumber.ToString()))
                                transfer.TxnNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.TxnDate.ToString()))
                                transfer.TxnDate = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.RefNumber.ToString()))
                                transfer.RefNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.FromInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.FromInventorySiteRefFullName = childNode.InnerText;

                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ListID.ToString()))
                                        transfer.ToInventorySiteRefListID = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                        transfer.ToInventorySiteRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.TransferInventoryLineRet.ToString()))
                            {
                                checkNullEntries(node, ref transfer, count);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.TxnLineID.ToString()))
                                        transfer.TxnLineID[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    // Axis -7(Axis 10.1changes)
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.FromInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.FromInventorySiteLineRefFullName[count] = subchildNode.InnerText;

                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.ToInventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {

                                            if (subchildNode.Name.Equals(QBXmlTransferInventoryList.FullName.ToString()))
                                                transfer.ToInventorySiteLineRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.SerialNumber.ToString()))
                                        transfer.SerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.LotNumber.ToString()))
                                        transfer.LotNumber[count] = childNode.InnerText;

                                    //End Of Axis -7(Axis 10.1changes) changes.
                                    if (childNode.Name.Equals(QBXmlTransferInventoryList.QuantityTransferred.ToString()))
                                        transfer.QuantityToTransfer[count] = childNode.InnerText;
                                }
                                count++;

                            }

                            if (node.Name.Equals(QBXmlTransferInventoryList.Memo.ToString()))
                                transfer.Memo = node.InnerText;
                        }
                        this.Add(transfer);
                    }
                }
                #endregion
            }

            //Returning object.
            return this;
        }
    }
}


