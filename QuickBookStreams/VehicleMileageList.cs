﻿// ===============================================================================
//  bug 485 axis 12.0 
//
// This file contains the implementations of the QuickBooks vehicle mileage request methods and
// Properties.
//
// Developed By : 
// Date : 29/01/16
// Modified By : 
// Date :
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;



namespace Streams
{


    public class VehicleMileageList : BaseVehicleMileageList
    {
        #region Public Properties
        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }
        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }
        public string VehicleRefFullName
        {
            get { return m_VehicleRefFullName; }
            set { m_VehicleRefFullName = value; }
        }

        public string CustomerRefFullName
        {
            get { return m_CustomerRefFullName; }
            set { m_CustomerRefFullName = value; }
        }

        //public string CustomerRef
        //{
        //    get { return m_CustomerRef; }
        //    set { m_CustomerRef = value; }
        //}
        //public string ItemRef
        //{
        //    get { return m_ItemRef; }
        //    set { m_ItemRef = value; }
        //}

        public string ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }
        //public string ClassRef
        //{
        //    get { return m_ClassRef; }
        //    set { m_ClassRef = value; }
        //}

        public string ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }
        public string TripStartDate
        {
            get { return m_TripStartDate; }
            set { m_TripStartDate = value; }
        }
        public string TripEndDate
        {
            get { return m_TripEndDate; }
            set { m_TripEndDate = value; }
        }
        public string OdometerStart
        {
            get { return m_OdometerStart; }
            set { m_OdometerStart = value; }
        }
        public string OdometerEnd
        {
            get { return m_OdometerEnd; }
            set { m_OdometerEnd = value; }
        }
        public string TotalMiles
        {
            get { return m_TotalMiles; }
            set { m_TotalMiles = value; }
        }
        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }
        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }
        public string[] TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }
        public string StandardMileageRate
        {
            get { return m_StandardMileageRate; }
            set { m_StandardMileageRate = value; }
        }
        public string StandardMileageTotalAmount
        {
            get { return m_StandardMileageTotalAmount; }
            set { m_StandardMileageTotalAmount = value; }
        }
        public string BillableRate
        {
            get { return m_BillableRate; }
            set { m_BillableRate = value; }
        }
        public string BillableAmount
        {
            get { return m_BillableAmount; }
            set { m_BillableAmount = value; }
        }


        #endregion

    }

    public class QBVehicleMileageListCollection : Collection<VehicleMileageList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all Vehicle Mileage expenses in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllVehicleMileageList(string QBFileName)
        {
            string mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehiclemileageQueryRq", QBFileName);

            List<string> mileageList = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(mileageXmlList);
            XmlFileData = mileageXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList mileageNodeList = outputXMLDoc.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());

            foreach (XmlNode MileageNodes in mileageNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //QBVehiclemileageList vehiclemileage = new QBVehiclemileageList();
                    foreach (XmlNode node in MileageNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                        {
                            if (!mileageList.Contains(node.InnerText))
                                mileageList.Add(node.InnerText);
                        }
                    }
                }
            }

            return mileageList;
        }


        public Collection<VehicleMileageList> PopulateVehicelMileageList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            string mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate);
            XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(mileageXmlList);
            XmlFileData = mileageXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList MileageNodeList = outputXMLDoc.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());
            foreach (XmlNode MileageNodes in MileageNodeList)
            {

                #region For VehicleMileage data
                if (bkWorker.CancellationPending != true)
                {
                    VehicleMileageList VehicleMileageList = new VehicleMileageList();
                    foreach (XmlNode node in MileageNodes.ChildNodes)
                    {

                        //Checking TxnID. 
                        if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                        {
                            VehicleMileageList.TxnID = node.InnerText;
                        }

                        //Checking TimeCreated list.
                        if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                        {
                            VehicleMileageList.TimeCreated = node.InnerText;
                        }

                        //Checking TimeCreated list.
                        if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                        {
                            VehicleMileageList.TimeModified = node.InnerText;
                        }

                        //Checking TripStartDate list.
                        if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                        {

                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    VehicleMileageList.TripStartDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    VehicleMileageList.TripStartDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                VehicleMileageList.TripStartDate = node.InnerText;
                            }
                        }

                        //Checking TripEndDate list.
                        if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    VehicleMileageList.TripEndDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    VehicleMileageList.TripEndDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                        }


                        //Checking OdometerStart list.
                        if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                        {
                            VehicleMileageList.OdometerStart = node.InnerText;
                        }

                        //Checking OdometerEnd list.
                        if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                        {
                            VehicleMileageList.OdometerEnd = node.InnerText;
                        }
                        //Checking TotalMiles list.
                        if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                        {
                            VehicleMileageList.TotalMiles = node.InnerText;
                        }
                        //Checking Billable Status list.
                        if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                        {
                            VehicleMileageList.BillableStatus = node.InnerText;
                        }

                        //Checking Billable Notes list.
                        if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                        {
                            VehicleMileageList.Notes = node.InnerText;
                        }

                        //Checking  VehicleRefFullName list.
                        if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                    VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                            }
                        }

                        //Checking  CustomerRefFullName list.
                        if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                    VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                            }
                        }

                        //Checking ClassRefFullName list.
                        if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                    VehicleMileageList.ClassRefFullName = childNode.InnerText;
                            }
                        }


                        //Checking ItemRefFullName  List.
                        if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                    VehicleMileageList.ItemRefFullName = childNode.InnerText;
                            }
                        }

                        //Checking StandardMileageRate list.
                        if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                        {
                            VehicleMileageList.StandardMileageRate = node.InnerText;
                        }

                        //Checking StandardMileageTotalAmount list.
                        if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                        {
                            VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                        }

                        //Checking BillableRate list.
                        if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                        {
                            VehicleMileageList.BillableRate = node.InnerText;
                        }
                        //Checking BillableAmount list.
                        if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                        {
                            VehicleMileageList.BillableAmount = node.InnerText;
                        }


                    }
                    this.Add(VehicleMileageList);
                }
                else
                { return null; }
                #endregion

            }
            return this;
        }





        //axis 12.0
        /// <summary>
        /// if no filter was selected axis 12.0
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> PopulateVehiclemileageList(string QBFileName)
        {
            #region Getting Vehicle Mileage List from QuickBooks.

            //Getting item list from QuickBooks.
            string mileageXmlList = string.Empty;
            mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehicleMileageQueryRq", QBFileName);
            VehicleMileageList VehicleMileageList;
            int i = 0;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileage = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (mileageXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfMileage.LoadXml(mileageXmlList);
                XmlFileData = mileageXmlList;

                #region Getting Vehicle Mileage Values from XML

                //Getting vehicle mileage  values from QuickBooks response.
                XmlNodeList mileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());

                foreach (XmlNode mileageNodes in mileageNodeList)
                {

                    if (bkWorker.CancellationPending != true)
                    {

                        VehicleMileageList = new VehicleMileageList();

                        #region  check data

                        foreach (XmlNode node in mileageNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }

                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }

                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }

                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }

                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }


                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }


                        #endregion
                        }
                        //Adding VehicleMileage list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileage.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> ModifiedPopulateVehicleMileageList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Mileage List from QuickBooks.

            //Getting item list from QuickBooks.
            string mileageXmlList = string.Empty;
            mileageXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            VehicleMileageList VehicleMileageList;
            int i = 0;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileage = new XmlDocument();

            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (mileageXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfMileage.LoadXml(mileageXmlList);
                XmlFileData = mileageXmlList;

                #region Getting Vehicle Mileage Values from XML

                //Getting vehicle mileage  values from QuickBooks response.

                XmlNodeList MileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());

                foreach (XmlNode MileageNodes in MileageNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        VehicleMileageList = new VehicleMileageList();

                        foreach (XmlNode node in MileageNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }

                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }
                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }
                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }
                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }

                        }
                #endregion
                        //Adding Invoice list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }
                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileage.RemoveAll();
        #endregion
            }
            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> ModifiedPopulateVehicleMileageList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Vehicle Mileage Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string mileageXmlList = string.Empty;
            mileageXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate);

            VehicleMileageList VehicleMileageList;
            int i = 0;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileage = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (mileageXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfMileage.LoadXml(mileageXmlList);
                XmlFileData = mileageXmlList;

                #region Getting Vehicle Mileage Values from XML

                //Getting vehicle mileage  values from QuickBooks response.

                XmlNodeList MileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());
                foreach (XmlNode MileageNodes in MileageNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        VehicleMileageList = new VehicleMileageList();

                        foreach (XmlNode node in MileageNodes.ChildNodes)
                        {
                            #region data
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }
                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }

                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }

                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }

                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }


                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }


                            #endregion



                        }
                        //Adding Invoice list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileage.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillList by Filter 
        /// Entity Filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> PopulateVehicleMileageListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting VehicleMileageList List from QuickBooks.

            //Getting item list from QuickBooks.
            string MileageXmlList = string.Empty;
            MileageXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("VehicleMileageQueryRq", QBFileName, entityFilter);

            VehicleMileageList VehicleMileageList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileages = new XmlDocument();


            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (MileageXmlList != string.Empty)
            {

                outputXMLDocOfMileages.LoadXml(MileageXmlList);
                XmlFileData = MileageXmlList;

                #region Getting VehicleMileageList Values from XML

                //Getting mileage values from QuickBooks response.
                XmlNodeList BillNodeList = outputXMLDocOfMileages.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());

                foreach (XmlNode BillNodes in BillNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        VehicleMileageList = new VehicleMileageList();
                        foreach (XmlNode node in BillNodes.ChildNodes)
                        {
                            #region data
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }

                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }

                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }

                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }

                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }


                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }


                            #endregion

                        }
                        //Adding vehiclemileage list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileages.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> PopulateVehicleMileageList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string mileageXmlList = string.Empty;
            mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate, entityFilter);


            VehicleMileageList VehicleMileageList;
            int i = 0;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileage = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (mileageXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfMileage.LoadXml(mileageXmlList);
                XmlFileData = mileageXmlList;

                #region Getting Vehicle Mileage Values from XML

                //Getting vehicle mileage  values from QuickBooks response.

                XmlNodeList MileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());
                foreach (XmlNode MileageNodes in MileageNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        VehicleMileageList = new VehicleMileageList();

                        foreach (XmlNode node in MileageNodes.ChildNodes)
                        {
                            #region data
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }
                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }

                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }

                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }

                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }


                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }


                            #endregion



                        }
                        //Adding Invoice list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileage.RemoveAll();
                #endregion
            }



            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<VehicleMileageList> PopulateVehicleMileageList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting VehicleMileage  List from QuickBooks.

            //Getting item list from QuickBooks.
            string mileageXmlList = string.Empty;
            mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate);

            VehicleMileageList VehicleMileageList;
            int i = 0;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfMileage = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (mileageXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfMileage.LoadXml(mileageXmlList);
                XmlFileData = mileageXmlList;

                #region Getting Vehicle Mileage Values from XML

                //Getting vehicle mileage  values from QuickBooks response.

                XmlNodeList MileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());
                foreach (XmlNode MileageNodes in MileageNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        VehicleMileageList = new VehicleMileageList();

                        foreach (XmlNode node in MileageNodes.ChildNodes)
                        {
                            #region data
                            //Checking TxnID. 
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                VehicleMileageList.TxnID = node.InnerText;
                            }

                            //Checking TimeCreated list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
                            {
                                VehicleMileageList.TimeCreated = node.InnerText;
                            }

                            //Checking TimeModified list.
                            if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
                            {
                                VehicleMileageList.TimeModified = node.InnerText;
                            }
                            //Checking TripStartDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
                            { VehicleMileageList.TripStartDate = node.InnerText; }

                            //Checking TripEndDate list.
                            if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
                            {
                                VehicleMileageList.TripEndDate = node.InnerText;
                            }
                            //Checking OdometerStart list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
                            {
                                VehicleMileageList.OdometerStart = node.InnerText;
                            }

                            //Checking OdometerEnd list.
                            if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
                            {
                                VehicleMileageList.OdometerEnd = node.InnerText;
                            }
                            //Checking TotalMiles list.
                            if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
                            {
                                VehicleMileageList.TotalMiles = node.InnerText;
                            }
                            //Checking Billable Status list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
                            {
                                VehicleMileageList.BillableStatus = node.InnerText;
                            }

                            //Checking Billable Notes list.
                            if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
                            {
                                VehicleMileageList.Notes = node.InnerText;
                            }

                            //Checking  VehicleRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.VehicleRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking  CustomerRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking ClassRefFullName list.
                            if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ClassRefFullName = childNode.InnerText;
                                }
                            }


                            //Checking ItemRefFullName  List.
                            if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
                                        VehicleMileageList.ItemRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking StandardMileageRate list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
                            {
                                VehicleMileageList.StandardMileageRate = node.InnerText;
                            }

                            //Checking StandardMileageTotalAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
                            {
                                VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
                            }

                            //Checking BillableRate list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
                            {
                                VehicleMileageList.BillableRate = node.InnerText;
                            }
                            //Checking BillableAmount list.
                            if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
                            {
                                VehicleMileageList.BillableAmount = node.InnerText;
                            }


                            #endregion



                        }
                        //Adding Invoice list.
                        this.Add(VehicleMileageList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfMileage.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        //public Collection<VehicleMileageList> PopulateVehicleMileageList(string QBFileName, DateTime FromDate, DateTime ToDate, string entityFilter, bool flag)
        //{
        //    #region Getting Bill Payments List from QuickBooks.

        //    //Getting item list from QuickBooks.
        //    string mileageXmlList = string.Empty;
        //    mileageXmlList = QBCommonUtilities.GetListFromQuickBook("VehicleMileageQueryRq", QBFileName, FromDate, ToDate, entityFilter);
        //    VehicleMileageList VehicleMileageList;
        //    int i = 0;
        //    #endregion
        //    TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
        //    BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

        //    //Create a xml document for output result.
        //    XmlDocument outputXMLDocOfMileage = new XmlDocument();

        //    //Getting current version of System. BUG(676)
        //    string countryName = string.Empty;
        //    countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

        //    if (mileageXmlList != string.Empty)
        //    {
        //        //Loading Item List into XML.
        //        outputXMLDocOfMileage.LoadXml(mileageXmlList);
        //        XmlFileData = mileageXmlList;

        //        #region Getting Vehicle Mileage Values from XML

        //        //Getting vehicle mileage  values from QuickBooks response.

        //        XmlNodeList MileageNodeList = outputXMLDocOfMileage.GetElementsByTagName(QBVehiclemileageList.VehicleMileageRet.ToString());
        //        foreach (XmlNode MileageNodes in MileageNodeList)
        //        {
        //            if (bkWorker.CancellationPending != true)
        //            {
        //                VehicleMileageList = new VehicleMileageList();

        //                foreach (XmlNode node in MileageNodes.ChildNodes)
        //                {
        //                    #region data
        //                    //Checking TxnID. 
        //                    if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
        //                    {
        //                        VehicleMileageList.TxnID = node.InnerText;
        //                    }

        //                    //Checking TimeCreated list.
        //                    if (node.Name.Equals(QBVehiclemileageList.TimeCreated.ToString()))
        //                    {
        //                        VehicleMileageList.TimeCreated = node.InnerText;
        //                    }

        //                    //Checking TimeModified list.
        //                    if (node.Name.Equals(QBVehiclemileageList.TimeModified.ToString()))
        //                    {
        //                        VehicleMileageList.TimeModified = node.InnerText;
        //                    }
        //                    //Checking TripStartDate list.
        //                    if (node.Name.Equals(QBVehiclemileageList.TripStartDate.ToString()))
        //                    { VehicleMileageList.TripStartDate = node.InnerText; }

        //                    //Checking TripEndDate list.
        //                    if (node.Name.Equals(QBVehiclemileageList.TripEndDate.ToString()))
        //                    {
        //                        VehicleMileageList.TripEndDate = node.InnerText;
        //                    }
        //                    //Checking OdometerStart list.
        //                    if (node.Name.Equals(QBVehiclemileageList.OdometerStart.ToString()))
        //                    {
        //                        VehicleMileageList.OdometerStart = node.InnerText;
        //                    }

        //                    //Checking OdometerEnd list.
        //                    if (node.Name.Equals(QBVehiclemileageList.OdometerEnd.ToString()))
        //                    {
        //                        VehicleMileageList.OdometerEnd = node.InnerText;
        //                    }
        //                    //Checking TotalMiles list.
        //                    if (node.Name.Equals(QBVehiclemileageList.TotalMiles.ToString()))
        //                    {
        //                        VehicleMileageList.TotalMiles = node.InnerText;
        //                    }
        //                    //Checking Billable Status list.
        //                    if (node.Name.Equals(QBVehiclemileageList.BillableStatus.ToString()))
        //                    {
        //                        VehicleMileageList.BillableStatus = node.InnerText;
        //                    }

        //                    //Checking Billable Notes list.
        //                    if (node.Name.Equals(QBVehiclemileageList.Notes.ToString()))
        //                    {
        //                        VehicleMileageList.Notes = node.InnerText;
        //                    }

        //                    //Checking  VehicleRefFullName list.
        //                    if (node.Name.Equals(QBVehiclemileageList.VehicleRef.ToString()))
        //                    {
        //                        foreach (XmlNode childNode in node.ChildNodes)
        //                        {
        //                            if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
        //                                VehicleMileageList.VehicleRefFullName = childNode.InnerText;
        //                        }
        //                    }

        //                    //Checking  CustomerRefFullName list.
        //                    if (node.Name.Equals(QBVehiclemileageList.CustomerRef.ToString()))
        //                    {
        //                        foreach (XmlNode childNode in node.ChildNodes)
        //                        {
        //                            if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
        //                                VehicleMileageList.CustomerRefFullName = childNode.InnerText;
        //                        }
        //                    }

        //                    //Checking ClassRefFullName list.
        //                    if (node.Name.Equals(QBVehiclemileageList.ClassRef.ToString()))
        //                    {
        //                        foreach (XmlNode childNode in node.ChildNodes)
        //                        {
        //                            if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
        //                                VehicleMileageList.ClassRefFullName = childNode.InnerText;
        //                        }
        //                    }


        //                    //Checking ItemRefFullName  List.
        //                    if (node.Name.Equals(QBVehiclemileageList.ItemRef.ToString()))
        //                    {
        //                        foreach (XmlNode childNode in node.ChildNodes)
        //                        {
        //                            if (childNode.Name.Equals(QBVehiclemileageList.FullName.ToString()))
        //                                VehicleMileageList.ItemRefFullName = childNode.InnerText;
        //                        }
        //                    }

        //                    //Checking StandardMileageRate list.
        //                    if (node.Name.Equals(QBVehiclemileageList.StandardMileageRate.ToString()))
        //                    {
        //                        VehicleMileageList.StandardMileageRate = node.InnerText;
        //                    }

        //                    //Checking StandardMileageTotalAmount list.
        //                    if (node.Name.Equals(QBVehiclemileageList.StandardMileageTotalAmount.ToString()))
        //                    {
        //                        VehicleMileageList.StandardMileageTotalAmount = node.InnerText;
        //                    }

        //                    //Checking BillableRate list.
        //                    if (node.Name.Equals(QBVehiclemileageList.BillableRate.ToString()))
        //                    {
        //                        VehicleMileageList.BillableRate = node.InnerText;
        //                    }
        //                    //Checking BillableAmount list.
        //                    if (node.Name.Equals(QBVehiclemileageList.BillableAmount.ToString()))
        //                    {
        //                        VehicleMileageList.BillableAmount = node.InnerText;
        //                    }


        //                    #endregion
        //                }
        //                //Adding Invoice list.
        //                this.Add(VehicleMileageList);
        //            }
        //            else
        //            { return null; }
        //        }

        //        //Removing all the references from OutPut Xml document.
        //        outputXMLDocOfMileage.RemoveAll();
        //        #endregion
        //    }

          
        //    //Returning object.
        //    return this;
        //}


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        //#endregion
    }
}
