// ===============================================================================
// 
// Sale3sOrderList.cs
//
// This file contains the implementations of the QuickBooks Customer request methods and
// Properties.
//
// Developed By : K.Gouraw
// Date : 23/02/2009
// Modified By : Modify by K. Gouraw
// Date : 17/03/2009
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
   public class QBCustomerList:BaseCustomerList
    {
        #region Public Properties

       public string ListId
       {
           get
           { return m_customerListId; }
           set
           {
               m_customerListId = value;
           }
       }

        public string Name
        {
            get
            {
                return m_customerName;
            }
            set
            {
                if (value.Length > 60)
                    m_customerName = value.Remove(60, (value.Length - 60));
                else
                    m_customerName = value;
            }
        }

       public string FullName
       {
           get
           {
               return m_CustomerFullName;
           }
           set
           {
               if (value.Length > 209)
                   m_CustomerFullName = value.Remove(209, (value.Length - 209));
                else
                   m_CustomerFullName = value;
           }
       }

        public string ComapanyName
        {
            get
            { return m_companyName; }
            set
            {
                if (value.Length > 60)
                    m_companyName = value.Remove(60, (value.Length - 60));
                else
                    m_companyName = value;
            }
        }

        public string Contact
        {
            get
            { return m_contactName; }
            set
            {
                if (value.Length > 30)
                    m_contactName = value.Remove(30, (value.Length - 30));
                else
                    m_contactName = value;
            }
        }
        public string Phone
        {
            get
            { return m_phone; }
            set
            {
                if (value.Length > 20)
                    m_phone = value.Remove(20, (value.Length - 20));
               else
                    m_phone = value; 
            }
        }


        public string BillAddr1
        {
            get
            { return m_addressLine1; }
            set
            {
                if (value.Length > 40)
                    m_addressLine1 = value.Remove(40, (value.Length - 40));
                else
                    m_addressLine1 = value;
            }
        }


        public string BillAddr2
        {
            get
            { return m_addressLine2; }
            set
            {
                if (value.Length > 40)
                    m_addressLine2 = value.Remove(40, (value.Length - 40));
                else
                    m_addressLine2 = value; 
            }
        }
       
        public string BillAddr4
        {
            get
            { return m_addressLine4; }
            set
            {
                if (value.Length > 40)
                    m_addressLine4 = value.Remove(40, (value.Length - 40));
                else
                    m_addressLine4 = value;
            }
        }
        public string BillAddr5
        {
            get
            { return m_addressLine5; }
            set
            {
                if (value.Length > 40)
                    m_addressLine5 = value.Remove(40, (value.Length - 40));
                else
                    m_addressLine5 = value;
            }
        }
        public string City
        {
            get { return m_city; }
            set 
            {
                if (value.Length > 40)
                    m_city = value.Remove(40, (value.Length - 40));
                else
                    m_city = value; 
            }
        }
        public string State
        {
            get { return m_state; }
            set 
            {
                if (value.Length > 21)
                    m_state = value.Remove(21, (value.Length - 21));
                else
                    m_state = value; 
            }
        }
       public string Sublevel
       {
           get { return m_Sublevel; }
           set
           {
               m_Sublevel = value;
           }
       }
        public string PostalCode
        {
            get
            { return m_postalCode; }
            set
            {
                if (value.Length > 9)
                    m_postalCode = value.Remove(9, (value.Length - 9));
                else
                    m_postalCode = value; 
            }
        }
        public string Country
        {
            get { return m_country; }
            set 
            {
                if (value.Length > 31)
                    m_country = value.Remove(31, (value.Length - 31));
                else
                    m_country = value;
            }
        }

       
       public string CustomerIsActive
       {
           get { return m_CustomerIsActive; }
           set
           {

               m_CustomerIsActive = value;
           }
       }
       public string ParentRefFullName
       {
           get { return m_ParentRefFullName; }
           set
           {

               m_ParentRefFullName = value;
           }
       }
       public string Salutation
       {
           get { return m_Salutation; }
           set
           {

               m_Salutation = value;
           }
       }
       public string FirstName
       {
           get { return m_FirstName; }
           set
           {

               m_FirstName = value;
           }
       }
       public string MiddleName
       {
           get { return m_MiddleName; }
           set
           {

               m_MiddleName = value;
           }
       }
       public string LastName
       {
           get { return m_LastName; }
           set
           {

               m_LastName = value;
           }
       }
       public string AddressLine3
       {
           get { return m_addressLine3; }
           set
           {

               m_addressLine3 = value;
           }
       }
       public string ShipAddr1
       {
           get { return m_ShipAddr1; }
           set
           {

               m_ShipAddr1 = value;
           }
       }
       public string ShipAddr2
       {
           get { return m_ShipAddr2; }
           set
           {

               m_ShipAddr2 = value;
           }
       }
       public string ShipCity
       {
           get { return m_ShipCity; }
           set
           {

               m_ShipCity = value;
           }
       }
       public string ShipState
       {
           get { return m_ShipState; }
           set
           {

               m_ShipState = value;
           }
       }
       public string ShipPostalCode
       {
           get { return m_ShipPostalCode; }
           set
           {

               m_ShipPostalCode = value;
           }
       }
       public string ShipCountry
       {
           get { return m_ShipCountry; }
           set
           {

               m_ShipCountry = value;
           }

       }
       public string ShipNote
       {
           get { return m_ShipNote; }
           set
           {

               m_ShipNote = value;
           }
       }
       public string Note
       {
           get { return m_Note; }
           set
           {

               m_Note = value;
           }
       }
       public string CustomerTypeRefFullName
       {
           get { return m_CustomerTypeRefFullName; }
           set
           {

               m_CustomerTypeRefFullName = value;
           }
       }
       public string TermsRefFullName
       {
           get { return m_TermsRefFullName; }
           set
           {

               m_TermsRefFullName = value;
           }
       }
       public string SalesRepRefFullName
       {
           get { return m_SalesRepRefFullName; }
           set
           {

               m_SalesRepRefFullName = value;
           }
       }
       public string Balance
       {
           get { return m_Balance; }
           set
           {

               m_Balance = value;
           }
       }
       public string TotalBalance
       {
           get { return m_TotalBalance; }
           set
           {

               m_TotalBalance = value;
           }
       }
       public string SalesTaxCodeRefFullName
       {
           get { return m_SalesTaxCodeRefFullName; }
           set
           {

               m_SalesTaxCodeRefFullName = value;
           }
       }
       public string ItemSalesTaxRefFullName
       {
           get { return m_ItemSalesTaxRefFullName; }
           set
           {

               m_ItemSalesTaxRefFullName = value;
           }
       }

       //Axis 10.0
       public string CreditCardNumber
       {
           get { return m_CreditCardNumber; }
           set
           {

               m_CreditCardNumber = value;
           }
       }


       public int ExpirationMonth
       {
           get { return m_ExpirationMonth; }
           set
           {

               m_ExpirationMonth = value;
           }
       }

       public int ExpirationYear
       {
           get { return m_ExpirationYear; }
           set
           {

               m_ExpirationYear = value;
           }
       }

       public string NameOnCard
       {
           get { return m_NameOnCard; }
           set
           {

               m_NameOnCard = value;
           }
       }

       public string CreditCardAddress
       {
           get { return m_CreditCardAddress; }
           set
           {

               m_CreditCardAddress = value;
           }
       }


       public string CreditCardPostalCode
       {
           get { return m_CreditCardPostalCode; }
           set
           {

               m_CreditCardPostalCode = value;
           }
       }

       
       public string JobStatus
       {
           get { return m_JobStatus; }
           set
           {

               m_JobStatus = value;
           }
       }
       public string JobStartDate
       {
           get { return m_JobStartDate; }
           set
           {

               m_JobStartDate = value;
           }
       }
       public string JobProjectedEndDate
       {
           get { return m_JobProjectedEndDate; }
           set
           {

               m_JobProjectedEndDate = value;
           }
       }
       public string JobEndDate
       {
           get { return m_JobEndDate; }
           set
           {

               m_JobEndDate = value;
           }
       }
       public string JobTypeRefFullName
       {
           get { return m_JobTypeRefFullName; }
           set
           {

               m_JobTypeRefFullName = value;
           }
       }
       public string Notes
       {
           get { return m_Notes; }
           set
           {

               m_Notes = value;
           }
       }
       public string PriceLevelRefFullName
       {
           get { return m_PriceLevelRefFullName; }
           set
           {

               m_PriceLevelRefFullName = value;
           }
       }
       public string CurrencyRefFullName
       {
           get { return m_CurrencyRefFullName; }
           set
           {

               m_CurrencyRefFullName = value;
           }
       }

       public string AltPhone
       {
           get
           { return m_AltPhone; }
           set
           {
               if (value.Length > 20)
                   m_AltPhone = value.Remove(20, (value.Length - 20));
               else
                   m_AltPhone = value;
           }
       }
       public string Fax
       {
           get
           {
               return m_Fax;
           }
           set
           {
               if (value.Length > 20)
                   m_Fax = value.Remove(20, (value.Length - 20));
               else
                   m_Fax = value;
           }
       }
       public string Email
       {
           get { return m_emailAddress; }
           set
           {
               if (value.Length > 100)
                   m_emailAddress = value.Remove(100, (value.Length - 100));
               else
                   m_emailAddress = value;
           }
       }
       public string AltContact
       {
           get
           {
               return m_AltContact;
           }
           set
           {
               if (value.Length > 30)
                   m_AltContact = value.Remove(30, (value.Length - 30));
               else
               m_AltContact = value;
           }
       }
       public string ResaleNumber
       { 
           get 
           {
               return m_ResaleNumber;
           } 
           set 
           {
               if (value.Length > 15)
                   m_ResaleNumber = value.Remove(15, (value.Length - 15));
               else
                   m_ResaleNumber = value;
           } 
       }
       public string AccountNumber
       {
           get
           {
               return m_AccountNumber;
           }
           set
           {
               m_AccountNumber = value;             
           }
       }
       public string CreditLimit
       {
           get
           {
               return m_CreditLimit;
           }
           set
           {
               m_CreditLimit = value;
           }
       }
       public string PreferredPaymentMethodRefFullName
       {
           get
           {
               return m_PreferredPaymentMethodRefFullName;
           }
           set
           {
               if (value.Length > 30)
                   m_PreferredPaymentMethodRefFullName = value.Remove(30, (value.Length - 30));
               else
                   m_PreferredPaymentMethodRefFullName = value;
           }
       }

       //Axis 8.0
       public string[] DataExtName = new string[550];
       public string[] DataExtValue = new string[550];

        #endregion

   }

    public class QBCustomerListCollection : Collection<QBCustomerList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all customer in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllCustomerList(string QBFileName)
        {
            string customerXmlList = QBCommonUtilities.GetListFromQuickBook("CustomerQueryRq", QBFileName);

            List<string> customerList=new List<string>();
            List<string> customerList1 = new List<string>();


            XmlDocument outputXMLDoc = new XmlDocument();
            
            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBXmlCustomerList.CustomerRet.ToString());

            foreach (XmlNode CustNodes in CustNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBCustomerList customer = new QBCustomerList();
                    foreach (XmlNode node in CustNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                        {
                            if (!customerList.Contains(node.InnerText))
                            {
                                customerList.Add(node.InnerText);
								//Bug No. 419                                
                                string name = node.InnerText;
                                customerList1.Add(name);

                            }
                        }
                    }
                }
            }
            CommonUtilities.GetInstance().CustomerListWithType = customerList1;

            //bug  463 free memory;
             customerXmlList = null;
             CustNodeList = null;
             XmlFileData = null;
             outputXMLDoc = null;
            return customerList1;

        }

        public Collection<QBCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            string customerXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBookByDate("CustomerQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("CustomerQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBXmlCustomerList.CustomerRet.ToString());
            foreach (XmlNode CustNodes in CustNodeList)
            {
                //Axis 8.0 Reset value of i to 0.
                i = 0;
                if (bkWorker.CancellationPending != true)
                {
                    QBCustomerList customer = new QBCustomerList();
                    foreach (XmlNode node in CustNodes.ChildNodes)
                    {

                        if (node.Name.Equals(QBXmlCustomerList.ListID.ToString()))
                            customer.ListId = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Name.ToString()))
                            customer.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                            customer.FullName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CompanyName.ToString()))
                            customer.ComapanyName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Contact.ToString()))
                            customer.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Phone.ToString()))
                            customer.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.IsActive.ToString()))
                            customer.CustomerIsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Sublevel.ToString()))
                            customer.Sublevel = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Salutation.ToString()))
                            customer.Salutation = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FirstName.ToString()))
                            customer.FirstName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.MiddleName.ToString()))
                            customer.MiddleName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.LastName.ToString()))
                            customer.LastName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Balance.ToString()))
                            customer.Balance = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.TotalBalance.ToString()))
                            customer.TotalBalance = node.InnerText;
                        ////Axis 10.0   

                        // bug 1428

                        if (node.Name.Equals(QBXmlCustomerList.CreditCardInfo.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardNumber.ToString()))
                                    customer.CreditCardNumber = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationMonth.ToString()))
                                    customer.ExpirationMonth = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationYear.ToString()))
                                    customer.ExpirationYear = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.NameOnCard.ToString()))
                                    customer.NameOnCard = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardAddress.ToString()))
                                    customer.CreditCardAddress = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardPostalCode.ToString()))
                                    customer.CreditCardPostalCode = childNode.InnerText;

                            }
                        }
                        //ends

                        if (node.Name.Equals(QBXmlCustomerList.JobStatus.ToString()))
                            customer.JobStatus = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobStartDate.ToString()))
                            customer.JobStartDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobProjectedEndDate.ToString()))
                            customer.JobProjectedEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobEndDate.ToString()))
                            customer.JobEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Notes.ToString()))
                            customer.Notes = node.InnerText;

                        if (node.Name.Equals(QBXmlCustomerList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ParentRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CustomerTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CustomerTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.TermsRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.TermsRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesRepRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesRepRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesTaxCodeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesTaxCodeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ItemSalesTaxRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ItemSalesTaxRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.JobTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.JobTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.PriceLevelRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PriceLevelRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CurrencyRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.BillAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.BillAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.BillAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr3.ToString()))
                                    customer.AddressLine3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.City = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.State = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.PostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.Country = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.Note = childNode.InnerText;
                                
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ShipAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.ShipAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.ShipAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.ShipCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.ShipState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.ShipPostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.ShipCountry = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.ShipNote = childNode.InnerText;

                            }

                        }
                        if (node.Name.Equals(QBXmlCustomerList.AltPhone.ToString()))
                            customer.AltPhone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Fax.ToString()))
                            customer.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Email.ToString()))
                            customer.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AltContact.ToString()))
                            customer.AltContact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.ResaleNumber.ToString()))
                            customer.ResaleNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AccountNumber.ToString()))
                            customer.AccountNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CreditLimit.ToString()))
                            customer.CreditLimit = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.PreferredPaymentMethodRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PreferredPaymentMethodRefFullName = childNode.InnerText;
                            }
                        }

                        //Axis 8.0 for Custom Field
                        if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                        {

                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                customer.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    customer.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }
                    }
                    this.Add(customer);
                }
                else
                { return null; }
            }

            return this;
        }

        /// <summary>
        /// If no filter selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBCustomerList> PopulateCustomerList(string QBFileName, bool inActiveFilter)
        {
            string customerXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBook("CustomerQueryRq", QBFileName);
            }
            else
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("CustomerQueryRq", QBFileName);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBXmlCustomerList.CustomerRet.ToString());
            foreach (XmlNode CustNodes in CustNodeList)
            {
                //Axis 8.0 Reset value of i to 0.
                i = 0;
                #region For Customer data
                if (bkWorker.CancellationPending != true)
                {
                    QBCustomerList customer = new QBCustomerList();
                    foreach (XmlNode node in CustNodes.ChildNodes)
                    {

                        if (node.Name.Equals(QBXmlCustomerList.ListID.ToString()))
                            customer.ListId = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Name.ToString()))
                            customer.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                            customer.FullName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CompanyName.ToString()))
                            customer.ComapanyName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Contact.ToString()))
                            customer.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Phone.ToString()))
                            customer.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.IsActive.ToString()))
                            customer.CustomerIsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Sublevel.ToString()))
                            customer.Sublevel = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Salutation.ToString()))
                            customer.Salutation = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FirstName.ToString()))
                            customer.FirstName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.MiddleName.ToString()))
                            customer.MiddleName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.LastName.ToString()))
                            customer.LastName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Balance.ToString()))
                            customer.Balance = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.TotalBalance.ToString()))
                            customer.TotalBalance = node.InnerText;

                        // bug 1428

                        if (node.Name.Equals(QBXmlCustomerList.CreditCardInfo.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardNumber.ToString()))
                                    customer.CreditCardNumber = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationMonth.ToString()))
                                    customer.ExpirationMonth = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationYear.ToString()))
                                    customer.ExpirationYear = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.NameOnCard.ToString()))
                                    customer.NameOnCard = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardAddress.ToString()))
                                    customer.CreditCardAddress = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardPostalCode.ToString()))
                                    customer.CreditCardPostalCode = childNode.InnerText;

                            }
                        }
                        //ends


                        if (node.Name.Equals(QBXmlCustomerList.JobStatus.ToString()))
                            customer.JobStatus = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobStartDate.ToString()))
                            customer.JobStartDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobProjectedEndDate.ToString()))
                            customer.JobProjectedEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobEndDate.ToString()))
                            customer.JobEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Notes.ToString()))
                            customer.Notes = node.InnerText;

                        if (node.Name.Equals(QBXmlCustomerList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ParentRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CustomerTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CustomerTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.TermsRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.TermsRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesRepRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesRepRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesTaxCodeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesTaxCodeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ItemSalesTaxRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ItemSalesTaxRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.JobTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.JobTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.PriceLevelRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PriceLevelRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CurrencyRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.BillAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.BillAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.BillAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr3.ToString()))
                                    customer.AddressLine3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.City = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.State = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.PostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.Country = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.Note = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ShipAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.ShipAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.ShipAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.ShipCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.ShipState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.ShipPostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.ShipCountry = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.ShipNote = childNode.InnerText;
                            }

                        }
                        if (node.Name.Equals(QBXmlCustomerList.AltPhone.ToString()))
                            customer.AltPhone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Fax.ToString()))
                            customer.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Email.ToString()))
                            customer.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AltContact.ToString()))
                            customer.AltContact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.ResaleNumber.ToString()))
                            customer.ResaleNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AccountNumber.ToString()))
                            customer.AccountNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CreditLimit.ToString()))
                            customer.CreditLimit = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.PreferredPaymentMethodRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PreferredPaymentMethodRefFullName = childNode.InnerText;
                            }
                        }

                        //Axis 8.0 for Custom Field
                        if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                        {

                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                customer.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    customer.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }
                    }
                    this.Add(customer);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }

        public Collection<QBCustomerList> PopulateCustomerList(string QBFileName, DateTime FromDate, DateTime ToDate,bool inActiveFilter)
        {
            string customerXmlList = string.Empty;
            if (inActiveFilter == false)//bug no.395
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBookByDate("CustomerQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                customerXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("CustomerQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();
            
            outputXMLDoc.LoadXml(customerXmlList);
            XmlFileData = customerXmlList;
            //Axis 8.0; Declare integer i
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList CustNodeList = outputXMLDoc.GetElementsByTagName(QBXmlCustomerList.CustomerRet.ToString());
            foreach (XmlNode CustNodes in CustNodeList)
            {
                //Axis 8.0 Reset value of i to 0.
                i = 0;
                #region For Customer data
                if (bkWorker.CancellationPending != true)
                {
                    QBCustomerList customer = new QBCustomerList();
                    foreach (XmlNode node in CustNodes.ChildNodes)
                    {

                        if (node.Name.Equals(QBXmlCustomerList.ListID.ToString()))
                            customer.ListId = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Name.ToString()))
                            customer.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                            customer.FullName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CompanyName.ToString()))
                            customer.ComapanyName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Contact.ToString()))
                            customer.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Phone.ToString()))
                            customer.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.IsActive.ToString()))
                            customer.CustomerIsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Sublevel.ToString()))
                            customer.Sublevel = node.InnerText;                  
                        if (node.Name.Equals(QBXmlCustomerList.Salutation.ToString()))
                            customer.Salutation = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.FirstName.ToString()))
                            customer.FirstName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.MiddleName.ToString()))
                            customer.MiddleName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.LastName.ToString()))
                            customer.LastName = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Balance.ToString()))
                            customer.Balance = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.TotalBalance.ToString()))
                            customer.TotalBalance = node.InnerText;

                        // bug 1428

                        if (node.Name.Equals(QBXmlCustomerList.CreditCardInfo.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardNumber.ToString()))
                                    customer.CreditCardNumber = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationMonth.ToString()))
                                    customer.ExpirationMonth = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.ExpirationYear.ToString()))
                                    customer.ExpirationYear = int.Parse(childNode.InnerText.ToString());
                                if (childNode.Name.Equals(QBXmlCustomerList.NameOnCard.ToString()))
                                    customer.NameOnCard = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardAddress.ToString()))
                                    customer.CreditCardAddress = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.CreditCardPostalCode.ToString()))
                                    customer.CreditCardPostalCode = childNode.InnerText;

                            }
                        }
                        //ends


                        if (node.Name.Equals(QBXmlCustomerList.JobStatus.ToString()))
                            customer.JobStatus = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobStartDate.ToString()))
                            customer.JobStartDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobProjectedEndDate.ToString()))
                            customer.JobProjectedEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.JobEndDate.ToString()))
                            customer.JobEndDate = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Notes.ToString()))
                            customer.Notes = node.InnerText;
                     
                        if (node.Name.Equals(QBXmlCustomerList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ParentRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CustomerTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CustomerTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.TermsRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.TermsRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesRepRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesRepRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.SalesTaxCodeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.SalesTaxCodeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ItemSalesTaxRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.ItemSalesTaxRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.JobTypeRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.JobTypeRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.PriceLevelRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PriceLevelRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.CurrencyRefFullName = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.BillAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.BillAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.BillAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr3.ToString()))
                                    customer.AddressLine3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.City = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.State = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.PostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.Country = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.Note = childNode.InnerText;
                            }
                        }
                        if (node.Name.Equals(QBXmlCustomerList.ShipAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr1.ToString()))
                                    customer.ShipAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Addr2.ToString()))
                                    customer.ShipAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.City.ToString()))
                                    customer.ShipCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.State.ToString()))
                                    customer.ShipState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.PostalCode.ToString()))
                                    customer.ShipPostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Country.ToString()))
                                    customer.ShipCountry = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlCustomerList.Note.ToString()))
                                    customer.ShipNote = childNode.InnerText;
                            }

                        }
                        if (node.Name.Equals(QBXmlCustomerList.AltPhone.ToString()))
                            customer.AltPhone = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Fax.ToString()))
                            customer.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.Email.ToString()))
                            customer.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AltContact.ToString()))
                            customer.AltContact = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.ResaleNumber.ToString()))
                            customer.ResaleNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.AccountNumber.ToString()))
                            customer.AccountNumber = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.CreditLimit.ToString()))
                            customer.CreditLimit = node.InnerText;
                        if (node.Name.Equals(QBXmlCustomerList.PreferredPaymentMethodRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlCustomerList.FullName.ToString()))
                                    customer.PreferredPaymentMethodRefFullName = childNode.InnerText;
                            }
                        }

                        //Axis 8.0 for Custom Field
                        
                        if (node.Name.Equals(QBXmlCustomerList.DataExtRet.ToString()))
                        {

                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                customer.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    customer.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }

                    }
                    this.Add(customer);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        #endregion


        
    }
}
