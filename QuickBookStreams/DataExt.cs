﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookStreams
{
     [XmlRootAttribute("DataExt", Namespace = "", IsNullable = false)]
  public class DataExt
    {
        private string m_OwnerID;
        private string m_DataExtName;
        private string m_DataExtValue;

        public DataExt()
        {

        }

        #region Public Properties

        public string OwnerID
        {
            get { return this.m_OwnerID; }
            set { this.m_OwnerID = value; }
        }

        public string DataExtName
        {
            get { return this.m_DataExtName; }
            set { this.m_DataExtName = value; }
        }

        public string DataExtValue
        {
            get { return this.m_DataExtValue; }
            set { this.m_DataExtValue = value; }
        }
        #endregion
    }
}
