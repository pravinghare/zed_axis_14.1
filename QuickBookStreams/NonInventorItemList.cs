// ===============================================================================
// 
// NonInventoryItemList.cs
//
// This file contains the implementations of the QuickBooks NonInventoryItem request methods and
// Properties.
//
// Developed By : Sandeep Patil.
// Date : 27/02/2009
// Modified By :K.Gouraw
// Date : 27/06/2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    /// <summary>
    /// This class provides properties and methods of QuickBooks Item List.
    /// </summary>
    public class QBNonInventoryItemList : BaseNonInventoryItemList
    {
        #region Public Properties

        public string ListID
        {
            get { return m_ListId; }
            set { m_ListId = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }
        
        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }

        }


        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }

        }

        /// <summary>
        /// Get and Set BarCodeValue
        /// </summary>
        public string BarCodeValue
        {
            get { return m_BarCodeValue; }
            set { m_BarCodeValue = value; }
        }


        public string ParentRefFullName
        {
            get
            {
                return m_ParentRefFullName;
            }
            set
            {
                m_ParentRefFullName = value;
            }
        }


        public string Sublevel
        {
            get { return m_Sublevel; }
            set { m_Sublevel = value; }
        }


        public string ManufacturerPartNumber
        {
            get { return m_ManufacturerPartNumber; }
            set { m_ManufacturerPartNumber = value; }
        }

        public string UnitOfMeasureSetRefFullName
        {
            get
            { return m_UnitOfMeasureSetRefFullName; }
            set
            {
                if (value.Length > 20)
                    m_UnitOfMeasureSetRefFullName = value.Remove(20, (value.Length - 20));
                else
                    m_UnitOfMeasureSetRefFullName = value;
            }
        }

        public string SalesTaxCodeRefFullName
        {
            get
            {
                return m_SalesTaxCodeRefFullName;
            }
            set
            {
                m_SalesTaxCodeRefFullName = value;
            }
        }

        public string Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public string Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }

        public string AccountRefFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }

        public string IncomeAccountRefFullName
        {
            get { return m_IncomeAccountRefFullName; }
            set { m_IncomeAccountRefFullName = value; }
        }

        public string PurchaseDesc
        {
            get { return m_PurchaseDesc; }
            set { m_PurchaseDesc = value; }
        }

        public string PurchaseCost
        {
            get { return m_PurchaseCost; }
            set { m_PurchaseCost = value; }

        }

        public string ExpenseAccountRefFullName
        {
            get { return m_ExpenseAccountRefFullName; }
            set { m_ExpenseAccountRefFullName = value; }
        }

        public string PrefVendorRefFullName
        {
            get { return m_PrefVendorRefFullName; }
            set { m_PrefVendorRefFullName = value; }
        }

        //public string DataExtName
        //{
        //    get { return m_DataExtName; }
        //    set { m_DataExtName = value; }
        //}

        //public string DataExtType
        //{
        //    get { return m_DatExtType; }
        //    set { m_DatExtType = value; }
        //}
        //public string DataExtValue
        //{
        //    get { return m_DatExtValue; }
        //    set { m_DatExtValue = value; }
        //}

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];


      

       
        #endregion
    }

    /// <summary>
    /// Collection class used for getting NonInventoryItem List from QuickBooks.
    /// </summary>
    public class QBNonInventoryItemListCollection : Collection<QBNonInventoryItemList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBNonInventoryItemList> PopulateNonInventoryItemList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting NonInventoryItem list from QuickBooks.
            string NonInventoryItemList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBookByDate("ItemNonInventoryQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemNonInventoryQueryRq", QBFileName, FromDate, ToDate);
            }
           
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBNonInventoryItemList NonInventoryItem;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfNonInventoryItem = new XmlDocument();

            if (NonInventoryItemList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfNonInventoryItem.LoadXml(NonInventoryItemList);
                XmlFileData = NonInventoryItemList;

                #region Getting NonInventoryItem Inventory Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList NonInventoryItemInventoryList = outputXMLDocOfNonInventoryItem.GetElementsByTagName(QBXmlNonInventoryItemList.ItemNonInventoryRet.ToString());

                foreach (XmlNode NonInventoryItemInventoryNodes in NonInventoryItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        NonInventoryItem = new QBNonInventoryItemList();
                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in NonInventoryItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlNonInventoryItemList.Name.ToString()))
                                NonInventoryItem.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ListID.ToString()))
                                NonInventoryItem.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeCreated.ToString()))
                                NonInventoryItem.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeModified.ToString()))
                                NonInventoryItem.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                NonInventoryItem.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.BarCodeValue.ToString()))
                                NonInventoryItem.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.ParentRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.Sublevel.ToString()))
                                NonInventoryItem.Sublevel = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ManufacturerPartNumber.ToString()))
                                NonInventoryItem.ManufacturerPartNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Desc.ToString()))
                                        NonInventoryItem.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Price.ToString()))
                                        NonInventoryItem.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }


                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesDesc.ToString()))
                                        NonInventoryItem.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesPrice.ToString()))
                                        NonInventoryItem.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseDesc.ToString()))
                                        NonInventoryItem.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseCost.ToString()))
                                        NonInventoryItem.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtValue.ToString()))
                            //            NonInventoryItem.DataExtValue = childNode.InnerText;

                            //    }

                            //}
                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtName.ToString()))
                            //            NonInventoryItem.DataExtName = childNode.InnerText;

                            //    }

                            //}

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    NonInventoryItem.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        NonInventoryItem.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(NonInventoryItem);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfNonInventoryItem.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBNonInventoryItemList> PopulateNonInventoryItemList(string QBFileName, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting NonInventoryItem list from QuickBooks.
            string NonInventoryItemList = string.Empty;
           
            if (inActiveFilter == false)//bug no. 395
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBook("ItemNonInventoryQueryRq", QBFileName);
            }
            else
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBookForInactive("ItemNonInventoryQueryRq", QBFileName);
            }
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBNonInventoryItemList NonInventoryItem;
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfNonInventoryItem = new XmlDocument();

            if (NonInventoryItemList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfNonInventoryItem.LoadXml(NonInventoryItemList);
                XmlFileData = NonInventoryItemList;

                #region Getting NonInventoryItem Inventory Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList NonInventoryItemInventoryList = outputXMLDocOfNonInventoryItem.GetElementsByTagName(QBXmlNonInventoryItemList.ItemNonInventoryRet.ToString());

                foreach (XmlNode NonInventoryItemInventoryNodes in NonInventoryItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        NonInventoryItem = new QBNonInventoryItemList();
                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in NonInventoryItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlNonInventoryItemList.Name.ToString()))
                                NonInventoryItem.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ListID.ToString()))
                                NonInventoryItem.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeCreated.ToString()))
                                NonInventoryItem.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeModified.ToString()))
                                NonInventoryItem.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                NonInventoryItem.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.BarCodeValue.ToString()))
                                NonInventoryItem.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.ParentRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlNonInventoryItemList.Sublevel.ToString()))
                                NonInventoryItem.Sublevel = node.InnerText;



                            if (node.Name.Equals(QBXmlNonInventoryItemList.ManufacturerPartNumber.ToString()))
                                NonInventoryItem.ManufacturerPartNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Desc.ToString()))
                                        NonInventoryItem.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Price.ToString()))
                                        NonInventoryItem.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }


                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesDesc.ToString()))
                                        NonInventoryItem.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesPrice.ToString()))
                                        NonInventoryItem.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseDesc.ToString()))
                                        NonInventoryItem.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseCost.ToString()))
                                        NonInventoryItem.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtValue.ToString()))
                            //            NonInventoryItem.DataExtValue = childNode.InnerText;

                            //    }

                            //}
                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtName.ToString()))
                            //            NonInventoryItem.DataExtName = childNode.InnerText;

                            //    }

                            //}

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    NonInventoryItem.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        NonInventoryItem.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(NonInventoryItem);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfNonInventoryItem.RemoveAll();
            }
            //Returning object.
            return this;
        }
     
        /// <summary>
        /// This method is used for adding various NonInventoryItem list into QuickBooks NonInventoryItem class.
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBNonInventoryItemList> PopulateNonInventoryItemList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting NonInventoryItem list from QuickBooks.
            string NonInventoryItemList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBookByDate("ItemNonInventoryQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                NonInventoryItemList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemNonInventoryQueryRq", QBFileName, FromDate, ToDate);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBNonInventoryItemList NonInventoryItem;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfNonInventoryItem = new XmlDocument();

            if (NonInventoryItemList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfNonInventoryItem.LoadXml(NonInventoryItemList);
                XmlFileData = NonInventoryItemList;

                #region Getting NonInventoryItem Inventory Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList NonInventoryItemInventoryList = outputXMLDocOfNonInventoryItem.GetElementsByTagName(QBXmlNonInventoryItemList.ItemNonInventoryRet.ToString());

                foreach (XmlNode NonInventoryItemInventoryNodes in NonInventoryItemInventoryList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        NonInventoryItem = new QBNonInventoryItemList();
                        //Getting node list of NonInventoryItem Inventory.
                        foreach (XmlNode node in NonInventoryItemInventoryNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlNonInventoryItemList.Name.ToString()))
                                NonInventoryItem.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ListID.ToString()))
                                NonInventoryItem.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeCreated.ToString()))
                                NonInventoryItem.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.TimeModified.ToString()))
                                NonInventoryItem.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                NonInventoryItem.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.BarCodeValue.ToString()))
                                NonInventoryItem.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.ParentRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlNonInventoryItemList.Sublevel.ToString()))
                                NonInventoryItem.Sublevel = node.InnerText;



                            if (node.Name.Equals(QBXmlNonInventoryItemList.ManufacturerPartNumber.ToString()))
                                NonInventoryItem.ManufacturerPartNumber = node.InnerText;

                            if (node.Name.Equals(QBXmlNonInventoryItemList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                        NonInventoryItem.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Desc.ToString()))
                                        NonInventoryItem.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.Price.ToString()))
                                        NonInventoryItem.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }


                            if (node.Name.Equals(QBXmlNonInventoryItemList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesDesc.ToString()))
                                        NonInventoryItem.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.SalesPrice.ToString()))
                                        NonInventoryItem.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseDesc.ToString()))
                                        NonInventoryItem.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PurchaseCost.ToString()))
                                        NonInventoryItem.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlNonInventoryItemList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlNonInventoryItemList.FullName.ToString()))
                                                NonInventoryItem.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtValue.ToString()))
                            //            NonInventoryItem.DataExtValue = childNode.InnerText;

                            //    }

                            //}
                            //if (node.Name.Equals(QBXmlNonInventoryItemList.DataExtRet.ToString()))
                            //{
                            //    foreach (XmlNode childNode in node.ChildNodes)
                            //    {
                            //        if (childNode.Name.Equals(QBXmlNonInventoryItemList.DataExtName.ToString()))
                            //            NonInventoryItem.DataExtName = childNode.InnerText;

                            //    }

                            //}

                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    NonInventoryItem.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        NonInventoryItem.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(NonInventoryItem);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfNonInventoryItem.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }       
    }
}
