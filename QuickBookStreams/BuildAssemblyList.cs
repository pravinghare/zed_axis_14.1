﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.ComponentModel;
using DataProcessingBlocks;
using QuickBookEntities;

namespace Streams
{
    public class BuildAssemblyList : BaseBuildAssemblyList
    {

        #region Public Methods

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string ItemInventoryAssemblyRefFullName
        {
            get { return m_ItemInventoryAssemblyRefFullName; }
            set { m_ItemInventoryAssemblyRefFullName = value; }
        }

        public string []FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public bool IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }

        public string QuantityCanBuild
        {
            get { return m_QuantityCanBuild; }
            set { m_QuantityCanBuild = value; }
        }

        public string QuantityOnHand
        {
            get { return m_QuantityOnHand; }
            set { m_QuantityOnHand = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string QuantityOnSalesOrder
        {
            get { return m_QuantityOnSalesOrder; }
            set { m_QuantityOnSalesOrder = value; }
        }

        public decimal? QuantityToBuild
        {
            get { return m_QuantityToBuild; }
            set { m_QuantityToBuild = value; }
        }

        public string ComponentItemLineRet
        {
            get { return m_ComponentItemLineRet; }
            set { m_ComponentItemLineRet = value; }
        }
        //for ComponentItemLineRet

        public string[] ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }

        public string[] TxnLineID
        {
            get { return m_TxnLineID; }
            set { m_TxnLineID = value; }
        }

        public string[] Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string[] ItemRefDesc
        {
            get { return m_ItemRefDesc; }
            set { m_ItemRefDesc = value; }
        }

        public string[] ItemRefQuantityOnHand
        {
            get { return m_ItemRefQuantityOnHand; }
            set { m_ItemRefQuantityOnHand = value; }
        }

        public string[] ItemRefQuantityNeeded
        {
            get { return m_ItemRefQuantityNeeded; }
            set { m_ItemRefQuantityNeeded = value; }
        }

        //AXIS 610
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }
        public string InventorySiteLocationRefFullName
        {
            get { return m_InventorySiteLocationRef; }
            set { m_InventorySiteLocationRef = value; }
        }
        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }
        public string LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        public string[] ItemRefInventorySiteLocationRef
        {
            get { return m_ItemRefInventorySiteLocationRef; }
            set { m_ItemRefInventorySiteLocationRef = value; }
        }
        public string[] ItemRefInventorySiteRef
        {
            get { return m_ItemRefInventorySiteRef; }
            set { m_ItemRefInventorySiteRef = value; }
        }
        public string[] ItemRefLotNumber
        {
            get { return m_ItemRefLotNumber; }
            set { m_ItemRefLotNumber = value; }
        }
        public string[] ItemRefSerialNumber
        {
            get { return m_ItemRefSerialNumber; }
            set { m_ItemRefSerialNumber = value; }
        }
        //END AXIS 610

        #endregion
    }

    public class QBBuildAssemblylListCollection : Collection<BuildAssemblyList>
    {

        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all Build Assemblies in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllBuildAssemblyList(string QBFileName)
        {
            string buildAssemblyXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName);

            List<string> buildAssemblyList = new List<string>();
            List<string> buildAssemblyList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(buildAssemblyXmlList);

            XmlFileData = buildAssemblyXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList BuilAssemblyNodeList = outputXMLDoc.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

            foreach (XmlNode baNodes in BuilAssemblyNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in baNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                        {
                            if (!buildAssemblyList.Contains(node.InnerText))
                            {
                                buildAssemblyList.Add(node.InnerText);
                                string name = node.InnerText + "     :     BuildAssembly";
                                buildAssemblyList1.Add(name);
                            }
                        }
                    }
                }
            }
            CommonUtilities.GetInstance().BuildAssemblyListWithType = buildAssemblyList1;
            return buildAssemblyList;

        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName)
        {
            #region Getting BuildAssembly Entry List from QuickBooks.


            string BuildAssemblyEntryXmlList = string.Empty;
            BuildAssemblyEntryXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName);

            BuildAssemblyList BuildAssemblyList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyEntryXmlList);
                XmlFileData = BuildAssemblyEntryXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {

                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();

                        //BuildAssemblyList.ComponentItemLineRet = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        //BuildAssemblyList.IsPending = Convert.ToBoolean( new bool[BuildAssemblyNodes.ChildNodes.Count]);
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
						BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];                       
                        
                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {
                               
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                        //}
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }
                                                      

                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }
        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Build AssemblyList from QuickBooks.

            //Getting item list from QuickBooks.
            string BuildAssemblyListXmlList = string.Empty;

            BuildAssemblyListXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BuildAssemblyQueryRq", QBFileName, FromDate, ToDate);
            BuildAssemblyList BuildAssemblyList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyListXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyListXmlList);
                XmlFileData = BuildAssemblyListXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();

                        //BuildAssemblyList.ComponentItemLineRet = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        //BuildAssemblyList.IsPending = Convert.ToBoolean( new bool[BuildAssemblyNodes.ChildNodes.Count]);
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];                       

                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {
                               
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }                           

                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter is selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Vendor List from QuickBooks.

            //Getting item list from QuickBooks.
            string BuildAssemblyListXmlList = string.Empty;

            if (inActiveFilter == false)//bug no. 395
            {
                BuildAssemblyListXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName);
            }
            else
            {
                BuildAssemblyListXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("BuildAssemblyQueryRq", QBFileName);
            }
            BuildAssemblyList BuildAssemblyList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyListXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyListXmlList);
                XmlFileData = BuildAssemblyListXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();

                        //BuildAssemblyList.ComponentItemLineRet = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        //BuildAssemblyList.IsPending = Convert.ToBoolean( new bool[BuildAssemblyNodes.ChildNodes.Count]);
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];                        

                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {                               
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                        //}



                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }


                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting Build Assembly List from QuickBook by
        /// passing company filename, fromDate, and Todate.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting Employee List from QuickBooks.

            //Getting item list from QuickBooks.
            string BuidAssemblyXmlList = string.Empty;
           
            BuidAssemblyXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName, FromDate, ToDate);
            BuildAssemblyList BuildAssemblyList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();
            //Axis 8.0; declare integer i
            int i = 0;
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuidAssemblyXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuidAssemblyXmlList);
                XmlFileData = BuidAssemblyXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    //Axis 8.0 Reset value of i to 0.
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();                        
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];                       
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];                       

                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {                                
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                        //}



                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }


                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for getting JournalEntry from QuickBoos for filter
        /// RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string BuildAssemblyEntryXmlList = string.Empty;
            BuildAssemblyEntryXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName, FromRefnum, ToRefnum);

            BuildAssemblyList BuildAssemblyList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyEntryXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyEntryXmlList);
                XmlFileData = BuildAssemblyEntryXmlList;


                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();
                       
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];                        
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
              			BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];                       

                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {                                
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);

                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }

                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        private void checkNullEntries(XmlNode node, ref BuildAssemblyList buildAssemblyList, int count)
        {
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.ItemRef.ToString()) == null)
            {
                buildAssemblyList.TxnLineID[count] = null;
                buildAssemblyList.ItemRefFullName[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.Desc.ToString()) == null)
            {
                buildAssemblyList.ItemRefDesc[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.QuantityOnHand.ToString()) == null)
            {
                buildAssemblyList.ItemRefQuantityOnHand[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.QuantityNeeded.ToString()) == null)
            {
                buildAssemblyList.ItemRefQuantityNeeded[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.InventorySiteRef.ToString()) == null)
            {
                buildAssemblyList.ItemRefInventorySiteRef[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.InventorySiteLocationRef.ToString()) == null)
            {
                buildAssemblyList.ItemRefInventorySiteLocationRef[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.SerialNumber.ToString()) == null)
            {
                buildAssemblyList.ItemRefSerialNumber[count] = null;
            }
            if (node.SelectSingleNode("./" + QBBuildAssemblyList.LotNumber.ToString()) == null)
            {
                buildAssemblyList.ItemRefLotNumber[count] = null;
            }
        }

        /// <summary>
        /// This method is used for getting JournalEntry from Quickbooks for filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string BuildAssemblyXmlList = string.Empty;
            BuildAssemblyXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BuildAssemblyList BuildAssemblyList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyXmlList);
                XmlFileData = BuildAssemblyXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {

                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();

                        //BuildAssemblyList.ComponentItemLineRet = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        //BuildAssemblyList.IsPending = Convert.ToBoolean( new bool[BuildAssemblyNodes.ChildNodes.Count]);
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];
                       
                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {                               
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }

                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        //Axis 671 DESKTOP
        public Collection<BuildAssemblyList> PopulateBuildAssemblyList(string QBFileName, DateTime FromDate, string ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting JournalEntry List from QuickBooks.


            string BuildAssemblyXmlList = string.Empty;
            BuildAssemblyXmlList = QBCommonUtilities.GetListFromQuickBook("BuildAssemblyQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BuildAssemblyList BuildAssemblyList;
            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBuildAssembly = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BuildAssemblyXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBuildAssembly.LoadXml(BuildAssemblyXmlList);
                XmlFileData = BuildAssemblyXmlList;

                #region Getting Journal Entry Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList BuildAssemblyNodeList = outputXMLDocOfBuildAssembly.GetElementsByTagName(QBBuildAssemblyList.BuildAssemblyRet.ToString());

                foreach (XmlNode BuildAssemblyNodes in BuildAssemblyNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        #region Get Build Assembly List
                        int count = 0;
                        int count1 = 0;
                        BuildAssemblyList = new BuildAssemblyList();

                        //BuildAssemblyList.ComponentItemLineRet = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.FullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        //BuildAssemblyList.IsPending = Convert.ToBoolean( new bool[BuildAssemblyNodes.ChildNodes.Count]);
                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count].ToString();
                        BuildAssemblyList.ItemRefDesc = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefFullName = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityNeeded = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefQuantityOnHand = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.Name = new string[BuildAssemblyNodes.ChildNodes.Count];
 BuildAssemblyList.ItemRefInventorySiteLocationRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefInventorySiteRef = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefLotNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        BuildAssemblyList.ItemRefSerialNumber = new string[BuildAssemblyNodes.ChildNodes.Count];
                        
                        BuildAssemblyList.TxnLineID = new string[BuildAssemblyNodes.ChildNodes.Count];

                        foreach (XmlNode node in BuildAssemblyNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnID.ToString()))
                            {
                                BuildAssemblyList.TxnID = node.InnerText;
                            }

                            //Checking TxnNumber. 
                            if (node.Name.Equals(QBBuildAssemblyList.TxnNumber.ToString()))
                            {
                                BuildAssemblyList.TxnNumber = node.InnerText;
                            }

                            //Checking ItemInventoryAssemblyFullName
                            if (node.Name.Equals(QBBuildAssemblyList.ItemInventoryAssemblyRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                        BuildAssemblyList.ItemInventoryAssemblyRefFullName = childNodes.InnerText;
                                }
                            }

                            //Checking InventorySiteRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.IsPending.ToString()))
                            {
                                BuildAssemblyList.IsPending = Convert.ToBoolean(node.InnerText);
                            }

                            //Checking InventorySiteLocationRefFullName
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityCanBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityCanBuild = node.InnerText;
                            }

                            //Checking SerialNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                            {
                                BuildAssemblyList.QuantityOnHand = node.InnerText;
                            }

                            //Checking LotNumber
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityOnSalesOrder.ToString()))
                            {
                                BuildAssemblyList.QuantityOnSalesOrder = node.InnerText;
                            }

                            //Checking TxnDate
                            if (node.Name.Equals(QBBuildAssemblyList.TxnDate.ToString()))
                            {
                                BuildAssemblyList.TxnDate = node.InnerText;
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(QBBuildAssemblyList.RefNumber.ToString()))
                            {
                                BuildAssemblyList.RefNumber = node.InnerText;
                            }

                            //Checking VendorAddress
                            if (node.Name.Equals(QBBuildAssemblyList.ComponentItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref BuildAssemblyList, count);
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBBuildAssemblyList.ItemRef.ToString()))
                                    {

                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.ListID.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 36)
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText.Remove(36, (childNodes.InnerText.Length - 36)).Trim();
                                                else
                                                    BuildAssemblyList.TxnLineID[count] = childNodes.InnerText;
                                            }
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 209)
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText.Remove(209, (childNodes.InnerText.Length - 209)).Trim();
                                                else
                                                    BuildAssemblyList.ItemRefFullName[count] = childNodes.InnerText;
                                            }

                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.Desc.ToString()))
                                        BuildAssemblyList.ItemRefDesc[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityOnHand.ToString()))
                                        BuildAssemblyList.ItemRefQuantityOnHand[count] = childNode.InnerText;


                                    if (childNode.Name.Equals(QBBuildAssemblyList.QuantityNeeded.ToString()))
                                        BuildAssemblyList.ItemRefQuantityNeeded[count] = childNode.InnerText;
                                    //ISSUE 610
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteRef[count] = childNode.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))

                                                BuildAssemblyList.ItemRefInventorySiteLocationRef[count] = childNode.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                                        BuildAssemblyList.ItemRefSerialNumber[count] = childNode.InnerText;

                                    if (childNode.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                                        BuildAssemblyList.ItemRefLotNumber[count] = childNode.InnerText;
                                    //ISSUE 610 END
                                }

                                count++;
                            }


                            //Checking QuantityToBuild
                            if (node.Name.Equals(QBBuildAssemblyList.QuantityToBuild.ToString()))
                            {
                                BuildAssemblyList.QuantityToBuild = Convert.ToDecimal(node.InnerText);
                            }
                            //ISSUE 610
                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {

                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteRefFullName = childNodes.InnerText;

                                    }
                                }
                            }
                            if (node.Name.Equals(QBBuildAssemblyList.InventorySiteLocationRef.ToString()))
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name.Equals(QBBuildAssemblyList.FullName.ToString()))
                                    {
                                        BuildAssemblyList.InventorySiteLocationRefFullName = childNodes.InnerText;
                                    }
                                }
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.SerialNumber.ToString()))
                            {
                                BuildAssemblyList.SerialNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.LotNumber.ToString()))
                            {
                                BuildAssemblyList.LotNumber = node.InnerText;
                            }

                            if (node.Name.Equals(QBBuildAssemblyList.Memo.ToString()))
                            {
                                BuildAssemblyList.Memo = node.InnerText;
                            }
                            //ISSUE 610 End
                        }
                        this.Add(BuildAssemblyList);
                        # endregion
                    }
                    else
                    { return null; }

                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBuildAssembly.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
        #endregion
    }
}
