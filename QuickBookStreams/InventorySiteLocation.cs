﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    class QBInventorySiteLocationList : InventorySiteList
    {
        


        public string ListId
        {
            get
            { return m_ListID; }
            set
            {
                m_ListID = value;
            }
        }

        public string Name
        {
            get
            {
                return m_Name;
            }
            set
            {
                if (value.Length > 31)
                    m_Name = value.Remove(31, (value.Length - 31));
                else
                    m_Name = value;
            }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set
            {

                m_TimeCreated = value;
            }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set
            {

                m_TimeModified = value;
            }
        }

         public string EditSequence
        {
            get
            {
                return m_EditSequence;
            }
            set
            {
                if (value.Length > 16)
                    m_EditSequence = value.Remove(16, (value.Length - 16));
                else
                    m_EditSequence = value;
            }
        }

         public string IsActive
         {
             get { return m_IsActive; }
             set {m_IsActive = value; }
         }

         public string ParentSiteFullName
         {
             get { return m_ParentSiteFullName; }
             set
             {
                 if (value.Length > 31)
                     m_ParentSiteFullName = value.Remove(31, (value.Length - 31));
                 else
                     m_ParentSiteFullName = value;
             }
         }
         public string IsDefaultSite
         {
             get { return m_IsDefaultSite; }
             set { m_IsDefaultSite = value; }
         }

         public string SiteDesc
         {
             get { return m_SiteDesc; }
             set
             {
                 if (value.Length > 100)
                     m_SiteDesc = value.Remove(100, (value.Length - 100));
                 else
                     m_SiteDesc = value;
             }
         }

         public string Contact
         {
             get { return m_Contact; }
             set
             {
                 if (value.Length > 41)
                     m_Contact = value.Remove(41, (value.Length - 41));
                 else
                     m_Contact = value;
             }
         }
         public string Phone
         {
             get { return m_Phone; }
             set
             {
                 if (value.Length > 21)
                     m_Phone = value.Remove(21, (value.Length - 21));
                 else
                     m_Phone = value;
             }
         }

         public string Fax
         {
             get { return m_Fax; }
             set
             {
                 if (value.Length > 21)
                     m_Fax = value.Remove(21, (value.Length - 21));
                 else
                     m_Fax = value;
             }
         }
         public string Email
         {
             get { return m_Email; }
             set
             {
                 if (value.Length > 1023)
                     m_Email = value.Remove(1023, (value.Length - 1023));
                 else
                     m_Email = value;
             }
         }

         public string SiteAddr1
         {
             get { return m_SiteAddr1; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddr1 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddr1 = value;
             }
         }

         public string SiteAddr2
         {
             get { return m_SiteAddr2; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddr2 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddr2 = value;
             }
         }

         public string SiteAddr3
         {
             get { return m_SiteAddr3; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddr3 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddr3 = value;
             }
         }

         public string SiteAddr4
         {
             get { return m_SiteAddr4; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddr4 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddr4 = value;
             }
         }

         public string SiteAddr5
         {
             get { return m_SiteAddr5; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddr5 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddr5 = value;
             }
         }

         public string SiteCity
         {
             get { return m_SiteCity; }
             set
             {
                 if (value.Length > 31)
                     m_SiteCity = value.Remove(31,(value.Length - 31));
                 else
                     m_SiteCity = value;
             }
         }
         public string SiteState
         {
             get { return m_SiteState; }
             set
             {
                 if (value.Length > 21)
                     m_SiteState = value.Remove(21, (value.Length - 21));
                 else
                     m_SiteState = value;
             }
         }

         public string SitePostalCode
         {
             get { return m_SitePostalCode; }
             set
             {
                 if (value.Length > 13)
                     m_SitePostalCode = value.Remove(13, (value.Length - 13));
                 else
                     m_SitePostalCode = value;
             }
         }

         public string SiteCountry
         {
             get { return m_SiteCountry; }
             set
             {
                 if (value.Length > 31)
                     m_SiteCountry = value.Remove(31, (value.Length - 31));
                 else
                     m_SiteCountry = value;
             }
         }


         public string SiteAddressBlockAddr1
         {
             get { return m_SiteAddressBlockAddr1; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddressBlockAddr1 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddressBlockAddr1 = value;
             }
         }

         public string SiteAddressBlockAddr2
         {
             get { return m_SiteAddressBlockAddr2; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddressBlockAddr2 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddressBlockAddr2 = value;
             }
         }

         public string SiteAddressBlockAddr3
         {
             get { return m_SiteAddressBlockAddr3; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddressBlockAddr3 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddressBlockAddr3 = value;
             }
         }

         public string SiteAddressBlockAddr4
         {
             get { return m_SiteAddressBlockAddr4; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddressBlockAddr4 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddressBlockAddr4 = value;
             }
         }

         public string SiteAddressBlockAddr5
         {
             get { return m_SiteAddressBlockAddr5; }
             set
             {
                 if (value.Length > 41)
                     m_SiteAddressBlockAddr5 = value.Remove(41, (value.Length - 41));
                 else
                     m_SiteAddressBlockAddr5 = value;
             }
         }


         public class QBInventorySiteLocationCollection : Collection<QBInventorySiteLocationList>
         {
             string XmlFileData = string.Empty;


             /// <summary>
             /// this method returns all InventorySite fields in quickbook
             /// </summary>
             /// <returns></returns>
             public List<string> PopulateInventorySiteLocationList(string QBFileName)
             {
                 string InventorySiteLocationXmlList = string.Empty;

                 InventorySiteLocationXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("InventorySiteQueryRq", QBFileName);
                
                 XmlDocument outputXMLDoc = new XmlDocument();

                 List<string> SiteLocList = new List<string>();
                 outputXMLDoc.LoadXml(InventorySiteLocationXmlList);
                 XmlFileData = InventorySiteLocationXmlList;
                 
                 int i = 0;
                 TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                 BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

                 XmlNodeList SiteLocationNodeList = outputXMLDoc.GetElementsByTagName(QBXmlInventorySiteLocationList.InventorySiteLocationRet.ToString());
                 
                 foreach (XmlNode SiteNodes in SiteLocationNodeList)
                 {
                     //Axis 8.0 Reset value of i to 0.
                     i = 0;

                     if (bkWorker.CancellationPending != true)
                     {
                         QBDepositList deposit = new QBDepositList();
                         foreach (XmlNode node in SiteNodes.ChildNodes)
                         {
                             if (node.Name.Equals(QBXmlInventorySiteLocationList.ListID.ToString()))
                             {
                                 if (!SiteLocList.Contains(node.InnerText))
                                     SiteLocList.Add(node.InnerText);
                             }
                         }
                     }
                 }

                 return SiteLocList;
               
             }





             /// <summary>
             /// This method is used to get the xml response from the QuikBooks.
             /// </summary>
             /// <returns></returns>
             public string GetXmlFileData()
             {
                 return XmlFileData;
             }

         }
    }
}
