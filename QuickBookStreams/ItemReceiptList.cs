﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using System.Xml;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;

namespace Streams
{
    public class ItemReceiptList : BaseItemReceiptList
    {
        #region Public Properties
        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string VenderRefFullName
        {
            get { return m_VenderRefFullName; }
            set { m_VenderRefFullName = value; }
        }
        public string APAccountRefFullName
        {
            get { return m_APAccountRefFullName; }
            set { m_APAccountRefFullName = value; }
        }

        public string LiabilityAccountRefFullName
        {
            get { return m_LiabilityAccountRefFullName; }
            set { m_LiabilityAccountRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string TotalAmount
        {
            get { return m_TotalAmount; }
            set { m_TotalAmount = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public string TotalAmountInHomeCurrency
        {
            get { return m_TotalAmountInHomeCurrency; }
            set { m_TotalAmountInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        //public string OpenAmount
        //{
        //    get { return m_OpenAmount; }
        //    set { m_OpenAmount = value; }
        //}

        public List<string> DataExtOwnerID
        {
            get { return m_DataExtOwnerID; }
            set { m_DataExtOwnerID = value; }
        }

        public List<string> DataExtName
        {
            get { return m_DataExtName; }
            set { m_DataExtName = value; }
        }

        public List<string> DataExtType
        {
            get { return m_DataExtType; }
            set { m_DataExtType = value; }
        }

        public List<string> DataExtValue
        {
            get { return m_DataExtValue; }
            set { m_DataExtValue = value; }
        }


        //Linked Transaction 
        public string LinkedTxnTxnID
        {
            get { return m_LinkedTxnTxnID; }
            set { m_LinkedTxnTxnID = value; }
        }

        public string LinkedTxnTxnDate
        {
            get { return m_LinkedTxnTxnDate; }
            set { m_LinkedTxnTxnDate = value; }
        }

        public string LinkedTxnTxnType
        {
            get { return m_LinkedTxnTxnType; }
            set { m_LinkedTxnTxnType = value; }
        }

        public string LinkedTxnRefNumber
        {
            get { return m_LinkedTxnRefNumber; }
            set { m_LinkedTxnRefNumber = value; }
        }

        public string LinkedTxnLinkType
        {
            get { return m_LinkedTxnLinkType; }
            set { m_LinkedTxnLinkType = value; }
        }

        public string LinkedTxnAmount
        {
            get { return m_LinkedTxnAmount; }
            set { m_LinkedTxnAmount = value; }
        }


        //For ExpenseLineRet,repeat
        public List<string> ExpensesLineTxnLineID
        {
            get { return m_ExpensesLineTxnLineID; }
            set { m_ExpensesLineTxnLineID = value; }
        }

        public List<string> ExpensesLineAccountRefFullName
        {
            get { return m_ExpensesLineAccountRefFullName; }
            set { m_ExpensesLineAccountRefFullName = value; }
        }

        public List<decimal?> ExpensesLineAmount
        {
            get { return m_ExpensesLineAmount; }
            set { m_ExpensesLineAmount = value; }
        }

        public List<string> ExpensesLineMemo
        {
            get { return m_ExpensesLineMemo; }
            set { m_ExpensesLineMemo = value; }
        }

        public List<string> ExpensesLineCustomerRefFullName
        {
            get { return m_ExpensesLineCustomerRefFullName; }
            set { m_ExpensesLineCustomerRefFullName = value; }
        }

        public List<string> ExpensesLineClassRefFullName
        {
            get { return m_ExpensesLineClassRefFullName; }
            set { m_ExpensesLineClassRefFullName = value; }
        }

        public List<string> ExpensesLineSalesTaxFullName
        {
            get { return m_ExpensesLineSalesTaxFullName; }
            set { m_ExpensesLineSalesTaxFullName = value; }
        }
        public List<string> ExpensesLineBillableStatus
        {
            get { return m_ExpensesLineBillableStatus; }
            set { m_ExpensesLineBillableStatus = value; }
        }
        public List<string> ExpensesLineSalesRepRefFullName
        {
            get { return m_ExpensesLineSalesRepRefFullName; }
            set { m_ExpensesLineSalesRepRefFullName = value; }
        }
        public List<string> ExpensesLineDataExtOwnerID
        {
            get { return m_ExpensesLineDataExtOwnerID; }
            set { m_ExpensesLineDataExtOwnerID = value; }
        }
        public List<string> ExpensesLineDataExtDataExtName
        {
            get { return m_ExpensesLineDataExtDataExtName; }
            set { m_ExpensesLineDataExtDataExtName = value; }
        }
        public List<string> ExpensesLineDataExtDataExtType
        {
            get { return m_ExpensesLineDataExtDataExtType; }
            set { m_ExpensesLineDataExtDataExtType = value; }
        }
        public List<string> ExpensesLineDataExtDataExtValue
        {
            get { return m_ExpensesLineDataExtDataExtValue; }
            set { m_ExpensesLineDataExtDataExtValue = value; }
        }

        //For ItemLineRet, repeat
        public List<string> ItemLineTxnLineID
        {
            get { return m_ItemLineTxnLineID; }
            set { m_ItemLineTxnLineID = value; }
        }
        public List<string> ItemLineItemRefFullName
        {
            get { return m_ItemLineRefFullName; }
            set { m_ItemLineRefFullName = value; }
        }
        public List<string> ItemLineInventorySiteLocationRefFullName
        {
            get { return m_ItemLineInventorySiteLocationRefFullName; }
            set { m_ItemLineInventorySiteLocationRefFullName = value; }
        }
        public List<string> ItemLineSerialNumber
        {
            get { return m_ItemLineSerialNumber; }
            set { m_ItemLineSerialNumber = value; }
        }
        public List<string> ItemLineLotNumber
        {
            get { return m_ItemLineLotNumber; }
            set { m_ItemLineLotNumber = value; }
        }
        public List<string> ItemLineDesc
        {
            get { return m_ItemLineDesc; }
            set { m_ItemLineDesc = value; }
        }
        public List<decimal?> ItemLineQuantity
        {
            get { return m_ItemLineQuantity; }
            set { m_ItemLineQuantity = value; }
        }
        public List<string> ItemLineUOM
        {
            get { return m_ItemLineUOM; }
            set { m_ItemLineUOM = value; }
        }

        public List<string> ItemLineOverrideUOMSetRefFullName
        {
            get { return m_ItemLineOverrideUOMSetRefFullName; }
            set { m_ItemLineOverrideUOMSetRefFullName = value; }
        }
        public List<decimal?> ItemLineCost
        {
            get { return m_ItemLineCost; }
            set { m_ItemLineCost = value; }
        }
        public List<decimal?> ItemLineAmount
        {
            get { return m_ItemLineAmount; }
            set { m_ItemLineAmount = value; }
        }
        public List<string> ItemLineCustomerRefFullName
        {
            get { return m_ItemLineCustomerRefFullName; }
            set { m_ItemLineCustomerRefFullName = value; }
        }
        public List<string> ItemLineClassRefFullName
        {
            get { return m_ItemLineClassRefFullName; }
            set { m_ItemLineClassRefFullName = value; }
        }
        public List<string> ItemLineSalesRepRefFullName
        {
            get { return m_ItemLineSalesRepRefFullName; }
            set { m_ItemLineSalesRepRefFullName = value; }
        }
        public List<string> ItemLineBillableStatus
        {
            get { return m_ItemLineBillableStatus; }
            set { m_ItemLineBillableStatus = value; }
        }
        public List<string> ItemLineDataExtOwnerID
        {
            get { return m_ItemLineDataExtOwnerID; }
            set { m_ItemLineDataExtOwnerID = value; }
        }
        public List<string> ItemLineDataExtDataExtName
        {
            get { return m_ItemLineDataExtDataExtName; }
            set { m_ItemLineDataExtDataExtName = value; }
        }
        public List<string> ItemLineDataExtDataExtType
        {
            get { return m_ItemLineDataExtDataExtType; }
            set { m_ItemLineDataExtDataExtType = value; }
        }
        public List<string> ItemLineDataExtDataExtValue
        {
            get { return m_ItemLineDataExtDataExtValue; }
            set { m_ItemLineDataExtDataExtValue = value; }
        }



        //For ItemGroupLineRet


        public List<string> ItemGroupLineRet
        {
            get { return m_ItemGroupLineRet; }
            set { m_ItemGroupLineRet = value; }
        }
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupLineTxnLineID; }
            set { m_GroupLineTxnLineID = value; }
        }
        public List<string> GroupLineItemGroupRefFullName
        {
            get { return m_GroupLineItemGroupRefFullName; }
            set { m_GroupLineItemGroupRefFullName = value; }
        }
        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }
        public List<string> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }
        public List<string> GroupLineUnitOfMeasure
        {
            get { return m_GroupLineUnitOfMeasure; }
            set { m_GroupLineUnitOfMeasure = value; }
        }
        public List<string> GroupLineOverrideUOMSetRefFullName
        {
            get { return m_GroupLineOverrideUOMSetRefFullName; }
            set { m_GroupLineOverrideUOMSetRefFullName = value; }
        }
        public List<decimal?> GroupLineTotalAmount
        {
            get { return m_GroupLineTotalAmount; }
            set { m_GroupLineTotalAmount = value; }
        }

        // groupline -->  itemLine repeated
        public List<string> GroupLineItemLineTxnLineID
        {
            get { return m_GroupLineItemLineTxnLineID; }
            set { m_GroupLineItemLineTxnLineID = value; }
        }
        public List<string> GroupLineItemLineItemRefFullName
        {
            get { return m_GroupLineItemLineItemRefFullName; }
            set { m_GroupLineItemLineItemRefFullName = value; }
        }
        public List<string> GroupLineItemLineInventorySiteLocationRefFullName
        {
            get { return m_GroupLineItemLineInventorySiteLocationRefFullName; }
            set { m_GroupLineItemLineInventorySiteLocationRefFullName = value; }
        }
        public List<string> GroupLineItemLineSerialNumber
        {
            get { return m_GroupLineItemLineSerialNumber; }
            set { m_GroupLineItemLineSerialNumber = value; }
        }
        public List<string> GroupLineItemLineLotNumber
        {
            get { return m_GroupLineItemLineLotNumber; }
            set { m_GroupLineItemLineLotNumber = value; }
        }
        public List<string> GroupLineItemLineDesc
        {
            get { return m_GroupLineItemLineDesc; }
            set { m_GroupLineItemLineDesc = value; }
        }
        public List<decimal?> GroupLineItemLineQuantity
        {
            get { return m_GroupLineItemLineQuantity; }
            set { m_GroupLineItemLineQuantity = value; }
        }
        public List<string> GroupLineItemLineUOM
        {
            get { return m_GroupLineItemLineUOM; }
            set { m_GroupLineItemLineUOM = value; }
        }
        public List<string> GroupLineItemLineOverrideUOMSetRefFullName
        {
            get { return m_GroupLineItemLineOverrideUOMSetRefFullName; }
            set { m_GroupLineItemLineOverrideUOMSetRefFullName = value; }
        }
        public List<decimal?> GroupLineItemLineCost
        {
            get { return m_GroupLineItemLineCost; }
            set { m_GroupLineItemLineCost = value; }
        }
        public List<decimal?> GroupLineItemLineAmount
        {
            get { return m_GroupLineItemLineAmount; }
            set { m_GroupLineItemLineAmount = value; }
        }
        public List<string> GroupLineItemLineCustomerRefFullName
        {
            get { return m_GroupLineItemLineCustomerRefFullName; }
            set { m_GroupLineItemLineCustomerRefFullName = value; }
        }
        public List<string> GroupLineItemLineClassRefFullName
        {
            get { return m_GroupLineItemLineClassRefFullName; }
            set { m_GroupLineItemLineClassRefFullName = value; }
        }
        public List<string> GroupLineItemLineSalesRepRefFullName
        {
            get { return m_GroupLineItemLineSalesRepRefFullName; }
            set { m_GroupLineItemLineSalesRepRefFullName = value; }
        }
        public List<string> GroupLineItemLineBillableStatus
        {
            get { return m_GroupLineItemLineBillableStatus; }
            set { m_GroupLineItemLineBillableStatus = value; }
        }
        public List<string> GroupLineItemLineDataExtOwnerID
        {
            get { return m_GroupLineItemLineDataExtOwnerID; }
            set { m_GroupLineItemLineDataExtOwnerID = value; }
        }
        public List<string> GroupLineItemLineDataExtDataExtName
        {
            get { return m_GroupLineItemLineDataExtDataExtName; }
            set { m_GroupLineItemLineDataExtDataExtName = value; }
        }
        public List<string> GroupLineItemLineDataExtDataExtType
        {
            get { return m_GroupLineItemLineDataExtDataExtType; }
            set { m_GroupLineItemLineDataExtDataExtType = value; }
        }
        public List<string> GroupLineItemLineDataExtDataExtValue
        {
            get { return m_GroupLineItemLineDataExtDataExtValue; }
            set { m_GroupLineItemLineDataExtDataExtValue = value; }
        }
       #endregion

    }

        public class QBItemReceiptListCollection : Collection<ItemReceiptList>
        {
            
            int i = 0;
            string XmlFileData = string.Empty;
        QBItemDetails itemList = null;
        //Getting current version of System. BUG(676)

        /// <summary>
        /// this method returns all Item Receipt records in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllItemReceiptList(string QBFileName)
            {
                string receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName);

                List<string> receiptList = new List<string>();

                XmlDocument outputXMLDoc = new XmlDocument();

                outputXMLDoc.LoadXml(receiptXmlList);
                XmlFileData = receiptXmlList;

                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

                XmlNodeList receiptNodeList = outputXMLDoc.GetElementsByTagName(QBItemReceiptList.ItemReceiptRet.ToString());

                foreach (XmlNode receiptNodes in receiptNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        //QBVehiclemileageList vehiclemileage = new QBVehiclemileageList();
                        foreach (XmlNode node in receiptNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBVehiclemileageList.TxnID.ToString()))
                            {
                                if (!receiptList.Contains(node.InnerText))
                                    receiptList.Add(node.InnerText);
                            }
                        }
                    }
                }

                return receiptList;
            }

            public void GetItemReceiptValueFromXML(string receiptXmlList)
            {
                XmlDocument outputXMLDoc = new XmlDocument();
                outputXMLDoc.LoadXml(receiptXmlList);
                XmlFileData = receiptXmlList;
                string countryName = string.Empty;
                countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                XmlNodeList receiptNodeList = outputXMLDoc.GetElementsByTagName(QBItemReceiptList.ItemReceiptRet.ToString());
                ItemReceiptList ItemReceiptList;

                if (receiptXmlList != string.Empty)
                {
                    #region copy
                    foreach (XmlNode receiptNodes in receiptNodeList)
                    {
                        #region For Item Receipt data
                        if (bkWorker.CancellationPending != true)
                        {
                            int count1 = 0;
                            int count2 = 0;
                            int count3 = 0;

                            ItemReceiptList = new ItemReceiptList();

                            foreach (XmlNode node in receiptNodes.ChildNodes)
                            {

                                //Checking TxnID. 
                                if (node.Name.Equals(QBItemReceiptList.TxnID.ToString()))
                                {
                                    ItemReceiptList.TxnID = node.InnerText;
                                }

                                //Checking TxnNumber .
                                if (node.Name.Equals(QBItemReceiptList.TxnNumber.ToString()))
                                { ItemReceiptList.TxnNumber = node.InnerText; }

                                //Checking vendorref .


                                //Checking TxnDate .
                                if (node.Name.Equals(QBItemReceiptList.TxnDate.ToString()))
                                {
                                    try
                                    {
                                        DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                        if (countryName == "United States")
                                        {
                                            ItemReceiptList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                        }
                                        else
                                        {
                                            ItemReceiptList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                        }
                                    }
                                    catch
                                    {
                                        ItemReceiptList.TxnDate = node.InnerText;
                                    }
                                }

                                //Checking VenderRefFullName.
                                if (node.Name.Equals(QBItemReceiptList.VendorRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                            ItemReceiptList.VenderRefFullName = childNode.InnerText;
                                    }
                                }
                                //Checking APAccountRefFullName.
                                if (node.Name.Equals(QBItemReceiptList.APAccountRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                            ItemReceiptList.APAccountRefFullName = childNode.InnerText;
                                    }
                                }

                                //Checking LiabilityAccountRefFullName.
                                if (node.Name.Equals(QBItemReceiptList.LiabilityAccountRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                            ItemReceiptList.LiabilityAccountRefFullName = childNode.InnerText;
                                    }
                                }

                                //Checking TotalAmount .
                                if (node.Name.Equals(QBItemReceiptList.TotalAmount.ToString()))
                                {
                                    ItemReceiptList.TotalAmount = node.InnerText;
                                }
                                //Checking LiabilityAccountRefFullName.
                                if (node.Name.Equals(QBItemReceiptList.CurrencyRef.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                            ItemReceiptList.CurrencyRefFullName = childNode.InnerText;
                                    }
                                }

                                //Checking ExchangeRate .
                                if (node.Name.Equals(QBItemReceiptList.ExchangeRate.ToString()))
                                {
                                    ItemReceiptList.ExchangeRate = node.InnerText;
                                }
                                //Checking TotalAmountInHomeCurrency .
                                if (node.Name.Equals(QBItemReceiptList.TotalAmountInHomeCurrency.ToString()))
                                {
                                    ItemReceiptList.TotalAmountInHomeCurrency = node.InnerText;
                                }
                                //Checking RefNumber .
                                if (node.Name.Equals(QBItemReceiptList.RefNumber.ToString()))
                                {
                                    ItemReceiptList.RefNumber = node.InnerText;
                                }
                                //Checking Memo .
                                if (node.Name.Equals(QBItemReceiptList.Memo.ToString()))
                                {
                                    ItemReceiptList.Memo = node.InnerText;
                                }
                                //Checking ExternalGUID .
                                if (node.Name.Equals(QBItemReceiptList.ExternalGUID.ToString()))
                                {
                                    ItemReceiptList.ExternalGUID = node.InnerText;
                                }

                                //Checking LinkedTxn. may repeat
                                if (node.Name.Equals(QBItemReceiptList.LinkedTxn.ToString()))
                                {
                                    foreach (XmlNode childNode in node.ChildNodes)
                                    {
                                        if (childNode.Name.Equals(QBItemReceiptList.TxnID.ToString()))
                                        {
                                            ItemReceiptList.LinkedTxnTxnID = childNode.InnerText;
                                        }
                                        if (childNode.Name.Equals(QBItemReceiptList.TxnDate.ToString()))
                                        {
                                            try
                                            {
                                                DateTime dttxn = Convert.ToDateTime(childNode.InnerText);
                                                if (countryName == "United States")
                                                {
                                                    ItemReceiptList.LinkedTxnTxnDate = dttxn.ToString("MM/dd/yyyy");
                                                }
                                                else
                                                {
                                                    ItemReceiptList.LinkedTxnTxnDate = dttxn.ToString("dd/MM/yyyy");
                                                }
                                            }
                                            catch
                                            {
                                                ItemReceiptList.LinkedTxnTxnDate = childNode.InnerText;
                                            }
                                        }
                                        if (childNode.Name.Equals(QBItemReceiptList.TxnType.ToString()))
                                        {
                                            ItemReceiptList.LinkedTxnTxnType = childNode.InnerText;
                                        }
                                        if (childNode.Name.Equals(QBItemReceiptList.RefNumber.ToString()))
                                        {
                                            ItemReceiptList.LinkedTxnRefNumber = childNode.InnerText;
                                        }
                                        if (childNode.Name.Equals(QBItemReceiptList.LinkType.ToString()))
                                        {
                                            ItemReceiptList.LinkedTxnLinkType = childNode.InnerText;
                                        }
                                        if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
                                        {
                                            ItemReceiptList.LinkedTxnAmount = childNode.InnerText;
                                        }

                                    }

                                }
                            //Checking Expenses Line Ret. may repeat
                            if (node.Name.Equals(QBItemReceiptList.ExpenseLineRet.ToString()))
                            {
                                checkExpNullEntries(node, ref ItemReceiptList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                    {
                                        ItemReceiptList.ExpensesLineTxnLineID.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ExpensesLineAccountRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
                                    {
                                        ItemReceiptList.ExpensesLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Memo.ToString()))
                                    {
                                        ItemReceiptList.ExpensesLineMemo.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.CurrencyRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ExpensesLineCustomerRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ExpensesLineClassRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
                                    {
                                        ItemReceiptList.ExpensesLineBillableStatus.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ExpensesLineSalesRepRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }

                                    if (childNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
                                    {

                                        //foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        //{
                                        //    if (subchildNode.Name.Equals(QBItemReceiptList.ExpensesLineDataExtDataExtName.ToString()))
                                        //        ItemReceiptList.ExpensesLineDataExtDataExtName[count7] = subchildNode.InnerText;
                                        //    if (subchildNode.Name.Equals(QBItemReceiptList.ExpensesLineDataExtDataExtValue.ToString()))
                                        //        ItemReceiptList.ExpensesLineDataExtDataExtValue[count7] = subchildNode.InnerText;
                                        //    if (subchildNode.Name.Equals(QBItemReceiptList.ExpensesLineDataExtDataExtType.ToString()))
                                        //        ItemReceiptList.ExpensesLineDataExtDataExtType[count7] = subchildNode.InnerText;
                                        //}
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            ItemReceiptList.ExpensesLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                ItemReceiptList.ExpensesLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtType").InnerText))
                                                ItemReceiptList.ExpensesLineDataExtDataExtType.Add(childNode.SelectSingleNode("./DataExtType").InnerText);
                                        }
                                        i++;
                                    }
                                    count1++;
                                }
                            }

                            //ItemLineRet may repeat
                            if (node.Name.Equals(QBItemReceiptList.ItemLineRet.ToString()))
                            {
                                checkNullEntries(node, ref ItemReceiptList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                    {
                                        ItemReceiptList.ItemLineTxnLineID.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineItemRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineInventorySiteLocationRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.SerialNumber.ToString()))
                                    {
                                        ItemReceiptList.ItemLineSerialNumber.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.LotNumber.ToString()))
                                    {
                                        ItemReceiptList.ItemLineLotNumber.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
                                    {
                                        ItemReceiptList.ItemLineDesc.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
                                    {
                                        ItemReceiptList.ItemLineQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
                                    {
                                        ItemReceiptList.ItemLineUOM.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineOverrideUOMSetRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Cost.ToString()))
                                    {
                                        ItemReceiptList.ItemLineCost.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
                                    {
                                        ItemReceiptList.ItemLineAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.CustomerRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineCustomerRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineClassRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
                                    {
                                        ItemReceiptList.ItemLineBillableStatus.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.ItemLineSalesRepRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
                                    {
                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            ItemReceiptList.ItemLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                ItemReceiptList.ItemLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtType").InnerText))
                                                ItemReceiptList.ItemLineDataExtDataExtType.Add(childNode.SelectSingleNode("./DataExtType").InnerText);
                                        }
                                        i++;
                                    }
                                }
                                count2++;
                            }
                            if (node.Name.Equals(QBItemReceiptList.ItemGroupLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                    {
                                        ItemReceiptList.GroupLineTxnLineID.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.GroupLineItemGroupRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
                                    {
                                        ItemReceiptList.GroupLineDesc.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
                                    {
                                        ItemReceiptList.GroupLineQuantity.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
                                    {
                                        ItemReceiptList.GroupLineUnitOfMeasure.Add(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                ItemReceiptList.GroupLineOverrideUOMSetRefFullName.Add(subchildNode.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBItemReceiptList.TotalAmount.ToString()))
                                    {
                                        ItemReceiptList.GroupLineTotalAmount.Add(Convert.ToDecimal(childNode.InnerText));
                                    }

                                    if (childNode.Name.Equals(QBItemReceiptList.ItemLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref ItemReceiptList);

                                        foreach (XmlNode subchildNode in childNode.ChildNodes)
                                        {
                                            if (subchildNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                                ItemReceiptList.GroupLineItemLineTxnLineID.Add(subchildNode.InnerText);


                                            if (childNode.Name.Equals(QBItemReceiptList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineItemRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }
                                            if (childNode.Name.Equals(QBItemReceiptList.InventorySiteLocationRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }
                                            if (subchildNode.Name.Equals(QBItemReceiptList.SerialNumber.ToString()))
                                                ItemReceiptList.GroupLineItemLineSerialNumber.Add(subchildNode.InnerText);

                                            if (subchildNode.Name.Equals(QBItemReceiptList.LotNumber.ToString()))
                                                ItemReceiptList.GroupLineItemLineLotNumber.Add(subchildNode.InnerText);

                                            if (subchildNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
                                                ItemReceiptList.GroupLineItemLineDesc.Add(subchildNode.InnerText);

                                            if (subchildNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
                                                ItemReceiptList.GroupLineItemLineQuantity.Add(Convert.ToDecimal(subchildNode.InnerText));

                                            if (subchildNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
                                                ItemReceiptList.GroupLineItemLineUOM.Add(subchildNode.InnerText);

                                            if (subchildNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }
                                            if (subchildNode.Name.Equals(QBItemReceiptList.Cost.ToString()))
                                                ItemReceiptList.GroupLineItemLineCost.Add(Convert.ToDecimal(subchildNode.InnerText));

                                            if (subchildNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
                                                ItemReceiptList.GroupLineItemLineAmount.Add(Convert.ToDecimal(subchildNode.InnerText));

                                            if (subchildNode.Name.Equals(QBItemReceiptList.CustomerRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineCustomerRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }
                                            if (subchildNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineClassRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }
                                            if (subchildNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
                                                ItemReceiptList.GroupLineItemLineBillableStatus.Add(subchildNode.InnerText);
                                            if (subchildNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
                                            {
                                                foreach (XmlNode subsubchildNode in childNode.ChildNodes)
                                                {
                                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
                                                        ItemReceiptList.GroupLineItemLineSalesRepRefFullName.Add(subsubchildNode.InnerText);

                                                }
                                            }

                                            if (subchildNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
                                            {
                                                if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    ItemReceiptList.GroupLineItemLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        ItemReceiptList.GroupLineItemLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText);
                                                    if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtType").InnerText))
                                                        ItemReceiptList.GroupLineItemLineDataExtDataExtType.Add(childNode.SelectSingleNode("./DataExtType").InnerText);
                                                }
                                                i++;
                                            }

                                        }
                                        count3++;
                                    }
                                }
                            }

                                if (node.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
                                {
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                    {
                                        ItemReceiptList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                        if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                            ItemReceiptList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                        if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtType").InnerText))
                                            ItemReceiptList.DataExtType.Add(node.SelectSingleNode("./DataExtType").InnerText);
                                    }
                                    i++;
                                }
                        #endregion
                            }

                            this.Add(ItemReceiptList);
                        }

                        else
                        {  }

                    }
                    #endregion

                }
            }

        private void checkGroupLineNullEntries(XmlNode childNode, ref ItemReceiptList itemReceiptList)
        {
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.TxnLineID.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.ItemRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineItemRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.InventorySiteLocationRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.SerialNumber.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineSerialNumber.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.LotNumber.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineLotNumber.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.Desc.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.Quantity.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.UnitOfMeasure.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.OverrideUOMSetRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.Cost.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineCost.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.Amount.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.CustomerRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineCustomerRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.ClassRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineClassRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.BillableStatus.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineBillableStatus.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.SalesRepRef.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineSalesRepRefFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBItemReceiptList.DataExtRet.ToString()) == null)
            {
                itemReceiptList.GroupLineItemLineDataExtDataExtName.Add(null);
                itemReceiptList.GroupLineItemLineDataExtDataExtValue.Add(null);
                itemReceiptList.GroupLineItemLineDataExtDataExtType.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref ItemReceiptList itemReceiptList)
        {
            if (node.SelectSingleNode("./" + QBItemReceiptList.TxnLineID.ToString()) == null)
            {
                itemReceiptList.ItemLineTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.ItemRef.ToString()) == null)
            {
                itemReceiptList.ItemLineItemRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.InventorySiteLocationRef.ToString()) == null)
            {
                itemReceiptList.ItemLineInventorySiteLocationRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.SerialNumber.ToString()) == null)
            {
                itemReceiptList.ItemLineSerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.LotNumber.ToString()) == null)
            {
                itemReceiptList.ItemLineLotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Desc.ToString()) == null)
            {
                itemReceiptList.ItemLineDesc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Quantity.ToString()) == null)
            {
                itemReceiptList.ItemLineQuantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.UnitOfMeasure.ToString()) == null)
            {
                itemReceiptList.ItemLineUOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.OverrideUOMSetRef.ToString()) == null)
            {
                itemReceiptList.ItemLineOverrideUOMSetRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Cost.ToString()) == null)
            {
                itemReceiptList.ItemLineCost.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Amount.ToString()) == null)
            {
                itemReceiptList.ItemLineAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.CustomerRef.ToString()) == null)
            {
                itemReceiptList.ItemLineCustomerRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.ClassRef.ToString()) == null)
            {
                itemReceiptList.ItemLineClassRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.BillableStatus.ToString()) == null)
            {
                itemReceiptList.ItemLineBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.SalesRepRef.ToString()) == null)
            {
                itemReceiptList.ItemLineSalesRepRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.DataExtRet.ToString()) == null)
            {
                itemReceiptList.ItemLineDataExtDataExtName.Add(null);
                itemReceiptList.ItemLineDataExtDataExtValue.Add(null);
                itemReceiptList.ItemLineDataExtDataExtType.Add(null);
            }
        }

        private void checkExpNullEntries(XmlNode node, ref ItemReceiptList itemReceiptList)
        {
            if (node.SelectSingleNode("./" + QBItemReceiptList.TxnLineID.ToString()) == null)
            {
                itemReceiptList.ExpensesLineTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.AccountRef.ToString()) == null)
            {
                itemReceiptList.ExpensesLineAccountRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Amount.ToString()) == null)
            {
                itemReceiptList.ExpensesLineAmount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.Memo.ToString()) == null)
            {
                itemReceiptList.ExpensesLineMemo.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.CurrencyRef.ToString()) == null)
            {
                itemReceiptList.ExpensesLineCustomerRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.ClassRef.ToString()) == null)
            {
                itemReceiptList.ExpensesLineClassRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.BillableStatus.ToString()) == null)
            {
                itemReceiptList.ExpensesLineBillableStatus.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.SalesRepRef.ToString()) == null)
            {
                itemReceiptList.ExpensesLineSalesRepRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBItemReceiptList.DataExtRet.ToString()) == null)
            {
                itemReceiptList.ExpensesLineDataExtDataExtName.Add(null);
                itemReceiptList.ExpensesLineDataExtDataExtValue.Add(null);
                itemReceiptList.ExpensesLineDataExtDataExtType.Add(null);
            }
        }

        /// <summary>
        /// This Method is used for getting item ReceiptList by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate)
            {
                string receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate);
                GetItemReceiptValueFromXML(receiptXmlList);
                return this;
            }

            //axis 12.0
            /// <summary>
            /// if no filter was selected axis 12.0
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName)
            {
                #region Getting Vehicle Mileage List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName);
                
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);
                
                return this;
            }

            /// <summary>
            /// Only Since Last Export
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="fromDate"></param>
            /// <param name="ToDate"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> ModifiedPopulateItemReceiptList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter, bool flag)
            {
                #region Getting Item Receipt List  from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, entityFilter);
               
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);
               
                         
                //Returning object.
                return this;
            }


            /// <summary>
            /// Only Since Last Export
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="fromDate"></param>
            /// <param name="ToDate"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> ModifiedPopulateItemReceiptList(string QBFileName, DateTime FromDate, string ToDate)
            {
                #region Getting Item Receipt List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate);

                GetItemReceiptValueFromXML(receiptXmlList);

                #endregion
               
                //Returning object.
                return this;
            }

            /// <summary>
            /// This method is used for getting ItemReceipt List by Filter 
            /// Entity Filter
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="PaidStatus"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptListEntityFilter(string QBFileName, List<string> entityFilter)
            {
                #region Getting ItemReceiptList List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("ItemReceiptQueryRq", QBFileName, entityFilter);

                GetItemReceiptValueFromXML(receiptXmlList);
               
                #endregion
               

                //Returning object.
                return this;
            }


            /// <summary>
            /// This Method is used for getting ItemReceiptList by Filter
            /// TxnDateRangeFilter and RefNumberRangeFilter.
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="FromDate"></param>
            /// <param name="ToDate"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
            {

                #region Getting Bills List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);
                //Returning object.
                return this;
            }


            /// <summary>
            /// This Method is used for getting Item ReceiptList by Filter
            /// TxnDateRangeFilter and RefNumberRangeFilter.
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="FromDate"></param>
            /// <param name="ToDate"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
            {

                #region Getting Bills List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
                #endregion

                GetItemReceiptValueFromXML(receiptXmlList);
                //Returning object.
                return this;
            }


            /// <summary>
            /// This Method is used for getting ItemReceipt List by Filter
            /// TxndateRangeFilter. 
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="fromDate"></param>
            /// <param name="ToDate"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
            {
                #region Getting Bill Payments List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, entityFilter);
                #endregion

                GetItemReceiptValueFromXML(receiptXmlList);

                //Returning object.
                return this;
            }


            /// <summary>
            /// This Method is used for getting BillList by Filter
            /// TxndateRangeFilter. 
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="fromDate"></param>
            /// <param name="ToDate"></param>
            /// <returns></returns>
            //public Collection<BillList> PopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate)
            //{
            //    #region Getting Bill Payments List from QuickBooks.

            //    //Getting item list from QuickBooks.
            //    string ItemReceiptXmlList = string.Empty;
            //    ItemReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("BillQueryRq", QBFileName, FromDate, ToDate);

            //    ItemReceiptList ItemReceiptList;
            //    #endregion

            //    TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            //    BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //    //Create a xml document for output result.
            //    XmlDocument outputXMLDocOfReceipt = new XmlDocument();


            //    //Getting current version of System. BUG(676)
            //    string countryName = string.Empty;
            //    countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            //    if (ItemReceiptXmlList != string.Empty)
            //    {

            //        outputXMLDocOfReceipt.LoadXml(ItemReceiptXmlList);
            //        XmlFileData = ItemReceiptXmlList;

            //        #region Getting Item Receipt Values from XML

            //        //Getting mileage values from QuickBooks response.
            //        XmlNodeList receiptNodeList = outputXMLDocOfReceipt.GetElementsByTagName(QBItemReceiptList.ItemReceiptRet.ToString());

            //        #region copy
            //        foreach (XmlNode receiptNodes in receiptNodeList)
            //        {

            //            #region For Item Receipt data
            //            if (bkWorker.CancellationPending != true)
            //            {
            //                int count = 0;
            //                int count1 = 0;
            //                int count2 = 0;
            //                int count3 = 0;

            //                ItemReceiptList = new ItemReceiptList();
            //                //ItemReceiptList.LinkedTxnTxnDate = new string[receiptNodes.ChildNodes.Count];
            //                //ItemReceiptList.LinkedTxnTxnID = new string[receiptNodes.ChildNodes.Count];
            //                //ItemReceiptList.LinkedTxnTxnType = new string[receiptNodes.ChildNodes.Count];
            //                //ItemReceiptList.LinkedTxnRefNumber = new string[receiptNodes.ChildNodes.Count];
            //                //ItemReceiptList.LinkedTxnLinkType = new string[receiptNodes.ChildNodes.Count];
            //                //ItemReceiptList.LinkedTxnAmount = new string[receiptNodes.ChildNodes.Count];
            //                //  ItemReceiptList.TxnID = new string[receiptNodes.ChildNodes.Count];

            //                ItemReceiptList.ExpensesLineTxnLineID = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineAccountRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineAmount = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineMemo = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineCustomerRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineClassRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineBillableStatus = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineSalesRepRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineDataExtDataExtName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineDataExtDataExtValue = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ExpensesLineDataExtDataExtType = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineTxnLineID = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineInventorySiteLocationRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineSerialNumber = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineLotNumber = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineDesc = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineQuantity = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineUOM = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineOverrideUOMSetRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineCost = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineAmount = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineBillableStatus = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineDataExtDataExtName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineDataExtDataExtValue = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.ItemLineDataExtDataExtType = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineTxnLineID = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemGroupRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDesc = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineQuantity = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineUnitOfMeasure = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineOverrideUOMSetRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineTotalAmount = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineTxnLineID = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemGroupRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDesc = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineQuantity = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineUnitOfMeasure = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineOverrideUOMSetRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineTotalAmount = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineTxnLineID = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineItemRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineSerialNumber = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineLotNumber = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDesc = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineQuantity = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineUOM = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineCost = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineAmount = new decimal?[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineCustomerRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineClassRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineBillableStatus = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineSalesRepRefFullName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDataExtDataExtName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDataExtDataExtValue = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.GroupLineItemLineDataExtDataExtType = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.DataExtName = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.DataExtValue = new string[receiptNodes.ChildNodes.Count];
            //                ItemReceiptList.DataExtType = new string[receiptNodes.ChildNodes.Count];
            //                foreach (XmlNode node in receiptNodes.ChildNodes)
            //                {

            //                    //Checking TxnID. 
            //                    if (node.Name.Equals(QBItemReceiptList.TxnID.ToString()))
            //                    {
            //                        ItemReceiptList.TxnID = node.InnerText;
            //                    }

            //                    //Checking TxnNumber .
            //                    if (node.Name.Equals(QBItemReceiptList.TxnNumber.ToString()))
            //                    { ItemReceiptList.TxnNumber = node.InnerText; }

            //                    //Checking vendorref .


            //                    //Checking TxnDate .
            //                    if (node.Name.Equals(QBItemReceiptList.TxnDate.ToString()))
            //                    {
            //                        try
            //                        {
            //                            DateTime dttxn = Convert.ToDateTime(node.InnerText);
            //                            if (countryName == "United States")
            //                            {
            //                                ItemReceiptList.TxnDate = dttxn.ToString("MM/dd/yyyy");
            //                            }
            //                            else
            //                            {
            //                                ItemReceiptList.TxnDate = dttxn.ToString("dd/MM/yyyy");
            //                            }
            //                        }
            //                        catch
            //                        {
            //                            ItemReceiptList.TxnDate = node.InnerText;
            //                        }
            //                    }

            //                    //Checking VenderRefFullName.
            //                    if (node.Name.Equals(QBItemReceiptList.VendorRef.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                ItemReceiptList.VenderRefFullName = childNode.InnerText;
            //                        }
            //                    }
            //                    //Checking APAccountRefFullName.
            //                    if (node.Name.Equals(QBItemReceiptList.APAccountRef.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                ItemReceiptList.APAccountRefFullName = childNode.InnerText;
            //                        }
            //                    }

            //                    //Checking LiabilityAccountRefFullName.
            //                    if (node.Name.Equals(QBItemReceiptList.LiabilityAccountRef.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                ItemReceiptList.LiabilityAccountRefFullName = childNode.InnerText;
            //                        }
            //                    }

            //                    //Checking TotalAmount .
            //                    if (node.Name.Equals(QBItemReceiptList.TotalAmount.ToString()))
            //                    {
            //                        ItemReceiptList.TotalAmount = node.InnerText;
            //                    }
            //                    //Checking LiabilityAccountRefFullName.
            //                    if (node.Name.Equals(QBItemReceiptList.CurrencyRef.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                ItemReceiptList.CurrencyRefFullName = childNode.InnerText;
            //                        }
            //                    }

            //                    //Checking ExchangeRate .
            //                    if (node.Name.Equals(QBItemReceiptList.ExchangeRate.ToString()))
            //                    {
            //                        ItemReceiptList.ExchangeRate = node.InnerText;
            //                    }
            //                    //Checking TotalAmountInHomeCurrency .
            //                    if (node.Name.Equals(QBItemReceiptList.TotalAmountInHomeCurrency.ToString()))
            //                    {
            //                        ItemReceiptList.TotalAmountInHomeCurrency = node.InnerText;
            //                    }
            //                    //Checking RefNumber .
            //                    if (node.Name.Equals(QBItemReceiptList.RefNumber.ToString()))
            //                    {
            //                        ItemReceiptList.RefNumber = node.InnerText;
            //                    }
            //                    //Checking Memo .
            //                    if (node.Name.Equals(QBItemReceiptList.Memo.ToString()))
            //                    {
            //                        ItemReceiptList.Memo = node.InnerText;
            //                    }
            //                    //Checking ExternalGUID .
            //                    if (node.Name.Equals(QBItemReceiptList.ExternalGUID.ToString()))
            //                    {
            //                        ItemReceiptList.ExternalGUID = node.InnerText;
            //                    }

            //                    //Checking LinkedTxn. may repeat
            //                    if (node.Name.Equals(QBItemReceiptList.LinkedTxn.ToString()))
            //                    {

            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {


            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnID.ToString()))
            //                            {
            //                                ItemReceiptList.LinkedTxnTxnID = childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnDate.ToString()))
            //                            {
            //                                try
            //                                {
            //                                    DateTime dttxn = Convert.ToDateTime(childNode.InnerText);
            //                                    if (countryName == "United States")
            //                                    {
            //                                        ItemReceiptList.LinkedTxnTxnDate = dttxn.ToString("MM/dd/yyyy");
            //                                    }
            //                                    else
            //                                    {
            //                                        ItemReceiptList.LinkedTxnTxnDate = dttxn.ToString("dd/MM/yyyy");
            //                                    }
            //                                }
            //                                catch
            //                                {
            //                                    ItemReceiptList.LinkedTxnTxnDate = childNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnType.ToString()))
            //                            {
            //                                ItemReceiptList.LinkedTxnTxnType = childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.RefNumber.ToString()))
            //                            {
            //                                ItemReceiptList.LinkedTxnRefNumber = childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.LinkType.ToString()))
            //                            {
            //                                ItemReceiptList.LinkedTxnLinkType = childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
            //                            {
            //                                ItemReceiptList.LinkedTxnAmount = childNode.InnerText;
            //                            }

            //                        }
            //                        count++;
            //                    }
            //                    //Checking Expenses Line Ret. may repeat
            //                    if (node.Name.Equals(QBItemReceiptList.ExpenseLineRet.ToString()))
            //                    {

            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
            //                            {
            //                                ItemReceiptList.ExpensesLineTxnLineID.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.AccountRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ExpensesLineTxnLineID.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
            //                            {
            //                                ItemReceiptList.ExpensesLineAmount.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Memo.ToString()))
            //                            {
            //                                ItemReceiptList.ExpensesLineMemo.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.CurrencyRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ExpensesLineCustomerRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ExpensesLineClassRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
            //                            {
            //                                ItemReceiptList.ExpensesLineBillableStatus.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ExpensesLineSalesRepRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
            //                            {
            //                                if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
            //                                {
            //                                    ItemReceiptList.ExpensesLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText;
            //                                    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
            //                                        ItemReceiptList.ExpensesLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText;
            //                                    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtType").InnerText))
            //                                        ItemReceiptList.ExpensesLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtType").InnerText;
            //                                }
            //                                i++;
            //                            }
            //                            count1++;
            //                        }
            //                    }

            //                    //ItemLineRet may repeat
            //                    if (node.Name.Equals(QBItemReceiptList.ItemLineRet.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineTxnLineID.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.ItemRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineItemRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.InventorySiteLocationRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineInventorySiteLocationRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.SerialNumber.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineSerialNumber.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.LotNumber.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineLotNumber.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineDesc.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineQuantity.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineUOM.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineOverrideUOMSetRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Cost.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineCost.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineAmount.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.CustomerRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineCustomerRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineClassRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
            //                            {
            //                                ItemReceiptList.ItemLineBillableStatus.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.ItemLineSalesRepRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
            //                            {
            //                                if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
            //                                {
            //                                    ItemReceiptList.ExpensesLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText;
            //                                    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
            //                                        ItemReceiptList.ExpensesLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText;
            //                                    if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtType").InnerText))
            //                                        ItemReceiptList.ExpensesLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtType").InnerText;
            //                                }
            //                                i++;
            //                            }

            //                        }
            //                        count2++;
            //                    }
            //                    if (node.Name.Equals(QBItemReceiptList.ItemGroupLineRet.ToString()))
            //                    {
            //                        foreach (XmlNode childNode in node.ChildNodes)
            //                        {
            //                            if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
            //                            {
            //                                ItemReceiptList.GroupLineTxnLineID.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.ItemGroupRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.GroupLineItemGroupRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
            //                            {
            //                                ItemReceiptList.GroupLineItemLineDesc.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
            //                            {
            //                                ItemReceiptList.GroupLineItemLineQuantity.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
            //                            {
            //                                ItemReceiptList.GroupLineUnitOfMeasure.Add(childNode.InnerText;
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                        ItemReceiptList.GroupLineOverrideUOMSetRefFullName.Add(subchildNode.InnerText;
            //                                }
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.TotalAmount.ToString()))
            //                            {
            //                                ItemReceiptList.GroupLineTotalAmount.Add(Convert.ToDecimal(childNode.InnerText);
            //                            }
            //                            if (childNode.Name.Equals(QBItemReceiptList.ItemLineRet.ToString()))
            //                            {
            //                                foreach (XmlNode subchildNode in childNode.ChildNodes)
            //                                {
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineTxnLineID.Add(subchildNode.InnerText;


            //                                    if (childNode.Name.Equals(QBItemReceiptList.ItemRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineItemRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }
            //                                    if (childNode.Name.Equals(QBItemReceiptList.InventorySiteLocationRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.SerialNumber.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineSerialNumber.Add(subchildNode.InnerText;

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.LotNumber.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineLotNumber.Add(subchildNode.InnerText;

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.Desc.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineDesc.Add(subchildNode.InnerText;

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.Quantity.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineQuantity.Add(Convert.ToDecimal(subchildNode.InnerText);

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.UnitOfMeasure.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineUOM.Add(subchildNode.InnerText;

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.OverrideUOMSetRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineInventorySiteLocationRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.Cost.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineCost.Add(Convert.ToDecimal(subchildNode.InnerText);

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.Amount.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineAmount.Add(Convert.ToDecimal(subchildNode.InnerText);

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.CustomerRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineCustomerRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.ClassRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineClassRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.BillableStatus.ToString()))
            //                                        ItemReceiptList.GroupLineItemLineBillableStatus.Add(subchildNode.InnerText;
            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.SalesRepRef.ToString()))
            //                                    {
            //                                        foreach (XmlNode subsubchildNode in childNode.ChildNodes)
            //                                        {
            //                                            if (subchildNode.Name.Equals(QBItemReceiptList.FullName.ToString()))
            //                                                ItemReceiptList.GroupLineItemLineSalesRepRefFullName.Add(subsubchildNode.InnerText;

            //                                        }
            //                                    }

            //                                    if (subchildNode.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
            //                                    {
            //                                        if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtName").InnerText))
            //                                        {
            //                                            ItemReceiptList.GroupLineItemLineDataExtDataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText;
            //                                            if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtValue").InnerText))
            //                                                ItemReceiptList.GroupLineItemLineDataExtDataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText;
            //                                            if (!string.IsNullOrEmpty(subchildNode.SelectSingleNode("./DataExtType").InnerText))
            //                                                ItemReceiptList.GroupLineItemLineDataExtDataExtType.Add(childNode.SelectSingleNode("./DataExtType").InnerText;
            //                                        }
            //                                        i++;
            //                                    }

            //                                }
            //                                count3++;
            //                            }
            //                        }



            //                    }

            //                    if (node.Name.Equals(QBItemReceiptList.DataExtRet.ToString()))
            //                    {
            //                        if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
            //                        {
            //                            ItemReceiptList.GroupLineItemLineDataExtDataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText;
            //                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
            //                                ItemReceiptList.GroupLineItemLineDataExtDataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText;
            //                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtType").InnerText))
            //                                ItemReceiptList.GroupLineItemLineDataExtDataExtType.Add(node.SelectSingleNode("./DataExtType").InnerText;
            //                        }
            //                        i++;
            //                    }
            //            #endregion
            //                }

            //                this.Add(ItemReceiptList);
            //            }

            //            else
            //            { return null; }

            //        }
            //        #endregion

            //        //Removing all the references from OutPut Xml document.
            //        outputXMLDocOfReceipt.RemoveAll();
            //        #endregion
            //    }

            //    //Returning object.
            //    return this;
            //}


            /// <summary>
            /// This Method is used for getting BillList by Filter
            /// RefNnumberRangeFilter,entity filter
            /// </summary>
            /// <param name="QbFileName"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
            {

                #region Getting Bills List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);
                //Returning object.
                return this;
            }



            /// <summary>
            /// This Method is used for getting BillList by Filter
            /// RefNnumberRangeFilter.
            /// </summary>
            /// <param name="QbFileName"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> PopulateItemReceiptList(string QBFileName, string FromRefnum, string ToRefnum)
            {

                #region Getting Bills List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromRefnum, ToRefnum);
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);

                //Returning object.
                return this;
            }
               

            /// <summary>
            /// This Method is used for getting BillList by Filter
            /// TxnDateRangeFilter and RefNumberRangeFilter.
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="FromDate"></param>
            /// <param name="ToDate"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> ModifiedPopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
            {

                #region Getting Item Receipt List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
                #endregion
                GetItemReceiptValueFromXML(receiptXmlList);

                //Returning object.
                return this;
            }

            /// <summary>
            /// This Method is used for getting BillList by Filter
            /// TxnDateRangeFilter and RefNumberRangeFilter.
            /// </summary>
            /// <param name="QBFileName"></param>
            /// <param name="FromDate"></param>
            /// <param name="ToDate"></param>
            /// <param name="FromRefnum"></param>
            /// <param name="ToRefnum"></param>
            /// <returns></returns>
            public Collection<ItemReceiptList> ModifiedPopulateItemReceiptList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
            {

                #region Getting ItemReceipt List from QuickBooks.

                //Getting item list from QuickBooks.
                string receiptXmlList = string.Empty;
                receiptXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
                #endregion

                GetItemReceiptValueFromXML(receiptXmlList);


                //Returning object.
                return this;
            }


            /// <summary>
            /// This method is used to get the xml response from the QuikBooks.
            /// </summary>
            /// <returns></returns>
            public string GetXmlFileData()
            {
                return XmlFileData;
            }

        //Axis 706
        public List<string> PopulateItemReceiptTxnIdAndGroupTxnIdInSequence(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting ItemReceipt List from QuickBooks.

            //Getting item list from QuickBooks.
            List<string> ItemReceiptTxnIdAndGroupTxnId = new List<string>();
            string ItemReceiptXmlList = string.Empty;
            ItemReceiptXmlList = QBCommonUtilities.GetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromRefnum, ToRefnum);

            ItemReceiptList ItemReceiptList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItemReceipt = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ItemReceiptXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfItemReceipt.LoadXml(ItemReceiptXmlList);
                XmlFileData = ItemReceiptXmlList;

                #region Getting ItemReceipt Values from XML

                //Getting ItemReceipt values from QuickBooks response.
                XmlNodeList ItemReceiptNodeList = outputXMLDocOfItemReceipt.GetElementsByTagName(QBItemReceiptList.ItemReceiptRet.ToString());

                foreach (XmlNode ItemReceiptNodes in ItemReceiptNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        ItemReceiptList = new ItemReceiptList();
                        foreach (XmlNode node in ItemReceiptNodes.ChildNodes)
                        {
                            //Checking TxnID. 
                            if (node.Name.Equals(QBItemReceiptList.ItemLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                    {
                                        ItemReceiptList.TxnID = childNode.InnerText;
                                        ItemReceiptTxnIdAndGroupTxnId.Add(ItemReceiptList.TxnID);
                                    }
                                }
                            }

                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBItemReceiptList.ItemGroupLineRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBItemReceiptList.ItemGroupRef.ToString()) == null)
                                {
                                    ItemReceiptList.GroupLineItemGroupRefFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBItemReceiptList.Desc.ToString()) == null)
                                {
                                    ItemReceiptList.GroupLineDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBItemReceiptList.Quantity.ToString()) == null)
                                {
                                    ItemReceiptList.GroupLineQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBItemReceiptList.UnitOfMeasure.ToString()) == null)
                                {
                                    ItemReceiptList.GroupLineItemLineUOM.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBItemReceiptList.TxnLineID.ToString()))
                                    {
                                        string GroupId = "";
                                        if (childNode.InnerText.Length > 36)
                                            GroupId = (childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            GroupId = childNode.InnerText;

                                        ItemReceiptTxnIdAndGroupTxnId.Add("GroupTxnLineID_" + GroupId);

                                    }
                                }
                            }
                            //

                        }
                    }
                }
                //Adding Invoice list.
                #endregion
            }
            return ItemReceiptTxnIdAndGroupTxnId;
        }
        //Axis 706 ends


    }
}

       