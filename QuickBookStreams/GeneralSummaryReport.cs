using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    public class GeneralSummaryReport : BaseGeneralSummaryreport
    {
        #region Public Properties

        public string ReportTitle
        {
            get { return m_ReportTitle; }
            set { m_ReportTitle = value; }
        }

        public string ReportSubtitle
        {
            get { return m_ReportSubtitle; }
            set { m_ReportSubtitle = value; }
        }

        public string ReportBasis
        {
            get { return m_ReportBasis; }
            set { m_ReportBasis = value; }
        }

        public string NumRows
        {
            get { return m_NumRows; }
            set { m_NumRows = value; }
        }

        public string NumColumns
        {
            get { return m_NumColumns; }
            set { m_NumColumns = value; }
        }

        public string NumColTitleRows
        {
            get { return m_NumColTitleRows; }
            set { m_NumColTitleRows = value; }
        }

        public string[] ColDescColTitle
        {
            get { return m_ColDescColTitle; }
            set { m_ColDescColTitle = value; }
        }

        public string[] ColDescColType
        {
            get { return m_ColDescColType; }
            set { m_ColDescColType = value; }
        }

        public string[] DataRowRowData
        {
            get { return m_DataRowRowData; }
            set { m_DataRowRowData = value; }
        }

        public string[] DataRowColData
        {
            get { return m_DataRowColData; }
            set { m_DataRowColData = value; }
        }

        public string[] DataRowColID
        {
            get { return m_DataRowColID; }
            set { m_DataRowColID = value; }
        }

        public string[] TextRow
        {
            get { return m_TextRow; }
            set { m_TextRow = value; }
        }

        public string[] SubtotalRowRowData
        {
            get { return m_SubtotalRowRowData; }
            set { m_SubtotalRowRowData = value; }
        }

        public string[] SubtotalRowColData
        {
            get { return m_SubtotalRowColData; }
            set { m_SubtotalRowColData = value; }
        }

        public string[] SubtotalRowColID
        {
            get { return m_SubtotalRowColID; }
            set { m_SubtotalRowColID = value; }
        }


        public string[] TotalRowRowData
        {
            get { return m_TotalRowRowData; }
            set { m_TotalRowRowData = value; }
        }

        public string[] TotalRowColData
        {
            get { return m_TotalRowColData; }
            set { m_TotalRowColData = value; }
        }

        public string[] TotalRowColID
        {
            get { return m_TotalRowColID; }
            set { m_TotalRowColID = value; }
        }

        #endregion
    }

    /// <summary>
    /// This class is used to get General Summary Report from QuickBook.
    /// </summary>
    public class GeneralSummaryReportCollection : Collection<GeneralSummaryReport>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<GeneralSummaryReport> PopulateGeneralSummaryReportList(string QBFileName)
        {
            #region Getting PopulateGeneralSummaryReportList List from QuickBooks.

            //Getting PopulateGeneralSummaryReportList from QuickBooks.
            string SummaryReportXmlList = string.Empty;
            SummaryReportXmlList = QBCommonUtilities.GetListFromQuickBook("GeneralSummaryReportQueryRq", QBFileName);

            GeneralSummaryReport GeneralSummaryReport;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLSummaryReport = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (SummaryReportXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLSummaryReport.LoadXml(SummaryReportXmlList);
                XmlFileData = SummaryReportXmlList;

                #region Getting GeneralSummaryReport Values from XML

                //Getting GeneralSummaryReport values from QuickBooks response.
                XmlNodeList SummaryReportNodeList = outputXMLSummaryReport.GetElementsByTagName(QBGeneralSummaryReport.ReportRet.ToString());

                foreach (XmlNode SummaryReportNodes in SummaryReportNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;

                        GeneralSummaryReport = new GeneralSummaryReport();

                        foreach (XmlNode node in SummaryReportNodes.ChildNodes)
                        {
                           
                            //Checking NumRows
                            if (node.Name.Equals(QBGeneralSummaryReport.NumRows.ToString()))
                            {
                                GeneralSummaryReport.NumRows = node.InnerText;
                            }

                            //Checking NumColumns
                            if (node.Name.Equals(QBGeneralSummaryReport.NumColumns.ToString()))
                            {
                                GeneralSummaryReport.NumColumns = node.InnerText;
                            }

                          

                            //Checking ColDesc
                            if (node.Name.Equals(QBGeneralSummaryReport.ColDesc.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking ColTitle
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.ColTitle.ToString()))
                                    {
                                        if (childNode.Attributes["value"] != null)
                                            if (!string.IsNullOrEmpty(childNode.Attributes["value"].Value.ToString()))
                                            {
                                                GeneralSummaryReport.ColDescColTitle[count] = GeneralSummaryReport.ColDescColTitle[count] + " " + childNode.Attributes["value"].Value.ToString();
                                            }
                                    }
                                   
                                }
                                count++;
                            }

                            //ReportData
                            if (node.Name.Equals(QBGeneralSummaryReport.ReportData.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //DataRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.DataRow.ToString()))
                                    {
                                        //count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.RowData.ToString()))
                                                GeneralSummaryReport.DataRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.DataRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.DataRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();
                                           
                                            
                                            count1++;
                                        }
                                    }


                                    ////TextRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.TextRow.ToString()))
                                    {
                                        if (childNode.Attributes.Item(1) != null)
                                        {
                                            GeneralSummaryReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = childNode.Attributes["value"].Value.ToString();
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) == 1)
                                                GeneralSummaryReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = "";
                                        }
                                    
                                    }

                                    ////SubtotalRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.SubtotalRow.ToString()))
                                    {
                             
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                           
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.SubtotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.SubtotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();
                                            count1++;
                                        }
                                    }

                                    ////TotalRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.TotalRow.ToString()))
                                    {
                                        //count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                           
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.TotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();
                                            
                                            
                                            count1++;
                                        }
                                    }
                                }
                            }
                        }
                        this.Add(GeneralSummaryReport);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLSummaryReport.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// DateRange and Report type selected.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<GeneralSummaryReport> PopulateGeneralSummaryReportList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting PopulateGeneralSummaryReportList List from QuickBooks.

            //Getting PopulateGeneralSummaryReportList from QuickBooks.
            string SummaryReportXmlList = string.Empty;
            SummaryReportXmlList = QBCommonUtilities.GetListFromQuickBookByDateGeneralReport("GeneralSummaryReportQueryRq", QBFileName, FromDate, ToDate);

            GeneralSummaryReport GeneralSummaryReport;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLSummaryReport = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (SummaryReportXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLSummaryReport.LoadXml(SummaryReportXmlList);
                XmlFileData = SummaryReportXmlList;               

                #region Getting GeneralSummaryReport Values from XML

                //Getting GeneralSummaryReport values from QuickBooks response.
                XmlNodeList SummaryReportNodeList = outputXMLSummaryReport.GetElementsByTagName(QBGeneralSummaryReport.ReportRet.ToString());

                foreach (XmlNode SummaryReportNodes in SummaryReportNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;

                        GeneralSummaryReport = new GeneralSummaryReport();

                        foreach (XmlNode node in SummaryReportNodes.ChildNodes)
                        {
                            
                            //Checking NumRows
                            if (node.Name.Equals(QBGeneralSummaryReport.NumRows.ToString()))
                            {
                                GeneralSummaryReport.NumRows = node.InnerText;
                            }

                            //Checking NumColumns
                            if (node.Name.Equals(QBGeneralSummaryReport.NumColumns.ToString()))
                            {
                                GeneralSummaryReport.NumColumns = node.InnerText;
                            }

                           
                            //Checking ColDesc
                            if (node.Name.Equals(QBGeneralSummaryReport.ColDesc.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking ColTitle
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.ColTitle.ToString()))
                                    {
                                        if (childNode.Attributes["value"] != null)
                                            if (!string.IsNullOrEmpty(childNode.Attributes["value"].Value.ToString()))
                                            {
                                                GeneralSummaryReport.ColDescColTitle[count] = GeneralSummaryReport.ColDescColTitle[count] + " " + childNode.Attributes["value"].Value.ToString();
                                            }
                                    }
                                 

                                }
                                count++;
                            }

                            //ReportData
                            if (node.Name.Equals(QBGeneralSummaryReport.ReportData.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //DataRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.DataRow.ToString()))
                                    {
                                        //count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.RowData.ToString()))
                                                GeneralSummaryReport.DataRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.DataRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.DataRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();


                                            count1++;
                                        }
                                    }


                                    ////TextRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.TextRow.ToString()))
                                    {
                                        if (childNode.Attributes.Item(1) != null)
                                        {
                                            GeneralSummaryReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = childNode.Attributes["value"].Value.ToString();
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) == 1)
                                                GeneralSummaryReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = "";
                                        }
                                     
                                    }

                                    ////SubtotalRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.SubtotalRow.ToString()))
                                    {
                                        //   count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                           
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.SubtotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.SubtotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();
                                            count1++;
                                        }
                                    }

                                    ////TotalRow
                                    if (childNode.Name.Equals(QBGeneralSummaryReport.TotalRow.ToString()))
                                    {                                     
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {                                           
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();
                                            if (childNodes.Name.Equals(QBGeneralSummaryReport.ColData.ToString()))
                                                GeneralSummaryReport.TotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();


                                            count1++;
                                        }
                                    }
                                }
                            }
                        }
                        this.Add(GeneralSummaryReport);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLSummaryReport.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

    }
}
