﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
    public class ClassList : BaseClassList
    {
        #region Public Properties
        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string ParentRefFullName
        {
            get { return m_ParentRefFullName; }
            set { m_ParentRefFullName = value; }
        }
        public string Sublevel
        {
            get { return m_Sublevel; }
            set
            {
                m_Sublevel = value;
            }
        }
        #endregion
    }

    public class QBClassListCollection : Collection<ClassList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        public void GetClassValuesfromXML(string ClassXmlList)
        {
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfClass = new XmlDocument();

            ClassList ClassList;

            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ClassXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfClass.LoadXml(ClassXmlList);
                XmlFileData = ClassXmlList;

                #region Getting Class Values from XML

                //Getting Class values from QuickBooks response.
                XmlNodeList ClassNodeList = outputXMLDocOfClass.GetElementsByTagName(QBClassList.ClassRet.ToString());

                foreach (XmlNode ClassNodes in ClassNodeList)
                {
                    i = 0;
                    #region For Class data
                    if (bkWorker.CancellationPending != true)
                    {
                        ClassList classList = new ClassList();
                        foreach (XmlNode node in ClassNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlClassList.ListID.ToString()))
                                classList.ListID = node.InnerText;
                            if (node.Name.Equals(QBXmlClassList.Name.ToString()))
                                classList.Name = node.InnerText;
                            if (node.Name.Equals(QBXmlClassList.ClassRet.ToString()))
                                classList.ParentRefFullName = node.InnerText;
                            if (node.Name.Equals(QBXmlClassList.IsActive.ToString()))
                                classList.IsActive = node.InnerText;
                            if (node.Name.Equals(QBXmlClassList.Sublevel.ToString()))
                                classList.Sublevel = node.InnerText;

                            if (node.Name.Equals(QBXmlClassList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlClassList.FullName.ToString()))
                                        classList.ParentRefFullName = childNode.InnerText;
                                }
                            }
                        }
                        this.Add(classList);
                    }
                    else
                    { }
                    #endregion
                }

                outputXMLDocOfClass.RemoveAll();
                #endregion
            }
        }


        /// <summary>
        /// this method returns all Vendors in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllClassList(string QBFileName)
        {
            string classXmlList = QBCommonUtilities.GetListFromQuickBook("ClassQueryRq", QBFileName);

            List<string> classList = new List<string>();
            List<string> classList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(classXmlList);

            XmlFileData = classXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList ClassNodeList = outputXMLDoc.GetElementsByTagName(QBClassList.ClassRet.ToString());

            foreach (XmlNode clsNodes in ClassNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in clsNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBClassList.FullName.ToString()))
                        {
                            if (!classList.Contains(node.InnerText))
                            {
                                classList.Add(node.InnerText);
                                string name = node.InnerText;
                                classList1.Add(name);
                            }
                        }
                    }
                }
            }
            
            classXmlList = null;
            ClassNodeList = null;
            XmlFileData = null;
            outputXMLDoc = null;
            return classList1;
        }

        /// <summary>
        /// If no filter selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>

        public Collection<ClassList> PopulateClassList(string QBFileName, bool inActiveFilter)
        {
            string classXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBook("ClassQueryRq", QBFileName);
            }
            else
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("ClassQueryRq", QBFileName);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(classXmlList);
            XmlFileData = classXmlList;
           
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList ClassNodeList = outputXMLDoc.GetElementsByTagName(QBXmlClassList.ClassRet.ToString());
            foreach (XmlNode ClassNodes in ClassNodeList)
            {
                i = 0;
                #region For Class data
                if (bkWorker.CancellationPending != true)
                {
                    ClassList classList = new ClassList();
                    foreach (XmlNode node in ClassNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlClassList.ListID.ToString()))
                            classList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Name.ToString()))
                            classList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.ClassRet.ToString()))
                            classList.ParentRefFullName = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.IsActive.ToString()))
                            classList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Sublevel.ToString()))
                            classList.Sublevel = node.InnerText;

                        if (node.Name.Equals(QBXmlClassList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlClassList.FullName.ToString()))
                                    classList.ParentRefFullName = childNode.InnerText;
                            }
                        }
                    }
                    this.Add(classList);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }

        
        public Collection<ClassList> PopulateClassList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            string classXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBookByDate("ClassQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ClassQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(classXmlList);
            XmlFileData = classXmlList;
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList classNodeList = outputXMLDoc.GetElementsByTagName(QBXmlClassList.ClassRet.ToString());
            foreach (XmlNode classNodes in classNodeList)
            {
                i = 0;
                if (bkWorker.CancellationPending != true)
                {
                    ClassList classList = new ClassList();
                    foreach (XmlNode node in classNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlClassList.ListID.ToString()))
                            classList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Name.ToString()))
                            classList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.IsActive.ToString()))
                            classList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Sublevel.ToString()))
                            classList.Sublevel = node.InnerText;
                        
                        if (node.Name.Equals(QBXmlClassList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlClassList.FullName.ToString()))
                                    classList.ParentRefFullName = childNode.InnerText;
                            }
                        }
                        
                    }
                    this.Add(classList);
                }
                else
                { return null; }
            }

            return this;
        }

       
        public Collection<ClassList> PopulateClassList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            string classXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBookByDate("ClassQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                classXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ClassQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(classXmlList);
            XmlFileData = classXmlList;
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList classNodeList = outputXMLDoc.GetElementsByTagName(QBXmlClassList.ClassRet.ToString());
            foreach (XmlNode classNodes in classNodeList)
            {
                i = 0;
                if (bkWorker.CancellationPending != true)
                {
                    ClassList classList = new ClassList();
                    foreach (XmlNode node in classNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlClassList.ListID.ToString()))
                            classList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Name.ToString()))
                            classList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.IsActive.ToString()))
                            classList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlClassList.Sublevel.ToString()))
                            classList.Sublevel = node.InnerText;

                        if (node.Name.Equals(QBXmlClassList.ParentRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlClassList.FullName.ToString()))
                                    classList.ParentRefFullName = childNode.InnerText;
                            }
                        }

                    }
                    this.Add(classList);
                }
                else
                { return null; }
            }

            return this;
        }

        public Collection<ClassList> PopulateClassListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            //Getting Class list from QuickBooks.
            string ClassXmlList = string.Empty;
            ClassXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("ClassQueryRq", QBFileName, entityFilter);
            GetClassValuesfromXML(ClassXmlList);
          
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

       
        #endregion

    } 
 }