using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    public class GeneralDetailReport : BaseGeneralDetailreport
    {
        #region Public Properties

        public string ReportTitle
        {
            get { return m_ReportTitle; }
            set { m_ReportTitle = value; }
        }

        public string ReportSubtitle
        {
            get { return m_ReportSubtitle; }
            set { m_ReportSubtitle = value; }
        }

        public string ReportBasis
        {
            get { return m_ReportBasis; }
            set { m_ReportBasis = value; }
        }

        public string NumRows
        {
            get { return m_NumRows; }
            set { m_NumRows = value; }
        }

        public string NumColumns
        {
            get { return m_NumColumns; }
            set { m_NumColumns = value; }
        }

        public string NumColTitleRows
        {
            get { return m_NumColTitleRows; }
            set { m_NumColTitleRows = value; }
        }

        public string[] ColDescColTitle
        {
            get { return m_ColDescColTitle; }
            set { m_ColDescColTitle = value; }
        }

        public string[] ColDescColType
        {
            get { return m_ColDescColType; }
            set { m_ColDescColType = value; }
        }

        public string[] DataRowRowData
        {
            get { return m_DataRowRowData; }
            set { m_DataRowRowData = value; }
        }

        public string[] DataRowColData
        {
            get { return m_DataRowColData; }
            set { m_DataRowColData = value; }
        }

        public string[] DataRowColID
        {
            get { return m_DataRowColID; }
            set { m_DataRowColID = value; }
        }

        public string[] TextRow
        {
            get { return m_TextRow; }
            set { m_TextRow = value; }
        }

        public string[] SubtotalRowRowData
        {
            get { return m_SubtotalRowRowData; }
            set { m_SubtotalRowRowData = value; }
        }

        public string[] SubtotalRowColData
        {
            get { return m_SubtotalRowColData; }
            set { m_SubtotalRowColData = value; }
        }

        public string[] SubtotalRowColID
        {
            get { return m_subtotalRowColID; }
            set { m_subtotalRowColID = value; }
        }

        public string[] TotalRowRowData
        {
            get { return m_TotalRowRowData; }
            set { m_TotalRowRowData = value; }
        }

        public string[] TotalRowColData
        {
            get { return m_TotalRowColData; }
            set { m_TotalRowColData = value; }
        }

        public string[] TotalRowColID
        {
            get { return m_TotalRowColID; }
            set { m_TotalRowColID = value; }
        }

        #endregion
    }

     /// <summary>
    /// This class is used to get General Detail Report from QuickBook.
    /// </summary>
    public class GeneralDetailReportCollection : Collection<GeneralDetailReport>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<GeneralDetailReport> PopulateGeneralDetailReportList(string QBFileName)
        {
            #region Getting PopulateGeneralDetailReportList List from QuickBooks.

            //Getting PopulateGeneralDetailReportList from QuickBooks.
            string DetailReportXmlList = string.Empty;
            DetailReportXmlList = QBCommonUtilities.GetListFromQuickBook("GeneralDetailReportQueryRq", QBFileName);

            GeneralDetailReport GeneralDetailReport;           
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDetailReport = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (DetailReportXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDetailReport.LoadXml(DetailReportXmlList);
                XmlFileData = DetailReportXmlList;

                #region Getting GeneralDetailReport Values from XML

                //Getting GeneralDetailReport values from QuickBooks response.
                XmlNodeList DetailReportNodeList = outputXMLDetailReport.GetElementsByTagName(QBGeneralDetailReport.ReportRet.ToString());

                foreach (XmlNode DetailReportNodes in DetailReportNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                       // int colCount = 0;
                        int rowNum = 0;

                        GeneralDetailReport = new GeneralDetailReport();

                        foreach (XmlNode node in DetailReportNodes.ChildNodes)
                        {

                           
                            ////Checking NumRows
                            if (node.Name.Equals(QBGeneralDetailReport.NumRows.ToString()))
                            {
                                GeneralDetailReport.NumRows = node.InnerText;
                            }

                            ////Checking NumColumns
                            if (node.Name.Equals(QBGeneralDetailReport.NumColumns.ToString()))
                            {
                                GeneralDetailReport.NumColumns = node.InnerText;
                            }

                          

                            //Checking ColDesc
                            if (node.Name.Equals(QBGeneralDetailReport.ColDesc.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking ColTitle
                                    if (childNode.Name.Equals(QBGeneralDetailReport.ColTitle.ToString()))
                                    {
                                        if (childNode.Attributes["value"] != null)
                                            if (!string.IsNullOrEmpty(childNode.Attributes["value"].Value.ToString()))
                                                GeneralDetailReport.ColDescColTitle[count] = childNode.Attributes["value"].Value.ToString();
                                    }
                                 

                                }
                                count++;
                            }
                            //ReportData
                            if (node.Name.Equals(QBGeneralDetailReport.ReportData.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {                                

                                    if (childNode.Name.Equals(QBGeneralDetailReport.DataRow.ToString()))
                                    {
                                        
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.DataRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }
                                   
                                    //TextRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TextRow.ToString()))
                                    {
                                        if (childNode.Attributes.Item(1) != null)
                                        {
                                            GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString())-1] = childNode.Attributes["value"].Value.ToString();
                                        }
                                        else
                                        {
                                            if(Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString())==1)
                                            GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString())-1] = "";
                                        }                                       
                                    }

                                    //SubtotalRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.SubtotalRow.ToString()))
                                    {
                                        // count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.SubtotalRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                   
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TotalRow.ToString()))
                                    {
                                        // count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                           
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();
                                            
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                }
                            }
                        }
                        this.Add(GeneralDetailReport);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLDetailReport.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// DateRange and Report type selected.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<GeneralDetailReport> PopulateGeneralDetailReportList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting PopulateGeneralDetailReportList List from QuickBooks.

            //Getting PopulateGeneralDetailReportList from QuickBooks.
            string DetailReportXmlList = string.Empty;
            DetailReportXmlList = QBCommonUtilities.GetListFromQuickBookByDate("GeneralDetailReportQueryRq", QBFileName, FromDate, ToDate);
            GeneralDetailReport GeneralDetailReport;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDetailReport = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (DetailReportXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDetailReport.LoadXml(DetailReportXmlList);
                XmlFileData = DetailReportXmlList;
                

                #region Getting GeneralDetailReport Values from XML

                //Getting GeneralDetailReport values from QuickBooks response.
                XmlNodeList DetailReportNodeList = outputXMLDetailReport.GetElementsByTagName(QBGeneralDetailReport.ReportRet.ToString());

                foreach (XmlNode DetailReportNodes in DetailReportNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                      //  int colCount = 0;
                        int rowNum = 0;

                        GeneralDetailReport = new GeneralDetailReport();

                        foreach (XmlNode node in DetailReportNodes.ChildNodes)
                        {

                            //Checking ReportTitle. 
                            //if (node.Name.Equals(QBGeneralDetailReport.ReportTitle.ToString()))
                            //{
                            //    GeneralDetailReport.ReportTitle = node.InnerText;
                            //}

                            ////Checking ReportSubtitle.
                            //if (node.Name.Equals(QBGeneralDetailReport.ReportSubtitle.ToString()))
                            //{
                            //    GeneralDetailReport.ReportSubtitle = node.InnerText;
                            //}

                            ////Checking ReportBasis.
                            //if (node.Name.Equals(QBGeneralDetailReport.ReportBasis.ToString()))
                            //{
                            //    GeneralDetailReport.ReportBasis = node.InnerText;
                            //}

                            ////Checking NumRows
                            if (node.Name.Equals(QBGeneralDetailReport.NumRows.ToString()))
                            {
                                GeneralDetailReport.NumRows = node.InnerText;
                            }

                            ////Checking NumColumns
                            if (node.Name.Equals(QBGeneralDetailReport.NumColumns.ToString()))
                            {
                                GeneralDetailReport.NumColumns = node.InnerText;
                            }

                            ////Chekcing NumColTitleRows
                            //if (node.Name.Equals(QBGeneralDetailReport.NumColTitleRows.ToString()))
                            //{
                            //    GeneralDetailReport.NumColTitleRows = node.InnerText;
                            //}

                            //Checking ColDesc
                            if (node.Name.Equals(QBGeneralDetailReport.ColDesc.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking ColTitle
                                    if (childNode.Name.Equals(QBGeneralDetailReport.ColTitle.ToString()))
                                    {
                                        if (childNode.Attributes["value"] != null)
                                            if (!string.IsNullOrEmpty(childNode.Attributes["value"].Value.ToString()))
                                                GeneralDetailReport.ColDescColTitle[count] = childNode.Attributes["value"].Value.ToString();
                                    }
                                    //Checking ColType
                                    //if (childNode.Name.Equals(QBGeneralDetailReport.ColType.ToString()))
                                    //{
                                    //    GeneralDetailReport.ColDescColType[count] = childNode.InnerText;
                                    //}

                                }
                                count++;
                            }
                            //ReportData
                            if (node.Name.Equals(QBGeneralDetailReport.ReportData.ToString()))
                            {

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //count1 = 0;
                                    //DataRow

                                    if (childNode.Name.Equals(QBGeneralDetailReport.DataRow.ToString()))
                                    {
                                        //count1 = 0;


                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.DataRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                    //TextRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TextRow.ToString()))
                                    {
                                        if (childNode.Attributes.Item(1) != null)
                                        {
                                            GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = childNode.Attributes["value"].Value.ToString();
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) == 1)
                                                GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = "";
                                        }
                                        //if (childNode.Attributes.Item(1) != null)
                                        //{
                                        //    try
                                        //    {
                                        //        GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString())] = childNode.Attributes["value"].Value.ToString();
                                        //    }
                                        //    catch (Exception exp)
                                        //    {
                                        //        MessageBox.Show(exp.Message.ToString());
                                        //    }
                                        //}

                                        // colCount++;
                                        // count1++;
                                    }

                                    //SubtotalRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.SubtotalRow.ToString()))
                                    {
                                        // count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.SubtotalRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                    //////TotalRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TotalRow.ToString()))
                                    {
                                        // count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            //if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                            //    GeneralDetailReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                }
                            }
                        }
                        this.Add(GeneralDetailReport);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLDetailReport.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// DateRange and Report type selected.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<GeneralDetailReport> PopulateGeneralDetailReportList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting PopulateGeneralDetailReportList List from QuickBooks.

            //Getting PopulateGeneralDetailReportList from QuickBooks.
            string DetailReportXmlList = string.Empty;
            DetailReportXmlList = QBCommonUtilities.GetListFromQuickBookByDateGeneralReport("GeneralDetailReportQueryRq", QBFileName, FromDate, ToDate);

            GeneralDetailReport GeneralDetailReport;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDetailReport = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (DetailReportXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDetailReport.LoadXml(DetailReportXmlList);
                XmlFileData = DetailReportXmlList;                

                #region Getting GeneralDetailReport Values from XML

                //Getting GeneralDetailReport values from QuickBooks response.
                XmlNodeList DetailReportNodeList = outputXMLDetailReport.GetElementsByTagName(QBGeneralDetailReport.ReportRet.ToString());

                foreach (XmlNode DetailReportNodes in DetailReportNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                   

                        GeneralDetailReport = new GeneralDetailReport();

                        foreach (XmlNode node in DetailReportNodes.ChildNodes)
                        {

                           

                            ////Checking NumRows
                            if (node.Name.Equals(QBGeneralDetailReport.NumRows.ToString()))
                            {
                                GeneralDetailReport.NumRows = node.InnerText;
                            }

                            ////Checking NumColumns
                            if (node.Name.Equals(QBGeneralDetailReport.NumColumns.ToString()))
                            {
                                GeneralDetailReport.NumColumns = node.InnerText;
                            }

                           

                            //Checking ColDesc
                            if (node.Name.Equals(QBGeneralDetailReport.ColDesc.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking ColTitle
                                    if (childNode.Name.Equals(QBGeneralDetailReport.ColTitle.ToString()))
                                    {
                                        if (childNode.Attributes["value"] != null)
                                            if (!string.IsNullOrEmpty(childNode.Attributes["value"].Value.ToString()))
                                                GeneralDetailReport.ColDescColTitle[count] = childNode.Attributes["value"].Value.ToString();
                                    }
                                   

                                }
                                count++;
                            }
                            //ReportData
                            if (node.Name.Equals(QBGeneralDetailReport.ReportData.ToString()))
                            {

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //count1 = 0;
                                    //DataRow

                                    if (childNode.Name.Equals(QBGeneralDetailReport.DataRow.ToString()))
                                    {
                                        //count1 = 0;


                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.DataRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.DataRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                    //TextRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TextRow.ToString()))
                                    {
                                        if (childNode.Attributes.Item(1) != null)
                                        {
                                            GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = childNode.Attributes["value"].Value.ToString();
                                        }
                                        else
                                        {
                                            if (Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) == 1)
                                                GeneralDetailReport.TextRow[Convert.ToInt32(childNode.Attributes["rowNumber"].Value.ToString()) - 1] = "";
                                        }
                                       
                                    }

                                    //SubtotalRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.SubtotalRow.ToString()))
                                    {
                                        // count1 = 0;
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.RowData.ToString()))
                                                GeneralDetailReport.SubtotalRowRowData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.SubtotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                    //////TotalRow
                                    if (childNode.Name.Equals(QBGeneralDetailReport.TotalRow.ToString()))
                                    {
                                       
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            
                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColData[count1] = childNodes.Attributes["value"].Value.ToString();

                                            if (childNodes.Name.Equals(QBGeneralDetailReport.ColData.ToString()))
                                                GeneralDetailReport.TotalRowColID[count1] = childNodes.Attributes["colID"].Value.ToString();

                                            count1++;
                                        }
                                    }

                                }
                            }
                        }
                        this.Add(GeneralDetailReport);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLDetailReport.RemoveAll();
                #endregion

            }
            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        public List<string> GetAllAccountList(string QBFileName)
        {
            string accountXmlList = QBCommonUtilities.GetListFromQuickBook("AccountQueryRq", QBFileName);

            List<string> accountList = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(accountXmlList);

            XmlFileData = accountXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList AccountNodeList = outputXMLDoc.GetElementsByTagName(QBAccountList.AccountRet.ToString());

            foreach (XmlNode accntNodes in AccountNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in accntNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBAccountList.FullName.ToString()))
                        {
                            if (!accountList.Contains(node.InnerText))
                                accountList.Add(node.InnerText);
                        }
                    }
                }
            }
            return accountList;
        }

    }
   

}
