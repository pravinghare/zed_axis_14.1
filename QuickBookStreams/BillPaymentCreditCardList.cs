﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This class provides the properties and methods for the bill payment credit card.
    /// </summary>
    public class BillPaymentCreditCardList : BaseBillPaymentCreditCardList
    {
        #region Public Properties

        public string TxnId
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }

        public string PayEntRefFullName
        {
            get { return m_PayEntityRefFullName; }
            set { m_PayEntityRefFullName = value; }
        }

        public string AccountRefFullName
        {
            get { return m_APAccountRefFullName; }
            set { m_APAccountRefFullName = value; }
        }
        public string CreditCardAccountRefFullName
        {
            get { return m_CreditCardAccountRefFullName; }
            set { m_CreditCardAccountRefFullName = value; }
        }
        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? AmountHomeCurrency
        {
            get { return m_AmountInHomeCurrency; }
            set { m_AmountInHomeCurrency = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }


        public string[] ATRTxnID
        {
            get { return m_ATRTxnID; }
            set { m_ATRTxnID = value; }
        }

        public string[] ATRRefNumber
        {
            get { return m_ATRRefNumber; }
            set { m_ATRRefNumber = value; }
        }

        //Axis Bug no. #292
        public string[] ATRAmount
        {
            get { return m_ATRAmount; }
            set { m_ATRAmount = value; }
        }
        //End  //Axis Bug no. #292

        #endregion
    }

    /// <summary>
    /// This collection class is used for getting bill payment credit card List from QuickBook.
    /// </summary>
    public class QBBillPaymentCreditCardCollection : Collection<BillPaymentCreditCardList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// This method is used to get Bill Payment credit card List for fiters
        /// EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCardEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("BillPaymentCreditCardQueryRq", QBFileName, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Bill Payment credit card List for fiters
        /// TxnDateRangeFilter,entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Bill Payment credit card List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.
           
            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCreditCardLst.TxnDate = node.InnerText;
                            }
                        }
                        //Credit Card Account Ref
                        if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                            }
                        }
                       
                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                        {
                            BillPaymentCreditCardLst.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                        {
                            BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                        {
                            BillPaymentCreditCardLst.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                        {
                            BillPaymentCreditCardLst.Memo = node.InnerText;
                        }
                        //Checking BillPayment Line ret values.
                        if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //Axis Bug no.#292
                                //Checking Amount
                                if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        ///Only Since Last Export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Bill Payment credit card List for fiters
        /// TxnDateRangeFilter,entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        ///Only Since Last Export 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;            
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }




        /// <summary>
        /// This method is used to get Bill Payment credit card List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Bill Payment Credit Card List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            //BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;

                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        ///This method is used to get Bill Payment List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        ///This method is used to get Bill Payment List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromRefnum, ToRefnum);
            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCreditCardLst.TxnDate = node.InnerText;
                            }
                        }
                        //Credit Card Account Ref
                        if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                        {
                            BillPaymentCreditCardLst.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                        {
                            BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                        {
                            BillPaymentCreditCardLst.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                        {
                            BillPaymentCreditCardLst.Memo = node.InnerText;
                        }
                        //Checking BillPayment Line ret values.
                        if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //Axis Bug no.#292
                                //Checking Amount
                                if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no. #292
                        BillPaymentCreditCardLst.ATRAmount=new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no. #292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292

                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> PopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.GetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                        //Checking PayeeEntityRef.
                        if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking ARAccountRefFullName.
                        if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                            }
                        }
                        //Checking TxnDate
                        if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                        {
                            try
                            {
                                DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                if (countryName == "United States")
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                }
                                else
                                {
                                    BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                }
                            }
                            catch
                            {
                                BillPaymentCreditCardLst.TxnDate = node.InnerText;
                            }
                        }
                        //Credit Card Account Ref
                        if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking TotalAmount
                        if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                        {
                            BillPaymentCreditCardLst.Amount = node.InnerText;
                        }

                        //Checking CurrecnyRef
                        if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                        {
                            foreach (XmlNode ChildNode in node.ChildNodes)
                            {
                                if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                    BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                            }
                        }

                        //Checking ExchangeRate
                        if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                        {
                            BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking TotalAmountInHomeCurrency
                        if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                        {
                            BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                        }

                        //Checking RefNumber
                        if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                        {
                            BillPaymentCreditCardLst.RefNumber = node.InnerText;
                        }
                        //Checking Memo
                        if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                        {
                            BillPaymentCreditCardLst.Memo = node.InnerText;
                        }
                        //Checking BillPayment Line ret values.
                        if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                //Checking TxnID Id value.
                                if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                {
                                    if (childNode.InnerText.Length > 36)
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                    else
                                        BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                }

                                //Checking RefNumber
                                if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //Axis Bug no.#292
                                //Checking Amount
                                if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                {
                                    try
                                    {
                                        BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                    }
                                    catch
                                    { }
                                }
                                //End Axis Bug no.#292

                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter,entity filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292

                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Bill Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<BillPaymentCreditCardList> ModifiedPopulateBillPaymentCreditCard(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Bill Payments List from QuickBooks.
            //Getting bill payment list from QuickBooks.
            string BillPaymentCrediCardXmlList = string.Empty;
            BillPaymentCrediCardXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillPaymentCreditCardQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            BillPaymentCreditCardList BillPaymentCreditCardLst;

            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfBillPaymentCreditCard = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (BillPaymentCrediCardXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfBillPaymentCreditCard.LoadXml(BillPaymentCrediCardXmlList);
                XmlFileData = BillPaymentCrediCardXmlList;


                #region Getting Bill Payment Values from XML.

                //Getting Bill Payment Credit Card values from QuickBooks response.
                XmlNodeList BillPaymentNodeList = outputXMLDocOfBillPaymentCreditCard.GetElementsByTagName(BillPayCreditCardList.BillPaymentCreditCardRet.ToString());

                foreach (XmlNode BillPaymentNodes in BillPaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        BillPaymentCreditCardLst = new BillPaymentCreditCardList();
                        BillPaymentCreditCardLst.ATRRefNumber = new string[BillPaymentNodes.ChildNodes.Count];
                        BillPaymentCreditCardLst.ATRTxnID = new string[BillPaymentNodes.ChildNodes.Count];
                        //Axis Bug no.#292
                        BillPaymentCreditCardLst.ATRAmount = new string[BillPaymentNodes.ChildNodes.Count];
                        //End Axis Bug no.#292
                        foreach (XmlNode node in BillPaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnId = node.InnerText;
                            }
                            //Checking TxnNumber
                            if (node.Name.Equals(BillPayCreditCardList.TxnNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.TxnNumber = node.InnerText;
                            }

                            //Checking PayeeEntityRef.
                            if (node.Name.Equals(BillPayCreditCardList.PayeeEntityRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.PayEntRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(BillPayCreditCardList.APAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.AccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(BillPayCreditCardList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        BillPaymentCreditCardLst.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    BillPaymentCreditCardLst.TxnDate = node.InnerText;
                                }
                            }
                            //Credit Card Account Ref
                            if (node.Name.Equals(BillPayCreditCardList.CreditCardAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CreditCardAccountRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking TotalAmount
                            if (node.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                            {
                                BillPaymentCreditCardLst.Amount = node.InnerText;
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(BillPayCreditCardList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(BillPayCheck.FullName.ToString()))
                                        BillPaymentCreditCardLst.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(BillPayCreditCardList.ExchangeRate.ToString()))
                            {
                                BillPaymentCreditCardLst.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(BillPayCreditCardList.AmountInHomeCurrency.ToString()))
                            {
                                BillPaymentCreditCardLst.AmountHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking RefNumber
                            if (node.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                            {
                                BillPaymentCreditCardLst.RefNumber = node.InnerText;
                            }
                            //Checking Memo
                            if (node.Name.Equals(BillPayCreditCardList.Memo.ToString()))
                            {
                                BillPaymentCreditCardLst.Memo = node.InnerText;
                            }
                            //Checking BillPayment Line ret values.
                            if (node.Name.Equals(BillPayCreditCardList.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(BillPayCreditCardList.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            BillPaymentCreditCardLst.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(BillPayCreditCardList.RefNumber.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRRefNumber[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //Axis Bug no.#292
                                    //Checking Amount
                                    if (childNode.Name.Equals(BillPayCreditCardList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            BillPaymentCreditCardLst.ATRAmount[count] = childNode.InnerText;
                                        }
                                        catch
                                        { }
                                    }
                                    //End Axis Bug no.#292
                                }
                                count++;
                            }
                        }
                        //Adding payment list.
                        this.Add(BillPaymentCreditCardLst);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfBillPaymentCreditCard.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


        
    }
}
