using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    ///This class provides methods and Porperties for Received payments.
    /// </summary>
    public class ReceivedPaymentsList : BaseReceivedPaymentsList
    {
        #region Public Methods

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string CustomerRefFullName
        {
            get { return m_CustomerRefFullName; }
            set { m_CustomerRefFullName = value; }
        }

        public string ARAccountRefFullName
        {
            get { return m_ARAccountRefFullName; }
            set { m_ARAccountRefFullName = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public decimal? TotalAmount
        {
            get { return m_TotalAmount; }
            set { m_TotalAmount = value; }
        }

        public string CurrencyRefFullName
        {
            get { return m_CurrencyRefFullName; }
            set { m_CurrencyRefFullName = value; }
        }

        public decimal? ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public decimal? TotalAmountInHomeCurrency
        {
            get { return m_TotalAmountInHomeCurrency; }
            set { m_TotalAmountInHomeCurrency = value; }
        }

        public string PaymentMethodRefFullName
        {
            get { return m_PaymentMethodRefFullName; }
            set { m_PaymentMethodRefFullName = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string DepositToAccountRefFullName
        {
            get { return m_DepositToAccountRefFullName; }
            set { m_DepositToAccountRefFullName = value; }
        }

        public string UnusedPayment
        {
            get { return m_UnusedPayment; }
            set { m_UnusedPayment = value; }
        }

        public string UnusedCredits
        {
            get { return m_UnusedCredits; }
            set { m_UnusedCredits = value; }
        }

        //AppliedToTxnRet
        public string[] ATRTxnID
        {
            get { return m_ATRTxnID; }
            set { m_ATRTxnID = value; }
        }

        //Changes Axis 10.1
        public string[] ATRTxnType
        {
            get { return m_ATRTxnType; }
            set { m_ATRTxnType = value; }
        }


        public string[] ATRRefNumber
        {
            get { return m_ATRRefNumber; }
            set { m_ATRRefNumber = value; }
        }

        public decimal?[] ATRAmount
        {
            get { return m_ATRAmount; }
            set { m_ATRAmount = value; }
        }

        public decimal?[] ATRDiscountAmount
        {
            get { return m_ATRDiscountAmount; }
            set { m_ATRDiscountAmount = value; }
        }

        public string[] ATRDiscountAmountName
        {
            get { return m_ATRDiscountAccount; }
            set { m_ATRDiscountAccount = value; }
        }


        #endregion
    }

    /// <summary>
    /// This collection class is used for getting Received Payments List from QuickBook.
    /// </summary>
    public class QBReceivedPaymentsListCollection : Collection<ReceivedPaymentsList>
    {
        string XmlFileData = string.Empty;


        /// <summary>
        /// This method is used to get Received Payments List for fiters
        /// TxnDateRangeFilter ,entity filter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];

                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This method is used to get Received Payments List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }






        /// <summary>
        /// This method is used to get Received Payments List for fiters
        /// TxnDateRangeFilter,Entity filter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }


                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }


                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }


                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Received Payments List for fiters
        /// TxnDateRangeFilter. 
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Receive Payments List from QuickBooks.
            //int count = 0;
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            //ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;

                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }


                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }


                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

        /// <summary>
        ///This method is used to get Received Payments List for filters
        ///EntityFilter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsListEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("ReceivePaymentQueryRq", QBFileName, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        ///This method is used to get Received Payments List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        ///This method is used to get Received Payments List for filters
        ///RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting Receive Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromRefnum, ToRefnum);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Received Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter,Entity Filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName)
        {
            #region Getting Receive Payments List from QuickBooks.
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking TxnType 
                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Received Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> PopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Receive Payments List from QuickBooks.
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.GetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking TxnType 
                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get Received Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter, entity Filter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting Receive Payments List from QuickBooks.
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking TxnType 
                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }




        /// <summary>
        /// This method is used to get Received Payments List for filters
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ReceivedPaymentsList> ModifiedPopulateReceivedPaymentsList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting Receive Payments List from QuickBooks.
            //Getting item list from QuickBooks.
            string ReceivePaymentXmlList = string.Empty;
            ReceivePaymentXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ReceivePaymentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);

            ReceivedPaymentsList ReceivePaymentList;
            #endregion


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceivePayment = new XmlDocument();
            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ReceivePaymentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceivePayment.LoadXml(ReceivePaymentXmlList);
                XmlFileData = ReceivePaymentXmlList;


                #region Getting Receive Payment Values from XML

                //Getting Receive Payments values from QuickBooks response.
                XmlNodeList ReceivePaymentNodeList = outputXMLDocOfReceivePayment.GetElementsByTagName(QBReceivePayments.ReceivePaymentRet.ToString());

                foreach (XmlNode ReceivePaymentNodes in ReceivePaymentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        ReceivePaymentList = new ReceivedPaymentsList();
                        ReceivePaymentList.ATRAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmount = new decimal?[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRDiscountAmountName = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRRefNumber = new string[ReceivePaymentNodes.ChildNodes.Count];
                        ReceivePaymentList.ATRTxnID = new string[ReceivePaymentNodes.ChildNodes.Count];
                        foreach (XmlNode node in ReceivePaymentNodes.ChildNodes)
                        {
                            //Checking TxnID
                            if (node.Name.Equals(QBReceivePayments.TxnID.ToString()))
                            {
                                ReceivePaymentList.TxnID = node.InnerText;
                            }


                            //Checking CustomerRefFullName.
                            if (node.Name.Equals(QBReceivePayments.CustomerRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CustomerRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking ARAccountRefFullName.
                            if (node.Name.Equals(QBReceivePayments.ARAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.ARAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking TxnDate
                            if (node.Name.Equals(QBReceivePayments.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        ReceivePaymentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    ReceivePaymentList.TxnDate = node.InnerText;
                                }
                            }
                            //Checking RefNumber
                            if (node.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                            {
                                ReceivePaymentList.RefNumber = node.InnerText;
                            }
                            //Checking TotalAmount
                            if (node.Name.Equals(QBReceivePayments.TotalAmount.ToString()))
                            {
                                ReceivePaymentList.TotalAmount = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking CurrecnyRef
                            if (node.Name.Equals(QBReceivePayments.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.CurrencyRefFullName = ChildNode.InnerText;
                                }
                            }

                            //Checking ExchangeRate
                            if (node.Name.Equals(QBReceivePayments.ExchangeRate.ToString()))
                            {
                                ReceivePaymentList.ExchangeRate = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking TotalAmountInHomeCurrency
                            if (node.Name.Equals(QBReceivePayments.TotalAmountInHomeCurrency.ToString()))
                            {
                                ReceivePaymentList.TotalAmountInHomeCurrency = Convert.ToDecimal(node.InnerText);
                            }

                            //Checking PaymentRefFullName
                            if (node.Name.Equals(QBReceivePayments.PaymentMethodRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.PaymentMethodRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking Memo
                            if (node.Name.Equals(QBReceivePayments.Memo.ToString()))
                            {
                                ReceivePaymentList.Memo = node.InnerText;
                            }

                            //Checking DepositToAccountRefFullName
                            if (node.Name.Equals(QBReceivePayments.DepositToAccountRef.ToString()))
                            {
                                foreach (XmlNode ChildNode in node.ChildNodes)
                                {
                                    if (ChildNode.Name.Equals(QBReceivePayments.FullName.ToString()))
                                        ReceivePaymentList.DepositToAccountRefFullName = ChildNode.InnerText;
                                }
                            }
                            //Checking UnusedCredits.
                            if (node.Name.Equals(QBReceivePayments.UnusedCredits.ToString()))
                            {
                                ReceivePaymentList.UnusedCredits = node.InnerText;
                            }
                            //Checking UnusedPayment.
                            if (node.Name.Equals(QBReceivePayments.UnusedPayment.ToString()))
                            {
                                ReceivePaymentList.UnusedPayment = node.InnerText;
                            }
                            //Checking ReceivePayment Line ret values.
                            if (node.Name.Equals(QBReceivePayments.AppliedToTxnRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnID Id value.
                                    if (childNode.Name.Equals(QBReceivePayments.TxnID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim();
                                        else
                                            ReceivePaymentList.ATRTxnID[count] = childNode.InnerText;
                                    }

                                    //Checking TxnType 
                                    if (childNode.Name.Equals(QBReceivePayments.TxnType.ToString()))
                                    {
                                        ReceivePaymentList.ATRTxnType[count] = childNode.InnerText;
                                    }

                                    //Checking RefNumber
                                    if (childNode.Name.Equals(QBReceivePayments.RefNumber.ToString()))
                                    {
                                        ReceivePaymentList.ATRRefNumber[count] = childNode.InnerText;
                                    }
                                    //Checking Amount
                                    if (childNode.Name.Equals(QBReceivePayments.Amount.ToString()))
                                    {
                                        ReceivePaymentList.ATRAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //checking DiscountAmount
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAmount.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            //Axis 694
                                            ReceivePaymentList.ATRDiscountAmount[count] = Convert.ToDecimal(childNode.InnerText);
                                            //if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            //{
                                            //    ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            //}

                                            //Axis 694 ends

                                        }
                                    }

                                    //Axis 694
                                    if (childNode.Name.Equals(QBReceivePayments.DiscountAccountRef.ToString()))
                                    {
                                        foreach (XmlNode ChildNodes in childNode.ChildNodes)
                                        {
                                            if (ChildNodes.Name.Equals(QBReceivePayments.FullName.ToString()))
                                            {
                                                ReceivePaymentList.ATRDiscountAmountName[count] = ChildNodes.InnerText;
                                            }
                                        }
                                    }
                                    //Axis 694 ends 
                                }
                                count++;
                            }
                        }
                        //Adding Receive Payment list.
                        this.Add(ReceivePaymentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfReceivePayment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


    }
}
