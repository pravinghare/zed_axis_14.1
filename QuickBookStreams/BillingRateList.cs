﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    public class BillingRateList : BaseBillingRate
    {
        #region Public Methods

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string EditSequence
        {
            get { return m_EditSequence; }
            set { m_EditSequence = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string BillingRateType
        {
            get { return m_BillingRateType; }
            set { m_BillingRateType = value; }
        }

        public string FixedBillingRate
        {
            get { return m_FixedBillingRate; }
            set { m_FixedBillingRate = value; }
        }

        public string[] LineID
        {
            get { return m_LineID; }
            set { m_LineID = value; }
        }

        public string[] ItemRefFullName
        {
            get { return m_ItemRefFullName; }
            set { m_ItemRefFullName = value; }
        }

        public decimal?[] CustomRate
        {
            get { return m_CustomRate; }
            set { m_CustomRate = value; }
        }

        public string[] CustomRatePercent
        {
            get { return m_CustomRatePercent; }
            set { m_CustomRatePercent = value; }
        }
        #endregion
    }

    /// <summary>
    /// This class is used to get BillingRate from QuickBook.
    /// </summary>
    public class QBBillingRateListCollection : Collection<BillingRateList>
    {
        #region public Method
        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all Vendors in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllBillingRateNameList(string QBFileName)
        {
            string BillingRateXmlList = QBCommonUtilities.GetListFromQuickBook("BillingRateQueryRq", QBFileName);

            List<string> billingRateList = new List<string>();
            List<string> billingRateList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(BillingRateXmlList);

            XmlFileData = BillingRateXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

           // XmlNodeList VendorNodeList = outputXMLDoc.GetElementsByTagName(QBVendorList.VendorRet.ToString());
            XmlNodeList BillingRateNodeList = outputXMLDoc.GetElementsByTagName(QBXmlBillingRateList.BillingRateRet.ToString());

            foreach (XmlNode billingRateNodes in BillingRateNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in billingRateNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlBillingRateList.Name.ToString()))
                        {
                            if (!billingRateList.Contains(node.InnerText))
                            {
                                billingRateList.Add(node.InnerText);
                                string name = node.InnerText + "     :     BillingRate";
                                billingRateList1.Add(name);
                            }
                        }
                    }
                }
            }
            CommonUtilities.GetInstance().BillingrateListWithType = billingRateList1;
            XmlFileData = null;
            outputXMLDoc = null;
            BillingRateXmlList = null;
            BillingRateNodeList = null;
            return billingRateList;
        }


        /// <summary>
        /// If no filter selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<BillingRateList> PopulateBillingRateList(string QBFileName)
        {
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.GetListFromQuickBook("BillingRateQueryRq", QBFileName);
            GetBillingRateValuesfromXML(BillingRateXmlList);

            return this;
        }

        /// <summary>
        /// This method is used to get BillingRateList
        /// for Filters DateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillingRateList> PopulateBillingRateList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.GetListFromQuickBookModifiedDate("BillingRateQueryRq", QBFileName, FromDate, ToDate);
            GetBillingRateValuesfromXML(BillingRateXmlList);

            return this;
        }

        /// <summary>
        /// This method is used for getting BillingRate List from QuickBook by
        /// passing company filename, fromDate, and Todate, entityName
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="entityFilter"></param>
        /// <returns></returns>
        public Collection<BillingRateList> PopulateBillingRateList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillingRateQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            GetBillingRateValuesfromXML(BillingRateXmlList);

            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillingRateList> ModifiedBillingRateList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillingRateQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            GetBillingRateValuesfromXML(BillingRateXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<BillingRateList> ModifiedBillingRateList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting BillingRate List from QuickBooks.
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("BillingRateQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            GetBillingRateValuesfromXML(BillingRateXmlList);
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting BillingRateList by Filter 
        /// Entity Filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<BillingRateList> PopulateBillingRateEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting BillingRate List from QuickBooks.
            string BillingRateXmlList = string.Empty;
            BillingRateXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("BillingRateQueryRq", QBFileName, entityFilter);
            #endregion

            GetBillingRateValuesfromXML(BillingRateXmlList);
            //Returning object.
            return this;
        }

        public void GetBillingRateValuesfromXML(string BillingRateXmlList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLDoc = new XmlDocument();

            BillingRateList BillingRateList;

            if (BillingRateXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDoc.LoadXml(BillingRateXmlList);
                XmlFileData = BillingRateXmlList;

                #region Getting BillingRate Values from XML

                XmlNodeList BillingRateNodeList = outputXMLDoc.GetElementsByTagName(QBXmlBillingRateList.BillingRateRet.ToString());

                foreach (XmlNode BillingRateNodes in BillingRateNodeList)
                {
                    #region
                    if (bkWorker.CancellationPending != true)
                    {
                        int count1 = 0;
                        BillingRateList = new BillingRateList();
                        BillingRateList.CustomRate = new decimal?[BillingRateNodes.ChildNodes.Count];
                        BillingRateList.CustomRatePercent = new string[BillingRateNodes.ChildNodes.Count];
                        BillingRateList.ItemRefFullName = new string[BillingRateNodes.ChildNodes.Count];
                        BillingRateList.LineID = new string[BillingRateNodes.ChildNodes.Count];

                        foreach (XmlNode node in BillingRateNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlBillingRateList.ListID.ToString()))
                            {
                                BillingRateList.ListID = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.TimeCreated.ToString()))
                            {
                                BillingRateList.TimeCreated = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.TimeModified.ToString()))
                            {
                                BillingRateList.TimeModified = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.EditSequence.ToString()))
                            {
                                BillingRateList.EditSequence = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.Name.ToString()))
                            {
                                BillingRateList.Name = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.BillingRateType.ToString()))
                            {
                                BillingRateList.BillingRateType = node.InnerText;
                            }

                            if (node.Name.Equals(QBXmlBillingRateList.FixedBillingRate.ToString()))
                            {
                                BillingRateList.FixedBillingRate = node.InnerText;
                            }

                            //Checking BillingRatePerItemRet
                            if (node.Name.Equals(QBXmlBillingRateList.BillingRatePerItemRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlBillingRateList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlBillingRateList.FullName.ToString()))
                                                BillingRateList.ItemRefFullName[count1] = childNodes.InnerText;
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlBillingRateList.CustomRate.ToString()))
                                    {
                                        BillingRateList.CustomRate[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    if (childNode.Name.Equals(QBXmlBillingRateList.CustomRatePercent.ToString()))
                                    {
                                        BillingRateList.CustomRatePercent[count1] = childNode.InnerText;
                                    }
                                }
                                count1++;
                            }
                        }
                        this.Add(BillingRateList);
                    }
                    else
                    { }

                    #endregion
                }
                //Removing all the references from OutPut Xml document.
                outputXMLDoc.RemoveAll();
                #endregion
            }
        }


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        #endregion
    }
}
