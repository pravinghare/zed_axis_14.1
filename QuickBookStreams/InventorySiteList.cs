﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Streams
{
    public class InventorySiteList : BaseInventorySiteList
    {
        #region Public Properties

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string ParentSiteRefFullName
        {
            get { return m_ParentSiteFullName; }
            set { m_ParentSiteFullName = value; }
        }

        public string SiteDesc
        {
            get { return m_SiteDesc; }
            set { m_SiteDesc = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string SiteAddr1
        {
            get { return m_SiteAddr1; }
            set { m_SiteAddr1 = value; }
        }

        public string SiteAddr2
        {
            get { return m_SiteAddr2; }
            set { m_SiteAddr2 = value; }
        }

        public string SiteAddr3
        {
            get { return m_SiteAddr3; }
            set { m_SiteAddr3 = value; }
        }

        public string SiteAddr4
        {
            get { return m_SiteAddr4; }
            set { m_SiteAddr4 = value; }
        }

        public string SiteAddr5
        {
            get { return m_SiteAddr5; }
            set { m_SiteAddr5 = value; }
        }

        public string SiteCity
        {
            get { return m_SiteCity; }
            set { m_SiteCity = value; }
        }

        public string SiteState
        {
            get { return m_SiteState; }
            set { m_SiteState = value; }
        }

        public string SitePostalCode
        {
            get { return m_SitePostalCode; }
            set { m_SitePostalCode = value; }
        }

        public string SiteCountry
        {
            get { return m_SiteCountry; }
            set { m_SiteCountry = value; }
        }

        public string IsDefaultSite
        {
            get { return m_IsDefaultSite; }
            set { m_IsDefaultSite = value; }
        }

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];
        #endregion
    }

    public class QBInventorySiteListCollection : Collection<InventorySiteList>
    {
        #region public Method

        string XmlFileData = string.Empty;

        /// <summary>
        /// this method returns all InventorySiteList in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllInventorySiteList(string QBFileName)
        {
            string inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBook("InventorySiteQueryRq", QBFileName);

            List<string> inventorySiteList = new List<string>();
            List<string> inventorySiteList1 = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(inventorySiteXmlList);

            XmlFileData = inventorySiteXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList InventorySiteNodeList = outputXMLDoc.GetElementsByTagName(QBInventorySiteList.InventorySiteRet.ToString());

            foreach (XmlNode inventoryNodes in InventorySiteNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in inventoryNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBInventorySiteList.FullName.ToString()))
                        {
                            if (!inventorySiteList.Contains(node.InnerText))
                            {
                                inventorySiteList.Add(node.InnerText);
                                string name = node.InnerText;
                                inventorySiteList1.Add(name);
                            }
                        }
                    }
                }
            }

            inventorySiteXmlList = null;
            InventorySiteNodeList = null;
            XmlFileData = null;
            outputXMLDoc = null;
            return inventorySiteList1;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public Collection<InventorySiteList> PopulateInventorySiteList(string QBFileName, bool inActiveFilter)
        {
            string inventorySiteXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBook("InventorySiteQueryRq", QBFileName);
            }
            else
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("InventorySiteQueryRq", QBFileName);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(inventorySiteXmlList);
            XmlFileData = inventorySiteXmlList;

            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList InventorySiteNodeList = outputXMLDoc.GetElementsByTagName(QBXmlInventorySiteList.InventorySiteRet.ToString());

            foreach (XmlNode InventorySiteNodes in InventorySiteNodeList)
            {
                i = 0;
                #region For InventorySite data
                if (bkWorker.CancellationPending != true)
                {
                    InventorySiteList inventorySiteList = new InventorySiteList();
                    foreach (XmlNode node in InventorySiteNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlInventorySiteList.ListID.ToString()))
                            inventorySiteList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Name.ToString()))
                            inventorySiteList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsActive.ToString()))
                            inventorySiteList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.SiteDesc.ToString()))
                            inventorySiteList.SiteDesc = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Contact.ToString()))
                            inventorySiteList.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Phone.ToString()))
                            inventorySiteList.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Fax.ToString()))
                            inventorySiteList.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Email.ToString()))
                            inventorySiteList.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsDefaultSite.ToString()))
                            inventorySiteList.IsDefaultSite = node.InnerText;

                        if (node.Name.Equals(QBXmlInventorySiteList.SiteAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr1.ToString()))
                                    inventorySiteList.SiteAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr2.ToString()))
                                    inventorySiteList.SiteAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr3.ToString()))
                                    inventorySiteList.SiteAddr3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr4.ToString()))
                                    inventorySiteList.SiteAddr4 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr5.ToString()))
                                    inventorySiteList.SiteAddr5 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.City.ToString()))
                                    inventorySiteList.SiteCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.State.ToString()))
                                    inventorySiteList.SiteState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.PostalCode.ToString()))
                                    inventorySiteList.SitePostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Country.ToString()))
                                    inventorySiteList.SiteCountry = childNode.InnerText;
                            }
                        }
                        
                        if (node.Name.Equals(QBXmlInventorySiteList.ParentSiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.FullName.ToString()))
                                    inventorySiteList.ParentSiteRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlInventorySiteList.DataExtRet.ToString()))
                        {
                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                inventorySiteList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    inventorySiteList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }
                    }
                    this.Add(inventorySiteList);
                }
                else
                { return null; }
                #endregion
            }
            return this;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public Collection<InventorySiteList> PopulateInventorySiteList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            string inventorySiteXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBookByDate("InventorySiteQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("InventorySiteQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(inventorySiteXmlList);
            XmlFileData = inventorySiteXmlList;
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList InventorySiteNodeList = outputXMLDoc.GetElementsByTagName(QBXmlInventorySiteList.InventorySiteRet.ToString());
            foreach (XmlNode InventorySiteNodes in InventorySiteNodeList)
            {
                i = 0;
                #region For InventorySite data
                if (bkWorker.CancellationPending != true)
                {
                    InventorySiteList inventorySiteList = new InventorySiteList();
                    foreach (XmlNode node in InventorySiteNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlInventorySiteList.ListID.ToString()))
                            inventorySiteList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Name.ToString()))
                            inventorySiteList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsActive.ToString()))
                            inventorySiteList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.SiteDesc.ToString()))
                            inventorySiteList.SiteDesc = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Contact.ToString()))
                            inventorySiteList.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Phone.ToString()))
                            inventorySiteList.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Fax.ToString()))
                            inventorySiteList.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Email.ToString()))
                            inventorySiteList.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsDefaultSite.ToString()))
                            inventorySiteList.IsDefaultSite = node.InnerText;

                        if (node.Name.Equals(QBXmlInventorySiteList.SiteAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr1.ToString()))
                                    inventorySiteList.SiteAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr2.ToString()))
                                    inventorySiteList.SiteAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr3.ToString()))
                                    inventorySiteList.SiteAddr3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr4.ToString()))
                                    inventorySiteList.SiteAddr4 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr5.ToString()))
                                    inventorySiteList.SiteAddr5 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.City.ToString()))
                                    inventorySiteList.SiteCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.State.ToString()))
                                    inventorySiteList.SiteState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.PostalCode.ToString()))
                                    inventorySiteList.SitePostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Country.ToString()))
                                    inventorySiteList.SiteCountry = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlInventorySiteList.ParentSiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.FullName.ToString()))
                                    inventorySiteList.ParentSiteRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlInventorySiteList.DataExtRet.ToString()))
                        {
                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                inventorySiteList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    inventorySiteList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }
                    }
                    this.Add(inventorySiteList);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public Collection<InventorySiteList> PopulateInventorySiteList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            string inventorySiteXmlList = string.Empty;
            if (inActiveFilter == false)
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBookByDate("InventorySiteQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                inventorySiteXmlList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("InventorySiteQueryRq", QBFileName, FromDate, ToDate);
            }
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(inventorySiteXmlList);
            XmlFileData = inventorySiteXmlList;
            int i = 0;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList InventorySiteNodeList = outputXMLDoc.GetElementsByTagName(QBXmlInventorySiteList.InventorySiteRet.ToString());
            foreach (XmlNode InventorySiteNodes in InventorySiteNodeList)
            {
                i = 0;
                #region For InventorySite data
                if (bkWorker.CancellationPending != true)
                {
                    InventorySiteList inventorySiteList = new InventorySiteList();
                    foreach (XmlNode node in InventorySiteNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlInventorySiteList.ListID.ToString()))
                            inventorySiteList.ListID = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Name.ToString()))
                            inventorySiteList.Name = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsActive.ToString()))
                            inventorySiteList.IsActive = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.SiteDesc.ToString()))
                            inventorySiteList.SiteDesc = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Contact.ToString()))
                            inventorySiteList.Contact = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Phone.ToString()))
                            inventorySiteList.Phone = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Fax.ToString()))
                            inventorySiteList.Fax = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.Email.ToString()))
                            inventorySiteList.Email = node.InnerText;
                        if (node.Name.Equals(QBXmlInventorySiteList.IsDefaultSite.ToString()))
                            inventorySiteList.IsDefaultSite = node.InnerText;

                        if (node.Name.Equals(QBXmlInventorySiteList.SiteAddress.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr1.ToString()))
                                    inventorySiteList.SiteAddr1 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr2.ToString()))
                                    inventorySiteList.SiteAddr2 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr3.ToString()))
                                    inventorySiteList.SiteAddr3 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr4.ToString()))
                                    inventorySiteList.SiteAddr4 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Addr5.ToString()))
                                    inventorySiteList.SiteAddr5 = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.City.ToString()))
                                    inventorySiteList.SiteCity = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.State.ToString()))
                                    inventorySiteList.SiteState = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.PostalCode.ToString()))
                                    inventorySiteList.SitePostalCode = childNode.InnerText;
                                if (childNode.Name.Equals(QBXmlInventorySiteList.Country.ToString()))
                                    inventorySiteList.SiteCountry = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlInventorySiteList.ParentSiteRef.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBXmlInventorySiteList.FullName.ToString()))
                                    inventorySiteList.ParentSiteRefFullName = childNode.InnerText;
                            }
                        }

                        if (node.Name.Equals(QBXmlInventorySiteList.DataExtRet.ToString()))
                        {
                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                inventorySiteList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    inventorySiteList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                            }

                            i++;
                        }
                    }
                    this.Add(inventorySiteList);
                }
                else
                { return null; }
                #endregion
            }

            return this;
        }


        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        #endregion
    }

}

