﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using System.Xml;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;

namespace Streams
{
    public class ItemGroupList : BaseItemGroupList
    {
        #region Public Properties

        public string[] ItemGroupRet
        {
            get { return m_ItemGroupRet; }
            set { m_ItemGroupRet = value; }
        }
        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }
     
        public string TimeCreated
      {
          get { return m_TimeCreated; }
          set { m_TimeCreated = value; }
      }
        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string EditSequence
        {
            get { return m_EditSequence; }
            set { m_EditSequence = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string BarCodeValue
        {
            get { return m_BarCodeValue; }
            set { m_BarCodeValue = value; }
        }
        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string ItemDesc
        {
            get { return m_ItemDesc; }
            set { m_ItemDesc = value; }
        }
               
       
       
        public string UnitOfMessureSetRefFullName
        {
            get { return m_UnitOfMessureSetRefFullName; }
            set { m_UnitOfMessureSetRefFullName = value; }
        }
        
        public string IsPrintItemsInGroup
        {
            get { return m_IsPrintItemsInGroup; }
            set { m_IsPrintItemsInGroup = value; }
        }
        
        public string SpecialItemType
        {
            get { return m_SpecialItemType; }
            set { m_SpecialItemType = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }
        
        public string[] ItemGroupLineItemRefListID
        {
            get { return m_ItemGroupLineItemRefListID; }
            set { m_ItemGroupLineItemRefListID = value; }
        }
        
        public string[] ItemGroupLineItemRefFullName
        {
            get { return m_ItemGroupLineItemRefFullName; }
            set { m_ItemGroupLineItemRefFullName = value; }
        }
        
        public string[] ItemGroupLineQuantity
        {
            get { return m_ItemGroupLineQuantity; }
            set { m_ItemGroupLineQuantity = value; }
        }

        public string[] ItemGroupLineUnitOfMeasure
        {
            get { return m_ItemGroupLineUnitOfMeasure; }
            set { m_ItemGroupLineUnitOfMeasure = value; }
        }
     
        public string[] DataExtOwnerID
        {
            get { return m_DataExtOwnerID; }
            set { m_DataExtOwnerID = value; }
        }

        public string[] DataExtName
        {
            get { return m_DataExtName; }
            set { m_DataExtName = value; }
        }

        public string[] DataExtType
        {
            get { return m_DataExtType; }
            set { m_DataExtType = value; }
        }

        public string[] DataExtValue
        {
            get { return m_DataExtValue; }
            set { m_DataExtValue = value; }
        }

        public string ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }


        public string UnitOfMeasure 
        { get{return m_UnitOfMeasure;}
          set{m_UnitOfMeasure=value;}
        }

        public string Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; } 
        }

        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

        public string ItemGroupRef
        {
            get { return m_ItemGroupRef; }
            set { m_ItemGroupRef = value; }
        }

        public string DataExtRet
        {
            get { return m_DataExtRet; }
            set { m_DataExtRet = value; }
        }


        #endregion


    }

    public class QBItemGroupListCollection : Collection<ItemGroupList>
    {
        #region public Method
        int i = 0;
        string XmlFileData = string.Empty;
        //Getting current version of System. BUG(676)

        /// <summary>
        /// this method returns all Item Group records in quickbook
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllItemGroupList(string QBFileName)
        {
            string itemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName);

            List<string> itemGroupList = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(itemGroupXmlList);
            XmlFileData = itemGroupXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList itemGroupNodeList = outputXMLDoc.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

            foreach (XmlNode itemGroupNodes in itemGroupNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                   
                    foreach (XmlNode node in itemGroupNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBItemGroupList.ListID.ToString()))
                        {
                            if (!itemGroupList.Contains(node.InnerText))
                                itemGroupList.Add(node.InnerText);
                        }
                    }
                }
            }

            return itemGroupList;
        }

        /// <summary>
        /// This Method is used for getting item Group by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            string itemGroupXmlList = QBCommonUtilities.GetListFromQuickBookByDate("ItemGroupQueryRq", QBFileName, FromDate, ToDate);
            XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(itemGroupXmlList);
            XmlFileData = itemGroupXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList itemGroupNodeList = outputXMLDoc.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

            if (itemGroupXmlList != string.Empty)
            {
                XmlToObject(itemGroupNodeList);


        #endregion
            }
            return this;

        }

        public QBItemGroupListCollection XmlToObject(XmlNodeList itemGroupNodeList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            ItemGroupList ItemGroupList;
            #region Main Function XmlToObject
            foreach (XmlNode itemGroupNodes in itemGroupNodeList)
            {

                #region For Item Group data
                if (bkWorker.CancellationPending != true)
                {
                    int count = 0;

                    ItemGroupList = new ItemGroupList();
                    ItemGroupList.ItemGroupLineItemRefListID = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.ItemGroupLineItemRefFullName = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.ItemGroupLineQuantity = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.ItemGroupLineUnitOfMeasure = new string[itemGroupNodes.ChildNodes.Count];

                    ItemGroupList.DataExtOwnerID = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.DataExtName = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.DataExtValue = new string[itemGroupNodes.ChildNodes.Count];
                    ItemGroupList.DataExtType = new string[itemGroupNodes.ChildNodes.Count];


                    foreach (XmlNode node in itemGroupNodes.ChildNodes)
                    {
                        //Checking TxnID. 
                        if (node.Name.Equals(QBItemGroupList.ListID.ToString()))
                        {
                            ItemGroupList.ListID = node.InnerText;
                        }

                        if (node.Name.Equals(QBItemGroupList.Name.ToString()))
                        {
                            ItemGroupList.Name = node.InnerText;
                        }

                        //Checking TimeCreated
                        if (node.Name.Equals(QBItemGroupList.TimeCreated.ToString()))
                        {
                            ItemGroupList.TimeCreated = node.InnerText;
                        }

                        //Checking TimeModified .
                        if (node.Name.Equals(QBItemGroupList.TimeModified.ToString()))
                        {
                            ItemGroupList.TimeModified = node.InnerText;
                        }


                        //Checking EditSequence .
                        if (node.Name.Equals(QBItemGroupList.EditSequence.ToString()))
                        {
                            ItemGroupList.EditSequence = node.InnerText;
                        }
                        //Checking BarCodeValue .
                        if (node.Name.Equals(QBItemGroupList.BarCodeValue.ToString()))
                        {
                            ItemGroupList.BarCodeValue = node.InnerText;
                        }
                        //Checking IsActive .
                        if (node.Name.Equals(QBItemGroupList.IsActive.ToString()))
                        {
                            ItemGroupList.IsActive = node.InnerText;
                        }
                        //Checking ItemDesc .
                        if (node.Name.Equals(QBItemGroupList.ItemDesc.ToString()))
                        {
                            ItemGroupList.ItemDesc = node.InnerText;
                        }
                        //Checking ExternalGUID .
                        if (node.Name.Equals(QBItemGroupList.ExternalGUID.ToString()))
                        {
                            ItemGroupList.ExternalGUID = node.InnerText;
                        }
                        //Checking IsPrintItemsInGroup .
                        if (node.Name.Equals(QBItemGroupList.IsPrintItemsInGroup.ToString()))
                        {
                            ItemGroupList.IsPrintItemsInGroup = node.InnerText;
                        }

                        if (node.Name.Equals(QBItemGroupList.ItemGroupLine.ToString()))
                        {
                            checkNullEntries(node, ref ItemGroupList, count);

                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBItemGroupList.DataExtOwnerID.ToString()))
                                {
                                    ItemGroupList.DataExtOwnerID[count] = childNode.InnerText;
                                }

                                if (childNode.Name.Equals(QBItemGroupList.ItemRef.ToString()))
                                {
                                    foreach (XmlNode subchildNode in childNode.ChildNodes)
                                    {
                                        if (subchildNode.Name.Equals(QBItemGroupList.ListID.ToString()))
                                        {
                                            ItemGroupList.ItemGroupLineItemRefListID[count] = subchildNode.InnerText;
                                        }
                                        if (subchildNode.Name.Equals(QBItemGroupList.FullName.ToString()))
                                        {
                                            ItemGroupList.ItemGroupLineItemRefFullName[count] = subchildNode.InnerText;
                                        }
                                    }
                                }

                                if (childNode.Name.Equals(QBItemGroupList.Quantity.ToString()))
                                {
                                    ItemGroupList.ItemGroupLineQuantity[count] = childNode.InnerText;
                                }
                                if (childNode.Name.Equals(QBItemGroupList.UnitOfMeasure.ToString()))
                                {
                                    ItemGroupList.ItemGroupLineUnitOfMeasure[count] = childNode.InnerText;
                                }
                            }
                            count++;
                        }
                        if (node.Name.Equals(QBItemGroupList.DataExtRet.ToString()))
                        {
                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./OwnerID").InnerText))
                            {
                                ItemGroupList.DataExtOwnerID[i] = node.SelectSingleNode("./OwnerID").InnerText;
                            }
                            if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                            {
                                ItemGroupList.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                    ItemGroupList.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtType").InnerText))
                                    ItemGroupList.DataExtType[i] = node.SelectSingleNode("./DataExtType").InnerText;
                            }
                            i++;
                        }

                #endregion
                    }

                    this.Add(ItemGroupList);
                }
                else
                {  }
            }
            return this;
            #endregion
        }

        private void checkNullEntries(XmlNode node, ref ItemGroupList itemGroupList, int count)
        {
            if (node.SelectSingleNode("./" + QBItemGroupList.DataExtOwnerID.ToString()) == null)
            {
                itemGroupList.DataExtOwnerID[count] = null;
            }
            if (node.SelectSingleNode("./" + QBItemGroupList.ItemRef.ToString()) == null)
            {
                itemGroupList.ItemGroupLineItemRefListID[count] = null;
            }
            if (node.SelectSingleNode("./" + QBItemGroupList.Quantity.ToString()) == null)
            {
                itemGroupList.ItemGroupLineQuantity[count] = null;
            }
            if (node.SelectSingleNode("./" + QBItemGroupList.UnitOfMeasure.ToString()) == null)
            {
                itemGroupList.ItemGroupLineUnitOfMeasure[count] = null;
            }
        }


        //axis 12.0
        /// <summary>
        /// if no filter was selected axis 12.0
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName)
        {
            #region Getting Vehicle Mileage List from QuickBooks.

            //Getting item list from QuickBooks.
            string itemGroupXmlList = string.Empty;
            itemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName);
      
            #endregion
          

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItemGroup = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (itemGroupXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfItemGroup.LoadXml(itemGroupXmlList);
                XmlFileData = itemGroupXmlList;
                XmlNodeList itemGroupNodeList = outputXMLDocOfItemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());
                XmlToObject(itemGroupNodeList);

            }
           // outputXMLDocOfItemGroup.RmoveAll();
           // Returning object;
            return this;
        }

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> ModifiedPopulateItemGroupList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Item Group List  from QuickBooks.

            //Getting item list from QuickBooks.
            string itemGroupXmlList = string.Empty;
            itemGroupXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceipt = new XmlDocument();


            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (itemGroupXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfReceipt.LoadXml(itemGroupXmlList);
                XmlFileData = itemGroupXmlList;
                XmlNodeList itemGroupNodeList = outputXMLDocOfReceipt.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> ModifiedPopulateItemGroupList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Item Group List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupRetXmlList = string.Empty;
            ItemGroupRetXmlList = QBCommonUtilities.GetListFromQuickBookByDate("ItemGroupQueryRq", QBFileName, FromDate, ToDate);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ItemGroupRetXmlList != string.Empty)
            {
                XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                outputXMLDocOfitemGroup.RemoveAll();
            }

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for getting ItemGroup List by Filter 
        /// List Filter
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="PaidStatus"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupListEntityFilter(string QBFileName, List<string> EntityFilter)
        {
            #region Getting ItemGroupList List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupXmlList = string.Empty;
            ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("ItemGroupQueryRq", QBFileName, EntityFilter);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (ItemGroupXmlList != string.Empty)
            {
                outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
                XmlFileData = ItemGroupXmlList;

                #region Getting Item Group Values from XML

                //Getting values from QuickBooks response.
                XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfitemGroup.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting ItemGroupList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {
            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupXmlList = string.Empty;
            ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ItemGroupXmlList != string.Empty)
            {
                outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
                XmlFileData = ItemGroupXmlList;

                #region Getting Item Group Values from XML

                //Getting mileage values from QuickBooks response.
                XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                outputXMLDocOfitemGroup.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting Item ReceiptList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {

            #region Getting Bills List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupXmlList = string.Empty;
            ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ItemGroupXmlList != string.Empty)
            {

                outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
                XmlFileData = ItemGroupXmlList;

                #region Getting Item Receipt Values from XML

                //Getting mileage values from QuickBooks response.
                XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                outputXMLDocOfitemGroup.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }


        /// <summary>
        /// This Method is used for getting ItemReceipt List by Filter
        /// TxndateRangeFilter. 
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter, bool flag)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupXmlList = string.Empty;
            ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfitemGroup = new XmlDocument();


            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ItemGroupXmlList != string.Empty)
            {

                outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
                XmlFileData = ItemGroupXmlList;

                #region Getting Item Receipt Values from XML

                //Getting mileage values from QuickBooks response.
                XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                outputXMLDocOfitemGroup.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }



        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// RefNnumberRangeFilter,entity filter
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        //public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, string FromRefnum, string ToRefnum, string entityFilter, bool flag)
        //{

        //    #region Getting Bills List from QuickBooks.

        //    //Getting item list from QuickBooks.
        //    string ItemGroupXmlList = string.Empty;
        //    ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
        //    #endregion


        //    //Create a xml document for output result.
        //    XmlDocument outputXMLDocOfReceipt = new XmlDocument();

        //    //Getting current version of System. BUG(676)
        //    string countryName = string.Empty;
        //    countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

        //    if (ItemGroupXmlList != string.Empty)
        //    {

        //        outputXMLDocOfReceipt.LoadXml(ItemGroupXmlList);
        //        XmlFileData = ItemGroupXmlList;

        //        #region Getting Item Receipt Values from XML

        //        //Getting mileage values from QuickBooks response.
        //        XmlNodeList itemGroupNodeList = outputXMLDocOfReceipt.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());





        //        XmlToObject(itemGroupNodeList);

        //        outputXMLDocOfReceipt.RemoveAll();
        //        #endregion
        //    }

        //    //Returning object.
        //    return this;
        //}

             

        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// RefNnumberRangeFilter.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        //public Collection<ItemGroupList> PopulateItemGroupList(string QBFileName, string FromRefnum, string ToRefnum)
        //{

        //    #region Getting Items List from QuickBooks.

        //    //Getting item list from QuickBooks.
        //    string ItemGroupXmlList = string.Empty;
        //    ItemGroupXmlList = QBCommonUtilities.GetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromRefnum, ToRefnum);
        //    #endregion


        //    //Create a xml document for output result.
        //    XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

        //    //Getting current version of System. BUG(676)
        //    string countryName = string.Empty;
        //    countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
        //    if (ItemGroupXmlList != string.Empty)
        //    {

        //        outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
        //        XmlFileData = ItemGroupXmlList;

        //        #region Getting Item Group Values from XML

        //        //Getting mileage values from QuickBooks response.
        //        XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

        //        XmlToObject(itemGroupNodeList);

        //        outputXMLDocOfitemGroup.RemoveAll();
        //        #endregion
        //    }

        //    //Returning object.
        //    return this;
        //}

            
        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        public Collection<ItemGroupList> ModifiedPopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter, bool flag)
        {

            #region Getting Item Group List from QuickBooks.

            //Getting item list from QuickBooks.
            string ItemGroupXmlList = string.Empty;
            ItemGroupXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemGroupQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
            #endregion

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfReceipt = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
            if (ItemGroupXmlList != string.Empty)
            {

                outputXMLDocOfReceipt.LoadXml(ItemGroupXmlList);
                XmlFileData = ItemGroupXmlList;

                #region Getting Item Receipt Values from XML

                //Getting mileage values from QuickBooks response.
                XmlNodeList itemGroupNodeList = outputXMLDocOfReceipt.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

                XmlToObject(itemGroupNodeList);

                outputXMLDocOfReceipt.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
        }

              


        /// <summary>
        /// This Method is used for getting BillList by Filter
        /// TxnDateRangeFilter and RefNumberRangeFilter.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="FromRefnum"></param>
        /// <param name="ToRefnum"></param>
        /// <returns></returns>
        //public Collection<ItemGroupList> ModifiedPopulateItemGroupList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        //{

        //    #region Getting ItemGroupList List from QuickBooks.

        //    //Getting item list from QuickBooks.
        //    string ItemGroupXmlList = string.Empty;
        //    ItemGroupXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("ItemReceiptQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
        //    #endregion

        //    //Create a xml document for output result.
        //    XmlDocument outputXMLDocOfitemGroup = new XmlDocument();

        //    //Getting current version of System. BUG(676)
        //    string countryName = string.Empty;
        //    countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;
        //    if (ItemGroupXmlList != string.Empty)
        //    {

        //        outputXMLDocOfitemGroup.LoadXml(ItemGroupXmlList);
        //        XmlFileData = ItemGroupXmlList;

        //        #region Getting Item Group Values from XML

        //        //Getting mileage values from QuickBooks response.
        //        XmlNodeList itemGroupNodeList = outputXMLDocOfitemGroup.GetElementsByTagName(QBItemGroupList.ItemGroupRet.ToString());

        //        XmlToObject(itemGroupNodeList);

        //        outputXMLDocOfitemGroup.RemoveAll();
        //        #endregion
        //    }

        //    //Returning object.
        //    return this;
        //}

              


        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
    }
}

