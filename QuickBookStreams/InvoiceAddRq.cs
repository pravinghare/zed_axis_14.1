using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;
using DataProcessingBlocks;
using System.Xml.Serialization;
using TransactionImporter;
using System.Windows.Forms;
using EDI.Message;
using System.Xml;

namespace Streams
{
    [XmlRootAttribute("InvoiceAddRq", Namespace = "", IsNullable = false)]
    public class InvoiceAddRq
    {
        #region Private Members

        private int m_messageId;
        private InvoiceAdd m_invoiceAdd;
        private string m_response;
        private int m_failureCount;
        private int m_successCount;
        private bool m_requestCancel;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or Sets the OS1 stream
        /// </summary>
        public InvoiceAdd InvoiceAdd
        {
            get { return m_invoiceAdd; }
            set { m_invoiceAdd = value; }
        }

        #endregion

        #region Constructors

        public InvoiceAddRq(int messageId)
        {
            m_messageId = messageId;
            m_requestCancel = false;
            m_response = string.Empty;
            m_failureCount = 0;
            m_successCount = 0;
            m_invoiceAdd = new InvoiceAdd();
        }
        public InvoiceAddRq()
        { }
        #endregion

        #region Private Methods

        private bool AddOS1(string[] stream)
        {
            if (stream.Length >= 17)
            {
                m_invoiceAdd.InvoiceLineAdd.Clear();
              
                if (stream[2].Trim() == string.Empty)
                {
                    throw new Exception("Customer field missing");
                }
                else
                {
                    m_invoiceAdd.CustomerRef.ListID = stream[2].Trim();
                }
                try
                {
                    m_invoiceAdd.ShipDate = stream[3].Trim();
                }
                catch { } 
                try
                {
                    m_invoiceAdd.TxnDate = stream[4].Trim() == string.Empty ? DateTime.Now.ToString(Constants.ISMFileDateTime) : stream[4].Trim();
                }
                catch { }
                m_invoiceAdd.ShipAddress.Addr1 = stream[5].Trim() == string.Empty ? string.Empty : stream[5].Trim();
                m_invoiceAdd.ShipAddress.Addr2 = stream[6].Trim() == string.Empty ? string.Empty : stream[6].Trim();
                m_invoiceAdd.ShipAddress.City = stream[7].Trim() == string.Empty ? string.Empty : stream[7].Trim();
                m_invoiceAdd.ShipAddress.State = stream[8].Trim() == string.Empty ? string.Empty : stream[8].Trim();
                m_invoiceAdd.ShipAddress.PostalCode = stream[9].Trim() == string.Empty ? string.Empty : stream[9].Trim();
                m_invoiceAdd.ShipAddress.Country = stream[10].Trim() == string.Empty ? string.Empty : stream[10].Trim();
                m_invoiceAdd.ShipAddress.Note = stream[11].Trim() == string.Empty ? string.Empty : stream[11].Trim();
                if (stream[13].Trim() == string.Empty)
                {
                    m_invoiceAdd.ShipMethodRef = null;
                }
                else
                {
                    m_invoiceAdd.ShipMethodRef.FullName = stream[13].Trim();
                }
                m_invoiceAdd.PONumber = stream[14].Trim() == string.Empty ? string.Empty : stream[14].Trim();
                
              
                DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();

                #region Checking and setting TaxCode

               
                string TaxRateValue = string.Empty;
                string ItemSaleTaxFullName = string.Empty;
                
                if (defaultSettings.GrossToNet == "1")
                {

                    if (defaultSettings.TaxCode != string.Empty)
                    {
                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, defaultSettings.TaxCode);

                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                    }
                    
                }
                #endregion


                //for refnumber
                if (stream.Length > 15)
                {
                    if (stream[15] != null && stream[15] != string.Empty)
                        m_invoiceAdd.RefNumber = stream[15].Trim() == string.Empty ? string.Empty : stream[15].Trim();//For Bug#619 fixes.
                }
                
                //FOR FREIGHT_CHARGE
                if (stream.Length > 17)
                {
                    if (stream[17] != null && stream[17] != string.Empty)
                    {
                        decimal rate = 0;
                        
                        if (!decimal.TryParse(m_invoiceAdd.FreightCharge.ToString(), out rate))
                        {

                        }
                        else
                        {
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (TaxRateValue != string.Empty)
                                {
                                    decimal Rate = Convert.ToDecimal(stream[17].Trim());

                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                    {
                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                        rate = Rate / (1 + (TaxRate / 100));
                                        m_invoiceAdd.FreightCharge = Convert.ToDecimal(Math.Round(rate, 5));
                                    }
                                    else
                                        m_invoiceAdd.FreightCharge = Convert.ToDecimal(Math.Round(Convert.ToDecimal(stream[17].Trim()), 5));

                                    
                                }
                                //Check if EstLine.Rate is null
                                if (m_invoiceAdd.FreightCharge.ToString() == null)
                                {
                                    m_invoiceAdd.FreightCharge = Convert.ToDecimal(Math.Round(Convert.ToDecimal(stream[17].Trim()), 5));
                                }
                            }
                            else
                            {
                                m_invoiceAdd.FreightCharge = Convert.ToDecimal(Math.Round(Convert.ToDecimal(stream[17].Trim()), 5));
                            }
                        }
                    }
                }

                if (stream.Length > 18)
                {
                    if (stream[18] != null && stream[18] != string.Empty)
                        m_invoiceAdd.ConsignmentRef = stream[18].Trim();
                }

                if (stream.Length > 19)
                {
                    if (stream[19] != null && stream[19] != string.Empty)
                        m_invoiceAdd.No_of_cartons = stream[19].Trim();
                }

                if (stream.Length > 20)
                {
                    if (stream[20] != null && stream[20] != string.Empty)
                        m_invoiceAdd.No_of_pallets = stream[20].Trim();
                }

                return true;
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }

        }

        private bool AddOS2(string[] stream)
        {
            InvoiceLineAdd invoiceadd = new InvoiceLineAdd();
            if (stream.Length == 3)
            {
                if (stream[1].Trim() == string.Empty)
                {
                    throw new Exception("Item name field missing");
                }
                else
                {
                    invoiceadd.ItemRef.FullName = stream[1].Trim();
                }
               

                if (stream[2].Trim() == string.Empty)
                {
                    throw new Exception("Item quantity field missing");
                }
                else
                {
                   invoiceadd.Quantity = Convert.ToInt32(stream[2].Trim());
                }
                             
                m_invoiceAdd.InvoiceLineAdd.Add(invoiceadd);
                return true;
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }
        }
        private bool AddOSFrieght(InvoiceAdd invoice)
        {
            InvoiceLineAdd invoiceadd = new InvoiceLineAdd();
            invoiceadd.ItemRef.FullName = "Freight";

            

            string desc = string.Empty;
            if (!string.IsNullOrEmpty(invoice.ConsignmentRef) || !string.IsNullOrEmpty(invoice.No_of_pallets) || !string.IsNullOrEmpty(invoice.No_of_cartons))
            {
                desc = "Consignment Ref:" + invoice.ConsignmentRef + "\n"
                       + "No of cartons:" + invoice.No_of_pallets + "\n"
                       + "No of pallets:" + invoice.No_of_cartons;

                invoiceadd.Desc = desc;
                
                m_invoiceAdd.InvoiceLineAdd.Add(invoiceadd);
            }
            if (!string.IsNullOrEmpty(invoice.FreightCharge.ToString()))
                invoiceadd.Rate = invoice.FreightCharge.ToString();

            //add quantity to 1 as per client suggestion while adding new Frieght item
            invoiceadd.Quantity = 1;

            #region Set Item Query
            DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();

            string ItemName = invoiceadd.ItemRef.FullName.ToString();
            string[] arr = new string[15];
            if (ItemName.Contains(":"))
            {
                arr = ItemName.Split(':');
            }
            else
            {
                arr[0] = invoiceadd.ItemRef.FullName.ToString();
            }


            string TaxRateValue = string.Empty;
            string IsTaxIncluded = string.Empty;
            string netRate = string.Empty;
            string ItemSaleTaxFullName = string.Empty;

            for (int i = 0; i < arr.Length; i++)
            {
                int item = 0;
                int a = 0;
                if (arr[i] != null && arr[i] != string.Empty)
                {
                    #region Passing Items Query
                    XmlDocument pxmldoc = new XmlDocument();
                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                    pxmldoc.AppendChild(qbXML);
                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                    qbXML.AppendChild(qbXMLMsgsRq);
                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                    ItemQueryRq.SetAttribute("requestID", "1");


                    if (i > 0)
                    {
                        if (arr[i] != null && arr[i] != string.Empty)
                        {
                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                            for (item = 0; item <= i; item++)
                            {
                                if (arr[item].Trim() != string.Empty)
                                {
                                    FullName.InnerText += arr[item].Trim() + ":";
                                }
                            }
                            if (FullName.InnerText != string.Empty)
                            {
                                ItemQueryRq.AppendChild(FullName);
                            }

                        }
                    }
                    else
                    {
                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                        FullName.InnerText = arr[i];
                        ItemQueryRq.AppendChild(FullName);
                    }

                    string pinput = pxmldoc.OuterXml;

                    string resp = string.Empty;
                    try
                    {
                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                    finally
                    {

                        if (resp != string.Empty)
                        {
                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                            outputXMLDoc.LoadXml(resp);
                            string statusSeverity = string.Empty;
                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                            {
                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                            }
                            outputXMLDoc.RemoveAll();
                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                            {

                                if (defaultSettings.Type == "NonInventoryPart")
                                {
                                    #region Item NonInventory Add Query

                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                    ININame.InnerText = arr[i];
                                    
                                    ItemNonInventoryAdd.AppendChild(ININame);

                                    //Solution for BUG 633
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {

                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                            for (a = 0; a <= i - 1; a++)
                                            {
                                                if (arr[a].Trim() != string.Empty)
                                                {

                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                }
                                            }
                                            if (INIChildFullName.InnerText != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                INIParent.AppendChild(INIChildFullName);
                                            }

                                        }
                                    }

                                    //Adding Tax Code Element.
                                    if (defaultSettings.TaxCode != string.Empty)
                                    {
                                        if (defaultSettings.TaxCode.Length < 4)
                                        {

                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                        }
                                    }

                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                    bool IsPresent = false;


                                    //Adding Desc And Rate
                                    //Solution for BUG 631 and 632

                                    if (invoiceadd.Desc != string.Empty)
                                    {
                                        XmlElement INIDesc = ItemNonInvendoc.CreateElement("Desc");
                                        INIDesc.InnerText = invoiceadd.Desc;
                                        INISalesAndPurchase.AppendChild(INIDesc);
                                        IsPresent = true;
                                    }



                                    if (invoiceadd.Rate != string.Empty)
                                    {
                                        XmlElement ISRate = ItemNonInvendoc.CreateElement("Price");
                                        ISRate.InnerText = netRate;
                                        INISalesAndPurchase.AppendChild(ISRate);
                                        IsPresent = true;
                                    }
                                    
                                    if (defaultSettings.IncomeAccount != string.Empty)
                                    {
                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                        //INIFullName.InnerText = "Sales";
                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                        IsPresent = true;
                                    }
                                    if (IsPresent == true)
                                    {
                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                    }
                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                    //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                    string respItemNonInvendoc = string.Empty;
                                    try
                                    {
                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                        outputXML.LoadXml(respItemNonInvendoc);
                                        string StatusSeverity = string.Empty;
                                        string statusMessage = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                        {
                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                        }
                                        outputXML.RemoveAll();
                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                        {
                                            //Task 1435 (Axis 6.0):
                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                            summary.ShowDialog();
                                            CommonUtilities.WriteErrorLog(statusMessage);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    string strtest2 = respItemNonInvendoc;

                                    #endregion
                                }
                                else if (defaultSettings.Type == "Service")
                                {
                                    #region Item Service Add Query

                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                
                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                    NameIS.InnerText = arr[i];
                                    
                                    ItemServiceAdd.AppendChild(NameIS);

                                    //Solution for BUG 633
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {

                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                            for (a = 0; a <= i - 1; a++)
                                            {
                                                if (arr[a].Trim() != string.Empty)
                                                {
                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                }
                                            }
                                            if (INIChildFullName.InnerText != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                ItemServiceAdd.AppendChild(INIParent);

                                                INIParent.AppendChild(INIChildFullName);
                                            }

                                        }
                                    }
                                    //Adding Tax code Element.
                                    if (defaultSettings.TaxCode != string.Empty)
                                    {
                                        if (defaultSettings.TaxCode.Length < 4)
                                        {
                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                        }
                                    }


                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                    bool IsPresent = false;
                                    

                                    //Adding Desc And Rate
                                    //Solution for BUG 631 and 632


                                    if (invoiceadd.Desc != string.Empty)
                                    {
                                        XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                        ISDesc.InnerText = invoiceadd.Desc;
                                        ISSalesAndPurchase.AppendChild(ISDesc);
                                        IsPresent = true;
                                    }
                                    


                                    if (invoiceadd.Rate != string.Empty)
                                    {
                                        XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                        ISRate.InnerText = netRate;
                                        ISSalesAndPurchase.AppendChild(ISRate);
                                        IsPresent = true;
                                    }
                                    

                                    if (defaultSettings.IncomeAccount != string.Empty)
                                    {
                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                        //Adding IncomeAccount FullName.
                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                        IsPresent = true;
                                    }

                                    if (IsPresent == true)
                                    {
                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                    }
                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                    
                                    string respItemServiceAddinputdoc = string.Empty;
                                    try
                                    {
                                    
                                        respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                        string StatusSeverity = string.Empty;
                                        string statusMessage = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                        {
                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                        }
                                        outputXML.RemoveAll();
                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                        {
                                            //Task 1435 (Axis 6.0):
                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                            summary.ShowDialog();
                                            CommonUtilities.WriteErrorLog(statusMessage);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    string strTest3 = respItemServiceAddinputdoc;
                                    #endregion
                                }
                                else if (defaultSettings.Type == "InventoryPart")
                                {
                                    #region Inventory Add Query
                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                    NameIS.InnerText = arr[i];
                                    
                                    ItemInventoryAdd.AppendChild(NameIS);

                                    //Solution for BUG 633
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {

                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                            for (a = 0; a <= i - 1; a++)
                                            {
                                                if (arr[a].Trim() != string.Empty)
                                                {

                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                }
                                            }
                                            if (INIChildFullName.InnerText != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                ItemInventoryAdd.AppendChild(INIParent);

                                                INIParent.AppendChild(INIChildFullName);
                                            }

                                        }
                                    }

                                    //Adding Tax code Element.
                                    if (defaultSettings.TaxCode != string.Empty)
                                    {
                                        if (defaultSettings.TaxCode.Length < 4)
                                        {
                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                        }
                                    }

                                    //Adding Desc
                                    if (invoiceadd.Desc != string.Empty)
                                    {
                                        XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                        ISDesc.InnerText = invoiceadd.Desc;
                                        ItemInventoryAdd.AppendChild(ISDesc);
                                    }



                                    if (invoiceadd.Rate != string.Empty)
                                    {
                                        XmlElement ISRate = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                        ISRate.InnerText = netRate;
                                        ItemInventoryAdd.AppendChild(ISRate);
                                    }
                                    


                                    //Adding IncomeAccountRef
                                    if (defaultSettings.IncomeAccount != string.Empty)
                                    {
                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                    }

                                    //Adding COGSAccountRef
                                    if (defaultSettings.COGSAccount != string.Empty)
                                    {
                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                    }

                                    //Adding AssetAccountRef
                                    if (defaultSettings.AssetAccount != string.Empty)
                                    {
                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                    }

                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                    string respItemInventoryAddinputdoc = string.Empty;
                                    try
                                    {
                                        respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                        string StatusSeverity = string.Empty;
                                        string statusMessage = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                        {
                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                        }
                                        outputXML.RemoveAll();
                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                        {
                                            //Task 1435 (Axis 6.0):
                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                            summary.ShowDialog();
                                            CommonUtilities.WriteErrorLog(statusMessage);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    string strTest4 = respItemInventoryAddinputdoc;
                                    #endregion
                                }
                            }
                        }

                    }

                    #endregion
                }
            }
            #endregion


            return true;

        }

        #endregion

        #region Public Methods

        public void Parse(string attachmentContent)
        {
            int startIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;

            while (endIndex <= (attachmentContent.Length))
            {
                string[] stream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                if (stream[0].Trim().Equals("OS1"))
                {
                    if (m_invoiceAdd.CustomerRef.ListID != null)
                    {
                        if (!ExportToQuickBooks())
                        {
                            break;
                        }
                    }
                    if (!AddOS1(stream))
                    {
                        throw new Exception("Error in parsing OS1 stream in the attachment");
                    }
                }
                else if (stream[0].Trim().Equals("OS2"))
                {
                    if (!AddOS2(stream))
                    {
                        throw new Exception("Error in parsing the OS2 stream in the attachment");
                    }
                   
                }
                else
                {
                    //throw new Exception("Attachment is not in correct format");
                }
                startIndex = endIndex + Constants.NEWLINE.Length;
                if (startIndex < attachmentContent.Length)
                {
                    endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                }
                else
                {
                    break;
                }
            }
            if (m_invoiceAdd.CustomerRef.ListID != null)
            {
                if (!AddOSFrieght(m_invoiceAdd))
                {
                  
                }
                if (ExportToQuickBooks())
                {
                  
                }
                else
                {
                   
                }               
            }

            if (CommonUtilities.GetInstance().IsInboundFlag == true)
            {
                CommonUtilities.WriteErrorLog(m_response);
              
                if (m_failureCount > 0)
                    CommonUtilities.GetInstance().FailureCount = CommonUtilities.GetInstance().FailureCount + 1;
                else if (m_successCount > 0)
                    CommonUtilities.GetInstance().ProcessedCount = CommonUtilities.GetInstance().ProcessedCount + 1;

            }
            else
            {
                ImportSummary importSummary = new ImportSummary();
                importSummary.SDKMSG = m_response;
                importSummary.FailMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG020"), Convert.ToString(m_failureCount));
                importSummary.SuccesMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG019"), Convert.ToString(m_successCount));
                importSummary.ShowDialog();
                importSummary.Dispose();
            }
            if (m_failureCount > 0)
            {
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Failed);
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Failed);
                CommonUtilities.WriteErrorLog(m_response);                
            }
            else if (m_successCount > 0)
            {
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Processed);
                new MessageController().ChangeAttachmentStatus(m_messageId, (int)MessageStatus.Processed);
            }
            else
            {
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Failed);
                new MessageController().ChangeMessageStatus(m_messageId, (int)MessageStatus.Failed);
                CommonUtilities.WriteErrorLog(m_response);          
            }
        }

        private bool ExportToQuickBooks()
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<InvoiceAddRq>.Save(this, fileName);
            }
            catch 
            {

                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillAddRq aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceAddRq = requestXmlDoc.CreateElement("InvoiceAddRq");
            inner.AppendChild(InvoiceAddRq);
            InvoiceAddRq.InnerXml = requestXML.Replace("<InvoiceLineAddREM>", string.Empty).Replace("</InvoiceLineAddREM>",string.Empty);
            //For add request id to track error message
            string requeststring = string.Empty;



            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);             
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(string.Empty, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            m_response = m_response + oNode.Attributes.Item(2).InnerText + "\r\n";
                            m_failureCount++;
                           
                        }
                        else if (statusSeverity == "Info")
                        {
                            m_response = "Record Inserted Successfully";
                            m_successCount++;
                            
                        }
                        else if (statusSeverity.ToLower() == "warn")
                        {
                            m_response = m_response + oNode.Attributes.Item(2).InnerText + "\r\n";
                            m_failureCount++;
                           
                        }
                    }

                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
          
            if (m_successCount > 0)
                return true;
            else
                return false;
        }

        #endregion
    }
}
