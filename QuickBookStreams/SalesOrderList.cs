// ===============================================================================
// 
// Sale3sOrderList.cs
//
// This file contains the implementations of the QuickBooks Sales Order request methods and
// Properties.
//
// Developed By : Sandeep Patil.
// Date : 10/03/2009
// Modified By : Modify by K. Gouraw
// Date : 17/03/2009
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This class provides properties and methods of QuickBooks SalesOrder List.
    /// </summary>
    public class SalesOrderList : BaseSalesOrderList
    {
        #region Public Properties

        /// <summary>
        /// Get and Set Refnumber of Quickbooks.
        /// </summary>
        public string RefNumber
        {
            get
            {
                return m_invoiceNumber;
            }
            set
            {
                if (value.Length > 30)
                {
                    m_invoiceNumber = value.Remove(30, (value.Length - 30));
                }
                else
                {
                    m_invoiceNumber = value.Trim();
                }
            }
        }

        /// <summary>
        /// Get and Set Customer List ID of Quickbooks.
        /// </summary>
        public string CustomerListID
        {
            get
            { return m_customerId; }
            set
            {
                if (value.Length > 36)
                    m_customerId = value.Remove(36, (value.Length - 36));
                else
                    m_customerId = value;
            }
        }

        /// <summary>
        /// Get and Set Ship Date of QuickBooks.
        /// </summary>
        public string ShipDate
        {
            get
            { return m_promisedDate; }
            set
            {
                m_promisedDate = value;
            }
        }


        /// <summary>
        /// Get and Set TxnID of QuickBooks.
        /// </summary>
        public string TxnID
        {
            get
            { return m_TxnID; }
            set
            {
                if (value.Length > 40)
                {
                    m_TxnID = value.Remove(40, (value.Length - 40));
                }
                else

                    m_TxnID = value;
            }
        }

        public string TxnNumber
        {
            get { return m_TxnNumber; }
            set { m_TxnNumber = value; }
        }


        /// <summary>
        /// Get and Set Invoice Date of QuickBooks.
        /// </summary>
        public string TxnDate
        {
            get
            {
                return m_invoiceDate;
            }
            set
            {
                m_invoiceDate = value;
            }
        }

        /// <summary>
        /// Get and Set Ship Address1 of Quickbooks.
        /// </summary>
        public string ShipToAdd
        {
            get
            { return m_shipToAddress; }
            set
            {
                if (value.Length > 60)
                    m_shipToAddress = value.Remove(60, (value.Length - 60));
                else
                    m_shipToAddress = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address2 of Quickbooks
        /// </summary>
        public string ShipToAdd1
        {
            get
            { return m_shipToAddressLine1; }
            set
            {
                if (value.Length > 40)
                    m_shipToAddressLine1 = value.Remove(40, (value.Length - 40));
                else
                    m_shipToAddressLine1 = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address City of QuickBooks
        /// </summary>
        public string ShipToAdd2
        {
            get
            { return m_shipToAddressLine2; }
            set
            {
                if (value.Length > 40)
                    m_shipToAddressLine2 = value.Remove(40, (value.Length - 40));
                else
                    m_shipToAddressLine2 = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address State of QuickBooks.
        /// </summary>
        public string ShipToAdd3
        {
            get
            { return m_shipToAddressLine3; }
            set
            {
                if (value.Length > 40)
                    m_shipToAddressLine3 = value.Remove(40, (value.Length - 40));
                else
                    m_shipToAddressLine3 = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address PostalCode of QuickBooks.
        /// </summary>
        public string ShipToAdd4
        {
            get
            { return m_shipToAddressLine4; }
            set
            {
                if (value.Length > 40)
                    m_shipToAddressLine4 = value.Remove(40, (value.Length - 40));
                else
                    m_shipToAddressLine4 = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address PostalCode of QuickBooks.
        /// </summary>
        public string ShipCity
        {
            get
            { return m_shipCity; }
            set
            {
                if (value.Length > 40)
                    m_shipCity = value.Remove(40, (value.Length - 40));
                else
                    m_shipCity = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address PostalCode of QuickBooks.
        /// </summary>
        public string ShipState
        {
            get
            { return m_shipState; }
            set
            {
                if (value.Length > 40)
                    m_shipState = value.Remove(40, (value.Length - 40));
                else
                    m_shipState = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address PostalCode of QuickBooks.
        /// </summary>
        public string ShipPostalCode
        {
            get
            { return m_shipPostalCode; }
            set
            {
                if (value.Length > 40)
                    m_shipPostalCode = value.Remove(40, (value.Length - 40));
                else
                    m_shipPostalCode = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address PostalCode of QuickBooks.
        /// </summary>
        public string ShipCountry
        {
            get
            { return m_shipCountry; }
            set
            {
                if (value.Length > 40)
                    m_shipCountry = value.Remove(40, (value.Length - 40));
                else
                    m_shipCountry = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address Country of QuickBooks.
        /// </summary>
        public string Country
        {
            get
            { return m_country; }
            set
            {
                if (value.Length > 40)
                    m_country = value.Remove(40, (value.Length - 40));
                else
                    m_country = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address Country of QuickBooks.
        /// </summary>
        public string City
        {
            get
            { return m_city; }
            set
            {
                m_city = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address Country of QuickBooks.
        /// </summary>
        public string State
        {
            get
            { return m_state; }
            set
            {

                m_state = value;
            }
        }
        /// <summary>
        /// Get or Set Ship Address Country of QuickBooks.
        /// </summary>
        public string PostalCode
        {
            get
            { return m_postalCode; }
            set
            {

                m_postalCode = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Address note of Quickbooks.
        /// </summary>
        public string Note
        {
            get
            { return m_note; }
            set
            {
                if (value.Length > 40)
                    m_note = value.Remove(40, (value.Length - 40));
                else
                    m_note = value;
            }
        }


        /// <summary>
        /// Get or Set Ship Method full name.
        /// </summary>
        public string ShipMethodFullName
        {
            get
            { return m_shippingMethod; }
            set
            {
                if (value.Length > 30)
                    m_shippingMethod = value.Remove(30, (value.Length - 30));
                else
                    m_shippingMethod = value;
            }
        }

        /// <summary>
        /// Get or set PONumber of QuickBooks.
        /// </summary>
        public string PONumber
        {
            get
            { return m_customerPoNumber; }
            set
            {
                if (value.Length > 30)
                    m_customerPoNumber = value.Remove(30, (value.Length - 30));
                else
                    m_customerPoNumber = value;
            }
        }

        /// <summary>
        /// Get or Set Memo of QuickBooks.
        /// </summary>
        public string Memo
        {
            get
            { return m_Comment; }
            set
            {
                if (value.Length > 30)
                    m_Comment = value.Remove(30, (value.Length - 30));
                else
                    m_Comment = value;
            }
        }





        //Get or Set TemplateRefFullName value.
        public string TemplateRefFullName
        {
            get
            {
                return m_TemplateRefFullName;
            }
            set
            {

                m_TemplateRefFullName = value;
            }
        }

        //Get or Set TermsRefFullName value.
        public string TermsRefFullName
        {
            get
            {
                return m_TermsRefFullName;
            }
            set
            {

                m_TermsRefFullName = value;
            }
        }

        //Get or Set DueDate value.
        public string DueDate
        {
            get
            {
                return m_DueDate;
            }
            set
            {

                m_DueDate = value;
            }
        }
        //Get or Set salesRepRefFullName value.
        //Axis-119 Export a Sales Order, Column for Sales Rep is missing data
        public string SalesRepRefFullName
        {
            get
            {
                return m_SalesRepRefFullName;
            }
            set
            {

                m_SalesRepRefFullName = value;
            }
        }
        //Get or Set FOB value.

        //Get or Set SubTotal value.
        public string SubTotal
        {
            get
            {
                return m_SubTotal;
            }
            set
            {

                m_SubTotal = value;
            }
        }
        //Get or Set Addr1 value.
        public string Addr1
        {
            get
            {
                return m_addr1;
            }
            set
            {

                m_addr1 = value;
            }
        }
        //Get or Set Addr2 value.
        public string Addr2
        {
            get
            {
                return m_addr2;
            }
            set
            {

                m_addr2 = value;
            }
        }
        //Get or Set Addr3 value.
        public string Addr3
        {
            get
            {
                return m_addr3;
            }
            set
            {

                m_addr3 = value;
            }
        }
        //Get or Set ItemSalesTaxRefFullName value.
        public string ItemSalesTaxRefFullName
        {
            get
            {
                return m_ItemSalesTaxRefFullName;
            }
            set
            {

                m_ItemSalesTaxRefFullName = value;
            }
        }
        //Get or Set SalesTaxPercentage value.
        public string SalesTaxPercentage
        {
            get
            {
                return m_SalesTaxPercentage;
            }
            set
            {

                m_SalesTaxPercentage = value;
            }
        }
        //Get or Set SalesTaxTotal value.
        public string SalesTaxTotal
        {
            get
            {
                return m_SalesTaxTotal;
            }
            set
            {

                m_SalesTaxTotal = value;
            }
        }
        //Get or Set TotalAmount value.
        public string TotalAmount
        {
            get
            {
                return m_TotalAmount;
            }
            set
            {

                m_TotalAmount = value;
            }
        }
        //Get or Set CurrencyRefFullName value.
        public string CurrencyRefFullName
        {
            get
            {
                return m_CurrencyRefFullName;
            }
            set
            {

                m_CurrencyRefFullName = value;
            }
        }
        //Get or Set ExchangeRate value.
        public string ExchangeRate
        {
            get
            {
                return m_ExchangeRate;
            }
            set
            {

                m_ExchangeRate = value;
            }
        }
        //Get or Set TotalAmountInHomeCurrency value.
        public string TotalAmountInHomeCurrency
        {
            get
            {
                return m_TotalAmountInHomeCurrency;
            }
            set
            {

                m_TotalAmountInHomeCurrency = value;
            }
        }

        //Get or Set CustomerMsgRefFullName value.
        public string CustomerMsgRefFullName
        {
            get
            {
                return m_CustomerMsgRefFullName;
            }
            set
            {

                m_CustomerMsgRefFullName = value;
            }
        }
        //Get or Set IsToBePrinted value.
        public string IsToBePrinted
        {
            get
            {
                return m_IsToBePrinted;
            }
            set
            {

                m_IsToBePrinted = value;
            }
        }
        //Get or Set IsToBeEmailed value.
        public string IsToBeEmailed
        {
            get
            {
                return m_IsToBeEmailed;
            }
            set
            {

                m_IsToBeEmailed = value;
            }
        }
        //Get or Set CustomerSalesTaxCodeRefFullName value.
        public string CustomerSalesTaxCodeRefFullName
        {
            get
            {
                return m_CustomerSalesTaxCodeRefFullName;
            }
            set
            {

                m_CustomerSalesTaxCodeRefFullName = value;
            }
        }
        //Get or Set Other value.
        public string Other
        {
            get
            {
                return m_Other;
            }
            set
            {

                m_Other = value;
            }
        }

        //Get or Set UnitOfMeasure value.
        public string UnitOfMeasure
        {
            get
            {
                return m_UnitOfMeasure;
            }
            set
            {

                m_UnitOfMeasure = value;
            }
        }

        //Get or Set SalesTaxCodeRefFullName value.
        public string SalesTaxCodeRefFullName
        {
            get
            {
                return m_SalesTaxCodeRefFullName;
            }
            set
            {

                m_SalesTaxCodeRefFullName = value;
            }
        }
        //Get or Set CustomerFullName value.
        public string CustomerFullName
        {
            get
            {
                return m_customerFullName;
            }
            set
            {

                m_customerFullName = value;
            }
        }
        //Get or Set ClassRefFullName value.
        public string ClassRefFullName
        {
            get
            {
                return m_ClassRefFullName;
            }
            set
            {

                m_ClassRefFullName = value;
            }
        }

        public List<string> Other1
        {
            get { return m_Other1; }
            set { m_Other1 = value; }
        }

        public List<string> Other2
        {
            get { return m_Other2; }
            set { m_Other2 = value; }
        }

        //FOB
        public string FOB
        {
            get
            {
                return m_FOB;
            }
            set
            {
                m_FOB = value;
            }
        }
        //Get or Set IsManuallyClosed value.
        public string IsManuallyClosed
        {
            get
            {
                return m_IsManuallyClosed;
            }
            set
            {

                m_IsManuallyClosed = value;
            }
        }
        //Get or Set IsFullyInvoiced value.
        public string IsFullyInvoiced
        {
            get
            {
                return m_IsFullyInvoiced;
            }
            set
            {

                m_IsFullyInvoiced = value;
            }
        }

        //Linked TxnID
        public string LinkedTxn
        {
            get { return m_LinkedTxnID; }
            set { m_LinkedTxnID = value; }
        }

        //For InvoiceLineRet
        public List<string> TxnLineID
        {
            get { return m_QBTxnLineNumber; }
            set { m_QBTxnLineNumber = value; }
        }

        public List<string> ItemFullName
        {
            get { return m_QBitemFullName; }
            set { m_QBitemFullName = value; }
        }

        public List<string> Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public List<decimal?> Quantity
        {
            get { return m_QBquantity; }
            set { m_QBquantity = value; }
        }

        public List<string> UOM
        {
            get { return m_UOM; }
            set { m_UOM = value; }
        }

        public List<string> OverrideUOMFullName
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }

        public List<decimal?> Rate
        {
            get { return m_Rate; }
            set { m_Rate = value; }
        }

        public List<decimal?> Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        public List<string> InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }
        // axis 10.0 changes

        public List<string> SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public List<string> LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        // axis 10.0 changes ends

        public List<string> ServiceDate
        {
            get { return m_ServiceDate; }
            set { m_ServiceDate = value; }
        }

        public List<string> SalesTaxCodeFullName
        {
            get { return m_SalesTaxCodeFullName; }
            set { m_SalesTaxCodeFullName = value; }
        }

        public List<string> Invoiced
        {
            get { return m_Invoiced; }
            set { m_Invoiced = value; }
        }

        public List<string> LineIsManuallyClosed
        {
            get { return m_LineIsManuallyClosed; }
            set { m_LineIsManuallyClosed = value; }
        }

        public List<string> LineClass
        {
            get { return m_LineClass; }
            set { m_LineClass = value; }
        }
        //For InvoiceLineGroupRet
        public List<string> GroupTxnLineID
        {
            get { return m_GroupTxnLineID; }
            set { m_GroupTxnLineID = value; }
        }

        public List<string> ItemGroupFullName
        {
            get { return m_ItemGroupFullName; }
            set { m_ItemGroupFullName = value; }
        }

        public List<string> GroupDesc
        {
            get { return m_GroupDesc; }
            set { m_GroupDesc = value; }
        }

        public List<decimal?> GroupQuantity
        {
            get { return m_GroupQuantity; }
            set { m_GroupQuantity = value; }
        }

        public List<string> IsPrintItemsInGroup
        {
            get { return m_IsPrintItemsInGroup; }
            set { m_IsPrintItemsInGroup = value; }
        }


        //For Sub InvoiceLineRet
        public List<string> GroupLineTxnLineID
        {
            get { return m_GroupQBTxnLineNumber; }
            set { m_GroupQBTxnLineNumber = value; }
        }

        public List<string> GroupLineItemFullName
        {
            get { return m_GroupLineItemFullName; }
            set { m_GroupLineItemFullName = value; }
        }

        public List<string> GroupLineDesc
        {
            get { return m_GroupLineDesc; }
            set { m_GroupLineDesc = value; }
        }

        public List<decimal?> GroupLineQuantity
        {
            get { return m_GroupLineQuantity; }
            set { m_GroupLineQuantity = value; }
        }

        public List<string> GroupLineUOM
        {
            get { return m_GroupLineUOM; }
            set { m_GroupLineUOM = value; }
        }

        public List<string> GroupLineOverrideUOM
        {
            get { return m_OverrideUOMFullName; }
            set { m_OverrideUOMFullName = value; }
        }

        public List<decimal?> GroupLineRate
        {
            get { return m_GroupLineRate; }
            set { m_GroupLineRate = value; }
        }

        public List<decimal?> GroupLineAmount
        {
            get { return m_GroupLineAmount; }
            set { m_GroupLineAmount = value; }
        }

        public List<string> GroupLineServiceDate
        {
            get { return m_GroupLineServiceDate; }
            set { m_GroupLineServiceDate = value; }
        }

        public List<string> GroupLineSalesTaxCodeFullName
        {
            get { return m_GroupLineSalesTaxCodeFullName; }
            set { m_GroupLineSalesTaxCodeFullName = value; }
        }
        public List<string> DataExtName = new List<string>();
        public List<string> DataExtValue = new List<string>();
        #endregion
    }

    /// <summary>
    /// Collection class used for getting Sales Order List from QuickBooks.
    /// </summary>
    public class QBSalesOrderCollection : Collection<SalesOrderList>
    {
        string XmlFileData = string.Empty;
        //Axis 706
        QBItemDetails itemList = null;
        int i = 0;

        public void GettSalesOrderValuesFromXML(string salesOrderXmlList)
        {
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            int count = 0;
            int count1 = 0;
            SalesOrderList salesOrderList;
            string countryName = string.Empty;
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfSalesOrder = new XmlDocument();

            if (salesOrderXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfSalesOrder.LoadXml(salesOrderXmlList);
                XmlFileData = salesOrderXmlList;

                #region Getting Sales Order Values from XML

                //Getting Sales Order values from QuickBooks response.
                XmlNodeList SalesOrderNodeList = outputXMLDocOfSalesOrder.GetElementsByTagName(QBXmlSalesOrderList.SalesOrderRet.ToString());

                foreach (XmlNode SalesOrderNodes in SalesOrderNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        salesOrderList = new SalesOrderList();
                        count = 0;
                        i = 0;

                        // axis 10.0 changes ends
                        foreach (XmlNode node in SalesOrderNodes.ChildNodes)
                        {
                            #region  sales order
                            //Assinging values to class.
                            if (node.Name.Equals(QBXmlSalesOrderList.RefNumber.ToString()))
                                salesOrderList.RefNumber = node.InnerText;

                            //Checking Customer Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.ListID.ToString()))
                                        salesOrderList.CustomerListID = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.CustomerFullName = childNode.InnerText;
                                }
                            }
                            //Checking Template Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.TemplateRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.TemplateRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking Terms Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.TermsRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.TermsRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking Sales Ref Full name 12-12-13
                            //   Axis-119 Export a Sales Order, Column for Sales Rep is missing data
                            if (node.Name.Equals(QBXmlSalesOrderList.SalesRepRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.SalesRepRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking currency Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.CurrencyRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.CurrencyRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlSalesOrderList.ShipDate.ToString()))
                            {
                                try
                                {
                                    DateTime dtship = Convert.ToDateTime(node.InnerText);
                                    salesOrderList.ShipDate = dtship.ToString("yyyy-MM-dd");
                                }
                                catch
                                {
                                    salesOrderList.ShipDate = node.InnerText;
                                }
                                salesOrderList.ShipDate = node.InnerText;
                            }
                            if (node.Name.Equals(QBXmlSalesOrderList.DueDate.ToString()))
                                salesOrderList.DueDate = node.InnerText;

                            //Assinging TxnID value to Sales Order
                            if (node.Name.Equals(QBXmlSalesOrderList.TxnID.ToString()))
                                salesOrderList.TxnID = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.ExchangeRate.ToString()))
                                salesOrderList.ExchangeRate = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.TotalAmountInHomeCurrency.ToString()))
                                salesOrderList.TotalAmountInHomeCurrency = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    salesOrderList.TxnDate = dttxn.ToString("yyyy-MM-dd");
                                }
                                catch
                                {
                                    salesOrderList.TxnDate = node.InnerText;
                                }
                            }

                            //Checking values of ShipAddress values.
                            if (node.Name.Equals(QBXmlSalesOrderList.BillAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr1.ToString()))
                                        salesOrderList.Addr1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr2.ToString()))
                                        salesOrderList.Addr2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr3.ToString()))
                                        salesOrderList.Addr3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.City.ToString()))
                                        salesOrderList.City = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.State.ToString()))
                                        salesOrderList.State = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.PostalCode.ToString()))
                                        salesOrderList.PostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Country.ToString()))
                                        salesOrderList.Country = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Note.ToString()))
                                        salesOrderList.Note = childNode.InnerText;
                                }
                            }

                            //Checking values of ShipAddress values.
                            if (node.Name.Equals(QBXmlSalesOrderList.ShipAddress.ToString()))
                            {
                                //Getting values of address.
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr1.ToString()))
                                        salesOrderList.ShipToAdd1 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr2.ToString()))
                                        salesOrderList.ShipToAdd2 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Addr3.ToString()))
                                        salesOrderList.ShipToAdd3 = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.City.ToString()))
                                        salesOrderList.ShipCity = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.State.ToString()))
                                        salesOrderList.ShipState = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.PostalCode.ToString()))
                                        salesOrderList.ShipPostalCode = childNode.InnerText;
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Country.ToString()))
                                        salesOrderList.ShipCountry = childNode.InnerText;
                                }
                            }

                            //Checking Ship Method Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.ShipMethodRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.ShipMethodFullName = childNode.InnerText;
                                }
                            }
                            //Checking CustomerMsg Ref list.
                            if (node.Name.Equals(QBXmlSalesOrderList.CustomerMsgRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.CustomerMsgRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking CustomerSalesTaxCodeRef list.
                            if (node.Name.Equals(QBXmlSalesOrderList.CustomerSalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                        salesOrderList.CustomerSalesTaxCodeRefFullName = childNode.InnerText;
                                }
                            }
                            //Checking PONumber values.
                            if (node.Name.Equals(QBXmlSalesOrderList.PONumber.ToString()))
                                salesOrderList.PONumber = node.InnerText;

                            //Checking LinkedTxnID
                            if (node.Name.Equals(QBXmlSalesOrderList.LinkedTxn.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnID.ToString()))
                                        salesOrderList.LinkedTxn = childNode.InnerText;
                                }
                            }
                            //Checking FOB
                            if (node.Name.Equals(QBXmlSalesOrderList.FOB.ToString()))
                                salesOrderList.FOB = node.InnerText;


                            //Checking Memo values.
                            if (node.Name.Equals(QBXmlSalesOrderList.Memo.ToString()))
                                salesOrderList.Memo = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.Subtotal.ToString()))
                                salesOrderList.SubTotal = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.SalesTaxPercentage.ToString()))
                                salesOrderList.SalesTaxPercentage = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.SalesTaxTotal.ToString()))
                                salesOrderList.SalesTaxTotal = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.TotalAmount.ToString()))
                                salesOrderList.TotalAmount = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.IsManuallyClosed.ToString()))
                                salesOrderList.IsManuallyClosed = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.IsFullyInvoiced.ToString()))
                                salesOrderList.IsFullyInvoiced = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.IsToBePrinted.ToString()))
                                salesOrderList.IsToBePrinted = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.IsToBeEmailed.ToString()))
                                salesOrderList.IsToBeEmailed = node.InnerText;

                            if (node.Name.Equals(QBXmlSalesOrderList.Other.ToString()))
                                salesOrderList.Other = node.InnerText;

                            #endregion


                            //Checking Sales order Line ret values.
                            #region SalesOrderLineRet
                            if (node.Name.Equals(QBXmlSalesOrderList.SalesOrderLineRet.ToString()))
                            {
                                checkNullEntries(node, ref salesOrderList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking Txn line Id value.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            salesOrderList.TxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            salesOrderList.TxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking Item ref value.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                            {
                                                if (childNodes.InnerText.Length > 159)
                                                    salesOrderList.ItemFullName.Add(childNodes.InnerText.Remove(159, (childNodes.InnerText.Length - 159)).Trim());
                                                else
                                                    salesOrderList.ItemFullName.Add(childNodes.InnerText);
                                            }
                                        }
                                    }

                                    //Checking Desc.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Desc.ToString()))
                                    {
                                        salesOrderList.Desc.Add(childNode.InnerText);
                                    }

                                    //Checking Quantity values.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            salesOrderList.Quantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking UOM
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.UnitOfMeasure.ToString()))
                                    {
                                        salesOrderList.UOM.Add(childNode.InnerText);
                                    }

                                    //Checking OverrideUOM
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.OverrideUOMSetRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                salesOrderList.OverrideUOMFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Checking Rate values.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Rate.ToString()))
                                    {
                                        try
                                        {
                                            salesOrderList.Rate.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }
                                    //Checking Amount Values.
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Amount.ToString()))
                                    {
                                        try
                                        {
                                            salesOrderList.Amount.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking InventorySiteRef
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.InventorySiteRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                salesOrderList.InventorySiteRefFullName.Add(childNodes.InnerText);
                                        }
                                    }



                                    // axis 10.0 changes
                                    // serial numbert
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.SerialNumber.ToString()))
                                    {
                                        salesOrderList.SerialNumber.Add(childNode.InnerText);
                                    }
                                    // lot number
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.LotNumber.ToString()))
                                    {
                                        salesOrderList.LotNumber.Add(childNode.InnerText);
                                    }
                                    // axis 10.0 changes ends

                                    //Checking Servicedate
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.ServiceDate.ToString()))
                                    {
                                        try
                                        {
                                            DateTime dtServiceDate = Convert.ToDateTime(childNode.InnerText);
                                            if (countryName == "United States")
                                            {
                                                salesOrderList.ServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                            }
                                            else
                                            {
                                                salesOrderList.ServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                            }
                                        }
                                        catch
                                        {
                                            salesOrderList.ServiceDate.Add(childNode.InnerText);
                                        }
                                    }

                                    //Checking SalesTaxCodeRef
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.SalesTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                salesOrderList.SalesTaxCodeFullName.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Other1
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Other1.ToString()))
                                    {
                                        salesOrderList.Other1.Add(childNode.InnerText);
                                    }

                                    //Checking Other2
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Other2.ToString()))
                                    {
                                        salesOrderList.Other2.Add(childNode.InnerText);
                                    }

                                    //Checking Invoiced
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Invoiced.ToString()))
                                    {
                                        salesOrderList.Invoiced.Add(childNode.InnerText);
                                    }

                                    //Checking LineIsManuallyClosed
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.IsManuallyClosed.ToString()))
                                    {
                                        salesOrderList.LineIsManuallyClosed.Add(childNode.InnerText);
                                    }

                                    //Checking LineClass
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.ClassRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                salesOrderList.LineClass.Add(childNodes.InnerText);
                                        }
                                    }
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            salesOrderList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + salesOrderList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                salesOrderList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + salesOrderList.TxnLineID[count]);
                                        }
                                        i++;
                                    }
                                }
                                count++;
                            }

                            #endregion

                            #region
                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBXmlSalesOrderList.SalesOrderLineGroupRet.ToString()))
                            {
                                int count2 = 0;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            salesOrderList.GroupTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            salesOrderList.GroupTxnLineID.Add(childNode.InnerText);
                                    }

                                    //Checking ItemGroupRef
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.ItemGroupRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                salesOrderList.ItemGroupFullName.Add(childNodes.InnerText);
                                        }
                                    }

                                    //Chekcing Desc
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Desc.ToString()))
                                    {
                                        salesOrderList.GroupDesc.Add(childNode.InnerText);
                                    }

                                    //Chekcing Quantity
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.Quantity.ToString()))
                                    {
                                        try
                                        {
                                            salesOrderList.GroupQuantity.Add(Convert.ToDecimal(childNode.InnerText));
                                        }
                                        catch
                                        { }
                                    }

                                    //Checking IsPrintItemsIngroup
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.IsPrintItemsInGroup.ToString()))
                                    {
                                        salesOrderList.IsPrintItemsInGroup.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBXmlSalesOrderList.DataExtRet.ToString()))
                                    {

                                        if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtName").InnerText))
                                        {
                                            salesOrderList.DataExtName.Add(childNode.SelectSingleNode("./DataExtName").InnerText + "|" + salesOrderList.TxnLineID[count]);
                                            if (!string.IsNullOrEmpty(childNode.SelectSingleNode("./DataExtValue").InnerText))
                                                salesOrderList.DataExtValue.Add(childNode.SelectSingleNode("./DataExtValue").InnerText + "|" + salesOrderList.TxnLineID[count]);
                                        }
                                        i++;
                                    }
                                    //Checking  sub SalesOrderLineRet
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.SalesOrderLineRet.ToString()))
                                    {
                                        checkGroupLineNullEntries(childNode, ref salesOrderList);
                                        foreach (XmlNode subChildNode in childNode.ChildNodes)
                                        {
                                            //Checking Txn line Id value.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                            {
                                                if (subChildNode.InnerText.Length > 36)
                                                    salesOrderList.GroupLineTxnLineID.Add(subChildNode.InnerText.Remove(36, (subChildNode.InnerText.Length - 36)).Trim());
                                                else
                                                    salesOrderList.GroupLineTxnLineID.Add(subChildNode.InnerText);
                                            }

                                            //Checking Item ref value.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.ItemRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                        salesOrderList.GroupLineItemFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Desc.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Desc.ToString()))
                                            {
                                                salesOrderList.GroupLineDesc.Add(subChildNode.InnerText);
                                            }
                                            //Checking Quantity values.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Quantity.ToString()))
                                            {
                                                try
                                                {
                                                    salesOrderList.GroupLineQuantity.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking UOM
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.UnitOfMeasure.ToString()))
                                            {
                                                salesOrderList.GroupLineUOM.Add(subChildNode.InnerText);
                                            }

                                            //Checking OverrideUOM
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.OverrideUOMSetRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                        salesOrderList.GroupLineOverrideUOM.Add(subChildNodes.InnerText);
                                                }
                                            }


                                            //Checking Rate values.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Rate.ToString()))
                                            {
                                                try
                                                {
                                                    salesOrderList.GroupLineRate.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }
                                            //Checking Amount Values.
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Amount.ToString()))
                                            {
                                                try
                                                {
                                                    salesOrderList.GroupLineAmount.Add(Convert.ToDecimal(subChildNode.InnerText));
                                                }
                                                catch
                                                { }
                                            }

                                            //Checking Servicedate
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.ServiceDate.ToString()))
                                            {
                                                try
                                                {
                                                    DateTime dtServiceDate = Convert.ToDateTime(subChildNode.InnerText);
                                                    if (countryName == "United States")
                                                    {
                                                        salesOrderList.GroupLineServiceDate.Add(dtServiceDate.ToString("MM/dd/yyyy"));
                                                    }
                                                    else
                                                    {
                                                        salesOrderList.GroupLineServiceDate.Add(dtServiceDate.ToString("dd/MM/yyyy"));
                                                    }
                                                }
                                                catch
                                                {
                                                    salesOrderList.GroupLineServiceDate.Add(subChildNode.InnerText);
                                                }
                                            }

                                            //Checking SalesTaxCodeRef
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.SalesTaxCodeRef.ToString()))
                                            {
                                                foreach (XmlNode subChildNodes in subChildNode.ChildNodes)
                                                {
                                                    if (subChildNodes.Name.Equals(QBXmlSalesOrderList.FullName.ToString()))
                                                        salesOrderList.GroupLineSalesTaxCodeFullName.Add(subChildNodes.InnerText);
                                                }
                                            }
                                            //Checking Other1
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Other1.ToString()))
                                            {
                                                salesOrderList.Other1.Add(subChildNode.InnerText);
                                            }

                                            //Checking Other2
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.Other2.ToString()))
                                            {
                                                salesOrderList.Other2.Add(subChildNode.InnerText);
                                            }
                                            if (subChildNode.Name.Equals(QBXmlSalesOrderList.DataExtRet.ToString()))
                                            {

                                                if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtName").InnerText))
                                                {
                                                    salesOrderList.DataExtName.Add(subChildNode.SelectSingleNode("./DataExtName").InnerText);
                                                    if (!string.IsNullOrEmpty(subChildNode.SelectSingleNode("./DataExtValue").InnerText))
                                                        salesOrderList.DataExtValue.Add(subChildNode.SelectSingleNode("./DataExtValue").InnerText);
                                                }
                                                i++;
                                            }

                                        }
                                        count2++;
                                    }
                                }

                                count1++;
                            }

                            if (node.Name.Equals(QBXmlSalesOrderList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    salesOrderList.DataExtName.Add(node.SelectSingleNode("./DataExtName").InnerText);
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        salesOrderList.DataExtValue.Add(node.SelectSingleNode("./DataExtValue").InnerText);
                                }

                                i++;
                            }

                        }
                        #endregion

                        //Adding Invoice list.
                        this.Add(salesOrderList);
                    }
                    else { }
                }


                //Removing all the references from OutPut Xml document.
                outputXMLDocOfSalesOrder.RemoveAll();
                #endregion
            }
        }

        private void checkGroupLineNullEntries(XmlNode childNode, ref SalesOrderList salesOrderList)
        {
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.TxnLineID.ToString()) == null)
            {
                salesOrderList.GroupLineTxnLineID.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.ItemRef.ToString()) == null)
            {
                salesOrderList.GroupLineItemFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Desc.ToString()) == null)
            {
                salesOrderList.GroupLineDesc.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Quantity.ToString()) == null)
            {
                salesOrderList.GroupLineQuantity.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.UnitOfMeasure.ToString()) == null)
            {
                salesOrderList.GroupLineUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.OverrideUOMSetRef.ToString()) == null)
            {
                salesOrderList.GroupLineOverrideUOM.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Rate.ToString()) == null)
            {
                salesOrderList.GroupLineRate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Amount.ToString()) == null)
            {
                salesOrderList.GroupLineAmount.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.ServiceDate.ToString()) == null)
            {
                salesOrderList.GroupLineServiceDate.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.SalesTaxCodeRef.ToString()) == null)
            {
                salesOrderList.GroupLineSalesTaxCodeFullName.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Other1.ToString()) == null)
            {
                salesOrderList.Other1.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.Other2.ToString()) == null)
            {
                salesOrderList.Other2.Add(null);
            }
            if (childNode.SelectSingleNode("./" + QBXmlSalesOrderList.DataExtRet.ToString()) == null)
            {
                salesOrderList.DataExtName.Add(null);
                salesOrderList.DataExtValue.Add(null);
            }
        }

        private void checkNullEntries(XmlNode node, ref SalesOrderList salesOrderList)
        {
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.TxnLineID.ToString()) == null)
            {
                salesOrderList.TxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.ItemRef.ToString()) == null)
            {
                salesOrderList.ItemFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Desc.ToString()) == null)
            {
                salesOrderList.Desc.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Quantity.ToString()) == null)
            {
                salesOrderList.Quantity.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.UnitOfMeasure.ToString()) == null)
            {
                salesOrderList.UOM.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.OverrideUOMSetRef.ToString()) == null)
            {
                salesOrderList.OverrideUOMFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Rate.ToString()) == null)
            {
                salesOrderList.Rate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Amount.ToString()) == null)
            {
                salesOrderList.Amount.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.InventorySiteRef.ToString()) == null)
            {
                salesOrderList.InventorySiteRefFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.SerialNumber.ToString()) == null)
            {
                salesOrderList.SerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.LotNumber.ToString()) == null)
            {
                salesOrderList.LotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.ServiceDate.ToString()) == null)
            {
                salesOrderList.ServiceDate.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.SalesTaxCodeRef.ToString()) == null)
            {
                salesOrderList.SalesTaxCodeFullName.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Other1.ToString()) == null)
            {
                salesOrderList.Other1.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Other2.ToString()) == null)
            {
                salesOrderList.Other2.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Invoiced.ToString()) == null)
            {
                salesOrderList.Invoiced.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.IsManuallyClosed.ToString()) == null)
            {
                salesOrderList.LineIsManuallyClosed.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.ClassRef.ToString()) == null)
            {
                salesOrderList.LineClass.Add(null);
            }
            if (node.SelectSingleNode("./" + QBXmlSalesOrderList.DataExtRet.ToString()) == null)
            {
                salesOrderList.DataExtName.Add(null);
                salesOrderList.DataExtValue.Add(null);
            }
        }

        /// <summary>
        /// This method is used for getting sales order list by passing Company file name
        /// and From and to date.
        /// </summary>
        /// <param name="QBFileName">Passing QuickBooks company file name.</param>
        /// <param name="FromDate">From date of Sales order.</param>
        /// <param name="ToDate">To date of sales order.</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderListEntityFilter(string QBFileName, List<string> entityFilter)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;

            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("SalesOrderQueryRq", QBFileName, entityFilter);
            GettSalesOrderValuesFromXML(salesOrderXmlList);
            #endregion




            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            int count = 0;
            int count1 = 0;



            /// <summary>
            /// This method is used for populate sales order list by passing date
            /// and ref number.
            /// </summary>
            /// <param name="QBFileName">QuickBooks company file.</param>
            /// <param name="FromDate">From modified date.</param>
            /// <param name="ToDate">To modified date.</param>
            /// <param name="FromRefNum">From ref number</param>
            /// <param name="ToRefNum">To Ref number</param>
            /// <returns></returns>


            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            string countryName = string.Empty;

            SalesOrderList salesOrderList;
            #endregion


            GettSalesOrderValuesFromXML(salesOrderXmlList);

            return this;
        }
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting Sales order List from QuickBooks.
            int count = 0;
            int count1 = 0;
            string salesOrderXmlList = string.Empty;
            string countryName = string.Empty;
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate);
            SalesOrderList salesOrderList;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            return this;
        }
        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, string ToDate, List<string> entityFilter)
        {
            #region Getting Sales order List from QuickBooks.
            int count = 0;
            string salesOrderXmlList = string.Empty;
            // salesOrderXmlList = QBCommonUtilities.GetListFromQuickBookTxnDate("SalesOrderQueryRq", QBFileName, FromDate, ToDate,entityFilter);
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            SalesOrderList salesOrderList;
            int count1 = 0;
            int i = 0;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }

        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting Sales order List from QuickBooks.
            int count = 0;
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            SalesOrderList salesOrderList;
            int count1 = 0;
            int i = 0;

            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            return this;
        }
        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Sales order List from QuickBooks.
            int count = 0;
            int i = 0;
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate);
            SalesOrderList salesOrderList;
            int count1 = 0;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            return this;
        }
        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {

            #region Getting Sales order List from QuickBooks.
            int count = 0;
            int i = 0;
            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate);

            SalesOrderList salesOrderList;
            int count1 = 0;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }





        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, string FromRefnum, string ToRefNum, List<string> entityFilter)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromRefnum, ToRefNum, entityFilter);
            string countryName = string.Empty;
            SalesOrderList salesOrderList;
            int count = 0;
            int count1 = 0;
            #endregion

            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for populate sales order list 
        /// by passing ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromRefnum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, string FromRefnum, string ToRefNum)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromRefnum, ToRefNum);
            string countryName = string.Empty;
            SalesOrderList salesOrderList;
            int count = 0;
            int count1 = 0;
            int i = 0;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <param name="ToRefNum">Entity filter</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum, entityFilter);
            string countryName = string.Empty;
            int count = 0;
            int count1 = 0;
            SalesOrderList salesOrderList;
            #endregion

            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName)
        {
            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;

            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName);
            string countryName = string.Empty;
            int count = 0;
            int count1 = 0;
            int i = 0;
            SalesOrderList salesOrderList;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> PopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;
            int count = 0;
            int count1 = 0;
            SalesOrderList salesOrderList;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }


        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <param name="ToRefNum">Entity Filter</param>
        /// <returns></returns>
        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum, List<string> entityFilter)
        {

            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum, entityFilter);
            string countryName = string.Empty;
            int count = 0;
            int count1 = 0;
            int i = 0;
            SalesOrderList salesOrderList;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);

            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used for populate sales order list by passing date
        /// and ref number.
        /// </summary>
        /// <param name="QBFileName">QuickBooks company file.</param>
        /// <param name="FromDate">From modified date.</param>
        /// <param name="ToDate">To modified date.</param>
        /// <param name="FromRefNum">From ref number</param>
        /// <param name="ToRefNum">To Ref number</param>
        /// <returns></returns>
        public Collection<SalesOrderList> ModifiedPopulateSalesOrderList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefNum, string ToRefNum)
        {
            #region Getting Sales order List from QuickBooks.

            //Getting item list from QuickBooks.
            string salesOrderXmlList = string.Empty;
            //salesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            salesOrderXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromDate, ToDate, FromRefNum, ToRefNum);
            string countryName = string.Empty;
            int count = 0;
            int count1 = 0;
            int i = 0;
            SalesOrderList salesOrderList;
            #endregion
            GettSalesOrderValuesFromXML(salesOrderXmlList);
            //Returning object.
            return this;
        }
        
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        //Axis 706
        public List<string> PopulateSalesOrderTxnIdAndGroupTxnIdInSequence(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting SalesOrder List from QuickBooks.

            //Getting item list from QuickBooks.
            List<string> SalesOrderTxnIdAndGroupTxnId = new List<string>();
            string SalesOrderXmlList = string.Empty;
            SalesOrderXmlList = QBCommonUtilities.GetListFromQuickBook("SalesOrderQueryRq", QBFileName, FromRefnum, ToRefnum);

            SalesOrderList SalesOrderList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLDocOfSalesOrder = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            if (SalesOrderXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLDocOfSalesOrder.LoadXml(SalesOrderXmlList);
                XmlFileData = SalesOrderXmlList;

                #region Getting SalesOrder Values from XML

                //Getting Invoices values from QuickBooks response.
                XmlNodeList SalesOrderNodeList = outputXMLDocOfSalesOrder.GetElementsByTagName(QBXmlSalesOrderList.SalesOrderRet.ToString());

                foreach (XmlNode SalesOrderNodes in SalesOrderNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count = 0;
                        int count1 = 0;
                        int i = 0;
                        SalesOrderList = new SalesOrderList();
                        foreach (XmlNode node in SalesOrderNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBXmlSalesOrderList.SalesOrderLineRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                    {
                                        SalesOrderList.TxnID = childNode.InnerText;
                                        SalesOrderTxnIdAndGroupTxnId.Add(SalesOrderList.TxnID);
                                    }
                                }
                            }



                            //Checking InvoiceGroupLineRet
                            if (node.Name.Equals(QBXmlSalesOrderList.SalesOrderLineGroupRet.ToString()))
                            {
                                //P Axis 13.2 : issue 695
                                if (node.SelectSingleNode("./" + QBXmlSalesOrderList.ItemGroupRef.ToString()) == null)
                                {
                                    SalesOrderList.ItemGroupFullName.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Desc.ToString()) == null)
                                {
                                    SalesOrderList.GroupDesc.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlSalesOrderList.Quantity.ToString()) == null)
                                {
                                    SalesOrderList.GroupQuantity.Add(null);
                                }
                                if (node.SelectSingleNode("./" + QBXmlSalesOrderList.UnitOfMeasure.ToString()) == null)
                                {
                                    SalesOrderList.UOM.Add("");
                                }
                                if (node.SelectSingleNode("./" + QBXmlSalesOrderList.IsPrintItemsInGroup.ToString()) == null)
                                {
                                    SalesOrderList.IsPrintItemsInGroup.Add("");
                                }

                                itemList = new QBItemDetails();
                                int count2 = 0;
                                bool dataextFlag = false;

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBXmlSalesOrderList.TxnLineID.ToString()))
                                    {
                                        string GroupId = "";
                                        if (childNode.InnerText.Length > 36)
                                            GroupId = (childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            GroupId = childNode.InnerText;

                                        SalesOrderTxnIdAndGroupTxnId.Add("GroupTxnLineID_" + GroupId);
                                    }
                                }
                            }
                            //
                        }
                    }
                }
                //Adding Invoice list.
                #endregion
            }
            return SalesOrderTxnIdAndGroupTxnId;
        }
        //Axis 706 ends
    }
}

