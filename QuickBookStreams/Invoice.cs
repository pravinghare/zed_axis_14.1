using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;

namespace Streams
{
    public class QBInvoice
    {
        #region Private Members

        List<QBInvoiceOS1> m_QBInvoiceOS1;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or Sets the OS1 stream
        /// </summary>
        public List<QBInvoiceOS1> QBInvoiceOS1
        {
            get { return m_QBInvoiceOS1; }
            set { m_QBInvoiceOS1 = value; }
        }

        #endregion

        #region Constructors

        public QBInvoice()
        {
            m_QBInvoiceOS1 = new List<QBInvoiceOS1>();
        }

        #endregion

        #region Private Methods

        private bool AddOS1(string[] stream)
        {
            if (stream.Length == 18)
            {
                QBInvoiceOS1 qbInvoiceOS1 = new QBInvoiceOS1();
                qbInvoiceOS1.ActionIndicator = Convert.ToChar(stream[1]);
                qbInvoiceOS1.LinkedTxn_RefNumber = stream[2];
                qbInvoiceOS1.CustomerRef_ListID = stream[3];
                qbInvoiceOS1.ShipDate = DateTime.ParseExact(stream[4], Constants.ISMFileDateTime, CultureInfo.InvariantCulture);
                qbInvoiceOS1.TxnDate = DateTime.ParseExact(stream[5], Constants.ISMFileDateTime, CultureInfo.InvariantCulture);
                qbInvoiceOS1.ShipAddress_Addr1 = stream[6];
                qbInvoiceOS1.ShipAddress_Addr2 = stream[7];
                qbInvoiceOS1.ShipAddress_City = stream[8];
                qbInvoiceOS1.ShipAddress_State = stream[9];
                qbInvoiceOS1.ShipAddress_PostalCode = stream[10];
                qbInvoiceOS1.ShipAddress_Country = stream[11];
                qbInvoiceOS1.ShipAddress_Note = stream[12];
                qbInvoiceOS1.ShipMethodRef_FullName = stream[13];
                qbInvoiceOS1.PONumber = stream[14];
                qbInvoiceOS1.RefNumber = stream[15];
                qbInvoiceOS1.ForwardingCharge = Convert.ToDecimal(stream[16]);
                qbInvoiceOS1.FreightCharge = Convert.ToDecimal(stream[17]);
                m_QBInvoiceOS1.Add(qbInvoiceOS1);
                return true;
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }
            
        }

        private bool AddOS2(string[] stream)
        {
            if (stream.Length == 3)
            {
                QBInvoiceOS2 qbInvoiceOS2 = new QBInvoiceOS2();
                qbInvoiceOS2.ItemRef_FullName = stream[1];
                qbInvoiceOS2.Quantity = Convert.ToInt32(stream[2]);
                m_QBInvoiceOS1[m_QBInvoiceOS1.Count - 1].QBInvoiceOS2.Add(qbInvoiceOS2);
                return true;
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }
        }

        #endregion

        #region Public Methods

        public void Parse(string attachmentContent)
        {
            int startIndex = attachmentContent.IndexOf(Constants.CRLF) + 2;
            int endIndex = attachmentContent.IndexOf(Constants.CRLF,startIndex);

            while (endIndex <= (attachmentContent.Length))
            {
               string[] stream = attachmentContent.Substring(startIndex,endIndex-startIndex).Split("|".ToCharArray());
               if(stream[0].Equals("OS1"))
               {
                   if (!AddOS1(stream))
                   {
                       throw new Exception("Error in parsing OS1 stream in the attachment");
                   }
               }
               else if (stream[0].Equals("OS2"))
               {
                   if (!AddOS2(stream))
                   {
                       throw new Exception("Error in parsing the OS2 stream in the attachment");
                   }
               }
               else
               {
                   throw new Exception("Attachment is not in correct format");
               }
               startIndex = endIndex + 2;
               if (startIndex < attachmentContent.Length)
               {
                   endIndex = attachmentContent.IndexOf(Constants.CRLF, startIndex);
                   endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
               }
               else
               {
                   break;
               }
            }
       }

              
        #endregion
    }
}
