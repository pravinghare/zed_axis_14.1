﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("Addresses", Namespace = "", IsNullable = false)]
    public class Addresses
    {
        #region Private Memeber Variables

        private string m_Addtype;
        private string m_Addr1;
        private string m_Addr2;
        private string m_Addr3;
        private string m_Addr4;
        private string m_City;
        private string m_region;
        private string m_PostalCode;
        private string m_Country;
        private string m_attentionTo;

        #endregion
       
        #region Constructors

        public Addresses()
        {

        }
        public Addresses(string addtype, string add1, string add2, string add3, string add4, string city, string region, string postalCode, string country, string attentionTo)
        {
            this.m_Addtype = addtype;
            this.m_Addr1 = add1;
            this.m_Addr2 = add2;
            this.m_Addr3 = add3;
            this.m_Addr4 = add4;
            this.m_City = city;
            this.m_region = region;
            this.m_Country = country;
            this.m_PostalCode = postalCode;
            this.m_attentionTo = attentionTo;
        }

        #endregion

        #region Public Properties

        public string AddressType
        {
            get { return this.m_Addtype; }
            set { this.m_Addtype = value; }
        }

        public string Addr1
        {
            get { return this.m_Addr1; }
            set { this.m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return this.m_Addr2; }
            set { this.m_Addr2 = value; }
        }

        public string Addr3
        {
            get { return this.m_Addr3; }
            set { this.m_Addr3 = value; }
        }

        public string Addr4
        {
            get { return this.m_Addr4; }
            set { this.m_Addr4 = value; }
        }

        public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }

        public string Region
        {
            get { return this.m_region; }
            set { this.m_region = value; }
        }

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        public string Country
        {
            get { return this.m_Country; }
            set { this.m_Country = value; }
        }

        public string AttentionTo
        {
            get { return this.m_attentionTo; }
            set { this.m_attentionTo = value; }
        }


        #endregion
    }

}
