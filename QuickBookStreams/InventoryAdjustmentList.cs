﻿

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections;

namespace Streams
{
    /// <summary>
    /// This class is used to declare  public methods and Properties.
    /// </summary>
    public class InventoryAdjustmentList : BaseInventoryAdjustmentList
    {
        string integrated_stringh = string.Empty;


        #region Public Methods

        public string TxnID
        {
            get { return m_TxnId; }
            set { m_TxnId = value; }
        }

        public string AccountFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public string InventorySiteRefFullName
        {
            get { return m_InventorySiteRefFullName; }
            set { m_InventorySiteRefFullName = value; }
        }       

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }
        //protected string m_TxnId;
        //protected string m_AccountRefFullName;
        //protected string m_InventorySiteRefFullName;
        //protected string m_TxnDate;
        //protected string m_RefNumber;
        //protected decimal? m_CustomerRefFullName;
        //protected string m_ClassRefFullName;
        //protected string m_Memo;
        //protected decimal? m_ExternalGUID;

        //protected List<string> m_InvenAdjLineTxnLineID = new string[10000];
        //protected List<string> m_InvenAdjLineItemRef = new string[10000];
        //protected List<string> m_InvenAdjLineSerialNumber = new string[10000];
        //protected List<string> m_InvenAdjLineLotNumber = new string[10000];
        //protected List<string> m_InvenAdjLineInventorySiteLocationRef = new string[10000];
        //protected List<string> m_InvenAdjLineQuantityDifference = new string[10000];
        //protected List<string> m_InvenAdjLineValueDifference = new string[10000];
        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string CustomerRefFullName
        {
            get { return m_CustomerRefFullName; }
            set { m_CustomerRefFullName = value; }
        }       

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ClassRefFullName
        {
            get { return m_ClassRefFullName; }
            set { m_ClassRefFullName = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        public List<string> InvenAdjLineTxnLineID
        {
            get { return m_InvenAdjLineTxnLineID; }
            set { m_InvenAdjLineTxnLineID = value; }
        }

        public List<string> InvenAdjLineItemRef
        {
            get { return m_InvenAdjLineItemRef; }
            set { m_InvenAdjLineItemRef = value; }
        }

       
        public List<string> InvenAdjLineSerialNumber
        {
            get { return m_InvenAdjLineSerialNumber; }
            set { m_InvenAdjLineSerialNumber = value; }
        }

        public List<string> InvenAdjLineLotNumber
        {
            get { return m_InvenAdjLineLotNumber; }
            set { m_InvenAdjLineLotNumber = value; }
        }

        public List<string> InvenAdjLineInvSiteLocRef
        {
            get { return m_InvenAdjLineInvSiteLocRef; }
            set { m_InvenAdjLineInvSiteLocRef = value; }
        }

        public List<string> InvenAdjLineQuantityDifference
        {
            get { return m_InvenAdjLineQuantityDifference; }
            set { m_InvenAdjLineQuantityDifference = value; }
        }

        public List<string> InvenAdjLineValueDifference
        {
            get { return m_InvenAdjLineValueDifference; }
            set { m_InvenAdjLineValueDifference = value; }
        }


        #endregion
    }

    /// <summary>
    /// This class is used to get CreditCardCharge from QuickBook.
    /// </summary>
    public class QBInventoryAdjustmentListCollection : Collection<InventoryAdjustmentList>
    {
        string XmlFileData = string.Empty;
        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentAllDataList(string QBFileName)
        {

            #region Getting InventoryAdjustmentList from QuickBooks.

            //Getting item list from QuickBooks.
            string InventoryAdjustmentXmlList = string.Empty;
            InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName);
            #endregion
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            //Returning object.
        }


        #region private method

        private Collection<InventoryAdjustmentList> GetInventoryAdjustmentList(string InventoryAdjustmentXmlList)
        {
            #region
            InventoryAdjustmentList InventoryAdjustmentList;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            //Create a xml document for output result.
            XmlDocument outputXMLInventoryAdjustment = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (InventoryAdjustmentXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLInventoryAdjustment.LoadXml(InventoryAdjustmentXmlList);
                XmlFileData = InventoryAdjustmentXmlList;


                #region Getting CreditCardCahrge Values from XML

                //Getting CreditCardChargeList values from QuickBooks response.
                XmlNodeList InventoryAdjustmentNodeList = outputXMLInventoryAdjustment.GetElementsByTagName(QBInventoryAdjustmentList.InventoryAdjustmentRet.ToString());

                foreach (XmlNode InventoryAdjustmentNodes in InventoryAdjustmentNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count1 = 0;
                        InventoryAdjustmentList = new InventoryAdjustmentList();
                       
                        foreach (XmlNode node in InventoryAdjustmentNodes.ChildNodes)
                        {

                            //Checking TxnID. 
                            if (node.Name.Equals(QBInventoryAdjustmentList.TxnID.ToString()))
                            {
                                InventoryAdjustmentList.TxnID = node.InnerText;
                            }

                            //Checking Account Ref list.
                            if (node.Name.Equals(QBInventoryAdjustmentList.AccountRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                        InventoryAdjustmentList.AccountFullName = childNode.InnerText;
                                }
                            }

                            //Checking PayeeEntityRef List.
                            if (node.Name.Equals(QBInventoryAdjustmentList.InventorySiteRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                        InventoryAdjustmentList.InventorySiteRefFullName = childNode.InnerText;
                                }
                            }

                            //Checking TxnDate.
                            if (node.Name.Equals(QBInventoryAdjustmentList.TxnDate.ToString()))
                            {
                                try
                                {
                                    DateTime dttxn = Convert.ToDateTime(node.InnerText);
                                    if (countryName == "United States")
                                    {
                                        InventoryAdjustmentList.TxnDate = dttxn.ToString("MM/dd/yyyy");
                                    }
                                    else
                                    {
                                        InventoryAdjustmentList.TxnDate = dttxn.ToString("dd/MM/yyyy");
                                    }
                                }
                                catch
                                {
                                    InventoryAdjustmentList.TxnDate = node.InnerText;
                                }
                            }


                            //Chekcing CurrencyRefFullName
                            if (node.Name.Equals(QBInventoryAdjustmentList.CustomerRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                        InventoryAdjustmentList.CustomerRefFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ClassRefFullName
                            if (node.Name.Equals(QBInventoryAdjustmentList.ClassRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                        InventoryAdjustmentList.ClassRefFullName = childNode.InnerText;
                                }
                            }

                            //Chekcing ExchangeRate
                            if (node.Name.Equals(QBInventoryAdjustmentList.ExternalGUID.ToString()))
                            {
                                InventoryAdjustmentList.ExternalGUID = node.InnerText;
                            }



                            //Checking RefNumber
                            if (node.Name.Equals(QBInventoryAdjustmentList.RefNumber.ToString()))
                            {
                                InventoryAdjustmentList.RefNumber = node.InnerText;
                            }

                            //Checking Memo values.
                            if (node.Name.Equals(QBInventoryAdjustmentList.Memo.ToString()))
                            {
                                InventoryAdjustmentList.Memo = node.InnerText;
                            }


                            //Chekcing ItemLineRet
                            if (node.Name.Equals(QBInventoryAdjustmentList.InventoryAdjustmentLineRet.ToString()))
                            {
                                checkNullEntries(node, ref InventoryAdjustmentList);

                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Checking TxnLineID
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.TxnLineID.ToString()))
                                    {
                                        if (childNode.InnerText.Length > 36)
                                            InventoryAdjustmentList.InvenAdjLineTxnLineID.Add(childNode.InnerText.Remove(36, (childNode.InnerText.Length - 36)).Trim());
                                        else
                                            InventoryAdjustmentList.InvenAdjLineTxnLineID.Add(childNode.InnerText);
                                    }
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                                InventoryAdjustmentList.InvenAdjLineItemRef.Add(childNodes.InnerText);
                                        }
                                    }
                                    // Axis 10.0 changes
                                    //Chekcing Serial Number
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.SerialNumber.ToString()))
                                    {
                                        InventoryAdjustmentList.InvenAdjLineSerialNumber.Add(childNode.InnerText);
                                    }
                                    // Checking Lot Number
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.LotNumber.ToString()))
                                    {
                                        InventoryAdjustmentList.InvenAdjLineLotNumber.Add(childNode.InnerText);
                                    }

                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.InventorySiteLocationRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBInventoryAdjustmentList.FullName.ToString()))
                                                InventoryAdjustmentList.InvenAdjLineInvSiteLocRef.Add(childNodes.InnerText);
                                        }
                                    }
                                    //Checking Quantiy
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.QuantityDifference.ToString()))
                                    {
                                        InventoryAdjustmentList.InvenAdjLineQuantityDifference.Add(childNode.InnerText);
                                    }
                                    //Checking UOM
                                    if (childNode.Name.Equals(QBInventoryAdjustmentList.ValueDifference.ToString()))
                                    {
                                        InventoryAdjustmentList.InvenAdjLineValueDifference.Add(childNode.InnerText);
                                    }
                                }
                                count1++;
                            }

                        }
                        //Adding CreditCardCharge list.
                        this.Add(InventoryAdjustmentList);
                    }
                    else
                    { return null; }
                }

                //Removing all the references from OutPut Xml document.
                outputXMLInventoryAdjustment.RemoveAll();
                #endregion
            }

            //Returning object.
            return this;
            #endregion

        }

        private void checkNullEntries(XmlNode node, ref InventoryAdjustmentList inventoryAdjustmentList)
        {
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.TxnLineID.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.ItemRef.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineItemRef.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.TxnLineID.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineTxnLineID.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.SerialNumber.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineSerialNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.LotNumber.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineLotNumber.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.InventorySiteLocationRef.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineInvSiteLocRef.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.QuantityDifference.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineQuantityDifference.Add(null);
            }
            if (node.SelectSingleNode("./" + QBInventoryAdjustmentList.ValueDifference.ToString()) == null)
            {
                inventoryAdjustmentList.InvenAdjLineValueDifference.Add(null);
            }
        }

        #endregion


        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName)
        {

            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion

        }

        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum, entityFilter);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }


        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromDate, ToDate, FromRefnum, ToRefnum);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }


        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromDate, ToDate, entityFilter);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }

        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, DateTime FromDate, DateTime ToDate)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromDate, ToDate);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }

        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, string FromRefnum, string ToRefnum, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromRefnum, ToRefnum, entityFilter);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }
        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentList(string QBFileName, string FromRefnum, string ToRefnum)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromRefnum, ToRefnum);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }
        public Collection<InventoryAdjustmentList> PopulateInventoryAdjustmentEntityFilter(string QBFileName, List<string> entityFilter)
        {
            #region Getting CreditCardChargeList List from QuickBooks.

            //Getting item list from QuickBooks.           

            string InventoryAdjustmentXmlList = QBCommonUtilities.GetListFromQuickBookEntityFilter("InventoryAdjustmentQueryRq", QBFileName, entityFilter);
            return GetInventoryAdjustmentList(InventoryAdjustmentXmlList);

            #endregion
        }


        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// 
        /// <param name="fromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<InventoryAdjustmentList> ModifiedPopulateInventoryAdjustmentList(string QBFileName, DateTime FromDate, string ToDate)
        {
            #region Getting Bill Payments List from QuickBooks.

            //Getting item list from QuickBooks.

            string vendorcreditCardCreditXmlList = QBCommonUtilities.ModifiedGetListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, FromDate, ToDate);

            return GetInventoryAdjustmentList(vendorcreditCardCreditXmlList);
            #endregion
        }

        public List<string> GetTxnLineIdList(string QBFileName, string TxnId)
        {
            List<string> inventoryAdjustmentTxnLineId = new List<string>();
            string inventoryAdjustmentXmlList = QBCommonUtilities.GetAllListFromQuickBook("InventoryAdjustmentQueryRq", QBFileName, TxnId);
            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(inventoryAdjustmentXmlList);
            XmlFileData = inventoryAdjustmentXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            XmlNodeList InventoryAdjustmentNodeList = outputXMLDoc.GetElementsByTagName(QBInventoryAdjustmentList.InventoryAdjustmentRet.ToString());
            
            foreach (XmlNode inventoryAdjustmentNodes in InventoryAdjustmentNodeList)
            {
                #region For inventoryAdjustment data
                if (bkWorker.CancellationPending != true)
                {
                    foreach (XmlNode node in inventoryAdjustmentNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBInventoryAdjustmentList.InventoryAdjustmentLineRet.ToString()))
                        {
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name.Equals(QBInventoryAdjustmentList.TxnLineID.ToString()))
                                {
                                    inventoryAdjustmentTxnLineId.Add(childNode.InnerText);
                                }
                            }
                        }
                    }
                }
                else
                { return null; }

                #endregion
            }
            return inventoryAdjustmentTxnLineId;
        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }


    }
}
