// ===============================================================================
// 
// ItemServiceList.cs
//
// This file contains the implementations of the QuickBooks Item Service request methods and
// Properties.
//
// Developed By : Sandeep Patil.
// Date : 27/02/2009
// Modified By :K.Gouraw
// Date : 27/06/2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This class provides properties and methods of QuickBooks Item List.
    /// </summary>
    public class QBItemServiceList : BaseItemServiceList
    {
        #region Public Properties

        public string ListID
        {
            get { return m_ListId; }
            set { m_ListId = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }

        }


        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }

        }

        /// <summary>
        /// Get and Set BarCodeValue
        /// </summary>
        public string BarCodeValue
        {
            get { return m_BarCodeValue; }
            set { m_BarCodeValue = value; }
        }


        public string ParentRefFullName
        {
            get
            {
                return m_ParentRefFullName;
            }
            set
            {
                m_ParentRefFullName = value;
            }
        }


        public string Sublevel
        {
            get { return m_Sublevel; }
            set { m_Sublevel = value; }
        }


       
        public string UnitOfMeasureSetRefFullName
        {
            get
            { return m_UnitOfMeasureSetRefFullName; }
            set
            {
                if (value.Length > 20)
                    m_UnitOfMeasureSetRefFullName = value.Remove(20, (value.Length - 20));
                else
                    m_UnitOfMeasureSetRefFullName = value;
            }
        }

        public string IsTaxIncluded
        {
            get
            {
                return m_IsTaxIncluded;
            }
            set
            {
                m_IsTaxIncluded = value;
            }
        }

        public string SalesTaxCodeRefFullName
        {
            get
            {
                return m_SalesTaxCodeRefFullName;
            }
            set
            {
                m_SalesTaxCodeRefFullName = value;
            }
        }

        public string Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public string Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }

        public string AccountRefFullName
        {
            get { return m_AccountRefFullName; }
            set { m_AccountRefFullName = value; }
        }

        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }

        public string SalesPricePercentage
        {
            get { return m_SalesPricePercentage; }
            set { m_SalesPricePercentage = value; }
        }


        public string IncomeAccountRefFullName
        {
            get { return m_IncomeAccountRefFullName; }
            set { m_IncomeAccountRefFullName = value; }
        }

        public string PurchaseDesc
        {
            get { return m_PurchaseDesc; }
            set { m_PurchaseDesc = value; }
        }

        public string PurchaseCost
        {
            get { return m_PurchaseCost; }
            set { m_PurchaseCost = value; }

        }

        public string PurchaseTaxCodeRefFullName
        {
            get { return m_PurchaseTaxCodeRefFullName; }
            set { m_PurchaseTaxCodeRefFullName = value; }

        }
        public string ExpenseAccountRefFullName
        {
            get { return m_ExpenseAccountRefFullName; }
            set { m_ExpenseAccountRefFullName = value; }
        }

        public string PrefVendorRefFullName
        {
            get { return m_PrefVendorRefFullName; }
            set { m_PrefVendorRefFullName = value; }
        }    

        public string[] DataExtName = new string[550];
        public string[] DataExtValue = new string[550];
        #endregion
    }

    /// <summary>
    /// Collection class used for getting Item Service List from QuickBooks.
    /// </summary>
    public class QBItemServiceListCollection : Collection<QBItemServiceList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemServiceList> PopulateItemServiceList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting Item Service List from QuickBooks.
            int i = 0;
            //Getting Item Service list from QuickBooks.
            string ItemServiceList = string.Empty;
            
            if (inActiveFilter == false)//bug no. 395
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBookByDate("ItemServiceQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemServiceQueryRq", QBFileName, FromDate, ToDate);
            }

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemServiceList ItemService;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItemService = new XmlDocument();

            if (ItemServiceList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfItemService.LoadXml(ItemServiceList);
                XmlFileData = ItemServiceList;

                #region Getting Item Service Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList ItemServiceNodeList = outputXMLDocOfItemService.GetElementsByTagName(QBXmlItemServiceList.ItemServiceRet.ToString());

                foreach (XmlNode ItemServiceNodes in ItemServiceNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        ItemService = new QBItemServiceList();
                        //Getting node list of Item Service.
                        foreach (XmlNode node in ItemServiceNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemServiceList.Name.ToString()))
                                 ItemService.Name = node.InnerText;

                             if (node.Name.Equals(QBXmlItemServiceList.ListID.ToString()))
                                 ItemService.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeCreated.ToString()))
                                ItemService.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeModified.ToString()))
                                ItemService.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                ItemService.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.BarCodeValue.ToString()))
                                ItemService.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.ParentRefFullName = childNode.InnerText;
                                }

                            }



                            if (node.Name.Equals(QBXmlItemServiceList.Sublevel.ToString()))
                                ItemService.Sublevel = node.InnerText;



                          
                            if (node.Name.Equals(QBXmlItemServiceList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.Desc.ToString()))
                                        ItemService.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.Price.ToString()))
                                        ItemService.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }


                            if (node.Name.Equals(QBXmlItemServiceList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesDesc.ToString()))
                                        ItemService.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesPrice.ToString()))
                                        ItemService.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseDesc.ToString()))
                                        ItemService.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseCost.ToString()))
                                        ItemService.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PurchaseTaxCodeRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    ItemService.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        ItemService.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }

                        }
                        this.Add(ItemService);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfItemService.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// If no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBItemServiceList> PopulateItemServiceList(string QBFileName, bool inActiveFilter)
        {
            #region Getting Item Service from QuickBooks.
            int i = 0;
            //Getting Item Service from QuickBooks.
            string ItemServiceList = string.Empty;
            
            if (inActiveFilter == false)//bug no. 395
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBook("ItemServiceQueryRq", QBFileName);
            }
            else
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBookForInactive("ItemServiceQueryRq", QBFileName);
            }
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemServiceList ItemService;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItemService = new XmlDocument();

            if (ItemServiceList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfItemService.LoadXml(ItemServiceList);
                XmlFileData = ItemServiceList;

                #region Getting Item Service Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList ItemServiceNodeList = outputXMLDocOfItemService.GetElementsByTagName(QBXmlItemServiceList.ItemServiceRet.ToString());

                foreach (XmlNode ItemServiceNodes in ItemServiceNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        ItemService = new QBItemServiceList();
                        //Getting node list of Item Service.
                        foreach (XmlNode node in ItemServiceNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemServiceList.Name.ToString()))
                                ItemService.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.ListID.ToString()))
                                ItemService.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeCreated.ToString()))
                                ItemService.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeModified.ToString()))
                                ItemService.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                ItemService.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.BarCodeValue.ToString()))
                                ItemService.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.ParentRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.Sublevel.ToString()))
                                ItemService.Sublevel = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.Desc.ToString()))
                                        ItemService.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.Price.ToString()))
                                        ItemService.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }


                            if (node.Name.Equals(QBXmlItemServiceList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesDesc.ToString()))
                                        ItemService.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesPrice.ToString()))
                                        ItemService.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseDesc.ToString()))
                                        ItemService.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseCost.ToString()))
                                        ItemService.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PurchaseTaxCodeRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                           
                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    ItemService.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        ItemService.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }

                        }
                        this.Add(ItemService);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfItemService.RemoveAll();
            }
            //Returning object.
            return this;

        }
      
        /// <summary>
        /// This method is used for adding various NonInventoryItem list into QuickBooks NonInventoryItem class.
        /// </summary>
        /// <param name="QBFileName">Passing Quickbooks company file name.</param>
        /// <param name="FromDate">Passing From date.</param>
        /// <param name="ToDate">Passing To Date.</param>
        /// <returns></returns>
        public Collection<QBItemServiceList> PopulateItemServiceList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting NonInventoryItem List from QuickBooks.
            int i = 0;
            //Getting ItemService list from QuickBooks.
            string ItemServiceList = string.Empty;            
            if (inActiveFilter == false)//bug no. 395
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBookByDate("ItemServiceQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                ItemServiceList = QBCommonUtilities.GetListFromQuickBookByDateForInActiveFilter("ItemServiceQueryRq", QBFileName, FromDate, ToDate);
            }
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            QBItemServiceList ItemService;
            #endregion
            //Create a xml document for output result.
            XmlDocument outputXMLDocOfItemService = new XmlDocument();

            if (ItemServiceList != string.Empty)
            {
                //Loading NonInventoryItem List into XML.
                outputXMLDocOfItemService.LoadXml(ItemServiceList);
                XmlFileData = ItemServiceList;

                #region Getting Item Service Values from XML

                //Getting NonInventoryItem Inventory list from XML.
                XmlNodeList ItemServiceNodeList = outputXMLDocOfItemService.GetElementsByTagName(QBXmlItemServiceList.ItemServiceRet.ToString());

                foreach (XmlNode ItemServiceNodes in ItemServiceNodeList)
                {
                    i = 0;
                    if (bkWorker.CancellationPending != true)
                    {
                        ItemService = new QBItemServiceList();
                        //Getting node list of Item Service.
                        foreach (XmlNode node in ItemServiceNodes.ChildNodes)
                        {
                            if (node.Name.Equals(QBXmlItemServiceList.Name.ToString()))
                                ItemService.Name = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.ListID.ToString()))
                                ItemService.ListID = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeCreated.ToString()))
                                ItemService.TimeCreated = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.TimeModified.ToString()))
                                ItemService.TimeModified = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                ItemService.FullName = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.BarCodeValue.ToString()))
                                ItemService.BarCodeValue = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.ParentRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.ParentRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.Sublevel.ToString()))
                                ItemService.Sublevel = node.InnerText;

                            if (node.Name.Equals(QBXmlItemServiceList.UnitOfMeasureSetRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.UnitOfMeasureSetRefFullName = childNode.InnerText;
                                }
                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesTaxCodeRef.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                        ItemService.SalesTaxCodeRefFullName = childNode.InnerText;
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesOrPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.Desc.ToString()))
                                        ItemService.Desc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.Price.ToString()))
                                        ItemService.Price = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.AccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.AccountRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }

                            }

                            if (node.Name.Equals(QBXmlItemServiceList.SalesAndPurchase.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesDesc.ToString()))
                                        ItemService.SalesDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.SalesPrice.ToString()))
                                        ItemService.SalesPrice = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.IncomeAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.IncomeAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseDesc.ToString()))
                                        ItemService.PurchaseDesc = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseCost.ToString()))
                                        ItemService.PurchaseCost = childNode.InnerText;

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PurchaseTaxCodeRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PurchaseTaxCodeRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.ExpenseAccountRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.ExpenseAccountRefFullName = childNodes.InnerText;
                                        }
                                    }

                                    if (childNode.Name.Equals(QBXmlItemServiceList.PrefVendorRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBXmlItemServiceList.FullName.ToString()))
                                                ItemService.PrefVendorRefFullName = childNodes.InnerText;
                                        }
                                    }
                                }
                            }


                            //Axis 8.0 for Custom Field
                            if (node.Name.Equals(QBXmlItemList.DataExtRet.ToString()))
                            {

                                if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtName").InnerText))
                                {
                                    ItemService.DataExtName[i] = node.SelectSingleNode("./DataExtName").InnerText;
                                    if (!string.IsNullOrEmpty(node.SelectSingleNode("./DataExtValue").InnerText))
                                        ItemService.DataExtValue[i] = node.SelectSingleNode("./DataExtValue").InnerText;
                                }

                                i++;
                            }
                        }
                        this.Add(ItemService);
                    }
                    else
                    {
                        return null;
                    }
                }
                #endregion

                //Removing all the references from OutPut Xml document.
                outputXMLDocOfItemService.RemoveAll();
            }
            //Returning object.
            return this;

        }

        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }
    }
}
