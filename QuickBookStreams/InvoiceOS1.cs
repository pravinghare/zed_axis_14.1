// ===============================================================================
// 
// Invoice.cs
//
// This file contains the implementations of the QBInvoice Class Members. 
// Developed By : Swapnil.
// Date : 12/03/2009
// Modified By : Swapnil.
// Date : 
// ==============================================================================
using System;
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class QBInvoiceOS1 : BaseInvoiceOS1
    {
        #region Public Properties

        /// <summary>
        /// Action indicator A:add U:update
        /// </summary>
        public char ActionIndicator
        {
            get { return m_actionIndicator; }
            set
            {
                if (value.Equals('A') || value.Equals('U'))
                {
                    m_actionIndicator = value;
                }
                else
                {
                    throw new ArgumentException("Invalid action indicator value");
                }
            }
        }
        
        /// <summary>
        /// Sales order number max length 30
        /// </summary>
        public string LinkedTxn_RefNumber
        {
            get { return m_customerOrderNumber; }
            set
            {
                if (value.Length <= 30)
                {
                    m_customerOrderNumber = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid Sales order number length");
                }
            }
        }

        /// <summary>
        /// ListID of the customer
        /// </summary>
        public string CustomerRef_ListID
        {
            get { return m_customerClientListId; }
            set { m_customerClientListId = value; }
        }

        /// <summary>
        /// Date order to be delivered
        /// </summary>
        public DateTime ShipDate
        {
            get { return m_etd; }
            set
            {
                if (value != null)
                {
                    m_etd = value;
                }
                else
                {
                    throw new ArgumentNullException("Ship date is null");
                }
            }
        }

        /// <summary>
        /// Date order placed
        /// </summary>
        public DateTime TxnDate
        {
            get { return m_orderDate; }
            set
            {
                if (value != null)
                {
                    m_orderDate = value;
                }
                else
                {
                    throw new ArgumentNullException("Order date is null");
                }
            }
        }

        /// <summary>
        /// ShipAddress Addr1
        /// </summary>
        public string ShipAddress_Addr1
        {
            get { return m_shipToName; }
            set { m_shipToName = value; }
        }

        /// <summary>
        /// ShipAddress:Addr2 max length 40
        /// </summary>
        public string ShipAddress_Addr2
        {
            get { return m_shipToAddress11; }
            set
            {
                if (value.Length <= 40)
                {
                    m_shipToAddress11 = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:Addr2 length");
                }
            }
        }

        /// <summary>
        /// ShipAddress:City max length 40
        /// </summary>
        public string ShipAddress_City
        {
            get { return m_shipToAddress12; }
            set
            {
                if (value.Length <= 40)
                {
                    m_shipToAddress12 = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:City length");
                }
            }
        }

        /// <summary>
        /// ShipAddress:State max length 40
        /// </summary>
        public string ShipAddress_State
        {
            get { return m_shipToAddress13; }
            set
            {
                if (value.Length <= 40)
                {
                    m_shipToAddress13 = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:State length");
                }
            }
        }

        /// <summary>
        /// ShipAddress:PosttalCode max length 40
        /// </summary>
        public string ShipAddress_PostalCode
        {
            get { return m_shipToAddress14; }
            set
            {
                if (value.Length <= 40)
                {
                    m_shipToAddress14 = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:PostalCode length");
                }
            }
        }

        /// <summary>
        /// ShipAddress:Country max length 40
        /// </summary>
        public string ShipAddress_Country
        {
            get { return m_shipToAddress15; }
            set
            {
                if (value.Length <= 40)
                {
                    m_shipToAddress15 = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:Country length");
                }
            }
        }

        /// <summary>
        /// Note description max length 40
        /// </summary>
        public string ShipAddress_Note
        {
            get { return m_deliveryDescription; }
            set
            {
                if (value.Length <= 40)
                {
                    m_deliveryDescription = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipAddress:Note length");
                }
            }
        }

        /// <summary>
        /// Mode of transport max length 30
        /// </summary>
        public string ShipMethodRef_FullName
        {
            get { return m_modeOfTransport; }
            set
            {
                if (value.Length <= 30)
                {
                    m_modeOfTransport = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid ShipMethodRef:FullName length");
                }
            }
        }

        /// <summary>
        /// Customer PO Number max length 30
        /// </summary>
        public string PONumber
        {
            get { return m_asn; }
            set
            {
                if (value.Length <= 30)
                {
                    m_asn = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid PONumber length");
                }
            }
        }

        /// <summary>
        /// Invoice Number max length 30
        /// </summary>
        public string RefNumber
        {
            get { return m_invoiceNumber; }
            set
            {
                if (value.Length <= 30)
                {
                    m_asn = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid PONumber length");
                }
            }
        }

        /// <summary>
        /// Forwarding charges
        /// </summary>
        public decimal ForwardingCharge
        {
            get { return m_forwardingCharges; }
            set { m_forwardingCharges = value; }
        }

        /// <summary>
        /// Freight charges
        /// </summary>
        public decimal FreightCharge
        {
            get { return m_freightCharges; }
            set { m_freightCharges = value; }
        }

        public List<BaseInvoiceOS2> QBInvoiceOS2
        {
            get { return m_BaseInvoiceOS2; }
            set { m_BaseInvoiceOS2 = value; }
        }

        #endregion

        #region Constructor

        internal QBInvoiceOS1()
        {
            m_BaseInvoiceOS2 = new List<BaseInvoiceOS2>();
        }
        #endregion

    }
}
