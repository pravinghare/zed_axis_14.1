﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;

namespace Streams
{
    /// <summary>
    /// This class is used to declare  public methods and Properties.
    /// </summary>
    public class PriceLevelList : BasePriceLevelList
    {
        #region Public Methods

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string TimeCreated
        {
            get { return m_TimeCreated; }
            set { m_TimeCreated = value; }
        }

        public string TimeModified
        {
            get { return m_TimeModified; }
            set { m_TimeModified = value; }
        }

        public string EditSequence
        {
            get { return m_EditSequence; }
            set { m_EditSequence = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string PriceLevelType
        {
            get { return m_PriceLevelType; }
            set { m_PriceLevelType = value; }
        }

        public string PriceLevelFixedPercentage
        {
            get { return m_PriceLevelFixedPercentage; }
            set { m_PriceLevelFixedPercentage = value; }
        }

        public string[] LineID
        {
            get { return m_LineID; }
            set { m_LineID = value; }
        }

        public string[] ItemRef
        {
            get {return m_ItemRefFullName ;}
            set {m_ItemRefFullName = value ;}
        }

        public decimal?[] CustomPrice
        {
            get {return m_CustomPrice ;}
            set {m_CustomPrice = value;}
        }

        public string[] CustomPricePercent
        {
            get {return m_CustomPricePer ;}
            set{m_CustomPricePer = value;}
        }

        public string CurrencyRef
        {
            get{return m_CurrencyRefFullName ;}
            set{m_CurrencyRefFullName = value;}
        }
 
        #endregion
    }

    /// <summary>
    /// This class is used to get Price Level from QuickBook.
    /// </summary>
    public class PriceLevelListCollection : Collection<PriceLevelList>
    {
        string XmlFileData = string.Empty;



        /// <summary>
        /// Only Since Last Export
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<PriceLevelList> PopulatePriceLevelList(string QBFileName, DateTime FromDate, string ToDate, bool inActiveFilter)
        {
            #region Getting PopulatePriceLevelList List from QuickBooks.

            //Getting item list from QuickBooks.
            string PriceLevelXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBookModifiedDate("PriceLevelQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBookModifiedDateForInActiveFilter("PriceLevelQueryRq", QBFileName, FromDate, ToDate);
            }
            PriceLevelList PriceLevelList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLPriceLevel = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (PriceLevelXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLPriceLevel.LoadXml(PriceLevelXmlList);
                XmlFileData = PriceLevelXmlList;

                #region Getting PriceLevel Values from XML

                //Getting PriceLevelList values from QuickBooks response.
                XmlNodeList PriceLevelNodeList = outputXMLPriceLevel.GetElementsByTagName(QBPriceLevelList.PriceLevelRet.ToString());

                foreach (XmlNode PriceLevelNodes in PriceLevelNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count1 = 0;

                        PriceLevelList = new PriceLevelList();
                        PriceLevelList.CustomPrice = new decimal?[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.CustomPricePercent = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.ItemRef = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.LineID = new string[PriceLevelNodes.ChildNodes.Count];

                        foreach (XmlNode node in PriceLevelNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPriceLevelList.ListID.ToString()))
                            {
                                PriceLevelList.ListID = node.InnerText;
                            }

                            //Checking Time Created.
                            if (node.Name.Equals(QBPriceLevelList.TimeCreated.ToString()))
                            {
                                PriceLevelList.TimeCreated = node.InnerText;
                            }

                            //Checking Time Modified.
                            if (node.Name.Equals(QBPriceLevelList.TimeModified.ToString()))
                            {
                                PriceLevelList.TimeModified = node.InnerText;
                            }

                            //Checking EditSequence
                            if (node.Name.Equals(QBPriceLevelList.EditSequence.ToString()))
                            {
                                PriceLevelList.EditSequence = node.InnerText;
                            }

                            //Checking Name
                            if (node.Name.Equals(QBPriceLevelList.Name.ToString()))
                            {
                                PriceLevelList.Name = node.InnerText;
                            }

                            //Chekcing IsActive
                            if (node.Name.Equals(QBPriceLevelList.IsActive.ToString()))
                            {
                                PriceLevelList.IsActive = node.InnerText;
                            }

                            //Chekcing PriceLevelType
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelType.ToString()))
                            {
                                PriceLevelList.PriceLevelType = node.InnerText;
                            }

                            //Chekcing PriceLevelFixedPercentage
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelFixedPercentage.ToString()))
                            {
                                PriceLevelList.PriceLevelFixedPercentage = node.InnerText;
                            }

                            //Checking CurrencyRef
                            if (node.Name.Equals(QBPriceLevelList.CurrencyRef.ToString()))
                            {
                                PriceLevelList.CurrencyRef = node.InnerText;
                            }

                            //Checking PriceLevelPerItemRet
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelPerItemRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBPriceLevelList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBPriceLevelList.FullName.ToString()))
                                                PriceLevelList.ItemRef[count1] = childNodes.InnerText;
                                        }
                                    }
                                    //Checking CustomPrice
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPrice.ToString()))
                                    {
                                        PriceLevelList.CustomPrice[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //Checking CustomPricePercent
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPricePercent.ToString()))
                                    {
                                        PriceLevelList.CustomPricePercent[count1] = childNode.InnerText;
                                    }

                                }
                                count1++;
                            }
                        }
                        this.Add(PriceLevelList);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLPriceLevel.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// if no filter was selected
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<PriceLevelList> PopulatePriceLevelList(string QBFileName, bool inActiveFilter)
        {
            #region Getting PopulatePriceLevelList List from QuickBooks.

            //Getting item list from QuickBooks.
            string PriceLevelXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBook("PriceLevelQueryRq", QBFileName);
            }
            else
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBookForInactive("PriceLevelQueryRq", QBFileName);
            }
            

            PriceLevelList PriceLevelList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLPriceLevel = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (PriceLevelXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLPriceLevel.LoadXml(PriceLevelXmlList);
                XmlFileData = PriceLevelXmlList;

                #region Getting PriceLevel Values from XML

                //Getting PriceLevelList values from QuickBooks response.
                XmlNodeList PriceLevelNodeList = outputXMLPriceLevel.GetElementsByTagName(QBPriceLevelList.PriceLevelRet.ToString());

                foreach (XmlNode PriceLevelNodes in PriceLevelNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count1 = 0;

                        PriceLevelList = new PriceLevelList();
                        PriceLevelList.CustomPrice = new decimal?[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.CustomPricePercent = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.ItemRef = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.LineID = new string[PriceLevelNodes.ChildNodes.Count];

                        foreach (XmlNode node in PriceLevelNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPriceLevelList.ListID.ToString()))
                            {
                                PriceLevelList.ListID = node.InnerText;
                            }

                            //Checking Time Created.
                            if (node.Name.Equals(QBPriceLevelList.TimeCreated.ToString()))
                            {
                                PriceLevelList.TimeCreated = node.InnerText;
                            }

                            //Checking Time Modified.
                            if (node.Name.Equals(QBPriceLevelList.TimeModified.ToString()))
                            {
                                PriceLevelList.TimeModified = node.InnerText;
                            }

                            //Checking EditSequence
                            if (node.Name.Equals(QBPriceLevelList.EditSequence.ToString()))
                            {
                                PriceLevelList.EditSequence = node.InnerText;
                            }

                            //Checking Name
                            if (node.Name.Equals(QBPriceLevelList.Name.ToString()))
                            {
                                PriceLevelList.Name = node.InnerText;
                            }

                            //Chekcing IsActive
                            if (node.Name.Equals(QBPriceLevelList.IsActive.ToString()))
                            {
                                PriceLevelList.IsActive = node.InnerText;
                            }

                            //Chekcing PriceLevelType
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelType.ToString()))
                            {
                                PriceLevelList.PriceLevelType = node.InnerText;
                            }

                            //Chekcing PriceLevelFixedPercentage
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelFixedPercentage.ToString()))
                            {
                                PriceLevelList.PriceLevelFixedPercentage = node.InnerText;
                            }

                            //Checking CurrencyRef
                            if (node.Name.Equals(QBPriceLevelList.CurrencyRef.ToString()))
                            {
                                PriceLevelList.CurrencyRef = node.InnerText;
                            }

                            //Checking PriceLevelPerItemRet
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelPerItemRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBPriceLevelList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBPriceLevelList.FullName.ToString()))
                                                PriceLevelList.ItemRef[count1] = childNodes.InnerText;
                                        }
                                    }
                                    //Checking CustomPrice
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPrice.ToString()))
                                    {
                                        PriceLevelList.CustomPrice[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //Checking CustomPricePercent
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPricePercent.ToString()))
                                    {
                                        PriceLevelList.CustomPricePercent[count1] = childNode.InnerText;
                                    }

                                }
                                count1++;
                            }
                        }
                        this.Add(PriceLevelList);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLPriceLevel.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        /// <summary>
        /// This method is used to get PriceLevelList
        /// for Filters TxnDateRangeFilters.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public Collection<PriceLevelList> PopulatePriceLevelList(string QBFileName, DateTime FromDate, DateTime ToDate, bool inActiveFilter)
        {
            #region Getting PopulatePriceLevelList List from QuickBooks.

            //Getting item list from QuickBooks.
            string PriceLevelXmlList = string.Empty;
            if (inActiveFilter == false)//bug no. 395
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBookModifiedDate("PriceLevelQueryRq", QBFileName, FromDate, ToDate);
            }
            else
            {
                PriceLevelXmlList = QBCommonUtilities.GetListFromQuickBookModifiedDateForInActiveFilter("PriceLevelQueryRq", QBFileName, FromDate, ToDate);
            }
            PriceLevelList PriceLevelList;
            #endregion

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
            //Create a xml document for output result.
            XmlDocument outputXMLPriceLevel = new XmlDocument();

            //Getting current version of System. BUG(676)
            string countryName = string.Empty;
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;


            if (PriceLevelXmlList != string.Empty)
            {
                //Loading Item List into XML.
                outputXMLPriceLevel.LoadXml(PriceLevelXmlList);
                XmlFileData = PriceLevelXmlList;

                #region Getting PriceLevel Values from XML

                //Getting PriceLevelList values from QuickBooks response.
                XmlNodeList PriceLevelNodeList = outputXMLPriceLevel.GetElementsByTagName(QBPriceLevelList.PriceLevelRet.ToString());

                foreach (XmlNode PriceLevelNodes in PriceLevelNodeList)
                {
                    if (bkWorker.CancellationPending != true)
                    {
                        int count1 = 0;

                        PriceLevelList = new PriceLevelList();
                        PriceLevelList.CustomPrice = new decimal?[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.CustomPricePercent = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.ItemRef = new string[PriceLevelNodes.ChildNodes.Count];
                        PriceLevelList.LineID = new string[PriceLevelNodes.ChildNodes.Count];

                        foreach (XmlNode node in PriceLevelNodes.ChildNodes)
                        {
                            //Checking ListID. 
                            if (node.Name.Equals(QBPriceLevelList.ListID.ToString()))
                            {
                                PriceLevelList.ListID = node.InnerText;
                            }

                        //Checking Time Created.
                        if (node.Name.Equals(QBPriceLevelList.TimeCreated.ToString()))
                        {
                            PriceLevelList.TimeCreated = node.InnerText;
                        }

                        //Checking Time Modified.
                        if (node.Name.Equals(QBPriceLevelList.TimeModified.ToString()))
                        {
                            PriceLevelList.TimeModified = node.InnerText;
                        }

                        //Checking EditSequence
                        if (node.Name.Equals(QBPriceLevelList.EditSequence.ToString()))
                        {
                            PriceLevelList.EditSequence = node.InnerText;
                        }

                        //Checking Name
                        if (node.Name.Equals(QBPriceLevelList.Name.ToString()))
                        {
                            PriceLevelList.Name = node.InnerText;
                        }

                        //Chekcing IsActive
                        if (node.Name.Equals(QBPriceLevelList.IsActive.ToString()))
                        {
                            PriceLevelList.IsActive = node.InnerText;
                        }

                        //Chekcing PriceLevelType
                        if (node.Name.Equals(QBPriceLevelList.PriceLevelType.ToString()))
                        {
                            PriceLevelList.PriceLevelType = node.InnerText;
                        }

                        //Chekcing PriceLevelFixedPercentage
                        if (node.Name.Equals(QBPriceLevelList.PriceLevelFixedPercentage.ToString()))
                        {
                            PriceLevelList.PriceLevelFixedPercentage = node.InnerText;
                        }

                        //Checking CurrencyRef
                        if (node.Name.Equals(QBPriceLevelList.CurrencyRef.ToString()))
                        {
                            PriceLevelList.CurrencyRef = node.InnerText;
                        }

                            //Checking PriceLevelPerItemRet
                            if (node.Name.Equals(QBPriceLevelList.PriceLevelPerItemRet.ToString()))
                            {
                                foreach (XmlNode childNode in node.ChildNodes)
                                {
                                    //Chekcing ItemRefFullName
                                    if (childNode.Name.Equals(QBPriceLevelList.ItemRef.ToString()))
                                    {
                                        foreach (XmlNode childNodes in childNode.ChildNodes)
                                        {
                                            if (childNodes.Name.Equals(QBPriceLevelList.FullName.ToString()))
                                                PriceLevelList.ItemRef[count1] = childNodes.InnerText;
                                        }
                                    }
                                    //Checking CustomPrice
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPrice.ToString()))
                                    {
                                        PriceLevelList.CustomPrice[count1] = Convert.ToDecimal(childNode.InnerText);
                                    }
                                    //Checking CustomPricePercent
                                    if (childNode.Name.Equals(QBPriceLevelList.CustomPricePercent.ToString()))
                                    {
                                        PriceLevelList.CustomPricePercent[count1] = childNode.InnerText;
                                    }

                                }
                                count1++;
                            }
                        }
                        this.Add(PriceLevelList);
                    }
                    else
                    { return null; }
                }
                //Removing the references.
                outputXMLPriceLevel.RemoveAll();
                #endregion
            }
            //Returning object.
            return this;
        }

        
        /// <summary>
        /// This method is used to get the xml response from the QuikBooks.
        /// </summary>
        /// <returns></returns>
        public string GetXmlFileData()
        {
            return XmlFileData;
        }

        
    }
}
