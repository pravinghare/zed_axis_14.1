using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using DataProcessingBlocks;
using EDI.Constant;
using System.Xml;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Globalization;
using Tamir.SharpSsh.jsch;
using System.Data.Odbc;

using MySql.Data.MySqlClient;



namespace TradingPartnerDetails
{
    public partial class TradingPartnerAddressBook : Form
    {
       
        private string ProductCaption = EDI.Constant.Constants.ProductName;

        private static TradingPartnerAddressBook m_TPAddressBook;

        private static long m_TradingPartnerID = -1;

        private MailSettings.TradingPartner m_TradingPartner = null;

        private string m_SessionID = string.Empty;

        private bool IsEditable = false;

        Session session;
        JSch jsch = new JSch();
        int rPort = -1;
        string host = null;

        public static TradingPartnerAddressBook GetInstance()
        {
            if (m_TPAddressBook == null)
                m_TPAddressBook = new TradingPartnerAddressBook();
            return m_TPAddressBook;
        }

        public TradingPartnerAddressBook()
        {
            InitializeComponent();
       
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrEmpty(textBoxTPName.Text.Trim()))
            {
                MessageBox.Show("Please enter trading partner name.",ProductCaption,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                textBoxTPName.Focus();
                return;
            }
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(-1))
            {
                MessageBox.Show("Please select a mail protocol.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTPMailProtocol.Focus();
                return;
            }
          
            //For POP3 mail account.
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(0))
            {
                #region For POP3 Mail Accounts

                IsEditable = false;
                if (!IsEditable)
                {
                    //Checking Duplicates Trading Partner.
                    object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                    if (countTP == null)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToInt32(countTP) > 0)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxTPEmailAddress.Text.Trim()))
                {
                    MessageBox.Show("Please enter email address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxTPEmailAddress.Focus();
                    return;
                }
                if (!DataProcessingBlocks.CommonUtilities.IsEmail(textBoxTPEmailAddress.Text.Trim()))
                {
                    MessageBox.Show("Please enter valid email as abc@xyz.com.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxTPEmailAddress.Focus();
                    return;
                }
                if (!DataProcessingBlocks.CommonUtilities.IsEmail(textBoxTPAlternateEmail.Text.Trim()))
                {
                    MessageBox.Show("Please enter valid email as abc@xyz.com.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxTPAlternateEmail.Focus();
                    return;
                }
                if (textBoxTPEmailAddress.Text.Trim().Equals(textBoxTPAlternateEmail.Text.Trim()))
                {
                    MessageBox.Show("Both email address should not be same, Please enter different email address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxTPAlternateEmail.Text = string.Empty;
                    textBoxTPAlternateEmail.Focus();
                    return;
                }
                if (MailSettings.TradingPartner.IsExistEmail(textBoxTPEmailAddress.Text.Trim(), textBoxTPAlternateEmail.Text.Trim(), m_TradingPartnerID))
                {
                    MessageBox.Show("This email address is already exist. Please enter different email address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, textBoxTPName.Text.Trim().Replace("'", string.Empty), comboBoxTPMailProtocol.SelectedValue.ToString(), textBoxTPEmailAddress.Text.Trim(), textBoxTPAlternateEmail.Text.Trim(), textBoxTPClientID.Text.Trim(), textBoxTPSchemaLoc.Text.Trim());
                if (!tradingPartner.Equal(m_TradingPartner))
                {
                    if (tradingPartner.SaveTradingPartnerDetail())
                    {
                        RefreshControl();
                        MessageBox.Show("Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (m_TradingPartnerID != -1)
                        {
                            panelTPABSave.Visible = false;
                            panelTPABShow.Visible = true;
                            PopulateDataGridView();
                        }
                        IsEditable = false;
                    }
                    else
                    {
                        MessageBox.Show("Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    RefreshControl();
                    MessageBox.Show("Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (m_TradingPartnerID != -1)
                    {
                        panelTPABSave.Visible = false;
                        panelTPABShow.Visible = true;
                        PopulateDataGridView();
                    }
                }

                #endregion

            }
            string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
            string from = dateTimePickerLastUpdated.Value.ToShortDateString() + " " + strTime;
            string to = DateTime.Now.ToShortDateString() + " " + strTime;
            DateTime fromDate = Convert.ToDateTime(from);
            DateTime toDate = Convert.ToDateTime(to);
            //For Saving eBay Transactions.
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(3))
            {
                #region For eBay Transactions

                if (!IsEditable)
                {
                    //Checking Duplicates Trading Partner.
                    object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                    if (countTP == null)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToInt32(countTP) > 0)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                //if (string.IsNullOrEmpty(textBoxeBayUsername.Text.Trim()))
                //{
                //    MessageBox.Show("Please enter eBay Username.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    textBoxeBayUsername.Focus();
                //    return;
                //}
                //if (string.IsNullOrEmpty(textBoxeBayPassword.Text.Trim()))
                //{
                //    MessageBox.Show("Please enter eBay Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    textBoxeBayPassword.Focus();
                //    return;
                //}
                if (string.IsNullOrEmpty(textBoxeBayAuthToken.Text.Trim()))
                {
                    MessageBox.Show("Please click on Authorisation button for eBay Auth Token.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    buttonAuthorization.Focus();
                    return;
                }
                // axis 11 pos
                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks" || TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks POS")
                {
                    if (comboBoxItemName.SelectedIndex.Equals(-1))
                    {
                        MessageBox.Show("Please select item for EBay.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        comboBoxItemName.Focus();
                        return;
                    }
                    if (comboBoxDiscountItems.SelectedIndex.Equals(-1))
                    {
                        MessageBox.Show("Please select item of type Discount/Charge for EBay.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        comboBoxDiscountItems.Focus();
                        return;
                    }

                }
                
                //string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
                //string from = dateTimePickerLastUpdated.Value.ToShortDateString() + " " + strTime;
                //string to = DateTime.Now.ToShortDateString() + " " + strTime;
                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);
                TimeSpan diffSpan = new TimeSpan();
                diffSpan = toDate.Subtract(fromDate);

                if (diffSpan.Days >= 30)
                {
                    MessageBox.Show("Last updated date should not exceed 30 days to current date.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dateTimePickerLastUpdated.Focus();
                    return;
                }
                if (diffSpan.Days < 0)
                {
                    MessageBox.Show("Last updated date should not be greater than current date.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    dateTimePickerLastUpdated.Focus();
                    return;
                }
                MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                //Axis 11 pos
                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks" ||TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks POS")
                {
                    string discountItem = string.Empty;
                    string shippingItem = string.Empty;
                    if (comboBoxDiscountItems.SelectedItem != null)
                    {
                        discountItem = comboBoxDiscountItems.SelectedItem.ToString();
                    }
                    if (comboBoxItemName.SelectedItem != null)
                    {
                        shippingItem = comboBoxItemName.SelectedItem.ToString();
                    }
                    //Save the eBay Trading Partner details.
                    //if (tradingPartner.SaveeBayTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxeBayUsername.Text.Trim(), textBoxeBayPassword.Text.Trim(), textBoxeBayAuthToken.Text, string.Empty, shippingItem, discountItem, fromDate.ToString("yyyy-MM-dd HH:mm:ss"),comboBoxeBayType.SelectedItem.ToString()))
                    if (tradingPartner.SaveeBayTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), string.Empty, string.Empty, textBoxeBayAuthToken.Text, string.Empty, shippingItem, discountItem, fromDate.ToString("yyyy-MM-dd HH:mm:ss"), comboBoxeBayType.SelectedItem.ToString()))
                    {
                        //Refresh The controls.
                        RefreshControl();
                        MessageBox.Show("eBay Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (m_TradingPartnerID != -1)
                        {
                            panelTPABSave.Visible = false;
                            panelTPABShow.Visible = true;
                            PopulateDataGridView();
                        }
                        IsEditable = false;
                    }
                    else
                    {
                        MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {
                    string discountItem = string.Empty;

                    if (comboBoxDiscountItems.SelectedItem != null)
                    {
                        discountItem = comboBoxDiscountItems.SelectedItem.ToString();
                    }
                    //Save the eBay Trading Partner details.
                    //if (tradingPartner.SaveeBayTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxeBayUsername.Text.Trim(), textBoxeBayPassword.Text.Trim(), textBoxeBayAuthToken.Text, discountItem, string.Empty, string.Empty, fromDate.ToString("yyyy-MM-dd HH:mm:ss"),comboBoxeBayType.SelectedItem.ToString()))
                    if (tradingPartner.SaveeBayTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), string.Empty, string.Empty, textBoxeBayAuthToken.Text, discountItem, string.Empty, string.Empty, fromDate.ToString("yyyy-MM-dd HH:mm:ss"), comboBoxeBayType.SelectedItem.ToString()))
                    {
                        //Refresh The controls.
                        RefreshControl();
                        MessageBox.Show("eBay Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (m_TradingPartnerID != -1)
                        {
                            panelTPABSave.Visible = false;
                            panelTPABShow.Visible = true;
                            PopulateDataGridView();
                        }
                        IsEditable = false;

                    }
                    else
                    {
                        MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                #endregion
            }
            //For Saving OSCommerce Transactions.
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(4))
            {
                string WithoutSSHPassword = textBoxOSCPassword.Text.Trim();

                //If WithoutSSHPassword contains ";".
                if (WithoutSSHPassword.Contains(";"))
                    WithoutSSHPassword = WithoutSSHPassword.Replace(WithoutSSHPassword, "'" + WithoutSSHPassword + "'");
               
                string ssh_HostName = string.Empty;
                string ssh_PortNo = string.Empty;
                string ssh_UserName = string.Empty;
                string ssh_Password = string.Empty;
                string db_Port = string.Empty;
                from = dateTimePickerOSCommerceLastDate.Value.ToShortDateString() + " " + strTime;
                to = DateTime.Now.ToShortDateString() + " " + strTime;
                fromDate = Convert.ToDateTime(from);
                toDate = Convert.ToDateTime(to);
                
                #region For OSCommerce Transactions
                if (!IsEditable)
                {
                    //Checking Duplicates Trading Partner.
                    object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                    if (countTP == null)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToInt32(countTP) > 0)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                #region Database Details.
                if (string.IsNullOrEmpty(textBoxWebStoreURL.Text.Trim()))
                {
                    MessageBox.Show("Please enter OSCommerce Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxWebStoreURL.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxDBPortNo.Text.Trim()))
                {
                    MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxDBPortNo.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        db_Port = Convert.ToInt32(textBoxDBPortNo.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxDBPortNo.Focus();
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxOSCUsername.Text.Trim()))
                {
                    MessageBox.Show("Please enter OSCommerce User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxOSCUsername.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxDatabaseName.Text.Trim()))
                {
                    MessageBox.Show("Please enter OSCommerce Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxDatabaseName.Focus();
                    return;
                }
                if (!string.IsNullOrEmpty(textBox_Prefix.Text.Trim()))
                {
                    if (!textBox_Prefix.Text.Trim().Contains("_"))
                    {
                        MessageBox.Show("Please enter the table prefix(For eg : osc_ or 3fish_ ).", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox_Prefix.Focus();
                        return;
                    }

                }
                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks" || TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks POS" )
                {
                    if (comboBoxOSCShippingItem.SelectedIndex.Equals(-1))
                    {
                        MessageBox.Show("Please select shipping item for OSCommerce.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        comboBoxOSCShippingItem.Focus();
                        return;
                    }

                }
                #endregion

                #region SSH Details.
                if (checkBoxSSHTunnel.Checked)
                {
                    if (string.IsNullOrEmpty(textBoxSSHHostName.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHHostName.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxSSHPortNo.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortNo.Focus();
                        return;
                    }
                    else
                    {
                        try
                        {
                            ssh_PortNo = Convert.ToInt32(textBoxSSHPortNo.Text.Trim()).ToString();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBoxSSHPortNo.Focus();
                            return;
                        }
                    }
                    if (string.IsNullOrEmpty(textBoxSSHUserName.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHUserName.Focus();
                        return;
                    }                  
                    if (string.IsNullOrEmpty(textBoxSSHPassword.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPassword.Focus();
                        return;
                    }
                    ssh_HostName = textBoxSSHHostName.Text.Trim();
                    ssh_UserName = textBoxSSHUserName.Text.Trim();
                    ssh_Password = textBoxSSHPassword.Text.Trim();

                    //If ssh_Password contains ";".
                    if (ssh_Password.Contains(";"))
                        ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");
                }
                #endregion

                if (!checkBoxSSHTunnel.Checked)
                {
                    if (!FinancialEntities.OSCommerce.GetInstance().CheckConnection(textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword, textBoxDatabaseName.Text.Trim(), db_Port))
                    {
                        MessageBox.Show("Please enter valid OSCommerce Database details OR Make sure that Remote access of OSCommerce Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword, textBoxDatabaseName.Text.Trim(), db_Port, textBoxSSHHostName.Text.Trim(), textBoxSSHPassword.Text.Trim(), textBoxSSHPortNo.Text.Trim(), textBoxSSHUserName.Text.Trim(),textBox_Prefix.Text.Trim()))                            
                        {
                            //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            //return;
                        }
                        else
                        {
                            MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                else
                {
                    int sshPort = 22; //ssh port                
                    int port = Convert.ToInt32(sshPort);

                    //OdbcConnection con = null;

                    Session session = null;
                    JSch jsch = new JSch();
                    try
                    {
                        session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                        session.setHost(ssh_HostName);
                        session.setPassword(ssh_Password);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);
                        session.connect();

                        //Set port forwarding on the opened session.
                        session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxWebStoreURL.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                        if (session.isConnected())
                        {
                            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                                                       
                            try
                            {
                                conn.ConnectionString = "Server=" + textBoxWebStoreURL.Text.Trim() + ";User id=" + textBoxOSCUsername.Text.Trim() + ";Pwd=" + WithoutSSHPassword + ";Database=" + textBoxDatabaseName.Text.Trim() + ";Connection Timeout = 30;";
                                conn.Open();

                                if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword, textBoxDatabaseName.Text.Trim(), db_Port, textBoxSSHHostName.Text.Trim(), textBoxSSHPassword.Text.Trim(), textBoxSSHPortNo.Text.Trim(), textBoxSSHUserName.Text.Trim(),textBox_Prefix.Text.Trim()))                            
                                {
                                    //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //return;
                                }
                                else
                                {
                                    MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            catch (SocketException ex)
                            {
                                MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            finally
                            {
                                conn.Close();
                                session.disconnect();
                            }
                        }
                    }
                    catch (SocketException)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //session.disconnect();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        CommonUtilities.WriteErrorLog(ex.Message);
                        //session.disconnect();
                    }
                    finally
                    {
                        if (session != null)
                            session.disconnect();

                    }
                }

                MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                //Save the OSCommerce Trading Partner details.
                if (tradingPartner.SaveOSCommerceTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword.Replace("'",string.Empty), textBoxWebStoreURL.Text.Trim(), db_Port, textBoxDatabaseName.Text.Trim(), comboBoxOSCShippingItem.SelectedItem == null ? string.Empty : comboBoxOSCShippingItem.SelectedItem.ToString(), ssh_HostName, ssh_PortNo, ssh_UserName, ssh_Password.Replace("'",string.Empty), fromDate.ToString("yyyy-MM-dd HH:mm:ss"), textBox_Prefix.Text.Trim()))
                {
                    //Refersh the controls
                    RefreshControl();
                    MessageBox.Show("OSCommerce Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (m_TradingPartnerID != -1)
                    {
                        panelTPABSave.Visible = false;
                        panelTPABShow.Visible = true;
                        PopulateDataGridView();
                    }
                    IsEditable = false;
                }
                else
                {
                    //MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                }

                #endregion
            }
            //For Saving Allied Express Transactions.
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(5))
            {
                #region For Allied Express

                //Checking Duplicates Trading Partner.
                object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSAE01, 6));
                if (countTP == null)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(textBoxAccountNumber.Text.Trim()))
                {
                    MessageBox.Show("Please enter Allied Express Account number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxAccountNumber.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxAccountCode.Text.Trim()))
                {
                    MessageBox.Show("Please enter Allied Express Account Code.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxAccountCode.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxAlliedExpressCompanyName.Text.Trim()))
                {
                    MessageBox.Show("Please enter Allied Express Company Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxAlliedExpressCompanyName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxCompanyPhone.Text.Trim()))
                {
                    MessageBox.Show("Please enter Allied Express Company Phone Number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxCompanyPhone.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxPrefix.Text.Trim()))
                {
                    MessageBox.Show("Please enter Prefix for Cannote number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxPrefix.Focus();
                    return;
                }
                if (textBoxPrefix.Text.Trim().Length < 3)
                {
                    MessageBox.Show("Please enter three digit Prefix for Cannote number.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxPrefix.Focus();
                    return;
                }

                if (Convert.ToInt32(countTP) == 0)
                {
                    //Display message of Duplicate Trading Partner.
                    // MessageBox.Show("This Trading partner already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                    //Save the Allied Express Trading Partner details.
                    if (tradingPartner.SaveAlliedExpressTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxAccountNumber.Text.Trim(), textBoxAccountCode.Text.Trim(), comboBoxState.SelectedItem.ToString(), textBoxAlliedExpressCompanyName.Text.Trim(),textBoxCompanyPhone.Text.Trim(),comboBoxAlliedExpressType.SelectedItem.ToString(),textBoxPrefix.Text.Trim()))
                    {
                        //Refresh Controls for Trading partner.
                        RefreshControl();
                        MessageBox.Show("Allied Express Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (m_TradingPartnerID != -1)
                        {
                            panelTPABSave.Visible = false;
                            panelTPABShow.Visible = true;
                            PopulateDataGridView();
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //return;
                    }
                }
                else
                { //Display message of Duplicate Trading Partner.
                    // MessageBox.Show("This Trading partner already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                    //Save the Allied Express Trading Partner details.
                    if (tradingPartner.UpdateAlliedExpressTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxAccountNumber.Text.Trim(), textBoxAccountCode.Text.Trim(), comboBoxState.SelectedItem.ToString(), textBoxAlliedExpressCompanyName.Text.Trim(), textBoxCompanyPhone.Text.Trim(),comboBoxAlliedExpressType.SelectedItem.ToString(), textBoxPrefix.Text.Trim()))
                    {
                        //Refresh Controls for Trading partner.
                        RefreshControl();
                        MessageBox.Show("Allied Express Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (m_TradingPartnerID != -1)
                        {
                            panelTPABSave.Visible = false;
                            panelTPABShow.Visible = true;
                            PopulateDataGridView();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
 
                }                

                #endregion
            }

            //Axis 8
            //For Saving X-Cart details
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(6))
            {
                #region For X-Cart

                string WithoutSSHPassword = textBoxXCartPwd.Text.Trim();

                //If WithoutSSHPassword contains ";".
                if (WithoutSSHPassword.Contains(";"))
                    WithoutSSHPassword = WithoutSSHPassword.Replace(WithoutSSHPassword, "'" + WithoutSSHPassword + "'");

                string ssh_HostName = string.Empty;
                string ssh_PortNo = string.Empty;
                string ssh_UserName = string.Empty;
                string ssh_Password = string.Empty;

                from = dateTimePickerXCartLastDate.Value.ToShortDateString() + " " + strTime;
                to = DateTime.Now.ToShortDateString() + " " + strTime;
                fromDate = Convert.ToDateTime(from);
                toDate = Convert.ToDateTime(to);

                string db_Port = string.Empty;

                if (!IsEditable)
                {
                    //Checking Duplicates Trading Partner.
                    object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                    if (countTP == null)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToInt32(countTP) > 0)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                #region Database Details.
                if (string.IsNullOrEmpty(textBoxXCartUrl.Text.Trim()))
                {
                    MessageBox.Show("Please enter X-Cart Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxXCartUrl.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxXCartPort.Text.Trim()))
                {
                    MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxXCartPort.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        db_Port = Convert.ToInt32(textBoxXCartPort.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxXCartPort.Text = string.Empty;
                        textBoxXCartPort.Focus();
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxXCartUserName.Text.Trim()))
                {
                    MessageBox.Show("Please enter X-Cart User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxXCartUserName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxXDBName.Text.Trim()))
                {
                    MessageBox.Show("Please enter X-Cart Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxXDBName.Focus();
                    return;
                }
                if (!string.IsNullOrEmpty(textBoxXCartPrefix.Text.Trim()))
                {
                    if (!textBoxXCartPrefix.Text.Trim().Contains("_"))
                    {
                        MessageBox.Show("Please enter the table prefix(For eg : xcart_).", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxXCartPrefix.Focus();
                        return;
                    }

                }

                #endregion

                #region SSH Details.

                if (checkBoxXCartSSHTunnel.Checked)
                {
                    if (string.IsNullOrEmpty(textBoxSSHHostnameXcart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHHostnameXcart.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxSSHPortnoXcart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortnoXcart.Focus();
                        return;
                    }
                    else
                    {
                        try
                        {
                            ssh_PortNo = Convert.ToInt32(textBoxSSHPortnoXcart.Text.Trim()).ToString();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBoxSSHPortnoXcart.Focus();
                            return;
                        }
                    }
                    if (string.IsNullOrEmpty(textBoxSSHUserNameXcart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHUserNameXcart.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxSSHPasswordXcart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPasswordXcart.Focus();
                        return;
                    }
                    ssh_HostName = textBoxSSHHostnameXcart.Text.Trim();
                    ssh_UserName = textBoxSSHUserNameXcart.Text.Trim();
                    ssh_Password = textBoxSSHPasswordXcart.Text.Trim();

                    //If ssh_Password contains ";".
                    if (ssh_Password.Contains(";"))
                        ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");
                }
                #endregion


                #region Check Connection
                if (!checkBoxSSHTunnel.Checked)
                {
                    if (!FinancialEntities.XCart.GetInstance().CheckConnection(textBoxXCartUrl.Text.Trim(), textBoxXCartUserName.Text.Trim(), textBoxXCartPwd.Text.Trim(), textBoxXDBName.Text.Trim(), db_Port))
                    {
                        MessageBox.Show("Please enter valid X-Cart Database details OR Make sure that Remote access of X-Cart Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    {
                        //if (FinancialEntities.XCart.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword, textBoxDatabaseName.Text.Trim(), db_Port, textBoxSSHHostName.Text.Trim(), textBoxSSHPassword.Text.Trim(), textBoxSSHPortNo.Text.Trim(), textBoxSSHUserName.Text.Trim(), textBox_Prefix.Text.Trim()))
                        //{
                        // MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //    //return;
                        //}
                        //else
                        //{
                        //    MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    return;
                        //}
                    }
                }
                else
                {
                    int sshPort = 22; //ssh port                
                    int port = Convert.ToInt32(sshPort);

                    //OdbcConnection con = null;

                    Session session = null;
                    JSch jsch = new JSch();
                    try
                    {
                        session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                        session.setHost(ssh_HostName);
                        session.setPassword(ssh_Password);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);
                        session.connect();
                        
                        //Set port forwarding on the opened session. 
                        session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxXCartUrl.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                        if (session.isConnected())
                        {
                            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();

                            try
                            {
                                conn.ConnectionString = "Server=" + textBoxXCartUrl.Text.Trim() + ";User id=" + textBoxXCartUserName.Text.Trim() + ";Pwd=" + WithoutSSHPassword + ";Database=" + textBoxXDBName.Text.Trim() + ";Connection Timeout = 30;"; 
                                conn.Open();

                                if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxXCartPort.Text, textBoxXCartUrl.Text.Trim(), textBoxXCartUserName.Text.Trim(), WithoutSSHPassword, textBoxXDBName.Text.Trim(), db_Port, textBoxSSHHostnameXcart.Text.Trim(), textBoxSSHPasswordXcart.Text.Trim(), textBoxSSHPortnoXcart.Text.Trim(), textBoxSSHUserNameXcart.Text.Trim(), textBoxXCartPrefix.Text.Trim())) 
                                {
                                    //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //return;
                                }
                                else
                                {
                                    MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            catch (SocketException ex)
                            {
                                MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            finally
                            {
                                conn.Close();
                                session.disconnect();
                            }
                        }
                    }
                    catch (SocketException)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //session.disconnect();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        CommonUtilities.WriteErrorLog(ex.Message);
                        //session.disconnect();
                    }
                    finally
                    {
                        if (session != null)
                            session.disconnect();

                    }
                }


                MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                //Save the X-Cart Trading Partner details.
                //DateTime ldate = Convert.ToDateTime(DateTime.Now);
                DateTime ldate = fromDate;
                if (tradingPartner.SaveXCartTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxXCartUserName.Text.Trim(), textBoxXCartPwd.Text.Trim(), textBoxXCartUrl.Text.Trim(), db_Port, textBoxXDBName.Text.Trim(), comboBoxXCartShippingItem.SelectedItem == null ? string.Empty : comboBoxXCartShippingItem.SelectedItem.ToString(), ssh_HostName, ssh_PortNo, ssh_UserName, ssh_Password.Replace("'",string.Empty), textBoxXCartPrefix.Text.Trim(), ldate.ToString("yyyy-MM-dd HH:mm:ss")))
                {
                    //Refersh the controls
                    RefreshControl();
                    MessageBox.Show("X-Cart Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (m_TradingPartnerID != -1)
                    {
                        panelTPABSave.Visible = false;
                        panelTPABShow.Visible = true;
                        PopulateDataGridView();
                    }
                    IsEditable = false;
                }


                #endregion

                #endregion

            }
			
			 //For Saving Zen cart transaction details

            if (comboBoxTPMailProtocol.SelectedIndex.Equals(7))
            {
                #region For Zen cart Transaction


                string WithoutSSHPassword = textBoxZenPassword.Text.Trim();

                //If WithoutSSHPassword contains ";".
                if (WithoutSSHPassword.Contains(";"))
                    WithoutSSHPassword = WithoutSSHPassword.Replace(WithoutSSHPassword, "'" + WithoutSSHPassword + "'");

                string ssh_HostName = string.Empty;
                string ssh_PortNo = string.Empty;
                string ssh_UserName = string.Empty;
                string ssh_Password = string.Empty;

                from = dateTimePickerZencartLastDate.Value.ToShortDateString() + " " + strTime;
                to = DateTime.Now.ToShortDateString() + " " + strTime;
                fromDate = Convert.ToDateTime(from);
                toDate = Convert.ToDateTime(to);

                string db_Port = string.Empty;


                if (!IsEditable)
                {
                    //Checking Duplicates Trading Partner.
                    object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                    if (countTP == null)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (Convert.ToInt32(countTP) > 0)
                    {
                        //Display message of Duplicate Trading Partner.
                        MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                #region Validation

                if (string.IsNullOrEmpty(textBoxZenWebStoreUrl.Text.Trim()))
                {
                    MessageBox.Show("Please enter Zencart Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxZenWebStoreUrl.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(textBoxZenPortNo.Text.Trim()))
                {
                    MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    string port = textBoxZenPortNo.Text.Trim();
                    textBoxZenPortNo.Text = port;
                    textBoxZenPortNo.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        db_Port = Convert.ToInt32(textBoxZenPortNo.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxZenPortNo.Text = "";
                        textBoxZenPortNo.Focus();
                        return;
                    }
                }


                if (string.IsNullOrEmpty(textBoxZenUsername.Text.Trim()))
                {
                    MessageBox.Show("Please enter Zencart User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxZenUsername.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxZenDatabaseName.Text.Trim()))
                {
                    MessageBox.Show("Please enter Zencart Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxZenDatabaseName.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(textBoxZenPassword.Text.Trim()))
                {
                    MessageBox.Show("Please enter Zencart Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxZenPassword.Focus();
                    return;
                }

                #endregion

                #region SSH Details.

                if (checkBoxZenCartSSHTunnel.Checked)
                {
                    if (string.IsNullOrEmpty(textBoxSSHHostnameZencart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHHostnameZencart.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxSSHPortnoZencart.Text.Trim()))
                    {
                        MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortnoZencart.Focus();
                        return;
                    }
                    else
                    {
                        try
                        {
                            ssh_PortNo = Convert.ToInt32(textBoxSSHPortnoZencart.Text.Trim()).ToString();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBoxSSHPortnoZencart.Focus();
                            return;
                        }
                    }
                    if (string.IsNullOrEmpty(textBoxSSHUsernameZencart.Text.Trim())) 
                    {
                        MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHUsernameZencart.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(textBoxSSHPasswordZencart.Text.Trim())) 
                    {
                        MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPasswordZencart.Focus();
                        return;
                    }
                    ssh_HostName = textBoxSSHHostnameZencart.Text.Trim();
                    ssh_UserName = textBoxSSHUsernameZencart.Text.Trim();
                    ssh_Password = textBoxSSHPasswordZencart.Text.Trim();

                    //If ssh_Password contains ";".
                    if (ssh_Password.Contains(";"))
                        ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");
                }
                #endregion

                //string Password = textBoxZenPassword.Text.Trim();
                //string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                ////If Password contains ";".
                //if (Password.Contains(";"))
                //    Password = Password.Replace(Password, "'" + Password + "'");

                #region Check Connection
                if (!checkBoxSSHTunnel.Checked)
                {
                    if (!FinancialEntities.ZenCart.GetInstance().CheckConnection(textBoxZenWebStoreUrl.Text.Trim(), textBoxZenUsername.Text.Trim(),textBoxZenPassword.Text.Trim(), textBoxZenDatabaseName.Text.Trim(), db_Port))
                    {
                        MessageBox.Show("Please enter valid Zencart Database details OR Make sure that Remote access of Zencart Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    int sshPort = 22; //ssh port                
                    int port = Convert.ToInt32(sshPort);

                    //OdbcConnection con = null;

                    Session session = null;
                    JSch jsch = new JSch();
                    try
                    {
                        session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                        session.setHost(ssh_HostName);
                        session.setPassword(ssh_Password);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);
                        session.connect();

                        //Set port forwarding on the opened session. 
                        session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxZenWebStoreUrl.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                        if (session.isConnected())
                        {
                            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(); 

                            try
                            {
                                conn.ConnectionString = "Server=" + textBoxZenWebStoreUrl.Text.Trim() + ";User id=" + textBoxZenUsername.Text.Trim() + ";Pwd=" + WithoutSSHPassword + ";Database=" + textBoxZenDatabaseName.Text.Trim() + ";Connection Timeout = 30;";
                                conn.Open();

                                if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxZenPortNo.Text, textBoxZenWebStoreUrl.Text.Trim(), textBoxZenUsername.Text.Trim(), WithoutSSHPassword, textBoxZenDatabaseName.Text.Trim(), db_Port, textBoxSSHHostnameZencart.Text.Trim(), textBoxSSHPasswordZencart.Text.Trim(), textBoxSSHPortnoZencart.Text.Trim(), textBoxSSHUsernameZencart.Text.Trim(),textBoxZenPrefix.Text.Trim()))
                                {
                                    //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //return;
                                }
                                else
                                {
                                    MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            catch (SocketException ex)
                            {
                                MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            finally
                            {
                                conn.Close();
                                session.disconnect();
                            }
                        }
                    }
                    catch (SocketException)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //session.disconnect();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        CommonUtilities.WriteErrorLog(ex.Message);
                        //session.disconnect();
                    }
                    finally
                    {
                        if (session != null)
                            session.disconnect();

                    }
                }
                #endregion
                MailSettings.TradingPartner tradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, comboBoxTPMailProtocol.SelectedValue.ToString());
                //Save the Zencart Trading Partner details.
                //DateTime ldate = Convert.ToDateTime(DateTime.Now);
                DateTime ldate = fromDate;
                if (tradingPartner.SaveZenCartTradingPartner(textBoxTPName.Text.Trim().Replace("'", string.Empty), textBoxZenUsername.Text.Trim(), textBoxZenPassword.Text.ToString().Replace("'", string.Empty), textBoxZenWebStoreUrl.Text.Trim(), db_Port, textBoxZenDatabaseName.Text.Trim(), comboBoxZenCartShippingItem.SelectedItem == null ? string.Empty : comboBoxZenCartShippingItem.SelectedItem.ToString(), ssh_HostName, ssh_PortNo, ssh_UserName, ssh_Password.Replace("'", string.Empty), ldate.ToString("yyyy-MM-dd HH:mm:ss"), textBoxZenPrefix.Text.Trim()))
                {
                    //Refersh the controls
                    RefreshControl();
                    MessageBox.Show("ZenCart Trading Partner Record is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (m_TradingPartnerID != -1)
                    {
                        panelTPABSave.Visible = false;
                        panelTPABShow.Visible = true;
                        PopulateDataGridView();
                    }
                    IsEditable = false;
                }
                else
                {
                    //MessageBox.Show("Please modify data then click save or Please try again.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //return;
                }
                #endregion
            }

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            panelTPABSave.Visible = false;
            panelTPABShow.Visible = true;
            PopulateDataGridView();
        }
       
        private void buttonTPSchemaBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialogTPSchemaBrowse.Filter = "schema file|*.xsd";
            DialogResult res = openFileDialogTPSchemaBrowse.ShowDialog();
            if (res == DialogResult.OK)
            {
                this.textBoxTPSchemaLoc.Text = this.openFileDialogTPSchemaBrowse.FileName;
            }
        }

        private void textBoxTPSchema_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                buttonAdd_Click(sender, e);
        }

        private void comboBoxTPMailProtocol_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxItemName.Visible = true;
            labelItemName.Visible = true;
            labelItemNameAsteric.Visible = true;

            if (comboBoxTPMailProtocol.SelectedIndex.Equals(1))
            {
                MessageBox.Show("Other mail protocol is not supported in this version of Zed Axis.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTPMailProtocol.SelectedIndex = 0;
                return;
            }
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(2))
            {
                MessageBox.Show("Other mail protocol is not supported in this version of Zed Axis.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTPMailProtocol.SelectedIndex = 0;
                return;
            }
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(0))
            {
                textBoxTPEmailAddress.Text = string.Empty;
                textBoxTPEmailAddress.Focus();
                textBoxTPAlternateEmail.Text = string.Empty;
                textBoxTPClientID.Text = string.Empty;
                textBoxTPSchemaLoc.Text = string.Empty;
                paneleBay.Visible = false;
                panelOSCommerce.Visible = false;
                panelAlliedExpress.Visible = false;
                panelZencart.Visible = false;
                panelPOP3.Visible = true;
                panelXCart.Visible = false;
            }
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(3))
            {
                //textBoxeBayUsername.Text = string.Empty;
                //textBoxeBayUsername.Focus();
                //textBoxeBayPassword.Text = string.Empty;
                //comboBoxItemMapping.SelectedIndex = 0;
                comboBoxItemName.SelectedIndex = -1;
                comboBoxDiscountItems.SelectedIndex = -1;
                comboBoxeBayType.SelectedIndex = 0;
                textBoxeBayAuthToken.Text = string.Empty;
                dateTimePickerLastUpdated.Value = DateTime.Now;
                panelPOP3.Visible = false;
                panelOSCommerce.Visible = false;
                panelZencart.Visible = false;
                panelAlliedExpress.Visible = false;
                buttoneBayTicket.Visible = false;
                paneleBay.Visible = true;
                panelXCart.Visible = false;

                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == Constants.QBstring)
                {
                    ItemQuery();
                    DiscountItemQuery();
                }
                else
                {
                    comboBoxItemName.Items.Clear();
                    comboBoxDiscountItems.Items.Clear();
                }
            }
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(4))
            {
                textBoxWebStoreURL.Text = string.Empty;
                textBoxWebStoreURL.Focus();
                textBoxOSCUsername.Text = string.Empty;
                textBoxOSCPassword.Text = string.Empty;
                textBoxDatabaseName.Text = string.Empty;
                textBoxDBPortNo.Text = string.Empty;
                textBox_Prefix.Text = string.Empty;

                textBoxSSHHostName.Text = string.Empty;
                textBoxSSHPortNo.Text = string.Empty;
                textBoxSSHUserName.Text = string.Empty;
                textBoxSSHPassword.Text = string.Empty;

                comboBoxOSCShippingItem.SelectedIndex = -1;
                comboBoxOSCShippingItem.Visible = true;
                labelOSCShippingItem.Visible = true;

                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks")
                {
                    ItemQuery();
                }
               
                paneleBay.Visible = false;
                panelPOP3.Visible = false;
                panelAlliedExpress.Visible = false;
                panelXCart.Visible = false;
			    panelZencart.Visible = false;
                panelOSCommerce.Visible = true;
            
            }

            if (comboBoxTPMailProtocol.SelectedIndex.Equals(5))
            {
                comboBoxAlliedExpressType.SelectedIndex = 0;
                comboBoxState.SelectedIndex = 0;
                textBoxAccountCode.Text = string.Empty;
                textBoxAccountNumber.Text = string.Empty;
                textBoxAccountNumber.Focus();
                textBoxAlliedExpressCompanyName.Text = string.Empty;
                textBoxCompanyPhone.Text = string.Empty;
                textBoxPrefix.Text = string.Empty;
                paneleBay.Visible = false;
                panelOSCommerce.Visible = false;
                panelPOP3.Visible = false;
                panelXCart.Visible = false;
			    panelZencart.Visible = false;
                panelAlliedExpress.Visible = true;

            }

            //For X-Cart
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(6))
            {                
                textBoxXCartUrl.Text = string.Empty;
                textBoxXCartPort.Text = string.Empty;
                textBoxXCartUserName.Text = string.Empty;
                textBoxXCartPwd.Text = string.Empty;
                textBoxXDBName.Text = string.Empty;
                paneleBay.Visible = false;
                panelOSCommerce.Visible = false;
                panelPOP3.Visible = false;
                panelAlliedExpress.Visible = false;
                panelXCart.Visible = true;
                panelZencart.Visible = false;
                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks")
                {
                    ItemQuery();
                }
               
            }
			 //For Zen Cart
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(7))
            {
                textBoxZenPortNo.Text = string.Empty;
                textBoxZenPassword.Text = string.Empty;
                textBoxZenDatabaseName.Text = string.Empty;
                textBoxZenUsername.Text = string.Empty;
                textBoxZenWebStoreUrl.Text = string.Empty;

                panelZencart.Visible = true;
                panelXCart.Visible = false;
                paneleBay.Visible = false;
                panelOSCommerce.Visible = false;
                panelPOP3.Visible = false;
                panelAlliedExpress.Visible = false;
                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks")
                {
                    ItemQuery();
                }
            }

            //For Magento
            if (comboBoxTPMailProtocol.SelectedIndex.Equals(8))
            {
                textBoxZenPortNo.Text = string.Empty;
                textBoxZenPassword.Text = string.Empty;
                textBoxZenDatabaseName.Text = string.Empty;
                textBoxZenUsername.Text = string.Empty;
                textBoxZenWebStoreUrl.Text = string.Empty;

               // panelZencart.Visible = true;
                panelZencart.Visible = false;
                panelXCart.Visible = false;
                paneleBay.Visible = false;
                panelOSCommerce.Visible = false;
                panelPOP3.Visible = false;
                panelAlliedExpress.Visible = false;
            }

			
        }

        private void buttonAddNew_Click(object sender, EventArgs e)
        {
            IsEditable = false;
            textBoxTPName.Enabled = true;
            comboBoxTPMailProtocol.Enabled = true;
            panelTPABShow.Visible = false;
            panelTPABSave.Visible = true;
            RefreshControl();
            m_TradingPartnerID = -1;
            m_TradingPartner = null;
            textBoxTPName.Focus();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            try
            {
                IsEditable = true;
                m_TradingPartnerID = Convert.ToInt64(dataGridViewTPABShow.SelectedRows[0].Cells[0].Value);
                textBoxTPName.Text = dataGridViewTPABShow.SelectedRows[0].Cells[1].Value.ToString();
                textBoxTPName.Enabled = false;
                string protocol = dataGridViewTPABShow.SelectedRows[0].Cells[2].Value.ToString();
                for (int i = 0; i < comboBoxTPMailProtocol.Items.Count; i++)
                {
                    if (protocol.Equals(((DataRowView)comboBoxTPMailProtocol.Items[i]).Row[MailProtocolColumns.MailProtocol.ToString()].ToString()))
                        comboBoxTPMailProtocol.SelectedIndex = i;
                }

                comboBoxTPMailProtocol.Enabled = false;
                if (comboBoxTPMailProtocol.SelectedIndex == 5)
                {
                    textBoxCompanyPhone.Text = dataGridViewTPABShow.SelectedRows[0].Cells[3].Value.ToString();
                    textBoxPrefix.Text = dataGridViewTPABShow.SelectedRows[0].Cells[4].Value.ToString();
                }
                else
                {
                    textBoxTPEmailAddress.Text = dataGridViewTPABShow.SelectedRows[0].Cells[3].Value.ToString();
                    textBoxTPAlternateEmail.Text = dataGridViewTPABShow.SelectedRows[0].Cells[4].Value.ToString();
                }
                textBoxTPClientID.Text = dataGridViewTPABShow.SelectedRows[0].Cells[5].Value.ToString();
                textBoxTPSchemaLoc.Text = dataGridViewTPABShow.SelectedRows[0].Cells[6].Value.ToString();
                m_TradingPartner = new MailSettings.TradingPartner(m_TradingPartnerID, textBoxTPName.Text, protocol, textBoxTPEmailAddress.Text, textBoxTPAlternateEmail.Text, textBoxTPClientID.Text, textBoxTPSchemaLoc.Text);
                if (protocol.Equals("eBay"))
                {
                    //textBoxeBayUsername.Text = dataGridViewTPABShow.SelectedRows[0].Cells[7].Value.ToString();
                    //textBoxeBayPassword.Text = dataGridViewTPABShow.SelectedRows[0].Cells[8].Value.ToString();
                    textBoxeBayAuthToken.Text = dataGridViewTPABShow.SelectedRows[0].Cells[9].Value.ToString();
                    string mapping = dataGridViewTPABShow.SelectedRows[0].Cells[13].Value.ToString();
                    //for (int i = 0; i < comboBoxItemMapping.Items.Count; i++)
                    //{
                    //    if (mapping.Equals(comboBoxItemMapping.Items[i].ToString()))
                    //        comboBoxItemMapping.SelectedIndex = i;
                    //}
                    string eBayType = dataGridViewTPABShow.SelectedRows[0].Cells["CompanyName"].Value.ToString();

                    if (string.IsNullOrEmpty(eBayType))
                    {
                        comboBoxeBayType.SelectedIndex = 0;
                    }
                    else
                    {
                        for (int i = 0; i < comboBoxeBayType.Items.Count; i++)
                        {
                            if (eBayType.Equals(comboBoxeBayType.Items[i].ToString()))
                                comboBoxeBayType.SelectedIndex = i;
                        }
                    }
                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == Constants.QBstring)
                    {
                        string shippingItem = dataGridViewTPABShow.SelectedRows[0].Cells[14].Value.ToString();
                        for (int i = 0; i < comboBoxItemName.Items.Count; i++)
                        {
                            if (shippingItem.Equals(comboBoxItemName.Items[i].ToString()))
                                comboBoxItemName.SelectedIndex = i;
                        }

                        string discountItem = dataGridViewTPABShow.SelectedRows[0].Cells[15].Value.ToString();
                        for (int i = 0; i < comboBoxDiscountItems.Items.Count; i++)
                        {
                            if (discountItem.Equals(comboBoxDiscountItems.Items[i].ToString()))
                                comboBoxDiscountItems.SelectedIndex = i;
                        }
                    }
                    else
                    {
                        comboBoxItemName.Items.Clear();
                        comboBoxDiscountItems.Items.Clear();
                    }
                    
                    dateTimePickerLastUpdated.Value = Convert.ToDateTime(dataGridViewTPABShow.SelectedRows[0].Cells[16].Value.ToString());
                    //Bug 981:Commenting(Axis 6.0)
                    //MessageBox.Show("If you want to modify ebay username and password then create new eBay Token for transaction.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    comboBoxTPMailProtocol.Enabled = false;

                }
                else
                {
                    if (protocol.Equals("OSCommerce"))
                    {
                        textBoxOSCUsername.Text = dataGridViewTPABShow.SelectedRows[0].Cells["UserName"].Value.ToString();
                        textBoxOSCPassword.Text = dataGridViewTPABShow.SelectedRows[0].Cells["Password"].Value.ToString();
                        textBoxWebStoreURL.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCWebStoreUrl"].Value.ToString();
                        textBoxDatabaseName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCDatabase"].Value.ToString();
                        textBoxDBPortNo.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayAuthToken"].Value.ToString();
                        textBoxSSHHostName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayItemMapping"].Value.ToString();
                        textBoxSSHPortNo.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayDevId"].Value.ToString();
                        textBoxSSHUserName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayCertId"].Value.ToString();
                        textBoxSSHPassword.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayAppId"].Value.ToString();
                        textBox_Prefix.Text = dataGridViewTPABShow.SelectedRows[0].Cells["XSDLocation"].Value.ToString();

                        if (!string.IsNullOrEmpty(dataGridViewTPABShow.SelectedRows[0].Cells["eBayItemMapping"].Value.ToString()))
                        {
                            checkBoxSSHTunnel.Checked = true;
                        }
                        else
                        {
                            checkBoxSSHTunnel.Checked = false;                           
                        }

                        string shippingItem = dataGridViewTPABShow.SelectedRows[0].Cells["eBayItem"].Value.ToString();

                        for (int i = 0; i < comboBoxOSCShippingItem.Items.Count; i++)
                        {
                            if (shippingItem.Equals(comboBoxOSCShippingItem.Items[i].ToString()))
                                comboBoxOSCShippingItem.SelectedIndex = i;
                        }
                        if (dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString() != string.Empty)
                            dateTimePickerOSCommerceLastDate.Value = Convert.ToDateTime(dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString());
                        comboBoxTPMailProtocol.Enabled = false;

                    }
                    else if (protocol.Equals("Allied Express"))
                    {
                        textBoxAccountNumber.Text = dataGridViewTPABShow.SelectedRows[0].Cells[19].Value.ToString();
                        textBoxAccountCode.Text = dataGridViewTPABShow.SelectedRows[0].Cells[7].Value.ToString();
                        textBoxAlliedExpressCompanyName.Text = dataGridViewTPABShow.SelectedRows[0].Cells[20].Value.ToString();

                        string AExpressType = dataGridViewTPABShow.SelectedRows[0].Cells["eBayItemMapping"].Value.ToString();
                        string AEState = dataGridViewTPABShow.SelectedRows[0].Cells[8].Value.ToString();

                        if (string.IsNullOrEmpty(AEState))
                        {
                            comboBoxState.SelectedIndex = 0;                         
                        }
                        else
                        {
                            for (int i = 0; i < comboBoxState.Items.Count; i++)
                            {
                                if (AEState.Equals(comboBoxState.Items[i].ToString()))
                                    comboBoxState.SelectedIndex = i;
                            }
                        }
                        if (string.IsNullOrEmpty(AExpressType))
                        {
                            comboBoxAlliedExpressType.SelectedIndex = 0;
                        }
                        else
                        {
                            for (int i = 0; i < comboBoxAlliedExpressType.Items.Count; i++)
                            {
                                if (AExpressType.Equals(comboBoxAlliedExpressType.Items[i].ToString()))
                                    comboBoxAlliedExpressType.SelectedIndex = i;
                            }
                        }
                    }

                    else if (protocol.Equals("X-Cart"))
                    {
                        textBoxXCartUserName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["UserName"].Value.ToString();
                        textBoxXCartPwd.Text = dataGridViewTPABShow.SelectedRows[0].Cells["Password"].Value.ToString();
                        textBoxXCartUrl.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCWebStoreUrl"].Value.ToString();
                        textBoxXDBName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCDatabase"].Value.ToString();
                        textBoxXCartPort.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayAuthToken"].Value.ToString();
                        textBoxXCartPrefix.Text = dataGridViewTPABShow.SelectedRows[0].Cells["XSDLocation"].Value.ToString();
                        
                        comboBoxTPMailProtocol.Enabled = false;

                        if (!string.IsNullOrEmpty(dataGridViewTPABShow.SelectedRows[0].Cells["eBayItemMapping"].Value.ToString()))
                        {
                            checkBoxXCartSSHTunnel.Checked = true;
                        }
                        else
                        {
                            checkBoxXCartSSHTunnel.Checked = false;
                        }
                         
                        string shippingItem = dataGridViewTPABShow.SelectedRows[0].Cells["eBayItem"].Value.ToString();

                        for (int i = 0; i < comboBoxXCartShippingItem.Items.Count; i++)
                        {
                            if (shippingItem.Equals(comboBoxXCartShippingItem.Items[i].ToString()))
                                comboBoxXCartShippingItem.SelectedIndex = i;
                        }
                        if (dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString() != string.Empty)
                            dateTimePickerXCartLastDate.Value = Convert.ToDateTime(dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString());
                                                 
                    }
					 else if (protocol.Equals("Zen Cart"))
                    {
                        textBoxZenWebStoreUrl.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCWebStoreUrl"].Value.ToString();
                        textBoxZenPassword.Text = dataGridViewTPABShow.SelectedRows[0].Cells["Password"].Value.ToString();
                        textBoxZenUsername.Text = dataGridViewTPABShow.SelectedRows[0].Cells["UserName"].Value.ToString();
                        textBoxZenDatabaseName.Text = dataGridViewTPABShow.SelectedRows[0].Cells["OSCDatabase"].Value.ToString();
                        textBoxZenPrefix.Text = dataGridViewTPABShow.SelectedRows[0].Cells["XSDLocation"].Value.ToString();
                        textBoxZenPortNo.Text = dataGridViewTPABShow.SelectedRows[0].Cells["eBayAuthToken"].Value.ToString();
                        comboBoxTPMailProtocol.Enabled = false;
                          
                        if (!string.IsNullOrEmpty(dataGridViewTPABShow.SelectedRows[0].Cells["eBayItemMapping"].Value.ToString()))
                        {
                            checkBoxZenCartSSHTunnel.Checked = true;
                        }
                        else
                        {
                            checkBoxZenCartSSHTunnel.Checked = false;
                        }

                        string shippingItem = dataGridViewTPABShow.SelectedRows[0].Cells["eBayItem"].Value.ToString();

                        for (int i = 0; i < comboBoxZenCartShippingItem.Items.Count; i++)
                        {
                            if (shippingItem.Equals(comboBoxZenCartShippingItem.Items[i].ToString()))
                                comboBoxZenCartShippingItem.SelectedIndex = i;
                        }
                        if (dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString() != string.Empty)
                            dateTimePickerZencartLastDate.Value = Convert.ToDateTime(dataGridViewTPABShow.SelectedRows[0].Cells["LastUpdatedDate"].Value.ToString());
                          
                    }
 
                }
                panelTPABShow.Visible = false;
                panelTPABSave.Visible = true;
            }
            catch (ArgumentOutOfRangeException)
            { }            
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete the record?", ProductCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                int ID = Convert.ToInt32(dataGridViewTPABShow.SelectedRows[0].Cells[0].Value);
                MailSettings.TradingPartner.DeleteTPByID(ID);
                comboBoxTPMailProtocol.Enabled = true;
                PopulateDataGridView();
            }
        }

        private void RefreshControl()
        {
            comboBoxTPMailProtocol.Enabled = true;
            textBoxTPName.Text = string.Empty;
            textBoxTPEmailAddress.Text = string.Empty;
            textBoxTPAlternateEmail.Text = string.Empty;
            textBoxTPClientID.Text = string.Empty;
            textBoxTPSchemaLoc.Text = string.Empty;
            comboBoxTPMailProtocol.SelectedIndex = 0;
        }

        private void TradingPartnerAddressBook_Load(object sender, EventArgs e)
        {
            comboBoxTPMailProtocol.DataSource = MailSettings.TradingPartner.GetMailProtocol();
            comboBoxTPMailProtocol.DisplayMember = DataProcessingBlocks.MailProtocolColumns.MailProtocol.ToString();
            comboBoxTPMailProtocol.ValueMember = DataProcessingBlocks.MailProtocolColumns.MailProtocol.ToString();
            panelTPABSave.Visible = false;
            panelTPABShow.Visible = true;
            dataGridViewTPABShow.DataSource = null;
            comboBoxTPMailProtocol.Enabled = true;
            this.dgColumnsBinding();
            PopulateDataGridView();
            PopuateStateDDL();
            
        }

        private void PopuateStateDDL()
        {
            this.comboBoxState.Items.Clear();
            this.comboBoxState.Items.Add(AlliedExpressState.NSW);
            this.comboBoxState.Items.Add(AlliedExpressState.VIC);
            this.comboBoxState.Items.Add(AlliedExpressState.QLD);
            this.comboBoxState.Items.Add(AlliedExpressState.SA);
            this.comboBoxState.Items.Add(AlliedExpressState.WA);
            this.comboBoxState.Items.Add(AlliedExpressState.TAS);
            this.comboBoxState.Items.Add(AlliedExpressState.NT);
            comboBoxState.SelectedIndex = 0;
        }

        private void dgColumnsBinding()
        {
            dataGridViewTPABShow.Columns.Add("ID", "ID");
            dataGridViewTPABShow.Columns.Add("Name","Name");
            dataGridViewTPABShow.Columns.Add("MailProtocol", "Mail Protocol");
            dataGridViewTPABShow.Columns.Add("Email", "Email");
            dataGridViewTPABShow.Columns.Add("AlternateEmail", "Alternate Email");
            dataGridViewTPABShow.Columns.Add("ClientID", "Client ID");
            dataGridViewTPABShow.Columns.Add("XSDLocation", "Schema Location (XSD)");
            dataGridViewTPABShow.Columns.Add("UserName", "ebay OR OSCommerce UserName");
            dataGridViewTPABShow.Columns.Add("Password", "eBay OR OSCommerce Password");
            dataGridViewTPABShow.Columns.Add("eBayAuthToken", "eBay Auth Token");
            dataGridViewTPABShow.Columns.Add("eBayDevId", "eBay Developer ID");
            dataGridViewTPABShow.Columns.Add("eBayCertId", "eBay Certification ID");
            dataGridViewTPABShow.Columns.Add("eBayAppId", "eBay Application Id");
            dataGridViewTPABShow.Columns.Add("eBayItemMapping", "eBay Item Mapping");
            dataGridViewTPABShow.Columns.Add("LastUpdatedDate", "Last Updated Date");
            dataGridViewTPABShow.Columns["Name"].Width = 200;
            dataGridViewTPABShow.Columns["MailProtocol"].Width = 200;
            dataGridViewTPABShow.Columns["LastUpdatedDate"].Width = 180;

            dataGridViewTPABShow.Columns.Add("OSCWebStoreUrl", "OSCommerce Web Store Server");
            dataGridViewTPABShow.Columns.Add("OSCDatabase", "OSCommerce Database Name.");
            dataGridViewTPABShow.Columns.Add("AccountNumber", "Allied Express Account Number");
            dataGridViewTPABShow.Columns.Add("CompanyName", "Allied Express Company Name");

            dataGridViewTPABShow.Columns.Add("eBayItem", "eBay OR OSCommerce Shipping Item");
            dataGridViewTPABShow.Columns.Add("eBayDiscountItem", "eBay Discount Item");

            dataGridViewTPABShow.Columns["ID"].DataPropertyName=TradingPartnerColumns.TradingPartnerID.ToString();
            dataGridViewTPABShow.Columns["Name"].DataPropertyName=TradingPartnerColumns.TPName.ToString();
            dataGridViewTPABShow.Columns["MailProtocol"].DataPropertyName = MailProtocolColumns.MailProtocol.ToString();
            dataGridViewTPABShow.Columns["Email"].DataPropertyName = TradingPartnerColumns.EmailID.ToString();
            dataGridViewTPABShow.Columns["AlternateEmail"].DataPropertyName = TradingPartnerColumns.AlternateEmailID.ToString();
            dataGridViewTPABShow.Columns["ClientID"].DataPropertyName = TradingPartnerColumns.ClientID.ToString();
            dataGridViewTPABShow.Columns["XSDLocation"].DataPropertyName = TradingPartnerColumns.XSDLocation.ToString();

            dataGridViewTPABShow.Columns["UserName"].DataPropertyName = TradingPartnerColumns.UserName.ToString();
            dataGridViewTPABShow.Columns["Password"].DataPropertyName = TradingPartnerColumns.Password.ToString();
            dataGridViewTPABShow.Columns["eBayAuthToken"].DataPropertyName = TradingPartnerColumns.eBayAuthToken.ToString();
            dataGridViewTPABShow.Columns["eBayDevId"].DataPropertyName = TradingPartnerColumns.eBayDevId.ToString();
            dataGridViewTPABShow.Columns["eBayCertId"].DataPropertyName = TradingPartnerColumns.eBayCertId.ToString();
            dataGridViewTPABShow.Columns["eBayAppId"].DataPropertyName = TradingPartnerColumns.eBayAppId.ToString();
            dataGridViewTPABShow.Columns["eBayItemMapping"].DataPropertyName = TradingPartnerColumns.eBayItemMapping.ToString();
            dataGridViewTPABShow.Columns["LastUpdatedDate"].DataPropertyName = MessageColumns.LastUpdatedDate.ToString();


            dataGridViewTPABShow.Columns["OSCWebStoreUrl"].DataPropertyName = TradingPartnerColumns.OSCWebStoreUrl.ToString();
            dataGridViewTPABShow.Columns["OSCDatabase"].DataPropertyName = TradingPartnerColumns.OSCDatabase.ToString();
            dataGridViewTPABShow.Columns["AccountNumber"].DataPropertyName = TradingPartnerColumns.AccountNumber.ToString();
            dataGridViewTPABShow.Columns["CompanyName"].DataPropertyName = TradingPartnerColumns.CompanyName.ToString();

            dataGridViewTPABShow.Columns["eBayItem"].DataPropertyName = TradingPartnerColumns.eBayItem.ToString();
            dataGridViewTPABShow.Columns["eBayDiscountItem"].DataPropertyName = TradingPartnerColumns.eBayDiscountItem.ToString();



            dataGridViewTPABShow.Columns["ID"].Visible = false;
            dataGridViewTPABShow.Columns["Email"].Visible = false;
            dataGridViewTPABShow.Columns["AlternateEmail"].Visible = false;
            dataGridViewTPABShow.Columns["ClientID"].Visible = false;
            dataGridViewTPABShow.Columns["XSDLocation"].Visible = false;

            dataGridViewTPABShow.Columns["UserName"].Visible = false;
            dataGridViewTPABShow.Columns["Password"].Visible = false;
            dataGridViewTPABShow.Columns["eBayAuthToken"].Visible = false;
            dataGridViewTPABShow.Columns["eBayDevId"].Visible = false;
            dataGridViewTPABShow.Columns["eBayCertId"].Visible = false;
            dataGridViewTPABShow.Columns["eBayAppId"].Visible = false;
            dataGridViewTPABShow.Columns["eBayItemMapping"].Visible = false;

            dataGridViewTPABShow.Columns["OSCWebStoreUrl"].Visible = false;
            dataGridViewTPABShow.Columns["OSCDatabase"].Visible = false;
            dataGridViewTPABShow.Columns["AccountNumber"].Visible = false;
            dataGridViewTPABShow.Columns["CompanyName"].Visible = false;

            dataGridViewTPABShow.Columns["eBayItem"].Visible = false;
            dataGridViewTPABShow.Columns["eBayDiscountItem"].Visible = false;          
        }

        private void buttonCancelForm_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            m_TPAddressBook = null;
            m_TradingPartner = null;
            dataGridViewTPABShow.DataSource = null;
        }

        private void PopulateDataGridView()
        {
            try
            {
                DataTable dtTradingPartner = MailSettings.TradingPartner.GetTradingPartner().Tables[0];
                if (dtTradingPartner != null)
                {
                    dataGridViewTPABShow.DataSource = dtTradingPartner;
                    if (dataGridViewTPABShow.RowCount == 0)
                    {
                        buttonDelete.Enabled = false;
                        buttonEdit.Enabled = false;
                    }
                    else
                    {
                        buttonEdit.Enabled = true;
                        buttonDelete.Enabled = true;
                    }
                }
                else
                {
                    buttonEdit.Enabled = true;
                    buttonDelete.Enabled = true;
                }
            }
            catch
            {
                buttonEdit.Enabled = false;
                buttonDelete.Enabled = false;
            }
        }

        private void dataGridViewTPABShow_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            IsEditable = true;
            buttonEdit_Click(sender, e);
        }

        private void buttonAuthorization_Click(object sender, EventArgs e)
        {
          
            //if (string.IsNullOrEmpty(textBoxeBayUsername.Text.Trim()))
            //{
            //    MessageBox.Show("Please enter eBay Username.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxeBayUsername.Focus();
            //    return;
            //}
            //if (string.IsNullOrEmpty(textBoxeBayPassword.Text.Trim()))
            //{
            //    MessageBox.Show("Please enter eBay Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxeBayPassword.Focus();
            //    return;
            //}
            
            
           
            #region Getting eBay Auth Token
            
            this.Cursor = Cursors.WaitCursor;

            //Get eBay Authentication ticket.
            //string eBaySession = FinancialEntities.eBayLibrary.GetInstance().GetSessionID(textBoxeBayUsername.Text, textBoxeBayPassword.Text,comboBoxeBayType.SelectedItem.ToString());
            string eBaySession = FinancialEntities.eBayLibrary.GetInstance().GetSessionID(string.Empty, string.Empty, comboBoxeBayType.SelectedItem.ToString());
            //string eBayAuthToken = "AgAAAA**AQAAAA**aAAAAA**doFdSw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4CoD5eAoQWdj6x9nY+seQ**4DoBAA**AAMAAA**m0MVO3yxF1U8nuqV1q0+hjOz23Z4kna+1r5IVaj3P6TPxOeFYGEIaQbZS9nsJcwXyo/G8DqwJtjLTDldqp28QR9LGwBuGQKM9NVpDRotUCbFDlUoqQUJMibSfqJjizOMF4aN3mMn+Z3EBbdpfu2hOC9CCOOyBDy0gEZOoH3AXGL8KD9sCtXZjdHQO4/oJ816/jJtTNPYuSLr0GFLL9LQxEQGokGDGFWsFDsBs427qHrCm35C/gHaeTKQ6dv1oFPafU5B81fjkMldgejUIuSj5Key26fGbV2iiqn4qcuwn9wljYPumbxMJkC5e9/+C+emx9Tcq793mSHJtFbb1XcV0gVVGSlYrleTKcQ9bbIT+5BZUzCjqyDew1B6rTpDq+8RcQQJIZ2/r4A+ZQfCDgnQI9bMZXUlDot609Wxt1TFxgRbXgvHUPrC5qghV8KUHCb/CHO5Wcv4NleAY30h6rGlgXpjqsQpASB1YalvlUkVW5YoClAMDS7mwVuIUxQ9j0ahD8cLF5zw6keSSk3OunqDU19UULrFZhm2umWNNQjIGvdH6ZRtU2RfHuJo3OYBFCJ3zZC6hfRyxLU2mkava+RlLdRvMrUiO9fkkGtqcQzoqjyq24OQPsU4sqnJTV5aQQ/8L2LQcRWy6CG5pP14Ek9ttPJXfK9hjAL90trLm9bKiBEYbHgZMc5xGPAkuc0hEKc/6uQYdcKsDkdhTVHWsjXU5eNLshnz6p9AhMdBhOuSSq6MWfQG0GTu6xDMTJNc8+zn";

            if (eBaySession == string.Empty)
            {
                //MessageBox.Show("Please enter valid eBay Username or Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Cursor = Cursors.Default;
                return;
            }
            else
            {
                if (eBaySession == "Application can not connect eBay.Please check the internet connection.")
                {
                    MessageBox.Show(eBaySession, EDI.Constant.Constants.ProductName);
                    this.Cursor = Cursors.Default;
                    return;
                }
                else
                    if (eBaySession == "Invalid user name and password.")
                    {
                        MessageBox.Show(eBaySession, EDI.Constant.Constants.ProductName);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    else
                    {
                        buttoneBayTicket.Visible = true;
                        m_SessionID = eBaySession;
                    }
                //textBoxeBayAuthToken.Text = eBayAuthToken;
            }

            this.Cursor = Cursors.Default;
            #endregion
        }

        private void buttoneBayTicket_Click(object sender, EventArgs e)
        {
            if (m_SessionID == string.Empty)
            {
                MessageBox.Show("Please click on Authorisation button.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                string ebayToken = FinancialEntities.eBayLibrary.GetInstance().GetEBayAuthToken(m_SessionID ,comboBoxeBayType.SelectedItem.ToString());
                if (ebayToken == string.Empty)
                {
                    //MessageBox.Show("Please enter valid ebay SandBox username and password OR Please complete the eBay Signing process.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    textBoxeBayAuthToken.Text = ebayToken;
                }
            }
        }

        //private void comboBoxItemMapping_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (comboBoxItemMapping.SelectedIndex == 0)
        //    {
        //        labelItemName.Text = "               Select Shipping Item:";                
        //    }
        //    else
        //    {
        //        labelItemName.Text = "Select " + comboBoxItemMapping.SelectedItem.ToString() + " Item: ";
        //    }
        //    labelDiscountItemName.Text = "Select Discount/Charge Item: ";

        //}

        /// <summary>
        /// This method is used to disable the controls when loading the form.
        /// </summary>
        private void DisableControls()
        {
            //labelItemName.Visible = false;
            //labelDiscountItemName.Visible = false;
            //labelDiscountItemsAsteric.Visible = false;
            //labelItemNameAsteric.Visible = false;
            comboBoxDiscountItems.Visible = false;
            comboBoxItemName.Visible = false;
        }

        /// <summary>
        /// This method is used to enable the controls when selecting the combobox.
        /// </summary>
        private void EnableControlsOnSelection()
        {
            labelItemName.Visible = true;
            labelDiscountItemName.Visible = true;
            //labelDiscountItemsAsteric.Visible = true;
            //labelItemNameAsteric.Visible = true;
            comboBoxDiscountItems.Visible = true;
            comboBoxItemName.Visible = true;
        }
      
        /// <summary>
        ///  Following method is for Retriving ItemQuery from Quick book .
        /// </summary>
        public bool ItemQuery()
        {           
            //Executing Item Query
            
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemQueryRq);
            ItemQueryRq.SetAttribute("requestID", "1");

            XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
            ItemQueryRq.AppendChild(ActiveStatus).InnerText = "All";
           
            string pinput = pxmldoc.OuterXml;
            string response = null;

            try
            {
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please connect to QuickBooks.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile , Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                response = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                        
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Not Connected to QuickBooks.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                MessageBox.Show("Please connect to QuickBooks. ", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            // parse the XML response and save data into Msde
            XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(response);

            try
            {

                # region Error
                //foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                //{
                //    string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                //    if (statusSeverity == "Error")
                //    {
                //        try
                //        {
                //            string statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                //            string ClientID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                //            string QueryString = "Insert into ConnectionErroLog(ClientID,ErrorMessage,TimeOfError) values ('" + ClientID + "','" + statusMessage + "','" + System.DateTime.Now.ToString("MM-dd-yyyy") + "')";
                //            SqlCommand cmd = new SqlCommand(QueryString, conn);
                //            cmd.ExecuteNonQuery();
                //            cmd.Dispose();
                //        }
                //        catch (Exception Ex)
                //        {
                //            MessageBox.Show(Ex.Message, CommonUtilities.ApplicationName);
                //        }
                //    }


                //}
                # endregion

                List<string> existingItem = new List<string>();

                #region Item Service
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemServiceRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Other Charge
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemOtherChargeRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Inventory
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemInventoryRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Non Inventory
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemNonInventoryRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Inventory Assembly
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemInventoryAssemblyRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item SubTotal
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemSubtotalRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Discount
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemDiscountRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Payment
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemPaymentRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                #region Item Group
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemGroupRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                comboBoxItemName.Items.Clear();
                comboBoxOSCShippingItem.Items.Clear();
                comboBoxZenCartShippingItem.Items.Clear();
                comboBoxXCartShippingItem.Items.Clear();

                for (int i = 0; i < existingItem.Count; i++)
                {
                    comboBoxItemName.Items.Add(existingItem[i]);
                    comboBoxOSCShippingItem.Items.Add(existingItem[i]);
                    comboBoxXCartShippingItem.Items.Add(existingItem[i]);
                    comboBoxZenCartShippingItem.Items.Add(existingItem[i]);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///  Following method is for Retriving ItemQuery from Quick book.
        /// </summary>
        public bool DiscountItemQuery()
        {
            //Executing Item Query
            
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemQueryRq);
            ItemQueryRq.SetAttribute("requestID", "1");

            XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
            ItemQueryRq.AppendChild(ActiveStatus).InnerText = "All";
          
            string pinput = pxmldoc.OuterXml;
            string response = null;

            try
            {
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please connect to QuickBooks.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                response = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Not Connected to QuickBooks.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                MessageBox.Show("Please connect to QuickBooks. ", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            // parse the XML response and save data into Msde
            XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(response);

            try
            {

                List<string> existingItem = new List<string>();
               
                #region Item Discount
                foreach (XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs/ItemDiscountRet"))
                {
                    if (oNode.SelectSingleNode("FullName") != null)
                    {
                        if (oNode.SelectSingleNode("FullName").InnerText.ToString() != "")
                        {
                            existingItem.Add(oNode.SelectSingleNode("FullName").InnerText.ToString());
                        }
                    }
                }
                #endregion

                comboBoxDiscountItems.Items.Clear();
                for(int index = 0;index < existingItem.Count;index++)
                    comboBoxDiscountItems.Items.Add(existingItem[index]);  
            }
            
            catch (Exception)
            {
                return false;
            }
            return true;
        }



        /// <summary>
        /// Access MySQL Through SSH Tunnel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSSHConnection_Click(object sender, EventArgs e)
        {
            string ssh_HostName = string.Empty;
            string ssh_PortNo = string.Empty;
            string ssh_UserName = string.Empty;
            string ssh_Password = string.Empty;
            string db_Port = string.Empty;


            #region For OSCommerce Transactions
            if (!IsEditable)
            {
                //Checking Duplicates Trading Partner.
                object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                if (countTP == null)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (Convert.ToInt32(countTP) > 0)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            #region Database Details.
            if (string.IsNullOrEmpty(textBoxWebStoreURL.Text.Trim()))
            {
                MessageBox.Show("Please enter OSCommerce Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxWebStoreURL.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxDBPortNo.Text.Trim()))
            {
                MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxDBPortNo.Focus();
                return;
            }
            else
            {
                try
                {
                    db_Port = Convert.ToInt32(textBoxDBPortNo.Text.Trim()).ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxDBPortNo.Focus();
                    return;
                }
            }
            if (string.IsNullOrEmpty(textBoxOSCUsername.Text.Trim()))
            {
                MessageBox.Show("Please enter OSCommerce User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxOSCUsername.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxDatabaseName.Text.Trim()))
            {
                MessageBox.Show("Please enter OSCommerce Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxDatabaseName.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(textBox_Prefix.Text.Trim()))
            {
                if (!textBox_Prefix.Text.Trim().Contains("_"))
                {
                    MessageBox.Show("Please enter the table prefix(For eg : osc_ or 3fish_ ).", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox_Prefix.Focus();
                    return;
                }

            }
            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == "QuickBooks")
            {
                if (comboBoxOSCShippingItem.SelectedIndex.Equals(-1))
                {
                    MessageBox.Show("Please select shipping item for OSCommerce.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    comboBoxOSCShippingItem.Focus();
                    return;
                }

            }
            #endregion

            #region SSH Details.
            if (checkBoxSSHTunnel.Checked)
            {
                if (string.IsNullOrEmpty(textBoxSSHHostName.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHHostName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPortNo.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPortNo.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        ssh_PortNo = Convert.ToInt32(textBoxSSHPortNo.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortNo.Focus();
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxSSHUserName.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHUserName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPassword.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPassword.Focus();
                    return;
                }
                ssh_HostName = textBoxSSHHostName.Text.Trim();
                ssh_UserName = textBoxSSHUserName.Text.Trim();
                ssh_Password = textBoxSSHPassword.Text.Trim();

                //If ssh_Password contains ";".
                if (ssh_Password.Contains(";"))
                    ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");
                
            }
            #endregion

            #endregion

            #region Test Connection.
            if (!checkBoxSSHTunnel.Checked)
            {
                string OscPassword = textBoxOSCPassword.Text.Trim();

                if (OscPassword.Contains(";"))
                {
                    OscPassword = OscPassword.Replace(OscPassword, "'" + OscPassword + "'");
                }
                if (!FinancialEntities.OSCommerce.GetInstance().CheckConnection(textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), OscPassword, textBoxDatabaseName.Text.Trim(), db_Port))
                {

                    MessageBox.Show("Test Connection Failed." + "\n" + "Please enter valid OSCommerce Database details OR Make sure that Remote access of OSCommerce Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;

                }
                else
                {   
                    if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), OscPassword, textBoxDatabaseName.Text.Trim(),db_Port,textBoxSSHHostName.Text.Trim(),textBoxSSHPassword.Text.Trim(),textBoxSSHPortNo.Text.Trim(),textBoxSSHUserName.Text.Trim(),textBox_Prefix.Text.Trim()))
                    {
                        MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }
                    else
                    {
                        MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;    
                    }
                }
            }
            else
            {
                int sshPort = 22; //ssh port                
                int port = Convert.ToInt32(sshPort);
                string OscPassword = textBoxOSCPassword.Text.Trim();
                if (OscPassword.Contains(";"))
                {
                    OscPassword = OscPassword.Replace(OscPassword, "'" + OscPassword + "'");
                }

                //OdbcConnection con = null;

                Session session = null;
                JSch jsch = new JSch();
                try
                {
                    session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                    session.setHost(ssh_HostName);
                    session.setPassword(ssh_Password);
                    UserInfo ui = new MyUserInfo();
                    session.setUserInfo(ui);
                    session.connect();

                    //Set port forwarding on the opened session.
                    session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxWebStoreURL.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        try
                        {
                            conn.ConnectionString = "Server=" + textBoxWebStoreURL.Text.Trim() + ";User id=" + textBoxOSCUsername.Text.Trim() + ";Pwd=" + OscPassword + ";Database=" + textBoxDatabaseName.Text.Trim() + ";Connection Timeout = 30;";
                            conn.Open();

                            if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), OscPassword, textBoxDatabaseName.Text.Trim(),db_Port,textBoxSSHHostName.Text.Trim(),textBoxSSHPassword.Text.Trim(),textBoxSSHPortNo.Text.Trim(),textBoxSSHUserName.Text.Trim(),textBox_Prefix.Text.Trim()))                            
                            {
                                MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                return;
                            }
                            else
                            {
                                MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (SocketException ex)
                        {
                            MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        finally
                        {
                            session.disconnect();
                            conn.Close();
                        }
                    }
                }
                catch (SocketException)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    CommonUtilities.WriteErrorLog(ex.Message);
                }
                finally
                {
                    if (session != null)
                        session.disconnect();
                }

            }
            #endregion
        }

        private void checkBoxSSHTunnel_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSSHTunnel.Checked)
            {
                panelSSH.Visible = true;           
            }
            else
            {
                panelSSH.Visible = false; 
            }
        }

        private void dateTimePickerOSCommerceLastDate_ValueChanged(object sender, EventArgs e)
        {
            dateTimePickerLastUpdated.Update();
        }
		
	    private void textBoxPrefix_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Char.IsDigit(e.KeyChar);
        }

        private void textBoxCompanyPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validnumber;
            e.Handled = !str.Contains(e.KeyChar.ToString());
        }

        private void buttonXTestConnection_Click(object sender, EventArgs e)
        {
            string ssh_HostName = string.Empty;
            string ssh_PortNo = string.Empty;
            string ssh_UserName = string.Empty;
            string ssh_Password = string.Empty;
            string db_Port = string.Empty;

            #region For Xcart Transactions

            if (!IsEditable)
            {
                //Checking Duplicates Trading Partner.
                object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                if (countTP == null)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (Convert.ToInt32(countTP) > 0)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }


            #endregion

            #region Validation
            if (string.IsNullOrEmpty(textBoxXCartUrl.Text.Trim()))
            {
                MessageBox.Show("Please enter X-Cart Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxXCartUrl.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxXCartPort.Text.Trim()))
            {
                MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxXCartPort.Focus();
                return;
            }
            else
            {
                try
                {
                    db_Port = Convert.ToInt32(textBoxXCartPort.Text.Trim()).ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxXCartPort.Text = string.Empty;
                    textBoxXCartPort.Focus();
                    return;
                }
            }
            if (string.IsNullOrEmpty(textBoxXCartUserName.Text.Trim()))
            {
                MessageBox.Show("Please enter X-Cart User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxXCartUserName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxXDBName.Text.Trim()))
            {
                MessageBox.Show("Please enter X-Cart Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxXDBName.Focus();
                return;
            }


            if (!FinancialEntities.XCart.GetInstance().CheckConnection(textBoxXCartUrl.Text.Trim(), textBoxXCartUserName.Text.Trim(), textBoxXCartPwd.Text.Trim(), textBoxXDBName.Text.Trim(), db_Port))
            {
                MessageBox.Show("Please enter valid X-Cart Database details OR Make sure that Remote access of X-Cart Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                //if (FinancialEntities.XCart.GetInstance().CheckOrderV2Test(textBoxDBPortNo.Text, textBoxWebStoreURL.Text.Trim(), textBoxOSCUsername.Text.Trim(), WithoutSSHPassword, textBoxDatabaseName.Text.Trim(), db_Port, textBoxSSHHostName.Text.Trim(), textBoxSSHPassword.Text.Trim(), textBoxSSHPortNo.Text.Trim(), textBoxSSHUserName.Text.Trim(), textBox_Prefix.Text.Trim()))
                //{
               // MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                //    //return;
                //}
                //else
                //{
                //    MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    return;
                //}
            }
            #endregion

            #region SSH Details.

            if (checkBoxXCartSSHTunnel.Checked)
            {
                if (string.IsNullOrEmpty(textBoxSSHHostnameXcart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHHostnameXcart.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPortnoXcart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPortnoXcart.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        ssh_PortNo = Convert.ToInt32(textBoxSSHPortnoXcart.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortnoXcart.Focus();
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxSSHUserNameXcart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHUserNameXcart.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPasswordXcart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPasswordXcart.Focus();
                    return;
                }
                
                ssh_HostName = textBoxSSHHostnameXcart.Text.Trim();
                ssh_UserName = textBoxSSHUserNameXcart.Text.Trim();
                ssh_Password = textBoxSSHPasswordXcart.Text.Trim();

                //If ssh_Password contains ";".
                if (ssh_Password.Contains(";"))
                    ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");

            }
            #endregion

            #region Test Connection.

            if (!checkBoxXCartSSHTunnel.Checked)
            {
                string Password = textBoxXCartPwd.Text.Trim();

                if (Password.Contains(";"))
                {
                    Password = Password.Replace(Password, "'" + Password + "'");  
                }
                if (!FinancialEntities.OSCommerce.GetInstance().CheckConnection(textBoxXCartUrl.Text.Trim(), textBoxXCartUserName.Text.Trim(), Password, textBoxXDBName.Text.Trim(), db_Port))
                {

                    MessageBox.Show("Test Connection Failed." + "\n" + "Please enter valid XCart Database details OR Make sure that Remote access of XCart Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;

                }
                else
                {
                    MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else
            {
                int sshPort = 22; //ssh port                
                int port = Convert.ToInt32(sshPort);
                string OscPassword = textBoxXCartPwd.Text.Trim();
                if (OscPassword.Contains(";"))
                {
                    OscPassword = OscPassword.Replace(OscPassword, "'" + OscPassword + "'");
                }

                //OdbcConnection con = null;

                Session session = null;
                JSch jsch = new JSch();
                try
                {
                    session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                    session.setHost(ssh_HostName);
                    session.setPassword(ssh_Password);
                    UserInfo ui = new MyUserInfo();
                    session.setUserInfo(ui);
                    session.connect();
                    
                    //Set port forwarding on the opened session. 
                    session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxXCartUrl.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        try
                        {
                            conn.ConnectionString = "Server=" + textBoxXCartUrl.Text.Trim() + ";User id=" + textBoxXCartUserName.Text.Trim() + ";Pwd=" + OscPassword + ";Database=" + textBoxXDBName.Text.Trim() + ";Connection Timeout = 30;"; 
                            conn.Open();

                            if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxXCartPort.Text, textBoxXCartUrl.Text.Trim(), textBoxXCartUserName.Text.Trim(), OscPassword, textBoxXDBName.Text.Trim(), db_Port, textBoxSSHHostnameXcart.Text.Trim(), textBoxSSHPasswordXcart.Text.Trim(), textBoxSSHPortnoXcart.Text.Trim(), textBoxSSHUserNameXcart.Text.Trim(), textBoxXCartPrefix.Text.Trim()))
                            {
                                MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                return;
                            }
                            else
                            {
                                MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (SocketException ex)
                        {
                            MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        finally
                        {
                            session.disconnect();
                            conn.Close();
                        }
                    }
                }
                catch (SocketException)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    CommonUtilities.WriteErrorLog(ex.Message);
                }
                finally
                {
                    if (session != null)
                        session.disconnect();
                }

            }
            #endregion
        }

        /// <summary>
        /// Method to check database connection for Zen Cart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonZenTestConnection_Click(object sender, EventArgs e)
        {
            string ssh_HostName = string.Empty;
            string ssh_PortNo = string.Empty;
            string ssh_UserName = string.Empty;
            string ssh_Password = string.Empty;
            string db_Port = string.Empty;

            #region For Zencart Transactions
            if (!IsEditable)
            {
                //Checking Duplicates Trading Partner.
                object countTP = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMSTP32, textBoxTPName.Text.Trim().Replace("'", string.Empty)));
                if (countTP == null)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                if (Convert.ToInt32(countTP) > 0)
                {
                    //Display message of Duplicate Trading Partner.
                    MessageBox.Show("This Trading partner is already exists.Please use another trading partner name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            #region Validation

            if (string.IsNullOrEmpty(textBoxZenWebStoreUrl.Text.Trim()))
            {
                MessageBox.Show("Please enter Zencart Web Store URL.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxZenWebStoreUrl.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxZenPortNo.Text.Trim()))
            {
                MessageBox.Show("Please enter the database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                string port = textBoxZenPortNo.Text.Trim();
                textBoxZenPortNo.Text = port;
                textBoxZenPortNo.Focus();
                return;
            }
            else
            {
                try
                {
                    db_Port = Convert.ToInt32(textBoxZenPortNo.Text.Trim()).ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter the valid database Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxZenPortNo.Text = "";
                    textBoxZenPortNo.Focus();
                    return;
                }
            }


            if (string.IsNullOrEmpty(textBoxZenUsername.Text.Trim()))
            {
                MessageBox.Show("Please enter Zencart User Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxZenUsername.Focus();
                return;
            }
            if (string.IsNullOrEmpty(textBoxZenUsername.Text.Trim()))
            {
                MessageBox.Show("Please enter Zencart Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxZenUsername.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxZenPassword.Text.Trim()))
            {
                MessageBox.Show("Please enter Zencart Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxZenPassword.Focus();
                return;
            }

            if (string.IsNullOrEmpty(textBoxZenDatabaseName.Text.Trim()))
            {
                MessageBox.Show("Please enter Zencart Database Name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxZenDatabaseName.Focus();
                return;
            }

            #endregion

            #region SSH Details.

            if (checkBoxZenCartSSHTunnel.Checked)
            {
                if (string.IsNullOrEmpty(textBoxSSHHostnameZencart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH HostName/IP address.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHHostnameZencart.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPortnoZencart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPortnoZencart.Focus();
                    return;
                }
                else
                {
                    try
                    {
                        ssh_PortNo = Convert.ToInt32(textBoxSSHPortnoZencart.Text.Trim()).ToString();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please enter the valid SSH Port No.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxSSHPortnoZencart.Focus();
                        return;
                    }
                }
                if (string.IsNullOrEmpty(textBoxSSHUsernameZencart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH UserName.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHUsernameZencart.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(textBoxSSHPasswordZencart.Text.Trim()))
                {
                    MessageBox.Show("Please enter the SSH Password.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxSSHPasswordZencart.Focus();
                    return;
                }
                ssh_HostName = textBoxSSHHostnameZencart.Text.Trim();
                ssh_UserName = textBoxSSHUsernameZencart.Text.Trim();
                ssh_Password = textBoxSSHPasswordZencart.Text.Trim();

                //If ssh_Password contains ";".
                if (ssh_Password.Contains(";"))
                    ssh_Password = ssh_Password.Replace(ssh_Password, "'" + ssh_Password + "'");

            }
            #endregion

            #endregion

            #region Test Connection.

            if (!checkBoxZenCartSSHTunnel.Checked)
            {
                string Password = textBoxZenPassword.Text.Trim();

                if (Password.Contains(";"))
                {
                    Password = Password.Replace(Password, "'" + Password + "'");
                }
                if (!FinancialEntities.OSCommerce.GetInstance().CheckConnection(textBoxZenWebStoreUrl.Text.Trim(), textBoxZenUsername.Text.Trim(), Password, textBoxZenDatabaseName.Text.Trim(), db_Port))
                {

                    MessageBox.Show("Test Connection Failed." + "\n" + "Please enter valid ZenCart Database details OR Make sure that Remote access of ZenCart Server is enabled.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;

                }
                else
                {
                    MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else
            {
                int sshPort = 22; //ssh port                
                int port = Convert.ToInt32(sshPort);
                string OscPassword = textBoxZenPassword.Text.Trim();
                if (OscPassword.Contains(";"))
                {
                    OscPassword = OscPassword.Replace(OscPassword, "'" + OscPassword + "'");
                }

                //OdbcConnection con = null;

                Session session = null;
                JSch jsch = new JSch();
                try
                {
                    session = jsch.getSession(ssh_UserName, ssh_HostName, port);
                    session.setHost(ssh_HostName);
                    session.setPassword(ssh_Password);
                    UserInfo ui = new MyUserInfo();
                    session.setUserInfo(ui);
                    session.connect();
                    
                    //Set port forwarding on the opened session. 
                    session.setPortForwardingL(Convert.ToInt32(db_Port), textBoxZenWebStoreUrl.Text.Trim(), Convert.ToInt32(ssh_PortNo));
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        try
                        {
                            conn.ConnectionString = "Server=" + textBoxZenWebStoreUrl.Text.Trim() + ";User id=" + textBoxZenUsername.Text.Trim() + ";Pwd=" + OscPassword + ";Database=" + textBoxZenDatabaseName.Text.Trim() + ";Connection Timeout = 30;";
                            conn.Open();

                            if (FinancialEntities.OSCommerce.GetInstance().CheckOrderV2Test(textBoxZenPortNo.Text, textBoxZenWebStoreUrl.Text.Trim(), textBoxZenUsername.Text.Trim(), OscPassword, textBoxZenDatabaseName.Text.Trim(), db_Port, textBoxSSHHostnameZencart.Text.Trim(), textBoxSSHPasswordZencart.Text.Trim(), textBoxSSHPortnoZencart.Text.Trim(), textBoxSSHUsernameZencart.Text.Trim(), textBoxZenPrefix.Text.Trim()))
                            {
                                MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                return;
                            }
                            else
                            {
                                MessageBox.Show("Please enter valid prefix or database name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            //MessageBox.Show("Test Connection Successful.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (SocketException ex)
                        {
                            MessageBox.Show("Connection timeout.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Test Connection Failed." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        finally
                        {
                            session.disconnect();
                            conn.Close();
                        }
                    }
                }
                catch (SocketException)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Test Connection Failed.Please enter the correct details & try again." + "\n", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    CommonUtilities.WriteErrorLog(ex.Message);
                }
                finally
                {
                    if (session != null)
                        session.disconnect();
                }

            }
            #endregion
        }

        private void checkBoxZenCartSSHTunnel_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxZenCartSSHTunnel.Checked)
            {
                panelSSHZencart.Visible = true;
            }
            else
            {
                panelSSHZencart.Visible = false;
            }
        }

        private void checkBoxXCartSSHTunnel_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxXCartSSHTunnel.Checked)
            {
                panelSSHXcart.Visible = true;
            }
            else
            {
                panelSSHXcart.Visible = false;
            }
        }


       
    }

    public class MyUserInfo : UserInfo
    {
        /// &lt;summary&gt;
        /// Holds the user password
        /// &lt;/summary&gt;
        private String passwd;

        /// &lt;summary&gt;
        /// Returns the user password
        /// &lt;/summary&gt;
        public String getPassword() { return passwd; }

        /// &lt;summary&gt;
        /// Prompt the user for a Yes/No input
        /// &lt;/summary&gt;
        public bool promptYesNo(String str)
        {
            return true;
        }

        /// &lt;summary&gt;
        /// Returns the user passphrase (passwd for the private key file)
        /// &lt;/summary&gt;
        public String getPassphrase() { return null; }

        /// &lt;summary&gt;
        /// Prompt the user for a passphrase (passwd for the private key file)
        /// &lt;/summary&gt;
        public bool promptPassphrase(String message) { return true; }

        /// &lt;summary&gt;
        /// Prompt the user for a password
        /// &lt;/summary&gt;
        public bool promptPassword(String message) { return true; }

        /// &lt;summary&gt;
        /// Shows a message to the user
        /// &lt;/summary&gt;
        public void showMessage(String message) { }

    }
}