﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ComponentModel;
using Intuit.Ipp.Data;

namespace OnlineEntities
{

    public class BillLineDetail
    {

        private Collection<ItemRef> m_ItemRef = new Collection<ItemRef>();

        public Collection<ItemRef> ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }
        private string m_LineUnitPrice;

        public string LineUnitPrice
        {
            get { return m_LineUnitPrice; }
            set { m_LineUnitPrice = value; }
        }
        private string m_LineQty;

        public string LineQty
        {
            get { return m_LineQty; }
            set { m_LineQty = value; }
        }
        private Collection<TaxCodeRef> m_TaxCodeRef = new Collection<TaxCodeRef>();

        public Collection<TaxCodeRef> TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }

        //586
        private Collection<TxnTaxDetail> m_TxnTaxDetail = new Collection<TxnTaxDetail>();

        public Collection<TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail = value; }
        }
    }
}
