﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
   public  class TaxLine
   {
       private string m_Amount;

       public string Amount
       {
           get { return m_Amount; }
           set { m_Amount = value; }
       }
       private string m_DetailType;

       public string DetailType
       {
           get { return m_DetailType; }
           set { m_DetailType = value; }
       }
      
       private Collection<TaxLineDetail> m_TaxLineDetail = new Collection<TaxLineDetail>();

       public Collection<TaxLineDetail> TaxLineDetail
       {
           get { return m_TaxLineDetail; }
           set { m_TaxLineDetail = value; }
       }
    }
}
