﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public  class ItemRef
    {
        private string m_name;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        //bug 486
        private string m_SKU;

        public string SKU
        {
            get { return m_SKU; }
            set { m_SKU = value; }
        }
        
      //  private string m_LineQty;
    }


    // Axis 718
    public class PayrollItemRef
    {
        private string m_name;
        private string m_value;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public string Value
        {
            get { return m_value; }
            set { m_value = value; }
        }
    }
}
