﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
    public class AccountBasedExpenseLineDetail
    {
        private string m_Description;

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        private string m_AccountAmount;

        public string AccountAmount
        {
            get { return m_AccountAmount; }
            set { m_AccountAmount = value; }
        }
        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        private Collection<CustomerRef> m_customerRef = new Collection<CustomerRef>();

        public Collection<CustomerRef> customerRef
        {
            get { return m_customerRef; }
            set { m_customerRef = value; }
        }

        private Collection<APAccountRef> m_APAccountRef = new Collection<APAccountRef>();

        public Collection<APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        private Collection<AccountRef> m_AccountRef = new Collection<AccountRef>();

        public Collection<AccountRef> AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }

        private string m_BillableStatus;

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }
        private string m_Descreption;

        public string Descreption
        {
            get { return m_Descreption; }
            set { m_Descreption = value; }
        }

        private Collection<TaxCodeRef> m_TaxCodeRef = new Collection<TaxCodeRef>();

        public Collection<TaxCodeRef> TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }
        private string m_LineAmount;

        public string LineAmount
        {
            get { return m_LineAmount; }
            set { m_LineAmount = value; }
        }
    }
}
