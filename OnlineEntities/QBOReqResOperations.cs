﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intuit.Ipp.Data;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using System.Collections.ObjectModel;
using Intuit.Ipp.QueryFilter;
using System.Threading.Tasks;

namespace DataProcessingBlocks.OnlineEntities
{
    class QBOReqResOperations
    {
        public async System.Threading.Tasks.Task<ReferenceType> QBOGetAccountDetails(string AccName)
        {
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();

            ReadOnlyCollection<Intuit.Ipp.Data.Account> discountdataAccount = null;
            QueryService<Account> ItemQueryService = new QueryService<Account>(serviceContext);
            ReferenceType AccDetails = new ReferenceType();
            int no;
            if (int.TryParse(AccName, out no))
            {
                discountdataAccount = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(ItemQueryService.ExecuteIdsQuery("select * from Account where ID='" + no + "'"));
                foreach (var item in discountdataAccount)
                {
                    AccDetails.Value = item.Id;
                    AccDetails.name = item.Name;
                }
            }
            else
            {
                string Accno = AccName.Substring(0, AccName.IndexOf(" "));
                if (AccName.Contains("'"))
                {
                    AccName = AccName.Replace("'", "\\'");
                }

                if (int.TryParse(Accno, out no))
                {
                    discountdataAccount = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(ItemQueryService.ExecuteIdsQuery("select * from Account where ID='" + no + "'"));
                    foreach (var item in discountdataAccount)
                    {
                        AccDetails.Value = item.Id;
                        AccDetails.name = item.Name;
                    }
                }
                else
                {

                    discountdataAccount = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(ItemQueryService.ExecuteIdsQuery("select * from Account where FullyQualifiedName='" + AccName + "'"));
                    foreach (var item in discountdataAccount)
                    {
                        AccDetails.Value = item.Id;
                        AccDetails.name = item.Name;
                    }
                }

            }
            return AccDetails;

        }





    }
}
