﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
    public class IncomeAccountRef
    {
        private string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        //private string m_AcctNum;

        //public string AcctNum
        //{
        //    get { return m_AcctNum; }
        //    set { m_AcctNum = value; }
        //}

        private string m_Type;

        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        private string m_Value;

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
    }
}
