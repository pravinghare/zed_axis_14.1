﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
  public  class VendorRef
    {
        private string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        private string m_Type;

        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        private string m_Value;

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        //P Axis 13.1 : issue 687
        private string m_POEmail;
        public string POEmail
        {
            get { return m_POEmail; }
            set { m_POEmail = value; }
        }

    }
}

