﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public class EmailAddressEmployee
    {
        #region member
        private string m_EmailAddr;
        private string m_WebAddress;
       
        #endregion

        #region properties
        public string EmailAddress
        {
            get { return m_EmailAddr; }
            set { m_EmailAddr = value; }
        }
        public string WebAddress
        {
            get { return m_WebAddress; }
            set { m_WebAddress = value; }
        }
        #endregion
    }
}
