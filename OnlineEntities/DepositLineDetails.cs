﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
namespace OnlineEntities
{
  public class DepositLineDetails
    {

        #region member

      private Collection<Entity> m_Entity = new Collection<Entity>();

      public Collection<Entity> EntityRef
      {
          get { return m_Entity; }
          set { m_Entity = value; }
      }

      private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();

      public Collection<ClassRef> ClassRef
      {
          get { return m_ClassRef; }
          set { m_ClassRef = value; }
      }

      private Collection<AccountRef> m_AccountRef = new Collection<AccountRef>();

      public Collection<AccountRef> AccountRef
      {
          get { return m_AccountRef; }
          set { m_AccountRef = value; }
      }

      private Collection<PaymentMethodRef> m_PaymentMethodRef = new Collection<PaymentMethodRef>();

      public Collection<PaymentMethodRef> PaymentMethodRef
      {
          get { return m_PaymentMethodRef; }
          set { m_PaymentMethodRef = value; }
      }

      private string m_CheckNum;
      public string CheckNum
      {
          get { return m_CheckNum; }
          set { m_CheckNum = value; }
      }
      private string m_TxnType;

      public string TxnType
      {
          get { return m_TxnType; }
          set { m_TxnType = value; }
      }

      private string m_TaxCodeRef;
      public string TaxCodeRef
      {
          get { return m_TaxCodeRef; }
          set { m_TaxCodeRef = value; }
      }

      private string m_TaxApplicableOn;
      public string TaxApplicableOn
      {
          get { return m_TaxApplicableOn; }
          set { m_TaxApplicableOn = value; }
      }
        #endregion


     
    }
}
