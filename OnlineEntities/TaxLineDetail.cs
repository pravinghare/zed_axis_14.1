﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
  public class TaxLineDetail
  {
     
      private Collection<OnlineEntities.TaxRateRef> m_TaxRateRef = new Collection<OnlineEntities.TaxRateRef>();

      public Collection<OnlineEntities.TaxRateRef> TaxRateRef
      {
          get { return m_TaxRateRef; }
          set { m_TaxRateRef = value; }
      }
      private string m_PercentBased;

      public string PercentBased
      {
          get { return m_PercentBased; }
          set { m_PercentBased = value; }
      }
      private string m_TaxPercent;

      public string TaxPercent
      {
          get { return m_TaxPercent; }
          set { m_TaxPercent = value; }
      }
      private string m_NetAmountTaxable;

      public string NetAmountTaxable
      {
          get { return m_NetAmountTaxable; }
          set { m_NetAmountTaxable = value; }
      }
    }
}
