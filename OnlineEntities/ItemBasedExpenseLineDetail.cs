﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
  public class ItemBasedExpenseLineDetail
    {
        private Collection<OnlineEntities.CustomerRef> m_CustomerRef = new Collection<OnlineEntities.CustomerRef>();

        public Collection<OnlineEntities.CustomerRef> CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }
        private Collection<ItemRef> m_ItemRef = new Collection<ItemRef>();

        public Collection<ItemRef> ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }
        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        private Collection<TaxCodeRef> m_TaxCodeRef = new Collection<TaxCodeRef>();

        public Collection<TaxCodeRef> TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }
        private string m_UnitPrice;

        public string UnitPrice
        {
            get { return m_UnitPrice; }
            set { m_UnitPrice = value; }
        }
        private string m_Qty;

        public string Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }
        private string m_BillableStatus;

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }
        private string m_Descreption;

        public string Descreption
        {
            get { return m_Descreption; }
            set { m_Descreption = value; }
        }
        private string m_LineAmount;

        public string LineAmount
        {
            get { return m_LineAmount; }
            set { m_LineAmount = value; }
        }

        private string m_SKU;

        public string SKU
        {
            get { return m_SKU; }
            set { m_SKU = value; }
        }
                 
    }
}
