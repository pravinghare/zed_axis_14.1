﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
    
  public class BillAddr
    {
        #region member
        private string m_BillLine1;

        public string BillLine1
        {
            get { return m_BillLine1; }
            set { m_BillLine1 = value; }
        }
        private string m_BillLine2;

        public string BillLine2
        {
            get { return m_BillLine2; }
            set { m_BillLine2 = value; }
        }

        private string m_BillLine3;

        public string BillLine3
        {
            get { return m_BillLine3; }
            set { m_BillLine3 = value; }
        }
        private string m_BillCity;

        public string BillCity
        {
            get { return m_BillCity; }
            set { m_BillCity = value; }
        }
        private string m_BillCountry;

        public string BillCountry
        {
            get { return m_BillCountry; }
            set { m_BillCountry = value; }
        }
        private string m_BillCountrySubDivisionCode;

        public string BillCountrySubDivisionCode
        {
            get { return m_BillCountrySubDivisionCode; }
            set { m_BillCountrySubDivisionCode = value; }
        }
        private string m_BillPostalCode;

        public string BillPostalCode
        {
            get { return m_BillPostalCode; }
            set { m_BillPostalCode = value; }
        }

        private string m_BillNote;

        public string BillNote
        {
            get { return m_BillNote; }
            set { m_BillNote = value; }
        }      

        private string m_BillLine4;

        public string BillLine4
        {
            get { return m_BillLine4; }
            set { m_BillLine4 = value; }
        }
        private string m_BillLine5;

        public string BillLine5
        {
            get { return m_BillLine5; }
            set { m_BillLine5 = value; }
        }

        private string m_BillAddrLat;

        public string BillAddrLat
        {
            get { return m_BillAddrLat; }
            set { m_BillAddrLat = value; }
        }
        private string m_BillAddrLong;

        public string BillAddrLong
        {
            get { return m_BillAddrLong; }
            set { m_BillAddrLong = value; }
        }
      
        #endregion 
    }
}
