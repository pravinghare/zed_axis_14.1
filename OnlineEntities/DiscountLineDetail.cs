﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
   public class DiscountLineDetail
    {
        private string m_PercentBased;

        public string PercentBased
        {
            get { return m_PercentBased; }
            set { m_PercentBased = value; }
        }
        private string m_LineDiscountAmount;

        public string LineDiscountAmount
        {
            get { return m_LineDiscountAmount; }
            set { m_LineDiscountAmount = value; }
        }
        private string m_DiscountPercent;

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
        private Collection<OnlineEntities.DiscountAccountRef> m_DiscountAccountRef = new Collection<OnlineEntities.DiscountAccountRef>();

        public Collection<OnlineEntities.DiscountAccountRef> DiscountAccountRef
        {
            get { return m_DiscountAccountRef; }
            set { m_DiscountAccountRef = value; }
        }
        private Collection<Linkedtxn> m_Linkedtxn = new Collection<Linkedtxn>();

        public Collection<Linkedtxn> Linkedtxn
        {
            get { return m_Linkedtxn; }
            set { m_Linkedtxn = value; }
        }
    }
}
