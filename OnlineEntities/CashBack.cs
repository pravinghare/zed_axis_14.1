﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
  public  class CashBack
    {
        #region member
        //private string m_AccountRef;

        //public string AccountRef 
        //{
        //    get { return m_AccountRef; }
        //    set { m_AccountRef = value; }
        //}
      private Collection<AccountRef> m_AccountRef = new Collection<AccountRef>();

      public Collection<AccountRef> AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }
        private string m_Amount;

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        private string m_Memo;

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }
        #endregion
    }
}
