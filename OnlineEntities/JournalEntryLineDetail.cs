﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
    public class JournalEntryLineDetail
    {


        private string m_PostingType;
        private string m_Debit;
        private string m_Credit;

        public string PostingType
        {
            get { return m_PostingType; }
            set { m_PostingType = value; }
        }
        public string Debit
        {
            get { return m_Debit; }
            set { m_Debit = value; }
        }
        public string Credit
        {
            get { return m_Credit; }
            set { m_Credit = value; }
        }

        private Collection<Entity> m_Entity = new Collection<Entity>();

        public Collection<Entity> Entity
        {
            get { return m_Entity; }
            set { m_Entity = value; }
        }


        private Collection<AccountRef> m_AccountRef = new Collection<AccountRef>();

      
        public Collection<AccountRef> AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }

        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        private Collection<DepartmentRef> m_DepartmentRef = new Collection<DepartmentRef>();

        public Collection<DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        private Collection<TaxCodeRef> m_TaxCodeRef = new Collection<TaxCodeRef>();

        public Collection<TaxCodeRef> TaxCodeRef
        {
            get { return m_TaxCodeRef; }
            set { m_TaxCodeRef = value; }
        }

          // bug 458
        private Collection<TxnTaxDetail> m_TxnTaxDetail= new Collection<TxnTaxDetail>();

        public Collection<TxnTaxDetail> TxnTaxDetail
        {
            get { return m_TxnTaxDetail; }
            set { m_TxnTaxDetail= value; }
        }


        private string m_BillableStatus;

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }

        private string m_TotalAmount;

        public string TotalAmount
        {
            get { return m_TotalAmount; }
            set { m_TotalAmount = value; }
        }
    }
}
