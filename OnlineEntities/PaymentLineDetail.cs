﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
    public class PaymentLineDetail
    {

        private Collection<ItemRef> m_ItemRef = new Collection<ItemRef>();
        public Collection<ItemRef> ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }

        private Collection<ClassRef> m_ClassRef = new Collection<ClassRef>();

        public Collection<ClassRef> ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        private Collection<DiscountRef> m_DiscountRef = new Collection<DiscountRef>();

        public Collection<DiscountRef> DiscountRef
        {
            get { return m_DiscountRef; }
            set { m_DiscountRef = value; }
        }


        private Collection<OnlineEntities.DiscountAccountRef> m_DiscountAccountRef = new Collection<OnlineEntities.DiscountAccountRef>();

        public Collection<OnlineEntities.DiscountAccountRef> DiscountAccountRef
        {
            get { return m_DiscountAccountRef; }
            set { m_DiscountAccountRef = value; }
        }

        private string m_DiscountPercent;

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }
        private string m_PercentBased;

        public string PercentBased
        {
            get { return m_PercentBased; }
            set { m_PercentBased = value; }
        }
        private string m_LinePaymentAmount;

        public string LinePaymentAmount
        {
            get { return m_LinePaymentAmount; }
            set { m_LinePaymentAmount = value; }
        }
      
    }
}

