﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public  class PaymentMethodRef
    {
        private string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
    }
}
