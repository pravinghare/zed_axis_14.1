﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public class TelephoneNumber
    {
       #region member
       private string m_PrimaryPhone;
       private string m_Mobile;
       private string m_AlternatePhone;
       private string m_Fax;
       //591
       private string m_otherContact;
       
       #endregion

       #region properties
       public string PrimaryPhone
       {
            get { return m_PrimaryPhone; }
            set { m_PrimaryPhone = value; }
       }
       public string Mobile
       {
           get { return m_Mobile; }
           set { m_Mobile = value; }
       }
       public string AlternatePhone
       {
           get { return m_AlternatePhone; }
           set { m_AlternatePhone = value; }
       }
       public string Fax
       {
           get { return m_Fax; }
           set { m_Fax = value; }
       }

       //591       
       public string OtherContactInfo
       {
           get { return m_otherContact; }
           set { m_otherContact = value; }
       }

       #endregion
    }
}
