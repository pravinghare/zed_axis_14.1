﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
    public class CreditCardPayment
    {

        private Collection<CCAccountRef> m_CCAccountRef = new Collection<CCAccountRef>();

        public Collection<CCAccountRef> CCAccountRef
        {
            get { return m_CCAccountRef; }
            set { m_CCAccountRef = value; }
        }
    }
}
