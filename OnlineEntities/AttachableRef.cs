﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
   public class AttachableRef
    {
       private Collection<Entity> m_Entity = new Collection<Entity>();
       private string m_LineInfo;

       public string LineInfo
       {
           get { return m_LineInfo; }
           set { m_LineInfo = value; }
       }

       public Collection<Entity> Entity
        {
            get { return m_Entity; }
            set { m_Entity = value; }
        }
    }
}
