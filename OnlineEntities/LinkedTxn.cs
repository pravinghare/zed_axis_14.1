﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
    public class Linkedtxn
    {
        private string m_Type;
        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        private string m_Value;

        public string ID
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
        private string m_AMt;

        public string AMount
        {
            get { return m_AMt; }
            set { m_AMt = value; }
        }

        private string m_applytobillref;

        public string Applytobillref
        {
            get { return m_applytobillref; }
            set { m_applytobillref = value; }
        }
/// 11.4 bug 436
        public string TxnLineId { get; set; }
    }
}
