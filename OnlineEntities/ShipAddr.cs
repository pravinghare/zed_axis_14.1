﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public  class ShipAddr
    {
        #region member

        private string m_ShipLine1;

        public string ShipLine1
        {
            get { return m_ShipLine1; }
            set { m_ShipLine1 = value; }
        }
        private string m_ShipLine2;

        public string ShipLine2
        {
            get { return m_ShipLine2; }
            set { m_ShipLine2 = value; }
        }

        private string m_ShipLine3;

        public string ShipLine3
        {
            get { return m_ShipLine3; }
            set { m_ShipLine3 = value; }
        }
        private string m_ShipCity;

        public string ShipCity
        {
            get { return m_ShipCity; }
            set { m_ShipCity = value; }
        }
        private string m_ShipCountry;

        public string ShipCountry
        {
            get { return m_ShipCountry; }
            set { m_ShipCountry = value; }
        }
        private string m_ShipCountrySubDivisionCode;

        public string ShipCountrySubDivisionCode
        {
            get { return m_ShipCountrySubDivisionCode; }
            set { m_ShipCountrySubDivisionCode = value; }
        }
        private string m_ShipPostalCode;

        public string ShipPostalCode
        {
            get { return m_ShipPostalCode; }
            set { m_ShipPostalCode = value; }
        }

        private string m_ShipNote;

        public string ShipNote
        {
            get { return m_ShipNote; }
            set { m_ShipNote = value; }
        }

        private string m_ShipLine4;

        public string ShipLine4
        {
            get { return m_ShipLine4; }
            set { m_ShipLine4 = value; }
        }
        private string m_ShipLine5;

        public string ShipLine5
        {
            get { return m_ShipLine5; }
            set { m_ShipLine5 = value; }
        }

        private string m_ShipAddrLat;

        public string ShipAddrLat
        {
            get { return m_ShipAddrLat; }
            set { m_ShipAddrLat = value; }
        }
        private string m_ShipAddrLong;

        public string ShipAddrLong
        {
            get { return m_ShipAddrLong; }
            set { m_ShipAddrLong = value; }
        }

        #endregion 
    }
}
