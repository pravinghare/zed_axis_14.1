﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
   public  class Line
    {
       
       #region member
        private string m_LineDescription;

        public string LineDescription
        {
            get { return m_LineDescription; }
            set { m_LineDescription = value; }
        }
        private string m_LineAmount;

        public string LineAmount
        {
            get { return m_LineAmount; }
            set { m_LineAmount = value; }
        }
        private string m_LineDetailType;

        public string LineDetailType
        {
            get { return m_LineDetailType; }
            set { m_LineDetailType = value; }
        }
        private Collection<SalesItemLineDetail> m_SalesItemLineDetail = new Collection<SalesItemLineDetail>();

        public Collection<SalesItemLineDetail> SalesItemLineDetail
        {
            get { return m_SalesItemLineDetail; }
            set { m_SalesItemLineDetail = value; }
        }

        private Collection<AccountBasedExpenseLineDetail> m_AccountBasedExpenseLineDetail = new Collection<AccountBasedExpenseLineDetail>();

        public Collection<AccountBasedExpenseLineDetail> AccountBasedExpenseLineDetail
        {
            get { return m_AccountBasedExpenseLineDetail; }
            set { m_AccountBasedExpenseLineDetail = value; }
        }
        private Collection<JournalEntryLineDetail> m_JournalEntryLineDetail = new Collection<JournalEntryLineDetail>();

        public Collection<JournalEntryLineDetail> JournalEntryLineDetail
        {
            get { return m_JournalEntryLineDetail; }
            set { m_JournalEntryLineDetail = value; }
        }
        private Collection<DiscountLineDetail> m_DiscountLineDetail = new Collection<DiscountLineDetail>();

        public Collection<DiscountLineDetail> DiscountLineDetail
        {
            get { return m_DiscountLineDetail; }
            set { m_DiscountLineDetail = value; }
        }

        private Collection<ItemBasedExpenseLineDetail> m_ItemBasedExpenseLineDetail = new Collection<ItemBasedExpenseLineDetail>();

        public Collection<ItemBasedExpenseLineDetail> ItemBasedExpenseLineDetail
        {
            get { return m_ItemBasedExpenseLineDetail; }
            set { m_ItemBasedExpenseLineDetail = value; }
        }
        private Collection<PaymentLineDetail> m_PaymentLineDetail = new Collection<PaymentLineDetail>();

        public Collection<PaymentLineDetail> PaymentLineDetail
        {
            get { return m_PaymentLineDetail; }
            set { m_PaymentLineDetail = value; }
        }
        private Collection<DepositLineDetails> m_DepositLineDetails = new Collection<DepositLineDetails>();

        public Collection<DepositLineDetails> DepositLineDetail
        {
            get { return m_DepositLineDetails; }
            set { m_DepositLineDetails = value; }
        }
        private Collection<Linkedtxn> m_Linkedtxn = new Collection<Linkedtxn>();

        public Collection<Linkedtxn> LinkedTxn
        {
            get { return m_Linkedtxn; }
            set { m_Linkedtxn = value; }
        }
        private Collection<CustomField> m_LineCustomField = new Collection<CustomField>();

        public Collection<CustomField> LineCustomField
        {
            get { return m_LineCustomField; }
            set { m_LineCustomField = value; }
        }

        #endregion
    }
}
