﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
    class OtherContact
    {

        private string m_otherContact;

        public string OtherContactInfo
        {
            get { return m_otherContact; }
            set { m_otherContact = value; }
        }

    }
}
