﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
  public class TaxCodeRef
    {
        private string m_name;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        private string m_Value;

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }
    }
}
