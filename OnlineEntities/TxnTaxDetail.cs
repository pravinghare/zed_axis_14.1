﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
   public  class TxnTaxDetail
    {

       private string m_TotalTax;

       public string TotalTax
        {
            get { return m_TotalTax; }
            set { m_TotalTax = value; }
        }
        private Collection<TxnTaxCodeRef> m_TxnTaxCodeRef = new Collection<TxnTaxCodeRef>();

        public Collection<TxnTaxCodeRef> TxnTaxCodeRef
        {
            get { return m_TxnTaxCodeRef; }
            set { m_TxnTaxCodeRef = value; }
        }
        private Collection<TaxLine> m_TaxLine = new Collection<TaxLine>();

        public Collection<TaxLine> TaxLine
        {
            get { return m_TaxLine; }
            set { m_TaxLine = value; }
        }


       // bug 458 Axis 12.0

        private string m_TaxApplicableOn;

        public string TaxApplicableOn
        {
            get { return m_TaxApplicableOn; }
            set { m_TaxApplicableOn = value; }
        }

        private string m_TaxAmount;

        public string TaxAmount
        {
            get { return m_TaxAmount; }
            set { m_TaxAmount = value; }
        }


    }
}
