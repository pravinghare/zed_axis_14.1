﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineEntities
{
   public class VendorAddr
    {
        #region member
        private string m_VendorLine1;

        public string VendorLine1
        {
            get { return m_VendorLine1; }
            set { m_VendorLine1 = value; }
        }
        private string m_VendorLine2;

        public string VendorLine2
        {
            get { return m_VendorLine2; }
            set { m_VendorLine2 = value; }
        }

        private string m_VendorLine3;

        public string VendorLine3
        {
            get { return m_VendorLine3; }
            set { m_VendorLine3 = value; }
        }
        private string m_VendorCity;

        public string VendorCity
        {
            get { return m_VendorCity; }
            set { m_VendorCity = value; }
        }
        private string m_VendorCountry;

        public string VendorCountry
        {
            get { return m_VendorCountry; }
            set { m_VendorCountry = value; }
        }
        private string m_VendorCountrySubDivisionCode;

        public string VendorCountrySubDivisionCode
        {
            get { return m_VendorCountrySubDivisionCode; }
            set { m_VendorCountrySubDivisionCode = value; }
        }
        private string m_VendorPostalCode;

        public string VendorPostalCode
        {
            get { return m_VendorPostalCode; }
            set { m_VendorPostalCode = value; }
        }

        private string m_VendorNote;

        public string VendorNote
        {
            get { return m_VendorNote; }
            set { m_VendorNote = value; }
        }

        private string m_VendorLine4;

        public string VendorLine4
        {
            get { return m_VendorLine4; }
            set { m_VendorLine4 = value; }
        }
        private string m_VendorLine5;

        public string VendorLine5
        {
            get { return m_VendorLine5; }
            set { m_VendorLine5 = value; }
        }

        private string m_VendorAddrLat;

        public string VendorAddrLat
        {
            get { return m_VendorAddrLat; }
            set { m_VendorAddrLat = value; }
        }
        private string m_VendorAddrLong;

        public string VendorAddrLong
        {
            get { return m_VendorAddrLong; }
            set { m_VendorAddrLong = value; }
        }

        #endregion 
    }
}
