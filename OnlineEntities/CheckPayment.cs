﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace OnlineEntities
{
    public class CheckPayment
    {
        private string m_Printstatus;
      
        public string Printstatus
        {
            get { return m_Printstatus; }
            set { m_Printstatus = value; }
        }


        private Collection<BankAccountRef> m_BankAccountRef = new Collection<BankAccountRef>();

        public Collection<BankAccountRef> BankAccountRef
        {
            get { return m_BankAccountRef; }
            set { m_BankAccountRef = value; }
        }
    }
}
