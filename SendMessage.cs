using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MailSettings;
using EDI.Message;
using System.Net.Mail;
using System.Text.RegularExpressions;
using EDI.Constant;
using System.IO;
using TransactionImporter;
using System.Runtime.Serialization.Formatters.Binary;

namespace DataProcessingBlocks
{
    public partial class SendMessage : Form
    {
        #region Private Members
        /// <summary>
        /// Message Id of the message
        /// </summary>
        public int m_MessageId;
        /// <summary>
        /// Date of the message
        /// </summary>
        public DateTime m_MessageDate;
        /// <summary>
        /// Refrence message id
        /// </summary>
        public string m_RefMessageId;

        private string m_MessageName=string.Empty;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public int MessageId
        {
            get { return m_MessageId; }
            set { m_MessageId = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime MessageDate
        {
            get { return m_MessageDate; }
            set { m_MessageDate = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RefMessageId
        {
            get { return m_RefMessageId; }
            set { m_RefMessageId = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// int
        /// </summary>
        /// <param name="messageId"></param>
        public SendMessage(int messageId)
        {
            InitializeComponent();
            m_MessageId = messageId;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetAddresses()
        {
            //AddressList addressList = new AddressList(TradingPartner.GetTradingPartnerDetails());
            AddressList addressList = new AddressList(TradingPartner.GetTradingPartnerAddressDetails());
            addressList.ShowDialog();
            return addressList.Addresses;
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool ValidateMessage()
        {
            if (textBoxTo.Text == string.Empty)
            {
                MessageBox.Show("Please enter a email address", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxTo.Focus();
                return false;
            }
            else if (textBoxSubject.Text == string.Empty)
            {
                MessageBox.Show("Please enter a subject", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                textBoxTo.Focus();
                return false;
            }
            return true;
        }



        #endregion

        #region Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendMessage_Load(object sender, EventArgs e)
        {
            try
            {
                if (m_MessageName.Equals(string.Empty)) //if there is message id is present
                {
                    using (DataSet dataSetMessage = new MessageController().GetMessage(m_MessageId, (int)MessageStatus.Draft, 0, 0,0))
                    {
                        if (dataSetMessage != null && dataSetMessage.Tables[0].Rows.Count == 1)
                        {
                            DataRow messageRow = dataSetMessage.Tables[0].Rows[0];
                            textBoxTo.Text = messageRow.ItemArray[1].ToString().Replace("[", " ").Replace("]", " ").Trim();
                            textBoxSubject.Text = messageRow.ItemArray[2].ToString();
                            richTextBoxBody.Text = messageRow.ItemArray[3].ToString();
                            m_MessageDate = Convert.ToDateTime(messageRow.ItemArray[4].ToString());
                            m_RefMessageId = messageRow.ItemArray[5].ToString();
                            textBoxCc.Text = messageRow.ItemArray[6].ToString();
                        }
                        else
                        {
                            MessageBox.Show("Error loading message. Please refer log for details.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            this.Close();
                        }
                    }
                    using (DataSet dataSetNames = new MessageController().GetAttachmentNames(m_MessageId, (int)MessageStatus.Draft, 0, 0,0))
                    {
                        int count = dataSetNames.Tables[0].Rows.Count;
                        if (dataSetNames != null && count > 0)
                        {
                            //int x = 217;
                            int x = 190;
                            //int y = 109;
                            int y = 90;
                            for (int index = 0; index < count; index++)
                            {
                                string fileName = dataSetNames.Tables[0].Rows[index].ItemArray[0].ToString();
                                LinkLabel labelIndex = new LinkLabel();
                                labelIndex.Text = fileName;
                                labelIndex.Location = new Point(x, y);
                                labelIndex.Visible = true;
                                labelIndex.Width = fileName.Length * 8;
                                labelIndex.Name = fileName;
                                labelIndex.Tag = dataSetNames.Tables[0].Rows[index].ItemArray[2].ToString();
                                labelIndex.Height = 18;
                                labelIndex.ForeColor = Color.Blue;
                                labelIndex.Click += new EventHandler(labelIndex_Click);
                                this.Controls.Add(labelIndex);
                                labelIndex.BringToFront();
                                if (y < 145)
                                {
                                    y += 18;
                                }
                                else if (x < 487)
                                {
                                    x += 135; y = 109;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelIndex_Click(object sender, EventArgs e)
        {
            try
            {
                LinkLabel linkLabel = (LinkLabel)sender;
                AttachmentViewer attch = new AttachmentViewer();
                object a;
                if (m_MessageName.Equals(string.Empty))//if integer constructor is called(message_id present)
                {
                    a = new MessageController().GetAttachmentFile(Convert.ToInt32(linkLabel.Tag), linkLabel.Text, (int)MessageStatus.Draft, 0, 0,0);
                    attch.AttachmentContent = ASCIIEncoding.ASCII.GetString((byte[])a);
                }
               
                attch.ShowDialog();

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTo_Click(object sender, EventArgs e)
        {
            string addresses = GetAddresses();
            if ((textBoxTo.Text == string.Empty) && (addresses != null))
                textBoxTo.Text = addresses;
            else if(addresses !=null && addresses!= string.Empty)
                textBoxTo.Text = textBoxTo.Text + "; " + addresses;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCc_Click(object sender, EventArgs e)
        {
            string addresses = GetAddresses();
            if ((textBoxCc.Text == string.Empty) && (addresses != null))
                textBoxCc.Text = addresses;
            else if(addresses!=null)
                textBoxCc.Text = textBoxCc.Text + "; " + addresses;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (ValidateMessage())
            {
                try
                {
                    Outlook outlook = new Outlook();
                    using (DataSet dataSetMailSettings = MailSetup.GetMailSetupDetails())
                    {
                        if (dataSetMailSettings != null && dataSetMailSettings.Tables[0].Rows.Count == 1 && dataSetMailSettings.Tables[0].Rows[0].ItemArray[9].ToString() == string.Empty)
                        {
                            //Password field is empty
                            //Display a dailog to accept password
                            Password password = new Password();
                            password.ShowDialog();
                            //set the accepted password and username from DB in outlook object.
                            outlook.Username = dataSetMailSettings.Tables[0].Rows[0].ItemArray[7].ToString();
                            outlook.Password = password.PasswordText;
                            
                        }
                        else
                        {
                            outlook.Username = dataSetMailSettings.Tables[0].Rows[0].ItemArray[7].ToString();
                            outlook.Password = dataSetMailSettings.Tables[0].Rows[0].ItemArray[9].ToString();
                        }
                    }
                    if (outlook.Send(this))
                    {
                        MessageBox.Show("Message sent.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Sending failed.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (ArgumentNullException)
                {
                    MessageBox.Show("Outgoing server settings not available. Please create settings.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (SmtpException ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    MessageBox.Show(ex.Message, "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    MessageBox.Show("Incorrect outgoing settings. Please check and try again", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDraft_Click(object sender, EventArgs e)
        {
            try
            {
                Outlook outlook = new Outlook();
                if (outlook.SaveDraft(this))
                {
                    MessageBox.Show("Message saved to drafts", "Zed Axis", MessageBoxButtons.OK,MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Could not save to drafts", "Zed Axis", MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
                
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show("Cannot save draft. Please refer log for details.", "Zed Axis", MessageBoxButtons.OK);
            }
        }

        #endregion
    }
}