namespace DataProcessingBlocks
{
    partial class MailViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailViewer));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabelAttachment = new System.Windows.Forms.LinkLabel();
            this.richTextBoxBody = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelHeaderFrom = new System.Windows.Forms.Label();
            this.labelAddresses = new System.Windows.Forms.Label();
            this.labelCc = new System.Windows.Forms.Label();
            this.labelSubject = new System.Windows.Forms.Label();
            this.labelHeaderTo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Attachment:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 71);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Subject:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // linkLabelAttachment
            // 
            this.linkLabelAttachment.AutoSize = true;
            this.linkLabelAttachment.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelAttachment.Location = new System.Drawing.Point(259, 114);
            this.linkLabelAttachment.Name = "linkLabelAttachment";
            this.linkLabelAttachment.Size = new System.Drawing.Size(0, 13);
            this.linkLabelAttachment.TabIndex = 20;
            // 
            // richTextBoxBody
            // 
            this.richTextBoxBody.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxBody.Location = new System.Drawing.Point(8, 165);
            this.richTextBoxBody.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.richTextBoxBody.MaxLength = 2000;
            this.richTextBoxBody.Name = "richTextBoxBody";
            this.richTextBoxBody.Size = new System.Drawing.Size(641, 408);
            this.richTextBoxBody.TabIndex = 15;
            this.richTextBoxBody.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(57, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Cc:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHeaderFrom
            // 
            this.labelHeaderFrom.AutoSize = true;
            this.labelHeaderFrom.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeaderFrom.Location = new System.Drawing.Point(43, 21);
            this.labelHeaderFrom.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelHeaderFrom.Name = "labelHeaderFrom";
            this.labelHeaderFrom.Size = new System.Drawing.Size(41, 13);
            this.labelHeaderFrom.TabIndex = 23;
            this.labelHeaderFrom.Text = "From:";
            this.labelHeaderFrom.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelAddresses
            // 
            this.labelAddresses.AutoSize = true;
            this.labelAddresses.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddresses.Location = new System.Drawing.Point(102, 21);
            this.labelAddresses.MaximumSize = new System.Drawing.Size(0, 13);
            this.labelAddresses.Name = "labelAddresses";
            this.labelAddresses.Size = new System.Drawing.Size(0, 13);
            this.labelAddresses.TabIndex = 24;
            // 
            // labelCc
            // 
            this.labelCc.AutoSize = true;
            this.labelCc.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCc.Location = new System.Drawing.Point(101, 45);
            this.labelCc.Name = "labelCc";
            this.labelCc.Size = new System.Drawing.Size(0, 13);
            this.labelCc.TabIndex = 25;
            // 
            // labelSubject
            // 
            this.labelSubject.AutoSize = true;
            this.labelSubject.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubject.Location = new System.Drawing.Point(101, 70);
            this.labelSubject.Name = "labelSubject";
            this.labelSubject.Size = new System.Drawing.Size(0, 13);
            this.labelSubject.TabIndex = 26;
            // 
            // labelHeaderTo
            // 
            this.labelHeaderTo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHeaderTo.Location = new System.Drawing.Point(43, 21);
            this.labelHeaderTo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelHeaderTo.Name = "labelHeaderTo";
            this.labelHeaderTo.Size = new System.Drawing.Size(41, 13);
            this.labelHeaderTo.TabIndex = 27;
            this.labelHeaderTo.Text = "To:";
            this.labelHeaderTo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MailViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(663, 579);
            this.Controls.Add(this.labelHeaderTo);
            this.Controls.Add(this.labelSubject);
            this.Controls.Add(this.labelCc);
            this.Controls.Add(this.labelAddresses);
            this.Controls.Add(this.labelHeaderFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabelAttachment);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBoxBody);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1029, 615);
            this.MinimizeBox = false;
            this.Name = "MailViewer";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mail preview";
            this.Load += new System.EventHandler(this.MailViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelAttachment;
        public System.Windows.Forms.RichTextBox richTextBoxBody;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelHeaderFrom;
        private System.Windows.Forms.Label labelAddresses;
        private System.Windows.Forms.Label labelCc;
        private System.Windows.Forms.Label labelSubject;
        private System.Windows.Forms.Label labelHeaderTo;

    }
}