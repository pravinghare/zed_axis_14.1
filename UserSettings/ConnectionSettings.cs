﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class is used to save value of 
    /// </summary>
    public class ConnectionSettings
    {
        #region Private Members
        private ConnectionSettings connectionSettings;
      
        private string m_QBCompanyFileName;
        private string m_MYSQLConnectionString;
        private string m_MYSQLSettingFlag;
              
        #endregion
                
        #region Properties

        //QuickBooks CompanyFileName
        public string QBCompanyFileName
        {
            get { return m_QBCompanyFileName; }
            set { m_QBCompanyFileName = value; }
        }

        public string MYSQLConnectionString
        {
            get { return m_MYSQLConnectionString; }
            set { m_MYSQLConnectionString = value; }
        }

        public string MYSQLSettingFlag
        {
            get { return m_MYSQLSettingFlag; }
            set { m_MYSQLSettingFlag = value; }
        }

        #endregion

        #region Private Methods
        
       
        #endregion

        #region Public Methods

        /// <summary>
        /// Save the connection settings in Registry.
        /// </summary>
        public void SaveConnectionSettingsInRegistry(string name, string value)
        {
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                newKey = keyName.CreateSubKey("Zed Systems\\Connection");

                newKey.SetValue(name, value);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Get the connection settings from Registry.
        /// </summary>
        /// <returns></returns>
        public string GetconnectionSettingsFromRegistry(string name)
        {
            string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE\Zed Systems\LastExport");

                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == name)
                            return keyName.GetValue(name).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in GetExportDateFromReg" + ex);
            }
            return string.Empty;
        }

        #endregion
    }
}
