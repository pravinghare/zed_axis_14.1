﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security;
using EDI.Constant;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class is used to store and save user settings
    /// values on import form.
    /// </summary>
    public class ImportSettings 
    {
        #region Private Members      

        private string m_Location;
        private string m_HeaderRow;
        private string m_TextFileOption;
        private string m_MappingName;
        private string currentConnectedSoftware;
        RegistryKey keyName;

        #endregion

        #region Constructor  

        //Set ConnectedSoftware name.
        public ImportSettings(string connectedSoftware)
        {
            currentConnectedSoftware = connectedSoftware;
        }
        #endregion

        #region Properties

        //Browse file Path
        public string Location
        {
            get { return m_Location; }
            set { m_Location = value; }
        }

        //Header Row (save value as 0 or 1)
        public string HeaderRow
        {
            get { return m_HeaderRow; }
            set { m_HeaderRow = value; }
        }

        //Text File Option
        public string TextFileOption
        {
            get { return m_TextFileOption; }
            set { m_TextFileOption = value; }
        }

        //Mapping selection file name
        public string MappingName
        {
            get { return m_MappingName; }
            set { m_MappingName = value; }
        }


        #endregion

        #region Private Methods


        /// <summary>
        /// Clear previous settings value set empty value.
        /// </summary>
        /// <param name="ConnectedSoftware"></param>
        private void ResetSettingsInRegistry()
        {
        
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Save Import settings values in xml file.
        /// </summary>
        public void SaveImportSettingsInRegistry(string name, string value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (currentConnectedSoftware == Constants.QBstring)
                {
                    newKey = keyName.CreateSubKey(Constants.importQuickBooksPath);
                    newKey.SetValue(name, value);
                }
                //Axis 11 pos
                if (currentConnectedSoftware == Constants.QBPOSstring)
                {
                    newKey = keyName.CreateSubKey(Constants.importPOSPath);
                    newKey.SetValue(name, value);
                }
                //Axis 11.1 online
                if (currentConnectedSoftware == Constants.QBOnlinestring)
                {
                    newKey = keyName.CreateSubKey(Constants.importOnlinePath);
                    newKey.SetValue(name, value);
                }
               
               //Axis 10.0 Changes
                if (currentConnectedSoftware == Constants.Xerostring)
                {
                    newKey = keyName.CreateSubKey(Constants.importXeropath);
                    newKey.SetValue(name, value);
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Get import settings values saved in XML file.
        /// </summary>
        /// <returns></returns>
        public ImportSettings GetImportSettingsFromRegistry()
        {   
                       
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                
                if (currentConnectedSoftware == Constants.QBstring)
                {
                     keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.importQuickBooksPath);
                }
                //Axis 11 pos
                if (currentConnectedSoftware == Constants.QBPOSstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.importPOSPath);
                }

                //Axis 11.1 online
                if (currentConnectedSoftware == Constants.QBOnlinestring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.importOnlinePath);
                }
              
                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == Import.HeaderRow.ToString())
                            this.HeaderRow = keyName.GetValue(Import.HeaderRow.ToString()).ToString();

                        if (valueNames == Import.Location.ToString())
                            this.Location = keyName.GetValue(Import.Location.ToString()).ToString();

                        if (valueNames == Import.MappingName.ToString())
                            this.MappingName = keyName.GetValue(Import.MappingName.ToString()).ToString();

                        if (valueNames == Import.TextFileOption.ToString())
                            this.TextFileOption = keyName.GetValue(Import.TextFileOption.ToString()).ToString();
                    }
                }
                else
                {
                    this.HeaderRow = string.Empty;
                    this.Location = string.Empty;
                    this.MappingName = string.Empty;
                    this.TextFileOption = string.Empty;
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Exception in GetExportDateFromReg" + ex);
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }
            return this;
            
        }
      

        #endregion
    }
}
