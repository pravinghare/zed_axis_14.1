﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Xml;
using System.Collections.Specialized;
using Microsoft.Win32;
using System.Security;
using EDI.Constant;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class is used to store and save user settings
    /// values on Export Form.
    /// </summary>
    public class ExportSettings 
    {
        #region Private Methods     
       
        private string m_ExportDataType;
        private string m_LastExport;
        private string m_RefNumber;
        private string m_FromRefnum;
        private string m_ToRefnum;
        private string m_DateRange;
        private string m_FromDate;
        private string m_ToDate;
        private string m_PaidStatus;
        private string m_PaidStatusValue;
        private string m_NewItems;
        private string m_NewNames;

        private string m_CustomerID;
        private string m_FromCustomerID;
        private string m_ToCustomerID;
        private string m_ItemID;
        private string m_FromItemID;
        private string m_ToItemID;
        private string m_InvoiceNo;
        private string m_FromInvoiceNo;
        private string m_ToInvoiceNo;
        private string m_PurchaseOrder;
        private string m_FromPurchaseOrder;
        private string m_ToPurchaseOrder;
        
        private string currentConnectedSoftware;

        RegistryKey keyName;

        #endregion

        #region Constructor

        public ExportSettings(string ConnectedSoftware)
        {
            currentConnectedSoftware = ConnectedSoftware;
        }
        #endregion

        #region Properties

        public string ExportDataType
        {
            get { return m_ExportDataType; }
            set { m_ExportDataType = value; }
        }

        public string LastExport
        {
            get { return m_LastExport; }
            set { m_LastExport = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string FromRefnum
        {
            get { return m_FromRefnum; }
            set { m_FromRefnum = value; }
        }

        public string ToRefnum
        {
            get { return m_ToRefnum; }
            set { m_ToRefnum = value; }
        }

        public string DateRange
        {
            get { return m_DateRange; }
            set { m_DateRange = value; }
        }

        public string FromDate
        {
            get { return m_FromDate; }
            set { m_FromDate = value; }
        }

        public string ToDate
        {
            get { return m_ToDate; }
            set { m_ToDate = value; }
        }

        public string PaidStatus
        {
            get { return m_PaidStatus; }
            set { m_PaidStatus = value; }
        }

        public string PaidStatusValue
        {
            get { return m_PaidStatusValue; }
            set { m_PaidStatusValue = value; }
        }

        public string NewItems
        {
            get { return m_NewItems; }
            set { m_NewItems = value; }
        }

        public string NewNames
        {
            get { return m_NewNames; }
            set { m_NewNames = value; }
        }

        public string Customer
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }

        public string FromCustomer
        {
            get { return m_FromCustomerID; }
            set { m_FromCustomerID = value; }
        }

        public string ToCustomer
        {
            get { return m_ToCustomerID; }
            set { m_ToCustomerID = value; }
        }

        public string ItemID
        {
            get { return m_ItemID; }
            set { m_ItemID = value; }
        }

        public string FromItem
        {
            get { return m_FromItemID; }
            set { m_FromItemID = value; }
        }

        public string ToItem
        {
            get { return m_ToItemID; }
            set { m_ToItemID = value; }
        }

        public string InvoiceID
        {
            get { return m_InvoiceNo; }
            set { m_InvoiceNo = value; }
        }

        public string FromInvoice
        {
            get { return m_FromInvoiceNo; }
            set { m_FromInvoiceNo = value; }
        }

        public string ToInvoice
        {
            get { return m_InvoiceNo; }
            set { m_InvoiceNo = value; }
        }

        public string PurchaseOrder
        {
            get { return m_PurchaseOrder; }
            set { m_PurchaseOrder = value; }
        }

        public string FromPurchase
        {
            get { return m_FromPurchaseOrder; }
            set { m_FromPurchaseOrder = value; }
        }

        public string ToPurchase
        {
            get { return m_ToPurchaseOrder; }
            set { m_ToPurchaseOrder = value; }
        }
       
        #endregion

        #region Private Methods

        /// <summary>
        /// Store export type and date to Registry.
        /// </summary>
        /// <param name="ExportType"></param>
        /// <param name="date"></param>
        private void StoreLastExportDate(string exportType, string date)
        {           
           RegistryKey currentUserKey = Registry.CurrentUser;
           RegistryKey newKey;
           RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);
           if (currentConnectedSoftware == Constants.QBstring)
           {
               newKey = keyName.CreateSubKey(Constants.lastExportQuickBooksPath);
               newKey.SetValue(exportType, date);
           }
               //Axis POS
           else if (currentConnectedSoftware == Constants.QBPOSstring)
           {
               newKey = keyName.CreateSubKey(Constants.lastExportQBPOSPath);
               newKey.SetValue(exportType, date);
           }
           else if (currentConnectedSoftware == Constants.QBOnlinestring)
           {
               newKey = keyName.CreateSubKey(Constants.lastExportQBOnlinePath);
               newKey.SetValue(exportType, date);
           }
        }
       
        #endregion

        #region Public Methods

        /// <summary>
        /// Save export settings values in Registry.
        /// </summary>
        public void SaveExportSettingsInRegistry(string name, string value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (currentConnectedSoftware == Constants.QBstring)
                {
                    newKey = keyName.CreateSubKey(Constants.exportQuickBooksPath);
                    newKey.SetValue(name, value);
                }
                //Axis pos 11
                if (currentConnectedSoftware == Constants.QBPOSstring)
                {
                    newKey = keyName.CreateSubKey(Constants.exportPOSPath);
                    newKey.SetValue(name, value);
                }
                //Axis  11.1
                if (currentConnectedSoftware == Constants.QBOnlinestring)
                {
                    newKey = keyName.CreateSubKey(Constants.exportOnlinePath);
                    newKey.SetValue(name, value);
                }
               
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Get export settings values saved from registry.
        /// </summary>
        /// <returns></returns>
        public ExportSettings GetExportSettingsFromRegistry()
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;

                if (currentConnectedSoftware == Constants.QBstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportQuickBooksPath);
                }
                //Axis 11 pos
                if (currentConnectedSoftware == Constants.QBPOSstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportPOSPath);
                }

                //Axis 11.1
                if (currentConnectedSoftware == Constants.QBOnlinestring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportOnlinePath);
                }
               
                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == Export.DataType.ToString())
                            this.ExportDataType = keyName.GetValue(Export.DataType.ToString()).ToString();

                        if (valueNames == Export.LastExportDate.ToString())
                            this.LastExport = keyName.GetValue(Export.LastExportDate.ToString()).ToString();

                        if (valueNames == Export.RefNumber.ToString())
                            this.RefNumber = keyName.GetValue(Export.RefNumber.ToString()).ToString();

                        if (valueNames == Export.FromRefnum.ToString())
                            this.FromRefnum = keyName.GetValue(Export.FromRefnum.ToString()).ToString();

                        if (valueNames == Export.ToRefum.ToString())
                            this.ToRefnum = keyName.GetValue(Export.ToRefum.ToString()).ToString();

                        if (valueNames == Export.DateRange.ToString())
                            this.DateRange = keyName.GetValue(Export.DateRange.ToString()).ToString();

                        if (valueNames == Export.FromDate.ToString())
                            this.FromDate = keyName.GetValue(Export.FromDate.ToString()).ToString();

                        if (valueNames == Export.ToDate.ToString())
                            this.ToDate = keyName.GetValue(Export.ToDate.ToString()).ToString();

                        if (valueNames == Export.PaidStatus.ToString())
                            this.PaidStatus = keyName.GetValue(Export.PaidStatus.ToString()).ToString();

                        if (valueNames == Export.PaidStatusValue.ToString())
                            this.PaidStatusValue = keyName.GetValue(Export.PaidStatusValue.ToString()).ToString();

                        if (valueNames == Export.NewItems.ToString())
                            this.NewItems = keyName.GetValue(Export.NewItems.ToString()).ToString();

                        if (valueNames == Export.NewNames.ToString())
                            this.NewNames = keyName.GetValue(Export.NewNames.ToString()).ToString();

                        if (valueNames == Export.CustomerID.ToString())
                            this.Customer = keyName.GetValue(Export.CustomerID.ToString()).ToString();

                        if (valueNames == Export.FromCustomerID.ToString())
                            this.FromCustomer = keyName.GetValue(Export.FromCustomerID.ToString()).ToString();

                        if (valueNames == Export.ToCustomerID.ToString())
                            this.ToCustomer = keyName.GetValue(Export.ToCustomerID.ToString()).ToString();

                        if (valueNames == Export.ItemID.ToString())
                            this.ItemID = keyName.GetValue(Export.ItemID.ToString()).ToString();

                        if (valueNames == Export.FromItemID.ToString())
                            this.FromItem = keyName.GetValue(Export.FromItemID.ToString()).ToString();

                        if (valueNames == Export.ToItemID.ToString())
                            this.ToItem = keyName.GetValue(Export.ToItemID.ToString()).ToString();

                        if (valueNames == Export.InvoiceNo.ToString())
                            this.InvoiceID = keyName.GetValue(Export.InvoiceNo.ToString()).ToString();

                        if (valueNames == Export.FromInvoice.ToString())
                            this.FromInvoice = keyName.GetValue(Export.FromInvoice.ToString()).ToString();

                        if (valueNames == Export.ToInvoice.ToString())
                            this.ToInvoice = keyName.GetValue(Export.ToInvoice.ToString()).ToString();

                        if (valueNames == Export.PurchaseOrder.ToString())
                            this.PurchaseOrder = keyName.GetValue(Export.PurchaseOrder.ToString()).ToString();

                        if (valueNames == Export.FromPurchase.ToString())
                            this.ToPurchase = keyName.GetValue(Export.FromPurchase.ToString()).ToString();

                        if (valueNames == Export.ToPurchase.ToString())
                            this.ToPurchase = keyName.GetValue(Export.ToPurchase.ToString()).ToString();

                      }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Exception in GetExportDateFromReg" + ex);
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }
            return this;            
        }

        /// <summary>
        /// Clear previous settings value set empty value.
        /// </summary>
        /// <param name="ConnectedSoftware"></param>
        public void ResetSettingsInRegistry()
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;

                if (currentConnectedSoftware == Constants.QBstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportQuickBooksPath);
                }
                //Axis pos 11
                if (currentConnectedSoftware == Constants.QBPOSstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportPOSPath);
                }
                //Axis  11.1
                if (currentConnectedSoftware == Constants.QBOnlinestring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.exportOnlinePath);
                }
               
                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == Export.DataType.ToString())
                            this.ExportDataType = string.Empty;

                        if (valueNames == Export.LastExportDate.ToString())
                            this.LastExport = string.Empty;

                        if (valueNames == Export.RefNumber.ToString())
                            this.RefNumber = string.Empty;

                        if (valueNames == Export.FromRefnum.ToString())
                            this.FromRefnum = string.Empty;

                        if (valueNames == Export.ToRefum.ToString())
                            this.ToRefnum = string.Empty;

                        if (valueNames == Export.DateRange.ToString())
                            this.DateRange = string.Empty;

                        if (valueNames == Export.FromDate.ToString())
                            this.FromDate = string.Empty;

                        if (valueNames == Export.ToDate.ToString())
                            this.ToDate = string.Empty;

                        if (valueNames == Export.PaidStatus.ToString())
                            this.PaidStatus = string.Empty;

                        if (valueNames == Export.NewItems.ToString())
                            this.NewItems = string.Empty;

                        if (valueNames == Export.NewNames.ToString())
                            this.NewNames = string.Empty;

                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Exception in ResetSettingsInRegistry" + ex);
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }
        }

        /// <summary>
        /// This method is used to format LastExportDate.
        /// </summary>
        public void SaveLastExportDate(string exportType, DateTime date)
        {
            try
            {
                string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
                //string strTime = System.DateTime.Now.TimeOfDay.ToString();

                StoreLastExportDate(exportType, date.ToString(EDI.Constant.Constants.MySqlDateFormatSaveRegestry) + " " + strTime);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            
        }

        /// <summary>
        /// Get the LastExportDate from registry.
        /// </summary>
        /// <param name="ExportType"></param>
        /// <returns></returns>
        public DateTime GetLastExportDate(string exportType)
        {
            string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                if (currentConnectedSoftware == Constants.QBstring)
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.lastExportQuickBooksPath);
                }
                else if(currentConnectedSoftware == Constants.QBPOSstring)///Axis 11 pos 
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.lastExportQBPOSPath);
                }
                else if (currentConnectedSoftware == Constants.QBOnlinestring)///Axis 11 pos 
                {
                    keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.lastExportQBOnlinePath);
                }
                
                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == exportType)
                            return Convert.ToDateTime(keyName.GetValue(exportType));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception in GetExportDateFromReg" + ex);
            }
            return Convert.ToDateTime(DateTime.Now.ToString());
        }
        
        #endregion
    }
}
