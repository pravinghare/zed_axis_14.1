﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Security;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class contains general method for saving valus in registry and getting it.
    /// </summary>
    public class UserSettings
    {       

        #region Public Methods

        public static void StoreValueInRegistry(string name, string value, string folderName)
        {
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);
                newKey = keyName.CreateSubKey("Zed Systems\\" + folderName);

                newKey.SetValue(name, value);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Get value from registry.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="folderName"></param>
        public static string GetValueFromRegistry(string name, string folderName)
        {
            string value = string.Empty;
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey keyName;
            try
            {                
                keyName = currentUserKey.OpenSubKey(@"SOFTWARE\Zed Systems\" + folderName);               
                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == name)
                            value = keyName.GetValue(name).ToString();
                    }
                }
                return value;
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return value;
            }

        }
        
        #endregion
    }
}
