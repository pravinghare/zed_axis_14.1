﻿// ===============================================================================
// 
// CurrencyRef.cs
//
// This file contains the implementations of the currency private members , 
// Properties, Constructors and Methods for QuickBooks.
//         CurrencyRef is used for Intuit QuickBooks SDK 8.0 Mapping Schema and It is
// applied for All QuickBooks versions (Australian and UK versions also)
// Developed By : Sandeep Patil.
// Date : 3rd August , 2009
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("CurrencyRef", Namespace = "", IsNullable = false)]
    public class CurrencyRef
    {
        private string m_ListID;
        private string m_FullName;

        public CurrencyRef()
        {

        }

        public CurrencyRef(string listID, string fullName)
        {
            if (listID != string.Empty)
                this.m_ListID = listID;
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public CurrencyRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
    }
}

