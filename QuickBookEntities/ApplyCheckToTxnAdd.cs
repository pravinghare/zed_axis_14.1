using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("ApplyCheckToTxnAdd", Namespace = "", IsNullable = false)]
    public class ApplyCheckToTxnAdd
    {
         #region Private Member Variables

        private string m_TxnID;
        private string m_Amount;

        #endregion

        #region Constructors

        public ApplyCheckToTxnAdd()
        {

        }

        public ApplyCheckToTxnAdd(string TxnID)
        {
            if (TxnID != string.Empty)
                this.m_TxnID = TxnID;
        }
       
        public ApplyCheckToTxnAdd(string TxnID, string Amount)
        {
            if (TxnID != string.Empty)
                this.m_TxnID = TxnID;
            if (Amount != string.Empty)
                this.m_Amount = Amount;
        }
        #endregion

        #region Public Properties

        public string TxnID
        {
            get { return this.m_TxnID; }
            set { this.m_TxnID = value; }
        }
        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        #endregion
    }
}
