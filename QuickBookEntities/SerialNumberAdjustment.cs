﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;


namespace QuickBookEntities
{
   [XmlRootAttribute("QuantityAdjustment", Namespace = "", IsNullable = false)]
   public class SerialNumberAdjustment
    {
        private string m_AddSerialNumber;
        private string m_RemoveSerialNumber;
        private InventorySiteLocationRef m_InventorySiteLocationRef;


        public string AddSerialNumber
        {
            get { return this.m_AddSerialNumber; }
            set { this.m_AddSerialNumber = value; }
        }
        public string RemoveSerialNumber
        {
            get { return this.m_RemoveSerialNumber; }
            set { this.m_RemoveSerialNumber = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }
    }
}
