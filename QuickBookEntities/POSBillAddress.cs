﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("POSBillAddress", Namespace = "", IsNullable = false)]
   public  class POSBillAddress
    {

        #region Private Memeber Variables

        private string m_City;
        private string m_Country;
        private string m_State;
        private string m_PostalCode;       
        private string m_Street;
        private string m_Street2;


        #endregion

        #region Constructors

        public POSBillAddress()
        {

        }
        public POSBillAddress(string city, string country, string state, string postalCode, string street, string street2)
        {
            
            this.m_City = city;
            this.m_Country = country;
            this.m_State = state;
            this.m_PostalCode = postalCode;
            this.m_Street = street;
            this.m_Street2 = street2;
        }

        #endregion

        #region Public Properties
        
         public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }

        public string State
        {
            get { return this.m_State; }
            set { this.m_State = value; }
        }

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        public string Country
        {
            get { return this.m_Country; }
            set { this.m_Country = value; }
        }

        public string Street
        {
            get { return this.m_Street; }
            set { this.m_Street = value; }
        }

        public string Street2
        {
            get { return this.m_Street2; }
            set { this.m_Street2 = value; }
        }


        #endregion
    }
}
