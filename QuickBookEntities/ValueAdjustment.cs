using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("ValueAdjustment", Namespace = "" , IsNullable = false)]
    public class ValueAdjustment
    {
        #region Private Meembers

        private string m_NewQuantity;
        private string m_QuantityDifference;
        private string m_NewValue;
        private string m_ValueDifference;

        #endregion

        #region Constructor
        public ValueAdjustment()
        { 
        }       
        #endregion

        #region Public Members

        public string NewQuantity
        {
            get { return m_NewQuantity; }
            set { m_NewQuantity = value; }
        }
        public string QuantityDifference
        {
            get { return m_QuantityDifference; }
            set { m_QuantityDifference = value; }
        }
        public string NewValue
        {
            get { return m_NewValue; }
            set { m_NewValue = value; }
        }
        public string ValueDifference
        {
            get { return m_ValueDifference; }
            set { m_ValueDifference = value; }
        }
        #endregion

    }
}
