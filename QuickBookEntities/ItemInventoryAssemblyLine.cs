using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("ItemInventoryAssemblyLine", Namespace = "", IsNullable = false)]

    public class ItemInventoryAssemblyLine
    {
        #region Private Member Variables

        private ItemInventoryRef ItemInventoryRef;

        private string m_Quantity;

        #endregion

         #region Constructors

        public ItemInventoryAssemblyLine()
        {

        }
        #endregion

          #region Public Properties

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }
          #endregion

    }
}
