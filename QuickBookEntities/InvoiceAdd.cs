// ===============================================================================
// 
// Invoice.cs
//
// This file contains the implementations of the QBInvoice Class Members. 
// Developed By : Swapnil.
// Date : 12/03/2009
// Modified By : Swapnil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;

namespace Streams
{
    [XmlRootAttribute("InvoiceAdd", Namespace = "", IsNullable = false)]
    public class InvoiceAdd : BaseInvoiceOS1
    {
       
        #region Private Members

        private CustomerRef m_customerRef;
        private ShipAddress m_shipAddress;
        private ShipMethodRef m_shipMethodRef;
        private List<InvoiceLineAdd> m_invoiceLineAdd;
        //private string m_LinkToTxnID;//for Bug# 619

        #endregion

        #region Public Properties

        /// <summary>
        /// ListID of the customer
        /// </summary>
        public CustomerRef CustomerRef
        {
            get { return m_customerRef; }
            set { m_customerRef = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string TxnDate
        {
            get { return m_orderDate; }
            set { m_orderDate = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2); }
        }

        
        /// <summary>
        /// Invoice Number max length 30
        /// </summary>
        public string RefNumber
        {
            get { return m_invoiceNumber; }
            set
            {
                if (value.Length <= 30)
                {
                    m_invoiceNumber = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid PONumber length");
                }
            }
        }
      

        /// <summary>
        /// 
        /// </summary>
        /// 
        public ShipAddress ShipAddress
        {
              get { return m_shipAddress; }
                set { m_shipAddress = value; }
        }

        /// <summary>
        /// Customer PO Number max length 30
        /// </summary>
        public string PONumber
        {
            get { return m_asn; }
            set
            {
                if (value.Length <= 30)
                {
                    m_asn = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid PONumber length");
                }
            }
        }

        [XmlIgnoreAttribute()]
        public string ConsignmentRef
        {
            get
            {
               return m_consignment_number;
            }
            set
            {
                m_consignment_number = value;
            }
        }
        [XmlIgnoreAttribute()]
        public string No_of_cartons
        {
            get 
            {
                return m_carton_quantity;
            }
            set
            {
                m_carton_quantity = value;
            }
        }
        [XmlIgnoreAttribute()]
        public string No_of_pallets
        {
            get 
            {
                return m_pallet_quantity;
            }
            set 
            {
                m_pallet_quantity = value;
            }
        }
        /// <summary>
        /// Date order to be delivered
        /// </summary>
        public string ShipDate
        {
            get { return m_etd; }
            set
            {
                if (value != null)
                {
                    m_etd = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2);
                }
                else
                {
                    throw new ArgumentNullException("Ship date is null");
                }
            }
        }

        /// <summary>
        /// Mode of transport max length 30
        /// </summary>
        public ShipMethodRef ShipMethodRef
        {
            get { return m_shipMethodRef; }
            set {m_shipMethodRef = value; }
        }

        /// <summary>
        /// Action indicator A:add U:update
        /// </summary>
        [XmlIgnoreAttribute()]
        public char ActionIndicator
        {
            get { return m_actionIndicator; }
            set
            {
                if (value.Equals('A') || value.Equals('U'))
                {
                    m_actionIndicator = value;
                }
                else
                {
                    throw new ArgumentException("Invalid action indicator value");
                }
            }
        }

        /*public string LinkToTxnID
        {
            get { return m_LinkToTxnID; }
            set { m_LinkToTxnID = value; }
        }*/
        [XmlArray("InvoiceLineAddREM")]
        public List<InvoiceLineAdd> InvoiceLineAdd
        {
            get {return  m_invoiceLineAdd; }
            set { m_invoiceLineAdd = value; }
        }


        ///// <summary>
        ///// Forwarding charges
        ///// </summary>
        //public decimal ForwardingCharge
        //{
        //    get { return m_forwardingCharges; }
        //    set { m_forwardingCharges = value; }
        //}

        /// <summary>
        /// Freight charges
        /// </summary>
        [XmlIgnoreAttribute()]
        public decimal FreightCharge
        {
            get { return m_freightCharges; }
            set { m_freightCharges = value; }
        }

      
        #endregion

        #region Constructor

        internal InvoiceAdd()
        {
            m_customerRef = new CustomerRef();
            m_shipAddress = new ShipAddress();
            m_shipMethodRef = new ShipMethodRef();
            m_invoiceLineAdd = new List<InvoiceLineAdd>();
        }
        #endregion

    }
}
