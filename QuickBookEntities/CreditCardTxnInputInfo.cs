using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("CreditCardTxnInputInfo", Namespace = "", IsNullable = false)]
    public class CreditCardTxnInputInfo
    {
        #region Private member variable

        private string m_CreditCardNumber;
        private int m_ExpirationMonth;
        private int m_ExpirationYear;
        private string m_NameOnCard;
        private string m_CreditCardAddress;
        private string m_CreditCardPostalCode;
        private string m_CommercialCardCode;
        private string m_TransactionMode;
        private string m_CreditCardTxnType;
        #endregion

        #region Constructor
        public CreditCardTxnInputInfo()
        {

        }

        public CreditCardTxnInputInfo(string ccNo,int expMonth, int expYear, string nameOnCard,string ccAdd, string ccPostalCode, string comCardCode, string txnMode, string ccTxnType)
        {
            if(ccNo!=string.Empty)
                this.m_CreditCardNumber = ccNo;
            if(expMonth!=0)
                this.m_ExpirationMonth = expMonth;
            if(expYear!=0)
                this.m_ExpirationYear = expYear;
            if (nameOnCard != string.Empty) 
                this.m_NameOnCard = nameOnCard;
            if(ccAdd!=string.Empty)
                 this.m_CreditCardAddress = ccAdd;
            if (ccPostalCode != string.Empty) 
                this.m_CreditCardPostalCode = ccPostalCode;
            if(comCardCode!=string.Empty)
                this.m_CommercialCardCode = comCardCode;
            if(txnMode!=string.Empty)
                this.m_TransactionMode = txnMode;
            if(ccTxnType!=string.Empty)
                this.m_CreditCardTxnType = ccTxnType;
        }
        #endregion

        #region Public properties
        public string CreditCardNumber
        {
            get { return m_CreditCardNumber; }
            set { m_CreditCardNumber = value; }
        }
        public int ExpirationMonth
        {
            get { return m_ExpirationMonth; }
            set { m_ExpirationMonth = value; }
        }
        public int ExpirationYear
        {
            get { return m_ExpirationYear; }
            set { m_ExpirationYear = value; }
        }
        public string NameOnCard
        {
            get { return m_NameOnCard; }
            set { m_NameOnCard = value; }
        }
        public string CreditCardAddress
        {
            get { return m_CreditCardAddress; }
            set { m_CreditCardAddress = value; }
        }
        public string CreditCardPostalCode
        {
            get { return m_CreditCardPostalCode; }
            set { m_CreditCardPostalCode = value; }
        }
        public string CommercialCardCode
        {
            get { return m_CommercialCardCode; }
            set { m_CommercialCardCode = value; }
        }
        public string TransactionMode
        {
            get { return m_TransactionMode; }
            set { m_TransactionMode = value; }
        }
        public string CreditCardTxnType
        {
            get { return m_CreditCardTxnType; }
            set { m_CreditCardTxnType = value; }
        }
        #endregion

    }
}
