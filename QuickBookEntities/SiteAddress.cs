using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("SiteAddress", Namespace = "", IsNullable = false)]
    public class SiteAddress
    {
        #region Private Memeber Variables

        private string m_Addr1;
        private string m_Addr2;
        private string m_Addr3;
        private string m_Addr4;
        private string m_Addr5;
        private string m_City;
        private string m_State;
        private string m_PostalCode;
        private string m_Country;
        
        #endregion

        #region Constructors

        public SiteAddress()
        {

        }
       
        #endregion

        #region Public Properties

        public string Addr1
        {
            get { return this.m_Addr1; }
            set { this.m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return this.m_Addr2; }
            set { this.m_Addr2 = value; }
        }

        public string Addr3
        {
            get { return this.m_Addr3; }
            set { this.m_Addr3 = value; }
        }

        public string Addr4
        {
            get { return this.m_Addr4; }
            set { this.m_Addr4 = value; }
        }

        public string Addr5
        {
            get { return this.m_Addr5; }
            set { this.m_Addr5 = value; }
        }

        public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }
      

        public string State
        {
            get { return this.m_State; }
            set { this.m_State = value; }
        }        

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        public string Country
        {
            get { return this.m_Country; }
            set { this.m_Country = value; }
        }

        #endregion
    }

}
