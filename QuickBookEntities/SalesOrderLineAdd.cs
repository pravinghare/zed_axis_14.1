using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;


namespace QuickBookEntities
{
    [XmlRootAttribute("SalesOrderLineAdd", Namespace = "", IsNullable = false)]
    public class SalesOrderLineAdd
    {
        #region Private Member variable

        private ItemRef m_ItemRef;
        private string m_Desc;
        private int m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Rate;
        private string m_RatePercent;
        private PriceLevelRef m_PriceLevelRef;
        private ClassRef m_ClassRef;
        private float m_Amount;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private bool m_IsManuallyClosed;
        private string m_Other1;
        private string m_Other2;
        private DataExt m_DataExt;

        #endregion

         #region  Constructors

        public SalesOrderLineAdd()
        {
        
        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public int Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
                this.m_RatePercent = null;
            }
        }

        public string RatePercent
        {
            get { return this.m_RatePercent; }
            set
            {
                this.m_RatePercent = value;
                this.m_Rate = null;
            }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public float Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public PriceLevelRef PriceLevelRef
        {
            get { return m_PriceLevelRef; }
            set { m_PriceLevelRef = value; }
        }

        public bool IsManuallyClosed
        {
            get { return m_IsManuallyClosed; }
            set { m_IsManuallyClosed = value; }
        }
        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }

        public DataExt DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
       
        #endregion
    }
}
