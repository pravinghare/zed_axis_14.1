using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("ShipMethodRef", Namespace = "", IsNullable = false)]
    public class ShipMethodRef
    {
         #region Private Member Variables

        private string m_FullName;

        #endregion

        #region Constructors

        public ShipMethodRef()
        {

        }

        public ShipMethodRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set {
                if (value.Length > 15)
                {
                    m_FullName = value.Remove(15, (value.Length - 15));
                }
                else
                this.m_FullName = value; }
        }

        #endregion
    }
}
