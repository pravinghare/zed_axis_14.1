﻿using System;
using System.Collections.Generic;
using System.Text;
using Streams;
using System.Xml.Serialization;
using System.Collections;
using QuickBookEntities;
using System.Collections.ObjectModel;


namespace Streams
{
    [XmlRootAttribute("ItemGroupAdd", Namespace = "", IsNullable = false)]
    public class ItemGroupAdd : BaseItemGroupList
    {
        #region Private Members

        private ItemRef m_Name;
        private Collection<ItemLineAdd> m_itemLineAdd;
        //private string m_LinkToTxnID;

        #endregion

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public ItemRef ItemRef
        {
            get
            {
                return m_Name;
            }
            set { m_Name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }


        /// <summary>
        /// 
        /// </summary>
      

        [XmlArray("ItemLineAddREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_itemLineAdd; }
            set { m_itemLineAdd = value; }
        }

        #endregion

        #region  Constructor

        public ItemGroupAdd()
        {
            m_Name = new ItemRef();
            m_itemLineAdd = new Collection<ItemLineAdd>();
        }

        #endregion
    }
}
