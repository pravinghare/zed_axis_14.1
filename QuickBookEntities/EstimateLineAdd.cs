//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Xml.Serialization;
//using System.Collections.ObjectModel;

//namespace QuickBookEntities
//{
//    [XmlRootAttribute("EstimateLineAdd", Namespace = "", IsNullable = false)]
//    public class EstimateLineAdd
//    {
//        #region Private Member Variables

//        private ItemRef m_ItemRef;
//        private string m_Desc;
//        private int m_Quantity;
//        private string m_UnitOfMeasure;
//        private string m_Rate;
//        private string m_RatePercent;
//        private ClassRef m_ClassRef;
//        private float m_Amount;
//        private SalesTaxCodeRef m_SalesTaxCodeRef;
//        private  string m_MarkupRate ;
//        private  string m_MarkupRatePercent ;
//        private  PriceLevelRef m_PriceLevelRef ;
//        private OverrideItemAccountRef m_OverrideAccountRef;
//        private string m_Other1;
//        private string m_Other2;
//        private DataExt m_DataExt;

//        #endregion

//        #region  Constructors

//        public EstimateLineAdd()
//        {
        
//        }
//        #endregion

//        #region Public Properties

//        public ItemRef ItemRef
//        {
//            get { return this.m_ItemRef; }
//            set { this.m_ItemRef = value; }
//        }

//        public string Desc
//        {
//            get { return this.m_Desc; }
//            set { this.m_Desc = value; }
//        }

//        public int Quantity
//        {
//            get { return this.m_Quantity; }
//            set { this.m_Quantity = value; }
//        }

//        public string UnitOfMeasure
//        {
//            get { return this.m_UnitOfMeasure; }
//            set { this.m_UnitOfMeasure = value; }
//        }

//        public string Rate
//        {
//            get { return this.m_Rate; }
//            set { 
//                    this.m_Rate = value;
//                    this.m_RatePercent = null;
//                }
//        }

//        public string RatePercent
//        {
//            get { return this.m_RatePercent; }
//            set { 
//                    this.m_RatePercent = value;
//                    this.m_Rate = null;
//                }
//        }

//        public ClassRef ClassRef
//        {
//            get { return this.m_ClassRef; }
//            set { this.m_ClassRef = value; }
//        }

//        public float Amount
//        {
//            get { return this.m_Amount; }
//            set { this.m_Amount = value; }
//        }

//        public SalesTaxCodeRef SalesTaxCodeRef
//        {
//            get { return this.m_SalesTaxCodeRef; }
//            set { this.m_SalesTaxCodeRef = value; }
//        }

//        public string MarkUpRate
//        {
//            get { return this.m_MarkupRate; }
//            set { 
//                    this.m_MarkupRate = value;
//                    this.m_MarkupRatePercent = null;
//                    this.m_PriceLevelRef = null;
//                }
//        }

//        public string MarkUpPercent
//        {
//            get { return this.m_MarkupRatePercent; }
//            set { 
//                    this.m_MarkupRatePercent = value;
//                    this.m_MarkupRate = null;
//                    this.m_PriceLevelRef = null;
//                }
//        }

//        public PriceLevelRef PriceLevelRef
//        {
//            get { return this.m_PriceLevelRef; }
//            set { 
//                    this.m_PriceLevelRef = value;
//                    this.m_MarkupRate = null;
//                    this.m_MarkupRatePercent = null;
//                }
//        }

//        public OverrideItemAccountRef OverrideItemAccountRef
//        {
//            get { return this.m_OverrideAccountRef; }
//            set { this.m_OverrideAccountRef = value; }
//        }

//        public string Other1
//        {
//            get { return this.m_Other1; }
//            set { this.m_Other1 = value; }
//        }
            
//        public string Other2
//        {
//            get { return this.m_Other2; }
//            set { this.m_Other2 = value; }
//        }

//        public DataExt DataExt
//        {
//            get { return this.m_DataExt; }
//            set { this.m_DataExt = value; }
//        }
       
//        #endregion
//    }
//}
