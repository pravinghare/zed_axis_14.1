using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("BillAddress", Namespace = "", IsNullable = false)]
    public class BillAddress
    {
        #region Private Memeber Variables

        private string m_Addr1;
        private string m_Addr2;
        private string m_Addr3;
        private string m_Addr4;
        private string m_Addr5;
        private string m_City;
        private string m_State;
        private string m_PostalCode;
        private string m_Country;
        private string m_Note;
        //Axis 11 pos
        private string m_Street;
        private string m_Street2;

        #endregion

        #region Constructors

        public BillAddress()
        {

        }
        public BillAddress(string add1, string add2, string add3, string add4, string add5, string city, string state, string postalCode, string country, string note)
        {
            this.m_Addr1 = add1;
            this.m_Addr2 = add2;
            this.m_Addr3 = add3;
            this.m_Addr4 = add4;
            this.m_Addr5 = add5;
            this.m_City = city;
            this.m_State = state;
            this.m_Country = country;
            this.m_PostalCode = postalCode;
            this.m_Note = note;
        }

        #endregion

        #region Public Properties

        public string Addr1
        {
            get { return this.m_Addr1; }
            set { this.m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return this.m_Addr2; }
            set { this.m_Addr2 = value; }
        }

        public string Addr3
        {
            get { return this.m_Addr3; }
            set { this.m_Addr3 = value; }
        }

        public string Addr4
        {
            get { return this.m_Addr4; }
            set { this.m_Addr4 = value; }
        }

        public string Addr5
        {
            get { return this.m_Addr5; }
            set { this.m_Addr5 = value; }
        }

        public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }
      

        public string State
        {
            get { return this.m_State; }
            set { this.m_State = value; }
        }        

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        public string Country
        {
            get { return this.m_Country; }
            set { this.m_Country = value; }
        }

        public string Note
        {
            get { return this.m_Note; }
            set { this.m_Note = value; }
        }

        public string Street
        {
            get { return this.m_Street; }
            set { this.m_Street = value; }
        }

        public string Street2
        {
            get { return this.m_Street2; }
            set { this.m_Street2 = value; }
        }



        #endregion
    }

}
