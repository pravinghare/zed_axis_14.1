using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("TaxVendorRef", Namespace = "", IsNullable = false)]
    public class TaxVendorRef
    {
        #region Private Member Variables

        private string m_FullName;

        #endregion

        #region Constructors

        public TaxVendorRef()
        {

        }

        public TaxVendorRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        #endregion
    }
}
