using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("ExpenseAccountRef", Namespace = "", IsNullable = false)]
    public class ExpenseAccountRef
    {
        #region Private Member Variables

        private string m_FullName;

        #endregion

        #region Constructors

        public ExpenseAccountRef()
        {

        }

        public ExpenseAccountRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        #endregion
    }
}