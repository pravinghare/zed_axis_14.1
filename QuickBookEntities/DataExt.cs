using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("DataExt", Namespace = "", IsNullable = false)]
    public class DataExt
    {
        #region Private Member Variables

        private static DataExt m_DataExt;
        private string m_OwnerID;
        private string m_DataExtName;
        private string m_DataExtValue;

        #endregion

        #region Constructors

        public DataExt()
        {

        }

        public DataExt(string ownerID,string dataExtName, string dataExtValue)
        {
            if (ownerID != string.Empty)
                this.m_OwnerID = ownerID;
            if (dataExtName != string.Empty)
                this.m_DataExtName = dataExtName;
            if (dataExtValue != string .Empty )
                this.m_DataExtValue = dataExtValue;
        }

        #endregion

        #region Dispose Method
        public static DataExt Dispose()
        {
            m_DataExt = null;
            return null;
        }
        #endregion

        #region Public Properties

        public string OwnerID
        {
            get { return this.m_OwnerID; }
            set { this.m_OwnerID = value; }
        }

        public string DataExtName
        {
            get { return this.m_DataExtName; }
            set { this.m_DataExtName = value; }
        }

        public string DataExtValue
        {
            get { return this.m_DataExtValue; }
            set { this.m_DataExtValue = value; }
        }
        #endregion

        #region Static Methods

        /// <summary>
        /// Getting a current instance.
        /// </summary>
        /// <param name="ownerID"></param>
        /// <param name="dataExtName"></param>
        /// <param name="dataExtValue"></param>
        /// <returns></returns>
        public static DataExt GetInstance(string ownerID, string dataExtName, string dataExtValue)
        {
            if (m_DataExt == null)
                m_DataExt = new DataExt(ownerID, dataExtName, dataExtValue);
            return m_DataExt;
        }

        /// <summary>
        /// Getting an current instance.
        /// </summary>
        /// <returns></returns>
        public static DataExt GetInstance()
        {
            if (m_DataExt == null)
                m_DataExt = new DataExt();
            return m_DataExt;
        }

        
        #endregion
    }

}
