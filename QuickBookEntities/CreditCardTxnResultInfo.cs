using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("CreditCardTxnResultInfo", Namespace = "", IsNullable = false)]
    public class CreditCardTxnResultInfo
    {
        private int m_ResultCode;
        private string m_ResultMessage;
        private string m_CreditCardTransID;
        private string m_MerchantAccountNumber;
        private string m_AthorizationCode;
        private Enum m_AVSStreet;
        private Enum m_AVSZip;
        private Enum m_CardSecurityCodeMatch;
        private string m_ReconBatchID;
        private int m_PaymentGroupingCode;
        private Enum m_PaymentStatus;
        private DateTime m_TxnAuthorizationTime;
        private int m_TxnAuthorizationStamp;
        private string m_ClientTransID;

        public CreditCardTxnResultInfo()
        {
        }
        public CreditCardTxnResultInfo(int rCode,string rMsg, string ccTransID, string mActNo, string AuthCode,Enum avsStreet,
            Enum avsZip, Enum cardSecqCodeMatch, string reconBatchID, int pmntGrpCode, Enum pmntStatus, DateTime txnAuthTime,
            int txnAuthStamp, string clientTransID)
        {
           this.m_ResultCode = rCode;
           this.m_ResultMessage = rMsg;
           this.m_CreditCardTransID = ccTransID;
           this.m_MerchantAccountNumber = mActNo;
           this.m_AthorizationCode = AuthCode;
           this.m_AVSStreet = avsStreet;
           this.m_AVSZip = avsZip;
           this.m_CardSecurityCodeMatch = cardSecqCodeMatch;
           this.m_ReconBatchID = reconBatchID;
           this.m_PaymentGroupingCode = pmntGrpCode;
           this.m_PaymentStatus = pmntStatus;
           this.m_TxnAuthorizationTime = txnAuthTime;
           this.m_TxnAuthorizationStamp = txnAuthStamp;
           this.m_ClientTransID = clientTransID;
            
        }
        public int ResultCode
        {
            get { return m_ResultCode; }
            set { m_ResultCode = value; }
        }
        public string ResultMessage
        {
            get { return m_ResultMessage; }
            set { m_ResultMessage = value; }
        }
        public string CreditCardTransID
        {
            get { return m_CreditCardTransID; }
            set { m_CreditCardTransID = value; }
        }
        public string MerchantAccountNumber
        {
            get { return m_MerchantAccountNumber; }
            set { m_MerchantAccountNumber = value; }
        }

        public string AthorizationCode
        {
            get { return m_AthorizationCode; }
            set { m_AthorizationCode = value; }
        }

        public Enum AVSStreet
        {
            get { return m_AVSStreet; }
            set { m_AVSStreet = value; }
        }
        public Enum AVSZip
        {
            get { return m_AVSZip; }
            set { m_AVSZip = value; }
        }
        public Enum CreditSecurityCodeMatch
        {
            get { return m_CardSecurityCodeMatch; }
            set { m_CardSecurityCodeMatch = value; }
        }

        public string ReconBatchID
        {
            get { return m_ReconBatchID; }
            set { m_ReconBatchID = value; }
        }

        public int PaymentGroupingCode
        {
            get { return m_PaymentGroupingCode; }
            set { m_PaymentGroupingCode = value; }
        }
        public Enum PaymentStatus
        {
            get { return m_PaymentStatus; }
            set { m_PaymentStatus = value; }
        }

        public DateTime TxnAuthorizationTime
        {
            get { return m_TxnAuthorizationTime; }
            set { m_TxnAuthorizationTime = value; }
        }

        public int TxnAuthorizationStamp
        {
            get { return m_TxnAuthorizationStamp; }
            set { m_TxnAuthorizationStamp = value; }
        }

        public string ClientTransID
        {
            get { return m_ClientTransID; }
            set { m_ClientTransID = value; }
        }
    }
}
