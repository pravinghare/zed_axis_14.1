﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("Phones", Namespace = "", IsNullable = false)]
   public class Phones
    {
        #region Private Memeber Variables

        private string m_PhoneType;
        private string m_PhoneNumber;
        private string m_PhoneAreaCode;
        private string m_PhoneCountryCode;
        #endregion
       
        #region Constructors

        public Phones()
        {

        }
        public Phones(string PhoneType, string PhoneNumber, string PhoneAreaCode, string PhoneCountryCode)
        {
            this.m_PhoneType = PhoneType;
            this.m_PhoneNumber = PhoneNumber;
            this.m_PhoneAreaCode = PhoneAreaCode;
            this.m_PhoneCountryCode = PhoneCountryCode;
           
        }

        #endregion

        #region Public Properties

        public string PhoneType
        {
            get { return this.m_PhoneType; }
            set { this.m_PhoneType = value; }
        }

        public string PhoneNumber
        {
            get { return this.m_PhoneNumber; }
            set { this.m_PhoneNumber = value; }
        }

        public string PhoneAreaCode
        {
            get { return this.m_PhoneAreaCode; }
            set { this.m_PhoneAreaCode = value; }
        }

        public string PhoneCountryCode
        {
            get { return this.m_PhoneCountryCode; }
            set { this.m_PhoneCountryCode = value; }
        }

        #endregion
    }
}
