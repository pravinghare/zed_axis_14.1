﻿// ===============================================================================
// 
// BarCode.cs
//
// This file contains BarCode class to be used in Item Service, Item Non Inventory,
// Item Other Charge, Item Inventory, Item Fixed Asset, Item Discount, Item Payment
// and Item Inventory Assembly.
// Barcode is introduced for QuickBooks 13.0 it is supported by QBSDK12.0
// Written for Axis 10.2
//
// Date : 6/6/2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("BarCode", Namespace = "", IsNullable = false)]
    public class BarCode
    {
        #region Private Member Variables

        private string m_BarCodeValue;
        private bool m_AssignEvenIfUsed;
        private bool m_AllowOverride;

        #endregion

        #region Public Properties

        public string BarCodeValue
        {
            get { return this.m_BarCodeValue; }
            set { this.m_BarCodeValue = value; }
        }
        public bool AssignEvenIfUsed
        {
            get { return this.m_AssignEvenIfUsed; }
            set { this.m_AssignEvenIfUsed = value; }
        }
        public bool AllowOverride
        {
            get { return this.m_AllowOverride; }
            set { this.m_AllowOverride = value; }
        }
        #endregion

        #region Constructors

        public BarCode()
        {

        }
        public BarCode(string bc_value)
        {
            if (bc_value != string.Empty)
            {
                this.m_BarCodeValue = bc_value;
            }
            this.m_AssignEvenIfUsed = true;
            this.m_AllowOverride = true;
        }
        public BarCode(string bc_value,bool assign_value,bool allow_value)
        {
            if (bc_value != string.Empty)
            {
                this.m_BarCodeValue = bc_value;
            }
            this.m_AssignEvenIfUsed = assign_value;
            this.m_AllowOverride = allow_value;
        }

        #endregion
    }
}
