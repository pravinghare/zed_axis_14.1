﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;


namespace QuickBookEntities
{
   [XmlRootAttribute("LotNumberAdjustment", Namespace = "", IsNullable = false)]
   public class LotNumberAdjustment
    {
        private string m_LotNumber;
        private string m_CountAdjustment;
        private InventorySiteLocationRef m_InventorySiteLocationRef;

        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }

        public string CountAdjustment
        {
            get { return this.m_CountAdjustment; }
            set { this.m_CountAdjustment = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }

      

    }
}
