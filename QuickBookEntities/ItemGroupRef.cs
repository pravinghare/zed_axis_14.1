using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("ItemGroupRef", Namespace = "", IsNullable = false)]
    public class ItemGroupRef
    {
        #region Private Member Variables

        private string m_FullName;
		//551
        private string m_itemGroupAdd;

        #endregion

        #region Constructors

        public ItemGroupRef()
        {

        }

        public ItemGroupRef(string fullName)
        {
           if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        #endregion
    }

}
