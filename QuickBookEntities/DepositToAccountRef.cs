using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("DepositToAccountRef", Namespace = "", IsNullable = false)]
    public class DepositToAccountRef
    {
        private string m_FullName;

        public DepositToAccountRef()
        {

        }
        public DepositToAccountRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

    }
}
