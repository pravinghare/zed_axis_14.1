using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("AssetAccountRef", Namespace = "", IsNullable = false)]
    public class AssetAccountRef
    {
        #region Private Member Variables

        private string m_FullName;

        #endregion

        #region Constructors

        public AssetAccountRef()
        {

        }

        public AssetAccountRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        #endregion
    }
}
