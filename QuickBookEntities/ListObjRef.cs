using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("ListObjRef", Namespace = "", IsNullable = false)]
    public class ListObjRef
    {
        #region Private Members

        private string m_ListId;
        private string m_FullName;

        #endregion

        #region Constructor
        
        public ListObjRef()
        {
        }

        #endregion

        #region Public Properties

        public string ListID
        {
            get { return m_ListId; }
            set { m_ListId = value; }
        }

        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }

        #endregion
    }
}
