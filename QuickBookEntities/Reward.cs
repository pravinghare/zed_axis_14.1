﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace  QuickBookEntities
{
    

    [XmlRootAttribute("Reward", Namespace = "", IsNullable = false)]
    public class Reward
    {
        #region Private Member Variables

       // private static Reward m_Reward;
        private string m_RewardAmount ;
        private string m_RewardPercent ;
        private string m_EarnedDate ;
        private string m_MatureDate; 
        private string m_ExpirationDate ;
        #endregion

        #region Constructors
        public Reward()
        {
        }
        #endregion

      #region Public Properties

        public string RewardAmount
        {
            get { return this.m_RewardAmount; }
            set { this.m_RewardAmount = value; }
        }

        public string RewardPercent
        {
            get { return this.m_RewardPercent; }
            set { this.m_RewardPercent = value; }
        }

        public string EarnedDate
        {
            get { return this.m_EarnedDate; }
            set { this.m_EarnedDate = value; }
        }
        public string MatureDate
        {
            get { return this.m_MatureDate; }
            set { this.m_MatureDate = value; }
        }
        public string ExpirationDate
        {
            get { return this.m_ExpirationDate; }
            set { this.m_ExpirationDate = value; }
        } 
        #endregion

     
    }
}
