using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;


namespace QuickBookEntities
{
  
   [XmlRootAttribute("DiscountLineAdd", Namespace = "", IsNullable = false)]
    public class DiscountLineAdd
    {
        #region Private Memeber Variables

        private string m_amount;
        //private string m_ratepercent;
        private string m_istaxable;
        private AccountRef m_AccountRef;
        #endregion

        #region Constructors

        public DiscountLineAdd()
        {

        }
        //public DiscountLineRet(string Amount, string IsTaxable, string FullName)
        //{
        //    this.m_amount = Amount;
        //    //this.m_ratepercent = ratepercent;
        //    this.m_istaxable = IsTaxable;
        //    this.AccountRef.FullName = FullName;

        //}

        #endregion

        #region Public Properties

        public string Amount
        {
            get { return this.m_amount; }
            set { this.m_amount = value; }
        }

        //public string ratepercent
        //{
        //    get { return this.m_ratepercent; }
        //    set { this.m_ratepercent = value; }
        //}

        public string IsTaxable
        {
            get { return this.m_istaxable; }
            set { this.m_istaxable = value; }
        }

        //public AccountRef AccountRef
        //{
        //    get { return this.m_AccountRef; }
        //    set { this.m_AccountRef = value; }
        //}
        public AccountRef AccountRef
        {
            get
            {
                return m_AccountRef;
            }
            set
            {
                m_AccountRef = value;
            }
        }

        #endregion

    }
}
