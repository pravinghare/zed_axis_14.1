using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;

namespace Streams
{
    [XmlRootAttribute("InvoiceLineAdd", Namespace = "", IsNullable = false)]
    public class InvoiceLineAdd : BaseInvoiceOS2
    {
        #region Private members

        private ItemRef m_itemRef;
        private string m_Rate;

        #endregion 
        
        #region Public Properties

        /// <summary>
        /// Product name max length 25
        /// </summary>
        public ItemRef ItemRef
        {
            get { return m_itemRef; }
            set { m_itemRef = value; }
        }

        /// <summary>
        /// Product description max value 4095
        /// </summary>
        public string Desc
        {
            get { return m_description; }
            set
            {
                if (value.Length <= 4095)
                {
                    m_description = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid Description length.");
                }
            }
        }


        /// <summary>
        /// Product quantity max value 999999999
        /// </summary>
        public int Quantity
        {
            get { return m_quantityShipped; }
            set
            {
                if (value <= 999999999)
                {
                    m_quantityShipped = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Invalid Quantity length");
                }
            }
        }


        /// <summary>
        /// Rate
        /// </summary>
        public string Rate
        {
            get
            {
                return m_Rate;
            }
            set
            {
                m_Rate = value;
            }
        }
     
        #endregion

        #region Constructor

        public InvoiceLineAdd()
        {
            m_itemRef = new ItemRef();
        }

        #endregion

    }
}
