using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("FixedAssetSalesInfo", Namespace = "", IsNullable = false)]
    public class FixedAssetSalesInfo
    {
        #region Private member variable

        private string m_SalesDesc;
        private string m_SalesDate;
        private string m_SalesPrice;
        private string m_SalesExpense;
        
        #endregion

        #region Constructor
        public FixedAssetSalesInfo()
        {

        }

        public FixedAssetSalesInfo(string salesDesc, string salesDate, string salesPrice, string salesExpense)
        {
            if (salesDesc != string.Empty)
                this.m_SalesDesc = salesDesc;

            if (salesDate != string.Empty)
                this.m_SalesDate = salesDate;

            if (salesPrice != string.Empty)
                this.m_SalesPrice = salesPrice;

            if (salesExpense != string.Empty)
                this.m_SalesExpense = salesExpense;
        }
        #endregion

        #region Public properties

        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        [XmlElement(DataType = "string")]
        public string SalesDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_SalesDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_SalesDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_SalesDate = value;
            }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }
        public string SalesExpense
        {
            get { return m_SalesExpense; }
            set { m_SalesExpense = value; }
        }
      
        #endregion

    }
}
