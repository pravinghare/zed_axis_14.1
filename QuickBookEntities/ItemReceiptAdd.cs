using System;
using System.Collections.Generic;
using System.Text;
using Streams;
using System.Xml.Serialization;
using System.Collections;
using QuickBookEntities;
using System.Collections.ObjectModel;


namespace Streams
{
    [XmlRootAttribute("ItemReceiptAdd", Namespace = "", IsNullable = false)]
    public class ItemReceiptAdd:BaseItemReceipt
    {
        #region Private Members

        private VendorRef m_vendorRef;
        private Collection<ItemLineAdd> m_itemLineAdd;
        //private string m_LinkToTxnID;

        #endregion

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public VendorRef VendorRef
        {
            get
            {
               return m_vendorRef;
            }
            set { m_vendorRef = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string TxnDate
        {
            get { return m_dateReceived; }
            set { m_dateReceived = value.Substring(0, 4) + "-" + value.Substring(4, 2) + "-" + value.Substring(6, 2); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string RefNumber
        {
            get { return m_purchaseOrdNumber; }
            set { m_purchaseOrdNumber = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        public string Memo
        {
            get { return m_remarks; }
            set { m_remarks = value; }
        }

        //public string LinkToTxnID
        //{
        //    get { return m_LinkToTxnID; }
        //    set { m_LinkToTxnID = value; }
        //}

        [XmlArray("ItemLineAddREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_itemLineAdd; }
            set { m_itemLineAdd = value; }
        }

        #endregion

        #region  Constructor

        public ItemReceiptAdd()
        {
            m_vendorRef = new VendorRef();
            m_itemLineAdd = new Collection<ItemLineAdd>();
        }

        #endregion
    }
}
