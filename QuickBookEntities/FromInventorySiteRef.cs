using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("FromInventorySiteRef", Namespace = "", IsNullable = false)]
     public class FromInventorySiteRef
    {
        #region Private Member Variables

        private string m_FullName;
        private string m_ListID;

        #endregion

        #region Constructors

        public FromInventorySiteRef()
        {

        }

         public FromInventorySiteRef(string fullName)
        {
           if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { m_FullName = value; }
        }

        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        #endregion
    }
}
