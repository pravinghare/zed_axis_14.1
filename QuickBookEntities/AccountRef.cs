using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("AccountRef", Namespace = "", IsNullable = false)]
    public class AccountRef
    {
        private string m_ListID;
        private string m_FullName;

        public AccountRef()
        {

        }

        public AccountRef(string listID, string fullName)
        {
            if (listID != string.Empty)
                this.m_ListID = listID;
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public AccountRef(string fullName)
        {
             if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
    }
}
