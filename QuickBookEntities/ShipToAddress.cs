﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("ShipToAddress", Namespace = "", IsNullable = false)]
    public class ShipToAddress
    {
        #region Private Memeber Variables

        private string m_Name;
        private string m_Addr1;
        private string m_Addr2;
        private string m_Addr3;
        private string m_Addr4;
        private string m_Addr5;
        private string m_City;
        private string m_State;
        private string m_PostalCode;
        private string m_Country;
        private string m_Note;         
        private string m_DefaultShipTo;
      
        #endregion

        #region Constructors

        public ShipToAddress()
        {

        }
        public ShipToAddress(string name, string add1, string add2, string add3, string add4, string add5, string city, string state, string postalCode, string country, string note,string defaultshipTo)
        {
            this.m_Addr1 = add1;
            this.m_Addr2 = add2;
            this.m_Addr3 = add3;
            this.m_Addr4 = add4;
            this.m_Addr5 = add5;
            this.m_City = city;
            this.m_State = state;
            this.m_Country = country;
            this.m_PostalCode = postalCode;
            this.m_Note = note;
            this.m_Name = name;
            this.m_DefaultShipTo = defaultshipTo;
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return this.m_Name; }
            set { this.m_Name = value; }
        }
        
        public string Addr1
        {
            get { return this.m_Addr1; }
            set { this.m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return this.m_Addr2; }
            set { this.m_Addr2 = value; }
        }

        public string Addr3
        {
            get { return this.m_Addr3; }
            set { this.m_Addr3 = value; }
        }

        public string Addr4
        {
            get { return this.m_Addr4; }
            set { this.m_Addr4 = value; }
        }

        public string Addr5
        {
            get { return this.m_Addr5; }
            set { this.m_Addr5 = value; }
        }

        public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }

        public string State
        {
            get { return this.m_State; }
            set { this.m_State = value; }
        }

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        public string Country
        {
            get { return this.m_Country; }
            set { this.m_Country = value; }
        }

        public string Note
        {
            get { return this.m_Note; }
            set { this.m_Note = value; }
        }

        //Axis11 pos
       
        public string DefaultShipTo
        {
            get { return this.m_DefaultShipTo; }
            set { this.m_DefaultShipTo = value; }
        }

   

        #endregion
    }
}
