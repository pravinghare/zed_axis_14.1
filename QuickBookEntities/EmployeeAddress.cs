using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("EmployeeAddress", Namespace = "", IsNullable = false)]
    public class EmployeeAddress
    {
        #region Private Memeber Variables

        private string m_Addr1;
        private string m_Addr2;
        private string m_City;
        private string m_State;
        private string m_PostalCode;
        
        #endregion

        #region Constructors

        public EmployeeAddress()
        {

        }
        public EmployeeAddress(string add1, string add2, string add3, string add4, string add5, string city, string state, string postalCode, string country, string note)
        {
            this.m_Addr1 = add1;
            this.m_Addr2 = add2;
            this.m_City = city;
            this.m_State = state;
            this.m_PostalCode = postalCode;
        }

        #endregion

        #region Public Properties

        public string Addr1
        {
            get { return this.m_Addr1; }
            set { this.m_Addr1 = value; }
        }

        public string Addr2
        {
            get { return this.m_Addr2; }
            set { this.m_Addr2 = value; }
        }

        
        public string City
        {
            get { return this.m_City; }
            set { this.m_City = value; }
        }

        public string State
        {
            get { return this.m_State; }
            set { this.m_State = value; }
        }

        public string PostalCode
        {
            get { return this.m_PostalCode; }
            set { this.m_PostalCode = value; }
        }

        

        #endregion
    }

}
