﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("ShippingLineAdd", Namespace = "", IsNullable = false)]

   public class ShippingLineAdd
    {
        
        #region Private Memeber Variables

        private string m_amount;
        //private string m_ratepercent;
        //private string m_istaxable;
        private AccountRef m_AccountRef;
        #endregion


         #region Constructors

        public ShippingLineAdd()
        {

        }
          #endregion
       
        #region Public Properties

        public string Amount
        {
            get { return this.m_amount; }
            set { this.m_amount = value; }
        }

        public AccountRef AccountRef
        {
            get
            {
                return m_AccountRef;
            }
            set
            {
                m_AccountRef = value;
            }
        }
        #endregion
    }
}
