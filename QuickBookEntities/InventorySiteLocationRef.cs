﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("InventorySiteLocationRef", Namespace = "", IsNullable = false)]
    public class InventorySiteLocationRef
    {
         #region Private Member Variables

        private string m_FullName;

        #endregion

        #region Constructors

        public InventorySiteLocationRef()
        {

        }

        public InventorySiteLocationRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        #endregion

    }
}
