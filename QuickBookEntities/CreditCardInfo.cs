using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("CreditCardInfo", Namespace = "", IsNullable = false)]
    public class CreditCardInfo
    {
        #region Private member variable

        private string m_CreditCardNumber;
        private string m_ExpirationMonth;
        private string m_ExpirationYear;
        private string m_NameOnCard;
        private string m_CreditCardAddress;
        private string m_CreditCardPostalCode;
        #endregion

        #region Constructor
        public CreditCardInfo()
        {

        }

        public CreditCardInfo(string ccNo, string expMonth, string expYear, string nameOnCard, string ccAdd, string ccPostalCode)
        {
            if (ccNo != string.Empty)
                this.m_CreditCardNumber = ccNo;
            if (expMonth != string.Empty)
                this.m_ExpirationMonth = expMonth;
            if (expYear != string.Empty)
                this.m_ExpirationYear = expYear;
            if (nameOnCard != string.Empty)
                this.m_NameOnCard = nameOnCard;
            if (ccAdd != string.Empty)
                this.m_CreditCardAddress = ccAdd;
            if (ccPostalCode != string.Empty)
                this.m_CreditCardPostalCode = ccPostalCode;
        }
        #endregion

        #region Public properties
        public string CreditCardNumber
        {
            get { return m_CreditCardNumber; }
            set { m_CreditCardNumber = value; }
        }
        public string ExpirationMonth
        {
            get { return m_ExpirationMonth; }
            set { m_ExpirationMonth = value; }
        }
        public string ExpirationYear
        {
            get { return m_ExpirationYear; }
            set { m_ExpirationYear = value; }
        }
        public string NameOnCard
        {
            get { return m_NameOnCard; }
            set { m_NameOnCard = value; }
        }
        public string CreditCardAddress
        {
            get { return m_CreditCardAddress; }
            set { m_CreditCardAddress = value; }
        }
        public string CreditCardPostalCode
        {
            get { return m_CreditCardPostalCode; }
            set { m_CreditCardPostalCode = value; }
        }
        
        #endregion

    }
}
