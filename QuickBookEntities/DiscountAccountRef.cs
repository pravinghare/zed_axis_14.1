using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("DiscountAccountRef", Namespace = "", IsNullable = false)]
    public class DiscountAccountRef
    {
        private string m_ListID;
        private string m_FullName;

        public DiscountAccountRef()
        {

        }

        public DiscountAccountRef(string listID, string fullName)
        {
            if (listID != string.Empty)
                this.m_ListID = listID;
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public DiscountAccountRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
    }
}
