using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("ItemRef", Namespace = "", IsNullable = false)]
    public class ItemRef
    {
        #region Private Member Variables

        private string m_FullName;
        private string m_ListID;

        #endregion

        #region Constructors

        public ItemRef()
        {

        }

        public ItemRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        public ItemRef(string fullName, string listId)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
            if (listId != string.Empty)
                this.m_ListID = listId;
        }
        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }

        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }
        
        #endregion
    }

}
