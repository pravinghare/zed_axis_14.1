using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("SalesTaxReturnRef", Namespace = "", IsNullable = false)]
    public class SalesTaxReturnRef
    {
        #region Private Member Variables

        private string m_FullName;
        private string m_IsTaxable;


        #endregion

        #region Constructors

        public SalesTaxReturnRef()
        {

        }

        public SalesTaxReturnRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }

        #endregion

        #region Public Properties

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
       
        #endregion
    }
}
