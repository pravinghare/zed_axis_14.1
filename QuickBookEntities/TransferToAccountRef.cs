﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("TransferToAccountRef", Namespace = "", IsNullable = false)]
  public   class TransferToAccountRef    {
    
        private string m_ListID;
        private string m_FullName;

        public TransferToAccountRef()
        {

        }

        public TransferToAccountRef(string listID, string fullName)
        {
            if (listID != string.Empty)
                this.m_ListID = listID;
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public TransferToAccountRef(string fullName)
        {
            
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }



        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
    }
}
