using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("LinkToTxn", Namespace = "", IsNullable = false)]
    public class LinkToTxn
    {
        #region Private Member Variables
        private string m_TxnID = "";
        private string m_TxnLineID = "";

        #endregion

        public LinkToTxn()
        {

        }

        public LinkToTxn(string txnID, string txnLineID)
        {
            if (txnID != string.Empty)
                this.m_TxnID = txnID;
            if (txnLineID != string.Empty)
                this.m_TxnLineID = txnLineID;
        }



        public string TxnID
        {
            get { return this.m_TxnID; }
            set { this.m_TxnID = value; }
        }

        public string TxnLineID
        {
            get { return this.m_TxnLineID; }
            set { this.m_TxnLineID = value; }
        }

    }
}
