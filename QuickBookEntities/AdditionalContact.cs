﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("AdditionalContact", Namespace = "", IsNullable = false)]
    public class AdditionalContactRef
    {
        #region Private Memeber Variables
        private string m_contactName;
        private string m_contactValue;

        #endregion

        #region Constructors

        public AdditionalContactRef()
        {

        }
        public AdditionalContactRef(string contactName, string contactValue)
        {
            this.m_contactName = contactName;
            this.m_contactValue = contactValue;
          
        }

        #endregion

        #region Public Properties

        public string ContactName
        {
            get { return this.m_contactName; }
            set { this.m_contactName = value; }
        }

        public string ContactValue
        {
            get { return this.m_contactValue; }
            set { this.m_contactValue = value; }
        }


        #endregion
    }
}
