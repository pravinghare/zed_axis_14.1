using System;
using System.Collections.Generic;
using System.Text;
using Streams;
using System.Xml.Serialization;
using QuickBookEntities;

namespace Streams
{
    [XmlRootAttribute("ItemLineAdd", Namespace = "", IsNullable = false)]
    public class ItemLineAdd:BaseItemReceipt
    {
        #region Private Members

        private ItemRef m_itemRef;

        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return m_itemRef; }
            set { m_itemRef = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Desc
        {
            get { return m_description; }
            set { m_description = value; }
        }

           /// <summary>
        /// 
        /// </summary>
        public int Quantity
        {
            get { return m_totalQunatityRec; }
            set
            {
                if (value < 999999999)
                {
                    m_totalQunatityRec = value;
                }
                else
                {
                    throw new Exception("Invalid quantity");
                }
            }
        }

        #endregion

        #region Constructor

        public ItemLineAdd()
        {
            m_itemRef = new ItemRef();
        }
        #endregion
    }
}
