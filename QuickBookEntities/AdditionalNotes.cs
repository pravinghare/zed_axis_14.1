﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QuickBookEntities
{
    [XmlRootAttribute("AdditionalNotes", Namespace = "", IsNullable = false)]
    public class AdditionalNotes
    {
        #region Private Member Variables

        private string m_Note;

        #endregion

        #region Constructors

        public AdditionalNotes()
        {

        }

        public AdditionalNotes(string note)
        {
            if (note != string.Empty)
                this.m_Note = note;
        }

        #endregion

        #region Public Properties

        public string Note
        {
            get { return this.m_Note; }
            set { this.m_Note = value; }
        }

        #endregion
    }
}
