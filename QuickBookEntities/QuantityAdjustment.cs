using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace QuickBookEntities
{
    [XmlRootAttribute("QuantityAdjustment", Namespace = "", IsNullable = false)]
    public class QuantityAdjustment
    {
        #region Private Members

        private string m_NewQuantity;
        //Axis 648
        private string m_QuantityDifference;

        private InventorySiteLocationRef m_InventorySiteLocationRef;

       

        // axis 10.0 chnges
        private string m_SerialNumber;
        private string m_LotNumber;
        // axis 10.0 chnges ends
        
        #endregion

        #region Constructor

        public QuantityAdjustment()
        { 
        }             

        #endregion

        #region Public Properties

        public string NewQuantity
        {
            get { return this.m_NewQuantity; }
            set { this.m_NewQuantity = value; }
        }

        // bug 496

            //Axis 648 
        public string QuantityDifference
        {
            get { return this.m_QuantityDifference; }
            set {this.m_QuantityDifference = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }
        //Axis 648 ends 

        // axis 10.0 changes
        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }

        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }
        // axis 10.0 changes



        #endregion
    }
}
