/*
SQLyog Community Edition- MySQL GUI v8.0 
MySQL - 5.0.45-community-nt : Database - edi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`edi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `edi`;

/*Table structure for table `export_filter` */

DROP TABLE IF EXISTS `export_filter`;

CREATE TABLE `export_filter` (
  `FilterID` int(11) NOT NULL auto_increment,
  `FilterDataType` varchar(20) default NULL,
  `FilterDate` datetime default NULL,
  PRIMARY KEY  (`FilterID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `export_filter` */



DROP TABLE IF EXISTS `mail_attachment`;

CREATE TABLE `mail_attachment` (
  `AttachID` int(11) unsigned NOT NULL auto_increment,
  `AttachName` varchar(20) default NULL,
  `AttachFile` longblob default NULL,
  `MessageId` int(11) default NULL,
  `Status` varchar(25) default NULL,
  PRIMARY KEY (AttachID)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


/*Table structure for table `mail_protocol_schema` */

DROP TABLE IF EXISTS `mail_protocol_schema`;

CREATE TABLE `mail_protocol_schema` (
  `MailProtocolID` smallint(5) unsigned NOT NULL auto_increment,
  `MailProtocol` varchar(50) default NULL,
  PRIMARY KEY  (`MailProtocolID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mail_protocol_schema` */

insert  into `mail_protocol_schema`(`MailProtocolID`,`MailProtocol`) values (1,'POP3/Email'),(2,'SMTP'),(3,'FTP');

/*Table structure for table `mail_setup_schema` */

DROP TABLE IF EXISTS `mail_setup_schema`;

CREATE TABLE `mail_setup_schema` (
  `MailSetupID` varchar(50) NOT NULL,
  `MailProtocolID` smallint(5) unsigned NOT NULL,
  `InPortNo` smallint(5) unsigned default NULL,
  `OutPortNo` smallint(5) unsigned default NULL,
  `IncommingServer` varchar(100) default NULL,
  `OutgoingServer` varchar(100) default NULL,
  `EmailID` varchar(100) default NULL,
  `UserName` varchar(100) default NULL,
  `Password` varchar(100) default NULL,
  PRIMARY KEY  (`MailSetupID`),
  KEY `FK_mailsetupschema` (`MailProtocolID`),
  CONSTRAINT `FK_mailsetupschema` FOREIGN KEY (`MailProtocolID`) REFERENCES `mail_protocol_schema` (`MailProtocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mail_setup_schema` */

insert  into `mail_setup_schema`(`MailSetupID`,`MailProtocolID`,`InPortNo`,`OutPortNo`,`IncommingServer`,`OutgoingServer`,`EmailID`,`UserName`,`Password`) values ('81ea8e8645',1,110,110,'mail.omni-bridge.com','mail.omni-bridge.com','gouraw@omni-bridge.com','gouraw','¸OßõgÜ/Ñ`NVï¿N');

/*Table structure for table `message_schema` */

DROP TABLE IF EXISTS `message_schema`;

CREATE TABLE `message_schema` (
  `MessageID` int(11) NOT NULL auto_increment,
  `MessageDate` datetime NOT NULL,
  `ArchiveDate` datetime default NULL,
  `FromAddress` varchar(100) default NULL,
  `ToAddress` varchar(400) default NULL,
  `RefMessageID` int(11) default NULL,
  `Subject` varchar(255) default NULL,
  `Body` varchar(2000) default NULL,
  `Status` varchar(25) default NULL,
  `AttachFile` blob,
  `AttachName` varchar(20) default NULL,
  PRIMARY KEY  (`MessageID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `message_schema` */

insert  into `message_schema`(`MessageID`,`MessageDate`,`ArchiveDate`,`FromAddress`,`ToAddress`,`RefMessageID`,`Subject`,`Body`,`Status`,`AttachFile`,`AttachName`) values (4,'2009-02-18 02:52:49','2009-02-18 02:52:49','swapnil@gmail.com','swapnil@yahoo.com',1,'test','Thsi is a test message','Recieved','','test.txt');

/*Table structure for table `message_status_schema` */

DROP TABLE IF EXISTS `message_status_schema`;

CREATE TABLE `message_status_schema` (
  `StatusID` smallint(5) unsigned NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Desc` varchar(100) default NULL,
  PRIMARY KEY  (`StatusID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `message_status_schema` */

insert  into `message_status_schema`(`StatusID`,`Name`,`Desc`) values (1,'Draft','\"Draft\"-message created but not sent.'),(2,'NotRecognized','\"NotRecognized\"-message unrecognized.'),(3,'Received','\"Received\"-message has been received.'),(4,'Sent','\"Sent\"-message has been sent.'),(5,'Processed','\"Processed\"-message has been proceesed.'),(6,'Archived','\"Archived\"-message is no longer active.');

/*Table structure for table `trading_partner_schema` */

DROP TABLE IF EXISTS `trading_partner_schema`;

CREATE TABLE `trading_partner_schema` (
  `TradingPartnerID` varchar(50) NOT NULL,
  `TPName` varchar(100) default NULL,
  `MailProtocolID` smallint(5) unsigned NOT NULL default '0',
  `EmailID` varchar(100) default NULL,
  `AlternateEmailID` varchar(100) default NULL,
  `ClientID` varchar(100) default NULL,
  `XSDLocation` varchar(100) default NULL,
  PRIMARY KEY  (`TradingPartnerID`),
  KEY `FK_tradingpartnerschema` (`MailProtocolID`),
  CONSTRAINT `FK_tradingpartnerschema` FOREIGN KEY (`MailProtocolID`) REFERENCES `mail_protocol_schema` (`MailProtocolID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trading_partner_schema` */

insert  into `trading_partner_schema`(`TradingPartnerID`,`TPName`,`MailProtocolID`,`EmailID`,`AlternateEmailID`,`ClientID`,`XSDLocation`) values ('677d33acf9','kuar',1,'kd@hh.com','','dj','');

/*Table structure for table `message_rule` */

DROP TABLE IF EXISTS `message_rule`;

CREATE TABLE `message_rule` (
        `RulesId` int(11) NOT NULL auto_increment,      
                `Name` varchar(50) NOT NULL,                    
                `TradingPartnerID` varchar(50) NOT NULL,        
                `CondSubEquals` char(50) default NULL,          
                `CondSubMatches` char(50) default NULL,         
                `CondFileEquals` char(50) default NULL,         
                `CondFileMatches` char(50) default NULL,        
                `ActTransType` char(50) NOT NULL,               
                `ActUsingMappingId` char(50) NOT NULL,          
                `OnCompleteStatusID` char(50) default NULL,     
                `OnCompleteErrStatusId` char(50) default NULL,  
                PRIMARY KEY  (`RulesId`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;   

/*Data for the table `message_rule` */



/* Procedure structure for procedure `GetAllProtocol` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetAllProtocol` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `GetAllProtocol`()
BEGIN
        Select * from mail_protocol_schema;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `GetAllTradingPartner` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetAllTradingPartner` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `GetAllTradingPartner`()
BEGIN
       select * from trading_partner_schema;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `InsertTradingPartnerDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `InsertTradingPartnerDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `InsertTradingPartnerDetails`(Id VarChar(50), TPName varchar(100),MPID smallint(5),EmailID varChar(100), AlterEmailID varchar(100), ClientID varchar(100), XSDLoc varchar(100))
BEGIN
      insert into trading_partner_schema(TradingPartnerID,TPName,MailProtocolID,EmailID, AlternateEmailID,ClientID,XSDLocation)
      values(Id,TPName,MPID,EmailID,AlterEmailId,ClientID,XSDLoc);
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
