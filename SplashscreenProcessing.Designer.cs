﻿namespace TransactionImporter
{
    partial class SplashscreenProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashscreenProcessing));
            this.buttonCancelSplashScreen = new System.Windows.Forms.Button();
            this.progressBarStatusBar = new System.Windows.Forms.ProgressBar();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.lablMeessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCancelSplashScreen
            // 
            this.buttonCancelSplashScreen.Location = new System.Drawing.Point(205, 79);
            this.buttonCancelSplashScreen.Name = "buttonCancelSplashScreen";
            this.buttonCancelSplashScreen.Size = new System.Drawing.Size(77, 20);
            this.buttonCancelSplashScreen.TabIndex = 4;
            this.buttonCancelSplashScreen.Text = "Quit";
            this.buttonCancelSplashScreen.UseVisualStyleBackColor = true;
            this.buttonCancelSplashScreen.Visible = false;
            // 
            // progressBarStatusBar
            // 
            this.progressBarStatusBar.Location = new System.Drawing.Point(9, 105);
            this.progressBarStatusBar.Name = "progressBarStatusBar";
            this.progressBarStatusBar.Size = new System.Drawing.Size(273, 23);
            this.progressBarStatusBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarStatusBar.TabIndex = 3;
            // 
            // timerStatus
            // 
            this.timerStatus.Enabled = true;
            this.timerStatus.Interval = 50;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // lablMeessage
            // 
            this.lablMeessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablMeessage.Location = new System.Drawing.Point(12, 21);
            this.lablMeessage.Name = "lablMeessage";
            this.lablMeessage.Size = new System.Drawing.Size(273, 55);
            this.lablMeessage.TabIndex = 8;
            // 
            // SplashscreenProcessing
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(321, 131);
            this.Controls.Add(this.lablMeessage);
            this.Controls.Add(this.buttonCancelSplashScreen);
            this.Controls.Add(this.progressBarStatusBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SplashscreenProcessing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Processing statement";
            this.Load += new System.EventHandler(this.SplashscreenProcessing_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancelSplashScreen;
        private System.Windows.Forms.ProgressBar progressBarStatusBar;
        public System.Windows.Forms.Timer timerStatus;
        public System.Windows.Forms.Label lablMeessage;
    }
}