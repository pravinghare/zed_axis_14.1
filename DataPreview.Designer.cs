//namespace TransactionImporter
//{
//    partial class DataPreview
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            m_DataPreview = null;
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.components = new System.ComponentModel.Container();
//            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
//            this.contextMenuStripDeleteRows = new System.Windows.Forms.ContextMenuStrip(this.components);
//            this.deleteRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
//            this.buttonOk = new System.Windows.Forms.Button();
//            this.buttonReload = new System.Windows.Forms.Button();
//            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
//            this.dataGridViewDataPreview = new System.Windows.Forms.DataGridView();
//            this.comboBoxXmlPreviewTable = new System.Windows.Forms.ComboBox();
//            this.contextMenuStripDeleteRows.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // contextMenuStripDeleteRows
//            // 
//            this.contextMenuStripDeleteRows.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
//            this.deleteRowsToolStripMenuItem});
//            this.contextMenuStripDeleteRows.Name = "contextMenuStripDeleteRows";
//            this.contextMenuStripDeleteRows.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
//            this.contextMenuStripDeleteRows.Size = new System.Drawing.Size(139, 26);
//            this.contextMenuStripDeleteRows.Text = "Delete Rows";
//            // 
//            // deleteRowsToolStripMenuItem
//            // 
//            this.deleteRowsToolStripMenuItem.Name = "deleteRowsToolStripMenuItem";
//            this.deleteRowsToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
//            this.deleteRowsToolStripMenuItem.Text = "Delete Rows";
//            //this.deleteRowsToolStripMenuItem.Click += new System.EventHandler(this.deleteRowsToolStripMenuItem_Click);
//            // 
//            // buttonOk
//            // 
//            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonOk.Location = new System.Drawing.Point(577, 323);
//            this.buttonOk.Name = "buttonOk";
//            this.buttonOk.Size = new System.Drawing.Size(87, 23);
//            this.buttonOk.TabIndex = 1;
//            this.buttonOk.Text = "OK";
//            this.buttonOk.UseVisualStyleBackColor = true;
//            //this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
//            // 
//            // buttonReload
//            // 
//            this.buttonReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
//            this.buttonReload.Location = new System.Drawing.Point(687, 323);
//            this.buttonReload.Name = "buttonReload";
//            this.buttonReload.Size = new System.Drawing.Size(87, 23);
//            this.buttonReload.TabIndex = 2;
//            this.buttonReload.Text = "Reload";
//            this.buttonReload.UseVisualStyleBackColor = true;
//            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
//            // 
//            // dataGridViewDataPreview
//            // 
//            this.dataGridViewDataPreview.AllowDrop = true;
//            this.dataGridViewDataPreview.AllowUserToResizeRows = false;
//            this.dataGridViewDataPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
//                        | System.Windows.Forms.AnchorStyles.Left)
//                        | System.Windows.Forms.AnchorStyles.Right)));
//            this.dataGridViewDataPreview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
//            this.dataGridViewDataPreview.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
//            this.dataGridViewDataPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
//            this.dataGridViewDataPreview.CausesValidation = false;
//            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
//            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(204)))), ((int)(((byte)(229)))));
//            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
//            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
//            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
//            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
//            this.dataGridViewDataPreview.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
//            this.dataGridViewDataPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
//            this.dataGridViewDataPreview.ContextMenuStrip = this.contextMenuStripDeleteRows;
//            this.dataGridViewDataPreview.GridColor = System.Drawing.SystemColors.Control;
//            this.dataGridViewDataPreview.Location = new System.Drawing.Point(15, 46);
//            this.dataGridViewDataPreview.MultiSelect = false;
//            this.dataGridViewDataPreview.Name = "dataGridViewDataPreview";
//            this.dataGridViewDataPreview.Size = new System.Drawing.Size(783, 271);
//            this.dataGridViewDataPreview.TabIndex = 0;
//            this.dataGridViewDataPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewDataPreview_MouseDown);
//            this.dataGridViewDataPreview.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridViewDataPreview_CellBeginEdit);
//            this.dataGridViewDataPreview.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewDataPreview_UserAddedRow);
//            this.dataGridViewDataPreview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDataPreview_CellDoubleClick);
//            this.dataGridViewDataPreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridViewDataPreview_MouseMove);
//            this.dataGridViewDataPreview.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewDataPreview_CellMouseDown);
//            this.dataGridViewDataPreview.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDataPreview_CellContentDoubleClick);
//            this.dataGridViewDataPreview.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewDataPreview_UserDeletedRow);
//            this.dataGridViewDataPreview.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridViewDataPreview_DragOver);
//            this.dataGridViewDataPreview.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDataPreview_CellEndEdit);
//            this.dataGridViewDataPreview.AllowUserToAddRowsChanged += new System.EventHandler(this.dataGridViewDataPreview_AllowUserToAddRowsChanged);
//            this.dataGridViewDataPreview.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewDataPreview_DataError);
//            this.dataGridViewDataPreview.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dataGridViewDataPreview_KeyUp);
//            this.dataGridViewDataPreview.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridViewDataPreview_DragDrop);
//            this.dataGridViewDataPreview.AllowUserToDeleteRowsChanged += new System.EventHandler(this.dataGridViewDataPreview_AllowUserToDeleteRowsChanged);
//            this.dataGridViewDataPreview.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDataPreview_CellContentClick);
//            // 
//            // comboBoxXmlPreviewTable
//            // 
//            this.comboBoxXmlPreviewTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
//            this.comboBoxXmlPreviewTable.FormattingEnabled = true;
//            this.comboBoxXmlPreviewTable.Location = new System.Drawing.Point(15, 12);
//            this.comboBoxXmlPreviewTable.Name = "comboBoxXmlPreviewTable";
//            this.comboBoxXmlPreviewTable.Size = new System.Drawing.Size(243, 21);
//            this.comboBoxXmlPreviewTable.TabIndex = 3;
//            this.comboBoxXmlPreviewTable.SelectedIndexChanged += new System.EventHandler(this.comboBoxXmlPreviewTable_SelectedIndexChanged);
//            // 
//            // DataPreview
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
//            this.BackColor = System.Drawing.Color.AliceBlue;
//            this.ClientSize = new System.Drawing.Size(811, 356);
//            this.Controls.Add(this.buttonReload);
//            this.Controls.Add(this.buttonOk);
//            this.Controls.Add(this.dataGridViewDataPreview);
//            this.Controls.Add(this.comboBoxXmlPreviewTable);
//            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
//            this.Name = "DataPreview";
//            this.ShowIcon = false;
//            this.ShowInTaskbar = false;
//            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
//            this.Text = "Preview";
//            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
//            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DataPreview_FormClosing);
//            this.contextMenuStripDeleteRows.ResumeLayout(false);
//            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDataPreview)).EndInit();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.Button buttonOk;
//        private System.Windows.Forms.Button buttonReload;
//        private System.Windows.Forms.BindingSource bindingSource1;
//        private System.Windows.Forms.ContextMenuStrip contextMenuStripDeleteRows;
//        private System.Windows.Forms.ToolStripMenuItem deleteRowsToolStripMenuItem;
//        private System.Windows.Forms.DataGridView dataGridViewDataPreview;
//        private System.Windows.Forms.ComboBox comboBoxXmlPreviewTable;
//    }
//}