﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Streams;


namespace POS_BaseStream
{
   public class POS_BaseInventoryCostAdjustmentList
   {
       protected string m_TxnID;
       protected string m_TimeCreated = string.Empty;
       protected string m_TimeModified = string.Empty;
       protected string m_Associate;
       protected string m_Comments;    
       protected decimal?[] m_CostDifference = new decimal?[30000];     
       protected string m_HistoryDocStatus;
       protected string m_InventoryAdjustmentNumber;
       protected string m_ItemsCount;
       protected decimal?[] m_NewCost = new decimal?[30000];
       protected decimal?[] m_OldCost = new decimal?[30000]; 
       protected string m_Reason;
       protected string m_StoreExchangeStatus;
       protected string m_StoreNumber;      
       protected string m_TxnDate;
       protected string m_TxnState;      
       protected string m_Workstation;

       protected Collection<QBInventoryCostAdjustmentItem> m_InventoryCostAdjustmentDetails;
       //InventoryCostAdjustmentRet
     
    }
}
