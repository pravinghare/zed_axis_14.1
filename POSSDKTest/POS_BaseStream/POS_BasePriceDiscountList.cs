﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public class POS_BasePriceDiscountList
    {
        protected string m_TxnID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_StoreExchangeStatus;
        protected string m_PriceDiscountName;
        protected string m_PriceDiscountReason;
        protected string m_Comments;
        protected string m_Associate;
        protected string m_LastAssociate;
        protected string m_PriceDiscountType;
        protected string m_IsInactive;
        protected string m_PriceDiscountPriceLevels;
        protected string m_PriceDiscountXValue;
        protected string m_PriceDiscountYValue;
        protected string m_IsApplicableOverXValue;
        protected string m_StartDate;
        protected string m_StopDate ;
        protected string m_ItemsCount ;
        protected string m_ListID;
        protected string m_TxnLineID;
        protected string m_UnitOfMeasure;
    }
}
