﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public class POS_BaseDataExtensionList
    {
        protected string m_TxnID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_DateApplied;
        protected string m_DateRestored;
        protected string m_PriceAdjustmentName;
        protected string m_Comments;
        protected string m_Associate;
        protected string m_AppliedBy;
        protected string m_RestoredBy;
        protected string m_ItemsCount;
        protected string m_PriceAdjustmentStatus;
    }
}
