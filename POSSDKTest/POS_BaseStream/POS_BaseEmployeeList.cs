﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public  class POS_BaseEmployeeList
  {
      #region Private Member Variable

        protected string m_ListID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;   
        protected string m_City;
        protected decimal m_CommissionPercent;
        protected string m_Country;
        protected string m_EMail;
        protected string m_FirstName;
        protected string m_IsTrackingHours;
        protected string m_LastName;
        protected string m_LoginName;
        protected string m_Notes;
        protected string m_Phone;
        protected string m_AltPhone;
        protected string m_Contact;
        protected string m_AltContact;
        protected string m_PostalCode;
        protected string m_State;
        protected string m_Street;
        protected string m_Street2;
        protected string m_SecurityGroup;

        #endregion


    }
}
