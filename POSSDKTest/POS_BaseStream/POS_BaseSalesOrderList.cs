﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace POS_BaseStream
{
   public class POS_BaseSalesOrderList
   {
         protected string m_TxnID;
         protected string m_TimeCreated = string.Empty;
         protected string m_TimeModified = string.Empty;
         protected string m_Associate;
         protected string m_Cashier;
         protected string m_CustomerListID;
         protected string m_DepositBalance;
         protected string m_Discount;
         protected string m_DiscountPercent;
         protected string m_Instructions;
         protected string m_ItemsCount;
         protected string m_PriceLevelNumber;
         protected string m_PromoCode;
         protected string m_Qty;
         protected string m_SalesOrderNumber;
         protected string m_SalesOrderStatusDesc;
         protected string m_SalesOrderType;
         protected string m_Subtotal;
         protected string m_TaxAmount;
         protected string m_TaxCategory;
         protected string m_TaxPercentage;
         protected string m_Total;
         protected string m_TxnDate;

         protected string m_AddressName;
         protected string m_City;
         protected string m_CompanyName;
         protected string m_Country;
         protected string m_FullName;
         protected string m_Phone;
         protected string m_Phone2;
         protected string m_Phone3;
         protected string m_Phone4;
         protected string m_PostalCode;
         protected string m_ShipBy;
         protected string m_Shipping;
         protected string m_State;
         protected string m_Street;
         protected string m_Street2;


        protected Collection<Streams.QBSalesOrderItem> m_QBSalesOrderItemDetails;
        protected string[] m_SalesTxnLineID = new string[100000];
      
    }
}
