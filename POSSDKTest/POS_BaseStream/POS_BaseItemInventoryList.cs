﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QBPOSStreams
{
    public class POS_BaseItemInventoryList
    {
        #region Properties
        protected string m_ListID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_ALU;      
        protected string m_Attribute;        
        protected string m_COGSAccount;  
        protected string m_Cost;      
        protected string m_DepartmentListID;   
        protected string m_Desc1;       
        protected string m_Desc2;     
        protected string m_IncomeAccount;
        protected string m_IsEligibleForCommission;
        protected string m_IsPrintingTags;
        protected string m_IsUnorderable;
        protected string m_IsEligibleForRewards;    
        protected string m_IsWebItem;
        protected string m_ItemType;
        protected string m_MarginPercent;
        protected string m_MarkupPercent;
        protected string m_MSRP;
        protected string m_OnHandStore01;
        protected string m_ReorderPointStore01;
        protected string m_OrderByUnit;
        protected string m_OrderCost;
        protected string m_Price1;
        protected string m_ReorderPoint;
        protected string m_SellByUnit;
        protected string m_Size;
        protected string m_TaxCode;
        protected string m_UnitOfMeasure;
        protected string m_UPC;
        protected string m_VendorListID;
        protected string m_Manufacturer;
        protected string m_Weight;
        protected string m_UOMALU;
        protected string m_UOMMSRP;
        protected string m_UOMNumberOfBaseUnits;
        protected string m_UOMPrice1;       
        protected string m_UOMUnitOfMeasure;
        protected string m_UOMUPC;
        protected string m_VendALU;
        protected string m_VendOrderCost;
        protected string m_VendUPC;
        protected string m_VendVendorListID;    
             
        #endregion
    }
}
