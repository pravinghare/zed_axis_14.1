﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public class POS_BaseDepartmentList
    {
        #region Private Member Variable

        protected string m_ListID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_DefaultMarginPercent;     
        protected string m_DefaultMarkupPercent;
        protected string m_DepartmentCode;
        protected string m_DepartmentName;
        protected string m_StoreExchangeStatus;
        protected string m_TaxCode;       

        #endregion
    }
}
