﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace POS_BaseStream
{
   public  class POS_BaseVoucherList
    {
        protected string m_TxnID;        
        protected string m_TimeCreated= string.Empty;        
        protected string m_TimeModified=string.Empty;  
        protected string m_Comments;       
        protected string m_CompanyName;
        protected decimal?[] m_Discount = new decimal?[30000];
        protected decimal?[] m_DiscountPercent = new decimal?[30000];
        protected decimal?[] m_Fee = new decimal?[30000];
        protected decimal?[] m_Freight = new decimal?[30000];         
        protected string m_HistoryDocStatus;
        protected string m_ItemsCount;        
        protected string m_PayeeCode;        
        protected string m_PayeeListID;        
        protected string m_PayeeName;   
        protected string m_StoreExchangeStatus;       
        protected string m_StoreNumber;
        protected decimal?[] m_Subtotal = new decimal?[30000];
        protected decimal?[] m_TermsDiscount = new decimal?[30000];          
        protected string m_TermsDiscountDays;        
        protected string m_TermsNetDays;
        protected decimal?[] m_Total = new decimal?[30000];           
        protected string m_TotalQty;        
        protected string m_TxnDate;       
        protected string m_TxnState;       
        protected string m_VendorCode;        
        protected string m_VendorListID;        
        protected string m_VoucherNumber;       
        protected string m_VoucherType;      
        protected string m_Workstation;      

        protected Collection<Streams.QBVoucherItem> m_VoucherItemsDetails;
        protected string[] m_QBTxnLineNumber = new string[30000];
       
    }
}
