﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public  class POS_BaseCustomerList
    {
      protected string m_ListID ;
      protected string m_TimeCreated = string.Empty;
      protected string m_TimeModified = string.Empty;
    //  protected decimal?[] m_AccountBalance = new decimal?[2000];
     // protected decimal?[] m_AccountLimit = new decimal?[2000];
      //protected decimal?[] m_AmountPastDue = new decimal?[2000];
      protected string m_CompanyName;
      protected string m_CustomerID;
      protected decimal m_CustomerDiscPercent;
      protected string m_CustomerDiscType;
      protected string m_CustomerType;
      protected string m_Email;
      protected string m_IsOkToEMail ;
      protected string m_FirstName;
      protected string m_FullName;
      protected string m_IsAcceptingChecks;
      protected string m_IsUsingChargeAccount ;
      protected string m_IsUsingWithQB ;
      protected string m_IsRewardsMember ;
      protected string m_IsNoShipToBilling ;
      protected string m_LastName;      
      protected string m_LastSale;
      protected string m_Notes;
      protected string m_Phone;     
      protected string m_Phone2;
      protected string m_Phone3;
      protected string m_Phone4;
      protected string m_PriceLevelNumber;
      protected string m_Salutation ;
      protected string m_StoreExchangeStatus ;
      protected string m_TaxCategory ;
      protected string m_WebNumber ;

      protected string m_BillCity;
      protected string m_BillCountry;
      protected string m_BillPostalCode;
      protected string m_BillState;
      protected string m_BillStreet;
      protected string m_BillStreet2;

      protected string m_DefaultShipAddress;
      protected string m_ShipAddressName;
      protected string m_ShipCompanyName;
      protected string m_ShipFullName;
      protected string m_ShipCity;
      protected string m_ShipCountry;
      protected string m_ShipPostalCode;
      protected string m_ShipState;
      protected string m_ShipStreet;
      protected string m_ShipStreet2;
      protected string m_OwnerID ;
      protected string[]  m_DataExtName ;
      protected string m_DataExtType ;
      protected string[]  m_DataExtValue;

    }
}
