﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public   class POS_BasePriceAdjustmentList
  {
      protected string m_TxnID;
      protected string m_TimeCreated = string.Empty;
      protected string m_TimeModified = string.Empty;
      protected string m_DateApplied ;
      protected string m_DateRestored ;
      protected string m_PriceAdjustmentName;
      protected string m_Comments;
      protected string m_Associate;
      protected string m_AppliedBy;
      protected string m_RestoredBy;
      protected string m_ItemsCount;
      protected string m_PriceAdjustmentStatus;
      protected string m_PriceLevelNumber;
      protected string m_StoreExchangeStatus;
      protected string m_ListID;
      protected string m_TxnLineID;
      protected decimal m_NewPrice;
      protected decimal m_OldPrice;
      protected decimal m_OldCost;
    }
}
