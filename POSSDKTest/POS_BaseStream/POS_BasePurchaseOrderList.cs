﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;


namespace POS_BaseStream
{
   public class POS_BasePurchaseOrderList
    {
        #region private member
        protected string m_TxnID;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_Associate;
        protected string m_CancelDate;
        protected string m_CompanyName;
        protected string m_Discount;
        protected string m_DiscountPercent;
        protected string m_Fee;
        protected string m_Instructions;
        protected string m_ItemsCount;
        protected string m_PurchaseOrderNumber;
        protected string m_PurchaseOrderStatusDesc;
        protected string m_QtyDue;
        protected string m_QtyOrdered ;
        protected string m_QtyReceived ;
        protected string m_SalesOrderNumber ;
        protected string m_ShipToStoreNumber;
        protected string m_StartShipDate;
        protected string m_StoreNumber;
        protected string m_Subtotal ;
        protected string m_Terms;
        protected string m_TermsDiscount;
        protected string m_TermsDiscountDays;
        protected string m_TermsNetDays;
        protected string m_Total ;
        protected string m_TxnDate;
        protected string m_UnfilledPercent;
        protected string m_VendorCode ;
        protected string m_VendorListID;

        protected Collection<Streams.QBPurchaseOrderItem> m_PurchaseOrderItemsDetails;
        protected string[] m_QBTxnLineNumber = new string[30000];
       
       
        #endregion
    }
}
