﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public class POS_BaseTimeEntryList
    {
        #region Private Member Variable

        protected string m_ListID;
        protected string m_ClockInTime = string.Empty;
        protected string m_ClockOutTime = string.Empty;
        protected string m_CreatedBy;       
        protected string m_EmployeeListID;
        protected string m_EmployeeLoginName;
        protected string m_FirstName;        
        protected string m_LastName;
        protected string m_StoreNumber;

        #endregion
    }
}
