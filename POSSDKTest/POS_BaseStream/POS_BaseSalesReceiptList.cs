﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS_BaseStream
{
  public class POS_BaseSalesReceiptList
    {

    protected string m_TxnID;
    protected string m_TimeCreated = string.Empty;
    protected string m_TimeModified = string.Empty;
    protected string m_Cashier ;
    protected string m_Comments ;
    protected string m_CustomerListID ;
    protected string m_ItemsCount;
    protected string m_PromoCode ;
    protected string m_SalesOrderTxnID ;
    protected string m_SalesReceiptNumber;
    protected string m_SalesReceiptType ;
    protected string m_ShipDate;    
    protected string m_StoreNumber;
    protected string m_Subtotal    ;
    protected string m_TaxCategory;
    protected string m_TenderType ;    
    protected string m_Total;
    protected string m_TrackingNumber;
    protected string m_TxnDate;
    protected string m_TxnState ;
    protected string m_Workstation;

    protected string[] m_ListID = new string[30000];
    protected string[] m_ALU = new string[30000];
    protected string[] m_Associate = new string[30000];
    protected string[] m_Attribute = new string[30000];
    protected decimal?[] m_Commission = new decimal?[30000];
    protected decimal?[] m_Cost = new decimal?[30000];
    protected string[] m_Desc1 = new string[30000];
    protected string[] m_Desc2 = new string[30000];
    protected decimal?[] m_Discount = new decimal?[30000];
    protected decimal?[] m_DiscountPercent = new decimal?[30000];
    protected string[] m_DiscountType = new string[30000];
    protected string[] m_DiscountSource = new string[30000];
    protected decimal?[] m_ExtendedPrice = new decimal?[30000];
    protected decimal?[] m_ExtendedTax = new decimal?[30000];
    protected string[] m_ItemNumber = new string[30000];
    protected string[] m_NumberOfBaseUnits = new string[30000];
    protected decimal?[] m_Price = new decimal?[30000];
    protected string[] m_PriceLevelNumber = new string[30000];
    protected decimal?[] m_Qty = new decimal?[30000];
    protected string[] m_SerialNumber = new string[30000];
    protected string[] m_Size = new string[30000];
    protected decimal?[] m_TaxAmount = new decimal?[30000];
    protected string[] m_TaxCode = new string[30000];
    protected decimal?[] m_TaxPercentage = new decimal?[30000];
    protected string[] m_UnitOfMeasure = new string[30000];
    protected string[] m_UPC = new string[30000];
    protected string[] m_WebDesc = new string[30000];
    protected string[] m_Manufacturer = new string[30000];
    protected string[] m_Weight = new string[30000];

    //protected decimal? m_TenderAccountAmount = new decimal?[200];
    //protected decimal?[] m_TenderAccountTipAmount = new decimal?[200];

    //protected decimal?[] m_TenderCashAmount = new decimal?[200];

    //protected string[] m_TenderCheckNumber = new string[200];
    //protected decimal?[] m_TenderCheckAmount = new decimal?[200];

    //protected string[] m_TenderCreditCardName = new string[200];
    //protected decimal?[] m_TenderCreditCardAmount = new decimal?[200];
    //protected decimal?[] m_TenderCreditCardTipAmount = new decimal?[200];

    //protected decimal?[] m_TenderDebitCardCashback = new decimal?[200];
    //protected decimal?[] m_TenderDebitCardAmount = new decimal?[200];

    //protected decimal?[] m_TenderDepositAmount = new decimal?[200];

    //protected string[] m_TenderGiftCertificateNumber = new string[200];
    //protected decimal?[] m_TenderGiftAmount = new decimal?[200];

    //protected decimal?[] m_TenderGiftCardAmount = new decimal?[200];
    //protected decimal?[] m_TenderGiftCardTipAmount = new decimal?[200];




    protected decimal? m_TenderAccountAmount ;
    protected decimal? m_TenderAccountTipAmount ;

    protected decimal? m_TenderCashAmount ;

    protected string m_TenderCheckNumber ;
    protected decimal? m_TenderCheckAmount ;

    protected string m_TenderCreditCardName;
    protected decimal? m_TenderCreditCardAmount;
    protected decimal? m_TenderCreditCardTipAmount;

    protected decimal? m_TenderDebitCardCashback;
    protected decimal? m_TenderDebitCardAmount;

    protected decimal? m_TenderDepositAmount;
    protected string m_TenderGiftCertificateNumber;
    protected decimal? m_TenderGiftAmount;

    protected decimal? m_TenderGiftCardAmount;
    protected decimal? m_TenderGiftCardTipAmount;
    }
}
