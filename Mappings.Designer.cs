﻿ namespace TransactionImporter
{
    partial class Mappings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
       
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mappings));
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.dataGridViewMapping = new Telerik.WinControls.UI.RadGridView();
            this.saveFileDialogExportMapping = new System.Windows.Forms.SaveFileDialog();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarButton1 = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.buttonSave = new Telerik.WinControls.UI.CommandBarButton();
            this.buttonSaveAs = new Telerik.WinControls.UI.CommandBarButton();
            this.buttonExport = new Telerik.WinControls.UI.CommandBarButton();
            this.buttonDelete = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarStripElement3 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarStripElement4 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.textBoxImportType = new Telerik.WinControls.UI.RadTextBox();
            this.radLabelDelete = new Telerik.WinControls.UI.RadLabel();
            this.radLabelExport = new Telerik.WinControls.UI.RadLabel();
            this.radLabelSaveAS = new Telerik.WinControls.UI.RadLabel();
            this.radLabelSave = new Telerik.WinControls.UI.RadLabel();
            this.textBoxMappingName = new Telerik.WinControls.UI.RadTextBox();
            this.comboBoxMappingNames = new Telerik.WinControls.UI.RadDropDownList();
            this.buttonHelp = new System.Windows.Forms.LinkLabel();
            this.labelMappings = new Telerik.WinControls.UI.RadLabel();
            this.linkLabelImportType = new System.Windows.Forms.LinkLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.comboBoxImportType = new Telerik.WinControls.UI.RadDropDownList();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement5 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.labelImportType = new Telerik.WinControls.UI.CommandBarLabel();
            this.commandBarRowElement5 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.osCommerce1 = new Monitor.OSCommerce();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapping.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            this.radCommandBar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxImportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSaveAS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMappingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMappingNames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMappings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewMapping
            // 
            this.dataGridViewMapping.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewMapping.AutoScroll = true;
            this.dataGridViewMapping.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewMapping.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.dataGridViewMapping.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridViewMapping.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dataGridViewMapping.Location = new System.Drawing.Point(0, 48);
            // 
            // 
            // 
            this.dataGridViewMapping.MasterTemplate.AllowAddNewRow = false;
            this.dataGridViewMapping.MasterTemplate.AllowDeleteRow = false;
            this.dataGridViewMapping.MasterTemplate.AllowRowResize = false;
            this.dataGridViewMapping.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMapping.MasterTemplate.EnableAlternatingRowColor = true;
            this.dataGridViewMapping.MasterTemplate.EnableGrouping = false;
            this.dataGridViewMapping.MasterTemplate.PageSize = 15;
            this.dataGridViewMapping.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.dataGridViewMapping.MasterTemplate.ShowFilteringRow = false;
            this.dataGridViewMapping.MasterTemplate.ShowHeaderCellButtons = true;
            this.dataGridViewMapping.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.dataGridViewMapping.Name = "dataGridViewMapping";
            // 
            // 
            // 
            this.dataGridViewMapping.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.dataGridViewMapping.ShowGroupPanel = false;
            this.dataGridViewMapping.ShowHeaderCellButtons = true;
            this.dataGridViewMapping.Size = new System.Drawing.Size(1048, 497);
            this.dataGridViewMapping.TabIndex = 18;
            this.dataGridViewMapping.ThemeName = "ControlDefault";
            this.dataGridViewMapping.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.dataGridViewMapping_CellFormatting);
            this.dataGridViewMapping.ValueChanged += new System.EventHandler(this.dataGridViewMapping_ValueChanged);
            this.dataGridViewMapping.Resize += new System.EventHandler(this.dataGridViewMapping_Resize);
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.AutoSize = false;
            this.commandBarStripElement1.Bounds = new System.Drawing.Rectangle(0, 0, 82, 50);
            this.commandBarStripElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarButton1});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement1.UseCompatibleTextRendering = false;
            // 
            // commandBarButton1
            // 
            this.commandBarButton1.AutoSize = false;
            this.commandBarButton1.Bounds = new System.Drawing.Rectangle(0, 0, 28, 48);
            this.commandBarButton1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarButton1.DisplayName = "commandBarButton1";
            this.commandBarButton1.Image = ((System.Drawing.Image)(resources.GetObject("commandBarButton1.Image")));
            this.commandBarButton1.Name = "commandBarButton1";
            this.commandBarButton1.Text = "commandBarButton1";
            this.commandBarButton1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarButton1.UseCompatibleTextRendering = false;
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Name = "commandBarRowElement1";
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            this.commandBarRowElement1.Text = "";
            this.commandBarRowElement1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement1.UseCompatibleTextRendering = false;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.AutoSize = true;
            this.commandBarRowElement2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "commandBarRowElement2";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2,
            this.commandBarStripElement3,
            this.commandBarStripElement4});
            this.commandBarRowElement2.Text = "";
            this.commandBarRowElement2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement2.UseCompatibleTextRendering = false;
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.AutoSize = false;
            this.commandBarStripElement2.Bounds = new System.Drawing.Rectangle(0, 0, 240, 50);
            this.commandBarStripElement2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.buttonSave,
            this.buttonSaveAs,
            this.buttonExport,
            this.buttonDelete});
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            this.commandBarStripElement2.Text = "";
            this.commandBarStripElement2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement2.UseCompatibleTextRendering = false;
            // 
            // buttonSave
            // 
            this.buttonSave.AutoSize = false;
            this.buttonSave.Bounds = new System.Drawing.Rectangle(0, -13, 45, 55);
            this.buttonSave.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonSave.DrawText = false;
            this.buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("buttonSave.Image")));
            this.buttonSave.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Text = "Save";
            this.buttonSave.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSave.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonSave.UseCompatibleTextRendering = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonSaveAs
            // 
            this.buttonSaveAs.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.buttonSaveAs.AutoSize = false;
            this.buttonSaveAs.Bounds = new System.Drawing.Rectangle(8, -14, 45, 55);
            this.buttonSaveAs.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonSaveAs.DrawText = false;
            this.buttonSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("buttonSaveAs.Image")));
            this.buttonSaveAs.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.buttonSaveAs.Name = "buttonSaveAs";
            this.buttonSaveAs.StretchHorizontally = false;
            this.buttonSaveAs.Text = "Save As";
            this.buttonSaveAs.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSaveAs.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonSaveAs.UseCompatibleTextRendering = false;
            this.buttonSaveAs.Click += new System.EventHandler(this.buttonSaveAs_Click);
            // 
            // buttonExport
            // 
            this.buttonExport.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.buttonExport.AutoSize = false;
            this.buttonExport.Bounds = new System.Drawing.Rectangle(12, -14, 45, 55);
            this.buttonExport.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonExport.DrawText = false;
            this.buttonExport.Image = ((System.Drawing.Image)(resources.GetObject("buttonExport.Image")));
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Text = "";
            this.buttonExport.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonExport.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonExport.UseCompatibleTextRendering = false;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.buttonDelete.AutoSize = false;
            this.buttonDelete.Bounds = new System.Drawing.Rectangle(14, -14, 70, 55);
            this.buttonDelete.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonDelete.DisplayName = "commandBarButton2";
            this.buttonDelete.DrawText = true;
            this.buttonDelete.Image = ((System.Drawing.Image)(resources.GetObject("buttonDelete.Image")));
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Text = "";
            this.buttonDelete.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonDelete.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.buttonDelete.UseCompatibleTextRendering = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // commandBarStripElement3
            // 
            this.commandBarStripElement3.AutoSize = false;
            this.commandBarStripElement3.Bounds = new System.Drawing.Rectangle(0, 0, 490, 50);
            this.commandBarStripElement3.DisplayName = "commandBarStripElement3";
            this.commandBarStripElement3.Name = "commandBarStripElement3";
            // 
            // commandBarStripElement4
            // 
            this.commandBarStripElement4.AutoSize = false;
            this.commandBarStripElement4.Bounds = new System.Drawing.Rectangle(0, 0, 350, 50);
            this.commandBarStripElement4.DisplayName = "commandBarStripElement4";
            this.commandBarStripElement4.Name = "commandBarStripElement4";
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.AutoSize = false;
            this.radCommandBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radCommandBar1.Controls.Add(this.textBoxImportType);
            this.radCommandBar1.Controls.Add(this.radLabelDelete);
            this.radCommandBar1.Controls.Add(this.radLabelExport);
            this.radCommandBar1.Controls.Add(this.radLabelSaveAS);
            this.radCommandBar1.Controls.Add(this.radLabelSave);
            this.radCommandBar1.Controls.Add(this.textBoxMappingName);
            this.radCommandBar1.Controls.Add(this.comboBoxMappingNames);
            this.radCommandBar1.Controls.Add(this.buttonHelp);
            this.radCommandBar1.Controls.Add(this.labelMappings);
            this.radCommandBar1.Controls.Add(this.linkLabelImportType);
            this.radCommandBar1.Controls.Add(this.radLabel1);
            this.radCommandBar1.Controls.Add(this.comboBoxImportType);
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2,
            this.commandBarRowElement3});
            this.radCommandBar1.Size = new System.Drawing.Size(1048, 45);
            this.radCommandBar1.TabIndex = 19;
            // 
            // textBoxImportType
            // 
            this.textBoxImportType.AutoSize = false;
            this.textBoxImportType.Location = new System.Drawing.Point(346, 17);
            this.textBoxImportType.Name = "textBoxImportType";
            this.textBoxImportType.Size = new System.Drawing.Size(138, 26);
            this.textBoxImportType.TabIndex = 2;
            // 
            // radLabelDelete
            // 
            this.radLabelDelete.AutoSize = false;
            this.radLabelDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radLabelDelete.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.radLabelDelete.Location = new System.Drawing.Point(159, 28);
            this.radLabelDelete.Name = "radLabelDelete";
            this.radLabelDelete.Size = new System.Drawing.Size(67, 15);
            this.radLabelDelete.TabIndex = 27;
            this.radLabelDelete.Text = "Delete Map";
            this.radLabelDelete.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabelDelete.Click += new System.EventHandler(this.radLabelDelete_Click);
            // 
            // radLabelExport
            // 
            this.radLabelExport.AutoSize = false;
            this.radLabelExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radLabelExport.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.radLabelExport.Location = new System.Drawing.Point(112, 28);
            this.radLabelExport.Name = "radLabelExport";
            this.radLabelExport.Size = new System.Drawing.Size(42, 15);
            this.radLabelExport.TabIndex = 26;
            this.radLabelExport.Text = "Export";
            this.radLabelExport.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabelExport.Click += new System.EventHandler(this.radLabelExport_Click);
            // 
            // radLabelSaveAS
            // 
            this.radLabelSaveAS.AutoSize = false;
            this.radLabelSaveAS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radLabelSaveAS.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.radLabelSaveAS.Location = new System.Drawing.Point(60, 28);
            this.radLabelSaveAS.Name = "radLabelSaveAS";
            this.radLabelSaveAS.Size = new System.Drawing.Size(47, 15);
            this.radLabelSaveAS.TabIndex = 25;
            this.radLabelSaveAS.Text = "Save As";
            this.radLabelSaveAS.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabelSaveAS.Click += new System.EventHandler(this.radLabelSaveAS_Click);
            // 
            // radLabelSave
            // 
            this.radLabelSave.AutoSize = false;
            this.radLabelSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radLabelSave.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.radLabelSave.Location = new System.Drawing.Point(8, 28);
            this.radLabelSave.Name = "radLabelSave";
            this.radLabelSave.Size = new System.Drawing.Size(45, 15);
            this.radLabelSave.TabIndex = 24;
            this.radLabelSave.Text = "Save";
            this.radLabelSave.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radLabelSave.Click += new System.EventHandler(this.radLabelSave_Click);
            // 
            // textBoxMappingName
            // 
            this.textBoxMappingName.AutoSize = false;
            this.textBoxMappingName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMappingName.Location = new System.Drawing.Point(579, 17);
            this.textBoxMappingName.Name = "textBoxMappingName";
            this.textBoxMappingName.Size = new System.Drawing.Size(138, 26);
            this.textBoxMappingName.TabIndex = 4;
            // 
            // comboBoxMappingNames
            // 
            this.comboBoxMappingNames.AutoSize = false;
            this.comboBoxMappingNames.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.comboBoxMappingNames.Location = new System.Drawing.Point(581, 17);
            this.comboBoxMappingNames.Name = "comboBoxMappingNames";
            this.comboBoxMappingNames.Size = new System.Drawing.Size(138, 26);
            this.comboBoxMappingNames.TabIndex = 21;
            this.comboBoxMappingNames.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxMappingNames_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.comboBoxMappingNames.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.comboBoxMappingNames.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.comboBoxMappingNames.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.comboBoxMappingNames.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.ArrowPrimitive)(this.comboBoxMappingNames.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(2))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            // 
            // buttonHelp
            // 
            this.buttonHelp.BackColor = System.Drawing.Color.White;
            this.buttonHelp.Font = new System.Drawing.Font("Verdana", 8F);
            this.buttonHelp.Location = new System.Drawing.Point(741, 17);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(40, 24);
            this.buttonHelp.TabIndex = 21;
            this.buttonHelp.TabStop = true;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.buttonHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.buttonHelp_LinkClicked);
            // 
            // labelMappings
            // 
            this.labelMappings.AutoSize = false;
            this.labelMappings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.labelMappings.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.labelMappings.ForeColor = System.Drawing.Color.Black;
            this.labelMappings.Location = new System.Drawing.Point(487, 17);
            this.labelMappings.Name = "labelMappings";
            this.labelMappings.Size = new System.Drawing.Size(89, 24);
            this.labelMappings.TabIndex = 23;
            this.labelMappings.Text = "Mapping Name:";
            // 
            // linkLabelImportType
            // 
            this.linkLabelImportType.BackColor = System.Drawing.Color.White;
            this.linkLabelImportType.Font = new System.Drawing.Font("Verdana", 8F);
            this.linkLabelImportType.Location = new System.Drawing.Point(785, 17);
            this.linkLabelImportType.Name = "linkLabelImportType";
            this.linkLabelImportType.Size = new System.Drawing.Size(280, 24);
            this.linkLabelImportType.TabIndex = 20;
            this.linkLabelImportType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabelImportType.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelImportType_LinkClicked);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.radLabel1.Location = new System.Drawing.Point(246, 17);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(96, 24);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "Transaction Type:";
            this.radLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxImportType
            // 
            this.comboBoxImportType.AutoSize = false;
            this.comboBoxImportType.BackColor = System.Drawing.Color.White;
            this.comboBoxImportType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.comboBoxImportType.Font = new System.Drawing.Font("Segoe UI", 8.5F);
            this.comboBoxImportType.Location = new System.Drawing.Point(346, 17);
            this.comboBoxImportType.Name = "comboBoxImportType";
            this.comboBoxImportType.Size = new System.Drawing.Size(138, 26);
            this.comboBoxImportType.TabIndex = 22;
            this.comboBoxImportType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxImportType_SelectedIndexChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.comboBoxImportType.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.Primitives.BorderPrimitive)(this.comboBoxImportType.GetChildAt(0).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.comboBoxImportType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor2 = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.comboBoxImportType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor = System.Drawing.Color.WhiteSmoke;
            ((Telerik.WinControls.Primitives.ArrowPrimitive)(this.comboBoxImportType.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(2))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Name = "commandBarRowElement3";
            // 
            // commandBarStripElement5
            // 
            this.commandBarStripElement5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement5.DisplayName = "commandBarStripElement5";
            this.commandBarStripElement5.Name = "commandBarStripElement5";
            this.commandBarStripElement5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement5.UseCompatibleTextRendering = false;
            // 
            // labelImportType
            // 
            this.labelImportType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(240)))), ((int)(((byte)(251)))));
            this.labelImportType.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.labelImportType.DisplayName = "commandBarLabel1";
            this.labelImportType.Name = "labelImportType";
            this.labelImportType.Text = "Transaction Type :";
            this.labelImportType.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.labelImportType.UseCompatibleTextRendering = false;
            // 
            // commandBarRowElement5
            // 
            this.commandBarRowElement5.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement5.Name = "commandBarRowElement5";
            // 
            // osCommerce1
            // 
            this.osCommerce1.CanPauseAndContinue = true;
            this.osCommerce1.ExitCode = 0;
            this.osCommerce1.ServiceName = "";
            // 
            // Mappings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1048, 545);
            this.Controls.Add(this.radCommandBar1);
            this.Controls.Add(this.dataGridViewMapping);
            this.DoubleBuffered = false;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Mappings";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Column Mappings";
            this.Load += new System.EventHandler(this.Mappings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapping.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            this.radCommandBar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxImportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSaveAS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMappingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxMappingNames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMappings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxImportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

            this.openFileDialogBrowseFile = new System.Windows.Forms.OpenFileDialog();
        }

        #endregion
        private Telerik.WinControls.RadThemeManager radThemeManager1;
       
        private Telerik.WinControls.UI.GridViewTextBoxColumn DatagridViewColumnQuickBooks;
        private Telerik.WinControls.UI.GridViewComboBoxColumn DataGridViewColumnMappFields;
        private Telerik.WinControls.UI.GridViewTextBoxColumn DataGridViewColumnsConstant;
        private Telerik.WinControls.UI.GridViewComboBoxColumn DataGridViewColumnsFunction;
        private Telerik.WinControls.UI.GridViewTextBoxColumn DataGridViewColumnDataPreview;
        private Monitor.OSCommerce osCommerce1;
        public Telerik.WinControls.UI.RadGridView dataGridViewMapping;
        private System.Windows.Forms.SaveFileDialog saveFileDialogExportMapping;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton commandBarButton1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton buttonSave;
        private Telerik.WinControls.UI.CommandBarButton buttonSaveAs;
        private Telerik.WinControls.UI.CommandBarButton buttonExport;
        private Telerik.WinControls.UI.CommandBarButton buttonDelete;
        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement5;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement4;
        private System.Windows.Forms.LinkLabel linkLabelImportType;
        private System.Windows.Forms.LinkLabel buttonHelp;
        private Telerik.WinControls.UI.CommandBarLabel labelImportType;
        private Telerik.WinControls.UI.RadDropDownList comboBoxMappingNames;
        private Telerik.WinControls.UI.RadLabel labelMappings;
        private Telerik.WinControls.UI.RadDropDownList comboBoxImportType;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox textBoxImportType;
        private Telerik.WinControls.UI.RadTextBox textBoxMappingName;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.RadLabel radLabelDelete;
        private Telerik.WinControls.UI.RadLabel radLabelExport;
        private Telerik.WinControls.UI.RadLabel radLabelSaveAS;
        private Telerik.WinControls.UI.RadLabel radLabelSave;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement5;
        public System.Windows.Forms.OpenFileDialog openFileDialogBrowseFile;

    }
}
