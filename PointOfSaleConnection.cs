﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataProcessingBlocks;
using EDI.Constant;
using System.Collections.Specialized;
using Interop.QBPOSXMLRPLIB;
using Microsoft.Win32;
using System.Security;
namespace DataProcessingBlocks
{
    public partial class PointOfSaleConnection : Form
    {
        private static PointOfSaleConnection m_options;
        ExportSettings exportSettings;
        RegistryKey keyName;


        #region Static Methods

        /// <summary>
        /// This method is used to get static instance.
        /// </summary>
        /// <returns></returns>
        public static PointOfSaleConnection GetInstance()
        {
            if (m_options == null)
                m_options = new PointOfSaleConnection();
            return m_options;
        }
        #endregion

        public PointOfSaleConnection()
        {
            InitializeComponent();
        }

        public void SavePOSConnectionInRegistry(string name, string value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {
                    newKey = keyName.CreateSubKey(Constants.QuickBooksPOSConnection);
                    newKey.SetValue(name, value);
                }

            }
            catch (UnauthorizedAccessException)
            {
                // MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                // MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        public void SavePOSIsPracticeInRegistry(string name, string value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {
                    newKey = keyName.CreateSubKey(Constants.QuickBooksPOSConnection);
                    newKey.SetValue(name, value);
                }

            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        public void GetPOSConnectionInRegistry()
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            CommonUtilities.GetInstance().QBPOSRequestProcessor = new RequestProcessor();
            keyName = currentUserKey.OpenSubKey(@"SOFTWARE\" + Constants.QuickBooksPOSConnection);
            if (keyName != null)
            {
                foreach (string valueNames in keyName.GetValueNames())
                {
                    if (valueNames != null)
                    {
                        if (valueNames == "Company Data")
                        {
                            txtCompany.Text = keyName.GetValue("Company Data").ToString();
                        }
                        if (valueNames == "Is Practice")
                        {
                            if (keyName.GetValue("Is Practice").ToString() == "True")
                                checkBoxIsPractice.Checked = true;
                        }
                    }
                    else
                    {
                    }

                }
            }

        }
        private void PointOfSaleConnection_Load(object sender, EventArgs e)
        {
            GetPOSConnectionInRegistry();
            txtComputer.Text = System.Windows.Forms.SystemInformation.ComputerName;
            CommonUtilities.GetInstance().computerName = txtComputer.Text;

        }
        private void comboBoxVersion_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void comboBoxVersion_MouseClick(object sender, MouseEventArgs e)
        {
        }
        private void checkBoxIsPractice_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIsPractice.Checked)
                CommonUtilities.GetInstance().IsPractice = "True";
            else
                CommonUtilities.GetInstance().IsPractice = "False";
        }
        private void buttonOK_Click(object sender, EventArgs e)
        {

            CommonUtilities.GetInstance().CompanyName = txtCompany.Text;
            if (checkBoxIsPractice.Checked)
                CommonUtilities.GetInstance().IsPractice = "True";
            else
                CommonUtilities.GetInstance().IsPractice = "False";
            CommonUtilities.GetInstance().Version = txtQBPOSVersion.Text;
            CommonUtilities.GetInstance().connectionString = "Computer Name=" + CommonUtilities.GetInstance().computerName + ";Company Data=" + CommonUtilities.GetInstance().CompanyName + ";Version=" + CommonUtilities.GetInstance().Version.ToString();

            CommonUtilities.GetInstance().Connect = "Ok";
            this.Close();
        }

        private void txtCompany_TextChanged(object sender, EventArgs e)
        {


        }

    }
}
