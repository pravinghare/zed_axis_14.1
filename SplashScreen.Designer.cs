namespace TransactionImporter
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashScreen));
            this.progressBarStatusBar = new System.Windows.Forms.ProgressBar();
            this.labelMeessage = new System.Windows.Forms.Label();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.buttonCancelSplashScreen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressBarStatusBar
            // 
            this.progressBarStatusBar.Location = new System.Drawing.Point(17, 90);
            this.progressBarStatusBar.Name = "progressBarStatusBar";
            this.progressBarStatusBar.Size = new System.Drawing.Size(273, 23);
            this.progressBarStatusBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarStatusBar.TabIndex = 0;
            // 
            // labelMeessage
            // 
            this.labelMeessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeessage.Location = new System.Drawing.Point(14, 18);
            this.labelMeessage.Name = "labelMeessage";
            this.labelMeessage.Size = new System.Drawing.Size(273, 26);
            this.labelMeessage.TabIndex = 1;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 50;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // buttonCancelSplashScreen
            // 
            this.buttonCancelSplashScreen.Location = new System.Drawing.Point(215, 47);
            this.buttonCancelSplashScreen.Name = "buttonCancelSplashScreen";
            this.buttonCancelSplashScreen.Size = new System.Drawing.Size(77, 20);
            this.buttonCancelSplashScreen.TabIndex = 2;
            this.buttonCancelSplashScreen.Text = "Cancel";
            this.buttonCancelSplashScreen.UseVisualStyleBackColor = true;
            this.buttonCancelSplashScreen.Visible = false;
            this.buttonCancelSplashScreen.Click += new System.EventHandler(this.buttonCancelSplashScreen_Click);
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(304, 130);
            this.ControlBox = false;
            this.Controls.Add(this.buttonCancelSplashScreen);
            this.Controls.Add(this.labelMeessage);
            this.Controls.Add(this.progressBarStatusBar);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SplashScreen";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Loading....";
            this.Load += new System.EventHandler(this.SplashScreen_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarStatusBar;
        private System.Windows.Forms.Timer timerStatus;
        public System.Windows.Forms.Label labelMeessage;
        private System.Windows.Forms.Button buttonCancelSplashScreen;
    }
}