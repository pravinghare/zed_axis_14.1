using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Security;


namespace DataProcessingBlocks
{
    public partial class PopUpMessage : Form
    {

        #region Constructor
        public PopUpMessage()
        {
            InitializeComponent();
            this.labelPopUpMessage.Text = "Thank you for trialling Zed Axis.";
            this.labelMessageNew.Text = "To help us improve the product and support could you please spare a few moments to answer some questions online";
        }
        #endregion

        #region Events

        private void buttonOk_Click(object sender, EventArgs e)
        {
            //Close message
            this.Close();
            
            //Set Survey=true
            try
            {
                this.SetSurveyInRegistry("Survey", "true");
            }
            catch { } 

            //Go to feedback page
            try
            {               
                System.Diagnostics.Process.Start( EDI.Constant.Constants.FeedbackID);        
            }
            catch
            { }
        }

        private void buttonLater_Click(object sender, EventArgs e)
        {
            // Close message and continue
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //Set Survey = skip
            try
            {
                this.SetSurveyInRegistry("Survey", "skip");
            }
            catch { }

            //Close message
            this.Close();
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Get Survey value from registry.
        /// </summary>
        /// <param name="exportType"></param>
        /// <returns></returns>

        #region Get Survey value from registry.
        public string GetSurveyValueFromReg(string exportType)
        {
            string expectedData = String.Empty;
            try
            {                
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE\Zed Systems\Survey");

                if (keyName != null)
                {
                    foreach (string valueNames in keyName.GetValueNames())
                    {
                        if (valueNames == exportType)
                            expectedData = keyName.GetValue(exportType).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }
            return expectedData;
        }
        #endregion


        /// <summary>
        /// Set Survey value in registry.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>

        #region Set Survey value in registry.
        public void SetSurveyInRegistry(string name, string value)
        {
            try
            {             
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE\Zed Systems\Survey", true);

                keyName.SetValue(name, value);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }
        #endregion

        #endregion

    }
}