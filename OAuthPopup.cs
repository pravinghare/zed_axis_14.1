﻿extern alias OnlineQBO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using DistributedDesktopApp;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

using Microsoft.Win32;  

namespace DistributedDesktopApp
{
    public partial class OAuthPopup : Form,IDisposable
    {
        #region " Member Variables "

        private const string _dummyProtocol = "http://";
        private const string _dummyHost = "www.intuit.com";
        private IppRealmOAuthProfile _ippRealmOAuthProfile;
        private OnlineQBO.DevDefined.OAuth.Framework.IToken _requestToken;
        private string _consumerKey = "";
        private string _consumerSecret = "";
        private string _oauthVerifier = "";
        private bool _caughtCallback = false;
        private delegate void FillValueDelegate();
      
        #endregion      

         public OAuthPopup(IppRealmOAuthProfile ippRealmOAuthProfile)
        {            
            try
            {
                InitializeComponent();
                _ippRealmOAuthProfile = ippRealmOAuthProfile;
                readConsumerKeysFromConfiguration();
                startOAuthHandshake();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); 
                
            }               
        }
        

        #region " App Configuration "

        private void readConsumerKeysFromConfiguration()
        {
            _consumerKey = "qyprdcK7XCZdMhpKMJwfpkqk1NlXoJ";
            _consumerSecret = "GY73ZHjyhTiKPEVYEnsDlFc9Hu7i5AHtMCODJQER";
        }

        #endregion

        #region " Start OAuth Handshake - Get Request Token and Redirect to IPP for User Authorization "

        private void startOAuthHandshake()
        {
            _requestToken = getOauthRequestTokenFromIpp(_consumerKey, _consumerSecret);          
            redirectToIppForUserAuthorization(_requestToken);
        }

        #region " Get Request Token "

        private OnlineQBO.DevDefined.OAuth.Framework.IToken getOauthRequestTokenFromIpp(string consumerKey, string consumerSecret)
        {
            OnlineQBO.DevDefined.OAuth.Consumer.IOAuthSession oauthSession = createDevDefinedOAuthSession(consumerKey, consumerSecret);
            return oauthSession.GetRequestToken();
        }

        #endregion

        #region " Redirect to Service Provider - IPP "      
        private void runBrowserThread(string  url)
        {
            var th = new Thread(() =>
            {
                var br = new WebBrowser();
                br.DocumentCompleted += browser_DocumentCompleted;
                br.Navigate(url);
                Application.Run();
            });
            th.SetApartmentState(ApartmentState.MTA);
            th.Start();
        }

        void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var br = sender as WebBrowser;
            if (br.Url == e.Url)
            {
                Application.ExitThread();   // Stops the thread
            }
        }
        private void redirectToIppForUserAuthorization(OnlineQBO.DevDefined.OAuth.Framework.IToken requestToken)
        {
            try
            {
                // put existing web browser code in here
                    var oauthUserAuthorizeUrl = "https://appcenter.intuit.com/Connect/Begin";
                   this.oauthBrowser.Navigate(oauthUserAuthorizeUrl + "?oauth_token=" + requestToken.Token + "&oauth_callback=" + OnlineQBO.DevDefined.OAuth.Framework.UriUtility.UrlEncode(_dummyProtocol + _dummyHost));                                       
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        #endregion

        #endregion

        #region " Capture Callback, Exchange Request Token for Access Token and Close Dialog "

        private void oauthBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (e.Url.Host == _dummyHost)
            {
                NameValueCollection query =  HttpUtility.ParseQueryString(e.Url.Query);
                _oauthVerifier = query["oauth_verifier"];
                _ippRealmOAuthProfile.realmId = query["realmId"];
                _ippRealmOAuthProfile.dataSource = query["dataSource"];
                _caughtCallback = true;
                oauthBrowser.Navigate("about:blank");
            }
            else if (_caughtCallback)
            {
                OnlineQBO.DevDefined.OAuth.Framework.IToken accessToken = exchangeRequestTokenForAccessToken(_consumerKey, _consumerSecret, _requestToken);
                _ippRealmOAuthProfile.accessToken = accessToken.Token;
                _ippRealmOAuthProfile.accessSecret = accessToken.TokenSecret;
                _ippRealmOAuthProfile.expirationDateTime = DateTime.Now.AddMonths(6);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        #region " Exhange Request Token for Access Token "


        public OnlineQBO.DevDefined.OAuth.Framework.IToken exchangeRequestTokenForAccessToken(string consumerKey, string consumerSecret, OnlineQBO.DevDefined.OAuth.Framework.IToken requestToken)
        {
            OnlineQBO.DevDefined.OAuth.Consumer.IOAuthSession oauthSession = createDevDefinedOAuthSession(consumerKey, consumerSecret);
           // return oauthSession.ExchangeRequestTokenForAccessToken(requestToken, _oauthVerifier);
            return  oauthSession.ExchangeRequestTokenForAccessToken(requestToken, _oauthVerifier);
        }

        #endregion

        #endregion

        #region " DevDefined Helper Functions "

        private OnlineQBO.DevDefined.OAuth.Consumer.IOAuthSession createDevDefinedOAuthSession(string consumerKey, string consumerSecret)
        {

            var oauthRequestTokenUrl = "https://oauth.intuit.com/oauth/v1" + "/get_request_token";
            var oauthAccessTokenUrl = "https://oauth.intuit.com/oauth/v1" + "/get_access_token";
            var oauthUserAuthorizeUrl = "https://appcenter.intuit.com/Connect/Begin";

            OnlineQBO.DevDefined.OAuth.Consumer.OAuthConsumerContext consumerContext = new OnlineQBO.DevDefined.OAuth.Consumer.OAuthConsumerContext
            {
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                SignatureMethod = OnlineQBO.DevDefined.OAuth.Framework.SignatureMethod.HmacSha1
            };

            return new OnlineQBO.DevDefined.OAuth.Consumer.OAuthSession(consumerContext, oauthRequestTokenUrl, oauthUserAuthorizeUrl, oauthAccessTokenUrl);

        }

        #endregion

        private void OAuthPopup_Load(object sender, EventArgs e)
        {

        }

    }
}
