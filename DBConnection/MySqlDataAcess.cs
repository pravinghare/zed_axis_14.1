using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Text;
using System.Windows;
using DataProcessingBlocks;
using Microsoft.Win32;
using OB.ApplicationBlocks.Data;
using System.Windows.Forms;
using System.Security;
using SQLQueries;

namespace DBConnection
{
    /// <summary>
    /// This class provides MySql connectivity and database operations with Applications.
    /// </summary>
  
    public class MySqlDataAcess
    {

        
        #region Private Members
        private static OdbcConnection connection = null;

        /// <summary>
        /// This function is used to open a odbc connection using the connection string from AppSettings.
        /// </summary>
        /// <returns>OdbcConnection object</returns>
        private static OdbcConnection Open()
        {
            //Create a OdbcConnection object using the connection string from AppSettings

            //connection = new OdbcConnection(CommonUtilities.GetAppSettingValue("MySQLConnectionString"));
            connection = new OdbcConnection(TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString"));

            try
            {
                //if connection notopen , then open a connection
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                //If connection open , then return the connection object.
                if (connection.State == ConnectionState.Open)
                {
                    return connection;
                }

                //Else return a null object
                else
                {
                    return null;
                }

            }
            //Handle exception
            catch (OdbcException)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER016"));
            }

        }


        /// <summary>
        /// Close the ODBC connection
        /// </summary>
        /// <returns>OdbcConnection object</returns>
        private static OdbcConnection Close()
        {
            try
            {
                //if connection notopen , then open a connection
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }

                //If connection open , then return the connection object.
                if (connection.State == ConnectionState.Closed)
                {
                    return connection;
                }
                //Else return a null object
                else
                {
                    connection.Dispose();
                    return null;
                }
            }
            //Handle Exception
            catch
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER017"));
            }

        }

        #endregion

        #region Public Members

        /// <summary>
        ///Testing the connection mysql
        /// </summary>
        public static void TestConnection()
        {
            try
            {
                //connection = new OdbcConnection(CommonUtilities.GetAppSettingValue("MySQLConnectionString"));
                connection = new OdbcConnection(TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString"));
                connection.Open();
            }
            catch (Exception)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER016"));
            }

        }

        /// <summary>

        /// Execute nonquery
        /// </summary>
        /// <param name="commandText">Command String</param>
        /// <returns>Number of row affected</returns>

        /// <summary>

        /// This static method is used for execute DML command to Mysql database
        /// </summary>
        /// <param name="commandText">Passing sql query</param>
        /// <returns></returns>

        public static int ExecuteNonQuery(string commandText)
        {
            try
            {
                return OdbcHelper.ExecuteNonQuery(Open(), CommandType.Text, commandText);

            }
            catch (OdbcException)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }
            finally
            {
                Close();
            }
        }


        /// <summary>
        /// Get MailProtocolID from database.
        /// </summary>
        /// <param name="messageID"></param>
        /// <returns></returns>
        public static int GetMailProtocolID(int messageID)
        {
            //Create query by adding parameters
            string commandText = string.Format(Queries.SQ052,
                                               messageID);
            try
            {
                //Execute query
                return Convert.ToInt32(ExecuteScaler(commandText));
            }
            catch (InvalidCastException)
            {
                return 0;
            }
            catch (NullReferenceException)
            {
                return 0;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }

        /// <summary>
        /// Get MailProtocol from databsae.
        /// </summary>
        /// <param name="mailPrototcolId"></param>
        /// <returns></returns>
        public static string GetMailProtocol(int mailPrototcolId)
        {
            string commandText = string.Format(Queries.SQ055,
                                               mailPrototcolId);
            try
            {
                return ExecuteScaler(commandText).ToString();
            }
            catch (InvalidCastException)
            {
                return string.Empty;
            }
            catch (NullReferenceException)
            {
                return string.Empty;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
        }

        /// <summary>

        /// This method is used for getting dataset from Database.
        /// </summary>
        /// <param name="commandText">Passing sql query.</param>
        /// <returns></returns>

        public static DataSet ExecuteDataset(string commandText)
        {
            try
            {
                return OdbcHelper.ExecuteDataset(Open(), CommandType.Text, commandText);
            }
            catch (OdbcException)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }
            finally
            {
                Close();
            }

        }
        public static DataSet ExecuteDataset1(string commandText)
        {
            try
            {
                return OdbcHelper.ExecuteDataset1(Open(), CommandType.Text, commandText);
            }
            catch (OdbcException)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }
            finally
            {
                Close();
            }

        }

        /// <summary>
        /// Excute Scalar command 
        /// </summary>
        /// <param name="commandText">command string</param>
        /// <returns>Object</returns>


        /// <summary>

        /// This method is used for getting single value from database.
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>

        public static object ExecuteScaler(string commandText)
        {
            try
            {
                return OdbcHelper.ExecuteScalar(Open(), CommandType.Text, commandText);
            }
            catch
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }
            finally
            {
                Close();
            }
        }


        /// <summary>

        /// Inserting the .ism file
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="odbcparam"></param>
        /// <returns></returns>

        /// <summary>

        /// This method is used for insert ISM message into database.
        /// </summary>
        /// <param name="commandText">Passing SQL Query</param>
        /// <param name="odbcparam">Passing ODBC parameter.</param>
        /// <returns></returns>

        public static int InsertISMFile(string commandText, OdbcParameter odbcparam)
        {
            try
            {

                return OdbcHelper.ExecuteNonQuery(Open(), CommandType.Text, commandText, odbcparam);

            }
            catch (OdbcException)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }
            finally
            {
                Close();
            }
        }


        /// <summary>

        /// Update the Export Filter schema according to DataType
        /// It insert/update the Date and DataType.
        /// </summary>
        /// <param name="date">To date of filter criteria</param>
        /// <param name="dataType">Export Data Type</param>
        /// <returns></returns>

        /// <summary>

        /// This method is used for update connection values in database.
        /// </summary>
        /// <param name="date">Passing Date value.</param>
        /// <param name="dataType">Passing Data Type value.</param>
        /// <returns></returns>

        public static bool UpdateExprotFilter(DateTime date, string dataType)
        {
            try
            {
                string strTime = System.DateTime.Now.TimeOfDay.Hours.ToString() + ":" + System.DateTime.Now.TimeOfDay.Minutes.ToString() + ":" + System.DateTime.Now.TimeOfDay.Seconds.ToString();
                //string strTime = System.DateTime.Now.TimeOfDay.ToString();
                string strQuery = string.Format(SQLQueries.Queries.SQ016, dataType);
                int row = 0;
                try
                {
                    row = Convert.ToInt32(ExecuteScaler(strQuery));
                }
                catch { }
                if (row.Equals(0))
                {
                    //inserting
                    //strQuery = string.Format(SQLQueries.Queries.SQ011, dataType, date.ToString(EDI.Constant.Constants.MySqlDateFormat) + " " + strTime);
                    //row = ExecuteNonQuery(strQuery);
                    StoreExportRegistry(dataType, date.ToString(EDI.Constant.Constants.MySqlDateFormat) + " " + strTime);
                }
                else
                {
                    //updating
                    //strQuery = string.Format(SQLQueries.Queries.SQ012, date.ToString(EDI.Constant.Constants.MySqlDateFormat) + " " + strTime, dataType);
                    //row = ExecuteNonQuery(strQuery);
                    StoreExportRegistry(dataType, date.ToString(EDI.Constant.Constants.MySqlDateFormat) + " " + strTime);
                }
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                throw new Exception(MessageCodes.GetValue("Zed Axis ER018"));
            }

        }

        //This method is used to store connection values.
        public static void StoreExportRegistry(string exportType, string exportDate)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                newKey = keyName.CreateSubKey("Zed Systems\\Connection");

                newKey.SetValue(exportType, exportDate);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        //This method is used to store EDI values.
        public static void StoreEDIRegistry(bool isExist)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                newKey = keyName.CreateSubKey("Zed Systems\\DBExist");

                newKey.SetValue("IsDBExist", isExist);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to export.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        ///This method is used to store the Auto Numbering & Address Details status for Axis Version 5.1.    
        /// </summary>
        public static void StoreStatusDetailsRegistry(string connectedSoftware, bool isAutoNumbering, bool isUpdateAddress)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newNumberingKey, newAddressKey;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Numbering_UpdateAddress",true);
                    if (RK != null)
                    {
                        RK.SetValue("IsAutoNumbering", isAutoNumbering);
                        RK.SetValue("IsUpdateAddressDetails", isUpdateAddress);
                    }
                    else
                    {
                        newNumberingKey = keyName.CreateSubKey("Zed Systems\\QuickBooks\\Import\\Numbering_UpdateAddress");
                        newNumberingKey.SetValue("IsAutoNumbering", isAutoNumbering);
                        newNumberingKey.SetValue("IsUpdateAddressDetails", isUpdateAddress);
                    }
                }
                // Axis 10.0 Changes
                if (connectedSoftware.Equals(EDI.Constant.Constants.Xerostring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\Xero\\Import\\Numbering_UpdateAddress", true);
                    if (RK != null)
                    {
                        RK.SetValue("IsAutoNumbering", isAutoNumbering);
                        RK.SetValue("IsUpdateAddressDetails", isUpdateAddress);
                    }
                    else
                    {
                        newNumberingKey = keyName.CreateSubKey("Zed Systems\\Xero\\Import\\Numbering_UpdateAddress");
                        newNumberingKey.SetValue("IsAutoNumbering", isAutoNumbering);
                        newNumberingKey.SetValue("IsUpdateAddressDetails", isUpdateAddress);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges."+ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Getting the AutoNumbering Status from Registry.
        /// </summary>
        /// <param name="connectedSoftware"></param>
        /// <returns></returns>
        public static bool GetAutoNumberingStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Numbering_UpdateAddress",true);
                    if (RK != null)
                    {
                        object obj = RK.GetValue("IsAutoNumbering");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }

        /// <summary>
        /// Getting the Address Details status from Registry. 
        /// </summary>
        /// <param name="connectedSoftware"></param>
        /// <returns></returns>
        public static bool GetAddressDetailsStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Numbering_UpdateAddress",true);
                    if (RK != null)
                    {
                        object obj = RK.GetValue("IsUpdateAddressDetails");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }
        #endregion


        #region Store & get the details of Duplicate,Skip & Overwrite functionality from Registry.

        /// <summary>
        ///This method is used to store the Duplicate,Skip & Overwrite status for Axis Version 5.1.    
        /// </summary>
        public static void StoreTransactionOptionStatus(string connectedSoftware, bool isDuplicate, bool isSkip,bool isOverwrite, bool isAppend)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newNumberingKey, newAddressKey;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (RK != null)
                    {
                        RK.SetValue("IsDuplicate", isDuplicate);
                        RK.SetValue("IsSkip", isSkip);
                        RK.SetValue("IsOverwrite", isOverwrite);
                        //Bun No. 412
                        RK.SetValue("IsAppend", isAppend);
                    }
                    else
                    {
                        newNumberingKey = keyName.CreateSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options");
                        newNumberingKey.SetValue("IsDuplicate", isDuplicate);
                        newNumberingKey.SetValue("IsSkip", isSkip);
                        newNumberingKey.SetValue("IsOverwrite", isOverwrite);
                        //Bun No. 412
                        newNumberingKey.SetValue("IsAppend", isAppend);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        /// <summary>
        /// Getting the Duplicate Status from Registry.
        /// </summary>
        /// <param name="connectedSoftware"></param>
        /// <returns></returns>
        public static bool GetDuplicateStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (RK != null)
                    {
                        object obj = RK.GetValue("IsDuplicate");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }

        /// <summary>
        /// Getting the Skip Status from Registry.
        /// </summary>
        /// <param name="connectedSoftware"></param>
        /// <returns></returns>
        public static bool GetSkipStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (RK != null)
                    {
                        object obj = RK.GetValue("IsSkip");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }

        /// <summary>
        /// Getting the Overwrite Status from Registry.
        /// </summary>
        /// <param name="connectedSoftware"></param>
        /// <returns></returns>
        public static bool GetOverwriteStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey RK;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    RK = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (RK != null)
                    {
                        object obj = RK.GetValue("IsOverwrite");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }



        public static bool GetGrossToNetStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                //if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))
                //{
                key = keyName.OpenSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options", true);
                if (key != null)
                {
                    object obj = key.GetValue("GrossToNet");
                    if ("True".Equals(obj))
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
                //}
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
          
        }

        //Axis 8.0
        #region vendor part number status

        public static bool GetVendorPartNumberLookupStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        object obj = key.GetValue("IsVendorPartNumberLookup");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }


        public static bool GetALULookupStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        object obj = key.GetValue("ALULookup");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }

        //Axis-565
        public static bool GetUPCLookupStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        object obj = key.GetValue("UPCLookup");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            return false;
        }
        //Axis-565 end

// bug 486
        public static bool GetSKULookupStatus(string connectedSoftware)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                //if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))
                //{
                    key = keyName.OpenSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        object obj = key.GetValue("SKULookup");
                        if ("True".Equals(obj))
                        {
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                //}
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return false;
            }
        }


        public static void SetVendorPartNumberLookupStatus(string connectedSoftware,bool value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBstring))
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        key.SetValue("IsVendorPartNumberLookup", value);
                    }
                    else
                    {
                        newKey = keyName.CreateSubKey("Zed Systems\\QuickBooks\\Import\\Trans_Options");
                        newKey.SetValue("IsVendorPartNumberLookup", value);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        //416 change conn str
        public static void SetALULookupStatus(string connectedSoftware, bool value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))// conn
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        key.SetValue("ALULookup", value);
                    }
                    else
                    {
                        newKey = keyName.CreateSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options");
                        newKey.SetValue("ALULookup", value);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        //Axis-565
        public static void SetUPCLookupStatus(string connectedSoftware, bool value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))// conn
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        key.SetValue("UPCLookup", value);
                    }
                    else
                    {
                        newKey = keyName.CreateSubKey("Zed Systems\\QuickBookPOS\\Import\\Trans_Options");
                        newKey.SetValue("UPCLookup", value);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        //Axis-565 end

        //bug 486 Axis 12.0 SKU Lookup 

        public static void SetSKULookupStatus(string connectedSoftware, bool value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);

                if (connectedSoftware.Equals(EDI.Constant.Constants.QBPOSstring))// conn
                {
                    key = keyName.OpenSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options", true);
                    if (key != null)
                    {
                        key.SetValue("SKULookup", value);
                    }
                    else
                    {
                        newKey = keyName.CreateSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options");
                        newKey.SetValue("SKULookup", value);
                    }
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }



        //bug 486 Axis 12.0 SKU Lookup 

        public static void SetGrossToNetStatus(string connectedSoftware, bool value)
        {
            try
            {
                //RegistryKey LocalMachineKey = Registry.LocalMachine;
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey newKey;
                RegistryKey key;
                RegistryKey keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);


                key = keyName.OpenSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options", true);
                if (key != null)
                {
                    key.SetValue("GrossToNet", value);
                }
                else
                {
                    newKey = keyName.CreateSubKey("Zed Systems\\QuickBook\\Import\\Trans_Options");
                    newKey.SetValue("GrossToNet", value);
                }

            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges." + ex.Message.ToString());
                return;
            }
            catch (SecurityException)
            {
                MessageBox.Show("Unauthorized user do not have permission to access registry.Need administrative privileges.");
                return;
            }
        }

        #endregion

        #endregion
    }
}