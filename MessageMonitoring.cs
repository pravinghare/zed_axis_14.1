// ===============================================================================
// 
// MessageMonitoring.cs
//
// This file contains the implementations of the methods which add services , 
// edit services and delete the services.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SQLQueries;
using DBConnection;
using DataProcessingBlocks;
using Microsoft.Win32;
using EDI.Constant;
using Telerik.WinControls.UI;
using System.Security;
using System.ServiceProcess;
using System.Configuration.Install;
using System.IO;
using System.Configuration;
using System.CodeDom.Compiler;
using System.Globalization;

namespace MessageMonitoringService
{
    public partial class MessageMonitoring : Form
    {
        #region Private Memmbers

        private static MessageMonitoring m_MessageMonitoring;

        #endregion

        #region Constructor

        public MessageMonitoring()
        {
            InitializeComponent();
        }

        #endregion
        
        #region Public Properties

        #endregion

        #region Static Methods

        /// <summary>
        /// Getting a new or current instance of Message Monitoring.
        /// </summary>
        /// <returns>Instance of Message Monitoring.</returns>
        public static MessageMonitoring GetInstance()
        {
            if (m_MessageMonitoring == null)
                m_MessageMonitoring = new MessageMonitoring();
            return m_MessageMonitoring;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// This method is used for binding Service datasource to grid.
        /// </summary>
        protected void BindServiceDetails()
        {
            try
            {
                //Getting Service data from database.
                DataSet dsServiceData = MySqlDataAcess.ExecuteDataset(SQLQueries.Queries.SQMS33);
                DataTable dsCloneData = new DataTable();
                if (dsServiceData != null)
                {
                    this.dataGridViewMessageServices.DataSource = null;
                    List<string> isActive = new List<string>();
                    isActive.Add("Active");
                    isActive.Add("InActive");

                    List<string> TimeFrame = new List<string>();
                    TimeFrame.Add("1 minute");
                    TimeFrame.Add("5 minute");
                    TimeFrame.Add("10 minute");
                    TimeFrame.Add("15 minute");
                    TimeFrame.Add("30 minute");
                    TimeFrame.Add("1 hour");
                    TimeFrame.Add("2 hour");
                    TimeFrame.Add("5 hour");
                    TimeFrame.Add("10 hour");
                    TimeFrame.Add("12 hour");
                    GridViewComboBoxColumn ComboBoxColumnIsActive = new GridViewComboBoxColumn();
                    ComboBoxColumnIsActive.HeaderText = "IsActive";
                    ComboBoxColumnIsActive.Name = "IsActive";
                    ComboBoxColumnIsActive.FieldName = "IsActive";
                    ComboBoxColumnIsActive.DataSource = isActive;

                    GridViewComboBoxColumn ComboBoxColumnTimeFrame = new GridViewComboBoxColumn();
                    ComboBoxColumnTimeFrame.HeaderText = "Period";
                    ComboBoxColumnTimeFrame.Name = "Period";
                    ComboBoxColumnTimeFrame.FieldName = "Period";
                    ComboBoxColumnTimeFrame.DataSource = TimeFrame;

                    DataColumn dcMonitorID = new DataColumn("MonitorId");
                    DataColumn dcIsActive = new DataColumn("IsActive");
                    DataColumn dcName = new DataColumn("Name");
                    DataColumn dcFolder = new DataColumn("Folder");
                    DataColumn dcPeriod = new DataColumn("Period");
                    dsCloneData.Columns.Add(dcMonitorID);
                    dsCloneData.Columns.Add(dcIsActive);

                    dsCloneData.Columns.Add(dcName);
                    dsCloneData.Columns.Add(dcFolder);
                    if (dataGridViewMessageServices.Columns.Count != 7)
                    {
                        dataGridViewMessageServices.Columns.Add("MonitorId");
                        dataGridViewMessageServices.Columns.Add(ComboBoxColumnIsActive);
                        dataGridViewMessageServices.Columns.Add("Name");
                        dataGridViewMessageServices.Columns.Add("Folder");
                        //GridViewBrowseColumn column = new GridViewBrowseColumn("Folder");
                        dataGridViewMessageServices.CellClick += new GridViewCellEventHandler(dataGridViewMessageServices_CellClick);
                        //dataGridViewMessageServices.Columns.Add(column);
                        dsCloneData.Columns.Add(dcPeriod);

                        dataGridViewMessageServices.Columns.Add(ComboBoxColumnTimeFrame);
                    }
                    dataGridViewMessageServices.Rows.Clear();

                    //for (int i = 0; i < dataGridViewMessageServices.ChildRows.Count; i++)
                    //{
                    //    dataGridViewMessageServices.ChildRows.RemoveAt(i);
                    //}
                    //dataGridViewMessageServices.CurrentRow=null;
                    //dataGridViewMessageServices.Refresh();
                    GridViewCommandColumn commandColumn = new GridViewCommandColumn();
                    commandColumn.Name = "Update";
                    commandColumn.DefaultText = "Update";
                    commandColumn.UseDefaultText = true;
                    commandColumn.FieldName = "Update";
                    commandColumn.HeaderText = "Update";
                    if (!dataGridViewMessageServices.Columns.Contains(commandColumn.DefaultText))
                        dataGridViewMessageServices.Columns.Add(commandColumn);
                    GridViewCommandColumn commandColumn2 = new GridViewCommandColumn();
                    commandColumn2.Name = "Delete";
                    commandColumn2.UseDefaultText = true;
                    commandColumn2.DefaultText = "Delete";
                    commandColumn2.FieldName = "Delete";
                    commandColumn2.HeaderText = "Delete";
                    if (!dataGridViewMessageServices.Columns.Contains(commandColumn2.DefaultText))
                    {
                        dataGridViewMessageServices.Columns.Add(commandColumn2);
                        dataGridViewMessageServices.CommandCellClick += new CommandCellClickEventHandler(radGridView1_CommandCellClick);
                        dataGridViewMessageServices.CellFormatting += new CellFormattingEventHandler(dataGridViewMessageServices_CreateRow);
                    }

                    //for (int i = 0; i < dataGridViewMessageServices.Columns.Count - 1; i++)
                    //{
                    //    this.dataGridViewMessageServices.Columns[i].BestFit();
                    //}
                    this.dataGridViewMessageServices.Columns[1].Width = 70;
                    this.dataGridViewMessageServices.Columns[2].Width = 100;
                    this.dataGridViewMessageServices.Columns[3].Width = 300;


                    this.dataGridViewMessageServices.Columns[4].Width = 100;
                    this.dataGridViewMessageServices.Columns[0].IsVisible = false;
                    //Checking dataset rows count.
                    if (dsServiceData.Tables[0].Rows.Count != 0)
                    {
                        object[] objArray = new object[5];
                        dataGridViewMessageServices.Rows.Add(objArray);
                        dataGridViewMessageServices.Rows.Clear();

                        foreach (DataRow dr in dsServiceData.Tables[0].Rows)
                        {
                            objArray[0] = (object)dr[0].ToString();
                            objArray[2] = (object)dr[1].ToString();
                            objArray[3] = (object)dr[2].ToString();
                            objArray[4] = (object)dr[3].ToString();

                            if (dr["IsActive"].ToString() == "0")
                                objArray[1] = (object)"InActive";
                            else
                                objArray[1] = (object)"Active";
                            dataGridViewMessageServices.Rows.Add(objArray);
                        }
                        //dsCloneData.AcceptChanges();
                        //this.dataGridViewMessageServices.DataSource = dsCloneData;

                        
                        dsServiceData = null;
                    }
                    else
                    {
                        //Display message of zero records.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG104"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        if (this.dataGridViewMessageServices.DataSource != null)
                            this.dataGridViewMessageServices.DataSource = null;
                        SetupMonitoringService.GetInstance().ServiceName = string.Empty;
                       
                        return;
                    }
                }
                else
                {
                    //Display message of zero records.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG104"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (this.dataGridViewMessageServices.DataSource != null)
                        this.dataGridViewMessageServices.DataSource = null;
                    SetupMonitoringService.GetInstance().ServiceName = string.Empty;
                   
                    return;
                }
            }
            catch (Exception ex)
            {
                //this.dataGridViewMessageServices.Rows.Clear();
                //Catching the exception.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void dataGridViewMessageServices_CreateRow(object sender, CellFormattingEventArgs e)
        {
            try
            {
                var MonitorId = e.CellElement.RowInfo.Cells[0].Value;

                if (e.CellElement.RowInfo.Cells[0].Value == null && (e.CellElement.ColumnInfo.HeaderText == "Update" || e.CellElement.ColumnInfo.HeaderText == "Delete"))
                {
                    GridViewCommandColumn column = e.CellElement.RowInfo.Cells[5].ColumnInfo as GridViewCommandColumn;
                    var cell = e.CellElement as GridCommandCellElement;
                    if (cell.CommandButton.Text == "Update")
                    {
                        cell.CommandButton.Text = "Add";
                    }
                    else if (cell.CommandButton.Text == "Delete")
                    {
                        cell.CommandButton.Text = "Cancel";
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        void dataGridViewMessageServices_CellClick(object sender, GridViewCellEventArgs e)
        {
            var ColIndex = ((sender as GridCellElement)).ColumnInfo.Index;

            if (ColIndex == 3)
            {
                var rowIndex = ((sender as GridCellElement)).RowInfo.Index;
                
                DialogResult key = this.folderBrowserDialogService.ShowDialog();
                if (key == DialogResult.OK)
                {
                    string logDirectoryPath = string.Empty;
                    logDirectoryPath = Application.StartupPath + "\\LOG";
                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            logDirectoryPath = logDirectoryPath.Replace("Program Files", Constants.xpPath);
                        else
                            logDirectoryPath = logDirectoryPath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                    catch
                    {
                    }
                    //Checking folder path to log Directory.
                    if (this.folderBrowserDialogService.SelectedPath == logDirectoryPath)
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG108"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else
                    //Assinging folder path to textbox.
                    {
                        try
                        {
                            dataGridViewMessageServices.CurrentRow.Cells[3].Value = this.folderBrowserDialogService.SelectedPath;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
        private void radGridView1_CommandCellClick(object sender, GridViewCellEventArgs e)
        {
            try
            {
                var GetCellIndex = e as GridViewCellEventArgs;
                var cell = e.Value as GridCommandCellElement;

                GridCommandCellElement gCommand = (sender as GridCommandCellElement);
                string buttonName = string.Empty;
                if (gCommand.CommandButton != null)
                    buttonName = gCommand.CommandButton.Text;
                //   GetCellIndex.
                // var cell = e as GridCommandCellElement;
                // var t = (sender as GridCommandCellElement).RowInfo.Cells[GetCellIndex.ColumnIndex];

                var RowInfo = e.Row;

                if (buttonName == "Delete")
                {
                    #region Remove Message Monitor Services

                    try
                    {
                        //Getting Data grid selected row collection from datagrid.
                        var dgvServicesRows = dataGridViewMessageServices.Rows[e.RowIndex];
                        if (dgvServicesRows != null)
                        {
                            if (dgvServicesRows.Cells[0] != null)
                            {
                                #region Remove Message Service Details from Registry and Database

                                try
                                {
                                    //Getting Monitor Service Name from Message Service grid.
                                    string monitorServiceName = dgvServicesRows.Cells[2].Value.ToString().Trim();
                                    string deleteMonitorServiceName = "SC DELETE " + "\"" + monitorServiceName + "\"";

                                    string result = RemoveMonitoringServices(deleteMonitorServiceName);

                                    if ((result.Contains(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG113").ToString().Replace("\n", string.Empty).Replace("\r", string.Empty))) ||
                                        (result.Contains("The specified service does not exist as an installed service.")))
                                    {
                                        #region Remove Message Service Details from Database
                                        //Getting Monitor ID from Message Service grid.
                                        int monitorId = Convert.ToInt32(dgvServicesRows.Cells[0].Value.ToString());
                                        //Delete query by adding parameters
                                        string commandText = string.Format(Queries.SQMS30, monitorId);
                                        //Execute query
                                        MySqlDataAcess.ExecuteNonQuery(commandText);

                                        #endregion

                                        //Services success message.
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG102"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        //Services refresh message.
                                        //MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG109"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        //Refreshing Services.
                                        string stopMonitorServiceName = "SC stop " + "\"" + monitorServiceName + "\"";
                                        string newresult = RemoveMonitoringServices(stopMonitorServiceName);

                                    }
                                    else
                                    {
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG114"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        // CommonUtilities.WriteErrorLog(result);
                                        return;
                                    }
                                    SetupMonitoringService.GetInstance().ServiceName = string.Empty;

                                }
                                catch (Exception ex)
                                {
                                    //Catching the exception.
                                    //CommonUtilities.WriteErrorLog(ex.Message);
                                    //CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;

                                }

                                #endregion

                            }
                            else
                            {
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG101"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }

                        }
                        else
                        {
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG101"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Catching the exception.
                        //CommonUtilities.WriteErrorLog(ex.Message);
                        //CommonUtilities.WriteErrorLog(ex.StackTrace);
                        MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    //Bind Refresh data.
                    BindServiceDetails();
                    return;
                    #endregion
                }
                else if (buttonName == "Cancel")
                {
                    BindServiceDetails();
                    return;
                }

                string folderPath = string.Empty;
                string isActive = string.Empty;
                string timePeriod = string.Empty;
                string serviceName = string.Empty;

                // IsActive
                if (RowInfo.Cells[1].Value != null && RowInfo.Cells[1].Value.ToString() != "")
                {
                    isActive = RowInfo.Cells[1].Value.ToString();
                }
                else
                {
                    //MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG106"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ////comboBoxPeriod.Focus();
                    //return;
                }
                // Name
                if (RowInfo.Cells[2].Value != null && RowInfo.Cells[2].Value.ToString() != "")
                {
                    serviceName = RowInfo.Cells[2].Value.ToString();
                }
                else
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG107"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                } // Path
                if (RowInfo.Cells[3].Value != null && RowInfo.Cells[3].Value.ToString() != "")
                {
                    folderPath = RowInfo.Cells[3].Value.ToString();
                    // MessageBox.Show(Path);
                    if (!System.IO.Directory.Exists(folderPath))
                    {
                        MessageBox.Show("Invalid Folder Path.Please select different Folder Path.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG105"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                //Period
                if (RowInfo.Cells[4].Value != null && RowInfo.Cells[4].Value.ToString() != "")
                {
                    timePeriod = RowInfo.Cells[4].Value.ToString();
                    //  MessageBox.Show(period);

                }
                else
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG106"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //comboBoxPeriod.Focus();
                    return;
                }
                if (buttonName == "Add")
                {
                    #region Save New Services



                    #region Validating Service Name and Saving Service into database.
                    //Checking Duplicates Services.
                    object countService = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMS32, serviceName.Trim()));
                    if (countService == null)
                    {
                        //Display message of Duplicate Service Name.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG110"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //textBoxServiceName.Focus();
                        return;
                    }
                    if (Convert.ToInt32(countService) > 0)
                    {
                        //Display message of Duplicate Service Name.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG110"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        // textBoxServiceName.Focus();
                        return;
                    }

                    //Storing the Message Monitoring Service details.
                    try
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG112"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        #region Creating and Installing Windows Monitoring Service

                        String exeName = string.Empty;
                        #region Commented code of ebay OsCommerse in rule
                        //if (comboBoxTradingPartner.SelectedIndex > 0)
                        //{
                        //    string commandText = string.Format(SQLQueries.Queries.eBaySQL0007, comboBoxTradingPartner.SelectedItem.ToString());
                        //    //Getting eBay Trading Partner details by passing Trading Partner name.
                        //    DataSet dsTradingPartnerDetails = DBConnection.MySqlDataAcess.ExecuteDataset(commandText);
                        //    if (dsTradingPartnerDetails != null)
                        //    {
                        //        if (dsTradingPartnerDetails.Tables[0].Rows.Count > 0)
                        //        {
                        //            if (dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString() == "4")
                        //            {
                        //                #region Convert eBay class to exe

                        //                string eBayToken = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();

                        //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                        //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                        //                string eBayType = dsTradingPartnerDetails.Tables[0].Rows[0]["CompanyName"].ToString();

                        //                if (string.IsNullOrEmpty(eBayType))
                        //                    eBayType = "SandBox";

                        //                string sourceName = Application.StartupPath + "\\eBay.cs";

                        //                //Create log directory for storing Messages.
                        //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                        //                //Adding Directory separate character to Logdirectory file path.
                        //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                        //                {
                        //                    logDirectory = Application.StartupPath + logDirectory;
                        //                    try
                        //                    {
                        //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        //                        else
                        //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        //                    }
                        //                    catch
                        //                    {
                        //                    }
                        //                }
                        //                //Checking if directory does not exists then create it.
                        //                if (!Directory.Exists(logDirectory))
                        //                {
                        //                    Directory.CreateDirectory(logDirectory);
                        //                }
                        //                //string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog " + textBoxServiceName.Text + ".txt";
                        //               // string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog\\" + textBoxServiceName.Text + ".txt";
                        //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                        //                try
                        //                {
                        //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        //                    else
                        //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        //                }
                        //                catch
                        //                {
                        //                }
                        //                string destName = logDirectory + "\\eBay.cs";

                        //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new eBay());  }";

                        //                string period = string.Empty;
                        //                switch (comboBoxPeriod.SelectedItem.ToString())
                        //                {
                        //                    case "1 minute":
                        //                        period = Convert.ToString(1 * 1000 * 60);
                        //                        break;
                        //                    case "5 minute":
                        //                        period = Convert.ToString(5 * 1000 * 60);
                        //                        break;
                        //                    case "10 minute":
                        //                        period = Convert.ToString(10 * 1000 * 60);
                        //                        break;
                        //                    case "15 minute":
                        //                        period = Convert.ToString(15 * 1000 * 60);
                        //                        break;
                        //                    case "30 minute":
                        //                        period = Convert.ToString(30 * 1000 * 60);
                        //                        break;
                        //                    case "45 minute":
                        //                        period = Convert.ToString(45 * 1000 * 60);
                        //                        break;
                        //                    case "1 hour":
                        //                        period = Convert.ToString(60 * 1000 * 60);
                        //                        break;
                        //                    case "2 hour":
                        //                        period = Convert.ToString(120 * 1000 * 60);
                        //                        break;
                        //                    case "5 hour":
                        //                        period = Convert.ToString(300 * 1000 * 60);
                        //                        break;
                        //                    case "10 hour":
                        //                        period = Convert.ToString(600 * 1000 * 60);
                        //                        break;
                        //                    case "12 hour":
                        //                        period = Convert.ToString(720 * 1000 * 60);
                        //                        break;
                        //                    default:
                        //                        period = Convert.ToString(10 * 1000 * 60);
                        //                        break;
                        //                }
                        //                //StreamReader strReader = new StreamReader(destName);
                        //                int lineCounter = 1;
                        //                StringBuilder sb = new StringBuilder();
                        //                //StreamWriter strWriter = new StreamWriter(destName, true);
                        //                string connectionString = string.Empty;
                        //                string file = string.Empty;
                        //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                        //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
                        //                using (StreamReader sr = new StreamReader(sourceName))
                        //                {

                        //                    string line;

                        //                    while ((line = sr.ReadLine()) != null)
                        //                    {
                        //                        if (line.Trim() == "}")
                        //                        {
                        //                            if (lineCounter == 1)
                        //                            {
                        //                                sb.AppendLine(line);
                        //                                sb.AppendLine();
                        //                                sb.AppendLine(addData);
                        //                                sb.AppendLine();
                        //                                lineCounter = 0;
                        //                            }
                        //                            else
                        //                                sb.AppendLine(line);
                        //                            //break;

                        //                        }
                        //                        else
                        //                        {
                        //                            if (line.Trim() == "private string connectionString = string.Empty;")
                        //                            {
                        //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                        //                            }
                        //                            else
                        //                                if (line.Trim() == "private string tpID = string.Empty;")
                        //                                {
                        //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                        //                                }
                        //                                else
                        //                                    if (line.Trim() == "private string mailID = string.Empty;")
                        //                                    {
                        //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                        //                                    }
                        //                                    else
                        //                                        if (line.Trim() == "private string eBayType = string.Empty;")
                        //                                        {
                        //                                            sb.AppendLine("private string eBayType = \"" + eBayType + "\";");
                        //                                        }
                        //                                    else
                        //                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                        //                                        {
                        //                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                        //                                        }
                        //                                        else
                        //                                            if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                        //                                            {
                        //                                                sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                        //                                            }
                        //                                            else
                        //                                                if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                        //                                                {
                        //                                                    sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                        //                                                }
                        //                                                else
                        //                                                    if (line.Trim() == "string eBayToken = string.Empty;")
                        //                                                    {
                        //                                                        sb.AppendLine("string eBayToken = @\"" + eBayToken + "\";");
                        //                                                    }
                        //                                                    else
                        //                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                        //                                                        {
                        //                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                        //                                                        }
                        //                                                        else
                        //                                                            sb.AppendLine(line);
                        //                        }

                        //                    }

                        //                }
                        //                using (StreamWriter strw = new StreamWriter(destName))
                        //                {
                        //                    strw.Write(sb.ToString());
                        //                }


                        //                FileInfo sourceFile = new FileInfo(destName);
                        //                CodeDomProvider provider = null;
                        //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                        //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                        //                //// Select the code provider based on the input file extension.
                        //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                        //                {
                        //                    provider = CodeDomProvider.CreateProvider("CSharp");
                        //                }


                        //                if (provider != null)
                        //                {

                        //                    // Format the executable file name.
                        //                    // Build the output assembly path using the current directory
                        //                    // and <source>_cs.exe or <source>_vb.exe.

                        //                    exeName = String.Format(@"{0}\{1}.exe",
                        //                        System.Environment.CurrentDirectory,
                        //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text + "_Edited" + DateTime.Now.Second.ToString()));

                        //                    CompilerParameters cp = new CompilerParameters();
                        //                    cp.ReferencedAssemblies.Add("System.dll");
                        //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                        //                    //cp.ReferencedAssemblies.Add("System.Net.dll");
                        //                    //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                        //                    //Microsoft.Office.Interop.Excel
                        //                    // Generate an executable instead of 
                        //                    // a class library.
                        //                    cp.GenerateExecutable = true;

                        //                    // Specify the assembly file name to generate.
                        //                    cp.OutputAssembly = exeName;

                        //                    // Save the assembly as a physical file.
                        //                    cp.GenerateInMemory = false;

                        //                    // Set whether to treat all warnings as errors.
                        //                    cp.TreatWarningsAsErrors = false;

                        //                    // Invoke compilation of the source file.
                        //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                        //                        destName);

                        //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                        //                    if (cr.Errors.Count > 0)
                        //                    {

                        //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //                        return;
                        //                    }

                        //                }

                        //                #endregion
                        //            }
                        //            else
                        //            {
                        //                #region Convert OSCommerce class to exe

                        //                string dbHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                        //                string dbUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["UserName"].ToString();
                        //                string dbPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["Password"].ToString();
                        //                if (dbPassword.Contains(";"))
                        //                    dbPassword = "'" + dbPassword + "'";
                        //                string dbName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCDatabase"].ToString();
                        //                string dbPrefix = dsTradingPartnerDetails.Tables[0].Rows[0]["XSDLocation"].ToString();

                        //                string dbPort = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                        //                string sshHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                        //                string sshPortNo = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayDevId"].ToString();
                        //                string sshUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayCertID"].ToString();
                        //                string sshPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAppId"].ToString();
                        //                if (sshPassword.Contains(";"))
                        //                    sshPassword = "'" + sshPassword + "'";

                        //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                        //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                        //                string sourceName = Application.StartupPath + "\\OSCommerce.cs";

                        //                //Create log directory for storing Messages.
                        //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                        //                //Adding Directory separate character to Logdirectory file path.
                        //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                        //                {
                        //                    logDirectory = Application.StartupPath + logDirectory;
                        //                    try
                        //                    {
                        //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        //                        else
                        //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        //                    }
                        //                    catch
                        //                    {
                        //                    }
                        //                }
                        //                //Checking if directory does not exists then create it.
                        //                if (!Directory.Exists(logDirectory))
                        //                {
                        //                    Directory.CreateDirectory(logDirectory);
                        //                }
                        //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog " + textBoxServiceName.Text + ".txt";
                        //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog\\" + textBoxServiceName.Text + ".txt";
                        //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                        //                try
                        //                {
                        //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        //                    else
                        //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        //                }
                        //                catch
                        //                {
                        //                }
                        //                string destName = logDirectory + "\\OSCommerce.cs";

                        //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new OSCommerce());  }";

                        //                string period = string.Empty;
                        //                switch (comboBoxPeriod.SelectedItem.ToString())
                        //                {
                        //                    case "1 minute":
                        //                        period = Convert.ToString(1 * 1000 * 60);
                        //                        break;
                        //                    case "5 minute":
                        //                        period = Convert.ToString(5 * 1000 * 60);
                        //                        break;
                        //                    case "10 minute":
                        //                        period = Convert.ToString(10 * 1000 * 60);
                        //                        break;
                        //                    case "15 minute":
                        //                        period = Convert.ToString(15 * 1000 * 60);
                        //                        break;
                        //                    case "30 minute":
                        //                        period = Convert.ToString(30 * 1000 * 60);
                        //                        break;
                        //                    case "45 minute":
                        //                        period = Convert.ToString(45 * 1000 * 60);
                        //                        break;
                        //                    case "1 hour":
                        //                        period = Convert.ToString(60 * 1000 * 60);
                        //                        break;
                        //                    case "2 hour":
                        //                        period = Convert.ToString(120 * 1000 * 60);
                        //                        break;
                        //                    case "5 hour":
                        //                        period = Convert.ToString(300 * 1000 * 60);
                        //                        break;
                        //                    case "10 hour":
                        //                        period = Convert.ToString(600 * 1000 * 60);
                        //                        break;
                        //                    case "12 hour":
                        //                        period = Convert.ToString(720 * 1000 * 60);
                        //                        break;
                        //                    default:
                        //                        period = Convert.ToString(10 * 1000 * 60);
                        //                        break;
                        //                }
                        //                //StreamReader strReader = new StreamReader(destName);
                        //                int lineCounter = 1;
                        //                StringBuilder sb = new StringBuilder();
                        //                //StreamWriter strWriter = new StreamWriter(destName, true);
                        //                string connectionString = string.Empty;
                        //                string file = string.Empty;
                        //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                        //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
                        //                using (StreamReader sr = new StreamReader(sourceName))
                        //                {

                        //                    string line;

                        //                    while ((line = sr.ReadLine()) != null)
                        //                    {
                        //                        if (line.Trim() == "}")
                        //                        {
                        //                            if (lineCounter == 1)
                        //                            {
                        //                                sb.AppendLine(line);
                        //                                sb.AppendLine();
                        //                                sb.AppendLine(addData);
                        //                                sb.AppendLine();
                        //                                lineCounter = 0;
                        //                            }
                        //                            else
                        //                                sb.AppendLine(line);
                        //                            //break;

                        //                        }
                        //                        else
                        //                        {
                        //                            if (line.Trim() == "private string connectionString = string.Empty;")
                        //                            {
                        //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                        //                            }
                        //                            else
                        //                                if (line.Trim() == "private string tpID = string.Empty;")
                        //                                {
                        //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                        //                                }
                        //                                else
                        //                                    if (line.Trim() == "private string mailID = string.Empty;")
                        //                                    {
                        //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                        //                                    }
                        //                                    else
                        //                                        if (line.Trim() == "private string dbHostName = string.Empty;")
                        //                                        {
                        //                                            sb.AppendLine("private string dbHostName = \"" + dbHostName + "\";");
                        //                                        }
                        //                                        else
                        //                                            if (line.Trim() == "private string dbUserName = string.Empty;")
                        //                                            {
                        //                                                sb.AppendLine("private string dbUserName = \"" + dbUserName + "\";");
                        //                                            }
                        //                                            else
                        //                                                if (line.Trim() == "private string dbPassword = string.Empty;")
                        //                                                {
                        //                                                    sb.AppendLine("private string dbPassword = \"" + dbPassword + "\";");
                        //                                                }
                        //                                                else
                        //                                                    if (line.Trim() == "private string dbName = string.Empty;")
                        //                                                    {
                        //                                                        sb.AppendLine("private string dbName = \"" + dbName + "\";");
                        //                                                    }
                        //                                                    else
                        //                                                        if (line.Trim() == "private string dbPrefix = string.Empty;")
                        //                                                        {
                        //                                                            sb.AppendLine("private string dbPrefix = \"" + dbPrefix + "\";");
                        //                                                        }
                        //                                                    else
                        //                                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                        //                                                        {
                        //                                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                        //                                                        }
                        //                                                        else
                        //                                                            if (line.Trim() == "private string sshHostName = string.Empty;")
                        //                                                            {
                        //                                                                sb.AppendLine("private string sshHostName = \"" + sshHostName + "\";");

                        //                                                            }
                        //                                                            else
                        //                                                                if (line.Trim() == "private string sshPortNo = string.Empty;")
                        //                                                                {
                        //                                                                    sb.AppendLine("private string sshPortNo = \"" + sshPortNo + "\";");

                        //                                                                }
                        //                                                                else
                        //                                                                    if (line.Trim() == "private string sshUserName = string.Empty;")
                        //                                                                    {
                        //                                                                        sb.AppendLine("private string sshUserName = \"" + sshUserName + "\";");

                        //                                                                    }
                        //                                                                    else
                        //                                                                        if (line.Trim() == "private string sshPassword = string.Empty;")
                        //                                                                        {
                        //                                                                            sb.AppendLine("private string sshPassword = \"" + sshPassword + "\";");

                        //                                                                        }
                        //                                                                        else
                        //                                                                            if (line.Trim() == "private string dbPort = string.Empty;")
                        //                                                                            {
                        //                                                                                sb.AppendLine("private string dbPort = \"" + dbPort + "\";");

                        //                                                                            }
                        //                                                                            else
                        //                                                                                if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                        //                                                                                {
                        //                                                                                    sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                        //                                                                                }
                        //                                                                                else
                        //                                                                                    if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                        //                                                                                    {
                        //                                                                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                        //                                                                                    }
                        //                                                                                    else

                        //                                                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                        //                                                                                        {
                        //                                                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                        //                                                                                        }
                        //                                                                                        else
                        //                                                                                            sb.AppendLine(line);
                        //                        }

                        //                    }

                        //                }
                        //                using (StreamWriter strw = new StreamWriter(destName))
                        //                {
                        //                    strw.Write(sb.ToString());
                        //                }


                        //                FileInfo sourceFile = new FileInfo(destName);
                        //                CodeDomProvider provider = null;
                        //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                        //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                        //                //// Select the code provider based on the input file extension.
                        //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                        //                {
                        //                    provider = CodeDomProvider.CreateProvider("CSharp");
                        //                }


                        //                if (provider != null)
                        //                {

                        //                    // Format the executable file name.
                        //                    // Build the output assembly path using the current directory
                        //                    // and <source>_cs.exe or <source>_vb.exe.

                        //                    exeName = String.Format(@"{0}\{1}.exe",
                        //                        System.Environment.CurrentDirectory,
                        //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text + "_Edited" + DateTime.Now.Second.ToString()));

                        //                    CompilerParameters cp = new CompilerParameters();
                        //                    cp.ReferencedAssemblies.Add("System.dll");
                        //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                        //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                        //                    cp.ReferencedAssemblies.Add("Tamir.SharpSSH.dll");
                        //                    cp.ReferencedAssemblies.Add("Org.Mentalis.Security.dll");
                        //                    cp.ReferencedAssemblies.Add("DiffieHellman.dll");
                        //                    cp.ReferencedAssemblies.Add("MySql.Data.dll");
                        //                    //Microsoft.Office.Interop.Excel
                        //                    // Generate an executable instead of 
                        //                    // a class library.
                        //                    cp.GenerateExecutable = true;

                        //                    // Specify the assembly file name to generate.
                        //                    cp.OutputAssembly = exeName;

                        //                    // Save the assembly as a physical file.
                        //                    cp.GenerateInMemory = false;

                        //                    // Set whether to treat all warnings as errors.
                        //                    cp.TreatWarningsAsErrors = false;

                        //                    // Invoke compilation of the source file.
                        //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                        //                        destName);

                        //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                        //                    if (cr.Errors.Count > 0)
                        //                    {

                        //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //                        return;
                        //                    }



                        //                }

                        //                #endregion
                        //            }
                        //        }
                        //        else
                        //        {
                        //            MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //            return;
                        //        }

                        //    }
                        //    else
                        //    {
                        //        MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //        return;
                        //    }


                        //}
                        //else
                        //{
                        #endregion


                        #region Convert class to exe

                        string sourceName = Application.StartupPath + "\\ServiceClass.cs";
                        //Create log directory for storing Messages.
                        string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");
                        //Adding Directory separate character to Logdirectory file path.
                        if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                        {
                            logDirectory = Application.StartupPath + logDirectory;
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                    logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                                else
                                    logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                            }
                            catch
                            {
                            }
                        }
                        //Checking if directory does not exists then create it.
                        if (!Directory.Exists(logDirectory))
                        {
                            Directory.CreateDirectory(logDirectory);
                        }
                        //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog " + textBoxServiceName.Text + ".txt";
                        // string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                        string logFileName = logDirectory + "\\" + serviceName + ".txt";

                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            else
                                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                        }
                        catch
                        {
                        }

                        string destName = logDirectory + "\\ServiceClass.cs";
                        //File.Copy(sourceName, destName,true);

                        string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new ServiceClass());  }";

                        string period = string.Empty;
                        switch (timePeriod)
                        {
                            case "1 minute":
                                period = Convert.ToString(1 * 1000 * 60);
                                break;
                            case "5 minute":
                                period = Convert.ToString(5 * 1000 * 60);
                                break;
                            case "10 minute":
                                period = Convert.ToString(10 * 1000 * 60);
                                break;
                            case "15 minute":
                                period = Convert.ToString(15 * 1000 * 60);
                                break;
                            case "30 minute":
                                period = Convert.ToString(30 * 1000 * 60);
                                break;
                            case "45 minute":
                                period = Convert.ToString(45 * 1000 * 60);
                                break;
                            case "1 hour":
                                period = Convert.ToString(60 * 1000 * 60);
                                break;
                            case "2 hour":
                                period = Convert.ToString(120 * 1000 * 60);
                                break;
                            case "5 hour":
                                period = Convert.ToString(300 * 1000 * 60);
                                break;
                            case "10 hour":
                                period = Convert.ToString(600 * 1000 * 60);
                                break;
                            case "12 hour":
                                period = Convert.ToString(720 * 1000 * 60);
                                break;
                            default:
                                period = Convert.ToString(10 * 1000 * 60);
                                break;
                        }
                        //StreamReader strReader = new StreamReader(destName);
                        int lineCounter = 1;
                        StringBuilder sb = new StringBuilder();
                        //StreamWriter strWriter = new StreamWriter(destName, true);
                        string connectionString = string.Empty;
                        string file = string.Empty;
                        //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                        connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("NewMySQLConnection");
                        using (StreamReader sr = new StreamReader(sourceName))
                        {

                            string line;

                            while ((line = sr.ReadLine()) != null)
                            {
                                if (line.Trim() == "}")
                                {
                                    if (lineCounter == 1)
                                    {
                                        sb.AppendLine(line);
                                        sb.AppendLine();
                                        sb.AppendLine(addData);
                                        sb.AppendLine();
                                        lineCounter = 0;
                                    }
                                    else
                                        sb.AppendLine(line);
                                    //break;

                                }
                                else
                                {
                                    if (line.Trim() == "this.ServiceName = string.Empty;")
                                    {
                                        sb.AppendLine("this.ServiceName = \"" + serviceName + "\";");

                                    }
                                    else if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                                    {
                                        sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                                        //sb.AppendLine("monitorTimer.Interval = " + period + ";");
                                    }
                                    else if (line.Trim() == "string directoryPath = string.Empty;")
                                    {
                                        sb.AppendLine("string directoryPath = @\"" + folderPath + "\";");
                                    }
                                    else if (line.Trim() == "con.ConnectionString = string.Empty;")
                                    {
                                        sb.AppendLine("con.ConnectionString = @\"" + connectionString + "\";");
                                    }
                                    else if (line.Trim() == "flinfo.CopyTo(string.Empty, true);")
                                    {
                                        string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                        try
                                        {
                                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                destPath = destPath.Replace("Program Files", Constants.xpPath);
                                            else
                                                destPath = destPath.Replace("Program Files", "Users\\Public\\Documents");
                                        }
                                        catch
                                        {
                                        }
                                        sb.AppendLine("flinfo.CopyTo(@" + destPath + ", true);");
                                    }
                                    else if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                                    {
                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                                    }
                                    else if (line.Trim() == "csvfl.CopyTo(string.Empty, true);")
                                    {
                                        string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                        try
                                        {
                                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                destPath = destPath.Replace("Program Files", Constants.xpPath);
                                            else
                                                destPath = destPath.Replace("Program Files", "Users\\Public\\Documents");
                                        }
                                        catch
                                        {
                                        }
                                        sb.AppendLine("csvfl.CopyTo(@" + destPath + ", true);");
                                    }
                                    else if (line.Trim() == "string serviceText = string.Empty;")
                                    {
                                        sb.AppendLine("string serviceText = \"" + serviceName + "\";");
                                    }
                                    else if (line.Trim() == "radDesktopAlert1.ContentText = \"File processed successfully: \" + string.Empty;")
                                    {
                                        sb.AppendLine("radDesktopAlert1.ContentText = \"File processed successfully: \" + Attachname;");
                                    }

                                    else
                                        sb.AppendLine(line);
                                }

                            }

                        }
                        using (StreamWriter strw = new StreamWriter(destName))
                        {
                            strw.Write(sb.ToString());
                        }


                        FileInfo sourceFile = new FileInfo(destName);
                        CodeDomProvider provider = null;
                        //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                        //ICodeCompiler provider = codeProvider.CreateCompiler();


                        //// Select the code provider based on the input file extension.
                        if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                        {
                            provider = CodeDomProvider.CreateProvider("CSharp");
                        }

                        exeName = string.Empty;
                        if (provider != null)
                        {

                            // Format the executable file name.
                            // Build the output assembly path using the current directory
                            // and <source>_cs.exe or <source>_vb.exe.

                            exeName = String.Format(@"{0}\{1}.exe",
                                System.Environment.CurrentDirectory,
                                sourceFile.Name.Replace(".", "_" + serviceName));

                            CompilerParameters cp = new CompilerParameters();
                            cp.ReferencedAssemblies.Add("System.dll");
                            cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                            cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                            cp.ReferencedAssemblies.Add("System.Configuration.dll");
                            cp.ReferencedAssemblies.Add("System.Data.dll");
                            cp.ReferencedAssemblies.Add("System.Xml.dll");
                            cp.ReferencedAssemblies.Add("MySql.Data.dll");
                            //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                            // Generate an executable instead of 
                            // a class library.
                            cp.GenerateExecutable = true;

                            // Specify the assembly file name to generate.
                            cp.OutputAssembly = exeName;

                            // Save the assembly as a physical file.
                            cp.GenerateInMemory = false;

                            // Set whether to treat all warnings as errors.
                            cp.TreatWarningsAsErrors = false;


                            // Invoke compilation of the source file.
                            CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                                destName);

                            //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                            if (cr.Errors.Count > 0)
                            {

                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG115"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }



                        }

                        #endregion


                        SetupMonitoringService.GetInstance().ServiceName = string.Empty;

                        #region Installing Service into System

                        //Adding Service Process Installer to class
                        ServiceProcessInstaller ProcesServiceInstaller = new ServiceProcessInstaller();
                        ProcesServiceInstaller.Account = ServiceAccount.LocalSystem;
                        ProcesServiceInstaller.Username = null;
                        ProcesServiceInstaller.Password = null;
                        //Creating Service installer object for service.
                        ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                        InstallContext Context = new System.Configuration.Install.InstallContext();
                        string svcApp = Application.StartupPath;

                        //Assinging path to Service class
                        String path = String.Format("/assemblypath={0}", exeName);
                        String[] cmdline = { path };
                        //Adding context into service.
                        Context = new System.Configuration.Install.InstallContext("", cmdline);
                        ServiceInstallerObj.Context = Context;
                        //Assinging display name to Service installer.
                        ServiceInstallerObj.DisplayName = "Zed Axis Service " + serviceName;
                        ServiceInstallerObj.Description = "Zed Axis Message Monitoring Service";
                        ServiceInstallerObj.ServiceName = serviceName;
                        if (isActive == "Active")
                            ServiceInstallerObj.StartType = ServiceStartMode.Automatic;
                        else
                            ServiceInstallerObj.StartType = ServiceStartMode.Manual;
                        ServiceInstallerObj.Parent = ProcesServiceInstaller;


                        System.Collections.Specialized.ListDictionary state = new System.Collections.Specialized.ListDictionary();
                        try
                        {
                            //Installing Service into Local Machine
                            ServiceInstallerObj.Install(state);

                            //Success message for creating service.
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG116"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ServiceController sc = new ServiceController(serviceName, System.Environment.MachineName);
                            if (isActive == "InActive")
                            {
                                //If status is inactive then service has been stopped.

                                try
                                {
                                    if (sc.Status == ServiceControllerStatus.Running)
                                    {
                                        //Service stopped.
                                        sc.Stop();
                                        sc.WaitForStatus(ServiceControllerStatus.Stopped);
                                    }
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    if (sc.Status != ServiceControllerStatus.Running)
                                    {
                                        //Service running.
                                        sc.Start();
                                        sc.WaitForStatus(ServiceControllerStatus.Running);
                                    }

                                }
                                catch
                                { }
                            }

                            #region Message Monitoring Service details into Database

                            int status = 0;
                            if (isActive == "Active")
                                status = 1;
                            string insertQuery = string.Empty;
                            insertQuery = string.Format(SQLQueries.Queries.SQMS28, serviceName, folderPath, timePeriod, status.ToString());

                            insertQuery = insertQuery.Replace("\\", "//");

                            //Performs the insert query into database.
                            DBConnection.MySqlDataAcess.ExecuteNonQuery(insertQuery);


                            #endregion
                            //Refresh Grid
                            BindServiceDetails();


                            return;
                        }
                        catch (SecurityException se)
                        {
                            //Log the security exceptions for unsufficient privileges.
                            CommonUtilities.WriteErrorLog(se.Message);
                            CommonUtilities.WriteErrorLog(se.Source);
                            //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG117"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        catch (Exception se)
                        {
                            //Log the exceptions for sufficient privileges.
                            CommonUtilities.WriteErrorLog(se.Message);
                            CommonUtilities.WriteErrorLog(se.Source);
                            //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG117"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        #endregion

                        #endregion

                    }
                    catch (Exception ex)
                    {
                        //Catching the exception.
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                        MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;

                    }
                    #endregion

                    #endregion
                }
                else if (buttonName == "Update")
                {
                    #region Remove Message Monitoring Service

                    string deleteServiceName = "SC DELETE " + "\"" + serviceName + "\"";

                    string result = RemoveMonitoringServices(deleteServiceName);

                    if (result.Contains(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG113").ToString().Replace("\n", string.Empty).Replace("\r", string.Empty)))
                    {
                        //Refreshing Services.
                        string stopServiceName = "SC stop " + "\"" + serviceName + "\"";
                        string newresult = RemoveMonitoringServices(stopServiceName);

                        newresult = null;

                    }
                    else
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CommonUtilities.WriteErrorLog(result);
                       // return;
                    }
                    #endregion

                    #region Convert class to exe

                    string sourceName = Application.StartupPath + "\\ServiceClass.cs";

                    //Create log directory for storing Messages.
                    string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                    //Adding Directory separate character to Logdirectory file path.
                    if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    {
                        logDirectory = Application.StartupPath + logDirectory;
                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            else
                                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                        }
                        catch
                        {

                        }
                    }
                    //Checking if directory does not exists then create it.
                    if (!Directory.Exists(logDirectory))
                    {
                        Directory.CreateDirectory(logDirectory);
                    }
                    //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog " + textBoxServiceName.Text + ".txt";
                    //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                    //string logFileName = logDirectory + "\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                    string logFileName = logDirectory + "\\" + serviceName + ".txt";

                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        else
                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                    }
                    catch
                    {

                    }

                    string destName = logDirectory + "\\ServiceClass.cs";

                    string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new ServiceClass());  }";

                    string period = string.Empty;
                    switch (timePeriod)
                    {
                        case "1 minute":
                            period = Convert.ToString(1 * 1000 * 60);
                            break;
                        case "5 minute":
                            period = Convert.ToString(5 * 1000 * 60);
                            break;
                        case "10 minute":
                            period = Convert.ToString(10 * 1000 * 60);
                            break;
                        case "15 minute":
                            period = Convert.ToString(15 * 1000 * 60);
                            break;
                        case "30 minute":
                            period = Convert.ToString(30 * 1000 * 60);
                            break;
                        case "45 minute":
                            period = Convert.ToString(45 * 1000 * 60);
                            break;
                        case "1 hour":
                            period = Convert.ToString(60 * 1000 * 60);
                            break;
                        case "2 hour":
                            period = Convert.ToString(120 * 1000 * 60);
                            break;
                        case "5 hour":
                            period = Convert.ToString(300 * 1000 * 60);
                            break;
                        case "10 hour":
                            period = Convert.ToString(600 * 1000 * 60);
                            break;
                        case "12 hour":
                            period = Convert.ToString(720 * 1000 * 60);
                            break;
                        default:
                            period = Convert.ToString(10 * 1000 * 60);
                            break;
                    }
                    //StreamReader strReader = new StreamReader(destName);
                    int lineCounter = 1;
                    StringBuilder sb = new StringBuilder();
                    //StreamWriter strWriter = new StreamWriter(destName, true);
                    string connectionString = string.Empty;
                    string file = string.Empty;
                    //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                    connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("NewMySQLConnection");
                    using (StreamReader sr = new StreamReader(sourceName))
                    {

                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Trim() == "}")
                            {
                                if (lineCounter == 1)
                                {
                                    sb.AppendLine(line);
                                    sb.AppendLine();
                                    sb.AppendLine(addData);
                                    sb.AppendLine();
                                    lineCounter = 0;
                                }
                                else
                                    sb.AppendLine(line);
                                //break;

                            }
                            else
                            {
                                if (line.Trim() == "this.ServiceName = string.Empty;")
                                {
                                    sb.AppendLine("this.ServiceName = \"" + serviceName + "\";");

                                }
                                else
                                    if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                                    {
                                        sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                                        //sb.AppendLine("monitorTimer.Interval = " + period + ";");
                                    }
                                    else

                                        if (line.Trim() == "string directoryPath = string.Empty;")
                                        {
                                            sb.AppendLine("string directoryPath = @\"" + folderPath + "\";");
                                        }
                                        else
                                            if (line.Trim() == "con.ConnectionString = string.Empty;")
                                            {
                                                sb.AppendLine("con.ConnectionString = @\"" + connectionString + "\";");
                                            }
                                            else
                                                if (line.Trim() == "flinfo.CopyTo(string.Empty, true);")
                                                {
                                                    string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                                    try
                                                    {
                                                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                            destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                        else
                                                            destPath = destPath.Replace("Program Files", "Users\\Public\\Documents");
                                                    }
                                                    catch
                                                    {
                                                    }
                                                    sb.AppendLine("flinfo.CopyTo(@" + destPath + ", true);");
                                                }
                                                else
                                                    if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                                                    {
                                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                                                    }
                                                    else
                                                        if (line.Trim() == "csvfl.CopyTo(string.Empty, true);")
                                                        {
                                                            string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";

                                                            try
                                                            {
                                                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                                    destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                                else
                                                                    destPath = destPath.Replace("Program Files", "Users\\Public\\Documents");
                                                            }
                                                            catch
                                                            {
                                                            }
                                                            sb.AppendLine("csvfl.CopyTo(@" + destPath + ", true);");
                                                        }
                                                        else
                                                            if (line.Trim() == "string serviceText = string.Empty;")
                                                            {
                                                                sb.AppendLine("string serviceText = \"" + serviceName + "\";");
                                                            }
                                                            else
                                                                sb.AppendLine(line);
                            }

                        }

                    }
                    using (StreamWriter strw = new StreamWriter(destName))
                    {
                        strw.Write(sb.ToString());
                    }


                    FileInfo sourceFile = new FileInfo(destName);
                    CodeDomProvider provider = null;
                    //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                    //ICodeCompiler provider = codeProvider.CreateCompiler();


                    //// Select the code provider based on the input file extension.
                    if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                    {
                        provider = CodeDomProvider.CreateProvider("CSharp");
                    }

                    String exeName = string.Empty;
                    if (provider != null)
                    {

                        // Format the executable file name.
                        // Build the output assembly path using the current directory
                        // and <source>_cs.exe or <source>_vb.exe.

                        exeName = String.Format(@"{0}\{1}.exe",
                            System.Environment.CurrentDirectory,
                            sourceFile.Name.Replace(".", "_" + serviceName));

                        CompilerParameters cp = new CompilerParameters();
                        cp.ReferencedAssemblies.Add("System.dll");
                        cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                        cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                        cp.ReferencedAssemblies.Add("System.Configuration.dll");
                        cp.ReferencedAssemblies.Add("System.Data.dll");
                        cp.ReferencedAssemblies.Add("System.Xml.dll");
                        cp.ReferencedAssemblies.Add("MySql.Data.dll");
                        //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                        //Microsoft.Office.Interop.Excel
                        // Generate an executable instead of 
                        // a class library.
                        cp.GenerateExecutable = true;

                        // Specify the assembly file name to generate.
                        cp.OutputAssembly = exeName;

                        // Save the assembly as a physical file.
                        cp.GenerateInMemory = false;

                        // Set whether to treat all warnings as errors.
                        cp.TreatWarningsAsErrors = false;

                        // Invoke compilation of the source file.
                        CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                            destName);

                        //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                        if (cr.Errors.Count > 0)
                        {

                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }



                    }

                    #endregion
                    // }

                    SetupMonitoringService.GetInstance().ServiceName = string.Empty;
                    #region Installing Service into System

                    //Adding Service Process Installer to class
                    ServiceProcessInstaller ProcesServiceInstaller = new ServiceProcessInstaller();
                    ProcesServiceInstaller.Account = ServiceAccount.LocalSystem;
                    ProcesServiceInstaller.Username = null;
                    ProcesServiceInstaller.Password = null;
                    //Creating Service installer object for service.
                    ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                    InstallContext Context = new System.Configuration.Install.InstallContext();
                    string svcApp = Application.StartupPath;

                    //Assinging path to Service class
                    String path = String.Format("/assemblypath={0}", exeName);
                    String[] cmdline = { path };
                    //Adding context into service.
                    Context = new System.Configuration.Install.InstallContext("", cmdline);
                    ServiceInstallerObj.Context = Context;
                    //Assinging display name to Service installer.
                    ServiceInstallerObj.DisplayName = "Zed Axis Service " + serviceName;
                    ServiceInstallerObj.Description = "Zed Axis Message Monitoring Service";
                    ServiceInstallerObj.ServiceName = serviceName;
                    if (isActive == "Active")
                        ServiceInstallerObj.StartType = ServiceStartMode.Automatic;
                    else
                        ServiceInstallerObj.StartType = ServiceStartMode.Manual;
                    ServiceInstallerObj.Parent = ProcesServiceInstaller;


                    System.Collections.Specialized.ListDictionary state = new System.Collections.Specialized.ListDictionary();
                    try
                    {
                        //Installing Service into Local Machine
                        ServiceInstallerObj.Install(state);

                        //Success message for updating service.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG119"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ServiceController sc = new ServiceController(serviceName, System.Environment.MachineName);
                        if (isActive == "InActive")
                        {
                            //If status is inactive then service has been stopped.

                            try
                            {
                                if (sc.Status == ServiceControllerStatus.Running)
                                {
                                    //Service stopped.
                                    sc.Stop();
                                    sc.WaitForStatus(ServiceControllerStatus.Stopped);
                                }
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                if (sc.Status != ServiceControllerStatus.Running)
                                {
                                    //Service running.
                                    sc.Start();
                                    sc.WaitForStatus(ServiceControllerStatus.Running);
                                }

                            }
                            catch
                            { }
                        }

                        #region Updating Service Details in Database

                        //Updating the Message Monitoring Service details.
                        try
                        {
                            int status = 0;
                            if (isActive == "Active")
                                status = 1;
                            object monitorId = MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMS34, serviceName));
                            if (monitorId != null)
                            {
                                string updateQuery = string.Empty;

                                //if (comboBoxTradingPartner.SelectedIndex > 0)

                                //    //Generate the MYSQL Query for update Message Monitoring data.
                                //    updateQuery = string.Format(SQLQueries.Queries.SQMS29, textBoxServiceName.Text, comboBoxTradingPartner.SelectedItem.ToString(), comboBoxPeriod.SelectedItem.ToString(), status.ToString(), monitorId.ToString());
                                //else
                                //Generate the MYSQL Query for update Message Monitoring data.
                                updateQuery = string.Format(SQLQueries.Queries.SQMS29, serviceName, folderPath, timePeriod, status.ToString(), monitorId.ToString());

                                updateQuery = updateQuery.Replace("\\", "//");

                                //Performs the update query into database.
                                DBConnection.MySqlDataAcess.ExecuteNonQuery(updateQuery);
                            }
                        }
                        catch (Exception ex)
                        {
                            //Catching the exception.
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                            MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;

                        }

                        #endregion
                        //Refresh grid values
                        BindServiceDetails();

                        return;
                    }
                    catch (SecurityException se)
                    {
                        //Log the security exceptions for unsufficient privileges.
                        CommonUtilities.WriteErrorLog(se.Message);
                        CommonUtilities.WriteErrorLog(se.Source);
                        //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    catch (Exception se)
                    {
                        //Log the exceptions for sufficient privileges.
                        CommonUtilities.WriteErrorLog(se.Message);
                        CommonUtilities.WriteErrorLog(se.Source);
                        //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    #endregion
                }
            }
            catch (Exception)
            {

                // throw;
            }

        }
        /// <summary>
        /// This event is used for binding Service data to grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MessageMonitoring_Load(object sender, EventArgs e)
        {
            #region Binding Service Grid

            BindServiceDetails();

            #endregion
        }

        /// <summary>
        /// This event is used for close Message Monitor form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            //Closing the Message Monitor form.
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// This event is used for display New Service Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNew_Click(object sender, EventArgs e)
        {
            #region Display new Service Form
            MessageMonitoringService.SetupMonitoringService.GetInstance().ServiceName = string.Empty;
            //Display Setup Monitoring Service form.
            MessageMonitoringService.SetupMonitoringService.GetInstance().ShowDialog();

            #endregion
        }

        /// <summary>
        /// This event is used for display Edit Service form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            #region Editing Service Details

            try
            {
                //Getting Data grid selected row collection from datagrid.
                GridViewSelectedRowsCollection dgvServicesRows = dataGridViewMessageServices.SelectedRows;
                if (dgvServicesRows != null)
                {
                    if (dgvServicesRows[0] != null)
                    {
                        MessageMonitoringService.SetupMonitoringService.GetInstance().ServiceName = dgvServicesRows[0].Cells[1].Value.ToString();
                        //Display Setup Monitoring Service form.
                        MessageMonitoringService.SetupMonitoringService.GetInstance().ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG101"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG101"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            catch (Exception ex)
            {
                //Catching the exception.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            #endregion
        }

        /// <summary>
        /// This event is used for remove Service from Database
        /// and Uninstall the Windows Service.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// This method is used for remove services from System.
        /// </summary>
        /// <param name="commandName">Passing Service Controller command</param>
        /// <returns></returns>
        protected string RemoveMonitoringServices(string commandName)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + commandName);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                //Returning result.
                return result;
            }
            catch (Exception objException)
            {
                // Log the exception
                //CommonUtilities.WriteErrorLog(objException.Message);
                //CommonUtilities.WriteErrorLog(objException.StackTrace);
                return string.Empty;
            }

        }

        #endregion
    }
}