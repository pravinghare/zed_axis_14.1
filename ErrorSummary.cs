using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TransactionImporter;

namespace DataProcessingBlocks
{
    public partial class ErrorSummary : Form
    {
        public string Message = string.Empty;
       
        public ErrorSummary(string message)
        {
            InitializeComponent();       
            Message = message;
        }

        private void buttonErrorMsgSkip_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void ErrorSummary_Load(object sender, EventArgs e)
        {
            labelQBErrorMessage.Text = string.Empty;
            labelQBErrorMessage.Text = Message;
            this.pictureBoxError.Image = System.Drawing.SystemIcons.Error.ToBitmap();
        }

        /// <summary>
        /// newly added in version 7.0 as per client requirement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonQuit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;  

            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                this.Close();
                this.Dispose();
                //DataProcessingBlocks.CommonUtilities.CloseSplash();
            }  
        }     
    }
}