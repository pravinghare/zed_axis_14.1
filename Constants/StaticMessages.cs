using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DataProcessingBlocks;

namespace EDI.Constant
{

    /// <summary>
    /// Static Class for message
    /// </summary>


    /// <summary>
    /// This method is used for provides static messages for application.
    /// </summary>


    public class StaticMessages
    {
        /// <summary>
        /// message for mandatory field which can be cancel or ignore.
        /// </summary>
        public static string ISM_Mandatory_Message = "The {0} for {1} is blank. It should not be blank. If you press Cancel this record will not be added to ISM Message. If Ignore it will proceed.";

        private string iSMFilePath = "c:\\Program Files\\temp\\";

        public  string ISMFilePath
        {
            get
            {
                return iSMFilePath;
            }
            set
            {
                iSMFilePath = value;
            }
        }




        public StaticMessages()
        {
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    iSMFilePath = iSMFilePath.Replace("Program Files", Constants.xpISMPath);
                else
                    iSMFilePath = iSMFilePath.Replace("Program Files", "Users\\Public\\Documents\\Zed\\Axis 9");
            }
            catch { }
        }

    }
}
