// ===============================================================================
// 
// Constants.cs
//
// This file contains the implementations of the constants values.
//
// Developed By : Sandeep Patil, K.Gouraw.
// Date : 
// Modified By :Sandeep Patil, K.Gouraw.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using DataProcessingBlocks;

namespace EDI.Constant
{
    /// <summary>
    /// This class provides constant values for application.
    /// </summary>
    internal class Constants
    {
        internal const string xpOSName = "XP";
        // internal const string xpPath = "Documents and Settings\\All Users\\Documents";
        internal const string xpPath = "Users\\Public\\Documents";
        internal const string xpmsgPath = @"C:\Documents and Settings\All Users\Documents\Zed";
        internal const string xpISMPath = "Documents and Settings\\All Users\\Documents\\Zed\\Axis10";
        internal const string ProgramDataPath = ":\\Users\\Public\\Documents\\Zed\\Axis10";
        internal const string QuickBooksPOSConnection = "Zed Systems\\QuickBooks POS\\Connection";

        internal const string QuickBooksOnlineConnection = "Zed Systems\\QuickBooks Online\\Connection";//11.1
        //Axis 10.0
        //Required to store Setting.xml,Coding.xml & Function.xml
        internal const string CommonActiveDir = @"c:\Users\Public\Documents\Zed";
        //Axis 11 POS
        internal const string importPOSPath = "Zed Systems\\QuickBooks POS\\Import";
        internal const string exportPOSPath = "Zed Systems\\QuickBooks POS\\Export";

        //Axis 11.1 Online
        internal const string importOnlinePath = "Zed Systems\\QuickBooks Online\\Import";
        internal const string exportOnlinePath = "Zed Systems\\QuickBooks Online\\Export";

        //Required path to save import settings in registry.
        internal const string importQuickBooksPath = "Zed Systems\\QuickBooks\\Import";
        internal const string importMYOBPath = "Zed Systems\\MYOB\\Import";
        //Axis 10.0 Changes
        internal const string importXeropath = "Zed Systems\\Xero\\Import";

        //Required path to save export settings in registry.
        internal const string exportQuickBooksPath = "Zed Systems\\QuickBooks\\Export";
        internal const string exportMYOBPath = "Zed Systems\\MYOB\\Export";

        //Required path to save LastExportDate settings in registry.
        internal const string lastExportQuickBooksPath = "Zed Systems\\QuickBooks\\LastExport";
        internal const string lastExportQBPOSPath = "Zed Systems\\QuickBooks POS\\LastExport";
        internal const string lastExportQBOnlinePath = "Zed Systems\\QuickBooks Online\\LastExport";
        internal const string lastExportMYOBPath = "Zed Systems\\MYOB\\LastExport";

        /// <summary>
        /// ErrorCode Message occur during connecting to QuickBook.
        /// </summary>        
        internal const string errorcode = "-2147220460";//Modal dialog box open
        internal const string errorcode1 = "-2147220469";//QuickBook closed through task manager
        internal const string errorcode2 = "-2147220454";//Access permission is denied to Application

        /// <summary>
        /// Default Axis Path where sample files and license files are saved.
        /// </summary>
        //internal const string AxisDocumentPath = ":\\Users\\Public\\Documents\\Zed\\Axis 4.3\\";
        internal const string AxisDocumentPath = ":\\Users\\Public\\Documents\\Zed\\Axis10\\";

        /// <summary>
        /// Errorcode for Office 2003/2007 COM object not created.
        /// </summary>
        internal const string officeErrorcode = "-2147221164";
        internal const string officeErrorcode1 = "-2146827284";
        /// <summary>
        /// Product Name
        /// </summary>
        public const string ProductName = "Zed Axis";

        public const string Appid = "73015724";
        /// <summary>
        /// This path describes Window Service System Registry Path.
        /// </summary>
        internal const string SystemServicePath = @"System\CurrentControlSet\Services";

        /// <summary>
        /// This block is used for DSN paths.
        /// </summary>
        internal const string SystemDsnPath = @"SOFTWARE\ODBC\ODBC.INI\ODBC Data Sources";
        internal const string UserDsnPath = @"Software\ODBC\ODBC.INI\ODBC Data Sources";
        internal const string FileDsnPath = @"C:\Program Files\Common Files\ODBC\Data Sources";
        internal const string GetMYOBODBC = "MYOAU0801";
        internal const string GetNewMYOBODBC = "MYOAU0901";
        internal const string OdbcDriverError = "Data source name not found and no default driver specified";
        /// <summary>
        /// This block contains constant values of Filters
        /// </summary>
        internal const string QuickBooksFilter = "QuickBooks Company File|*.qbw";
        internal const string MyobFilter = "MYOB Company Files(*.myo)|*.myo";
        internal const string MyobAppFilter = "Exe Files|*.exe";
        internal const string MyobRegKeyFilter = "MYOB Registration keys|*.key";
        /// <summary>
        /// Confirmation message of MYOB Connection
        /// </summary>
        internal const string MyobConMsg = "Application connected to {0} (MYOB Company file).";
        /// <summary>
        /// This block contains Data Type values.
        /// </summary>
        internal const string Customerstring = "Customer";
        internal const string Itemstring = "ItemInventory";
        internal const string SalesOrderString = "SalesOrder";
        internal const string PurchaseOrderString = "PurchaseOrder";
        internal const string InvoiceString = "Invoice";
        internal const string SalesReceiptsString = "SalesReceipt";
        internal const string CreditMemoString = "CreditMemo";
        internal const string ReceivePaymentsString = "ReceivePayment";
        internal const string BillsString = "Bill";
        internal const string BillPaymentChequeString = "BillPaymentCheck";
        internal const string BillPaymentCreditCardString = "BillPaymentCreditCard";
        internal const string ChecksString = "Check";
        internal const string CreditCardChargeString = "CreditCardCharge";
        internal const string JournalEntries = "JournalEntry";
        internal const string TimeEntries = "TimeTracking";
        internal const string PriceLevelString = "PriceLevel";
        internal const string BankTransferString = "Transfer";
        //Issue 402
        internal const string BuildAssemblyString = "BuildAssembly";

        //592
        internal const string TransferString = "Transfer";

        //new changes in version 6.0
        internal const string Vendorstring = "Vendor";
        internal const string Employeestring = "Employee";
        internal const string Otherstring = "OtherName";
        internal const string TransferInventorystring = "TransferInventory";
        internal const string NonInventoryItemstring = "ItemNonInventory";
        internal const string ItemServicestring = "ItemService";
        //bug no. 404
        internal const string ItemsStringOnline = "Item";
        internal const string CustomerStringOnline = "Customer";
        internal const string AccountString = "Account";
        internal const string RefundReceiptString = "RefundReceipt";
        //Axis 9.0
        internal const string ItemInventoryAssemblystring = "ItemInventoryAssembly";

        //Axis 8.0
        internal const string GeneralDetailReportstring = "GeneralDetailReport";
        internal const string GeneralSummaryReportstring = "GeneralSummaryReport";

        
        // Axis 9.0 
        internal const string Depositstring = "Deposit";
        internal const string EstimateString = "Estimate";
        //Axis 11.1
        internal const string PaymentString = "Payment";
        internal const string vendorCreditString = "VendorCredit";

        internal const string creditMemoString = "CreditMemo";
        internal const string billPaymentString = "BillPayment";
        internal const string purchaseString = "Purchase";

        // Axis 10.0
        internal const string Contactstring = "Contact";
        internal const string XeroItemstring = "Item";
        internal const string XeroJournalstring = "Journal";
        internal const string XeroInvoicestring = "Invoice-Supplier";
        internal const string XeroInvoicecustomerstring = "Invoice-Customer";
        internal const string XeroInvoicesstring = "Invoices";
        internal const string Bankstring = "Bank-Receive";
        internal const string BankSendString = "Bank-Spend";

        //Bug 551
        internal const string ItemGroupString = "ItemGroup";
        internal const string ClassString = "Class";
        internal const string BillingRateString = "BillingRate";

        internal const string ItemInventoryString = "ItemInventory";
        internal const string PriceAdjustmentString = "PriceAdjustment";
        internal const string PriceDiscountString = "PriceDiscount";
        internal const string InventoryCostAdjustmentString = "InventoryCostAdjustment";
        internal const string InventoryQtyAdjustmentString = "InventoryQtyAdjustment";
        internal const string TimeEntryString = "TimeEntry";
        internal const string VoucherString = "Voucher";
        internal const string DepartmentString = "Department";

        //Axis 12.0 bug 477
        internal const string InventorySiteString = "InventorySite";

        //Axis 12.0
        internal const string CreditCardCreditString = "CreditCardCredit";
        internal const string inventoryAdjustmentString = "InventoryAdjustment";
        internal const string VehicleMileageString = "VehicleMileage";
        internal const string ItemReceiptString = "ItemReceipt";
        internal const string ReportQueryBaseString = "ReportQueryBase";
        
        
        //Axis 8
        /// <summary>
        /// This block Contains Account Type values for Missing Checks Report type.
        /// </summary>
        #region MissingChecks Account Type
        internal const string AccountsPayable = "AccountsPayable";
        internal const string AccountsReceivable = "AccountsReceivable";
        internal const string AllowedFor1099 = "AllowedFor1099";
        internal const string APAndSalesTax = "APAndSalesTax";
        internal const string APOrCreditCard = "APOrCreditCard ";
        internal const string ARAndAP = "ARAndAP";
        internal const string Asset = "Asset";
        internal const string BalanceSheet = "BalanceSheet ";
        internal const string Bank = "Bank";
        internal const string BankAndARAndAPAndUF = "BankAndARAndAPAndUF";
        internal const string BankAndUF = "BankAndUF";
        internal const string CostOfSales = "CostOfSales";
        internal const string CreditCard = "CreditCard";
        internal const string CurrentAsset = "CurrentAsset";
        internal const string CurrentAssetAndExpense = "CurrentAssetAndExpense";
        internal const string CurrentLiability = "CurrentLiability";
        internal const string Equity = "Equity";
        internal const string EquityAndIncomeAndExpense = "EquityAndIncomeAndExpense";
        internal const string ExpenseAndOtherExpense = "ExpenseAndOtherExpense";
        internal const string FixedAsset = "FixedAsset";
        internal const string IncomeAndExpense = "IncomeAndExpense";
        internal const string IncomeAndOtherIncome = "IncomeAndOtherIncome";
        internal const string Liability = "Liability";
        internal const string LiabilityAndEquity = "LiabilityAndEquity";
        internal const string LongTermLiability = "LongTermLiability";
        internal const string NonPosting = "NonPosting";
        internal const string OrdinaryExpense = "OrdinaryExpense";
        internal const string OrdinaryIncome = "OrdinaryIncome";
        internal const string OrdinaryIncomeAndCOGS = "OrdinaryIncomeAndCOGS";
        internal const string OrdinaryIncomeAndExpense = "OrdinaryIncomeAndExpense";
        internal const string OtherAsset = "OtherAsset";
        internal const string OtherCurrentAsset = "OtherCurrentAsset";
        internal const string OtherCurrentLiability = "OtherCurrentLiability";
        internal const string OtherExpense = "OtherExpense";
        internal const string OtherIncome = "OtherIncome";
        internal const string OtherIncomeOrExpense = "OtherIncomeOrExpense";


        #endregion

        /// <summary>
        /// This block Contains Report Type values for GeneralDetailReport Type.
        /// </summary>

        #region GeneralDetailReport Type
        internal const string Detail1099 = "1099Detail";
        internal const string AuditTrail = "AuditTrail";
        internal const string BalanceSheetDetail = "BalanceSheetDetail";
        internal const string CheckDetail = "CheckDetail";
        internal const string CustomerBalanceDetail = "CustomerBalanceDetail";
        internal const string DepositDetail = "DepositDetail";
        internal const string EstimatesByJob = "EstimatesByJob";
        internal const string ExpenseByVendorDetail = "ExpenseByVendorDetail";
        internal const string GeneralLedger = "GeneralLedger";
        internal const string IncomeByCustomerDetail = "IncomeByCustomerDetail";
        internal const string IncomeTaxDetail = "IncomeTaxDetail";
        internal const string InventoryValuationDetail = "InventoryValuationDetail";
        internal const string JobProgressInvoicesVsEstimates = "JobProgressInvoicesVsEstimates";
        internal const string Journal = "Journal";
        internal const string MissingChecks = "MissingChecks";
        internal const string OpenInvoices = "OpenInvoices";
        internal const string OpenPOs = "OpenPOs";
        internal const string OpenPOsByJob = "OpenPOsByJob";
        internal const string OpenSalesOrderByCustomer = "OpenSalesOrderByCustomer";
        internal const string OpenSalesOrderByItem = "OpenSalesOrderByItem";
        internal const string PendingSales = "PendingSales";
        internal const string ProfitAndLossDetail = "ProfitAndLossDetail";
        internal const string PurchaseByItemDetail = "PurchaseByItemDetail";
        internal const string PurchaseByVendorDetail = "PurchaseByVendorDetail";
        internal const string SalesByCustomerDetail = "SalesByCustomerDetail";
        internal const string SalesByItemDetail = "SalesByItemDetail";
        internal const string SalesByRepDetail = "SalesByRepDetail";
        internal const string TxnDetailByAccount = "TxnDetailByAccount";
        internal const string TxnListByCustomer = "TxnListByCustomer";
        internal const string TxnListByDate = "TxnListByDate";
        internal const string TxnListByVendor = "TxnListByVendor";
        internal const string UnpaidBillsDetail = "UnpaidBillsDetail";
        internal const string UnbilledCostsByJob = "UnbilledCostsByJob";
        internal const string VendorBalanceDetail = "VendorBalanceDetail";

        #endregion


        #region DetailQBOReport
        internal const string ProfitAndLossQBO = "ProfitAndLossDetail";
        internal const string GeneralLedgerQBO = "GeneralLedger";
        internal const string TrailBalanceQBO = "TrailBalance";
       
        #endregion
        /// <summary>
        /// This block Contains Report Type values for GeneralSummaryReport Type.
        /// </summary>
        #region GeneralSummaryReportType

        internal const string BalanceSheetPrevYearComp = "BalanceSheetPrevYearComp";
        internal const string BalanceSheetStandard = "BalanceSheetStandard";
        internal const string BalanceSheetSummary = "BalanceSheetSummary";
        internal const string CustomerBalanceSummary = "CustomerBalanceSummary";
        internal const string ExpenseByVendorSummary = "ExpenseByVendorSummary";
        internal const string IncomeByCustomerSummary = "IncomeByCustomerSummary";
        internal const string InventoryStockStatusByItem = "InventoryStockStatusByItem";
        internal const string InventoryStockStatusByVendor = "InventoryStockStatusByVendor";
        internal const string IncomeTaxSummary = "IncomeTaxSummary";
        internal const string InventoryValuationSummary = "InventoryValuationSummary";
        internal const string PhysicalInventoryWorksheet = "PhysicalInventoryWorksheet";
        internal const string ProfitAndLossByClass = "ProfitAndLossByClass";
        internal const string ProfitAndLossByJob = "ProfitAndLossByJob";
        internal const string ProfitAndLossPrevYearComp = "ProfitAndLossPrevYearComp";
        internal const string ProfitAndLossStandard = "ProfitAndLossStandard";
        internal const string ProfitAndLossYTDComp = "ProfitAndLossYTDComp";
        internal const string PurchaseByItemSummary = "PurchaseByItemSummary";
        internal const string PurchaseByVendorSummary = "PurchaseByVendorSummary";
        internal const string SalesByCustomerSummary = "SalesByCustomerSummary";
        internal const string SalesByItemSummary = "SalesByItemSummary";
        internal const string SalesByRepSummary = "SalesByRepSummary";
        internal const string SalesTaxLiability = "SalesTaxLiability";
        internal const string SalesTaxRevenueSummary = "SalesTaxRevenueSummary";
        internal const string TrialBalance = "TrialBalance";
        internal const string VendorBalanceSummary = "VendorBalanceSummary";


        #endregion

        /// <summary>
        /// Contains service level DDL for shippping Reuqest.
        /// </summary>
        #region Service Level

        internal const string OvernightExpress = "C(Overnight Express)";
        internal const string Local = "L(Local)";
        internal const string RoadExpress = "R(RoadExpress)";
        internal const string SameDay = "DS(SameDay)";
        internal const string International = "IC(International)";
        internal const string Saturday = "SD(Saturday)";
        internal const string Pallet = "PT(Pallet)";

        #endregion



        /// <summary>
        /// This block contains Import Quote data into MYOB.
        /// </summary>

        #region Import Quote data Into MYOB

        internal const string MYOBQuoteQuery = "SELECT SALEID FROM SALES WHERE INVOICENUMBER = ";


        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportQuoteIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'Q')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportQuoteIntoMyobWithoutpremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                            ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                            "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CUrrencyCode,ExchangeRate,PaymentIsDue, " +
                                                            " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                             "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                            "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                            ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                            "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                            "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                             "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'Q')";

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportMulQuoteIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'Q')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportMulQuoteIntoMyobWithoutPremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                     ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                     "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CurrencyCode,ExchangeRate,PaymentIsDue, " +
                                                     " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                      "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                     "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                     ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                     "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate][PaymentIsDue], " +
                                                     "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                      "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'Q')";

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportQuoteItemsMyob = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'Q')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportQuoteItemsMyobWithoutPremier = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                    ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                    "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                    "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                     "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'Q')";



        #endregion


        /// <summary>
        /// This block contains Import Invoice data into MYOB.
        /// </summary>
        #region Import Invoice data Into MYOB

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportInvoiceIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Job,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'I')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportInvoiceIntoMyobWithoutPremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Job,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CurrencyCode,ExchangeRate,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                       "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'I')";

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportMulInvoiceIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Job,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'I')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportMulInvoiceIntoMyobWithoutPremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Job,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CurrencyCode,ExchangeRate,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                       "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'I')";

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportInvoiceItemsMyob = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'I')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportInvoiceItemsMyobWithoutPremier = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                   ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Job],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                   "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                   "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                    "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'I')";


        #endregion


        /// <summary>
        /// This block contains Import Order data into MYOB.
        /// </summary>
        #region Import Order data Into MYOB

        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportOrderIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'O')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportOrderIntoMyobWithoutPremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CUrrencyCode,ExchangeRate,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                       "VALUES([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'O')";
        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportMulOrderIntoMyob = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                       "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,PaymentIsDue, " +
                                                       " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                        "DrawerAccountName,DrawerChequeNumber,Category,CardID,RecordID,SaleStatus) " +
                                                       "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'O')";

        //This query is called when connection is done through MYOBEnterprise.Location field is included here.

        internal const string ImportMulOrderIntoMyobWithoutPremier = "INSERT INTO IMPORT_ITEM_SALES(CoLastName,AddressLine1,AddressLine2,AddressLine3,AddressLine4,Inclusive,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                     ",ItemNumber,DeliveryStatus,Quantity,Description,ExTaxPrice,IncTaxPrice,Disc3ount,ExTaxTotal,IncTaxTotal,Comment,Memo,SalespersonLastName,SalespersonFirstName,ShippingDate,TaxCode," +
                                                     "NonGSTLCTAmount,GSTAmount,LCT1Amount,Freight1Ex1Tax1Amount,Freight2Inc2Tax2Amount,Freight2Tax2Code,Freight2Non2GSTLCT2Amount,Freight2GST2Amount,FreightLCT2Amount,CurrencyCode,ExchangeRate,PaymentIsDue, " +
                                                     " DiscountDays,BalanceDueDays,PercentDiscount,PercentMonthlyCharge,AmountPaid,PaymentMethod,PaymentNotes,CardNumber,ExpiryDate,AuthorisationCode,DrawerBSB,DrawerAccountNumber," +
                                                      "DrawerAccountName,DrawerChequeNumber,Category,Location,CardID,RecordID,SaleStatus) " +
                                                     "VALUES(([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                     ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Disc3ount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                     "[NonGSTLCTAmount],[GSTAmount],[LCT1Amount],[Freight1Ex1Tax1Amount],[Freight2Inc2Tax2Amount],[Freight2Tax2Code],[Freight2Non2GSTLCT2Amount],[Freight2GST2Amount],[FreightLCT2Amount],[CurrencyCode],[ExchangeRate][PaymentIsDue], " +
                                                     "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                      "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'O')";
        //This query is called when connection is done through MYOB Premier19.Location field is not included here.

        internal const string ImportOrderItemsMyob = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                       ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                       "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[PaymentIsDue], " +
                                                       "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                        "[DrawerAccountName],[DrawerChequeNumber],[Category],[CardID],[RecordID],'O')";


        #endregion
        //This query is called when connection is done through MYOBEnterprise.Location field is included here.


        internal const string ImportOrderItemsMyobWithoutPremier = "([CoLastName],[AddressLine1],[AddressLine2],[AddressLine3],[AddressLine4],[Inclusive],[InvoiceNumber],[SaleDate],[CustomersNumber],[ShipVia]" +
                                                           ",[ItemNumber],[DeliveryStatus],[Quantity],[Description],[ExTaxPrice],[IncTaxPrice],[Discount],[ExTaxTotal],[IncTaxTotal],[Comment],[Memo],[SalespersonLastName],[SalespersonFirstName],[ShippingDate],[TaxCode]," +
                                                           "[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[FreightExTaxAmount],[FreightIncTaxAmount],[FreightTaxCode],[FreightNonGSTLCTAmount],[FreightGSTAmount],[FreightLCTAmount],[CurrencyCode],[ExchangeRate],[PaymentIsDue], " +
                                                           "[DiscountDays],[BalanceDueDays],[PercentDiscount],[PercentMonthlyCharge],[AmountPaid],[PaymentMethod],[PaymentNotes],[CardNumber],[ExpiryDate],[AuthorisationCode],[DrawerBSB],[DrawerAccountNumber]," +
                                                            "[DrawerAccountName],[DrawerChequeNumber],[Category],[Location],[CardID],[RecordID],'O')";




        //Axis 8.0 new changes to import moneyspend and moneyreceived.
        /// <summary>
        /// This block contains Import Money Received data into MYOB.
        /// </summary>
        #region Import Money Received data Into MYOB

        //internal const string MYOBMoneyReceivedQuery = "SELECT MONEYRECEIVEDID FROM MONEYRECEIVED WHERE TRANSACTIONID = ";

        internal const string ImportMoneyReceivedIntoMyob = "INSERT INTO IMPORT_RECEIVE_MONEY(DepositAccount,TransactionID,ReceiptDate,CoLastName,FirstName,Memo,Inclusive,AllocationAccount,ExTaxAmount," +
                                                            "IncTaxAmount,Job,TaxCode,NonGSTLCTAmount,GSTAmount,LCTAmount,CurrencyCode,ExchangeRate,PaymentMethod,DrawerAccountName,DrawerBSB," +
                                                            "DrawerAccountNumber,DrawerChequeNumber,CardNumber,NameOnCard,ExpiryDate,AuthorisationCode,PaymentNotes,AllocationMemo,Category,CardID,RecordID) " +
                                                            "VALUES(([DepositAccount],[TransactionID],[ReceiptDate],[CoLastName],[FirstName],[Memo],[Inclusive],'',[ ExTaxAmount]," +
                                                            "[IncTaxAmount],[Job],' ',[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[CurrencyCode],[ExchangeRate],[PaymentMethod],[DrawerAccountName],[DrawerBSB]," +
                                                            "[DrawerAccountNumber],[DrawerChequeNumber],[CardNumber],[NameOnCard],[ExpiryDate],[AuthorisationCode],[PaymentNotes],[AllocationMemo],[Category],[CardID],[RecordID])," +
                                                            "([DepositAccount],[TransactionID],[ReceiptDate],[CoLastName],[FirstName],[Memo],[Inclusive],[AllocationAccount],[ExTaxAmount]," +
                                                            "[IncTaxAmount],[Job],[TaxCode],[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[CurrencyCode],[ExchangeRate],[PaymentMethod],[DrawerAccountName],[DrawerBSB]," +
                                                            "[DrawerAccountNumber],[DrawerChequeNumber],[CardNumber],[NameOnCard],[ExpiryDate],[AuthorisationCode],[PaymentNotes],[AllocationMemo],[Category],[CardID],[RecordID]))";

        internal const string ImportMulMoneyReceivedInMyob = "INSERT INTO IMPORT_RECEIVE_MONEY(DepositAccount,TransactionID,ReceiptDate,CoLastName,FirstName,Memo,Inclusive,AllocationAccount,ExTaxAmount," +
                                                            "IncTaxAmount,Job,TaxCode,NonGSTLCTAmount,GSTAmount,LCTAmount,CurrencyCode,ExchangeRate,PaymentMethod,DrawerAccountName,DrawerBSB," +
                                                            "DrawerAccountNumber,DrawerChequeNumber,CardNumber,NameOnCard,ExpiryDate,AuthorisationCode,PaymentNotes,AllocationMemo,Category,CardID,RecordID) " +
                                                            "VALUES(([DepositAccount],[TransactionID],[ReceiptDate],[CoLastName],[FirstName],[Memo],[Inclusive],'',[ExTaxAmount]," +
                                                            "[IncTaxAmount],[Job],' ',[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[CurrencyCode],[ExchangeRate],[PaymentMethod],[DrawerAccountName],[DrawerBSB]," +
                                                            "[DrawerAccountNumber],[DrawerChequeNumber],[CardNumber],[NameOnCard],[ExpiryDate],[AuthorisationCode],[PaymentNotes],[AllocationMemo],[Category],[CardID],[RecordID])," +
                                                            "([DepositAccount],[TransactionID],[ReceiptDate],[CoLastName],[FirstName],[Memo],[Inclusive],[AllocationAccount],[ExTaxAmount]," +
                                                            "[IncTaxAmount],[Job],[TaxCode],[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[CurrencyCode],[ExchangeRate],[PaymentMethod],[DrawerAccountName],[DrawerBSB]," +
                                                            "[DrawerAccountNumber],[DrawerChequeNumber],[CardNumber],[NameOnCard],[ExpiryDate],[AuthorisationCode],[PaymentNotes],[AllocationMemo],[Category],[CardID],[RecordID]))";

        internal const string ImportMoneyReceivedItemsMyob = "([DepositAccount],[TransactionID],[ReceiptDate],[CoLastName],[FirstName],[Memo],[Inclusive],[AllocationAccount],[ExTaxAmount]," +
                                                             "[IncTaxAmount],[Job],[TaxCode],[NonGSTLCTAmount],[GSTAmount],[LCTAmount],[CurrencyCode],[ExchangeRate],[PaymentMethod],[DrawerAccountName],[DrawerBSB]," +
                                                             "[DrawerAccountNumber],[DrawerChequeNumber],[CardNumber],[NameOnCard],[ExpiryDate],[AuthorisationCode],[PaymentNotes],[AllocationMemo],[Category],[CardID],[RecordID])";


        #endregion


        /// <summary>
        /// This block contains Import Money Spend data into MYOB.
        /// </summary>
        #region Import Money Spend data Into MYOB

        internal const string ImportMoneySpendIntoMyob = "INSERT INTO IMPORT_SPEND_MONEY(ChequeAccount,ChequeNumber,PaymentDate,Inclusive,CoLastName,FirstName,PayeeLine1,PayeeLine2,PayeeLine3," +
                                                         "PayeeLine4,Memo,AllocationAccount,ExTaxAmount,IncTaxAmount,Job,TaxCode,NonGSTImportAmount,GSTAmount,ImportDutyAmount,AlreadyPrinted," +
                                                         "CurrencyCode,ExchangeRate,StatementText,AllocationMemo,Category,CardID,RecordID,DeliveryStatus)" +
                                                         "VALUES(([ChequeAccount],[ChequeNumber],[PaymentDate],[Inclusive],[CoLastName],[FirstName],[PayeeLine1],[PayeeLine2],[PayeeLine3]," +
                                                         "[PayeeLine4],[Memo],'',[ExTaxAmount],[IncTaxAmount],[Job],' ',[NonGSTImportAmount],[GSTAmount],[ImportDutyAmount],[AlreadyPrinted]," +
                                                         "[CurrencyCode],[ExchangeRate],[StatementText],[AllocationMemo],[Category],[CardID],[RecordID],[DeliveryStatus])," +
                                                         "([ChequeAccount],[ChequeNumber],[PaymentDate],[Inclusive],[CoLastName],[FirstName],[PayeeLine1],[PayeeLine2],[PayeeLine3]," +
                                                         "[PayeeLine4],[Memo],[AllocationAccount],[ExTaxAmount],[IncTaxAmount],[Job],[TaxCode],[NonGSTImportAmount],[GSTAmount],[ImportDutyAmount],[AlreadyPrinted]," +
                                                         "[CurrencyCode],[ExchangeRate],[StatementText],[AllocationMemo],[Category],[CardID],[RecordID],[DeliveryStatus]))";

        internal const string ImportMulMoneySpendInMyob = "INSERT INTO IMPORT_SPEND_MONEY(ChequeAccount,ChequeNumber,PaymentDate,Inclusive,CoLastName,FirstName,PayeeLine1,PayeeLine2,PayeeLine3," +
                                                         "PayeeLine4,Memo,AllocationAccount,ExTaxAmount,IncTaxAmount,Job,TaxCode,NonGSTImportAmount,GSTAmount,ImportDutyAmount,AlreadyPrinted," +
                                                         "CurrencyCode,ExchangeRate,StatementText,AllocationMemo,Category,CardID,RecordID,DeliveryStatus)" +
                                                         "VALUES(([ChequeAccount],[ChequeNumber],[PaymentDate],[Inclusive],[CoLastName],[FirstName],[PayeeLine1],[PayeeLine2],[PayeeLine3]," +
                                                         "[PayeeLine4],[Memo],'',[ExTaxAmount],[IncTaxAmount],[Job],' ',[NonGSTImportAmount],[GSTAmount],[ImportDutyAmount],[AlreadyPrinted]," +
                                                         "[CurrencyCode],[ExchangeRate],[StatementText],[AllocationMemo],[Category],[CardID],[RecordID],[DeliveryStatus])," +
                                                         "([ChequeAccount],[ChequeNumber],[PaymentDate],[Inclusive],[CoLastName],[FirstName],[PayeeLine1],[PayeeLine2],[PayeeLine3]," +
                                                         "[PayeeLine4],[Memo],[AllocationAccount],[ExTaxAmount],[IncTaxAmount],[Job],[TaxCode],[NonGSTImportAmount],[GSTAmount],[ImportDutyAmount],[AlreadyPrinted]," +
                                                         "[CurrencyCode],[ExchangeRate],[StatementText],[AllocationMemo],[Category],[CardID],[RecordID],[DeliveryStatus]))";

        internal const string ImportMoneySpendItemsMyob = "([ChequeAccount],[ChequeNumber],[PaymentDate],[Inclusive],[CoLastName],[FirstName],[PayeeLine1],[PayeeLine2],[PayeeLine3]," +
                                                         "[PayeeLine4],[Memo],[AllocationAccount],[ExTaxAmount],[IncTaxAmount],[Job],[TaxCode],[NonGSTImportAmount],[GSTAmount],[ImportDutyAmount],[AlreadyPrinted]," +
                                                         "[CurrencyCode],[ExchangeRate],[StatementText],[AllocationMemo],[Category],[CardID],[RecordID],[DeliveryStatus])";

        #endregion




        #region MYOB Queries for Shipping Request

        internal const string MYOBGetItemExist = "SELECT Count(*) FROM ITEMS WHERE ITEMNUMBER='{0}'";

        //This query 
        internal const string MYOBItemQuery1 = "SELECT C.CustomListText,I.CustomField1 FROM ITEMS I,CUSTOMLISTS C WHERE I.ITEMNUMBER = '{0}' And I.CustomList1ID = C.CustomListID And C.CustomListText in ('Length','Width','Height')";

        internal const string MYOBItemQuery2 = "SELECT C.CustomListText,I.CustomField2 FROM ITEMS I,CUSTOMLISTS C WHERE I.ITEMNUMBER = '{0}' And I.CustomList2ID = C.CustomListID And C.CustomListText in ('Length','Width','Height')";

        internal const string MYOBItemQuery3 = "SELECT C.CustomListText,I.CustomField3 FROM ITEMS I,CUSTOMLISTS C WHERE I.ITEMNUMBER = '{0}' And I.CustomList3ID = C.CustomListID And C.CustomListText in ('Length','Width','Height')";

        //Insert custom field to MYOB.
        internal const string MYOBItemQuery4 = "INSERT INTO IMPORT_CUSTOM_LIST (CustomListName, Type, Number) VALUES ('{0}', 'I', '{1}')";
        //Insert item with custom fields.
        internal const string MYOBItemQuery5 = "INSERT INTO IMPORT_ITEMS (ITEMNUMBER,ITEMNAME,ASSETACCOUNT,INCOMEACCOUNT,EXPENSEACCOUNT,Sell,Buy,CUSTOMLIST1,CUSTOMLIST2,CUSTOMLIST3,CUSTOMFIELD1,CUSTOMFIELD2,CUSTOMFIELD3) VALUES ('{0}','{1}','{2}','{3}','{4}','1','1','Length','Width','Height','{5}','{6}','{7}')";
        //Get the ItemNumber for an existing item.
        internal const string MYOBItemQuery6 = "SELECT ITEMNUMBER FROM ITEMS WHERE ITEMNAME = '{0}'";

        #endregion

        /// <summary>
        /// This block contains Item and account queries.
        /// </summary>
        #region MYOB Import Item and Customer Queries

        internal const string MYOBImportCheckItemExists = "SELECT Count(*) FROM ITEMS WHERE ITEMNUMBER='{0}'";

        internal const string MYOBImportCreateItem = "INSERT INTO IMPORT_ITEMS (ITEMNUMBER,ASSETACCOUNT,INCOMEACCOUNT,EXPENSEACCOUNT,Sell,TaxCodeWhenSold,Buy,ItemName,Description) VALUES('{0}','{1}','{2}','{3}','1','{4}','1','{5}','{6}')";

        internal const string MYOBImportCreateItemWithSellingPrice = "INSERT INTO IMPORT_ITEMS (ITEMNUMBER,ASSETACCOUNT,INCOMEACCOUNT,EXPENSEACCOUNT,Sell,TaxCodeWhenSold,Buy,SellingPrice,ItemName,Description) VALUES('{0}','{1}','{2}','{3}','1','{4}','1','{5}','{6}','{7}')";

        internal const string MYOBImportGetAccountNumber = "SELECT ACCOUNTNUMBER FROM ACCOUNTS WHERE ACCOUNTNAME = '{0}'";

        internal const string MYOBImportCheckCustomerExistsByCardID = "SELECT Count(*) FROM CUSTOMERS WHERE CARDRECORDID ='{0}'";

        internal const string MYOBImportCheckCustomerExistsByCardIdentification = "SELECT Count(*) FROM CUSTOMERS WHERE CardIdentification ='{0}'";

        internal const string MYOBImportCheckCustomerExistsByCoLastName = "SELECT COUNT(*) FROM CUSTOMERS WHERE LASTNAME ='{0}'";

        internal const string MYOBImportCheckCustomerExistsByCoFirstName = "SELECT COUNT(*) FROM CUSTOMERS WHERE FIRSTNAME ='{0}'";

        internal const string MYOBImportCheckCustomerExistsByCoFirstNameAndLastName = "SELECT COUNT(*) FROM CUSTOMERS WHERE LASTNAME ='{0}' AND FIRSTNAME ='{1}'";

        internal const string MYOBImportCreateCustomers = "INSERT INTO IMPORT_CUSTOMER_CARDS (CoLastName,FirstName,CardId,Address1AddressLine1,Address1AddressLine2,Address1AddressLine3,Address1AddressLine4) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')";

        internal const string MYOBImportCreateCustomersWithAddress = "INSERT INTO IMPORT_CUSTOMER_CARDS (CoLastName,FirstName,CardId,Address1AddressLine1,Address1AddressLine2,Address1AddressLine3,Address1AddressLine4,Address1Phone1,Address1Phone2,Address1Phone3,Address1Fax,Address1Email) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')";

        internal const string MYOBImportGetCardIDByFirstNameandLastName = "SELECT CARDIDENTIFICATION FROM CARDS WHERE CARDRECORDID = (SELECT CARDRECORDID FROM CUSTOMERS WHERE FIRSTNAME='{0}' AND LASTNAME='{1}')";

        internal const string MYOBImportGetCardIDByeBayUserID = "SELECT CARDIDENTIFICATION FROM CARDS WHERE CARDRECORDID = (SELECT CARDRECORDID FROM CUSTOMERS WHERE LASTNAME='{0}')";

        internal const string MYOBImportGetMaxCustomerCardID = "SELECT MAX(CARDIDENTIFICATION) FROM CARDS WHERE CARDTYPEID = 'C'";

        internal const string MYOBSelectItems = "SELECT ITEMNUMBER FROM ITEMS";

        #endregion

        /// <summary>
        /// This block contains Adding response into MYOB
        /// </summary>

        internal const string MYOBFetchAccountNumber = "SELECT ACCOUNTNUMBER FROM ACCOUNTS WHERE ACCOUNTNAME='Inventory Adjustment'";

        internal const string MYOBFetchUnitPriceOfItemSales = "SELECT BaseSellingPrice FROM ITEMS WHERE ITEMNUMBER='{0}'";

        internal const string MYOBFetchTaxCodeIDOfItemSales = "SELECT SellTaxCodeID FROM ITEMS WHERE ITEMNUMBER='{0}'";

        internal const string MYOBFetchTaxCodeOfTaxCodes = "SELECT TaxCode FROM TAXCODES WHERE TAXCODEID='{0}'";

        internal const string MYOBFetchTaxPercentageRateOfItemSales = "SELECT TaxPercentageRate FROM TAXCODES WHERE TAXCODE='{0}'";

        internal const string MYOBFetchTaxCode = "SELECT T.TAXCODE FROM TAXCODES T JOIN ITEMS I ON I.SELLTAXCODEID = T.TAXCODEID WHERE I.ITEMNUMBER ='{0}'";

        internal const string MYOBFetchTaxCodeList = "SELECT DISTINCT T.TAXCODE FROM TAXCODES T JOIN ITEMS I ON I.SELLTAXCODEID = T.TAXCODEID";

        internal const string MYOBFetchPurchaseTaxCode = "SELECT T.TAXCODE FROM TAXCODES T JOIN ITEMS I ON I.BUYTAXCODEID = T.TAXCODEID WHERE I.ITEMNUMBER ='{0}'";

        //new changes in version 6.0

        internal const string MYOBFetchDiscount = "SELECT Discount FROM ITEMSALELINESALL WHERE SALEID = (SELECT SALEID FROM SALES WHERE INVOICENUMBER='{0}')";

        //new changes in version 6.0
        internal const string MYOBFetchFirstName = "SELECT FIRSTNAME FROM CARDS WHERE CARDRECORDID = (SELECT SALESPERSONID FROM SALES WHERE INVOICENUMBER='{0}')";

        internal const string MYOBFetchLastName = "SELECT LASTNAME FROM CARDS WHERE CARDRECORDID = (SELECT SALESPERSONID FROM SALES WHERE INVOICENUMBER='{0}')";

        //new changes in version 8.0
        #region Queries required for Statement Import of MYOB

        //Get Bank Accounts
        internal const string MYOBGetBankAccounts = "SELECT ACCOUNTNAME FROM ACCOUNTS WHERE SUBACCOUNTCLASSIFICATIONID IN ('B','CC')";

        //Get all cards names
        internal const string MYOBGetCardsName = "SELECT NAME FROM CARDS";

        //Get all Accounts number and Names
        internal const string MYOBGetAccounts = "SELECT ACCOUNTNUMBER + '  ' + ACCOUNTNAME FROM ACCOUNTS WHERE TAXCODEID != 0 ORDER BY ACCOUNTNUMBER";

        //Get all Accounts number
        internal const string MYOBGetAccountNumber = "SELECT ACCOUNTNUMBER FROM ACCOUNTS";

        //Get all Items
        internal const string MYOBGetItems = "SELECT ITEMNUMBER + '  ' + ITEMNAME FROM ITEMS";

        //Get Jobs list
        internal const string MYOBGetJobsList = "SELECT JOBNUMBER + '  ' + JOBNAME FROM JOBS WHERE ISHEADER = 'N'";

        //Get all Tax Code list
        internal const string MYOBGetTaxCodeList = "SELECT TAXCODE FROM TAXCODES";

        //Get type of Account of selected account
        internal const string MYOBGetTypeOfAccount = "SELECT SUBACCOUNTCLASSIFICATIONID FROM ACCOUNTS WHERE ACCOUNTNAME = '{0}'";

        //Get AccountNumber of Selected account
        internal const string MYOBGetSelectedAccountNumber = "SELECT ACCOUNTNUMBER FROM ACCOUNTS WHERE ACCOUNTNAME = '{0}'";

        //Get TaxPercentage Rate of selected TaxCode
        internal const string MYOBGetTaxPercentage = "SELECT TAXPERCENTAGERATE FROM TAXCODES WHERE TAXCODE = '{0}'";

        //Get TaxThreshold for selected TaxCode
        internal const string MYOBGetTaxThreshold = "SELECT TAXTHRESHOLD FROM TAXCODES WHERE TAXCODE = '{0}'";

        //Get TaxCodeTypeID for TaxCode
        internal const string MYOBGetTaxCodeTypeID = "SELECT TAXCODETYPEID FROM TAXCODES WHERE TAXCODE = '{0}'";

        //Get TaxCodeID from TaxCode        
        internal const string MYOBGetTaxCodeId = "select taxcodeid from taxcodes where taxcode = '{0}'";

        //Get Percentage addition of Consolidated TaxCode child elements.
        internal const string MYOBGetPercentageTotal = "select sum(TaxPercentageRate) from TaxCodeConsolidations t JOIN TaxCodes c ON t.ElementTaxCodeID = c.TaxCodeID where consolidatedTaxcodeId = '{0}'";

        #endregion


        #region MYOB Accounts Related Queries

        //internal const string MYOBAssetAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Asset' AND ACCOUNTS.ISINACTIVE='N'";
        internal const string MYOBAssetAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Asset' AND ACCOUNTS.ISINACTIVE='N' AND ACCOUNTS.ACCOUNTTYPEID IN ('D','B')";

        //internal const string MYOBIncomeAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Income' AND ACCOUNTS.ISINACTIVE='N'";
        internal const string MYOBIncomeAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Income' AND ACCOUNTS.ISINACTIVE='N' AND ACCOUNTS.ACCOUNTTYPEID IN ('D','B')";

        //internal const string MYOBExpenseAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Expense' AND ACCOUNTS.ISINACTIVE='N'";
        internal const string MYOBExpenseAccounts = "SELECT ACCOUNTS.ACCOUNTNAME FROM ACCOUNTS JOIN ACCOUNTCLASSIFICATION ON ACCOUNTS.ACCOUNTCLASSIFICATIONID = ACCOUNTCLASSIFICATION.ACCOUNTCLASSIFICATIONID WHERE ACCOUNTCLASSIFICATION.DESCRIPTION='Expense' AND ACCOUNTS.ISINACTIVE='N' AND ACCOUNTS.ACCOUNTTYPEID IN ('D','B')";

        internal const string MYOBGetAccountNumberByAccountName = "SELECT ACCOUNTNUMBER FROM ACCOUNTS WHERE ACCOUNTNAME = '{0}'";
        #endregion

        /// <summary>
        /// This query is used for Getting UnitCost for Inventory Adjustments.
        /// </summary>
        internal const string MYOBUnitCostOfInventory = "SELECT TaxInclusiveLastPurchasePrice FROM ITEMS WHERE ITEMNUMBER='{0}'";

        internal const string MYOBDateFormat = "MM/dd/yyyy";

        #region Adding Inventory adjustment INTO MYOB(SA)
        internal const string AddInventoryAdjustmentsMyob = "INSERT INTO IMPORT_INVENTORY_ADJUSTMENTS (AdjustmentDate,Memo,ItemNumber,Quantity,Amount,Account,UnitCost)" +
                                                            " VALUES ('{0}','{1}','{2}',{3},{4},'52200',{5})";
        internal const string AddmulInventoryAdjustmentsMyob = "INSERT INTO IMPORT_INVENTORY_ADJUSTMENTS (AdjustmentDate,Memo,ItemNumber,Quantity,Amount,Account,UnitCost)" +
                                                            " VALUES ('{0}','{1}','{2}',{3},{4},'52200',{5})";
        internal const string AddmulInventoryAdjustmentsItemsMyob = "('{0}','','{1}',{2},{3},'52200',{4})";

        #endregion

        #region Adding Item Sales Into MYOB(PC)

        #region New query for newly added three fields
        internal const string AddInvoiceReceiveMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,Quantity,Description,Comment,SaleStatus,ShippingDate,Inclusive,RecordId,IncTaxPrice,IncTaxTotal,TaxCode,Discount,SalespersonFirstName,SalespersonLastName,FreightIncTaxAmount,Job) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9},'{10}','{11}','I','{12}','1','{13}',{14},{15},'{16}','{17}','{18}','{19}','{20}','{21}')";

        /*internal const string AddInvoiceReceiveMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                       ",ItemNumber,Quantity,Description,Comment,SaleStatus,ShippingDate,Inclusive,CUSTOMERID,IncTaxPrice,IncTaxTotal,TaxCode,Discount,SalespersonFirstName,SalespersonLastName) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9},'{10}','{11}','I','{12}','1','{13}',{14},{15},'{16}','{17}','{18}','{19}')";*/
        #endregion

        internal const string AddmulInvoiceReceiveMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,InvoiceNumber,SaleDate,CustomersNumber,ShipVia" +
                                                        ",ItemNumber,Quantity,Description,Comment,SaleStatus,ShippingDate,Inclusive,RecordId,IncTaxPrice,IncTaxTotal,TaxCode,Discount,SalespersonFirstName,SalespersonLastName,FreightIncTaxAmount,Job) VALUES(('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',{9},'{10}','{11}','I','{12}','1','{13}',{14},{15},'{16}','{17}','{18}','{19}','{20}','{21}')";

        internal const string AddmulInvoiceReceiveItemsMyob = "('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','I','{12}','1','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}')";


        #endregion

        internal const string GetCardRecordIdMyob = "SELECT CARDRECORDID FROM PURCHASES WHERE PURCHASENUMBER='{0}'";

        #region Adding Item Receipt into MYOB (TP)

        internal const string AddItemReceipt = "INSERT INTO IMPORT_ITEM_PURCHASES (PurchaseNumber,RecordID,ShippingDate,ItemNumber,Quantity,Received,Ordered,Comment,PurchaseStatus,Inclusive,IncTaxPrice,IncTaxTotal,TaxCode) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','O','1',{8},{9},'{10}')";
        internal const string AddmulItemReceiptItemsMyob = "('{0}','{1}','','{2}','{3}','{4}','{5}','','O','1',{6},{7},'{8}')";

        #endregion

        internal const string MYOBWarning1 = "default substituted";
        internal const string MYOBWarning2 = "ignored.";
        /// <summary>
        /// Display QuickBooks flash message.
        /// </summary>
        internal const string QBflashmessage = "Connecting to QuickBooks";
        internal const string QBPOSflashmessage = "Connecting to QuickBooks POS";
        internal const string QBOnlineflashmessage = "Connecting to QuickBooks Online";
        // internal const string QBPOSflashmessage = "Connecting to QuickBooks POS Version:11.0";
        internal const string Xeroflashmessage = "Connecting to Xero";
        internal const string ExportingDataFlashMessage = "Fetching the records from QuickBooks";

        // Axis 10.0
        internal const string ExportingDataFromXeroFlashMessage = "Fetching the records from Xero";

        internal const string Explorer = "explorer";
        internal const string IExplorer = "IExplore.exe";

        // Axis 10.2
        internal const string ExportLoadFilterMessage = "Retrieving lists for filters...";
        //Axis 11.2 
        //Flash message for progress bar
        internal const string CodingTemplateflashmessage = "Please wait,while the coding is being applied to the statement, click Quit if you wish to cancel this..";
        internal const string statementPrevflashmessage = "Please wait while Axis opens the statement and retrives the lists from QuickBooks...";
        internal const string statementDeleteRecords = "Please wait while Axis Deleting the Records......";

        //bug 486 sku lookup
        //SKU
        internal const string EmptySkuFieldError = "Required Parameter :ItemRef.SKU is missing in the request";
        internal const string EmptySkuFieldErrorExpense = "Required Parameter : line.ItemBasedExpenseLineDetail.ItemRef.SKU is missing in the request";
        internal const string EmptySkuFieldErrorSales = "Required Parameter : line.SalesItemLineDetail.ItemRef.SKU is missing in the request";
       

        /// <summary>
        /// This block contains web link addresses values.
        /// </summary>
        internal const string ChatID = "http://support.zed-systems.com/ChatScript.ashx?config=1&id=ControlID";
        internal const string BuyID = "https://www.zed-systems.com/axis-buy.html";
        internal const string SupportID = "http://support.zed-systems.com";
        internal const string ZedID = "https://www.zed-systems.com/company.html";
        internal const string OnlineDemoID = "https://www.zed-systems.com/axis-demo.html";
        internal const string HelpQb = "http://support.zed-systems.com/kb/a46/error-unable-to-connect-to-quickbooks-a-quickbooks";
        internal const string HelpQBO = "http://support.zed-systems.com/kb/a167/how-to-connect-to-quickbooks-online.aspx";
        internal const string HelpPOS = "https://support.zed-systems.com/kb/a300/how-to-connect-zed-axis-to-quickbooks-point-of-sale.aspx";

        internal const string HelpXero = "http://support.zed-systems.com/kb/a191/how-to-connect-to-xero";
        internal const string HelpMYOB = "http://support.zed-systems.com/KB/a66/connecting-to-myob-using-a-dsn.aspx";
        internal const string FeedbackID = "http://www.surveymonkey.com/s/PTBCVGH";

        /// <summary>
        /// This block contains Myob Connection values.
        /// </summary>
        internal const string MyobConnectionwithkey = @"Driver={MYOAU0801};TYPE=MYOB;UID={0};PWD={1};DATABASE={2};ACCESS_TYPE=READ_WRITE;HOST_EXE_PATH={0};KEY={1};NETWORK_PROTOCOL=NONET;DRIVER_COMPLETION=DRIVER_NOPROMPT;";
        internal const string MyobConnection = @"Driver={MYOAU0801};TYPE=MYOB;UID={0};PWD={1};DATABASE={2};ACCESS_TYPE=READ_WRITE;HOST_EXE_PATH={0};NETWORK_PROTOCOL=NONET;DRIVER_COMPLETION=DRIVER_NOPROMPT;";
        //internal const string MyobConnectionwithdsn = @"DSN={0};";
        internal const string MyobConnectionwithdsn = @"DSN={0};KEY=740B0DC1CE7E9DB1C04D2772103549B6C6648BC4B71705967A850543240B6599B207C6578FA686512201D0419B2E1100F8423F481E510DC33ECE22C15954FD94;ACCESS_TYPE=READ_WRITE;NETWORK_PROTOCOL=NONET;DRIVER_COMPLETION=DRIVER_NOPROMPT;SUPPRESS_WARNINGS=TRUE;";


        /// <summary>
        /// Validation string of numbers.
        /// </summary>
        internal const string validnumber = "\b0123456789";
        /// <summary>
        /// Validation string required for ItemPackage.
        /// </summary>
        internal const string validNumberForItem = "\b0123456789.";
        /// <summary>
        /// QuickBooks and Myob string.
        /// </summary>
        internal const string QBstring = "QuickBooks";
        internal const string QBPOSstring = "QuickBooks POS";
        internal const string QBOnlinestring = "QuickBooks Online";
        internal const string Myobstring = "MYOB";
        internal const string Xerostring = "Xero";
        internal const string MYOBBrowseMappingInfo = "Match the columns from the import file to the corresponding fields in MYOB";
        internal const string QBBrowseMappingInfo = "Match the columns from the import file to the corresponding fields in QuickBooks";

        /// <summary>
        /// Customer and item string.
        /// </summary>
        internal const string selectstring = "-Select-";
        internal const string CUSTOMERS = "CUSTOMERS";
        internal const string ITEMS = "ITEMS";
        internal const string SALES = "SALES";
        /// <summary>
        /// Date format of MYSQL.
        /// </summary>
        internal const string MySqlDateFormat = "yyyy-MM-dd HH:mm";
        internal const string MySqlDateFormatSaveRegestry = "yyyy-MM-dd";
        internal const string MySqlNewDateFormat = "yyyy-MM-dd HH:mm";
        /// <summary>
        /// Default Account and Settings type.
        /// </summary>
        internal const string Inventory = "InventoryPart";
        internal const string NonInventory = "NonInventoryPart";
        internal const string Service = "Service";

        /// <summary>
        /// Date format of MYOB.
        /// </summary>
        internal const string MyOBDateFormat = "yyyy-MM-dd hh:mm:ss";

        #region Customer Queries of MYOB
        /// <summary>
        /// MyOB Customer Query.
        /// </summary>
        internal const string MyobCustomerQuery = "SELECT CUSTOMERS.NAME, CUSTOMERS.CUSTOMERID , ADDRESS.CONTACTNAME,ADDRESS.PHONE1,ADDRESS.STREETLINE1,ADDRESS.STREETLINE2,ADDRESS.CITY,ADDRESS.STATE,ADDRESS.POSTCODE,ADDRESS.COUNTRY,ADDRESS.EMAIL FROM CUSTOMERS LEFT OUTER JOIN ADDRESS ON CUSTOMERS.CARDRECORDID = ADDRESS.CARDRECORDID  WHERE CUSTOMERS.CUSTOMERID >= {0} AND CUSTOMERS.CUSTOMERID <= {1} AND (ADDRESS.LOCATION = 1 OR ADDRESS.LOCATION IS NULL)";
        /// <summary>
        /// MYOB Customer Query of sales order.
        /// </summary>
        internal const string MyobCustomerWithSOQuery = "SELECT CUSTOMERS.NAME, CUSTOMERS.CUSTOMERID , ADDRESS.CONTACTNAME,ADDRESS.PHONE1,ADDRESS.STREETLINE1,ADDRESS.STREETLINE2,ADDRESS.CITY,ADDRESS.STATE,ADDRESS.POSTCODE,ADDRESS.COUNTRY,ADDRESS.EMAIL FROM CUSTOMERS LEFT OUTER JOIN ADDRESS ON CUSTOMERS.CARDRECORDID = ADDRESS.CARDRECORDID  WHERE CUSTOMERS.CUSTOMERID = {0} AND (ADDRESS.LOCATION = 1 OR ADDRESS.LOCATION IS NULL)";

        /// <summary>
        /// MYOB Supplier Query of Purchase Order.
        /// </summary>
        internal const string MYOBSupplierWithPOQuery = "SELECT SUPPLIERS.NAME, SUPPLIERS.SUPPLIERID , ADDRESS.CONTACTNAME,ADDRESS.PHONE1,ADDRESS.STREETLINE1,ADDRESS.STREETLINE2,ADDRESS.CITY,ADDRESS.STATE,ADDRESS.POSTCODE,ADDRESS.COUNTRY,ADDRESS.EMAIL FROM SUPPLIERS LEFT OUTER JOIN ADDRESS ON SUPPLIERS.CARDRECORDID = ADDRESS.CARDRECORDID  WHERE SUPPLIERS.SUPPLIERID = {0} AND (ADDRESS.LOCATION = 1 OR ADDRESS.LOCATION IS NULL)";
        #endregion

        #region Item Queries of MYOB
        /// <summary>
        /// MYOB Item Query.
        /// </summary>
        internal const string MyobItemQuery = "SELECT ItemID,ItemNumber,ItemName,SupplierItemNumber,BaseSellingPrice,SellUnitMeasure,CustomField1 FROM ITEMS WHERE ITEMID >= {0} AND ITEMID <= {1}";

        /// <summary>
        /// Myob Item Query of Sales Order.
        /// </summary>
        internal const string MyobItemWithSOQuery = "SELECT ItemID,ItemNumber,ItemName,SupplierItemNumber,BaseSellingPrice,SellUnitMeasure,CustomField1 FROM ITEMS WHERE ITEMID = {0} ";
        #endregion

        #region Sales Order MYOB Queries

        /// <summary>
        /// MYOB Sales Order OR1 Query with Date filter.
        /// </summary>
        internal const string MyobSalesOrderOR1QueryWithDate = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='OR'AND SALES.InvoiceDate >= '{0}' AND SALES.InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Sales Order OR1 Query with Invoice Date and Invoice Number filter.
        /// </summary>
        internal const string MyobSalesOrderOR1QueryWithDateAndInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='OR'AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}' AND SALES.InvoiceDate >= '{2}' AND SALES.InvoiceDate <= '{3}'";

        /// <summary>
        /// MYOB Sales Order OR1 Query with Invoice Number filter.
        /// </summary>
        internal const string MyobSalesOrderOR1QueryWithInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join  Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='OR' AND (SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}')";

        /// <summary>
        /// MYOB Sales Order Distinct Invoice Number query with date filter.
        /// </summary>
        internal const string MyobSalesOrderInvoiceNumberwithDate = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='OR' AND InvoiceDate >= '{0}' AND InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Sales Order Distinct Invoice Number Query with Date and Number Filter.
        /// </summary>
        internal const string MyobSalesOrderInvoiceNumberWithDateAndNumber = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='OR' AND InvoiceDate >= '{0}' AND InvoiceDate <= '{1}' AND InvoiceNumber >= '{2}' AND InvoiceNumber <= '{3}'";

        /// <summary>
        /// MYOB Sales Order Distinct Invoice Number Query With Number filter.
        /// </summary>
        internal const string MyobSalesOrderInvoiceNumberWithNumber = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='OR' AND InvoiceNumber >= '{0}' AND InvoiceNumber <= '{1}'";

        /// <summary>
        /// MYOB Sales Order OR2 Query.
        /// </summary>
        internal const string MYOBSalesOrderOR2QueryWithInvoiceNumber = "SELECT SALES.InvoiceNumber,ItemSaleLines.LineNumber,Items.ItemNumber,ItemSaleLines.Quantity,Items.Itemid FROM SALES JOIN ITEMSALELINES ON SALES.SaleId = ItemSaleLines.SaleId JOIN Items ON ItemSaleLines.ItemID = Items.Itemid WHERE SALES.INVOICESTATUSID ='OR' AND SALES.INVOICENUMBER='{0}'";


        /// <summary>
        /// MYOB Sales Order Query with Date filter.
        /// </summary>
        internal const string MyobSalesOrderQueryWithDate = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='OR' AND SALES.InvoiceDate >= '{0}' AND SALES.InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Sales Order Query with Invoice Date and Invoice Number filter.
        /// </summary>
        internal const string MyobSalesOrderQueryWithDateAndInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='OR' AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}' AND SALES.InvoiceDate >= '{2}' AND SALES.InvoiceDate <= '{3}'";

        /// <summary>
        /// MYOB Sales Order Query with Invoice Number filter.
        /// </summary>
        internal const string MyobSalesOrderQueryWithInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='OR' AND (SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}')";

        #endregion

        #region Invoice MYOB Queries

        /// <summary>
        /// MYOB Invoice OR1 Query with Date filter.
        /// </summary>
        internal const string MyobInvoiceOR1QueryWithDate = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='O'AND SALES.InvoiceDate >= '{0}' AND SALES.InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Invoice OR1 Query with Invoice Date and Invoice Number filter.
        /// </summary>
        internal const string MyobInvoiceOR1QueryWithDateAndInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='O'AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}' AND SALES.InvoiceDate >= '{2}' AND SALES.InvoiceDate <= '{3}'";

        /// <summary>
        /// MYOB Invoice OR1 Query with Invoice Number filter.
        /// </summary>
        internal const string MyobInvoiceOR1QueryWithInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod FROM sales Left outer join  Customers on Sales.CardRecordid = Customers.CardRecordId left outer join ShippingMethods on sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.invoicestatusid ='O' AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}'";

        /// <summary>
        /// MYOB Invoicer Distinct Invoice Number query with date filter.
        /// </summary>
        internal const string MyobInvoiceInvoiceNumberwithDate = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='O' AND InvoiceDate >= '{0}' AND InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Invoice Distinct Invoice Number Query with Date and Number Filter.
        /// </summary>
        internal const string MyobInvoiceInvoiceNumberWithDateAndNumber = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='O' AND InvoiceDate >= '{0}' AND InvoiceDate <= '{1}' AND InvoiceNumber >= '{2}' AND InvoiceNumber <= '{3}'";

        /// <summary>
        /// MYOB Invoice Distinct Invoice Number Query With Number filter.
        /// </summary>
        internal const string MyobInvoiceInvoiceNumberWithNumber = "SELECT DISTINCT InvoiceNumber FROM SALES WHERE InvoiceStatusID ='O' AND InvoiceNumber >= '{0}' AND InvoiceNumber <= '{1}'";

        /// <summary>
        /// MYOB Invoice OR2 Query.
        /// </summary>
        internal const string MYOBInvoiceOR2QueryWithInvoiceNumber = "SELECT SALES.InvoiceNumber,ItemSaleLines.LineNumber,Items.ItemNumber,ItemSaleLines.Quantity,Items.Itemid FROM SALES JOIN ITEMSALELINES ON SALES.SaleId = ItemSaleLines.SaleId JOIN Items ON ItemSaleLines.ItemID = Items.Itemid WHERE SALES.INVOICESTATUSID ='O' AND SALES.INVOICENUMBER='{0}'";


        /// <summary>
        /// MYOB Invoice Query with Date filter.
        /// </summary>
        internal const string MyobInvoiceQueryWithDate = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='O' AND SALES.InvoiceDate >= '{0}' AND SALES.InvoiceDate <= '{1}'";

        /// <summary>
        /// MYOB Invoice Query with Invoice Date and Invoice Number filter.
        /// </summary>
        internal const string MyobInvoiceQueryWithDateAndInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.InvoiceDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='O' AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}' AND SALES.InvoiceDate >= '{2}' AND SALES.InvoiceDate <= '{3}'";

        /// <summary>
        /// MYOB Invoice Query with Invoice Number filter.
        /// </summary>
        internal const string MyobInvoiceQueryWithInvoiceNumber = "SELECT sales.InvoiceNumber,Customers.CustomerID,sales.PromisedDate,sales.ShipToAddress,sales.ShipToAddressLine1,sales.ShipToAddressline2,sales.ShipToAddressline3,sales.ShipToAddressline4,sales.CustomerPONumber,sales.Comment,ShippingMethods.ShippingMethod,ItemSaleLines.LineNUmber,Items.ItemNumber,ItemSaleLines.Quantity FROM sales join ItemSaleLines on Sales.SaleId=ItemSaleLines.SaleId join Items on ItemSaleLines.ItemID = Items.Itemid left outer join Customers ON Sales.CardRecordid = Customers.CardRecordId LEFT outer join ShippingMethods ON sales.ShippingMethodID = ShippingMethods.ShippingMethodID WHERE sales.InvoiceStatusID ='O' AND SALES.INVOICENUMBER >= '{0}' AND SALES.INVOICENUMBER <= '{1}'";

        #endregion

        #region Purchase Order MYOB Queries
        /// <summary>
        /// MYOB Purchase order query with purchase number.
        /// </summary>
        internal const string MYOBPurchaseOrderQuerywithPurchaseNumber = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE,ITEMS.ITEMNUMBER,ItemPurchaseLines.QUANTITY FROM purchases join ItempurchaseLines on purchases.purchaseId=ItempurchaseLines.purchaseId join Items on ItemPurchaseLines.ItemID = Items.Itemid left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE (PURCHASES.PURCHASENUMBER >= '{0}' AND PURCHASES.PURCHASENUMBER <= '{1}') AND PURCHASES.PurchaseStatusID='OR'";

        /// <summary>
        /// MYOB Purchase order query with purchase number and purchase date.
        /// </summary>
        internal const string MYOBPurchaseOrderQuerywithPurchaseNumberAndPurchaseDate = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE,ITEMS.ITEMNUMBER,ItemPurchaseLines.QUANTITY FROM purchases join ItempurchaseLines on purchases.purchaseId=ItempurchaseLines.purchaseId join Items on ItemPurchaseLines.ItemID = Items.Itemid left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE ((PURCHASES.PURCHASENUMBER >= '{0}' AND PURCHASES.PURCHASENUMBER <= '{1}') AND (PURCHASEDATE >= '{2}' AND PURCHASEDATE <= '{3}')) AND (PURCHASES.PurchaseStatusID='OR')";


        /// <summary>
        /// MYOB Purchase order query with purchase date.
        /// </summary>
        internal const string MYOBPurchaseOrderQuerywithPurchaseDate = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE,ITEMS.ITEMNUMBER,ItemPurchaseLines.QUANTITY FROM purchases join ItempurchaseLines on purchases.purchaseId=ItempurchaseLines.purchaseId join Items on ItemPurchaseLines.ItemID = Items.Itemid left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE (PURCHASEDATE >= '{0}' AND PURCHASEDATE <= '{1}') AND (PURCHASES.PurchaseStatusID='OR')";


        /// <summary>
        /// MYOB Purchase order PT1 query with purchase number.
        /// </summary>
        internal const string MYOBPurchasePT1OrderQuerywithPurchaseNumber = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE FROM purchases left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE (PURCHASES.PURCHASENUMBER >= '{0}' AND PURCHASES.PURCHASENUMBER <= '{1}') AND (PURCHASES.PurchaseStatusID='OR')";

        /// <summary>
        /// MYOB Purchase PT1 order query with purchase number and purchase date.
        /// </summary>
        internal const string MYOBPurchasePT1OrderQuerywithPurchaseNumberAndPurchaseDate = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE FROM purchases left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE ((PURCHASES.PURCHASENUMBER >= '{0}' AND PURCHASES.PURCHASENUMBER <= '{1}') AND (PURCHASEDATE >= '{2}' AND PURCHASEDATE <= '{3}')) AND (PURCHASES.PurchaseStatusID='OR')";


        /// <summary>
        /// MYOB Purchase PT1 order query with purchase date.
        /// </summary>
        internal const string MYOBPurchasePT1OrderQuerywithPurchaseDate = "SELECT PURCHASES.PURCHASENUMBER,SUPPLIERS.SUPPLIERID,PURCHASES.PROMISEDDATE FROM purchases left outer join Suppliers ON Purchases.CardRecordid = Suppliers.CardRecordId WHERE (PURCHASES.PURCHASEDATE >= '{0}' AND PURCHASES.PURCHASEDATE <= '{1}') AND (PURCHASES.PurchaseStatusID='OR')";

        /// <summary>
        /// MYOB Purchase PT2 order query with purchase number.
        /// </summary>
        internal const string MYOBPurchasePT2OrderQuerywithPurchaseNumber = "SELECT PURCHASES.PURCHASENUMBER,ITEMS.ITEMNUMBER,ITEMS.ITEMID,ItemPurchaseLines.QUANTITY FROM purchases join ItempurchaseLines on purchases.purchaseId=ItempurchaseLines.purchaseId join Items on ItemPurchaseLines.ItemID = Items.Itemid WHERE PURCHASES.PURCHASENUMBER = '{0}'";

        /// <summary>
        /// This method is used for getting minimum purchase number.
        /// </summary>
        internal const string MYOBMinimumPurchaseNumber = "SELECT MIN(PURCHASENUMBER) FROM PURCHASES WHERE PurchaseStatusID='OR'";

        /// <summary>
        /// This method is used for getting maximum purchase number.
        /// </summary>
        internal const string MYOBMaximumPurchaseNumber = "SELECT MAX(PURCHASENUMBER) FROM PURCHASES WHERE PurchaseStatusID='OR'";
        #endregion


        /// <summary>
        /// ISM File Header Information.
        /// </summary>
        internal const string ISMHeader = "HD";
        internal const string ISMCustomerStream = "RC";
        internal const string ISMItemStream = "IT";
        internal const string ISMSalesOrderStream = "OR";
        internal const string ISMPurchaseOrderStream = "PT";
        internal const string ISMPurchaseOrderOneStream = "PT1";
        internal const string ISMPurchaseOrderTwoStream = "PT2";
        internal const string ISMSalesOrderOneStream = "OR1";
        internal const string ISMSalesOrderTwoStream = "OR2";
        internal const string ISMSalesOrderFourStream = "OR4";
        internal const string ISMActionIndicaterU = "U";
        internal const string ISMActionIndicaterA = "A";
        internal const string ISMVendorStream = "RC";

        //new changes in version 6.0
        internal const string ISMSalesReceiptStream = "OR";
        internal const string ISMSalesReceiptOneStream = "OR1";
        internal const string ISMSalesReceiptTwoStream = "OR2";
        internal const string ISMSalesReceiptFourStream = "OR4";

        //new changes in version 6.0
        internal const string ISMInvoiceStream = "OR";
        internal const string ISMInvoiceOneStream = "OR1";
        internal const string ISMInvoiceTwoStream = "OR2";
        internal const string ISMInvoiceFourStream = "OR4";

        //new changes in version 6.0
        internal const string ISMEmployeeStream = "RC";

        //new changes in version 6.0
        internal const string ISMOtherStream = "RC";

        /// <summary>
        /// Delimeter used in .ism file.
        /// </summary>
        internal const string ISMDelimiter = "|";

        internal const string ISMFileNameString = "yyyyMMddHHmmss";
        internal const string connectstring = "Connect";
        //internal const string connectxerostring = "Connect";
        /// <summary>
        /// ISIS file format extension name.
        /// </summary>
        internal const string ISMExtension = ".ism";

        internal const string Disconnectstring = "Disconnect";
        //  internal const string DiscconectedXeroString = "Discoonect";

        internal const string ISMParameter = "parameterName";

        //public string ISMFilePath = Application.StartupPath + "\\temp\\";

        internal const string MinCustomerId = "This is smallest customer id of Myob";

        internal const string MaxCustomerId = "This is largest customer id of Myob";

        internal const string MaxItemId = "This is largest item id of Myob";


        /// <summary>
        /// Logic protect Product Name
        /// </summary>
        public const string OldLPProductName = "Zed Axis 3.0";

        /// <summary>
        /// Logic protect Product Name
        /// </summary>
      //  public const string LPProductName = "LPProductNameZed Axis 11.3";
       
        public const string LPProductName = "Zed Axis 14";
        public const int ProductVersion = 14;

        
        /// <summary>
        /// Host url for logic protect.
        /// </summary>
        public const string Host = "https://www.zed-systems.com/lps";
      //  public const string Host = "http://www.zed-systems.com/lps";

        /// <summary>
        /// Triald days logic protect.
        /// </summary>
        public const int TrialDays = 30;

        /// <summary>
        /// Dialog Title logic protect.
        /// </summary>
        public const string DialogTitle = "Software license activation for Zed Axis";

        /// <summary>
        /// Email validation string.
        /// </summary>
        //  public const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        //+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        //+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        //+ @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

        public const string MatchEmailPattern = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";


        /// <summary>
        /// Numeric field validation
        /// </summary>
        public const string Numeric_Pattern = @"^\d*$";

        /// <summary>
        /// alpha numeric field validation.
        /// </summary>
        public const string AlphNumeric_Pattern = "^[a-zA-Z0-9]*$";
        /// <summary>
        /// mysql download link
        /// </summary>
        //public const string MySqlDownLoadLink = "http://dev.mysql.com/downloads/mysql/";
        //public const string MySqlDownLoadLink = "http://support.zed-systems.com/kb/a45/how-to-install-the-database-for-zed-axis-edi";
        //AXIS-590
        public const string MySqlDownLoadLink = "https://www.zed-systems.com/downloads/InstallAxisdatabase.msi";
        public const string RegHelpLink = "http://support.zed-systems.com/kb/a307/error-registry-key-for-setting-ie-web-browser-not-found.aspx";
        //AXIS-590 end
        
        /// <summary>
        /// MySql Script File name.
        /// </summary>
        public const string MySQLScriptFile = @"\MySqlScript.txt";

        /// <summary>
        /// Supportive file for TDP-2456 Plus Printer.
        /// </summary>
        public const string ULPCXFilePath = @"\UL.PCX";

        /// <summary>
        /// New line and carriage return string. Used in parsing functions
        /// </summary>
        public const string CRLF = "\r\n";

        /// <summary>
        /// New line and carriage return string. Used in parsing functions
        /// </summary>
        public const string NEWLINE = "\n";

        /// <summary>
        /// 
        /// </summary>
        internal const string ISMFileDateTime = "yyyyMMddHHmmss";

        #region Constants About eBay
        /// <summary>
        /// This eBay Sandbox url is used for Testing Purpose.
        /// </summary>
        internal const string eBaySandBoxUrl = @"https://api.sandbox.ebay.com/wsapi";

        /// <summary>
        /// This eBay Production Url is used for Production Purpose.
        /// </summary>
        internal const string eBayProductionUrl = @"https://api.ebay.com/wsapi";

        /// <summary>
        /// This eBay Version is used for Transactions.
        /// </summary>
        internal const string eBayVersion = "551";

        #endregion

        internal const string GetCustomerRecordID = "SELECT CARDRECORDID FROM CUSTOMERS WHERE LastName='{0}'";

        internal const string AddeBaySalesMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,CardID,InvoiceNumber,SaleDate,CustomersNumber" +
                                                      ",ItemNumber,Description,Quantity,ShippingDate,SaleStatus,Inclusive,IncTaxPrice,TaxCode,FreightIncTaxAmount,IncTaxTotal,ExTaxPrice,ExTaxTotal,GSTAmount,FreightTaxCode) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},'{11}','I','1','{12}','GST','{13}','{14}','{15}','{16}','{17}','GST')";

        internal const string AddmuleBaySalesMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,CardID,InvoiceNumber,SaleDate,CustomersNumber" +
                                                  ",ItemNumber,Description,Quantity,ShippingDate,SaleStatus,Inclusive,IncTaxPrice,TaxCode,FreightIncTaxAmount,IncTaxTotal,ExTaxPrice,ExTaxTotal,GSTAmount,FreightTaxCode) VALUES(('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},'{11}','I','1','{12}','GST','{13}','{14}','{15}','{16}','{17}','GST')";


        internal const string AddmuleBaySalesItemsMyob = "('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},'{11}','I','1','{12}','GST','{13}','{14}','{15}','{16}','{17}','GST')";



        //internal const string AddOSCommerceSalesMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,RecordID,InvoiceNumber,SaleDate" +
        //                                              ",ItemNumber,Quantity,SaleStatus,Inclusive,IncTaxPrice,TaxCode,FreightIncTaxAmount,AmountPaid,IncTaxTotal,ExTaxPrice,ExTaxTotal,GSTAmount) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'I','0','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}')";
        internal const string AddOSCommerceSalesMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,CardID,InvoiceNumber,SaleDate" +
                                                      ",ItemNumber,Quantity,SaleStatus,Inclusive,TaxCode,FreightIncTaxAmount,ExTaxPrice,ExTaxTotal,GSTAmount,FreightTaxCode) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'I','0','{9}','{10}','{11}','{12}','{13}','GST')";


        internal const string AddmulOSCommerceSalesMyob = "INSERT INTO IMPORT_ITEM_SALES(AddressLine1,AddressLine2,AddressLine3,AddressLine4,CardID,InvoiceNumber,SaleDate" +
                                                  ",ItemNumber,Quantity,SaleStatus,Inclusive,TaxCode,FreightIncTaxAmount,ExTaxPrice,ExTaxTotal,GSTAmount,FreightTaxCode) VALUES(('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'I','0','{9}','{10}','{11}','{12}','{13}','GST')";


        internal const string AddmulOSCommerceSalesItemsMyob = "('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'I','0','{9}','{10}','{11}','{12}','{13}','GST')";


        /// <summary>
        /// For Version 5.
        /// </summary>

        public const string FileDialogFilterQuickBook = "Excel, Text, Xml, ODS or IIF files(*.xls,*.xlsx,*.csv,*.txt,*.xml,*.iif,*.qif,*.qfx,*.ofx,*.qbo,*.ods)|*.xls;*.xlsx;*.xlsm;*.csv;*.txt;*.xml;*.iif;*.qif;*.qfx;*.ofx;*.qbo;*.ods";

        public const string FileDialogFilterMyob = "Excel or Text(*.xls,*.xlsx,*.csv,*.txt,*.qif,*.qfx,*.ofx,*.qbo,*.iif)|*.xls;*.xlsx;*.csv;*.txt;*.qif;*.qfx;*.ofx;*.qbo;*.iif";

        public const string FileDilogFilterXero = "Excel, Text,Xml or iif files(*.xls,*.xlsx,*.csv,*.txt,*.xml,*.iif)|*.xls;*.xlsx;*.csv;*.txt;*.xml;*.iif";

        public const string SecurityExceptionMsg = "Security error occured.";

        public const string NoDeleteMsg = "Cannnot delete the row.";

        public const string InvalidXmlFileMsg = "Invalid file.Please select a valid file.";

        #region OSCommerce Old Query for version 2.0

        internal const string OSCommerceMySqlServerOrderStringOldOrder = "SELECT orders_id from orders WHERE date_purchased >= '{0}' AND date_purchased <= '{1}'";

        //internal const string OSCommerceMySqlServerOrderStringOldOrder = "SELECT orders_id from orders WHERE last_modified >= '{0}' AND last_modified <= '{1}'";


        internal const string OSCommerceMySqlServerOrderStringOld = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,op.products_id,op.products_price,op.final_price," +
                                "op.products_tax,op.products_quantity,pd.products_name,osh.comments,ot.title,ot.value FROM orders ordr join orders_products op on ordr.orders_id = op.orders_id join products_description pd on op.products_id = pd.products_id join orders_total ot on ordr.orders_id = ot.orders_id join orders_status_history osh on osh.orders_id = ordr.orders_id WHERE pd.language_id=1 and ordr.last_modified >= '{0}' AND ordr.last_modified <= '{1}' AND ot.class='Shipping'";


        //internal const string OSCommerceMySqlServerOrderStringOldMain = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price," +
        //                      "op.products_tax,op.products_quantity FROM orders ordr join orders_products op on ordr.orders_id = op.orders_id join products_description pd on op.products_id = pd.products_id join orders_status_history osh on osh.orders_id = ordr.orders_id WHERE pd.language_id=1 and ordr.last_modified >= '{0}' AND ordr.last_modified <= '{1}'";

        internal const string OSCommerceMySqlServerOrderStringOldMain = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM orders ordr LEFT OUTER join orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join products_description pd on op.products_id = pd.products_id LEFT OUTER join orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.orders_status_id = 1 AND ordr.orders_id = {0}";

        internal const string OSCommerceMySqlServerOrderStringOldShipping = "SELECT title,value FROM orders_total WHERE orders_id ={0} AND class='ot_shipping'";

        internal const string OSCommerceMySqlServerOrderStringOldTotal = "SELECT value FROM orders_total WHERE orders_id ={0} AND class='ot_total'";

        #endregion

        #region OSCommerce New Query for version 3.0

        //internal const string OSCommerceMySqlServerOrderStringOrder = "SELECT orders_id from osc_orders WHERE last_modified >= '{0}' AND last_modified <= '{1}'";

        internal const string OSCommerceMySqlServerOrderStringOrder = "SELECT orders_id from osc_orders WHERE date_purchased >= '{0}' AND date_purchased <= '{1}'";

        internal const string OSCommerceMySqlServerOrderString = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,op.products_id,op.products_price,op.products_tax,op.products_quantity,pd.products_name,osh.comments,ot.title,ot.value FROM osc_orders ordr join osc_orders_products op on ordr.orders_id = op.orders_id join osc_products_description pd on op.products_id = pd.products_id join osc_orders_total ot on ordr.orders_id = ot.orders_id join osc_orders_status_history osh on osh.orders_id = ordr.orders_id WHERE pd.language_id=1 and ordr.last_modified >= '{0}' AND ordr.last_modified <= '{1}' OR ot.class='Shipping'";

        //internal const string OSCommerceMySqlServerOrderStringMain = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.products_tax,op.products_quantity FROM osc_orders ordr join osc_orders_products op on ordr.orders_id = op.orders_id join osc_products_description pd on op.products_id = pd.products_id join osc_orders_total ot on ordr.orders_id = ot.orders_id join osc_orders_status_history osh on osh.orders_id = ordr.orders_id WHERE pd.language_id=1 and ordr.last_modified >= '{0}' AND ordr.last_modified <= '{1}'";

        internal const string OSCommerceMySqlServerOrderStringMain = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.products_tax,op.products_quantity,op.products_model FROM osc_orders ordr LEFT OUTER join osc_orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join osc_products_description pd on op.products_id = pd.products_id LEFT OUTER join osc_orders_total ot on ordr.orders_id = ot.orders_id LEFT OUTER join osc_orders_status_history osh on ordr.orders_id = osh.orders_id WHERE pd.language_id=1 and and osh.orders_status_id = 1 and ordr.orders_id = {0}";

        internal const string OSCommerceMySqlServerOrderStringShipping = "SELECT title,value FROM osc_orders_total WHERE orders_id ={0} AND class='ot_shipping'";

        internal const string OSCommerceMySqlServerOrderStringTotal = "SELECT value FROM osc_orders_total WHERE orders_id ={0} AND class='ot_total'";



        #endregion


        internal const string GetTPIDByTPName = "SELECT TradingPartnerID FROM trading_partner_schema WHERE TPName='{0}'";

        internal const string GetDetailsByTPName = "SELECT * FROM trading_partner_schema WHERE TPName='{0}'";

        internal const string GetWebUrlByTPName = "SELECT OSCWebStoreUrl FROM trading_partner_schema WHERE TPName='{0}'";

        #region Axis 5.1

        //Desktop Help Links
        internal const string JournalEntriesLink = "http://support.zed-systems.com/KB/a50/importing-journal-entries-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string EstimateLink = "http://support.zed-systems.com/KB/a96/importing-estimates-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string SalesOrderLink = "http://support.zed-systems.com/KB/a57/import-sales-orders-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CustomerLink = "http://support.zed-systems.com/KB/a134/importing-customer-lists-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string VendorLink = "http://support.zed-systems.com/KB/a135/importing-vendor-lists-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string EmployeeLink = "http://support.zed-systems.com/KB/a136/importing-employee-lists-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string InvoiceLink = "http://support.zed-systems.com/KB/a52/importing-invoices-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string SalesReciptLink = "http://support.zed-systems.com/KB/a95/importing-sales-receipts-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CreditMemoLink = "http://support.zed-systems.com/KB/a97/importing-credit-memos-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string BillPaymentCreditCardLink = "http://support.zed-systems.com/KB/a82/importing-bill-payments-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string BillPaymentCheckLink = "http://support.zed-systems.com/KB/a82/importing-bill-payments-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string DepositsLink = "http://support.zed-systems.com/KB/a98/importing-deposits-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string BillsLink = "http://support.zed-systems.com/KB/a81/importing-bills-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CreditCardChargesLink = "http://support.zed-systems.com/KB/a87/import-credit-card-charges-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CreditCardCreditLink = "http://support.zed-systems.com/KB/a101/importing-credit-card-credits-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CheckLink = "http://support.zed-systems.com/KB/a100/importing-checks-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string CustomFieldLink = "https://support.zed-systems.com/kb/a156/import-custom-fields-into-quickbooks.aspx";
        internal const string VendorCreditsLink = "http://support.zed-systems.com/KB/a99/importing-vendor-credits-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string TimeTrackingLink = "http://support.zed-systems.com/KB/a51/importing-time-entries-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string RecievePaymentLink = "http://support.zed-systems.com/KB/a60/importing-receive-payments-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string PriceLevelLink = "http://support.zed-systems.com/KB/a59/importing-price-level-lists-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string InventoryAdjustmentLink = "http://support.zed-systems.com/KB/a76/import-inventory-adjustment-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string PurchaseOrderLink = "https://support.zed-systems.com/kb/a171/import-purchase-orders-into-quickbooks.aspx";
        internal const string ItemDiscountLink = "https://support.zed-systems.com/kb/a313/import-discount-items-into-quickbooks.aspx";
        internal const string ItemFixedAssetLink = "http://support.zed-systems.com/kb/a150/import-fixed-asset-items-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemGroupLink = "https://support.zed-systems.com/kb/a204/how-to-import-group-items-into-quickbooks.aspx";
        internal const string ItemInventoryLink = "http://support.zed-systems.com/KB/a137/importing-inventory-items-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemInventoryAssemblyLink = "http://support.zed-systems.com/kb/a155/import-inventory-assembly-items-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemNonInventoryLink = "http://support.zed-systems.com/KB/a139/how-to-import-non-inventory-items-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemOtherChargeLink = "https://support.zed-systems.com/kb/a314/import-other-charge-items-into-quickbooks-desktop.aspx";
        internal const string ItemPaymentLink = "https://support.zed-systems.com/kb/a315/how-to-import-payment-items-into-quickbooks-desktop.aspx";
        internal const string ItemReceiptLink = "http://support.zed-systems.com/KB/a140/how-to-import-item-receipts-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemSalesTaxLink = "https://support.zed-systems.com/kb/a188/how-to-import-sales-tax-item-lists-into-quickbooks.aspx";
        internal const string ItemSalesTaxGroupLink = "https://support.zed-systems.com/kb/a324/how-to-import-sales-tax-groups-into-quickbooks.aspx";
        internal const string ItemServiceLink = "http://support.zed-systems.com/KB/a141/importing-service-items-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string ItemSubTotalLink = "https://support.zed-systems.com/kb/a325/how-to-import-subtotal-items-into-quickbooks.aspx";
        internal const string TransferInventoryLink = "http://support.zed-systems.com/KB/a142/import-inventory-transfers-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string VehicleMileageLink = "http://support.zed-systems.com/kb/a170/import-vehicle-mileage-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string StatementChargesLink = "http://support.zed-systems.com/kb/a169/import-statement-charges-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string TransferLink = "https://support.zed-systems.com/kb/a259/import-transfer-funds-into-quickbooks.aspx";
        internal const string BuildAssemblyLink = "https://support.zed-systems.com/kb/a274/import-build-assemblies-into-quickbooks.aspx";
        internal const string InventorySiteLink = "https://support.zed-systems.com/kb/a292/import-inventory-site-lists-into-quickbooks.aspx";
        internal const string ClassLink = "https://support.zed-systems.com/kb/a321/how-to-import-classes-into-quickbooks-desktop.aspx";
        internal const string OtherNameLink = "https://support.zed-systems.com/kb/a326/how-to-import-other-names-into-quickbooks.aspx";
        internal const string ListMergeLink = "https://support.zed-systems.com/kb/a320/how-to-merge-lists-in-quickbooks-desktop.aspx";
        internal const string ClearedStatusLink = "https://support.zed-systems.com/kb/a323/how-to-mark-transactions-as-cleared-in-quickbooks.aspx";
        internal const string BillingRateLink = "https://support.zed-systems.com/kb/a322/how-to-import-billing-rates-into-quickbooks.aspx";


        //Online Help Links
        internal const string OnlineSalesReceiptLink = "https://support.zed-systems.com/kb/a235/import-sales-receipts-into-quickbooks-online.aspx";
        internal const string OnlineInvoiceLink = "https://support.zed-systems.com/kb/a197/import-invoices-into-quickbooks-online.aspx";
        internal const string OnlineBillLink = "https://support.zed-systems.com/kb/a238/import-bills-into-quickbooks-online.aspx";
        internal const string OnlineJournalEntryLink = "http://support.zed-systems.com/kb/a236/import-journal-entries-into-quickbooks-online.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string OnlineTimeActivityLink = "http://support.zed-systems.com/kb/a221/import-timesheets-into-quickbooks-online.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string OnlinePurchaseOrderLink = "https://support.zed-systems.com/kb/a237/import-purchase-orders-into-quickbooks-online.aspx";
        internal const string OnlineCashPurchaseLink = "https://support.zed-systems.com/kb/a327/how-to-import-expenses-into-quickbooks-online.aspx";
        internal const string OnlineCheckPurchaseLink = "https://support.zed-systems.com/kb/a263/import-checks-into-quickbooks-online.aspx";
        internal const string OnlineCreditCardPurchaseLink = "https://support.zed-systems.com/kb/a240/import-credit-card-purchases-into-quickbooks-online.aspx";
        internal const string OnlineEstimateLink = "https://support.zed-systems.com/kb/a241/import-estimates-into-quickbooks-online.aspx";
        internal const string OnlinePaymentLink = "http://support.zed-systems.com/kb/a242/import-receive-payments-into-quickbooks-online.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string OnlineVendorCreditLink = "https://support.zed-systems.com/kb/a243/import-vendor-credits-into-quickbooks-online.aspx";
        internal const string OnlineBillPaymentLink = "http://support.zed-systems.com/kb/a239/import-bill-payments-into-quickbooks-online.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        internal const string OnlineCreditMemoLink = "https://support.zed-systems.com/kb/a244/import-credit-memos-into-quickbooks-online.aspx";
        internal const string OnlineEmployeeLink = "https://support.zed-systems.com/kb/a268/import-employees-into-quickbooks-online.aspx";
        internal const string OnlineItemsLink = "https://support.zed-systems.com/kb/a273/import-products-and-services-lists-into-quickbooks-online.aspx";
        internal const string OnlineCustomerLink = "https://support.zed-systems.com/kb/a272/import-customer-lists-into-quickbooks-online.aspx";
        internal const string OnlineDepositLink = "https://support.zed-systems.com/kb/a278/import-deposits-into-quickbooks-online.aspx";
        internal const string OnlineAccountLink = "https://support.zed-systems.com/kb/a295/import-chart-of-account-lists-into-quickbooks-online.aspx";
        internal const string OnlineRefundReceiptLink = "https://support.zed-systems.com/kb/a297/import-refund-receipts-into-quickbooks-online.aspx";
        internal const string OnlineTransferLink = "https://support.zed-systems.com/kb/a305/import-transfers-into-quickbooks-online.aspx";
        internal const string OnlineVendorLink = "https://support.zed-systems.com/kb/a299/import-vendor-lists-into-quickbooks-online.aspx";

        //POS Help Links
        internal const string POSCustomerLink = "https://support.zed-systems.com/kb/a253/import-customers-into-quickbooks-pos.aspx";
        internal const string POSEmployeeLink = "https://support.zed-systems.com/kb/a254/import-employees-into-quickbooks-pos.aspx";
        internal const string POSItemInventoryLink = "https://support.zed-systems.com/kb/a255/import-inventory-items-into-quickbooks-pos.aspx";
        internal const string POSPriceDiscountLink = "https://support.zed-systems.com/kb/a257/import-price-discounts-into-quickbooks-pos.aspx";
        internal const string POSPriceAdjustmentLink = "https://support.zed-systems.com/kb/a256/import-price-adjustments-into-quickbooks-pos.aspx";
        internal const string POSCustomFieldListLink = "https://support.zed-systems.com/kb/a328/how-to-import-custom-fields-into-quickbooks-pos.aspx";
        internal const string POSSalesOrderLink = "https://support.zed-systems.com/kb/a246/import-sales-orders-into-quickbooks-pos.aspx";
        internal const string POSPurchaseOrderLink = "https://support.zed-systems.com/kb/a247/import-purchase-orders-into-quickbooks-pos.aspx";
        internal const string POSSalesReceiptLink = "https://support.zed-systems.com/kb/a245/import-sales-receipts-into-quickbooks-pos.aspx";
        internal const string POSTimeEntryLink = "https://support.zed-systems.com/kb/a248/import-time-entries-into-quickbooks-pos.aspx";
        internal const string POSVoucherLink = "https://support.zed-systems.com/kb/a249/import-vouchers-into-quickbooks-pos.aspx";
        internal const string POSInventoryCostAdjustmentLink = "https://support.zed-systems.com/kb/a251/import-inventory-cost-adjustments-into-quickbooks-pos.aspx";
        internal const string POSInventoryQtyAdjustmentLink = "https://support.zed-systems.com/kb/a250/import-inventory-quantity-adjustments-into-quickbooks-pos.aspx";
        internal const string POSTransferSlipLink = "https://support.zed-systems.com/kb/a252/import-transfer-slips-into-quickbooks-pos.aspx";
     

        //internal const string PurchaseOrderLink = "http://support.zed-systems.com/KB/a72/import-purchase-orders-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        //internal const string CustomFieldLink = "http://support.zed-systems.com/kb/a169/import-statement-charges-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        //internal const string TransferLink = "http://support.zed-systems.com/kb/a258/import-transfer-funds-into-quickbooks.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";
        // internal const string PurchaseLink = "http://support.zed-systems.com/kb/a240/import-credit-card-expense-into-quickbooks-online.aspx?utm_source=inproduct&utm_medium=link&utm_campaign=axis";

        #endregion

        //Axis 6.0 
        //Getting the scheduled time from mail_setup_schema.

        internal const string GetScheduledTime = "SELECT ScheduleInMin FROM mail_setup_schema";

        internal const string CheckInboundOption = "SELECT InboundFlag FROM mail_setup_schema";

        internal const string CheckOutboundOption = "SELECT OutboundFlag FROM mail_setup_schema";



        //Axis Online OAuth 2.0 API  AXIS-711
        internal const string QBOLogPath = "C:\\Users\\Public\\Documents\\Zed\\Axis 14.1\\LOG";
        internal const string QBOMinorVersion = "23";
        internal const string QBOBaseUrl = "https://quickbooks.api.intuit.com/";
        internal const string QBOEnviornment = "production";
        internal const string QBORedirectUrl = "https://axis.zed-systems.com/axis-desktop-redirect";
        internal const string QBOClientId = "L0dv5PtxCGUNWdP4SmrDgC2uOKaW4xcInijfESR5J0gBe4cWre";
        internal const string QBOClientSecret = "HnuNvLyLN5C8w3DNbRAJJf84D3dbQi9s7GhLaqPV";
        internal const string ConnectToQuickbookUrl = "https://axis.zed-systems.com/axis-desktop-connect?new=true";
        internal const string QuickbookGraphQlUrl = "https://v4thirdparty.api.intuit.com/graphql";
        internal const string ConnectToQuickbookHelpURL = "https://support.zed-systems.com/kb/a167/how-to-connect-to-quickbooks-online.aspx";
        internal const string ReadEmployeeCompensation = "{\"query\":\"query ReadEmployeeCompensations {\\r\\n  company {\\r\\n    employeeContacts {\\r\\n      edges {\\r\\n        node {\\r\\n          id\\r\\n\\t\\t  externalIds {\\r\\n\\t\\t  localId\\r\\n\\t\\t  }\\r\\n          profiles {\\r\\n            employee {\\r\\n              compensations {\\r\\n                edges {\\r\\n                  node {\\r\\n                    id\\r\\n                    amount\\r\\n                    employerCompensation {\\r\\n                      name\\r\\n                    }\\r\\n                  }\\r\\n                }\\r\\n              }\\r\\n            }\\r\\n          }\\r\\n        }\\r\\n      }\\r\\n    }\\r\\n  }\\r\\n}\",\"variables\":\"\"}";

        // Don't change Encryption Key as it will be sync with Zed Axis online key
        // used for encryption and decryption of Access and refresh token.
        internal const string EncryptionKey = "rS&7$FTqo0e92";


        // for sandbox version
        // internal const string QBOBaseUrl = "https://sandbox-quickbooks.api.intuit.com/";
        //internal const string QBORedirectUrl = "http://localhost:4200/axis-desktop-redirect";
        //internal const string QBOEnviornment = "sandbox";
        //internal const string QBOClientId = "L0aFgiS2UmKHmLSDs0kPIk4torff5fkb2e2wQYIQHVr5ptzCIv";
        //internal const string QBOClientSecret = "yhNdv3PRZw5XbSWQZhbJgnEwt7SCsgrq5Slktx0Q";
        //internal const string ConnectToQuickbookUrl = "http://localhost:4200/axis-desktop-connect?new=true";

        //Xero OAuth 2.0 API
        //internal const string XeroClientId = "9A7A72B06AFB49FAB9CF26DCD241422C";
        //internal const string XeroClientSecret = "z9lmrvP0ejwDTdTguAf1yLAGipLN4QrQhE2yDHGQBNFB4PP3";
        //internal const string XeroScope = "openid profile email files accounting.transactions accounting.contacts offline_access";
        //internal const string XeroCallbackUri = "https://staging.zed-systems.com/axis-desktop-xero-connect";
        //internal const string ConnectToXeroUrl = "https://staging.zed-systems.com/axis-desktop-xero-connect?new=true";
        //internal const string RefreshXeroTokenBaseUrl = "https://staging.zed-systems.com/api/v1/AxisDesktop/";
        //internal const string ConnectToXeroHelpURL = "http://support.zed-systems.com/kb/a191/how-to-connect-to-xero";

        internal const string XeroClientId = "9A7A72B06AFB49FAB9CF26DCD241422C";
        internal const string XeroClientSecret = "z9lmrvP0ejwDTdTguAf1yLAGipLN4QrQhE2yDHGQBNFB4PP3";
        internal const string XeroScope = "openid profile email files accounting.transactions accounting.contacts offline_access";
        internal const string XeroCallbackUri = "https://axis.zed-systems.com/axis-desktop-xero-connect";
        internal const string ConnectToXeroUrl = "https://axis.zed-systems.com/axis-desktop-xero-connect?new=true";
        internal const string RefreshXeroTokenBaseUrl = "https://axis.zed-systems.com/api/v1/AxisDesktop/";
        internal const string ConnectToXeroHelpURL = "http://support.zed-systems.com/kb/a191/how-to-connect-to-xero";
       
    }
}
