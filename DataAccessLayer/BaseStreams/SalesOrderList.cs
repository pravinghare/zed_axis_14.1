// ===============================================================================
// 
// SalesOrderList.cs
//
// This file contains the implementations of the Sales Order Base Class Members. 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provides members of Sales Order.
    /// </summary>
    public class BaseSalesOrderList
    {
        #region Private Members
        //Adding Private member of Sales Order.
        protected string m_invoiceNumber;
        protected string m_customerId;
        protected string m_promisedDate;
        protected string m_invoiceDate;
        protected string m_addr1;
        protected string m_addr2;
        protected string m_addr3;
        protected string m_shipToAddress;
        protected string m_shipToAddressLine1;
        protected string m_shipToAddressLine2;
        protected string m_shipToAddressLine3;
        protected string m_shipToAddressLine4;
        protected string m_shipCity;
        protected string m_shipState;
        protected string m_shipPostalCode;
        protected string m_shipCountry;

        protected string m_shippingMethod;
        protected string m_customerPoNumber;
        protected string m_Comment;
        protected int m_lineNumber;
        protected string m_itemNumber;
        protected int m_quantity;
        protected string m_country;
        protected string m_state;

        protected string m_postalCode;
        protected string m_city;
        protected string m_overrideUOMFullName;
        protected string m_note;
        protected string m_TxnID;
        protected string m_TxnNumber;

        protected string m_TxnLineNumber;
        protected string[] m_QBTxnLineNumber = new string[2000];
        protected decimal?[] m_QBquantity = new decimal?[2000];
        protected string[] m_QBitemNumber = new string[2000];

        //Axis 6.0 changes
        protected string m_customerFullName;
        protected string m_ClassRefFullName;       
        protected string m_TemplateRefFullName;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;        
        protected string m_SubTotal;
        protected string m_ItemSalesTaxRefFullName;
        protected string m_SalesTaxPercentage;
        protected string m_SalesTaxTotal;
        protected string m_TotalAmount;
        protected string m_CurrencyRefFullName;
        protected string m_ExchangeRate;
        protected string m_TotalAmountInHomeCurrency;
        

        protected string m_CustomerMsgRefFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_CustomerSalesTaxCodeRefFullName;
        protected string m_Other;

        protected string m_FOB;
        protected string m_IsManuallyClosed;
        protected string m_IsFullyInvoiced;
        protected string m_Memo;
        protected string m_LinkedTxnID;
     
        protected string m_UnitOfMeasure;

        protected string m_SalesTaxCodeRefFullName;

        protected string[] m_Other1 = new string[2000];
        protected string[] m_Other2 = new string[2000];
        protected string[] m_Invoiced = new string[2000];
        protected string[] m_LineIsManuallyClosed = new string[2000];
        protected string[] m_LineClass = new string[2000];
       
        //for export InvoiceLineRet;    
    
        protected string[] m_QBitemFullName = new string[2000];
        protected string[] m_Desc = new string[2000];
        protected string[] m_UOM = new string[2000];
        protected string[] m_OverrideUOMFullName = new string[2000];
        protected decimal?[] m_Rate = new decimal?[2000];
        protected decimal?[] m_Amount = new decimal?[2000];
        protected string[] m_InventorySiteRefFullName = new string[2000];
        protected string[] m_ServiceDate = new string[2000];        
        protected string[] m_SalesTaxCodeFullName = new string[2000];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends
        //for export InvoiceLineGroupRet;
        protected string[] m_GroupTxnLineID = new string[2000];
        protected string[] m_ItemGroupFullName = new string[2000];
        protected string[] m_GroupDesc = new string[2000];
        protected decimal?[] m_GroupQuantity = new decimal?[2000];
        protected string[] m_IsPrintItemsInGroup = new string[2000];

        //Sub InvoiceLineRet;
        protected string[] m_GroupQBTxnLineNumber = new string[2000];
        protected string[] m_GroupLineItemFullName = new string[2000];
        protected string[] m_GroupLineDesc = new string[2000];
        protected decimal?[] m_GroupLineQuantity = new decimal?[2000];
        protected string[] m_GroupLineUOM = new string[2000];
        protected string[] m_GroupLineOverrideUOM = new string[2000];
        protected decimal?[] m_GroupLineRate = new decimal?[2000];
        protected decimal?[] m_GroupLineAmount = new decimal?[2000];
        protected string[] m_GroupLineServiceDate = new string[2000];
        protected string[] m_GroupLineSalesTaxCodeFullName = new string[2000];
          
        #endregion
    }
}
