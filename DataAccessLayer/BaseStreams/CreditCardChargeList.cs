﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class is used to provide members of CreditCardChargeList.
    /// </summary>
    public class BaseCreditCardChargeList
    {
        #region Protected members

        protected string m_TxnId;
        protected string m_AccountRefFullName;
        protected string m_PayeeEntityFullName;
        protected string m_TxnDate;
        protected decimal? m_Amount;
        protected string m_CurrencyFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountInHomeCurrency;
        protected string m_RefNumber;
        protected string m_Memo;
        protected string m_IsTaxIncluded;
        protected string m_SalesTaxCodeFullName;

        //For ExpenseLineRet
        protected string[] m_ExpTxnLineID = new string[200];
        protected string[] m_ExpAccountFullName = new string[200];
        protected decimal?[] m_ExpAmount = new decimal?[200];
        protected string[] m_ExpMemo = new string[200];
        protected string[] m_ExpCustomerFullname = new string[200];
        protected string[] m_ExpClassFullName = new string[200];
        protected string[] m_ExpSalesTaxCodeFullName = new string[200];
        protected string[] m_ExpBillableStatus = new string[200];


        //For ItemLineRet
        protected string[] m_TxnLineID = new string[200];
        protected string[] m_ItemFullName = new string[200];
        protected string[] m_Desc = new string[200];
        protected decimal?[] m_Quantity = new decimal?[200];
        protected string[] m_UOM = new string[200];
        protected decimal?[] m_Cost = new decimal?[200];
        protected decimal?[] m_ItemAmount = new decimal?[200];
        protected string[] m_ItemCustomerFullName = new string[200];
        protected string[] m_ItemClassName = new string[200];
        protected string[] m_ItemSalesTaxCodeFullName = new string[200];
        protected string[] m_ItemBillableStatus = new string[200];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends
        //For group ItemLineRet
        protected string[] m_GroupTxnLineId = new string[200];
        protected string[] m_ItemgroupFullName = new string[200];

        //For sub ItemLineRet
        protected string[] m_GroupLineTxnLineID = new string[200];
        protected string[] m_GroupLineItemFullName = new string[200];
        protected string[] m_GroupLineDesc = new string[200];
        protected decimal?[] m_GroupLineQuantity = new decimal?[200];
        protected string[] m_GroupLineUOM = new string[200];
        protected decimal?[] m_GroupLineCost = new decimal?[200];
        protected decimal?[] m_GroupLineAmount = new decimal?[200];
        protected string[] m_GroupLineCustomerFullName = new string[200];
        protected string[] m_GroupLineClassName = new string[200];
        protected string[] m_GroupLineSalesTaxCodeFullName = new string[200];
        protected string[] m_GroupLineBillableStatus = new string[200];


        #endregion
    }
}
