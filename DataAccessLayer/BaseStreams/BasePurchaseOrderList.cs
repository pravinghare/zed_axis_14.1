using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Streams
{
    public class BasePurchaseOrderList
    {
        #region Private variable
        protected string m_TxnID;
        protected string m_RefNumber;
        protected string m_VenderListID;
        protected string m_ExpectedDate;
        protected string m_ItemRefFullName;
        protected string m_Quantity;
        protected Collection<QBItemDetails> m_QBItemsDetails;

        protected string m_TxnNumber;
        protected string m_VendorRefFullName;


       protected string[] m_QBTxnLineNumber = new string[100000];
        protected string[] m_GroupTxnLineID = new string[100000];

        protected string m_ClassRefFullName;
        protected string m_InventorySiteRefFullName;
        protected string m_ShipToEntityRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_VendorAddress1;
        protected string m_VendorAddress2;
        protected string m_VendorAddress3;
        protected string m_VendorCity;
        protected string m_VendorState;
        protected string m_VendorPostalCode;
        protected string m_VendorCountry;
        protected string m_VendorNote;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipCity;
        protected string m_ShipState;
        protected string m_ShipPostalCode;
        protected string m_ShipCountry;
        protected string m_ShipNote;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_ShipMethodRefFullName;
        protected string m_TotalAmount;
        protected string m_CurrencyRefFullName;
        protected string m_ExchangeRate;
        protected string m_TotalAmountInHomeCurrency;
        protected string m_Memo;
        protected string m_IsManuallyClosed;
        protected string m_IsFullyReceived;
        protected string m_VendorMsgRefFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_Other1;
        protected string m_Other2;
        protected string m_FOB;
        protected string m_LinkedTxnID;

        protected string[] m_ManufacturerPartNumber = new string[100000];
        protected string[] m_ServiceDate = new string[100000];
        protected string[] m_ReceivedQuantity = new string[100000];
        protected string[] m_LineIsManuallyClosed = new string[100000];
        protected string[] m_LineClass = new string[100000];
        protected string[] m_LineOther1 = new string[100000];
        protected string[] m_LineOther2 = new string[100000];


        #endregion
    }
}
