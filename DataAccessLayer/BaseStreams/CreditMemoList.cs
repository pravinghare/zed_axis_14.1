using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provide members of Credit Memo.
    /// </summary>
    public class BaseCreditMemoList
    {
        #region Protected Methods

        protected string m_TxnID;
        protected string m_CustomerRefFullName;
        protected string m_ClassRefFullName;
        protected string m_ARAccountRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;
        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_PONumber;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_FOB;
        protected string m_Shipdate;
        protected string m_ShipMethodRefFullName;
        protected string m_ItemSalesTaxRefFullName;
        protected string m_SalesTaxPercentage;
        protected decimal? m_TotalAmount;
        protected decimal? m_CreditRemaining;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_CreditRemainingInHomeCurrency;
        protected string m_Memo;
        protected string m_CustomerMsgRefFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxCodeRefFullName;
        protected string m_LinkedTxnID;

        //CreditMemoLineRet
        protected string[] m_TxnLineID = new string[200];
        protected string[] m_ItemRefFullName = new string[200];
        protected string[] m_desc = new string[200];
        protected decimal?[] m_Quantity = new decimal?[200];
        protected string[] m_UOM = new string[200];
        protected decimal?[] m_Rate = new decimal?[200];
        protected decimal?[] m_Amount = new decimal?[200];
        protected string[] m_InventorySiteRefFullName = new string[200];
        protected string[] m_ServiceDate = new string[200];
        protected string[] m_SalesTaxCodeRefFullName = new string[200];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends
        //CreditMemoLineGroupRet
        protected string[] m_GroupTxnLineID = new string[200];
        protected string[] m_GroupItemFullName = new string[200];        

        //Sub CreditMemoLineRet
        protected string[] m_GroupLineTxnLineID = new string[200];
        protected string[] m_GroupLineItemFullName = new string[200];
        protected string[] m_GroupLineDesc = new string[200];
        protected decimal?[] m_GroupLineQuantity = new decimal?[200];
        protected string[] m_GroupLineUOM = new string[200];
        protected decimal?[] m_GroupLineRate = new decimal?[200];
        protected decimal?[] m_GroupLineAmount = new decimal?[200];
        protected string[] m_GroupLineServiceDate = new string[200];
        protected string[] m_GroupLineSalesTaxCodeFullName = new string[200];

        #endregion
    }
}
