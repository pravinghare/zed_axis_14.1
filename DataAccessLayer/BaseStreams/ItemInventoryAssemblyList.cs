using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Item List
    /// </summary>
    /// <summary>
    /// This method provide members of items.
    /// </summary>
    public class BaseItemInventoryAssemblylist
    {
        #region Private Members
        //Private members of Itemlist.
        protected string m_Name = string.Empty;
        protected string m_ListId = string.Empty;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_DataExtName = string.Empty;
        protected string m_FullName = string.Empty;
        protected string m_IsActive = string.Empty;
        protected string m_ParentRefFullName = string.Empty;
        protected string m_Sublevel = string.Empty;
        protected string m_UnitOfMeasureSetRefFullName = string.Empty;
        protected string m_SalesTaxCodeRefFullName = string.Empty;
        protected string m_SalesDesc = string.Empty;
        protected string m_SalesPrice = string.Empty;
        protected string m_IncomeAccountRefFullName = string.Empty;
        protected string m_PurchaseDesc = string.Empty;
        protected string m_PurchaseCost = string.Empty;
        protected string m_COGSAcoountRefFullName = string.Empty;
        protected string m_PrefVendorRefFullName = string.Empty;
        protected string m_AssetAccountRefFullName = string.Empty;
        protected string m_BuildPoint = string.Empty;
        protected string m_QunatityOnHand = string.Empty;
        protected string m_AverageCost = string.Empty;
        protected string m_QuantityOnOrder = string.Empty;
        protected string m_QuantityOnSalesOrder = string.Empty;
        protected string m_ItemInventoryAssemblyLine = string.Empty;

        // for exporting ItemInventoryRef
        protected string[] m_QBitemInventoryListID = new string[25];
        protected string[] m_QBitemInventoryFullName = new string[25];
        protected decimal?[] m_QBquantity = new decimal?[25];

        // protected string m_ItemInventoryAssemblyLineQuantity;
        protected string m_DatExtValue = string.Empty;
        protected string m_DatExtType = string.Empty;
        #endregion
    }
}