// ===============================================================================
// 
// Invoice.cs
//
// This file contains the implementations of the Invoice Base Class Members. 
// Developed By : Swapnil.
// Date : 12/03/2009
// Modified By : Swapnil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Invoice.
    /// </summary>
    /// <summary>
    /// This class provides members for Invoice.
    /// </summary>
    public class BaseInvoiceOS1
    {
        #region Protected Members

        protected char m_actionIndicator;
        protected string m_customerOrderNumber;
        protected string m_customerClientListId;
        protected string m_etd;
        protected string m_orderDate;
        protected string m_shipToName;
        protected string m_shipToAddress11;
        protected string m_shipToAddress12;
        protected string m_shipToAddress13;
        protected string m_shipToAddress14;
        protected string m_shipToAddress15;
        protected string m_deliveryDescription;
        protected string m_modeOfTransport;
        protected string m_asn;
        protected string m_invoiceNumber;
        protected decimal m_forwardingCharges;
        protected decimal m_freightCharges;
        protected List<BaseInvoiceOS2> m_BaseInvoiceOS2;

        protected string m_consignment_number;
        protected string m_carton_quantity;
        protected string m_pallet_quantity;
        #endregion

    }
}
