﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataProcessingBlocks
{
    //This class Provides Members of JournalEntry.
    public class BaseJournalEntry
    {
        #region Protected Methods

        protected string m_TxnId;
        protected string m_TxnNumber;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_IsAdjustment;
        protected string m_IsHomeCurrencyAdjustment;
        protected string m_IsAmountEnteredinHomeCurrency;
        protected string m_CurrencyFullName;
        protected decimal? m_ExchangeRate;
        
        //JournalDebitLine
        protected string[] m_DLineTxnLineId = new string[200];
        protected string[] m_DLineAccountFullName = new string[200];
        protected decimal?[] m_DLineAmount = new decimal?[200];
        protected string[] m_DLineMemo = new string[200];
        protected string[] m_DLineEntityFullName = new string[200];
        protected string[] m_DLineClassFullName = new string[200];
        protected string[] m_DLineBillableStatus = new string[200];
        protected string[] m_DLineItemSalesTaxRefFullName = new string[200];

        //JournalCreditLine
        protected string[] m_CLineTxnlineId = new string[200];
        protected string[] m_ClineAccountFullName = new string[200];
        protected decimal?[] m_CLineAmount = new decimal?[200];
        protected string[] m_CLineMemo = new string[200];
        protected string[] m_CLineEntityFullName = new string[200];
        protected string[] m_CLineClassFullName = new string[200];
        protected string[] m_CLineBillableStatus = new string[200];
        protected string[] m_CLineItemSalesTaxRefFullName = new string[200];

        #endregion
    }
}
