// ===============================================================================
// Axis 9.0
// BaseEstimateList.cs
//
// This file contains the implementations of the Estimate Base Class Members. 
//
// Added By : Akanksha.
// ===============================================================================


using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseEstimateList
    {

        #region Protected Members
     
        protected string m_TxnId;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected int m_TxnNumber;
        protected string m_customerRefFullName;
        protected string m_customerListID;
        protected string m_ClassRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_billAddr1;
        protected string m_billAddr2;
        protected string m_billAddr3;
        protected string m_billAddr4;
        protected string m_billAddr5;
        protected string m_billCity;
        protected string m_billState;
        protected string m_billPostalCode;
        protected string m_billCountry;
        protected string m_billNote;
        protected string m_billAddrBlock1;
        protected string m_billAddrBlock2;
        protected string m_billAddrBlock3;
        protected string m_billAddrBlock4;
        protected string m_billAddrBlock5;
        protected string m_shipAddress1;
        protected string m_shipAddress2;
        protected string m_shipAddress3;
        protected string m_shipAddress4;
        protected string m_shipAddress5;
        protected string m_shipAddressCity;
        protected string m_shipAddressState;
        protected string m_shipAddressPostalCode;
        protected string m_shipAddressCountry;
        protected string m_shipAddressNote;
        protected string m_shipAddrBlock1;
        protected string m_shipAddrBlock2;
        protected string m_shipAddrBlock3;
        protected string m_shipAddrBlock4;
        protected string m_shipAddrBlock5;
        protected string m_IsActive;
        protected string m_poNumber;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_FOB;
        protected string m_SubTotal;
        protected string m_ItemSalesTaxRefFullName;
        protected string m_SalesTaxPercentage;
        protected string m_SalesTaxTotal;
        protected string m_TotalAmount;
        protected string m_CurrencyRefFullName;
        protected string m_ExchangeRate;
        protected string m_TotalAmountInHomeCurrency;
        protected string m_memo;
        protected string m_CustomerMsgRefFullName;
        protected string m_IsToBeEmailed;
        protected string m_CustomerSalesTaxCodeRefFullName;
        protected string m_Other;
        protected string m_Other1;
        protected string m_Other2;
        protected string m_externalGUID;
        protected string m_txnType;
        protected string m_linkType;
        protected decimal m_amount;
        protected string m_ownerId;
        protected string m_extName;
        protected string m_dataExtType;
        protected string m_dataExtValue;
        protected string m_isTaxIncluded;




        //For EstimateLineRet,repeat
        //Bug 11(Axis 10.1 Changes).
        protected string[] m_QBTxnLineNumber = new string[5000];
        protected string[] m_EstItemRefFullName = new string[3000];
        protected string[] m_EstDesc = new string[3000];
        protected decimal?[] m_EstQuantity = new decimal?[3000];
        protected string[] m_EstUOM = new string[3000];
        protected string[] m_EstOverrideUOMSetRefFullName = new string[3000];
        protected decimal?[] m_EstRate = new decimal?[3000];
        protected string[] m_EstClassRefFullName = new string[3000];
        protected decimal?[] m_EstAmouunt = new decimal?[3000];
        protected string[] m_EstInventorySiteRefFullName = new string[3000];
        protected string[] m_EstSalesTaxCodeRefFullName = new string[3000];
        protected decimal?[] m_EstMarkUpRate = new decimal?[3000];
        protected decimal?[] m_EstMarkUpRatePercent = new decimal?[3000];
        protected string[] m_EstOther1 = new string[5000];
        protected string[] m_EstOther2 = new string[5000];

        //For EstimateLineGroupRet,repeat
        protected string[] m_EstGroupTxnLineID = new string[5000];
        protected string[] m_EstGroupItemRefFullName = new string[3000];
        protected string[] m_EstGroupDesc = new string[3000];
        protected decimal?[] m_EstGroupQuantity = new decimal?[3000];
        protected string[] m_EstGroupUOM = new string[3000];
        protected string[] m_EstGroupOverrideUOMSetRefFullName = new string[3000];
        protected bool[] m_EstGroupIsPrintItemInGroup;
        protected decimal?[] m_EstGroupTotalAmount = new decimal?[3000];

        //For  Sub EstimateLineRet,repeat
        protected string[] m_EstGroupLineTxnLineID = new string[3000];
        protected string[] m_EstGroupLineItemRefFullName = new string[3000];
        protected string[] m_EstGroupLineDesc = new string[3000];
        protected decimal?[] m_EstGroupLineQuantity = new decimal?[3000];
        protected string[] m_EstGroupLineUOM = new string[3000];
        protected string[] m_EstGroupLineOverrideUOMSetRefFullName = new string[3000];
        protected decimal?[] m_EstGroupLineRate = new decimal?[3000];
        protected string[] m_EstGroupLineClassRefFullName = new string[3000];
        protected decimal?[] m_EstGroupLineAmouunt = new decimal?[3000];
        protected string[] m_EstGroupLineInventorySiteRefFullName = new string[3000];
        protected string[] m_EstGroupLineSalesTaxCodeRefFullName = new string[3000];
        protected decimal?[] m_EstGroupLineMarkUpRate = new decimal?[3000];
        protected decimal?[] m_EstGroupLineMarkUpRatePercent = new decimal?[3000];
        protected string[] m_EstGroupLineOther1 = new string[5000];
        protected string[] m_EstGroupLineOther2 = new string[5000];



       

                
        #endregion
    }
}
