using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseGeneralSummaryreport
    {
        #region Protected Members

        protected string m_ReportTitle;
        protected string m_ReportSubtitle;
        protected string m_ReportBasis;
        protected string m_NumRows;
        protected string m_NumColumns;
        protected string m_NumColTitleRows;
        protected string[] m_ColDescColTitle = new string[150];
        protected string[] m_ColDescColType = new string[150];
        protected string[] m_DataRowRowData = new string[50000];
        protected string[] m_DataRowColData = new string[50000];
        protected string[] m_DataRowColID = new string[50000];
        protected string[] m_TextRow = new string[50000];
        protected string[] m_SubtotalRowRowData = new string[50000];
        protected string[] m_SubtotalRowColData = new string[50000];
        protected string[] m_SubtotalRowColID = new string[50000];
        protected string[] m_TotalRowRowData = new string[50000];
        protected string[] m_TotalRowColData = new string[50000];
        protected string[] m_TotalRowColID = new string[50000];

        #endregion
    }
}
