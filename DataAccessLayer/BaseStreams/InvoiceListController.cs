using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.IO;
using DataProcessingBlocks;
using System.Windows.Forms;
using System.Globalization;

namespace Streams
{
    public class InvoiceListController : CollectionBase
    {
        #region Private Member

        //Object of Myob Invoice list.
        private List<MYOBInvoiceList> myobInvoiceList;
        private string m_FileName = string.Empty;

        #endregion

        #region Constructor

        public InvoiceListController()
        {
            //Calling Myob Invoice list constructor.
            myobInvoiceList = new List<MYOBInvoiceList>();  
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get or Set filename value.
        /// </summary>
        public string FileName
        {
            get
            {
                return m_FileName;
            }
            set
            {
                m_FileName = value;
            }
        }
       
        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used for adding invoice object into collection.
        /// </summary>
        /// <param name="MYOBInvoiceList">Passing invoice object</param>
        public void Add(MYOBInvoiceList invoice)
        {
            //Adding sales order object to class.
            this.myobInvoiceList.Add(invoice);
        }

        /// <summary>
        /// This method is used for validate invoice data.
        /// </summary>
        /// <param name="invoice">Passing invoice data.</param>
        /// <returns>DataTable of invoice.</returns>
        public DataTable ISMInvoiceValidation(DataTable InvoiceData)
        {
            #region Checking Validations of MYOB Invoice data
           //Count row index of Invoice data.
            for (int rowIndex = 0; rowIndex < InvoiceData.Rows.Count; rowIndex++)
            {
                #region Adding ISM Invoice data
                
                //Checking Promised date value.
                if (InvoiceData.Columns.Contains(MyobInvoiceColumns.PromisedDate.ToString().ToUpper()))
                {
                    #region Validations of Sales Promised date

                    if (InvoiceData.Rows[rowIndex][MyobInvoiceColumns.PromisedDate.ToString()].ToString() != string.Empty)
                    {
                        #region validations of PromisedDate
                        DateTime PromisedDT = new DateTime();
                        if (!DateTime.TryParse(InvoiceData.Rows[rowIndex][MyobInvoiceColumns.PromisedDate.ToString().ToUpper()].ToString(), out PromisedDT))
                        {
                            DateTime dttest = new DateTime();
                            bool IsValid = false;

                            try
                            {
                                dttest = DateTime.ParseExact(InvoiceData.Rows[rowIndex][MyobInvoiceColumns.PromisedDate.ToString().ToUpper()].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                IsValid = true;
                            }
                            catch
                            {
                                IsValid = false;
                            }
                            if (IsValid == false)
                            {
                                //Display message of item number.
                                string strMessages = "The Invoice Promised date is not valid for ISM Message.If you press cancel this record will not be added to ISM Message.If Ignore it will proceed.";
                                DialogResult result = CommonUtilities.ShowMappingMessage(strMessages, MessageBoxIcon.Warning);
                                if (Convert.ToString(result) == "Cancel")
                                {
                                    //Remove row of Invoice data.
                                    InvoiceData.Rows.Remove(InvoiceData.Rows[rowIndex]);
                                    InvoiceData.AcceptChanges();
                                    rowIndex++;
                                    continue;
                                }
                            }
                           

                        }

                        #endregion

                    }
                    
                    #endregion

                }
                //Checking Invoice date value.
                if (InvoiceData.Columns.Contains(MyobInvoiceColumns.InvoiceDate.ToString().ToUpper()))
                {
                    #region Validations of Invoice Date

                    if (InvoiceData.Rows[rowIndex][MyobInvoiceColumns.InvoiceDate.ToString()].ToString() != string.Empty)
                    {
                        #region validations of InvoiceDate
                        DateTime InvoiceDT = new DateTime();
                        if (!DateTime.TryParse(InvoiceData.Rows[rowIndex][MyobInvoiceColumns.InvoiceDate.ToString().ToUpper()].ToString(), out InvoiceDT))
                        {
                            DateTime dttest = new DateTime();
                            bool IsValid = false;

                            try
                            {
                                dttest = DateTime.ParseExact(InvoiceData.Rows[rowIndex][MyobInvoiceColumns.InvoiceDate.ToString().ToUpper()].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                IsValid = true;
                            }
                            catch
                            {
                                IsValid = false;
                            }
                            if (IsValid == false)
                            {
                                //Display message of item number.
                                string strMessages = "The Invoice date is not valid for ISM Message.If you press cancel this record will not be added to ISM Message.If Ignore it will proceed.";
                                DialogResult result = CommonUtilities.ShowMappingMessage(strMessages, MessageBoxIcon.Warning);
                                if (Convert.ToString(result) == "Cancel")
                                {
                                    //Remove row of Invoice data.
                                    InvoiceData.Rows.Remove(InvoiceData.Rows[rowIndex]);
                                    InvoiceData.AcceptChanges();
                                    rowIndex++;
                                    continue;
                                }
                            }


                        }

                        #endregion

                    }

                    #endregion
                }
                //Checking Quantity value.
                if (InvoiceData.Columns.Contains(MyobInvoiceColumns.Quantity.ToString().ToUpper()))
                {
                    #region Validations of Quantity

                    if (InvoiceData.Rows[rowIndex][MyobInvoiceColumns.Quantity.ToString()].ToString() != string.Empty)
                    {
                        #region validations of Quantity
                        int qty = 0;
                        if (!int.TryParse(InvoiceData.Rows[rowIndex][MyobInvoiceColumns.Quantity.ToString().ToUpper()].ToString(), out qty))
                        {

                            //Display message of item number.
                            string strMessages = "The Invoice Quantity is not valid for ISM Message.If you press cancel this record will not be added to ISM Message.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMappingMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                //Remove row of Invoice data.
                                InvoiceData.Rows.Remove(InvoiceData.Rows[rowIndex]);
                                InvoiceData.AcceptChanges();
                                rowIndex++;
                                continue;
                            }

                        }

                        #endregion

                    }

                    #endregion
                }
               
                
                #endregion

            }
            #endregion
            //Update dataset changes.
            InvoiceData.AcceptChanges();
            return InvoiceData;
        }

       /// <summary>
       /// This method is used to create ISM stream of Invoice OR1 stream.
       /// </summary>
       /// <param name="invoiceData">Passing Invoice OR1 Header data.</param>
       /// <param name="FilePath">Path of file name</param>
       /// <returns></returns>
        public string ConvertToOR1ISISstreams(MYOBInvoiceList invoiceData, string FilePath)
        {
            try
            {
                //Filepath to create Ism file in directory .
                //string IsmFilePath = FilePath + "\\" + EDI.Constant.Constants.ISMInvoiceStream + DateTime.Now.ToString(EDI.Constant.Constants.ISMFileNameString) + Constants.ISMExtension;
                string IsmFilePath = FilePath;
                //Checking file existance for appending Ism file.
                if (!File.Exists(IsmFilePath))
                {
                    FileStream strm = File.Create(IsmFilePath);
                    strm.Close();
                }
                //Create an instance of stream writer to store file.
                StreamWriter wrt = new StreamWriter(IsmFilePath, true);

                #region Adding data into properties


                //Writing line in streamwriter.
                string writeInvoiceLine = EDI.Constant.Constants.ISMInvoiceOneStream + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMActionIndicaterA +
                    EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.CustomerID
                    + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.PromisedDate + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.PromisedDate + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceDate
                    + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine1 + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine1 + EDI.Constant.Constants.ISMDelimiter.ToString()
                    + invoiceData.ShipToAddressLine2 + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine3 + EDI.Constant.Constants.ISMDelimiter.ToString() +
                    invoiceData.ShipToAddressLine4 + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString()
                    + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShippingMethod + EDI.Constant.Constants.ISMDelimiter + invoiceData.CustomerPONumber + EDI.Constant.Constants.ISMDelimiter + invoiceData.Comment;


                //wrt.WriteLine(EDI.Constant.Constants.ISMInvoiceOneStream + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMActionIndicaterA +
                    //EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.CustomerID
                    //+ EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.PromisedDate + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.PromisedDate + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceDate
                    //+ EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine1 + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine1 + EDI.Constant.Constants.ISMDelimiter.ToString()
                    //+ invoiceData.ShipToAddressLine2 + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine3 + EDI.Constant.Constants.ISMDelimiter.ToString() +
                    //invoiceData.ShipToAddressLine4 + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShipToAddressLine4 + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString()
                    //+ string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ShippingMethod + EDI.Constant.Constants.ISMDelimiter + invoiceData.CustomerPONumber + EDI.Constant.Constants.ISMDelimiter + invoiceData.Comment);

                wrt.WriteLine(writeInvoiceLine);

                #endregion

                this.FileName = IsmFilePath;
                wrt.Close();//Closing streamwriter.

                return IsmFilePath;

            }
            catch(Exception ex)
            {
                //writing error logs.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                throw new Exception();
                //return string.Empty;
                
            }
        }

        /// <summary>
        /// This method is used to create ISM stream of Invoice OR2 stream.
        /// </summary>
        /// <param name="invoiceData">Passing Invoice OR2 Header stream.</param>
        /// <param name="FilePath">Path of file.</param>
        /// <returns></returns>
        public string ConvertToOR2ISISstreams(MYOBInvoiceList invoiceData, string FilePath)
        {
            try
            {
                //Filepath to create Ism file in directory .
                //string IsmFilePath = FilePath + "\\" + EDI.Constant.Constants.ISMInvoiceStream + DateTime.Now.ToString(EDI.Constant.Constants.ISMFileNameString) + Constants.ISMExtension;
                string IsmFilePath = FilePath;
                //Checking file existance for appending Ism file.
                if (!File.Exists(IsmFilePath))
                {
                    FileStream strm = File.Create(IsmFilePath);
                    strm.Close(); 
                }
                //Create an instance of stream writer to store file.
                StreamWriter wrt = new StreamWriter(IsmFilePath, true);

                #region Adding data into properties
                //Writing line in streamwriter.
               
                //wrt.WriteLine(EDI.Constant.Constants.ISMInvoiceTwoStream + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMActionIndicaterA +
                    //EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.LineNumber.ToString() + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ItemNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.Quantity.ToString());

                wrt.WriteLine(EDI.Constant.Constants.ISMInvoiceTwoStream + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMActionIndicaterA +
                    EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.ItemNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.Quantity.ToString());
                   

                #endregion

                this.FileName = IsmFilePath;
                wrt.Close();//Closing streamwriter.

                return IsmFilePath;

            }
            catch (Exception ex)
            {
                //writing error logs.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                throw new Exception();
                //return string.Empty;

            }
        }

        /// <summary>
        /// This method is used to create ISM stream of Invoice OR4 stream.
        /// </summary>
        /// <param name="invoiceData"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public string ConvertToOR4ISISstreams(MYOBInvoiceList invoiceData, string FilePath)
        {
            try
            {
                //Filepath to create Ism file in directory .
                //string IsmFilePath = FilePath + "\\" + EDI.Constant.Constants.ISMInvoiceStream + DateTime.Now.ToString(EDI.Constant.Constants.ISMFileNameString) + Constants.ISMExtension;
                string IsmFilePath = FilePath;
                //Checking file existance for appending Ism file.
                if (!File.Exists(IsmFilePath))
                {
                    FileStream strm = File.Create(IsmFilePath);
                    strm.Close();
                }
                //Create an instance of stream writer to store file.
                StreamWriter wrt = new StreamWriter(IsmFilePath, true);

                #region Adding data into properties
                //Writing line in streamwriter.
                wrt.WriteLine(EDI.Constant.Constants.ISMInvoiceFourStream + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMActionIndicaterA +
                    EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.InvoiceNumber + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + string.Empty + EDI.Constant.Constants.ISMDelimiter.ToString() + invoiceData.Comment);


                #endregion

                this.FileName = IsmFilePath;
                wrt.Close();//Closing streamwriter.

                return IsmFilePath;

            }
            catch (Exception ex)
            {
                //writing error logs.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                throw new Exception();
                //return string.Empty;

            }
        }

        /// <summary>
        /// This override method is used for update Header information of ISM file
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public string ConvertToISISstreams(string FilePath, string ClientID)
        {
            try
            {
                //Filepath to create Ism file in directory.
                string IsmFilePath = FilePath + "\\" + EDI.Constant.Constants.ISMInvoiceStream + DateTime.Now.ToString(EDI.Constant.Constants.ISMFileNameString) + EDI.Constant.Constants.ISMExtension;
                //this.InvoiceFilePath = IsmFilePath;
                
                //Checking file existance for appending Ism file.
                if (!File.Exists(IsmFilePath))
                {
                    FileStream strm = File.Create(IsmFilePath);
                    strm.Close();
                }
                else
                {
                    File.Delete(IsmFilePath);
                    FileStream strm = File.Create(IsmFilePath);
                    strm.Close();

                }
                //Create an instance of stream writer to store file.
                StreamWriter wrt = new StreamWriter(IsmFilePath, true);

                //Writing line in streamwriter.
                wrt.WriteLine(EDI.Constant.Constants.ISMHeader + EDI.Constant.Constants.ISMDelimiter.ToString() + ClientID + EDI.Constant.Constants.ISMDelimiter.ToString() + EDI.Constant.Constants.ISMInvoiceStream);
                this.FileName = IsmFilePath;

                wrt.Close();//Closing streamwriter.

                return IsmFilePath;

            }
            catch(Exception ex)
            {
                //writing error logs.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                
                throw new Exception();
                //return string.Empty;
            }

        }



        #endregion
    }
}
