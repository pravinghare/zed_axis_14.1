﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provides the members of the Bill Payment check .
    /// </summary>
    public class BaseBillPaymentCheckList
    {
        #region Protected Members
        protected string m_TxnID;
        protected string m_TxnNumber;
        protected string m_PayEntityRefFullName;
        protected string m_APAccountRefFullName;
        protected string m_TxnDate;
        protected string m_BankAccountRefFullName;
        protected string m_Amount;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountInHomeCurrency;
        protected string m_RefNumber;
        protected string m_Memo;
        protected string m_Addr1;
        protected string m_Addr2;
        protected string m_City;
        protected string m_State;
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_IsToBePrinted;

        //AppliedToTxnRet collection
        protected string[] m_ATRTxnID = new string[200];

        protected string[] m_ATRRefNumber = new string[200];

        #endregion
    }   

}
