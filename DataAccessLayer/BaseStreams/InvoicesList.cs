using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provides members of Invoices.
    /// </summary>
    public class BaseInvoicesList
    {
        #region Protected Members
        //Adding private member of Invoices.
        //protected string m_CustomerListID;
        protected string m_TxnID;       
        protected string m_TxnNumber;
        protected string m_CustomerFullName;
        protected string m_CustomerListID;
        protected string m_ClassFullName;
        protected string m_ARAccountFullName;
        protected string m_TemplateFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_City;
        protected string m_State;        
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;
        //Axis 10.0
        protected string m_ShipAddress5;

        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_IsFinanceCharge;
        protected string m_PONumber;
        protected string m_TermsFullName;
        protected string m_DueDate;
        protected string m_SalesRepFullName;
        protected string m_FOB;
        protected string m_ShipDate;
        protected string m_ShipMethodFullName;
        protected string m_Memo;
        protected string m_ItemSalesTaxFullName;
        protected string m_SalesTaxPercentage;
        protected decimal m_AppliedAmount;
        protected decimal m_BalanceRemaining;
        protected string m_CurrencyFullName;
        protected decimal m_ExchangeRate;
        protected decimal m_BalanceRemainingInHomeCurrency;
        protected string m_IsPaid;
        protected string m_CustomerMsgFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxCodeFullName;
        protected decimal m_SuggestedDiscountAmount;
        protected string m_SuggestedDiscountDate;
        protected string m_LinkedTxnID;
        //For bug#1496
        protected string m_Other;
        protected string[] m_Other1 = new string[200];
        protected string[] m_Other2 = new string[200];
       
        //for export InvoiceLineRet;    
        protected string[] m_QBTxnLineNumber = new string[200];
        protected string[] m_QBitemFullName = new string[200];
        protected string[] m_Desc = new string[200];
        protected decimal?[] m_QBquantity = new decimal?[200];
        protected string[] m_UOM = new string[200];
        protected string[] m_OverrideUOMFullName = new string[200];
        protected decimal?[] m_Rate = new decimal?[200];
        protected decimal?[] m_Amount = new decimal?[200];
        protected string[] m_InventorySiteRefFullName = new string[200];
        protected string[] m_ServiceDate = new string[200];
        protected string[] m_SalesTaxCodeFullName = new string[200];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends
        //for export InvoiceLineGroupRet;
        protected string[] m_GroupTxnLineID = new string[200];
        protected string[] m_ItemGroupFullName = new string[200];
        protected string[] m_GroupDesc = new string[200];
        protected decimal?[] m_GroupQuantity = new decimal?[200];
        protected string[] m_GroupUOM = new string[200];
        protected string[] m_IsPrintItemsInGroup = new string[200];

        //Sub InvoiceLineRet;
        protected string[] m_GroupQBTxnLineNumber = new string[200];
        protected string[] m_GroupLineItemFullName = new string[200];
        protected string[] m_GroupLineDesc = new string[200];
        protected decimal?[] m_GroupLineQuantity = new decimal?[200];
        protected string[] m_GroupLineUOM = new string[200];
        protected string[] m_GroupLineOverrideUOM = new string[200];
        protected decimal?[] m_GroupLineRate = new decimal?[200];
        protected decimal?[] m_GroupLineAmount = new decimal?[200];
        protected string[] m_GroupLineServiceDate = new string[200];
        protected string[] m_GroupLineSalesTaxCodeFullName = new string[200];

        //MYOB invoice List
        protected string m_invoiceNumber;
        protected string m_customerId;
        protected string m_promisedDate;
        protected string m_invoiceDate;
        protected string m_shipToAddress;
        protected string m_shipToAddressLine1;
        protected string m_shipToAddressLine2;
        protected string m_shipToAddressLine3;
        protected string m_shipToAddressLine4;
        protected string m_shippingMethod;
        protected string m_customerPoNumber;
        protected string m_Comment;
        protected int m_lineNumber;
        protected string m_itemNumber;
        protected int m_quantity;
      
        #endregion
    }
}
