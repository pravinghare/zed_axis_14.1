using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provide members of Received Payments.
    /// </summary>
    public class BaseReceivedPaymentsList
    {
        #region Protected Members
        protected string m_TxnID;
        protected string m_CustomerRefFullName;
        protected string m_ARAccountRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected decimal? m_TotalAmount;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_TotalAmountInHomeCurrency;
        protected string m_PaymentMethodRefFullName;
        protected string m_Memo;
        protected string m_DepositToAccountRefFullName;
        protected string m_UnusedPayment;
        protected string m_UnusedCredits;

        //AppliedToTxnRet collection
        protected string[] m_ATRTxnID = new string[200];
        protected string[] m_ATRTxnType = new string[200];
        protected string[] m_ATRRefNumber = new string[200];
        protected decimal?[] m_ATRAmount = new decimal?[200];
        protected decimal?[] m_ATRDiscountAmount = new decimal?[200];
        protected string[] m_ATRDiscountAccount = new string[200];
        
        #endregion
    }
}
