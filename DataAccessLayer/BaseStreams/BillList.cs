using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class is used to declare Members of CheckList.
    /// </summary>
    public class BaseBillList
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_VendorFullName;
        protected string m_APAccountRefFullName;
        protected string m_TxnDate;
        protected string m_DueDate;
        protected decimal? m_AmountDue;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountDueInHomeCurrency;
        protected string m_RefNumber;
        protected string m_TermsRefFullName;
        protected string m_Memo;
        protected string m_IsTaxIncluded;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_IsPaid;
        protected string m_LinkedTxnID;
        protected decimal? m_OpenAmount;

        //For ExpenseLineRet,repeat
        protected string[] m_ExpTxnLineID = new string[200];
        protected string[] m_AccountRefFullName = new string[200];
        protected decimal?[] m_ExpAmount = new decimal?[200];
        protected string[] m_ExpMemo = new string[200];
        protected string[] m_ExpCustomerRefFullName = new string[200];
        protected string[] m_ExpClassRefFullName = new string[200];
        protected string[] m_ExpSalesTaxFullName = new string[200];
        protected string[] m_BillableStatus = new string[200];

        //For ItemLineRet, repeat
        protected string[] m_ItemTxnLineID = new string[200];
        protected string[] m_ItemRefFullName = new string[200];
        protected string[] m_Desc = new string[200];
        protected decimal?[] m_Quantity = new decimal?[200];
        protected string[] m_UOM = new string[200];
        protected decimal?[] m_Cost = new decimal?[200];
        protected decimal?[] m_ItemAmount = new decimal?[200];
        protected string[] m_ItemCustomerRefFullName = new string[200];
        protected string[] m_ItemClassRefFullName = new string[200];
        protected string[] m_ItemSalesTaxFullName = new string[200];
        protected string[] m_ItemBillableStatus = new string[200];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends
        //For ItemGroupLineRet
        protected string[] m_GroupTxnLineID = new string[200];
        protected string[] m_ItemGroupRefFullName = new string[200];
       
        //For Sub ItemLineRet
        protected string[] m_GroupLineTxnLineID = new string[200];
        protected string[] m_GroupLineItemRefFullName = new string[200];
        protected string[] m_GroupLineDesc = new string[200];
        protected decimal?[] m_GroupLineQuantity = new decimal?[200];
        protected string[] m_GroupLineUOM = new string[200];
        protected decimal?[] m_GroupLineCost = new decimal?[200];
        protected decimal?[] m_GroupLineAmount = new decimal?[200];
        protected string[] m_GroupLineCustomerRefFullName = new string[200];
        protected string[] m_GroupLineClassRefFullName = new string[200];
        protected string[] m_GroupLineSalesTaxFullName = new string[200];
        protected string[] m_GroupLineBillableStatus = new string[200];

        #endregion
    }
}
