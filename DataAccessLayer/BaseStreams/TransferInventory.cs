using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Members for Transfer inventory
    /// </summary>
    public class BaseTransferInventoryList
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_TxnNumber;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_FromInventorySiteRefListID;
        protected string m_FromInventorySiteRefFullName;
        protected string m_ToInventorySiteRefListID;
        protected string m_ToInventorySiteRefFullName;
        protected string m_Memo;
        
        //For TransferInventoryLineAdd
        protected string[] m_TxnLineID = new string[200];
        protected string[] m_ItemRefListID = new string[200];
        protected string[] m_ItemRefFullName = new string[200];
        protected string[] m_FromInventorySiteLineRefFullName = new string[200];
        protected string[] m_ToInventorySiteLineRefFullName = new string[200];
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        protected string[] m_QuantityToTransfer = new string[200];

        #endregion
    }
}
