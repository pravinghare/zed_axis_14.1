using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This Class Provide Members of SalesReceipt.
    /// </summary>
    public class BaseSalesReceiptList
    {
        #region Protected Methods

        protected string m_TxnID;        
        protected string m_TxnNumber;
        protected string m_CustomerListID;
        protected string m_CustomerRefFullName;
        protected string m_ClassRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_City;
        protected string m_State;
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;
        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_CheckNumber;
        protected string m_PaymentMethodRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_Shipdate;
        protected string m_ShipMethodRefFullName;
        protected string m_FOB;
        protected string m_ItemSalesTaxFullName;
        protected string m_SalesTaxPercentage;
        protected decimal? m_MainTotalAmount;
        protected string m_CurrencyFullName;
        protected string m_Memo;
        protected string m_CutomerMsgFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxFullName;
        protected string m_DepositToAccountFullName;     
        protected string m_Other;
        
        
        //SalesReceiptLineRet
        protected string[] m_TxnLineID = new string[200];
        protected string[] m_ItemFullName = new string[200];
        protected string[] m_Desc = new string[200];
        protected decimal?[] m_Quantity = new decimal?[200];
        protected string[] m_UOM = new string[200];
        protected string[] m_OverrideUOMFullName = new string[200];        
        protected decimal?[] m_Rate = new decimal?[200];
        protected string[] m_LineClassRefFullName = new string[200];
        protected decimal?[] m_Amount = new decimal?[200];
        protected string[] m_InventorySiteRefFullName = new string[200];
        protected string[] m_ServiceDate = new string[200];
        protected string[] m_SalesTaxCodeFullName = new string[200];
        protected string[] m_Other1 = new string[200];
        protected string[] m_Other2 = new string[200];
        // axis 10.0 changes
        protected string[] m_SerialNumber = new string[200];
        protected string[] m_LotNumber = new string[200];
        // axis 10.0 changes ends

        //SalesReceiptLineGroupRet
        protected string[] m_GroupTxnLineID = new string[200];
        protected string[] m_ItemGroupFullName = new string[200];
        protected string[] m_GroupDesc = new string[200];
        protected decimal?[] m_GroupQuantity = new decimal?[200];
        protected string[] m_IsPrintItemsInGroup = new string[200];

        //Sub SalesReceiptLineRet
        protected string[] m_GroupLineTxnLineID = new string[200];
        protected string[] m_GroupLineItemFullName = new string[200];
        protected string[] m_GroupLineDesc = new string[200];
        protected decimal?[] m_GroupLineQuantity = new decimal?[200];
        protected string[] m_GroupLineUOM = new string[200];
        protected string[] m_GroupLineOverrideUOM = new string[200];   
        protected decimal?[] m_GroupLineRate = new decimal?[200];
        protected decimal?[] m_GroupLineAmount = new decimal?[200];
        protected string[] m_GroupLineServiceDate = new string[200];
        protected string[] m_GroupLineSalesTaxCodeFullName = new string[200];
        
        #endregion
    }
}
