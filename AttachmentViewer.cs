using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class AttachmentViewer : Form
    {
        private string m_attchmentContent;

        public string AttachmentContent
        {
            get { return AttachmentContent; }
            set { m_attchmentContent = value; }
        }

        public AttachmentViewer()
        {
            InitializeComponent();
            m_attchmentContent = "Empty";
        }

        private void AttachmentViewer_Load(object sender, EventArgs e)
        {
            richTextBoxAttachment.Text = m_attchmentContent;
        }
    }
}