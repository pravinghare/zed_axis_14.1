﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace QBPOSEntities
{
  public class SalesOrderItemAdd
    {
        #region Private Member Variables
        private string m_ListID;
        private string m_ALU;
        private string m_Associate;
        private string m_Attribute;
        private string m_Commission;
        private string m_Desc1;
        private string m_Desc2;
        private string m_Discount;
        private string m_DiscountPercent;
        private string m_DiscountType;
        private string m_ExtendedPrice;
        private string m_Qty;
        private string m_SerialNumber;
        private string m_Price;
        private string m_Size;
        private string m_TaxCode;
        private string m_UnitOfMeasure;
        private string m_UPC;
       #endregion

       #region Constructors
       public SalesOrderItemAdd()
        {

        }
       #endregion

       #region Properties
       public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }       

        public string ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }        

        public string Associate
        {
            get { return m_Associate; }
            set { m_Associate = value; }
        }       

        public string Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }
       
        public string Commission
        {
            get { return m_Commission; }
            set { m_Commission = value; }
        }        

        public string Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }
       
        public string Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }       

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }        

        public string DiscountPercent
        {
            get { return m_DiscountPercent; }
            set { m_DiscountPercent = value; }
        }        

        public string DiscountType
        {
            get { return m_DiscountType; }
            set { m_DiscountType = value; }
        }        

        public string ExtendedPrice
        {
            get { return m_ExtendedPrice; }
            set { m_ExtendedPrice = value; }
        }        

        public string Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }
       
        public string Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }        

        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }        

        public string Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }        

        public string TaxCode
        {
            get { return m_TaxCode; }
            set { m_TaxCode = value; }
        }        

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
       
        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }   
        #endregion
    }
}
