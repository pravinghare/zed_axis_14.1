﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
   public class ShippingInformation
   {
       #region Private Member Variables
       private string m_AddressName;
       private string m_City;
       private string m_CompanyName;
       private string m_Country;
       private string m_FullName;
       private string m_Phone;
       private string m_Phone2;
       private string m_Phone3;       
       private string m_Phone4;
       private string m_PostalCode;
       private string m_ShipBy;
       private string m_Shipping;
       private string m_Street;
       private string m_Street2;      
       #endregion

       #region constructor
       public ShippingInformation()
       {
       }
       #endregion

       #region properties
       public string AddressName
       {
           get { return m_AddressName; }
           set { m_AddressName = value; }
       }

       public string City
       {
           get { return m_City; }
           set { m_City = value; }
       }

       public string CompanyName
       {
           get { return m_CompanyName; }
           set { m_CompanyName = value; }
       }

       public string Country
       {
           get { return m_Country; }
           set { m_Country = value; }
       }

       public string FullName
       {
           get { return m_FullName; }
           set { m_FullName = value; }
       }

       public string Phone
       {
           get { return m_Phone; }
           set { m_Phone = value; }
       }

       public string Phone2
       {
           get { return m_Phone2; }
           set { m_Phone2 = value; }
       }

       public string Phone3
       {
           get { return m_Phone3; }
           set { m_Phone3 = value; }
       }

       public string Phone4
       {
           get { return m_Phone4; }
           set { m_Phone4 = value; }
       }

       public string PostalCode
       {
           get { return m_PostalCode; }
           set { m_PostalCode = value; }
       }

       public string ShipBy
       {
           get { return m_ShipBy; }
           set { m_ShipBy = value; }
       }

       public string Shipping
       {
           get { return m_Shipping; }
           set { m_Shipping = value; }
       }
       private string m_State;

       public string State
       {
           get { return m_State; }
           set { m_State = value; }
       }

       public string Street
       {
           get { return m_Street; }
           set { m_Street = value; }
       }

       public string Street2
       {
           get { return m_Street2; }
           set { m_Street2 = value; }
       }
       #endregion

   }
}
