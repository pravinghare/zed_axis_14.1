﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;


namespace QBPOSEntities
{
   public  class PriceDiscountItemAdd
    {
        #region Private Member Variables

         private string m_ListID;
         private string m_UnitOfMeasure;
        
        #endregion

        #region Constructors

         public PriceDiscountItemAdd()
        {

        }
          #endregion

  

        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }
        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }
    }
}
