﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
     [XmlRootAttribute("InventoryCostAdjustmentItemAdd", Namespace = "", IsNullable = false)]
  public class InventoryCostAdjustmentItemAdd
    {
       #region Private Member Variables
         private string m_ListID;
         private string m_NewCost;
         private string m_UnitOfMeasure;         
       #endregion

        #region Constructors
         public InventoryCostAdjustmentItemAdd()
         {
         }
        // public InventoryCostAdjustmentItemAdd(string listID)
        // {
        //     if (listID != string.Empty)
        //         this.m_ListID = listID;
        // }         

        //public InventoryCostAdjustmentItemAdd(string newCost)
        //{
        //    if (newCost != string.Empty)
        //        this.m_NewCost = newCost;
        //}
        //public InventoryCostAdjustmentItemAdd(string unitOfMeasure)
        //{
        //    if (unitOfMeasure != string.Empty)
        //        this.m_UnitOfMeasure = unitOfMeasure;
        //}

        #endregion

         public string ListID
         {
             get { return this.m_ListID; }
             set { this.m_ListID = value; }
         }

         public string NewCost
         {
             get { return this.m_NewCost; }
             set { this.m_NewCost = value; }
         }

         public string UnitOfMeasure
         {
             get { return m_UnitOfMeasure; }
             set { m_UnitOfMeasure = value; }
         }
    }
}
