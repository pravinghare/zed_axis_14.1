﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
   public class TenderCreditCardAdd
    { 
       #region Private Member Variables
       private string m_CardName;
       private string m_TenderAmount;
       private string m_TipAmount;

       
       #endregion

       #region Constructor
       public TenderCreditCardAdd()
       {
       }
       #endregion

       #region Properties
       public string CardName
       {
           get { return m_CardName; }
           set { m_CardName = value; }
       }
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
       public string TipAmount
       {
           get { return m_TipAmount; }
           set { m_TipAmount = value; }
       }
       #endregion
    }
}
