﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
    [XmlRootAttribute("InventoryQtyAdjustmentItemAdd", Namespace = "", IsNullable = false)]
   public class InventoryQtyAdjustmentItemAdd
    {
        #region Private Member Variables
         private string m_ListID;
         private string m_NewQuantity;
         private string m_SerialNumber;
         private string m_UnitOfMeasure;
         //Axis-565
         private string m_ALU;
         private string m_UPC;
         private string m_Desc1;
       #endregion

        #region Constructors
         public InventoryQtyAdjustmentItemAdd()
         {
         }
      
        #endregion

         public string ListID
         {
             get { return this.m_ListID; }
             set { this.m_ListID = value; }
         }

         public string NewQuantity
         {
             get { return this.m_NewQuantity; }
             set { this.m_NewQuantity = value; }
         }
         public string SerialNumber
         {
             get { return this.m_SerialNumber; }
             set { this.m_SerialNumber = value; }
         }

         public string UnitOfMeasure
         {
             get { return m_UnitOfMeasure; }
             set { m_UnitOfMeasure = value; }
         }

         //Axis-565
         public string ALU
         {
             get { return m_ALU; }
             set { m_ALU = value; }
         }

         public string UPC
         {
             get { return m_UPC; }
             set { m_UPC = value; }
         }

         public string Desc1
         {
             get { return m_Desc1; }
             set { m_Desc1 = value; }
         }
        //Axis-565 end
    }
}
