﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
    public class VendorInfo5
    {
        #region Private Member Variables
        private string m_ALU;
        private string m_OrderCost;
        private string m_UPC;
        private string m_VendorListID;
        #endregion

        public VendorInfo5()
        {

        }
        public VendorInfo5(string ALU, string OrderCost, string UPC, string VendorListID)
        {
            this.m_ALU = ALU;
            this.m_OrderCost = OrderCost;
            this.m_UPC = UPC;
            this.m_VendorListID = VendorListID;
        }

        #region Public Properties

        public string ALU
        {
            get { return this.m_ALU; }
            set { this.m_ALU = value; }
        }

        public string OrderCost
        {
            get { return m_OrderCost; }
            set { m_OrderCost = value; }
        }

        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }

        public string VendorListID
        {
            get { return m_VendorListID; }
            set { m_VendorListID = value; }
        }
        #endregion
    }
}
