﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;


namespace QBPOSEntities
{
    [XmlRootAttribute("PriceAdjustmentItemAdd", Namespace = "", IsNullable = false)]
  public class PriceAdjustmentItemAdd
    {
      #region Private Member Variables

         private string m_ListID;
         private string m_NewPrice;
        
     #endregion

        #region Constructors

         public PriceAdjustmentItemAdd()
        {

        }
          #endregion

        //public PriceAdjustmentItemAdd(string listID, string newPrice)
        //{
        //    this.m_ListID = listID;
        //    this.m_NewPrice = newPrice;
        //}

        //public PriceAdjustmentItemAdd(string newPrice)
        //{
        //    if (newPrice != string.Empty)
        //        this.m_NewPrice = newPrice;
        //}

        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }
        public string NewPrice
        {
            get { return this.m_NewPrice; }
            set { this.m_NewPrice = value; }
        }

    }
}
