﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
   public  class TenderAccountAdd
   {
       #region Private Member Variables
       private string m_TenderAmount;
       private string m_TipAmount;

       
       #endregion

       #region Constructor
       public TenderAccountAdd()
       {
       }
       #endregion

       #region Properties
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
       public string TipAmount
       {
           get { return m_TipAmount; }
           set { m_TipAmount = value; }
       }
       #endregion
   }
    
}
