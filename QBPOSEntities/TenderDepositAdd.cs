﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
 public class TenderDepositAdd
    {
      #region Private Member Variables
       private string m_TenderAmount;
      #endregion

       #region Constructor
       public TenderDepositAdd()
       {
       }
       #endregion

       #region Properties
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
      
       #endregion
    }
}
