﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
  public class TenderDebitCardAdd
    {
      #region Private Member Variables
      private string m_Cashback;
      private string m_TenderAmount;         
       #endregion

       #region constructor
         public TenderDebitCardAdd()
       {
       }
       #endregion

       #region Properties
      
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
       public string Cashback
       {
           get { return m_Cashback; }
           set { m_Cashback = value; }
       }
       #endregion
    }
}
