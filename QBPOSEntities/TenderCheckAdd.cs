﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
  public class TenderCheckAdd
    {
       
      #region private members
      private string m_TenderAmount;
      private string m_CheckNumber;     
      #endregion

      #region contructor
      public TenderCheckAdd()
       {
       }
      #endregion

      #region properties
      public string CheckNumber
      {
          get { return m_CheckNumber; }
          set { m_CheckNumber = value; }
      }
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
      #endregion
    }
}
