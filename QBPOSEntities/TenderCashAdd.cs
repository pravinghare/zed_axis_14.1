﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
  public   class TenderCashAdd
  {
      #region private members
      private string m_TenderAmount;      
      #endregion

      #region contructor
       public TenderCashAdd()
       {
       }
      #endregion

      #region properties
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
      #endregion

  }
}
