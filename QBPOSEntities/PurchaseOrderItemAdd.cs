﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
  public class PurchaseOrderItemAdd
    {
        #region Private Member Variables
        private string m_ListID;
        private string m_ALU;        
        private string m_Attribute;
        private string m_Cost;       
        private string m_Desc1;
        private string m_Desc2;
        private string m_ExtendedCost;     
        private string m_Qty;       
        private string m_Size;       
        private string m_UnitOfMeasure;
        private string m_UPC;
        #endregion

      #region Constructors
        public PurchaseOrderItemAdd()
        {

        }
       #endregion

       #region Properties
       public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }       

        public string ALU
        {
            get { return m_ALU; }
            set { m_ALU = value; }
        }       

        public string Attribute
        {
            get { return m_Attribute; }
            set { m_Attribute = value; }
        }

        public string Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }              

        public string Desc1
        {
            get { return m_Desc1; }
            set { m_Desc1 = value; }
        }
       
        public string Desc2
        {
            get { return m_Desc2; }
            set { m_Desc2 = value; }
        }     
      
        public string ExtendedCost
        {
            get { return m_ExtendedCost; }
            set { m_ExtendedCost = value; }
        }        

        public string Qty
        {
            get { return m_Qty; }
            set { m_Qty = value; }
        }       
           
        public string Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }  

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }
       
        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }   
        #endregion
    }
}
