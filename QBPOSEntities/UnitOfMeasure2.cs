﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
namespace QBPOSEntities
{
  public  class UnitOfMeasure2
    {
      
        #region Private Member Variables

         private string m_ALU;
         private string m_MSRP;
         private string m_NumberOfBaseUnits;
         private string m_Price1;
         private string m_Price2;
         private string m_Price3;
         private string m_Price4;
         private string m_Price5;
         private string m_UnitOfMeasure;
         private string m_UPC;        

        #endregion

        #region Constructors

        public UnitOfMeasure2()
        {

        }

        public UnitOfMeasure2(string ALU, string MSRP, string NumberOfBaseUnits, string Price1,string Price2,string Price3,string Price4,string Price5, string UOM, string UPC)
        {
           this.m_ALU = ALU;
           this.m_MSRP = MSRP;
           this.m_NumberOfBaseUnits = NumberOfBaseUnits;
           this.m_Price1 = Price1;
           this.m_Price2 = Price2;
           this.m_Price3 = Price3;
           this.m_Price4 = Price4;
           this.m_Price5 = Price5;
           this.m_UnitOfMeasure = UOM;
           this.m_UPC = UPC;
        }

        #endregion

        #region Public Properties

        public string ALU
        {
            get { return this.m_ALU; }
            set { this.m_ALU = value; }
        }

        public string MSRP
        {
            get { return m_MSRP; }
            set { m_MSRP = value; }
        }

        public string NumberOfBaseUnits
        {
            get { return m_NumberOfBaseUnits; }
            set { m_NumberOfBaseUnits = value; }
        }
        public string Price1
        {
            get { return m_Price1; }
            set { m_Price1 = value; }
        }


        public string Price2
        {
            get { return m_Price2; }
            set { m_Price2 = value; }
        }


        public string Price3
        {
            get { return m_Price3; }
            set { m_Price3 = value; }
        }

        public string Price4
        {
            get { return m_Price4; }
            set { m_Price4 = value; }
        }

        public string Price5
        {
            get { return m_Price5; }
            set { m_Price5 = value; }
        }

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }

        public string UPC
        {
            get { return m_UPC; }
            set { m_UPC = value; }
        }
        #endregion
    }
}
