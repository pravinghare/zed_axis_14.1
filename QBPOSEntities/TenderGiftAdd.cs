﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace QBPOSEntities
{
  public class TenderGiftAdd
    {
       #region Private Member Variables
         private string m_TenderAmount;
         private string m_GiftCertificateNumber;         
       #endregion

       #region constructor
       public TenderGiftAdd()
       {
       }
       #endregion

       #region Properties
       public string GiftCertificateNumber
       {
           get { return m_GiftCertificateNumber; }
           set { m_GiftCertificateNumber = value; }
       }
       public string TenderAmount
       {
           get { return m_TenderAmount; }
           set { m_TenderAmount = value; }
       }
       #endregion

    
    }
}
