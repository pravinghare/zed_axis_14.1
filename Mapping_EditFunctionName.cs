﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class Mapping_EditFunctionName : Form
    {
        private Collection<string> _functionNames;
        private bool closeForm=true;

        public string functionName
        {
            get
            {
                return this.textBoxFunctionName.Text.TrimStart().TrimEnd().Trim();
            }
        }
        public Mapping_EditFunctionName()
        {
            InitializeComponent();
        }


        public Mapping_EditFunctionName(Collection<string> functionNames)
        {
            _functionNames = functionNames;
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string functionName = this.textBoxFunctionName.Text.Trim().TrimStart().TrimEnd();
            if (functionName == string.Empty)
            {
                MessageBox.Show("Please enter function name", "Zed Axis");
                closeForm = false;
            }
            else if(functionName.Contains("<")|| functionName.Contains(">"))
            {
                MessageBox.Show("Enter valid function name, (Avoid keywords as '<' and '>').", "Zed Axis");
                closeForm = false;
            }
            else
            {
                if (_functionNames.Contains(functionName))
                {
                    MessageBox.Show("Function with this name already exists, Please try with different name", "Zed Axis");
                    closeForm = false;
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void Mapping_EditFunctionName_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!closeForm)
                e.Cancel = true;
            closeForm = true;

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
