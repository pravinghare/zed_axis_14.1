namespace TransactionImporter
{
    partial class MySqlSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlSetting));
            this.buttonMySQLDBOk = new System.Windows.Forms.Button();
            this.openFileDialogMySqlPath = new System.Windows.Forms.OpenFileDialog();
            this.panelMySqlDBInfo = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.linkLabelMysqlDwnld = new System.Windows.Forms.LinkLabel();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.textBoxMySqlPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelMySqlDBInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonMySQLDBOk
            // 
            this.buttonMySQLDBOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonMySQLDBOk.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMySQLDBOk.Location = new System.Drawing.Point(215, 124);
            this.buttonMySQLDBOk.Name = "buttonMySQLDBOk";
            this.buttonMySQLDBOk.Size = new System.Drawing.Size(75, 23);
            this.buttonMySQLDBOk.TabIndex = 4;
            this.buttonMySQLDBOk.Text = "&OK";
            this.buttonMySQLDBOk.UseVisualStyleBackColor = true;
            this.buttonMySQLDBOk.Click += new System.EventHandler(this.buttonMySQLDBOk_Click);
            // 
            // openFileDialogMySqlPath
            // 
            this.openFileDialogMySqlPath.FileName = "openFileDialogMySqlPath";
            this.openFileDialogMySqlPath.InitialDirectory = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.0\\bin";
            // 
            // panelMySqlDBInfo
            // 
            this.panelMySqlDBInfo.Controls.Add(this.label4);
            this.panelMySqlDBInfo.Controls.Add(this.buttonCancel);
            this.panelMySqlDBInfo.Controls.Add(this.linkLabelMysqlDwnld);
            this.panelMySqlDBInfo.Controls.Add(this.buttonBrowse);
            this.panelMySqlDBInfo.Controls.Add(this.textBoxMySqlPath);
            this.panelMySqlDBInfo.Controls.Add(this.label3);
            this.panelMySqlDBInfo.Controls.Add(this.buttonMySQLDBOk);
            this.panelMySqlDBInfo.Location = new System.Drawing.Point(22, 62);
            this.panelMySqlDBInfo.Name = "panelMySqlDBInfo";
            this.panelMySqlDBInfo.Size = new System.Drawing.Size(587, 157);
            this.panelMySqlDBInfo.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(486, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "*";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(312, 124);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // linkLabelMysqlDwnld
            // 
            this.linkLabelMysqlDwnld.AutoSize = true;
            this.linkLabelMysqlDwnld.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelMysqlDwnld.Location = new System.Drawing.Point(260, 84);
            this.linkLabelMysqlDwnld.Name = "linkLabelMysqlDwnld";
            this.linkLabelMysqlDwnld.Size = new System.Drawing.Size(221, 13);
            this.linkLabelMysqlDwnld.TabIndex = 3;
            this.linkLabelMysqlDwnld.TabStop = true;
            this.linkLabelMysqlDwnld.Text = "Download MySQL and ODBC Installer";
            this.linkLabelMysqlDwnld.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMysqlDwnld_LinkClicked);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBrowse.Location = new System.Drawing.Point(503, 40);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(75, 21);
            this.buttonBrowse.TabIndex = 0;
            this.buttonBrowse.Text = "&Browse ...";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // textBoxMySqlPath
            // 
            this.textBoxMySqlPath.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxMySqlPath.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMySqlPath.Location = new System.Drawing.Point(149, 41);
            this.textBoxMySqlPath.MaxLength = 10;
            this.textBoxMySqlPath.Name = "textBoxMySqlPath";
            this.textBoxMySqlPath.ReadOnly = true;
            this.textBoxMySqlPath.Size = new System.Drawing.Size(334, 21);
            this.textBoxMySqlPath.TabIndex = 6;
            this.textBoxMySqlPath.Text = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.0\\bin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "MySql Installation Path:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(590, 46);
            this.label7.TabIndex = 6;
            this.label7.Text = resources.GetString("label7.Text");
            // 
            // MySqlSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(621, 232);
            this.ControlBox = false;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panelMySqlDBInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MySqlSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set up Axis database";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlSetting_FormClosing);
            this.Load += new System.EventHandler(this.MySqlSetting_Load);
            this.panelMySqlDBInfo.ResumeLayout(false);
            this.panelMySqlDBInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonMySQLDBOk;
        private System.Windows.Forms.OpenFileDialog openFileDialogMySqlPath;
        private System.Windows.Forms.Panel panelMySqlDBInfo;
        private System.Windows.Forms.TextBox textBoxMySqlPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.LinkLabel linkLabelMysqlDwnld;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
    }
}