﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Overview of QuickBooks Assembly Items and Build Assembly</title>
    <link rel="StyleSheet" href="css/23_BuildAssembly.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="23_BuildAssembly.32.1.html#171048">Using Assembly Item and BuildAssembly Functionality</a> : Overview of QuickBooks Assembly Items and Build Assembly </div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="171048">Overview of QuickBooks Assembly Items and Build Assembly </a></h2>
      <p class="T1_002eText1"><a name="171410">An assembly item in QuickBooks is an inventory-tracked item made up of individual </a>inventory items (inventory “parts” in the UI) and/or other assembly items as shown in <a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.2.html#171207', '');">Figure 30-1</a>. The items and/or assemblies that make up the assembly are called <em class="italics">components</em>. All of the components in an assembly must first be defined in the QuickBooks company as inventory items or assembly items before you can use them in an assembly. (Services cannot be used as a component part.) </p>
      <p class="Anchor"><a name="171200"><img id="171204" class="Default" src="images/AssemblyAndComponents.png" width="486" height="183" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 30-1	<a name="171207">Assembly item and its component parts</a></div>
      <p class="T1_002eText1"><a name="171199">How many components can an assembly have? For Premier, an assembly can have a </a>maximum of 100 components. For Enterprise, the maximum is 500 components. </p>
      <p class="T1_002eText1"><a name="171338">When you add an assembly item (ItemInventoryAssemblyAddRq in the SDK</a><span style="font-family: Verdana; font-size: 10.0pt;">) </span>you produce no affect on any inventory levels, since the assembly item serves as a “definition” that specifies how the assembly is to be built, which accounts are to be used, the sale price, and so forth. Inventory is not affected until you perform a Build Assembly transaction either via the QuickBooks UI or via the SDK BuildAssemblyAdd request.</p>
      <p class="T1_002eText1"><a name="170874">As a result of the build assembly transaction, the assembly item units are incremented in </a>inventory and the component parts or component assemblies are decremented from inventory.</p>
      <p class="Note"><a name="171436">If you enter a quantity on hand in the New Item window while </a>defining a new assembly item, this transaction is recorded as an inventory adjustment where assembly units are added to inventory but components are not deducted from inventory.</p>
      <p class="T1_002eText1"><a name="171815">Notice that inventory permissions are required to add assembly items and build assemblies.</a></p>
      <h3 class="H2_002eHeading2"><a name="171785">You Must Have Sufficient Components for the BuildAssembly</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171847">In the QuickBooks UI, you have the option of creating a pending build if there aren’t </a>enough components in inventory to build the assembly in the quantities specified. Beginning with qbXML 7.0 and QuickBooks 2008, you can also do this via the SDK using the MarkPendingIfRequired. </p>
      <p class="T1_002eText1"><a name="171848">If you don’t use MarkPendingIfRequired and you invoke BuildAssemblyAdd with a </a>quantity that would exceed the on-hand quantities of any component, you get a status code error of 3370 and a status message indicating that there are insufficient components for the request.</p>
      <p class="T1_002eText1"><a name="171857">You will get the same status code error 3370 if you attempt to use BuildAssemblyMod to </a>remove the pending status from a pending BuildAssembly transaction (created via the UI) if you lack sufficient component quantities, because this is effectively performing a BuildAssembly.</p>
      <h3 class="H2_002eHeading2"><a name="172033">QB Activities that Change BuildAssembly Transactions into Pending</a></h3>
      <p class="T1_002eText1"><a name="172034">There are circumstances where even a finalized BuildAssembly transaction can be changed </a>into a pending transaction by other activities in QuickBooks, whether from the UI or from the SDK.</p>
      <p class="T1_002eText1"><a name="172035">Finalized builds change to pending whenever the quantity of at least one component drops </a>below the quantity needed to build the specified number of assemblies on the build transaction date. </p>
      <p class="T1_002eText1"><a name="172081">This means that if a QB user or integrated application changes past inventory quantities or </a>the dates of purchase orders, invoices, or sales receipts in ways that result in QuickBooks built assemblies lacking sufficient components on the build date, the affected assembly builds would change from finalized to pending.</p>
      <h3 class="H2_002eHeading2"><a name="171719">Consequences of Modifying an Existing Inventory Assembly Item</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171720">Assembly definition details, such as the list of and quantity of components, can be modified </a>at any time either in the UI or via the SDK. If an assembly item is modified while there is a pending build for that assembly, at the time when the pending build is actually built QuickBooks prompts the user to build either with the most recent assembly definition or with the definition that is currently in effect for that build.</p>
      <p class="T1_002eText1"><a name="172050">Keep in mind that such modifications to the assembly (revision history) is not tracked; if </a>you need to build a previous version of an assembly, you need to modify the assembly again to reflect the desired component list. One feature that can help you reconstruct a previously used assembly item is to look up a past BuildAssembly transaction (BuildAssemblyQuery in the SDK) and use the component list from that transaction. The component list for the transaction is saved even if the assembly item’s component list is changed subsequent to the BuildAssembly transaction. However, this approach requires the QuickBooks user or the integrated application to note and keep track of whichever BuildAssembly transaction (and thus its component list) is important for revision history purposes.</p>
      <p class="T1_002eText1"><a name="171722">Finally, keep in mind that changing quantity on hand for assemblies adjusts the overall </a>number of assembly units in inventory, but it does not change the quantity on hand of components (inventory part items or assemblies) used in the parent assembly.</p>
      <h3 class="H2_002eHeading2"><a name="172118">Impact of SalesReceipts and Invoices on Assemblies in Inventory</a></h3>
      <p class="T1_002eText1"><a name="172120">If a QB user or application attempts to sell via sales receipt or invoice more assembly units </a>than are available in inventory, the UI behavior varies slightly from the SDK. In the UI, the user is warned that quantities are insufficient to fulfill the order: the user can respond by accepting or cancelling. In the SDK, the SalesReceiptAdd or InvoiceAdd simply adds the SalesReceipt or Invoice without the warning. But for both SDK and UI (assuming the UI user opts to continue with the transaction), the quantity on hand for the specified assembly changes to a negative value. </p>
      <p class="Note"><a name="172170">When assembly items appear on a UI form (for example, a </a>sales receipt or an invoice) or a report, their component items are not displayed. Only the assembly name, description, and price are displayed.</p>
      <p class="T1_002eText1"><a name="172121">Notice that if you modify the component list of an assembly and build that assembly while </a>you still have a quantity of a previous version in stock, QuickBooks cannot distinguish between these two versions at the time you make a sale. The typical recommendation is to either sell out of one version before building another version, or “disassemble” the on hand inventory of the previous build. We’ll show you how to do that shortly. </p>
      <p class="T1_002eText1"><a name="172195">Either approach enables a QB user or integrated application to track the versions by the sale </a>date, if the user or application keeps track of this date.</p>
      <h3 class="H2_002eHeading2"><a name="172124">Disassembling Inventory Assemblies</a></h3>
      <p class="TA_002eTextAfterHead"><a name="172125">As noted previously, in some instances you may need to disassemble inventory assemblies, </a>for example, if you are changing the component list and want to maintain only one version of the assembly in inventory.</p>
      <p class="T1_002eText1"><a name="172202">There are several ways to disassemble inventory assemblies and return component items to </a>inventory. You can:</p>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="172128">Open the build transaction that built the assembly you want to disassemble and reduce </a>the quantity to build in the Build Assemblies window. The result is the same as if you had only built the smaller number of assemblies in the first place. The quantity of assembly units in inventory decreases, and the quantity of component inventory parts is increased accordingly.</div>
      <p class="NT_002eNoteText"><a name="172130">Note: Build transactions with changed amounts will display on the audit </a>trail report.</p>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="172131">Adjust Quantity/Value on Hand for each assembly component and the assembly item.</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="172263">Delete the build transaction. The quantity of assembly units in inventory is decreased </a>and the quantity of component inventory parts is increased accordingly. This method completely removes the build transaction from QuickBooks and should not be used if you want to maintain a record of the transaction.</div>
      <h3 class="H2_002eHeading2"><a name="171995">Getting BuildAssembly and Assembly Item Reports</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171996">You can use the SDK’s CustomSummaryReportQuery or CustomDetailReportQuery to get </a>BuildAssembly and item assembly reports. To get BuildAssembly reports, use the ReportTxnTypeFilter with the TxnFilter set to BuildAssembly. To get assembly item reports, use the ReportItemFilter with the ItemTypeFilter set to InventoryAndAssembly.</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>