<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Querying for Workers Comp Codes</title>
    <link rel="StyleSheet" href="css/22_1_WorkerCompCode.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="22_1_WorkerCompCode.29.1.html#170835">Adding, Modifying, Querying Worker Comp Codes</a> : Querying for Workers Comp Codes</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="170835">Querying for Workers Comp Codes</a></h2>
      <p class="TA_002eTextAfterHead"><a name="170836">To query for comp codes using the SDK, use the request WorkersCompCodeQuery. You </a>can filter on the name of the code, the active status, effective date/date range, and modified date.</p>
      <p class="T1_002eText1"><a name="170878">Notice that this query does not support iterators, as the number of codes is expected to be </a>relatively small.</p>
      <h3 class="H2_002eHeading2"><a name="171033">Querying for Comp Codes in qbXML</a></h3>
      <p class="T1_002eText1"><a name="171031">The following qbXML shows a query that checks for all comp codes (inactive as well as </a>active) that have the number 5 in the comp code name, and that has an effective date between January 1, 2003 and December 28, 2008. Also, just to show another filter in the mix, we want only those that have been modified between the start of 2003 and the end of 2007.</p>
      <p class="Cv_002eCode"><a name="170883">&lt;?xml version="1.0"?&gt;</a><br />&lt;?qbxml version="7.0"?&gt;<br />&lt;QBXML&gt;<br />&nbsp;&nbsp;&nbsp;&lt;QBXMLMsgsRq onError="continueOnError"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;WorkersCompCodeQueryRq requestID="2"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ActiveStatus&gt;All&lt;/ActiveStatus&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;FromModifiedDate&gt;2003-01-01&lt;/FromModifiedDate&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ToModifiedDate&gt;2007-12-28&lt;/ToModifiedDate&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;NameFilter&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;MatchCriterion&gt;Contains&lt;/MatchCriterion&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;Name&gt;5&lt;/Name&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/NameFilter&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;FromEffectiveDate&gt;2003-01-01&lt;/FromEffectiveDate&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ToEffectiveDate&gt;2008-12-28&lt;/ToEffectiveDate&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/WorkersCompCodeQueryRq&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/QBXMLMsgsRq&gt;<br />&lt;/QBXML&gt;</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>