﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Modifying an Existing BuildAssembly Transaction</title>
    <link rel="StyleSheet" href="css/23_BuildAssembly.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="23_BuildAssembly.32.1.html#171618">Using Assembly Item and BuildAssembly Functionality</a> : Modifying an Existing BuildAssembly Transaction</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="171618">Modifying an Existing BuildAssembly Transaction</a></h2>
      <p class="TA_002eTextAfterHead"><a name="171623">The OSR listing below (</a><a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.7.html#174697', '');">Figure 30-10</a>) shows the available modifications you can make to a BuildAssembly transaction.</p>
      <p class="Anchor"><a name="174680"><img id="174685" class="Default" src="images/BuildAssemblyMod.png" width="655" height="186" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 30-10	<a name="174697">BuildAssemblyMod OSR Listing</a></div>
      <p class="T1_002eText1"><a name="174727">Notice that along with TxnID, every BuildAssemblyMod requires the edit sequence, which </a>prevents accidental overwrites to data by ensuring you are saving the most recent version of the data. This means that before modifying any existing BuildAssembly transaction, you must first do a BuildAssemblyQuery--even if you already have the TxnID--to get the current EditSequence. </p>
      <p class="Important">
        <b style="font-weight: bold;">
          <img src="images" alt="*" id="bullet175609" border="0" width="" height="" />Note: </b><a name="175609">You’ll also want to check for status code 3200 in the response </a>to your mod request. This code indicates that someone has modified the transaction since you last retrieved the transaction from QuickBooks. You’ll have to re-retrieve that transaction and apply your mods to that newer version.</p>
      <p class="T1_002eText1"><a name="174681">If the transaction is pending, you can remove the pending status, effectively performing the </a>build. This mod will work in the SDK only if there are sufficient components for the build on the TxnDate.</p>
      <p class="T1_002eText1"><a name="174858">Any fields not explicitly modified in the BuildAssemblyMod request keep their existing </a>values.</p>
      <h3 class="H2_002eHeading2"><a name="174838">Modifying a BuildAssembly in qbXML</a></h3>
      <p class="TA_002eTextAfterHead"><a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.7.html#174859', '');" name="174846">Listing 30-8</a> shows a BuildAssemblyMod request that sets new values for the Memo and QuantityToBuild fields. The edit sequence was obtained from a previous BuildAssemblyQuery.</p>
      <div class="L_002eListing">	Listing 30-8	<a name="174859">Constructing a BuildAssemblyMod in qbXML</a></div>
      <p class="Cv_002eCode"><a name="174866">&lt;?qbxml version="5.0"?&gt;</a><br />&lt;QBXML&gt;<br />&lt;QBXMLMsgsRq onError="continueOnError"&gt;<br />&nbsp;&nbsp;&nbsp;&lt;BuildAssemblyModRq&gt;<br />&nbsp;&nbsp;&nbsp;&lt;BuildAssemblyMod&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TxnID&gt;25-1130282678&lt;/TxnID&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;EditSequence&gt;1130282678&lt;/EditSequence&gt; <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;Memo&gt;Built from component list 2&lt;/Memo&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;QuantityToBuild&gt;4&lt;/QuantityToBuild&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/BuildAssemblyMod&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/BuildAssemblyModRq&gt; <br />&lt;/QBXMLMsgsRq&gt;<br />&lt;/QBXML&gt;</p>
      <h3 class="H2_002eHeading2"><a name="174849">Modifying a BuildAssembly in QBFC</a></h3>
      <p class="T1_002eText1"><a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.7.html#174949', '');" name="174847">Listing 30-9</a> shows a self-contained procedure that </p>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="175066">Opens a connection with QuickBooks</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="175070">Queries for build assembly transactions that fall within the specified date range</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="175071">Arbitrarily picks the first transaction from the query and saves its TxnID and edit </a>sequence for the upcoming mod request</div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="175073">Issues a mod request using the TxnID and edit sequence obtained in the query, </a>modifying the memo and the quantity</div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="175079">Shows the results, and closes the connection. </a></div>
      <p class="T1_002eText1"><a name="175718">Notice that the query specifies that only TxnID and EditSequence (via IncludeRetElement) </a>is to be returned in the response because that is all we need from the query.</p>
      <div class="L_002eListing">	Listing 30-9	<a name="174949">Querying for a BuildAssembly Transaction and modifying it</a></div>
      <p class="Cv_002eCode"><a name="174958">Public Sub QBFC_ModBuildAssembly()</a></p>
      <p class="Cv_002eCode"><a name="174959">&nbsp;</a>&nbsp;&nbsp;Dim SessionManager As QBSessionManager<br />&nbsp;&nbsp;&nbsp;Set SessionManager = New QBSessionManager<br />&nbsp;&nbsp;&nbsp;SessionManager.OpenConnection "", "IDN Mod Build Assembly Sample"<br />&nbsp;&nbsp;&nbsp;SessionManager.BeginSession "", omDontCare</p>
      <p class="Cv_002eCode"><a name="175101">&nbsp;</a>&nbsp;&nbsp;Dim requestMsgSet As IMsgSetRequest<br />&nbsp;&nbsp;&nbsp;Set requestMsgSet = SessionManager.CreateMsgSetRequest("US", 5, 0)<br />&nbsp;&nbsp;&nbsp;' Initialize the message set request's attributes<br />&nbsp;&nbsp;&nbsp;requestMsgSet.Attributes.OnError = roeStop</p>
      <p class="Cv_002eCode"><a name="175160">&nbsp;</a>&nbsp;&nbsp;'First we query QB for build assembly transactions within a date range<br />&nbsp;&nbsp;&nbsp;Dim BuildAssmblyQuery As IBuildAssemblyQuery<br />&nbsp;&nbsp;&nbsp;Set BuildAssmblyQuery = requestMsgSet.AppendBuildAssemblyQueryRq<br />&nbsp;&nbsp;&nbsp;BuildAssmblyQuery.IncludeRetElementList.Add ("EditSequence")<br />&nbsp;&nbsp;&nbsp;BuildAssmblyQuery.IncludeRetElementList.Add ("TxnID")<br />&nbsp;&nbsp;&nbsp;BuildAssmblyQuery.ORBuildAssemblyQuery.BuildAssemblyFilter. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ORDateRangeFilter.TxnDateRangeFilter.ORTxnDateRangeFilter. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TxnDateFilter.FromTxnDate.setValue ("2005-10-25")<br />&nbsp;&nbsp;&nbsp;BuildAssmblyQuery.ORBuildAssemblyQuery.BuildAssemblyFilter. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ORDateRangeFilter.TxnDateRangeFilter.ORTxnDateRangeFilter. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TxnDateFilter.ToTxnDate.setValue ("2005-10-26")</p>
      <p class="Cv_002eCode"><a name="175210">&nbsp;</a>&nbsp;&nbsp;Dim responseMsgSet As IMsgSetResponse<br />&nbsp;&nbsp;&nbsp;Set responseMsgSet = SessionManager.DoRequests(requestMsgSet)<br />&nbsp;&nbsp;&nbsp;Dim response As IResponse</p>
      <p class="Cv_002eCode"><a name="175227">&nbsp;</a>&nbsp;&nbsp;' Responselist contains one response, because we made one request<br />&nbsp;&nbsp;&nbsp;Set response = responseMsgSet.ResponseList.GetAt(0)</p>
      <p class="Cv_002eCode"><a name="177106">'Make sure there is data first</a><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If response.Detail Is Nothing Then<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MsgBox "No Detail available"<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exit Sub<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End If</p>
      <p class="Cv_002eCode"><a name="177109">&nbsp;</a>&nbsp;&nbsp;'This is a query, so the response detail is a ret list<br />&nbsp;&nbsp;&nbsp;Dim BuildAssemblyRetList As IBuildAssemblyRetList<br />&nbsp;&nbsp;&nbsp;Dim BuildAssemblyRet As IBuildAssemblyRet<br />&nbsp;&nbsp;&nbsp;Set BuildAssemblyRetList = response.Detail</p>
      <p class="Cv_002eCode"><a name="175267">&nbsp;</a>&nbsp;&nbsp;'Potentially many transactions in the retlist: we get the first one<br />&nbsp;&nbsp;&nbsp;'for our convenience: you'll do something smarter or let the user pick<br />&nbsp;&nbsp;&nbsp;Set BuildAssemblyRet = BuildAssemblyRetList.GetAt(0)</p>
      <p class="Cv_002eCode"><a name="175287">&nbsp;</a>&nbsp;&nbsp;'Save the TxnID and EditSequence: we need 'em for our mod request<br />&nbsp;&nbsp;&nbsp;Dim TransID As String<br />&nbsp;&nbsp;&nbsp;Dim EditSeq As String<br />&nbsp;&nbsp;&nbsp;TransID = BuildAssemblyRet.TxnID.getValue<br />&nbsp;&nbsp;&nbsp;EditSeq = BuildAssemblyRet.EditSequence.getValue<br />&nbsp;&nbsp;&nbsp;‘clear the message set so we can re-stuff it with the mod request<br />&nbsp;&nbsp;&nbsp;requestMsgSet.ClearRequests</p>
      <p class="Cv_002eCode"><a name="175316">&nbsp;</a>&nbsp;&nbsp;' Add the request to the message set request object<br />&nbsp;&nbsp;&nbsp;Dim BuildAssmblyMod As IBuildAssemblyMod<br />&nbsp;&nbsp;&nbsp;Set BuildAssmblyMod = requestMsgSet.AppendBuildAssemblyModRq</p>
      <p class="Cv_002eCode"><a name="175337">&nbsp;</a>&nbsp;&nbsp;'Set the properties in the BuildAssembly mod object<br />&nbsp;&nbsp;&nbsp;BuildAssmblyMod.TxnID.setValue (TransID)<br />&nbsp;&nbsp;&nbsp;BuildAssmblyMod.EditSequence.setValue (EditSeq)<br />&nbsp;&nbsp;&nbsp;BuildAssmblyMod.TxnDate.setValue ("2005-10-25")<br />&nbsp;&nbsp;&nbsp;BuildAssmblyMod.Memo.setValue ("Build from component list 4")<br />&nbsp;&nbsp;&nbsp;BuildAssmblyMod.QuantityToBuild.setValue (5)</p>
      <p class="Cv_002eCode"><a name="175366">&nbsp;</a>&nbsp;&nbsp;' Perform the request and obtain a response from QuickBooks<br />&nbsp;&nbsp;&nbsp;Set responseMsgSet = SessionManager.DoRequests(requestMsgSet)<br />&nbsp;&nbsp;&nbsp; MsgBox responseMsgSet.ToXMLString<br />&nbsp;&nbsp;&nbsp;' Close the session and connection with QuickBooks.<br />&nbsp;&nbsp;&nbsp;SessionManager.EndSession<br />&nbsp;&nbsp;&nbsp;SessionManager.CloseConnection<br />End Sub</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>