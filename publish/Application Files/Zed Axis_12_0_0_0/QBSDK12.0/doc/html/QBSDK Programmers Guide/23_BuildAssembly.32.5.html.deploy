﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Querying for Inventory Assembly Items</title>
    <link rel="StyleSheet" href="css/23_BuildAssembly.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="23_BuildAssembly.32.1.html#171612">Using Assembly Item and BuildAssembly Functionality</a> : Querying for Inventory Assembly Items</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="171612">Querying for Inventory Assembly Items</a></h2>
      <p class="T1_002eText1"><a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.5.html#175924', '');" name="175954">Figure 30-5</a> shows the query filters you can use. If you’re familiar with SDK queries, there is nothing special or tricky about this particular query. If you’re not familiar with SDK queries, you might want to take a quick look at <a href="javascript:WWHClickedPopup('QBSDKProGuide', '04_SupportingUserAuth.6.1.html#168208', '');">Chapter 4, “Specifying Authorization Preferences,”</a> which provides general information on queries, such as using iterators for large query returns, using IncludeRetElement to get only the data you need, using various types of filters, and so on.</p>
      <p class="T1_002eText1"><a name="175958">The ActiveStatus filter is useful if you need to make your query retrieve any inactive </a>assembly items. (By default only active items are returned in the query.) Specify the value “All” to get all assemblies, both active or inactive, or “InactiveOnly” to get only inactive items.</p>
      <p class="Anchor"><a name="175878"><img id="175883" class="Default" src="images/ItemAssemblyQuery.png" width="867" height="396" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 30-5	<a name="175924">ItemInventoryAssemblyQuery OSR listing</a></div>
      <h3 class="H2_002eHeading2"><a name="175631">Querying for Assembly Items in qbXML</a></h3>
      <p class="TA_002eTextAfterHead"><a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.5.html#175770', '');" name="175769">Listing 30-5</a> shows a sample query in qbXML, where we search for all active and inactive item assemblies that contain the name “Panel”, and that were last modified in the specified date range,</p>
      <div class="L_002eListing">	Listing 30-5	<a name="175770">Constructing an ItemInventoryAssemblyQuery Request in qbXML</a></div>
      <p class="Cv_002eCode"><a name="175780">&lt;?xml version="1.0" ?&gt;</a><br />&lt;?qbxml version="5.0"?&gt;<br />&nbsp;&nbsp;&nbsp;&lt;QBXML&gt;<br />&nbsp;&nbsp;&nbsp;&lt;QBXMLMsgsRq onError = "stopOnError"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ItemInventoryAssemblyQueryRq requestID = "0"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ActiveStatus&gt;All&lt;/ActiveStatus&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;FromModifiedDate&gt;2005-10-01&lt;/FromModifiedDate&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ToModifiedDate&gt;2005-10-27&lt;/ToModifiedDate&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;NameFilter&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;MatchCriterion&gt;Contains&lt;/MatchCriterion&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;Name&gt;Panel&lt;/Name&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/NameFilter&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/ItemInventoryAssemblyQueryRq&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/QBXMLMsgsRq&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/QBXML&gt;</p>
      <h3 class="H2_002eHeading2"><a name="175632">Querying for Assembly Items in QBFC</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171596">We’ve already shown an example of constructing an ItemInventoryAssemblyQuery and </a>processing some of its response data in <a href="javascript:WWHClickedPopup('QBSDKProGuide', '23_BuildAssembly.32.4.html#175816', '');">Listing 30-4</a>.</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>