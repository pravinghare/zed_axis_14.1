﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Using the Quickbooks Vehicle Mileage Feature</title>
    <link rel="StyleSheet" href="css/22_Vehicle_Mileage.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">Using the Quickbooks Vehicle Mileage Feature</div>
    <hr align="left" />
    <blockquote>
      <h2 class="ChT_002eChapterTitle"><a name="166240">Using the Quickbooks Vehicle Mileage Feature</a></h2>
      <p class="T1_002eText1"><a name="169105">In QuickBooks, vehicle mileage tracking is trip-based, with each trip and its mileage </a>entered as a separate transaction in QuickBooks, with QuickBooks automatically calculating the resulting mileage costs based on user supplied rates. When you need to see the information accumulated from these transactions, you can run the Mileage by Vehicle or Mileage by Job reports (<a href="javascript:WWHClickedPopup('QBSDKProGuide', '22_Vehicle_Mileage.28.1.html#169376', '');">Figure 26-1</a>).</p>
      <p class="Anchor"><a name="169361"><img id="169365" class="Default" src="images/Overview.png" width="553" height="278" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 26-1	<a name="169376">Mileage transactions and mileage expense reports</a></div>
      <p class="T1_002eText1"><a name="169384">In addition to accumulating and generating data for reports, another way to use the vehicle </a>mileage feature is to use it in conjunction with invoices to bill customers for mileage-related charges. To use the feature in this way you simply make the transaction billable and add a little more information to the vehicle mileage transaction, such as the customer to be charged and the item used to set the <em class="italics">billable</em> mileage rate (<a href="javascript:WWHClickedPopup('QBSDKProGuide', '22_Vehicle_Mileage.28.1.html#169430', '');">Figure 26-2</a>).</p>
      <p class="Anchor"><a name="169412"><img id="169416" class="Default" src="images/Billable_mileage.PNG" width="543" height="350" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" />bi</a></p>
      <div class="Fg_002eFigure">Figure 26-2	<a name="169430">Billable mileage transaction</a></div>
      <p class="T1_002eText1"><a name="167638">Keep in mind that the </a><em class="italics">tax</em> mileage rate is completely unrelated to and unaffected by the billable mileage rate, and cannot be set via the SDK. For that reason, QuickBooks provides two separate mileage reports: Mileage by Vehicle, which reports only the tax-related vehicle mileage and mileage expense, and Mileage by Job, which tracks both non billable and billable mileage (along with the billable charges).</p>
      <h3 class="H2_002eHeading2"><a name="167627">Key Limitations of QB SDK Support for Vehicle Mileage</a></h3>
      <p class="T1_002eText1"><a name="167628">One key limitation of the SDK’s support of this feature is that you cannot automate the </a>insertion of billable mileage charges directly into invoices via QB SDK requests. Only the QuickBooks interactive user can do that because it entails choosing charges from a list of outstanding customer charges at the time of invoice creation, and the company owner (the QuickBooks interactive user) must remain in control of this.</p>
      <p class="T1_002eText1"><a name="167631">Another limitation, which we have already mentioned, is that the mileage rate used to </a>calculate mileage expense for the tax agency (such as the IRS) can only be set in the QuickBooks UI, via the Mileage Rates button in the Mileage entry form. Only the billable mileage rate can be specified via the QB SDK, by setting the amount in the item (service item or other charge item) you use for mileage charges. </p>
      <p class="T1_002eText1"><a name="167816">Finally, the Vehicle Mileage Mod operation is currently not supported, whereas you can </a>modify a vehicle mileage transaction in the UI.</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>