﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>How Do I Create Data Extensions?</title>
    <link rel="StyleSheet" href="css/11_CustomFields.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="11_CustomFields.15.1.html#173802">Data Ext: Using Custom Fields and Private Data</a> : How Do I Create Data Extensions?</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="173802">How Do I Create Data Extensions?</a></h2>
      <p class="TA_002eTextAfterHead"><a name="173803">On the theory that a picure is worth a thousand words, let’s start off with a couple of </a>diagrams.</p>
      <p class="Anchor"><a name="173807"><img id="173805" class="Default" src="images/DataExtDef_dataExts.png" width="578" height="435" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 13-1	<a name="173809">Order of creation: Data Ext Definition created first, then Data Ext</a></div>
      <p class="T1_002eText1"><a name="173813">As we hope to convey in </a><a href="javascript:WWHClickedPopup('QBSDKProGuide', '11_CustomFields.15.3.html#173809', '');">Figure 13-1</a>, you start off your custom field or private data work by creating the data ext definition <em class="italics">first</em>, during which process you specify the objects you want to assign this definition to. We happened to pick customer, employee, and vendor, which will work for both custom data and private data.</p>
      <p class="T1_002eText1"><a name="173814">After you create the definition and assign it to one or more object types, you can write the </a>actual custom data or private data to an instance of the object that has the definition, as we show in <a href="javascript:WWHClickedPopup('QBSDKProGuide', '11_CustomFields.15.3.html#173823', '');">Figure 13-2</a>.</p>
      <p class="Anchor"><a name="173821"><img id="173819" class="Default" src="images/Customer_with_DataExt.png" width="645" height="211" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 13-2	<a name="173823">Writing DataExt data to a customer instance: John Sidmark</a></div>
      <p class="T1_002eText1"><a name="173827">As indicated in </a><a href="javascript:WWHClickedPopup('QBSDKProGuide', '11_CustomFields.15.3.html#173823', '');">Figure 13-2</a>, we have already added a data ext definition named “Category” to the object type Customer. Because Customer now has the Category definition, we can write Category data to an individual customer, say, John Sidmark. You do this via the SDK request DataExtAdd or DataExtMod.</p>
      <p class="T1_002eText1"><a name="173828">The SDK knows to write the data to the John Sidmark record because the ObjectRef inside </a>the DataExtAdd specifies “John Sidmark”. The SDK knows <em class="italics">how</em> to write the data because the DataExtAdd tells it to use the definition “Category.”</p>
      <h3 class="H2_002eHeading2"><a name="173829">Enough Pictures: Show Me Some Code</a></h3>
      <p class="TA_002eTextAfterHead"><a href="javascript:WWHClickedPopup('QBSDKProGuide', '11_CustomFields.15.3.html#173835', '');" name="173833">Listing 13-1</a> shows how to define a custom field extension and then write data to it. The sample adds the data ext def “Category” to the Customer object type using the QBFC library in VB. It then uses that definition to write the data “Gold Member” to a customer, John Sidmark. </p>
      <div class="L_002eListing">	Listing 13-1	<a name="173835">Adding a DataExtDef to Customer and using it to write data to a customer (QBFC)</a></div>
      <p class="CvF_002eCodeFull"><a name="173860">‘ Build a 6.0 request set and create a data ext def add request</a><br />Dim DataExt_Set As IMsgSetRequest<br />Set DataExt_Set = SessionManager.CreateMsgSetRequest("US", 6, 0)<br />DataExt_Set.Attributes.OnError = roeContinue<br />Dim MyDataExtDef As IDataExtDefAdd<br />Set MyDataExtDef = DataExt_Set.AppendDataExtDefAddRq</p>
      <p class="CvF_002eCodeFull"><a name="173861">‘ We’re making this a custom field, so use 0 for ownerID, STR255TYPE for the type</a><br />MyDataExtDef.DataExtName.setValue "Category"<br />MyDataExtDef.OwnerID.setValue "0"<br />MyDataExtDef.DataExtType.setValue detSTR255TYPE<br />MyDataExtDef.AssignToObjectList.Add atoCustomer</p>
      <p class="CvF_002eCodeFull"><a name="173862">‘ Write data to the custom field using DataExtMod, as a shortcut</a><br />‘ For private data, our first write to an object’s data ext MUST be a DataExtAdd<br />Dim MyDataExtMod As IDataExtMod<br />Set MyDataExtMod = DataExt_Set.AppendDataExtModRq</p>
      <p class="CvF_002eCodeFull"><a name="173863">MyDataExtMod.DataExtName.setValue "Category"</a><br />MyDataExtMod.DataExtValue.setValue "Gold Member"<br />MyDataExtMod.OwnerID.setValue "0"<br />MyDataExtMod.ORListTxn.ListDataExt.ListDataExtType.setValue ldetCustomer<br />MyDataExtMod.ORListTxn.ListDataExt.ListObjRef.FullName.setValue "John Sidmark"</p>
      <p class="CvF_002eCodeFull"><a name="173864">‘ Now send the request to QB</a><br />Dim MyDataExt_resp As IMsgSetResponse<br />Set MyDataExt_resp = SessionManager.DoRequests(DataExt_Set)</p>
      <p class="CvF_002eCodeFull"><a name="173865">SessionManager.EndSession</a><br />SessionManager.CloseConnection<br />Set SessionManager = Nothing</p>
      <p class="T1_002eText1"><a name="173866">In the sample, notice that we happened to send the data ext def and then write to it within </a>the same message set. If you do this, you need to build the data ext def first in the request set, preceding the DataExtAdd/Mod that uses it. Of course, you aren’t required to do it this way. You could add your data ext definition by iteself and then later use it in your DataExtAdd/Mod when you need to write data. </p>
      <p class="T1_002eText1"><a name="173867">If you want to do the same thing in qbXML, here is how it would look:</a></p>
      <div class="L_002eListing">	Listing 13-2	<a name="173872">qbXML to add a new DataExtDef</a></div>
      <p class="Cv_002eCode"><a name="173873">&lt;?xml version="1.0" ?&gt;</a><br />&lt;?qbxml version="6.0"?&gt;<br />&lt;QBXML&gt;<br />&nbsp;&nbsp;&nbsp;&lt;QBXMLMsgsRq onError = "stopOnError"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtDefAddRq requestID = "0"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtDefAdd&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;OwnerID&gt;0&lt;/OwnerID&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtName&gt;Category&lt;/DataExtName&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtType&gt;STR255TYPE&lt;/DataExtType&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;AssignToObject&gt;Customer&lt;/AssignToObject&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/DataExtDefAdd&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/DataExtDefAddRq&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtModRq requestID = "1"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtMod&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;OwnerID&gt;0&lt;/OwnerID&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtName&gt;Category&lt;/DataExtName&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ListDataExtType&gt;Customer&lt;/ListDataExtType&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ListObjRef&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;&nbsp;&nbsp;&nbsp;FullName&gt;John Sidmark&lt;/FullName&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/ListObjRef&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;DataExtValue&gt;Gold Member&lt;/DataExtValue&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/DataExtMod&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/DataExtModRq&gt;<br />&nbsp;&nbsp;&nbsp;&lt;/QBXMLMsgsRq&gt;<br />&lt;/QBXML&gt;</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>