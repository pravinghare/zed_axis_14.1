﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Types of Error Codes</title>
    <link rel="StyleSheet" href="css/32_MakingRobustApps.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="32_MakingRobustApps.41.1.html#166249">Making Your Application Robust</a> : Types of Error Codes</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="166249">Types of Error Codes</a></h2>
      <p class="T1_002eText1"><a name="166252">Your application needs to deal with two main categories of error codes:</a></p>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="166253">Error codes related to passing messages to and from QuickBooks</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="166254">Message set status code, which is a status code for the message as a whole</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="166255">Response status for each request sent to QuickBooks (status code, status message, and </a>status severity)</div>
      <p class="T1_002eText1"><a name="166256">To help you create an application that integrates successfully with QuickBooks, this chapter </a>provides some pointers on particular result codes and status codes that are especially important to monitor. This material is meant to be a starting point for how your application should handle certain error conditions. Beyond that, of course, are many application-specific concerns that cannot be anticipated here.</p>
      <h3 class="H2_002eHeading2"><a name="166258">Appendix A for Status Code Information</a></h3>
      <p class="T1_002eText1"><a href="javascript:WWHClickedPopup('QBSDKProGuide', 'A_StatusCodes.43.1.html#167165', '');" name="166260">Appendix A</a> contains relevant error/status codes. It lists the status codes, status messages, and status severity, which are all returned as attributes in the response message for every request, and lists the HRESULTs that are returned by the COM API that enables your application to communicate with QuickBooks. The appendix also lists the status codes that are returned in QuickBooks Online Edition responses. (It does not list the HTTPS error codes that may occur with QBOE, since these are standard error codes documented elsewhere.) Finally, the appendix lists the HRESULTs returned by the QBFC COM API, which are very similar to those sent by the qbXML Request Processor API.</p>
      <h3 class="H2_002eHeading2"><a name="166267">Monitoring HRESULTs and HTTP Errors</a></h3>
      <p class="T1_002eText1"><a name="166270">Error codes related to communicating with QuickBooks must be monitored and handled </a>appropriately. The section in this chapter called <a href="javascript:WWHClickedPopup('QBSDKProGuide', '32_MakingRobustApps.41.6.html#166844', '');">“Synchronizing Data between Your Application and Quickbooks”</a> describes a general strategy for implementing an error recovery routine that is invoked each time your application starts up. The error recovery routine checks for possible processing problems in the previous session. It is also invoked whenever certain error codes having to do with processing or communication problems are generated.</p>
      <p class="T1_002eText1"><a name="166274">An example of an HRESULT returned by the qbXML Request Processor is:</a></p>
      <p class="Cv_002eCode"><a name="166275">0x80040416</a><br />If QuickBooks is not running, the company data file name must be supplied to BeginSession.</p>
      <h3 class="H2_002eHeading2"><a name="166276">Monitoring Message Set Status Codes</a></h3>
      <p class="T1_002eText1"><a name="166278">This status code deals with the message set as a whole and is returned in response to a </a>status check or clear status. It is also returned if some specific error recovery operation is invoked and fails, such as a standard check for a valid message set ID.</p>
      <h3 class="H2_002eHeading2"><a name="166279">Monitoring Status Codes</a></h3>
      <p class="T1_002eText1"><a name="166281">Status codes (and their accompanying status message and status severity) are included in </a>the response for each request. This chapter provides special sections describing how your application might deal with status codes related to versioning issues, and with status codes related to problems in synchronizing company data between your application and QuickBooks.</p>
      <p class="T1_002eText1"><a name="168253">An example of a status code is:</a></p>
      <p class="Cv_002eCode"><a name="168254">&lt;CustomerAddRs requestID=”2” </a><span class="bold">status code</span>=”3231” <span class="bold">statusSeverity</span>=”Error”<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bold">statusMessage</span>=”The request has not been processed.”/&gt;</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>