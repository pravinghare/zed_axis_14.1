<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>What is the Workflow? How Do I use a Billing Rate?</title>
    <link rel="StyleSheet" href="css/21_BillingRates.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="21_BillingRates.25.1.html#171682">Using Billing Rates To Bill For Time</a> : What is the Workflow? How Do I use a Billing Rate?</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="171682">What is the Workflow? How Do I use a Billing Rate?</a></h2>
      <p class="T1_002eText1"><a name="171683">The core transaction in the billing rate workflow is the time tracking transaction because </a>that is where the billable time charges are recorded against the customer. From the UI perspective, this transaction is recorded via the Weekly Timesheet form or the Time/Enter Single Activity form in the UI. The easiest way to arrive at these forms is to click on the Enter Time icon in the QuickBooks Home page navigator. (In the SDK, you use the TimeTrackAdd request.) <a href="javascript:WWHClickedPopup('QBSDKProGuide', '21_BillingRates.25.3.html#171764', '');">Figure 23-1</a> shows the Time/Enter SingleActivity form.</p>
      <p class="Anchor"><a name="171749"><img id="171753" class="Default" src="images/SingleActivityTime.PNG" width="545" height="456" style="display: block; float: none; left: 0.0; text-align: left; top: 0.0;" /></a></p>
      <div class="Fg_002eFigure">Figure 23-1	<a name="171764">Entering a time tracking transaction</a></div>
      <p class="T1_002eText1"><a name="171858">The time transaction, not surprisingly, tracks time duration of billable and unbillable </a>activities within the specified timeframe. In the time transaction, you specify the customer, the time duration, the service item that identifies the type of activity performed, the entity that did the work (employee, vendor, or other name), and so forth as shown in the circled items in <a href="javascript:WWHClickedPopup('QBSDKProGuide', '21_BillingRates.25.3.html#171764', '');">Figure 23-1</a>. Notice that you can choose to make the time billable or not.</p>
      <p class="Note"><a name="171862">One scenario where you would not make the time billable </a>would be if you were tracking vendor time but were passing the vendor’s time bills directly to the customer: in this case you wouldn’t want to make the time billable as that would result in double billing.</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>