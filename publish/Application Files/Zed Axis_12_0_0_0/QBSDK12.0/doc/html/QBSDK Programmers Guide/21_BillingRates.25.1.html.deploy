﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Using Billing Rates To Bill For Time</title>
    <link rel="StyleSheet" href="css/21_BillingRates.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">Using Billing Rates To Bill For Time</div>
    <hr align="left" />
    <blockquote>
      <h2 class="ChT_002eChapterTitle"><a name="168978">Using Billing Rates To Bill For Time</a></h2>
      <p class="z-Top_of_Form" style="margin-bottom: 0.0pt; vertical-align: baseline;"><a name="169031">&nbsp;</a></p>
      <p class="T1_002eText1"><a name="171653">Billing rates (called billing rate </a><em class="italics">levels</em> in the UI, but simply billing rates in this document) are used in certain QuickBooks editions to allow you to charge different rates for a service item based on who does the work (employee, or vendor, or other name). This feature supports scenarios such as allowing you to bill at different rates for employees doing the same service but with different experience levels. Another scenario supported is the ability to charge different rates for employees based on the difficulty of a task. </p>
      <p class="T1_002eText1"><a name="171641">This chapter describes the use of billing rates first from the perspective of the QuickBooks </a>UI, to show the workflow that is supported by this feature, and any limitations of the feature when exercised by the SDK requests. The chapter also shows you how to build the BillingRateAdd request in qbXML and in QBFC.</p>
      <h3 class="H2_002eHeading2"><a name="169221">Which QuickBooks Editions Support Billing Rates?</a></h3>
      <p class="T1_002eText1"><a name="171575">The billing rates feature is currently available only in certain flavors of QuickBooks </a>Premier and Enterprise:</p>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="171529">Contractor</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="171535">Professional Services</a></div>
      <div class="B1_002eBullet1">•&nbsp;&nbsp;&nbsp;<a name="171536">Accountant</a></div>
      <h3 class="H2_002eHeading2"><a name="171581">Key SDK Limitations You Need to Know Before You Start</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171585">One key limitation of the SDK’s support of the billing rates feature is that you cannot </a>automate the insertion of billable time charges directly into invoices via QB SDK requests. Only the QuickBooks interactive user can do that because it entails choosing charges from a list of outstanding customer charges at the time of invoice creation, and the company owner (the QuickBooks interactive user) must remain in control of this.</p>
      <p class="T1_002eText1"><a name="171674">Another limitation in the SDK is that you can assign Billing Rates only to employees and </a>vendors, whereas in the UI, you can assign Billing Rates to employees, vendors, and other names.</p>
      <h3 class="H2_002eHeading2"><a name="171612">What Happens If I Use Both Price Levels and Billing Rates?</a></h3>
      <p class="TA_002eTextAfterHead"><a name="171621">It is possible for a customer to have a price level that changes the standard rate assigned to </a>a service item. What happens when you have billable time against that customer using the same service item and you also have a billing rate that applies to that service item? In the case where there is both a customer price level and a billing rate operating on a service item price, the price level always “wins”. Only the price level is used in this case.</p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>