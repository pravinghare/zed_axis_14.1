﻿<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="en_US" lang="en_US" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <!-- MOTW-DISABLED saved from url=(0014)about:internet -->
    <title>Using doquery to Invoke Pre-Set SDK Requests</title>
    <link rel="StyleSheet" href="css/InteractiveMode.css" type="text/css" media="all" />
    <link rel="StyleSheet" href="css/webworks.css" type="text/css" media="all" />
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/context.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/towwhdir.js"></script>
    <script type="text/javascript" language="JavaScript1.2" src="wwhdata/common/wwhpagef.js"></script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        var  WebWorksRootPath = "";
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        // Set reference to top level help frame
        //
        var  WWHFrame = WWHGetWWHFrame("", true);
      // -->
    </script>
    <script type="text/javascript" language="JavaScript1.2" src="scripts/expand.js"></script>
  </head>
  <body style="" onLoad="WWHUpdate();" onUnload="WWHUnload();" onKeyDown="WWHHandleKeyDown((document.all||document.getElementById||document.layers)?event:null);" onKeyPress="WWHHandleKeyPress((document.all||document.getElementById||document.layers)?event:null);" onKeyUp="WWHHandleKeyUp((document.all||document.getElementById||document.layers)?event:null);">
    <br />
    <div class="WebWorks_Breadcrumbs" style="text-align: left;">
      <a class="WebWorks_Breadcrumb_Link" href="InteractiveMode.8.1.html#167718">Interacting Directly with the Web Connector</a> : Using doquery to Invoke Pre-Set SDK Requests</div>
    <hr align="left" />
    <blockquote>
      <h2 class="H1_002eHeading1"><a name="167718">Using doquery to Invoke Pre-Set SDK Requests</a></h2>
      <p class="TA_002eTextAfterHead"><a name="167719">To invoke the pre-set SDK requests in response to user input, you would use this syntax:</a></p>
      <p class="Cv_002eCode"><a name="167720">qbwc://&lt;operation&gt;/&lt;request&gt;?&lt;parameters&gt;</a></p>
      <p class="T1_002eText1"><a name="167721">For &lt;operation&gt;, in the present case, specify doquery, which uses the specified pre-set SDK </a>queries from QuickBooks.</p>
      <p class="T1_002eText1"><a name="167722">The following preset queries are available, all take a single parameter, a sessionID which is </a>the GUID of an existing communication session between your web service and QuickBooks. The intent here is for support of interactive mode support during a session</p>
      <p class="T1_002eText1"><a name="167837">The </a><span style="font-style: italic;">doquery</span> operation offers the following requests, returning all elements of the response XML except in those cases where specific fields are listed:</p>
      <table id="167838" class="Wide" style="margin-bottom: pt; margin-left: pt; margin-right: pt; margin-top: pt; text-align: left; width: 502.7987pt;" cellspacing="0">
        <caption></caption>
        <tr>
          <td id="tc167840" style="background-color: none; border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: bottom; width: 187.5016pt;">
            <div class="TbH_002eTblHd"><a name="167840">Request</a></div>
          </td>
          <td id="tc167842" style="background-color: none; border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: bottom; width: 315.2971pt;">
            <div class="TbH_002eTblHd"><a name="167842">Description</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167844" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167844">AccountQuery </a></div>
          </td>
          <td id="tc167846" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167846">queries for active accounts</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167848" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167848">CustomerBillAddressQuery</a></div>
          </td>
          <td id="tc167850" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167850"> Same as above, but including the BillingAddress for the customer</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167852" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167852">CustomerQuery </a></div>
          </td>
          <td id="tc167854" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167854">Queries for active customers returning the ListID, Name, </a>FullName, Sublevel, Balance and TotalBalance for each customer.</div>
          </td>
        </tr>
        <tr>
          <td id="tc167856" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167856">EmployeeQuery </a></div>
          </td>
          <td id="tc167858" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167858">Queries for active employess returning ListID, Name, PrintAs, </a>Phone, Mobile, Pager, PagerPIN, Email, EmployeeAddress, UseTimeDataToCreatePaychecks, IsUsingTimeDataToCreatePaychecks.</div>
          </td>
        </tr>
        <tr>
          <td id="tc167860" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167860">ItemDiscountQuery</a></div>
          </td>
          <td id="tc167862" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167862"> queries for active discount items </a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167864" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167864">ItemFixedAssetQuery </a></div>
          </td>
          <td id="tc167866" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167866">queries for active fixed asset items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167868" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167868">ItemGroupQuery </a></div>
          </td>
          <td id="tc167870" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167870">queries for group items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167872" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167872">ItemInventoryAssemblyQuery </a></div>
          </td>
          <td id="tc167874" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167874">queries for active assembly items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167876" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167876">ItemInventoryQuery </a></div>
          </td>
          <td id="tc167878" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167878">queries for active inventory items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167880" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167880">ItemNonInventoryQuery</a></div>
          </td>
          <td id="tc167882" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167882"> queries for active non-inventory items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167884" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167884">ItemOtherChargeQuery</a></div>
          </td>
          <td id="tc167886" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167886"> queries for active other charge items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167888" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167888">ItemPaymentQuery </a></div>
          </td>
          <td id="tc167890" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167890">queries for active payment items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167892" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167892">ItemQuery </a></div>
          </td>
          <td id="tc167894" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167894">queries for active items, returning ListID, Name, and FullName.</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167896" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167896">ItemSalesTaxGroupQuery</a></div>
          </td>
          <td id="tc167898" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167898"> queries for active sales tax group items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167900" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167900">ItemSalesTaxQuery</a></div>
          </td>
          <td id="tc167902" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167902"> queries for active sales tax items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167904" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167904">ItemServiceQuery </a></div>
          </td>
          <td id="tc167906" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167906">queries for active service items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167908" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167908">ItemSubtotalQuery</a></div>
          </td>
          <td id="tc167910" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167910"> queries for active subtotal items</a></div>
          </td>
        </tr>
        <tr>
          <td id="tc167912" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167912">ListDisplayAdd</a></div>
          </td>
          <td id="tc167914" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167914"> executes a list display add request for the list type specified in </a>the type query parameter</div>
          </td>
        </tr>
        <tr>
          <td id="tc167916" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167916">ListDisplayMod </a></div>
          </td>
          <td id="tc167918" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167918">executes a list display mod request for the list type specified in </a>the type query parameter, showing the list item provided in the ListID query parameter.</div>
          </td>
        </tr>
        <tr>
          <td id="tc167920" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167920">TxnDisplayAdd</a></div>
          </td>
          <td id="tc167922" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167922"> a transaction display add request for the transaction type </a>specified in the type query parameter</div>
          </td>
        </tr>
        <tr>
          <td id="tc167924" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167924">TxnDisplayMod</a></div>
          </td>
          <td id="tc167926" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><span style="font-size: 12.0pt;"><a name="167926"> </a></span>executes a Transaction display mod request for the transaction type specified in the type query parameter, showing the list item provided in the TxnID query parameter.</div>
          </td>
        </tr>
        <tr>
          <td id="tc167928" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167928">VendorAddressQuery </a></div>
          </td>
          <td id="tc167930" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167930">Queries for active vendors, returning ListID, Name, CreditLimit, </a>Balance, and VendorAddress/VendorAddressBlock.</div>
          </td>
        </tr>
        <tr>
          <td id="tc167932" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167932">VendorQuery</a></div>
          </td>
          <td id="tc167934" style="border-bottom-color: #000000; border-bottom-style: solid; border-bottom-width: 1px; border-left-color: #000000; border-left-style: solid; border-left-width: 1px; border-right-color: #000000; border-right-style: solid; border-right-width: 1px; border-top-color: #000000; border-top-style: solid; border-top-width: 1px; padding-bottom: pt; padding-left: pt; padding-right: pt; padding-top: pt; vertical-align: top;">
            <div class="TbT_002eTblText"><a name="167934">Queries for active vendors, returning ListID, Name, CreditLimit, </a>Balance, Phone, Fax , Email, and Contact fields for each.</div>
          </td>
        </tr>
      </table>
      <p class="TA_002eTextAfterHead"><a name="167935">&nbsp;</a></p>
      <p class="T1_002eText1"><a name="167623">&nbsp;</a></p>
      <script type="text/javascript" language="JavaScript1.2">
        <!--
          // Clear related topics
          //
          WWHClearRelatedTopics();

          document.writeln(WWHRelatedTopicsInlineHTML());
        // -->
      </script>
    </blockquote>
    <script type="text/javascript" language="JavaScript1.2">
      <!--
        document.write(WWHRelatedTopicsDivTag() + WWHPopupDivTag() + WWHALinksDivTag());
      // -->
    </script>
  </body>
</html>