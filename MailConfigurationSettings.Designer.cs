namespace MailConfigurations
{
    partial class MailConfigurationSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            //m_MailConfigurationSettings = null;
            base.Dispose(disposing);
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailConfigurationSettings));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxEmailAddress = new System.Windows.Forms.TextBox();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxOutServer = new System.Windows.Forms.TextBox();
            this.textBoxInMailServer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxInPortNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOutPortNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMailProtocol = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBoxUseSSL = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.checkBoxRememberPassword = new System.Windows.Forms.CheckBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonTestConnection = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBoxAdvancedSettings = new System.Windows.Forms.GroupBox();
            this.labelMin = new System.Windows.Forms.Label();
            this.textBoxSendReceive = new System.Windows.Forms.TextBox();
            this.checkBoxInboundMessages = new System.Windows.Forms.CheckBox();
            this.checkBoxOutboundMessages = new System.Windows.Forms.CheckBox();
            this.checkBoxSendReceive = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxAdvancedSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.Transparent;
            this.buttonCancel.Location = new System.Drawing.Point(399, 537);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 25);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxEmailAddress
            // 
            this.textBoxEmailAddress.Location = new System.Drawing.Point(246, 54);
            this.textBoxEmailAddress.MaxLength = 100;
            this.textBoxEmailAddress.Name = "textBoxEmailAddress";
            this.textBoxEmailAddress.Size = new System.Drawing.Size(209, 21);
            this.textBoxEmailAddress.TabIndex = 6;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(246, 23);
            this.textBoxUserName.MaxLength = 100;
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(209, 21);
            this.textBoxUserName.TabIndex = 5;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(246, 85);
            this.textBoxPassword.MaxLength = 100;
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(209, 21);
            this.textBoxPassword.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 8F);
            this.label5.Location = new System.Drawing.Point(171, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Password :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 8F);
            this.label6.Location = new System.Drawing.Point(163, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "User name :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 8F);
            this.label7.Location = new System.Drawing.Point(144, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Email Address :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 8F);
            this.label8.Location = new System.Drawing.Point(106, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Outgoing mail server :";
            // 
            // textBoxOutServer
            // 
            this.textBoxOutServer.Location = new System.Drawing.Point(246, 89);
            this.textBoxOutServer.MaxLength = 100;
            this.textBoxOutServer.Name = "textBoxOutServer";
            this.textBoxOutServer.Size = new System.Drawing.Size(209, 21);
            this.textBoxOutServer.TabIndex = 2;
            // 
            // textBoxInMailServer
            // 
            this.textBoxInMailServer.Location = new System.Drawing.Point(246, 59);
            this.textBoxInMailServer.MaxLength = 100;
            this.textBoxInMailServer.Name = "textBoxInMailServer";
            this.textBoxInMailServer.Size = new System.Drawing.Size(209, 21);
            this.textBoxInMailServer.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8F);
            this.label4.Location = new System.Drawing.Point(104, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Incoming mail server :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8F);
            this.label3.Location = new System.Drawing.Point(48, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Incoming port no (Default 110) :";
            // 
            // textBoxInPortNo
            // 
            this.textBoxInPortNo.Location = new System.Drawing.Point(246, 119);
            this.textBoxInPortNo.MaxLength = 4;
            this.textBoxInPortNo.Name = "textBoxInPortNo";
            this.textBoxInPortNo.Size = new System.Drawing.Size(50, 21);
            this.textBoxInPortNo.TabIndex = 3;
            this.textBoxInPortNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxInPortNo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8F);
            this.label2.Location = new System.Drawing.Point(57, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Outgoing port no (Default 25) :";
            // 
            // textBoxOutPortNo
            // 
            this.textBoxOutPortNo.Location = new System.Drawing.Point(246, 150);
            this.textBoxOutPortNo.MaxLength = 4;
            this.textBoxOutPortNo.Name = "textBoxOutPortNo";
            this.textBoxOutPortNo.Size = new System.Drawing.Size(50, 21);
            this.textBoxOutPortNo.TabIndex = 4;
            this.textBoxOutPortNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxOutPortNo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8F);
            this.label1.Location = new System.Drawing.Point(154, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Mail protocol :";
            // 
            // comboBoxMailProtocol
            // 
            this.comboBoxMailProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMailProtocol.FormattingEnabled = true;
            this.comboBoxMailProtocol.Location = new System.Drawing.Point(246, 30);
            this.comboBoxMailProtocol.Name = "comboBoxMailProtocol";
            this.comboBoxMailProtocol.Size = new System.Drawing.Size(209, 21);
            this.comboBoxMailProtocol.TabIndex = 0;
            this.comboBoxMailProtocol.SelectedIndexChanged += new System.EventHandler(this.comboBoxMailProtocol_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.checkBoxUseSSL);
            this.groupBox1.Controls.Add(this.comboBoxMailProtocol);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxOutPortNo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxInPortNo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxInMailServer);
            this.groupBox1.Controls.Add(this.textBoxOutServer);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(14, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(485, 213);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mail Server Settings";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(322, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(322, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(465, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(465, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(465, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "*";
            // 
            // checkBoxUseSSL
            // 
            this.checkBoxUseSSL.AutoSize = true;
            this.checkBoxUseSSL.Location = new System.Drawing.Point(246, 181);
            this.checkBoxUseSSL.Name = "checkBoxUseSSL";
            this.checkBoxUseSSL.Size = new System.Drawing.Size(73, 17);
            this.checkBoxUseSSL.TabIndex = 12;
            this.checkBoxUseSSL.Text = "Use SSL";
            this.checkBoxUseSSL.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.checkBoxRememberPassword);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBoxEmailAddress);
            this.groupBox2.Controls.Add(this.textBoxUserName);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBoxPassword);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(14, 258);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(485, 150);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mail Account Information";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(461, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(461, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "*";
            // 
            // checkBoxRememberPassword
            // 
            this.checkBoxRememberPassword.AutoSize = true;
            this.checkBoxRememberPassword.Location = new System.Drawing.Point(246, 114);
            this.checkBoxRememberPassword.Name = "checkBoxRememberPassword";
            this.checkBoxRememberPassword.Size = new System.Drawing.Size(147, 17);
            this.checkBoxRememberPassword.TabIndex = 8;
            this.checkBoxRememberPassword.Text = "Remember password";
            this.checkBoxRememberPassword.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.BackColor = System.Drawing.Color.Transparent;
            this.buttonSave.Location = new System.Drawing.Point(293, 537);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(100, 25);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "&Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonTestConnection
            // 
            this.buttonTestConnection.BackColor = System.Drawing.Color.Transparent;
            this.buttonTestConnection.Location = new System.Drawing.Point(90, 537);
            this.buttonTestConnection.Name = "buttonTestConnection";
            this.buttonTestConnection.Size = new System.Drawing.Size(135, 25);
            this.buttonTestConnection.TabIndex = 9;
            this.buttonTestConnection.Text = "&Test connection...";
            this.buttonTestConnection.UseVisualStyleBackColor = false;
            this.buttonTestConnection.Click += new System.EventHandler(this.buttonTestConnection_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(344, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Enter email details to send and receive business messages";
            // 
            // groupBoxAdvancedSettings
            // 
            this.groupBoxAdvancedSettings.Controls.Add(this.labelMin);
            this.groupBoxAdvancedSettings.Controls.Add(this.textBoxSendReceive);
            this.groupBoxAdvancedSettings.Controls.Add(this.checkBoxInboundMessages);
            this.groupBoxAdvancedSettings.Controls.Add(this.checkBoxOutboundMessages);
            this.groupBoxAdvancedSettings.Controls.Add(this.checkBoxSendReceive);
            this.groupBoxAdvancedSettings.Location = new System.Drawing.Point(14, 414);
            this.groupBoxAdvancedSettings.Name = "groupBoxAdvancedSettings";
            this.groupBoxAdvancedSettings.Size = new System.Drawing.Size(485, 113);
            this.groupBoxAdvancedSettings.TabIndex = 18;
            this.groupBoxAdvancedSettings.TabStop = false;
            this.groupBoxAdvancedSettings.Text = "Advanced Settings";
            // 
            // labelMin
            // 
            this.labelMin.AutoSize = true;
            this.labelMin.Location = new System.Drawing.Point(303, 30);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(50, 13);
            this.labelMin.TabIndex = 22;
            this.labelMin.Text = "(in Min)";
            // 
            // textBoxSendReceive
            // 
            this.textBoxSendReceive.Location = new System.Drawing.Point(246, 27);
            this.textBoxSendReceive.MaxLength = 3;
            this.textBoxSendReceive.Name = "textBoxSendReceive";
            this.textBoxSendReceive.Size = new System.Drawing.Size(50, 21);
            this.textBoxSendReceive.TabIndex = 21;
            this.textBoxSendReceive.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSendReceive_KeyPress);
            // 
            // checkBoxInboundMessages
            // 
            this.checkBoxInboundMessages.AutoSize = true;
            this.checkBoxInboundMessages.Location = new System.Drawing.Point(71, 89);
            this.checkBoxInboundMessages.Name = "checkBoxInboundMessages";
            this.checkBoxInboundMessages.Size = new System.Drawing.Size(191, 17);
            this.checkBoxInboundMessages.TabIndex = 20;
            this.checkBoxInboundMessages.Text = "Receiving Inbound Messages";
            this.checkBoxInboundMessages.UseVisualStyleBackColor = true;
            // 
            // checkBoxOutboundMessages
            // 
            this.checkBoxOutboundMessages.AutoSize = true;
            this.checkBoxOutboundMessages.Location = new System.Drawing.Point(71, 60);
            this.checkBoxOutboundMessages.Name = "checkBoxOutboundMessages";
            this.checkBoxOutboundMessages.Size = new System.Drawing.Size(190, 17);
            this.checkBoxOutboundMessages.TabIndex = 19;
            this.checkBoxOutboundMessages.Text = "Sending Outbound Messages";
            this.checkBoxOutboundMessages.UseVisualStyleBackColor = true;
            // 
            // checkBoxSendReceive
            // 
            this.checkBoxSendReceive.AutoSize = true;
            this.checkBoxSendReceive.Location = new System.Drawing.Point(71, 30);
            this.checkBoxSendReceive.Name = "checkBoxSendReceive";
            this.checkBoxSendReceive.Size = new System.Drawing.Size(166, 17);
            this.checkBoxSendReceive.TabIndex = 18;
            this.checkBoxSendReceive.Text = "Automatic Send/Receive";
            this.checkBoxSendReceive.UseVisualStyleBackColor = true;
            this.checkBoxSendReceive.CheckedChanged += new System.EventHandler(this.checkBoxSendReceive_CheckedChanged);
            // 
            // MailConfigurationSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(513, 566);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.buttonTestConnection);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBoxAdvancedSettings);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MailConfigurationSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mail Configuration Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxAdvancedSettings.ResumeLayout(false);
            this.groupBoxAdvancedSettings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxEmailAddress;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxOutServer;
        private System.Windows.Forms.TextBox textBoxInMailServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxInPortNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOutPortNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMailProtocol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox checkBoxRememberPassword;
        private System.Windows.Forms.Button buttonTestConnection;
        private System.Windows.Forms.CheckBox checkBoxUseSSL;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBoxAdvancedSettings;
        private System.Windows.Forms.CheckBox checkBoxInboundMessages;
        private System.Windows.Forms.CheckBox checkBoxOutboundMessages;
        private System.Windows.Forms.CheckBox checkBoxSendReceive;
        private System.Windows.Forms.TextBox textBoxSendReceive;
        private System.Windows.Forms.Label labelMin;

    }
}