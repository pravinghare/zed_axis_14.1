﻿namespace TransactionImporter
{
    partial class Splashscreenforstatementpreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splashscreenforstatementpreview));
            this.buttonCancelSplashScreen = new System.Windows.Forms.Button();
            this.progressBarStatusBar = new System.Windows.Forms.ProgressBar();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.labelMeessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCancelSplashScreen
            // 
            this.buttonCancelSplashScreen.Location = new System.Drawing.Point(211, 92);
            this.buttonCancelSplashScreen.Name = "buttonCancelSplashScreen";
            this.buttonCancelSplashScreen.Size = new System.Drawing.Size(77, 20);
            this.buttonCancelSplashScreen.TabIndex = 6;
            this.buttonCancelSplashScreen.Text = "Cancel";
            this.buttonCancelSplashScreen.UseVisualStyleBackColor = true;
            this.buttonCancelSplashScreen.Visible = false;
            this.buttonCancelSplashScreen.Click += new System.EventHandler(this.buttonCancelSplashScreen_Click_1);
            // 
            // progressBarStatusBar
            // 
            this.progressBarStatusBar.Location = new System.Drawing.Point(15, 116);
            this.progressBarStatusBar.Name = "progressBarStatusBar";
            this.progressBarStatusBar.Size = new System.Drawing.Size(273, 23);
            this.progressBarStatusBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarStatusBar.TabIndex = 5;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 50;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // labelMeessage
            // 
            this.labelMeessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeessage.Location = new System.Drawing.Point(12, 41);
            this.labelMeessage.Name = "labelMeessage";
            this.labelMeessage.Size = new System.Drawing.Size(273, 26);
            this.labelMeessage.TabIndex = 7;
            this.labelMeessage.Click += new System.EventHandler(this.labelMeessage_Click);
            // 
            // Splashscreenforstatementpreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(305, 158);
            this.Controls.Add(this.labelMeessage);
            this.Controls.Add(this.buttonCancelSplashScreen);
            this.Controls.Add(this.progressBarStatusBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Splashscreenforstatementpreview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Opening statement";
            this.Load += new System.EventHandler(this.Splashscreenforstatementpreview_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancelSplashScreen;
        private System.Windows.Forms.ProgressBar progressBarStatusBar;
        private System.Windows.Forms.Timer timerStatus;
        public System.Windows.Forms.Label labelMeessage;
    }
}