using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public partial class Messages : Form
    {
        private static Messages m_Mess;

        public static Messages getInstance()
        {
            if (m_Mess == null)
                m_Mess = new Messages();
            return m_Mess;
        }

        
        public Messages()
        {
            InitializeComponent();
        }

        public void SetDialogBox(string message, MessageBoxIcon icon)
        {
         
            this.growLabelMessags.Text = message;
            
            this.pictureBox1.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
            /*switch (icon)
            {
                case MessageBoxIcon.Error:
                    this.pcbErrorICON.Image = System.Drawing.SystemIcons.Error.ToBitmap();
                    this.btnok.Visible = true;
                    break;
                case MessageBoxIcon.Information:
                    this.pcbErrorICON.Image = System.Drawing.SystemIcons.Information.ToBitmap();
                    this.btnok.Visible = true;
                    break;
                case MessageBoxIcon.Warning:
                    this.pcbErrorICON.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
                    this.btnok.Visible = true;
                    break;

                case MessageBoxIcon.Question:
                    this.pcbErrorICON.Image = System.Drawing.SystemIcons.Question.ToBitmap();
                    this.btnNo.Visible = true;
                    this.btnYes.Visible = true;
                    break;

                default:
                    this.pcbErrorICON.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
                    this.btnok.Visible = true;
                    break;

            }*/
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
           
            if (buttonCancel.Text == "Add")
            {
                this.DialogResult = DialogResult.OK;
            }
            if (buttonCancel.Text == "Help")
            {
                System.Diagnostics.Process.Start(Constants.Explorer, Constants.RegHelpLink);
            }
            this.Close();
        }

        private void buttonIgnore_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        private void buttonIgnoreAll_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void buttonQuit_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.No;
                TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
                BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                this.Close();
            }
            catch
            {
                this.Close();
            }
        }


    }
}