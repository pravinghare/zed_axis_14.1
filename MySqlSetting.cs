using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDI.Constant;
using System.Data.Odbc;
using OB.ApplicationBlocks.Data;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.Diagnostics;
using System.Reflection;

namespace TransactionImporter
{
    // ===============================================================================
    // 
    // MySqlSetting.cs
    //
    // This file provide the setting for MySqlDatabase.
    //
    // Developed By : K.Gouraw, Sandeep Patil.
    // Date : 10-3-2009
    // ==============================================================================


    public partial class MySqlSetting : Form
    {
       
        /// <summary>
        /// Used to store MySql Script path.
        /// </summary>
        private readonly string m_MySQLScriptPath;
        /// <summary>
        /// Used to store connection string of MySql server.
        /// </summary>
		// Axis 590
        public string m_ConnectionString;

        /// <summary>
        /// Used to store mysql server password.
        /// </summary>
        private string m_mysqlPassword = string.Empty;

        /// <summary>
        /// Flag used to close the form.
        /// </summary>
		// Axis 590
        public  bool m_IsClose;

        /// <summary>
        /// Registry Path for odbc driver.
        /// </summary>
        const string ODBC_DRIVER_KEY = @"HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers";

       
        public MySqlSetting()
        {         
            m_MySQLScriptPath= Application.StartupPath + Constants.MySQLScriptFile;               
            InitializeComponent();
            //Getting the connection string from app.config file.
            //m_ConnectionString = DataProcessingBlocks.CommonUtilities.GetAppSettingValue("MySQLConnectionString");
            m_ConnectionString = TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
            if (m_ConnectionString == string.Empty)
                m_ConnectionString = "Driver={MySQL ODBC 3.51 Driver};Server=localhost;Database=edi;User=root;Password=root;";
            m_mysqlPassword = GetMySqlPassword(m_ConnectionString);

           
            IsSchemaEquals(m_ConnectionString); 
            
            //textBoxMySqlDBPass.Text = m_mysqlPassword;
        }

        //Getting the password of mysql database.
        private string GetMySqlPassword(string connectionString)
        {
            string[] password = connectionString.Split(';');
            if (connectionString.Contains("Password"))
            {
                string[] pass = password[4].Split('=');
                string result = pass[1].ToString();
                return result;
            }
            else
            {
                return string.Empty;
            }
        }                      
        private void MySqlSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_IsClose)
            {
                e.Cancel = false;
                //TransactionImporter.IsCloseAppl = false;
               
            }
            else
            {
                e.Cancel = true;
            }         
        }

        private void buttonMySQLDBOk_Click(object sender, EventArgs e)
        {
            m_IsClose = false;
            if (string.IsNullOrEmpty(textBoxMySqlPath.Text.Trim()) || !File.Exists(textBoxMySqlPath.Text.Trim()))
            {
                MessageBox.Show("Please Provide the path for mysql.exe.\nIf MySql Server is not installed in your local machine, Please click on the download link over the form.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            /*if (string.IsNullOrEmpty(textBoxMySQLDBName.Text.Trim()))
            {
                MessageBox.Show("Please enter the DataBase name.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxMySQLDBName.Focus();
                return;
            }


            if (string.IsNullOrEmpty(textBoxMySqlDBPass.Text.Trim()))
            {
                MessageBox.Show("Please enter the password.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxMySqlDBPass.Focus();
                return;
            }
            if (!Regex.IsMatch(textBoxMySQLDBName.Text.Trim(), Constants.AlphNumeric_Pattern))
            {
                MessageBox.Show("Please enter only alphanumeric value.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxMySQLDBName.Focus();
                return;
            }*/
            //if (textBoxMySqlDBPass.Text.Trim().Length < 5)
            //{
            //   MessageBox.Show("Password should be minimum 6 characters.", Constants.productName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    textBoxMySqlDBPass.Focus();
            //    return;
            //}
			// Axis 590
            if (string.IsNullOrEmpty(m_ConnectionString))
            {
                MessageBox.Show("Please check the app.config file. It returns null value", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return ;
            }
            if (check_ODBCDriver().Equals("Does not exist") || check_ODBCDriver() == null)
            {
                MessageBox.Show("To use the Automate feature you need to install ODBC driver. Click OK to continue.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                InstallODBCDriver();
                return ;
            }
            if (!this.ExecuteSqlScript())
            {
                return ;
            }
            if (!appendInMyINI())
            {
                return ;

            }
            UpdateMySQLConnectionString();
            
                m_IsClose = true;
                TransactionImporter.IsCloseAppl = false;
                MessageBox.Show("MySQL Server setting is configured successfully.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DBConnection.MySqlDataAcess.StoreEDIRegistry(true);
            
        }

       
         // Axis 590
        public bool CheckMySqlSetting()
        {

            if (string.IsNullOrEmpty(m_ConnectionString))
            {
                MessageBox.Show("Please check the app.config file. It returns null value", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            if (check_ODBCDriver().Equals("Does not exist") || check_ODBCDriver() == null)
            {
                MessageBox.Show("To use the Automate feature you need to install a MySQL database. Click OK to continue.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                InstallODBCDriver();
                return false;
            }
            return true;

        }

        /// <summary>
        /// Executing the mysql script file
        /// </summary>
        /// <returns></returns>
        public bool ExecuteSqlScript()
        {
            //Updating the connection string
           string[] arrString = m_ConnectionString.Split(';');
           arrString[2] = string.Empty;
           //arrString[4] = "Password=" + textBoxMySqlDBPass.Text.Trim();
           //arrString[4] = "Password=root";
           //arrString[4] = "Password=" + m_mysqlPassword;
           arrString[4] = "Password=root";
           string connectionString=string.Empty;
           
           foreach (string str in arrString)
           {
             if(!string.IsNullOrEmpty(str))  
                connectionString += str + ";";
           }

           //string DBName = textBoxMySQLDBName.Text.Trim();
           string DBName = "EDI";
            OdbcConnection connection=null;
            bool dbFlag = false;
            bool isEqual=false;
            try
            {
                connection = new OdbcConnection(connectionString);
                connection.Open();
            }
            catch 
            {     
                connection.Close();
                //textBoxMySqlDBPass.Text = string.Empty;
                //textBoxMySqlDBPass.Focus();
                m_IsClose = false;
                MessageBox.Show("Please check your MySql password.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            try
            {
                OdbcDataReader dbReader = OdbcHelper.ExecuteReader(connection, CommandType.Text, "SHOW DATABASES");
                while (dbReader.Read())
                {
                    if (dbReader.GetString(0).Equals(DBName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        dbFlag = true;
                        break;
                    }
                }
                dbReader.Close();

                if (!dbFlag)
                {
                    OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "DROP DATABASE IF EXISTS " + DBName);
                    OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "CREATE DATABASE " + DBName);
                    OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "USE " + DBName);
                    StreamReader reader = new StreamReader(m_MySQLScriptPath);
                    string stringQuery = reader.ReadToEnd().Replace("\r", string.Empty).Replace("\n", string.Empty);
                    string[] arrQuery = stringQuery.Split(';');
                    for (int i = 0; i < arrQuery.Length - 1; i++)
                    {
                        string sqlcmd = arrQuery[i];
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, sqlcmd);
                    }
                    connection.Close();
                    return true;
                }
                else
                {
                    connection.Close();
                    string[] arrConnString = connectionString.Split(';');
                    arrString[2] = "Database=" + DBName;
                    string connString = string.Empty;
                    foreach (string str in arrString)
                    {
                        if (!string.IsNullOrEmpty(str))
                            connString += str + ";";
                    }
                    isEqual = IsSchemaEquals(connString);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                m_IsClose = false;
                return false;
            }
            if (!isEqual)
            {
                connection.Close();
                string[] arrConnString = connectionString.Split(';');
                arrString[2] = "Database=" + DBName;
                string connString = string.Empty;
                foreach (string str in arrString)
                {
                    if (!string.IsNullOrEmpty(str))
                        connString += str + ";";
                }
                //textBoxMySQLDBName.Text = string.Empty;
                //textBoxMySQLDBName.Focus();
                DialogResult dlgResult = MessageBox.Show("This database is already exist, but the schema is not matched with that of EDI.\nApplication will override this database with new database schema ,your existing data will not be removed.If you Click Yes then It will override your database.", Constants.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dlgResult.ToString() == DialogResult.Yes.ToString())
                {
                    connection = new OdbcConnection(connString);
                    StreamReader reader = new StreamReader(m_MySQLScriptPath);
                    string stringQuery = reader.ReadToEnd().Replace("\r", string.Empty).Replace("\n", string.Empty);
                    string[] arrQuery = stringQuery.Split(';');
                    for (int i = 0; i < arrQuery.Length - 1; i++)
                    {
                        string sqlcmd = arrQuery[i];
                        try
                        {
                           OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, sqlcmd);
                        }
                        catch
                        {
                            //MessageBox.Show(ex.Message.ToString());
                        }
                        
                    }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "alter table mail_attachment modify column AttachName varchar(250)");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "alter table export_filter modify column FilterDataType varchar(30)");
                    }
                    catch
                    { }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "alter table trading_partner_schema add column UserName VARCHAR(50) , add column Password VARCHAR(50) , ADD COLUMN eBayAuthToken varchar(5000),ADD COLUMN eBayDevId varchar(50),ADD COLUMN eBayCertId varchar(50),ADD COLUMN eBayAppId varchar(50), add column eBayItemMapping varchar(50), add column OSCWebStoreUrl varchar(50),add column OSCDatabase varchar(20),add column AccountNumber varchar(50),add column CompanyName varchar(100),add column LastUpdatedDate datetime ,add column eBayItem varchar(30),add column eBayDiscountItem varchar(30)");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "alter table message_schema add column MailProtocolId Smallint(5),ADD COLUMN TradingPartnerID Bigint(20)");
                    }
                    catch
                    { }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "insert into mail_protocol_schema(MailProtocolId,MailProtocol) values (4,'eBay'),(5,'OSCommerce'),(6,'Allied Express')");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "alter table trading_partner_schema modify column eBayItemMapping varchar(50)");
                        
                    }
                    catch
                    {
 
                    }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "insert into message_status_schema(StatusID,Name,Descs) values (7,'Failed','Message is failed'),(8,'Dispatched','Message is Dispatched')");
                    }
                    catch
                    {

                    }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "insert into message_status_schema(StatusID,Name,Descs) values (10,'Pending','Message received but not processed')");
                    }
                    catch
                    {

                    }
                    try
                    {
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE mail_setup_schema ADD COLUMN AutomaticFlag tinyint(1), add column OutboundFlag tinyint(1), add column InboundFlag tinyint(1), add column ScheduleInMin int(3)");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE export_filter MODIFY COLUMN FilterID int(11) unsigned NOT NULL auto_increment,MODIFY COLUMN FilterDataType varchar(30) default NULL, MODIFY COLUMN FilterDate datetime default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE mail_attachment MODIFY COLUMN AttachID int(11) unsigned NOT NULL auto_increment,MODIFY  COLUMN  AttachName varchar(250) default NULL,MODIFY COLUMN AttachFile longblob default NULL,MODIFY COLUMN MessageId int(11) default NULL,MODIFY COLUMN Status varchar(25) default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE mail_protocol_schema MODIFY COLUMN MailProtocolID smallint(5) unsigned NOT NULL auto_increment,MODIFY COLUMN MailProtocol varchar(50) default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE mail_setup_schema MODIFY COLUMN MailSetupID bigint(50) unsigned NOT NULL auto_increment,MODIFY COLUMN MailProtocolID smallint(5) unsigned NOT NULL,MODIFY COLUMN InPortNo smallint(5) unsigned default NULL,MODIFY COLUMN OutPortNo smallint(5) unsigned default NULL,MODIFY COLUMN IsSSL tinyint(1) default NULL,MODIFY COLUMN IncommingServer varchar(100) default NULL,MODIFY COLUMN OutgoingServer varchar(100) default NULL,MODIFY COLUMN EmailID varchar(100) default NULL,MODIFY COLUMN UserName varchar(100) default NULL,MODIFY COLUMN  Password varchar(100) default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE message_monitor MODIFY COLUMN monitorid int(11) NOT NULL auto_increment,MODIFY COLUMN Name varchar(40) NOT NULL,MODIFY COLUMN Folder varchar(255) NOT NULL,MODIFY COLUMN Period varchar(10) NOT NULL,MODIFY COLUMN IsActive smallint(6) NOT NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE message_rule MODIFY COLUMN RulesId int(11) NOT NULL auto_increment,MODIFY COLUMN Name varchar(50) NOT NULL,MODIFY COLUMN TradingPartnerID varchar(50) NOT NULL,MODIFY COLUMN CondSubEquals char(50) default NULL,MODIFY COLUMN CondSubMatches char(50) default NULL,MODIFY COLUMN  CondFileEquals char(50) default NULL,MODIFY COLUMN  CondFileMatches char(50) default NULL,MODIFY COLUMN  ActTransType char(50) NOT NULL,MODIFY COLUMN ActUsingMappingId char(50) NOT NULL,MODIFY COLUMN OnCompleteStatusID char(50) default NULL,MODIFY COLUMN   OnCompleteErrStatusID char(50) default NULL,MODIFY COLUMN Status varchar(10) default NULL,MODIFY COLUMN  SoundChime tinyint(1) default NULL,MODIFY COLUMN  SoundAlarm tinyint(1) default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE message_schema MODIFY COLUMN MessageID int(11) unsigned NOT NULL auto_increment,MODIFY COLUMN   MessageDate datetime NOT NULL,MODIFY COLUMN ArchiveDate datetime default NULL,MODIFY COLUMN FromAddress varchar(100) default NULL,MODIFY COLUMN ToAddress varchar(400) default NULL,MODIFY COLUMN RefMessageID varchar(10) default NULL,MODIFY COLUMN Subject varchar(255) default NULL,MODIFY COLUMN  Body varchar(2000) default NULL,MODIFY COLUMN Status varchar(25) default NULL,MODIFY COLUMN Cc varchar(400) default NULL,MODIFY COLUMN ReadFlag tinyint(1) default NULL,MODIFY COLUMN MailProtocolId smallint(5)  NULL,MODIFY COLUMN TradingPartnerID bigint(20)  NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE message_status_schema MODIFY COLUMN StatusID smallint(5) unsigned NOT NULL auto_increment,MODIFY COLUMN  Name varchar(50) default NULL,MODIFY COLUMN  Descs varchar(100) default NULL");
                        OdbcHelper.ExecuteNonQuery(connection, CommandType.Text, "ALTER TABLE trading_partner_schema MODIFY COLUMN  TradingPartnerID bigint(20) unsigned NOT NULL auto_increment,MODIFY COLUMN  TPName varchar(100) default NULL,MODIFY COLUMN  MailProtocolID smallint(5) unsigned NOT NULL,MODIFY COLUMN  EmailID varchar(100) default NULL,MODIFY COLUMN  AlternateEmailID varchar(100) default NULL,MODIFY COLUMN  ClientID varchar(100) default NULL,MODIFY COLUMN XSDLocation varchar(100) default NULL,MODIFY COLUMN  UserName varchar(50) default NULL,MODIFY COLUMN Password varchar(50) default NULL,MODIFY COLUMN  eBayAuthToken varchar(5000) default NULL,MODIFY COLUMN eBayItemMapping varchar(50) default NULL,MODIFY COLUMN  eBayDevId varchar(50) default NULL,MODIFY COLUMN eBayCertID varchar(50) default NULL,MODIFY COLUMN  eBayAppId varchar(50) default NULL,MODIFY COLUMN  OSCWebStoreUrl varchar(50) default NULL,MODIFY COLUMN OSCDatabase varchar(20) default NULL,MODIFY COLUMN AccountNumber varchar(50) default NULL,MODIFY COLUMN  CompanyName varchar(100) default NULL,MODIFY COLUMN LastUpdatedDate datetime  NULL,MODIFY COLUMN  eBayItem varchar(31)  NULL,MODIFY COLUMN  eBayDiscountItem varchar(31)  NULL");
                    }
                    catch
                    {

                    }

                    connection.Close();
                    return true;
                }
                else
                    m_IsClose = false;
                return false;
            }
            else
            {              
                return true;
            }
               
         }

        private bool IsSchemaEquals(string connString)
        {
            string sqlScriptText = string.Empty;
            string sqlScript = string.Empty;
            string strTable = string.Empty;
            string strColumnName = string.Empty;
            string strColumnType = string.Empty;
            string strColumnNull = string.Empty;
            string strColumnKey = string.Empty;
            string strColumnDflt = string.Empty;
            string strColumnExtr = string.Empty;
            try
            {
                OdbcConnection connection = new OdbcConnection(connString);
                connection.Open();
                OdbcDataReader tableReader = OdbcHelper.ExecuteReader(connection, CommandType.Text, "SHOW TABLES");
                while (tableReader.Read())
                {
                    strTable = tableReader.GetString(0);
                    //Removed below line for preventing clearing and deleting table.
                    //sqlScriptText += "DROP TABLE IF EXISTS " + strTable + ";";
                    //sqlScriptText += "\r\n\r\n";
                    sqlScriptText += "CREATE TABLE " + strTable + " (";
                    //sqlScriptText += "\r\n";

                    OdbcDataReader columnReader = OdbcHelper.ExecuteReader(connection, CommandType.Text, "SHOW COLUMNS IN " + strTable + "");

                    bool pFlag = false;
                    string strPColumnName = string.Empty;
                    string strFColumnName = string.Empty;

                    while (columnReader.Read())
                    {
                        strColumnName = columnReader.GetString(0);
                        strColumnType = columnReader.GetString(1);
                        strColumnNull = columnReader.GetString(2);
                        strColumnKey = columnReader.GetString(3);
                        //strColumnDflt = columnReader.GetString(4);
                        strColumnExtr = columnReader.GetString(5);

                        if (strColumnNull.Equals("NO"))
                            strColumnNull = "NOT NULL";
                        else
                            strColumnNull = "default NULL";

                        if (strColumnKey.Equals("PRI"))
                        {
                            pFlag = true;
                            strPColumnName = strColumnName;
                        }
                        sqlScriptText += "  " + strColumnName + "";
                        sqlScriptText += " " + strColumnType + "";
                        sqlScriptText += " " + strColumnNull + "";
                        if (!string.IsNullOrEmpty(strColumnExtr))
                            sqlScriptText += " " + strColumnExtr + "";
                        sqlScriptText += ",";
                       // sqlScriptText += "\r\n";

                    }
                    columnReader.Close();
                    if (pFlag)
                        sqlScriptText += "  PRIMARY KEY (" + strPColumnName + ")";
                    //sqlScriptText += "\r\n";
                    sqlScriptText += ")";
                    sqlScriptText += " ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
                   // sqlScriptText += "\r\n\r\n\r\n";
                }
                tableReader.Close();
                StreamReader reader = File.OpenText(m_MySQLScriptPath);
                string input = null;
                while ((input = reader.ReadLine()) != null)
                {
                    if (input.StartsWith("ALTER"))
                        break;
                    sqlScript += input;
                }
            }
            catch
            {
                return false;
            }
            if (string.Equals(sqlScript, sqlScriptText, StringComparison.CurrentCultureIgnoreCase))
                return true;
            else
                return false;

        }
        /// <summary>
        /// Updating the MySql connection string in app.config file.
        /// also updating a flag through which this form is visible only once.
        /// </summary>
        public void UpdateMySQLConnectionString()
        {
            string[] arrString = m_ConnectionString.Split(';');
            //arrString[2] = "Database=" +textBoxMySQLDBName.Text.Trim();
            //arrString[4] = "Password=" + textBoxMySqlDBPass.Text.Trim();

            arrString[2] = "Database=EDI";
            //arrString[4] = "Password=root";
            //arrString[4] = "Password=" + m_mysqlPassword;
            arrString[4] = "Password=root";
            string connectionString = string.Empty;
            foreach (string str in arrString)
            {
                if (!string.IsNullOrEmpty(str))
                     connectionString += str + ";";
            }

            DBConnection.MySqlDataAcess.StoreExportRegistry("MySQLConnectionString", connectionString);

            DBConnection.MySqlDataAcess.StoreExportRegistry("MySQLSettingFlag", "false");

            //DataProcessingBlocks.CommonUtilities.UpdateAppSettings("MySQLConnectionString", connectionString);
           
            //updating the flag.
           // DataProcessingBlocks.CommonUtilities.UpdateAppSettings("MySQLSettingFlag", "false");
        }

        private void MySqlSetting_Load(object sender, EventArgs e)
        {
            ToolTip clickHere = new ToolTip();
            clickHere.SetToolTip(linkLabelMysqlDwnld, "Click here to download and installed MySQL Server");
           
            //clickHere.SetToolTip(textBoxMySQLDBName, "Maximum length 20 characters");
            
            //clickHere.SetToolTip(textBoxMySqlDBPass, "Maximum length 15 characters");
            clickHere.AutoPopDelay = 50000;
            buttonBrowse.Focus();
           
        }

        private void linkLabelMysqlDwnld_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           // MessageBox.Show("Application is now terminating, Please run the application after MySQL is installed.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
           
            //launching the Internet explorer to download MySql Server
            Process.Start(Constants.Explorer, Constants.MySqlDownLoadLink);
            //MessageBox.Show("Please install Axis Database Installer and then double click on the Install Axis database icon on your desktop to finalise the setup.");

            TransactionImporter.IsCloseAppl = true;
            m_IsClose = true;
           
            this.Hide();
            //Application.Exit();
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            openFileDialogMySqlPath.Filter = "MySql exe|mysql.exe";
            openFileDialogMySqlPath.FileName = "";
            if (openFileDialogMySqlPath.ShowDialog() == DialogResult.OK)
            {
                textBoxMySqlPath.Text = openFileDialogMySqlPath.FileName;
            }
        }

       /// <summary>
       /// Appending a line in my.ini file of MySqlSetup file
       /// to increase the packet size for storing BLOB type value,
       /// </summary>
       /// <returns></returns>
        public bool appendInMyINI(bool fromTrans=false)
        {
			// Axis 590
            string mySqlPath;
            mySqlPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\MySQL\\MySQL Server 5.0\\bin\\mysql.exe";
            if (!File.Exists(mySqlPath))
            {
                mySqlPath = textBoxMySqlPath.Text.Trim();
            }
            //if (fromTrans)
            //{
            //    mySqlPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + "\\MySQL\\MySQL Server 5.0\\bin\\mysql.exe";
            //}
            //else
            //{
            //    mySqlPath = textBoxMySqlPath.Text.Trim();
            //}
            try
            {
                string path = mySqlPath.Replace("\\bin\\mysql.exe", "\\my.ini"); //file path of mysql setup file.
                if (File.Exists(mySqlPath) && File.Exists(path))
                {
                    string insertTag = "max_allowed_packet=512MB";              //string value which going to append.
                    String tag = "max_connections=100";
                    
                    //before inserting checking the string whether it is already exist.
                    bool flag = false;
                    StreamReader SR=File.OpenText(path);
                    string str=SR.ReadLine();
                    while(str!=null)
                    {
                        if (str.Equals(insertTag))
                        {
                            flag = true;
                            break;
                        }
                         str=SR.ReadLine();

                    }
                    SR.Close();
                    if (!flag)
                    {
                        //inserting the line.
                        String Contents = File.ReadAllText(path);
                        int index = Contents.IndexOf(tag);
                        index += tag.Length;
                        Contents = Contents.Insert(index, "\r\n");
                        index = index + 2;

                        Contents = Contents.Insert(index, insertTag);
                        try
                        {
                            File.WriteAllText(path, Contents);
                        }
                        catch
                        { }
                    }
                    return true;
                }
                else
                {
                    MessageBox.Show("MySql is not installed properly.Please install properly.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }

            catch 
            {
                MessageBox.Show("Error in MySQL setup.Please install properly.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Check for mysql connector driver whether it is installed in local machine,
        /// if it is not, It will install the driver.
        /// </summary>
        /// <returns></returns>
        public static bool InstallODBCDriver()
        {
            //const string ODBC_DRIVER = "mysql-connector-odbc-3.51.27-win32.msi";        
            //string ODBC_DRIVER_PATH = Application.StartupPath + @"\" + ODBC_DRIVER;

            //try
            //{
            //    //checking in registry it is installed or not.
            //    string isExist = check_ODBCDriver(); // (string)Registry.GetValue(ODBC_DRIVER_KEY, "MySQL ODBC 3.51 Driver", "Does not exist");
            //    if (isExist.Equals("Does not exist") || isExist == null)
            //    {
            //        if (File.Exists(ODBC_DRIVER_PATH))
            //        {
            //            Process listFiles = Process.Start(ODBC_DRIVER_PATH);

            //            listFiles.WaitForExit();
            //            if (listFiles.HasExited)
            //                return true;
            //            else
            //                return false;
            //        }
            //        else
            //        {
            //            MessageBox.Show("Pre-Requisite ODBC driver is not found in setup file. Appliction is now terminated.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //            return false;
            //        }
            //    }
            //    else
            //        return true;
            //}
            //catch(FileNotFoundException ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    return false;
            //}

            return true;
        }

        public static string check_ODBCDriver()
        {

            //checking in registry it is installed or not.
            return (string)Registry.GetValue(ODBC_DRIVER_KEY, "MySQL ODBC 3.51 Driver", "Does not exist");

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DataProcessingBlocks.CommonUtilities.GetInstance().IsCancelSetup = false;

            //Bug#1500 in Axis 6.0
            DBConnection.MySqlDataAcess.StoreExportRegistry("MySQLSettingFlag", "false");


            if (MessageBox.Show("Are you sure, you want to cancel MySql setting?", Constants.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) //
            {
                TransactionImporter.IsCloseAppl = true;
                m_IsClose = true;
                DataProcessingBlocks.CommonUtilities.GetInstance().IsCancelSetup = true;
                //this.Hide();
                //Application.Exit();
            }
            else
                m_IsClose = false;
        }
    }
}