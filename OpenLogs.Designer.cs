namespace DataProcessingBlocks
{
    partial class OpenLogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxOpenLog = new System.Windows.Forms.RichTextBox();
            this.buttonLogFiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBoxOpenLog
            // 
            this.richTextBoxOpenLog.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxOpenLog.Location = new System.Drawing.Point(12, 24);
            this.richTextBoxOpenLog.Name = "richTextBoxOpenLog";
            this.richTextBoxOpenLog.Size = new System.Drawing.Size(586, 473);
            this.richTextBoxOpenLog.TabIndex = 1;
            this.richTextBoxOpenLog.Text = "";
            // 
            // buttonLogFiles
            // 
            this.buttonLogFiles.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonLogFiles.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogFiles.Location = new System.Drawing.Point(465, 502);
            this.buttonLogFiles.Name = "buttonLogFiles";
            this.buttonLogFiles.Size = new System.Drawing.Size(101, 23);
            this.buttonLogFiles.TabIndex = 2;
            this.buttonLogFiles.Text = "Save Log files";
            this.buttonLogFiles.UseVisualStyleBackColor = true;
            this.buttonLogFiles.Click += new System.EventHandler(this.buttonLogFiles_Click);
            // 
            // OpenLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(610, 527);
            this.Controls.Add(this.buttonLogFiles);
            this.Controls.Add(this.richTextBoxOpenLog);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpenLogs";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Log data";
            this.Load += new System.EventHandler(this.OpenLog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxOpenLog;
        private System.Windows.Forms.Button buttonLogFiles;
    }
}