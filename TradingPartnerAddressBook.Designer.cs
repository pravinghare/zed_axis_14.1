namespace TradingPartnerDetails
{
    partial class TradingPartnerAddressBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TradingPartnerAddressBook));
            this.textBoxTPAlternateEmail = new System.Windows.Forms.TextBox();
            this.labelAlterEmailAddress = new System.Windows.Forms.Label();
            this.comboBoxTPMailProtocol = new System.Windows.Forms.ComboBox();
            this.labelMailProtocol = new System.Windows.Forms.Label();
            this.textBoxTPSchemaLoc = new System.Windows.Forms.TextBox();
            this.buttonTPSchemaBrowse = new System.Windows.Forms.Button();
            this.labelXSDSchema = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxTPClientID = new System.Windows.Forms.TextBox();
            this.textBoxTPEmailAddress = new System.Windows.Forms.TextBox();
            this.textBoxTPName = new System.Windows.Forms.TextBox();
            this.labelClientID = new System.Windows.Forms.Label();
            this.labelEmailAddress = new System.Windows.Forms.Label();
            this.labelTradingPartnerName = new System.Windows.Forms.Label();
            this.openFileDialogTPSchemaBrowse = new System.Windows.Forms.OpenFileDialog();
            this.labelTPNAsterik = new System.Windows.Forms.Label();
            this.labelMailProtocolAsteric = new System.Windows.Forms.Label();
            this.labelEmailAddressAsteric = new System.Windows.Forms.Label();
            this.panelTPABSave = new System.Windows.Forms.Panel();
            this.panelZencart = new System.Windows.Forms.Panel();
            this.panelSSHZencart = new System.Windows.Forms.Panel();
            this.textBoxSSHHostnameZencart = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.labelSSHHostnameZencart = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelSSHPortnoZencart = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelSSHUsernameZencart = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelSSHPasswordZencart = new System.Windows.Forms.Label();
            this.textBoxSSHPasswordZencart = new System.Windows.Forms.TextBox();
            this.textBoxSSHPortnoZencart = new System.Windows.Forms.TextBox();
            this.textBoxSSHUsernameZencart = new System.Windows.Forms.TextBox();
            this.dateTimePickerZencartLastDate = new System.Windows.Forms.DateTimePicker();
            this.labelZencartLastDate = new System.Windows.Forms.Label();
            this.checkBoxZenCartSSHTunnel = new System.Windows.Forms.CheckBox();
            this.labelZenCartShippingItem = new System.Windows.Forms.Label();
            this.comboBoxZenCartShippingItem = new System.Windows.Forms.ComboBox();
            this.labelPrefixExample = new System.Windows.Forms.Label();
            this.textBoxZenPrefix = new System.Windows.Forms.TextBox();
            this.labelZenPrefix = new System.Windows.Forms.Label();
            this.buttonZenTestConnection = new System.Windows.Forms.Button();
            this.labelZenDatabasenameAsteric = new System.Windows.Forms.Label();
            this.labelZenUsernameAsteric = new System.Windows.Forms.Label();
            this.labelZenPortNoAsteric = new System.Windows.Forms.Label();
            this.labelZenWebStoreAsteric = new System.Windows.Forms.Label();
            this.textBoxZenDatabaseName = new System.Windows.Forms.TextBox();
            this.textBoxZenPassword = new System.Windows.Forms.TextBox();
            this.labelZenDatabaseName = new System.Windows.Forms.Label();
            this.labelZenPassword = new System.Windows.Forms.Label();
            this.textBoxZenUsername = new System.Windows.Forms.TextBox();
            this.labelZenUserName = new System.Windows.Forms.Label();
            this.textBoxZenPortNo = new System.Windows.Forms.TextBox();
            this.labelZenPortNo = new System.Windows.Forms.Label();
            this.textBoxZenWebStoreUrl = new System.Windows.Forms.TextBox();
            this.labelZencartWebUrl = new System.Windows.Forms.Label();
            this.panelOSCommerce = new System.Windows.Forms.Panel();
            this.labelPrefixNote = new System.Windows.Forms.Label();
            this.textBox_Prefix = new System.Windows.Forms.TextBox();
            this.PrefixLabel = new System.Windows.Forms.Label();
            this.panelSSH = new System.Windows.Forms.Panel();
            this.textBoxSSHHostName = new System.Windows.Forms.TextBox();
            this.labelPasswordAsteric = new System.Windows.Forms.Label();
            this.labelSSHHostname = new System.Windows.Forms.Label();
            this.labelSSHUserAsteric = new System.Windows.Forms.Label();
            this.labelSSHPortNo = new System.Windows.Forms.Label();
            this.labelSSHPortAsteric = new System.Windows.Forms.Label();
            this.labelSSHUserName = new System.Windows.Forms.Label();
            this.labelSSHHostnameAsteric = new System.Windows.Forms.Label();
            this.labelSSHPassword = new System.Windows.Forms.Label();
            this.textBoxSSHPassword = new System.Windows.Forms.TextBox();
            this.textBoxSSHPortNo = new System.Windows.Forms.TextBox();
            this.textBoxSSHUserName = new System.Windows.Forms.TextBox();
            this.checkBoxSSHTunnel = new System.Windows.Forms.CheckBox();
            this.buttonSSHConnection = new System.Windows.Forms.Button();
            this.labelSSHMark = new System.Windows.Forms.Label();
            this.textBoxDBPortNo = new System.Windows.Forms.TextBox();
            this.labelPortno = new System.Windows.Forms.Label();
            this.dateTimePickerOSCommerceLastDate = new System.Windows.Forms.DateTimePicker();
            this.labelOSClastdate = new System.Windows.Forms.Label();
            this.comboBoxOSCShippingItem = new System.Windows.Forms.ComboBox();
            this.labelOSCShippingItem = new System.Windows.Forms.Label();
            this.labelDatabaseNameAsteric = new System.Windows.Forms.Label();
            this.textBoxDatabaseName = new System.Windows.Forms.TextBox();
            this.labelDatabaseName = new System.Windows.Forms.Label();
            this.textBoxOSCPassword = new System.Windows.Forms.TextBox();
            this.labelOSCPassword = new System.Windows.Forms.Label();
            this.labelOSCUsernameAsteric = new System.Windows.Forms.Label();
            this.labelOSCommerceWebStoreURL = new System.Windows.Forms.Label();
            this.textBoxWebStoreURL = new System.Windows.Forms.TextBox();
            this.labelWebStoreURLAsteric = new System.Windows.Forms.Label();
            this.labelOSCUsername = new System.Windows.Forms.Label();
            this.textBoxOSCUsername = new System.Windows.Forms.TextBox();
            this.panelXCart = new System.Windows.Forms.Panel();
            this.panelSSHXcart = new System.Windows.Forms.Panel();
            this.textBoxSSHHostnameXcart = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.labelSSHHostnameXcart = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.labelSSHPortnoXcart = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.labelSSHUserNameXcart = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.labelSSHPasswordXcart = new System.Windows.Forms.Label();
            this.textBoxSSHPasswordXcart = new System.Windows.Forms.TextBox();
            this.textBoxSSHPortnoXcart = new System.Windows.Forms.TextBox();
            this.textBoxSSHUserNameXcart = new System.Windows.Forms.TextBox();
            this.checkBoxXCartSSHTunnel = new System.Windows.Forms.CheckBox();
            this.dateTimePickerXCartLastDate = new System.Windows.Forms.DateTimePicker();
            this.labelXCartLastDate = new System.Windows.Forms.Label();
            this.comboBoxXCartShippingItem = new System.Windows.Forms.ComboBox();
            this.labelXCartShippingItem = new System.Windows.Forms.Label();
            this.labelXCartPrefExample = new System.Windows.Forms.Label();
            this.textBoxXCartPrefix = new System.Windows.Forms.TextBox();
            this.labelXCartPrefix = new System.Windows.Forms.Label();
            this.buttonXTestConnection = new System.Windows.Forms.Button();
            this.labelXCartDBAsterisk = new System.Windows.Forms.Label();
            this.textBoxXDBName = new System.Windows.Forms.TextBox();
            this.labelXCartDBName = new System.Windows.Forms.Label();
            this.labelXPAsterisk = new System.Windows.Forms.Label();
            this.textBoxXCartPort = new System.Windows.Forms.TextBox();
            this.labelXPort = new System.Windows.Forms.Label();
            this.labelXUserAsterisk = new System.Windows.Forms.Label();
            this.textBoxXCartUserName = new System.Windows.Forms.TextBox();
            this.labelXCartUserName = new System.Windows.Forms.Label();
            this.labelXPwdAsterisk = new System.Windows.Forms.Label();
            this.textBoxXCartPwd = new System.Windows.Forms.TextBox();
            this.labelXCartPwd = new System.Windows.Forms.Label();
            this.labelXURLAsterisk = new System.Windows.Forms.Label();
            this.textBoxXCartUrl = new System.Windows.Forms.TextBox();
            this.labelXCartUrl = new System.Windows.Forms.Label();
            this.panelPOP3 = new System.Windows.Forms.Panel();
            this.panelAlliedExpress = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxState = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxAccountCode = new System.Windows.Forms.TextBox();
            this.textBoxAccountNumber = new System.Windows.Forms.TextBox();
            this.labelAlliedExpressAccountNumber = new System.Windows.Forms.Label();
            this.labelAlliedExpressAccountNumberAsteric = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAlliedExpressType = new System.Windows.Forms.ComboBox();
            this.labelAlledExpressType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPrefix = new System.Windows.Forms.TextBox();
            this.labelPrefix = new System.Windows.Forms.Label();
            this.labelAlliedExpressFromPhone = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxCompanyPhone = new System.Windows.Forms.TextBox();
            this.labelAECompanyNameAsteric = new System.Windows.Forms.Label();
            this.labelAlliedExpressCompanyName = new System.Windows.Forms.Label();
            this.textBoxAlliedExpressCompanyName = new System.Windows.Forms.TextBox();
            this.paneleBay = new System.Windows.Forms.Panel();
            this.labeleBayTypeAsteric = new System.Windows.Forms.Label();
            this.comboBoxeBayType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDiscountItemsAsteric = new System.Windows.Forms.Label();
            this.labelItemNameAsteric = new System.Windows.Forms.Label();
            this.comboBoxDiscountItems = new System.Windows.Forms.ComboBox();
            this.comboBoxItemName = new System.Windows.Forms.ComboBox();
            this.labelDiscountItemName = new System.Windows.Forms.Label();
            this.labelItemName = new System.Windows.Forms.Label();
            this.labelUpdatedDateAsterick = new System.Windows.Forms.Label();
            this.labelDateMessage = new System.Windows.Forms.Label();
            this.dateTimePickerLastUpdated = new System.Windows.Forms.DateTimePicker();
            this.labelLastUpdatedDate = new System.Windows.Forms.Label();
            this.buttoneBayTicket = new System.Windows.Forms.Button();
            this.textBoxeBayAuthToken = new System.Windows.Forms.TextBox();
            this.labelEbayToken = new System.Windows.Forms.Label();
            this.buttonAuthorization = new System.Windows.Forms.Button();
            this.panelTPABShow = new System.Windows.Forms.Panel();
            this.dataGridViewTPABShow = new System.Windows.Forms.DataGridView();
            this.buttonCancelForm = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAddNew = new System.Windows.Forms.Button();
            this.panelTPABSave.SuspendLayout();
            this.panelZencart.SuspendLayout();
            this.panelSSHZencart.SuspendLayout();
            this.panelOSCommerce.SuspendLayout();
            this.panelSSH.SuspendLayout();
            this.panelXCart.SuspendLayout();
            this.panelSSHXcart.SuspendLayout();
            this.panelPOP3.SuspendLayout();
            this.panelAlliedExpress.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.paneleBay.SuspendLayout();
            this.panelTPABShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTPABShow)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxTPAlternateEmail
            // 
            this.textBoxTPAlternateEmail.Location = new System.Drawing.Point(226, 64);
            this.textBoxTPAlternateEmail.MaxLength = 100;
            this.textBoxTPAlternateEmail.Name = "textBoxTPAlternateEmail";
            this.textBoxTPAlternateEmail.Size = new System.Drawing.Size(200, 21);
            this.textBoxTPAlternateEmail.TabIndex = 3;
            // 
            // labelAlterEmailAddress
            // 
            this.labelAlterEmailAddress.AutoSize = true;
            this.labelAlterEmailAddress.Location = new System.Drawing.Point(40, 72);
            this.labelAlterEmailAddress.Name = "labelAlterEmailAddress";
            this.labelAlterEmailAddress.Size = new System.Drawing.Size(159, 13);
            this.labelAlterEmailAddress.TabIndex = 14;
            this.labelAlterEmailAddress.Text = "Alternative Email Address:";
            // 
            // comboBoxTPMailProtocol
            // 
            this.comboBoxTPMailProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTPMailProtocol.FormattingEnabled = true;
            this.comboBoxTPMailProtocol.Location = new System.Drawing.Point(282, 52);
            this.comboBoxTPMailProtocol.Name = "comboBoxTPMailProtocol";
            this.comboBoxTPMailProtocol.Size = new System.Drawing.Size(200, 21);
            this.comboBoxTPMailProtocol.TabIndex = 1;
            this.comboBoxTPMailProtocol.SelectedIndexChanged += new System.EventHandler(this.comboBoxTPMailProtocol_SelectedIndexChanged);
            // 
            // labelMailProtocol
            // 
            this.labelMailProtocol.AutoSize = true;
            this.labelMailProtocol.Location = new System.Drawing.Point(154, 52);
            this.labelMailProtocol.Name = "labelMailProtocol";
            this.labelMailProtocol.Size = new System.Drawing.Size(96, 13);
            this.labelMailProtocol.TabIndex = 13;
            this.labelMailProtocol.Text = "   Mail Protocol:";
            // 
            // textBoxTPSchemaLoc
            // 
            this.textBoxTPSchemaLoc.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxTPSchemaLoc.Location = new System.Drawing.Point(226, 153);
            this.textBoxTPSchemaLoc.MaxLength = 100;
            this.textBoxTPSchemaLoc.Name = "textBoxTPSchemaLoc";
            this.textBoxTPSchemaLoc.ReadOnly = true;
            this.textBoxTPSchemaLoc.Size = new System.Drawing.Size(200, 21);
            this.textBoxTPSchemaLoc.TabIndex = 6;
            this.textBoxTPSchemaLoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTPSchema_KeyDown);
            // 
            // buttonTPSchemaBrowse
            // 
            this.buttonTPSchemaBrowse.Location = new System.Drawing.Point(434, 152);
            this.buttonTPSchemaBrowse.Name = "buttonTPSchemaBrowse";
            this.buttonTPSchemaBrowse.Size = new System.Drawing.Size(100, 21);
            this.buttonTPSchemaBrowse.TabIndex = 5;
            this.buttonTPSchemaBrowse.Text = "&Browse...";
            this.buttonTPSchemaBrowse.UseVisualStyleBackColor = true;
            this.buttonTPSchemaBrowse.Click += new System.EventHandler(this.buttonTPSchemaBrowse_Click);
            // 
            // labelXSDSchema
            // 
            this.labelXSDSchema.AutoSize = true;
            this.labelXSDSchema.Location = new System.Drawing.Point(51, 156);
            this.labelXSDSchema.Name = "labelXSDSchema";
            this.labelXSDSchema.Size = new System.Drawing.Size(148, 13);
            this.labelXSDSchema.TabIndex = 8;
            this.labelXSDSchema.Text = "Schema Location (XSD):";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(335, 577);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(100, 25);
            this.buttonCancel.TabIndex = 16;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(190, 577);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(100, 25);
            this.buttonAdd.TabIndex = 22;
            this.buttonAdd.Text = "&Save";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxTPClientID
            // 
            this.textBoxTPClientID.Location = new System.Drawing.Point(226, 103);
            this.textBoxTPClientID.MaxLength = 100;
            this.textBoxTPClientID.Name = "textBoxTPClientID";
            this.textBoxTPClientID.Size = new System.Drawing.Size(200, 21);
            this.textBoxTPClientID.TabIndex = 4;
            // 
            // textBoxTPEmailAddress
            // 
            this.textBoxTPEmailAddress.Location = new System.Drawing.Point(226, 24);
            this.textBoxTPEmailAddress.MaxLength = 100;
            this.textBoxTPEmailAddress.Name = "textBoxTPEmailAddress";
            this.textBoxTPEmailAddress.Size = new System.Drawing.Size(200, 21);
            this.textBoxTPEmailAddress.TabIndex = 2;
            // 
            // textBoxTPName
            // 
            this.textBoxTPName.Location = new System.Drawing.Point(282, 11);
            this.textBoxTPName.MaxLength = 100;
            this.textBoxTPName.Name = "textBoxTPName";
            this.textBoxTPName.Size = new System.Drawing.Size(200, 21);
            this.textBoxTPName.TabIndex = 0;
            // 
            // labelClientID
            // 
            this.labelClientID.AutoSize = true;
            this.labelClientID.Location = new System.Drawing.Point(136, 111);
            this.labelClientID.Name = "labelClientID";
            this.labelClientID.Size = new System.Drawing.Size(63, 13);
            this.labelClientID.TabIndex = 2;
            this.labelClientID.Text = "Client ID:";
            // 
            // labelEmailAddress
            // 
            this.labelEmailAddress.AutoSize = true;
            this.labelEmailAddress.Location = new System.Drawing.Point(106, 32);
            this.labelEmailAddress.Name = "labelEmailAddress";
            this.labelEmailAddress.Size = new System.Drawing.Size(93, 13);
            this.labelEmailAddress.TabIndex = 1;
            this.labelEmailAddress.Text = "Email Address:";
            // 
            // labelTradingPartnerName
            // 
            this.labelTradingPartnerName.AutoSize = true;
            this.labelTradingPartnerName.Location = new System.Drawing.Point(106, 14);
            this.labelTradingPartnerName.Name = "labelTradingPartnerName";
            this.labelTradingPartnerName.Size = new System.Drawing.Size(150, 13);
            this.labelTradingPartnerName.TabIndex = 0;
            this.labelTradingPartnerName.Text = "   Trading Partner Name:";
            // 
            // openFileDialogTPSchemaBrowse
            // 
            this.openFileDialogTPSchemaBrowse.FileName = "openFileDialog1";
            // 
            // labelTPNAsterik
            // 
            this.labelTPNAsterik.AutoSize = true;
            this.labelTPNAsterik.ForeColor = System.Drawing.Color.Red;
            this.labelTPNAsterik.Location = new System.Drawing.Point(488, 17);
            this.labelTPNAsterik.Name = "labelTPNAsterik";
            this.labelTPNAsterik.Size = new System.Drawing.Size(14, 13);
            this.labelTPNAsterik.TabIndex = 15;
            this.labelTPNAsterik.Text = "*";
            // 
            // labelMailProtocolAsteric
            // 
            this.labelMailProtocolAsteric.AutoSize = true;
            this.labelMailProtocolAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelMailProtocolAsteric.Location = new System.Drawing.Point(488, 55);
            this.labelMailProtocolAsteric.Name = "labelMailProtocolAsteric";
            this.labelMailProtocolAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelMailProtocolAsteric.TabIndex = 16;
            this.labelMailProtocolAsteric.Text = "*";
            // 
            // labelEmailAddressAsteric
            // 
            this.labelEmailAddressAsteric.AutoSize = true;
            this.labelEmailAddressAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelEmailAddressAsteric.Location = new System.Drawing.Point(432, 27);
            this.labelEmailAddressAsteric.Name = "labelEmailAddressAsteric";
            this.labelEmailAddressAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelEmailAddressAsteric.TabIndex = 17;
            this.labelEmailAddressAsteric.Text = "*";
            // 
            // panelTPABSave
            // 
            this.panelTPABSave.Controls.Add(this.panelXCart);
            this.panelTPABSave.Controls.Add(this.panelZencart);
            this.panelTPABSave.Controls.Add(this.panelOSCommerce);
            this.panelTPABSave.Controls.Add(this.panelPOP3);
            this.panelTPABSave.Controls.Add(this.panelAlliedExpress);
            this.panelTPABSave.Controls.Add(this.comboBoxTPMailProtocol);
            this.panelTPABSave.Controls.Add(this.labelTradingPartnerName);
            this.panelTPABSave.Controls.Add(this.labelMailProtocolAsteric);
            this.panelTPABSave.Controls.Add(this.labelTPNAsterik);
            this.panelTPABSave.Controls.Add(this.textBoxTPName);
            this.panelTPABSave.Controls.Add(this.buttonAdd);
            this.panelTPABSave.Controls.Add(this.buttonCancel);
            this.panelTPABSave.Controls.Add(this.labelMailProtocol);
            this.panelTPABSave.Controls.Add(this.paneleBay);
            this.panelTPABSave.Location = new System.Drawing.Point(12, 25);
            this.panelTPABSave.Name = "panelTPABSave";
            this.panelTPABSave.Size = new System.Drawing.Size(620, 605);
            this.panelTPABSave.TabIndex = 18;
            // 
            // panelZencart
            // 
            this.panelZencart.Controls.Add(this.panelSSHZencart);
            this.panelZencart.Controls.Add(this.dateTimePickerZencartLastDate);
            this.panelZencart.Controls.Add(this.labelZencartLastDate);
            this.panelZencart.Controls.Add(this.checkBoxZenCartSSHTunnel);
            this.panelZencart.Controls.Add(this.labelZenCartShippingItem);
            this.panelZencart.Controls.Add(this.comboBoxZenCartShippingItem);
            this.panelZencart.Controls.Add(this.labelPrefixExample);
            this.panelZencart.Controls.Add(this.textBoxZenPrefix);
            this.panelZencart.Controls.Add(this.labelZenPrefix);
            this.panelZencart.Controls.Add(this.buttonZenTestConnection);
            this.panelZencart.Controls.Add(this.labelZenDatabasenameAsteric);
            this.panelZencart.Controls.Add(this.labelZenUsernameAsteric);
            this.panelZencart.Controls.Add(this.labelZenPortNoAsteric);
            this.panelZencart.Controls.Add(this.labelZenWebStoreAsteric);
            this.panelZencart.Controls.Add(this.textBoxZenDatabaseName);
            this.panelZencart.Controls.Add(this.textBoxZenPassword);
            this.panelZencart.Controls.Add(this.labelZenDatabaseName);
            this.panelZencart.Controls.Add(this.labelZenPassword);
            this.panelZencart.Controls.Add(this.textBoxZenUsername);
            this.panelZencart.Controls.Add(this.labelZenUserName);
            this.panelZencart.Controls.Add(this.textBoxZenPortNo);
            this.panelZencart.Controls.Add(this.labelZenPortNo);
            this.panelZencart.Controls.Add(this.textBoxZenWebStoreUrl);
            this.panelZencart.Controls.Add(this.labelZencartWebUrl);
            this.panelZencart.Location = new System.Drawing.Point(17, 83);
            this.panelZencart.Name = "panelZencart";
            this.panelZencart.Size = new System.Drawing.Size(570, 488);
            this.panelZencart.TabIndex = 52;
            // 
            // panelSSHZencart
            // 
            this.panelSSHZencart.Controls.Add(this.textBoxSSHHostnameZencart);
            this.panelSSHZencart.Controls.Add(this.label12);
            this.panelSSHZencart.Controls.Add(this.labelSSHHostnameZencart);
            this.panelSSHZencart.Controls.Add(this.label13);
            this.panelSSHZencart.Controls.Add(this.labelSSHPortnoZencart);
            this.panelSSHZencart.Controls.Add(this.label14);
            this.panelSSHZencart.Controls.Add(this.labelSSHUsernameZencart);
            this.panelSSHZencart.Controls.Add(this.label16);
            this.panelSSHZencart.Controls.Add(this.labelSSHPasswordZencart);
            this.panelSSHZencart.Controls.Add(this.textBoxSSHPasswordZencart);
            this.panelSSHZencart.Controls.Add(this.textBoxSSHPortnoZencart);
            this.panelSSHZencart.Controls.Add(this.textBoxSSHUsernameZencart);
            this.panelSSHZencart.Location = new System.Drawing.Point(67, 278);
            this.panelSSHZencart.Name = "panelSSHZencart";
            this.panelSSHZencart.Size = new System.Drawing.Size(493, 154);
            this.panelSSHZencart.TabIndex = 54;
            this.panelSSHZencart.Visible = false;
            // 
            // textBoxSSHHostnameZencart
            // 
            this.textBoxSSHHostnameZencart.Location = new System.Drawing.Point(196, 13);
            this.textBoxSSHHostnameZencart.Name = "textBoxSSHHostnameZencart";
            this.textBoxSSHHostnameZencart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHHostnameZencart.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(400, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "*";
            // 
            // labelSSHHostnameZencart
            // 
            this.labelSSHHostnameZencart.AutoSize = true;
            this.labelSSHHostnameZencart.Location = new System.Drawing.Point(11, 16);
            this.labelSSHHostnameZencart.Name = "labelSSHHostnameZencart";
            this.labelSSHHostnameZencart.Size = new System.Drawing.Size(167, 13);
            this.labelSSHHostnameZencart.TabIndex = 36;
            this.labelSSHHostnameZencart.Text = "SSH Hostname/ IP address:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(400, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "*";
            // 
            // labelSSHPortnoZencart
            // 
            this.labelSSHPortnoZencart.AutoSize = true;
            this.labelSSHPortnoZencart.Location = new System.Drawing.Point(23, 52);
            this.labelSSHPortnoZencart.Name = "labelSSHPortnoZencart";
            this.labelSSHPortnoZencart.Size = new System.Drawing.Size(155, 13);
            this.labelSSHPortnoZencart.TabIndex = 37;
            this.labelSSHPortnoZencart.Text = "SSH Port No (Default: 22)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(400, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "*";
            // 
            // labelSSHUsernameZencart
            // 
            this.labelSSHUsernameZencart.AutoSize = true;
            this.labelSSHUsernameZencart.Location = new System.Drawing.Point(80, 90);
            this.labelSSHUsernameZencart.Name = "labelSSHUsernameZencart";
            this.labelSSHUsernameZencart.Size = new System.Drawing.Size(99, 13);
            this.labelSSHUsernameZencart.TabIndex = 38;
            this.labelSSHUsernameZencart.Text = "SSH UserName:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(400, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "*";
            // 
            // labelSSHPasswordZencart
            // 
            this.labelSSHPasswordZencart.AutoSize = true;
            this.labelSSHPasswordZencart.Location = new System.Drawing.Point(84, 126);
            this.labelSSHPasswordZencart.Name = "labelSSHPasswordZencart";
            this.labelSSHPasswordZencart.Size = new System.Drawing.Size(94, 13);
            this.labelSSHPasswordZencart.TabIndex = 39;
            this.labelSSHPasswordZencart.Text = "SSH Password:";
            // 
            // textBoxSSHPasswordZencart
            // 
            this.textBoxSSHPasswordZencart.Location = new System.Drawing.Point(196, 124);
            this.textBoxSSHPasswordZencart.Name = "textBoxSSHPasswordZencart";
            this.textBoxSSHPasswordZencart.PasswordChar = '*';
            this.textBoxSSHPasswordZencart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPasswordZencart.TabIndex = 12;
            // 
            // textBoxSSHPortnoZencart
            // 
            this.textBoxSSHPortnoZencart.Location = new System.Drawing.Point(196, 49);
            this.textBoxSSHPortnoZencart.Name = "textBoxSSHPortnoZencart";
            this.textBoxSSHPortnoZencart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPortnoZencart.TabIndex = 10;
            // 
            // textBoxSSHUsernameZencart
            // 
            this.textBoxSSHUsernameZencart.Location = new System.Drawing.Point(196, 87);
            this.textBoxSSHUsernameZencart.Name = "textBoxSSHUsernameZencart";
            this.textBoxSSHUsernameZencart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHUsernameZencart.TabIndex = 11;
            // 
            // dateTimePickerZencartLastDate
            // 
            this.dateTimePickerZencartLastDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerZencartLastDate.Location = new System.Drawing.Point(268, 431);
            this.dateTimePickerZencartLastDate.Name = "dateTimePickerZencartLastDate";
            this.dateTimePickerZencartLastDate.Size = new System.Drawing.Size(112, 21);
            this.dateTimePickerZencartLastDate.TabIndex = 52;
            // 
            // labelZencartLastDate
            // 
            this.labelZencartLastDate.AutoSize = true;
            this.labelZencartLastDate.Location = new System.Drawing.Point(96, 437);
            this.labelZencartLastDate.Name = "labelZencartLastDate";
            this.labelZencartLastDate.Size = new System.Drawing.Size(156, 13);
            this.labelZencartLastDate.TabIndex = 53;
            this.labelZencartLastDate.Text = "Select Last Updated Date:";
            // 
            // checkBoxZenCartSSHTunnel
            // 
            this.checkBoxZenCartSSHTunnel.AutoSize = true;
            this.checkBoxZenCartSSHTunnel.Location = new System.Drawing.Point(267, 261);
            this.checkBoxZenCartSSHTunnel.Name = "checkBoxZenCartSSHTunnel";
            this.checkBoxZenCartSSHTunnel.Size = new System.Drawing.Size(117, 17);
            this.checkBoxZenCartSSHTunnel.TabIndex = 50;
            this.checkBoxZenCartSSHTunnel.Text = "Use SSH Tunnel";
            this.checkBoxZenCartSSHTunnel.UseVisualStyleBackColor = true;
            this.checkBoxZenCartSSHTunnel.CheckedChanged += new System.EventHandler(this.checkBoxZenCartSSHTunnel_CheckedChanged);
            // 
            // labelZenCartShippingItem
            // 
            this.labelZenCartShippingItem.AutoSize = true;
            this.labelZenCartShippingItem.Location = new System.Drawing.Point(113, 237);
            this.labelZenCartShippingItem.Name = "labelZenCartShippingItem";
            this.labelZenCartShippingItem.Size = new System.Drawing.Size(131, 13);
            this.labelZenCartShippingItem.TabIndex = 51;
            this.labelZenCartShippingItem.Text = "Select Shipping Item:\r\n";
            // 
            // comboBoxZenCartShippingItem
            // 
            this.comboBoxZenCartShippingItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxZenCartShippingItem.FormattingEnabled = true;
            this.comboBoxZenCartShippingItem.Location = new System.Drawing.Point(262, 234);
            this.comboBoxZenCartShippingItem.Name = "comboBoxZenCartShippingItem";
            this.comboBoxZenCartShippingItem.Size = new System.Drawing.Size(298, 21);
            this.comboBoxZenCartShippingItem.TabIndex = 49;
            // 
            // labelPrefixExample
            // 
            this.labelPrefixExample.AutoSize = true;
            this.labelPrefixExample.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrefixExample.Location = new System.Drawing.Point(262, 215);
            this.labelPrefixExample.Name = "labelPrefixExample";
            this.labelPrefixExample.Size = new System.Drawing.Size(113, 12);
            this.labelPrefixExample.TabIndex = 18;
            this.labelPrefixExample.Text = "(For eg : \" zencart_\" )";
            // 
            // textBoxZenPrefix
            // 
            this.textBoxZenPrefix.Location = new System.Drawing.Point(261, 186);
            this.textBoxZenPrefix.Name = "textBoxZenPrefix";
            this.textBoxZenPrefix.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenPrefix.TabIndex = 17;
            // 
            // labelZenPrefix
            // 
            this.labelZenPrefix.AutoSize = true;
            this.labelZenPrefix.Location = new System.Drawing.Point(166, 191);
            this.labelZenPrefix.Name = "labelZenPrefix";
            this.labelZenPrefix.Size = new System.Drawing.Size(80, 13);
            this.labelZenPrefix.TabIndex = 16;
            this.labelZenPrefix.Text = "Table Prefix:";
            // 
            // buttonZenTestConnection
            // 
            this.buttonZenTestConnection.Location = new System.Drawing.Point(39, 457);
            this.buttonZenTestConnection.Name = "buttonZenTestConnection";
            this.buttonZenTestConnection.Size = new System.Drawing.Size(143, 23);
            this.buttonZenTestConnection.TabIndex = 15;
            this.buttonZenTestConnection.Text = "Test Connection";
            this.buttonZenTestConnection.UseVisualStyleBackColor = true;
            this.buttonZenTestConnection.Click += new System.EventHandler(this.buttonZenTestConnection_Click);
            // 
            // labelZenDatabasenameAsteric
            // 
            this.labelZenDatabasenameAsteric.AutoSize = true;
            this.labelZenDatabasenameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelZenDatabasenameAsteric.Location = new System.Drawing.Point(470, 156);
            this.labelZenDatabasenameAsteric.Name = "labelZenDatabasenameAsteric";
            this.labelZenDatabasenameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelZenDatabasenameAsteric.TabIndex = 14;
            this.labelZenDatabasenameAsteric.Text = "*";
            // 
            // labelZenUsernameAsteric
            // 
            this.labelZenUsernameAsteric.AutoSize = true;
            this.labelZenUsernameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelZenUsernameAsteric.Location = new System.Drawing.Point(469, 80);
            this.labelZenUsernameAsteric.Name = "labelZenUsernameAsteric";
            this.labelZenUsernameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelZenUsernameAsteric.TabIndex = 12;
            this.labelZenUsernameAsteric.Text = "*";
            // 
            // labelZenPortNoAsteric
            // 
            this.labelZenPortNoAsteric.AutoSize = true;
            this.labelZenPortNoAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelZenPortNoAsteric.Location = new System.Drawing.Point(469, 48);
            this.labelZenPortNoAsteric.Name = "labelZenPortNoAsteric";
            this.labelZenPortNoAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelZenPortNoAsteric.TabIndex = 11;
            this.labelZenPortNoAsteric.Text = "*";
            // 
            // labelZenWebStoreAsteric
            // 
            this.labelZenWebStoreAsteric.AutoSize = true;
            this.labelZenWebStoreAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelZenWebStoreAsteric.Location = new System.Drawing.Point(469, 14);
            this.labelZenWebStoreAsteric.Name = "labelZenWebStoreAsteric";
            this.labelZenWebStoreAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelZenWebStoreAsteric.TabIndex = 10;
            this.labelZenWebStoreAsteric.Text = "*";
            // 
            // textBoxZenDatabaseName
            // 
            this.textBoxZenDatabaseName.Location = new System.Drawing.Point(262, 151);
            this.textBoxZenDatabaseName.Name = "textBoxZenDatabaseName";
            this.textBoxZenDatabaseName.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenDatabaseName.TabIndex = 9;
            // 
            // textBoxZenPassword
            // 
            this.textBoxZenPassword.Location = new System.Drawing.Point(262, 113);
            this.textBoxZenPassword.Name = "textBoxZenPassword";
            this.textBoxZenPassword.PasswordChar = '*';
            this.textBoxZenPassword.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenPassword.TabIndex = 8;
            // 
            // labelZenDatabaseName
            // 
            this.labelZenDatabaseName.AutoSize = true;
            this.labelZenDatabaseName.Location = new System.Drawing.Point(143, 154);
            this.labelZenDatabaseName.Name = "labelZenDatabaseName";
            this.labelZenDatabaseName.Size = new System.Drawing.Size(103, 13);
            this.labelZenDatabaseName.TabIndex = 7;
            this.labelZenDatabaseName.Text = "Database Name:";
            // 
            // labelZenPassword
            // 
            this.labelZenPassword.AutoSize = true;
            this.labelZenPassword.Location = new System.Drawing.Point(133, 116);
            this.labelZenPassword.Name = "labelZenPassword";
            this.labelZenPassword.Size = new System.Drawing.Size(114, 13);
            this.labelZenPassword.TabIndex = 6;
            this.labelZenPassword.Text = "Zencart Password:";
            // 
            // textBoxZenUsername
            // 
            this.textBoxZenUsername.Location = new System.Drawing.Point(263, 77);
            this.textBoxZenUsername.Name = "textBoxZenUsername";
            this.textBoxZenUsername.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenUsername.TabIndex = 5;
            // 
            // labelZenUserName
            // 
            this.labelZenUserName.AutoSize = true;
            this.labelZenUserName.Location = new System.Drawing.Point(129, 80);
            this.labelZenUserName.Name = "labelZenUserName";
            this.labelZenUserName.Size = new System.Drawing.Size(118, 13);
            this.labelZenUserName.TabIndex = 4;
            this.labelZenUserName.Text = "Zencart Username:";
            // 
            // textBoxZenPortNo
            // 
            this.textBoxZenPortNo.Location = new System.Drawing.Point(263, 43);
            this.textBoxZenPortNo.Name = "textBoxZenPortNo";
            this.textBoxZenPortNo.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenPortNo.TabIndex = 3;
            // 
            // labelZenPortNo
            // 
            this.labelZenPortNo.AutoSize = true;
            this.labelZenPortNo.Location = new System.Drawing.Point(193, 46);
            this.labelZenPortNo.Name = "labelZenPortNo";
            this.labelZenPortNo.Size = new System.Drawing.Size(54, 13);
            this.labelZenPortNo.TabIndex = 2;
            this.labelZenPortNo.Text = "Port No:";
            // 
            // textBoxZenWebStoreUrl
            // 
            this.textBoxZenWebStoreUrl.Location = new System.Drawing.Point(264, 9);
            this.textBoxZenWebStoreUrl.Name = "textBoxZenWebStoreUrl";
            this.textBoxZenWebStoreUrl.Size = new System.Drawing.Size(200, 21);
            this.textBoxZenWebStoreUrl.TabIndex = 1;
            // 
            // labelZencartWebUrl
            // 
            this.labelZencartWebUrl.AutoSize = true;
            this.labelZencartWebUrl.Location = new System.Drawing.Point(101, 12);
            this.labelZencartWebUrl.Name = "labelZencartWebUrl";
            this.labelZencartWebUrl.Size = new System.Drawing.Size(146, 13);
            this.labelZencartWebUrl.TabIndex = 0;
            this.labelZencartWebUrl.Text = "Zencart Web Store URL:";
            // 
            // panelOSCommerce
            // 
            this.panelOSCommerce.Controls.Add(this.labelPrefixNote);
            this.panelOSCommerce.Controls.Add(this.textBox_Prefix);
            this.panelOSCommerce.Controls.Add(this.PrefixLabel);
            this.panelOSCommerce.Controls.Add(this.panelSSH);
            this.panelOSCommerce.Controls.Add(this.checkBoxSSHTunnel);
            this.panelOSCommerce.Controls.Add(this.buttonSSHConnection);
            this.panelOSCommerce.Controls.Add(this.labelSSHMark);
            this.panelOSCommerce.Controls.Add(this.textBoxDBPortNo);
            this.panelOSCommerce.Controls.Add(this.labelPortno);
            this.panelOSCommerce.Controls.Add(this.dateTimePickerOSCommerceLastDate);
            this.panelOSCommerce.Controls.Add(this.labelOSClastdate);
            this.panelOSCommerce.Controls.Add(this.comboBoxOSCShippingItem);
            this.panelOSCommerce.Controls.Add(this.labelOSCShippingItem);
            this.panelOSCommerce.Controls.Add(this.labelDatabaseNameAsteric);
            this.panelOSCommerce.Controls.Add(this.textBoxDatabaseName);
            this.panelOSCommerce.Controls.Add(this.labelDatabaseName);
            this.panelOSCommerce.Controls.Add(this.textBoxOSCPassword);
            this.panelOSCommerce.Controls.Add(this.labelOSCPassword);
            this.panelOSCommerce.Controls.Add(this.labelOSCUsernameAsteric);
            this.panelOSCommerce.Controls.Add(this.labelOSCommerceWebStoreURL);
            this.panelOSCommerce.Controls.Add(this.textBoxWebStoreURL);
            this.panelOSCommerce.Controls.Add(this.labelWebStoreURLAsteric);
            this.panelOSCommerce.Controls.Add(this.labelOSCUsername);
            this.panelOSCommerce.Controls.Add(this.textBoxOSCUsername);
            this.panelOSCommerce.Location = new System.Drawing.Point(54, 84);
            this.panelOSCommerce.Name = "panelOSCommerce";
            this.panelOSCommerce.Size = new System.Drawing.Size(554, 487);
            this.panelOSCommerce.TabIndex = 20;
            this.panelOSCommerce.Visible = false;
            // 
            // labelPrefixNote
            // 
            this.labelPrefixNote.AutoSize = true;
            this.labelPrefixNote.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrefixNote.Location = new System.Drawing.Point(228, 207);
            this.labelPrefixNote.Name = "labelPrefixNote";
            this.labelPrefixNote.Size = new System.Drawing.Size(151, 12);
            this.labelPrefixNote.TabIndex = 51;
            this.labelPrefixNote.Text = "(For eg : \" osc_\" or \"3fish_\" )";
            // 
            // textBox_Prefix
            // 
            this.textBox_Prefix.Location = new System.Drawing.Point(225, 184);
            this.textBox_Prefix.Name = "textBox_Prefix";
            this.textBox_Prefix.Size = new System.Drawing.Size(199, 21);
            this.textBox_Prefix.TabIndex = 50;
            // 
            // PrefixLabel
            // 
            this.PrefixLabel.AutoSize = true;
            this.PrefixLabel.Location = new System.Drawing.Point(128, 188);
            this.PrefixLabel.Name = "PrefixLabel";
            this.PrefixLabel.Size = new System.Drawing.Size(80, 13);
            this.PrefixLabel.TabIndex = 49;
            this.PrefixLabel.Text = "Table Prefix:";
            // 
            // panelSSH
            // 
            this.panelSSH.Controls.Add(this.textBoxSSHHostName);
            this.panelSSH.Controls.Add(this.labelPasswordAsteric);
            this.panelSSH.Controls.Add(this.labelSSHHostname);
            this.panelSSH.Controls.Add(this.labelSSHUserAsteric);
            this.panelSSH.Controls.Add(this.labelSSHPortNo);
            this.panelSSH.Controls.Add(this.labelSSHPortAsteric);
            this.panelSSH.Controls.Add(this.labelSSHUserName);
            this.panelSSH.Controls.Add(this.labelSSHHostnameAsteric);
            this.panelSSH.Controls.Add(this.labelSSHPassword);
            this.panelSSH.Controls.Add(this.textBoxSSHPassword);
            this.panelSSH.Controls.Add(this.textBoxSSHPortNo);
            this.panelSSH.Controls.Add(this.textBoxSSHUserName);
            this.panelSSH.Location = new System.Drawing.Point(30, 274);
            this.panelSSH.Name = "panelSSH";
            this.panelSSH.Size = new System.Drawing.Size(493, 154);
            this.panelSSH.TabIndex = 48;
            this.panelSSH.Visible = false;
            // 
            // textBoxSSHHostName
            // 
            this.textBoxSSHHostName.Location = new System.Drawing.Point(196, 13);
            this.textBoxSSHHostName.Name = "textBoxSSHHostName";
            this.textBoxSSHHostName.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHHostName.TabIndex = 9;
            // 
            // labelPasswordAsteric
            // 
            this.labelPasswordAsteric.AutoSize = true;
            this.labelPasswordAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelPasswordAsteric.Location = new System.Drawing.Point(400, 127);
            this.labelPasswordAsteric.Name = "labelPasswordAsteric";
            this.labelPasswordAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelPasswordAsteric.TabIndex = 47;
            this.labelPasswordAsteric.Text = "*";
            // 
            // labelSSHHostname
            // 
            this.labelSSHHostname.AutoSize = true;
            this.labelSSHHostname.Location = new System.Drawing.Point(11, 16);
            this.labelSSHHostname.Name = "labelSSHHostname";
            this.labelSSHHostname.Size = new System.Drawing.Size(167, 13);
            this.labelSSHHostname.TabIndex = 36;
            this.labelSSHHostname.Text = "SSH Hostname/ IP address:";
            // 
            // labelSSHUserAsteric
            // 
            this.labelSSHUserAsteric.AutoSize = true;
            this.labelSSHUserAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelSSHUserAsteric.Location = new System.Drawing.Point(400, 90);
            this.labelSSHUserAsteric.Name = "labelSSHUserAsteric";
            this.labelSSHUserAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelSSHUserAsteric.TabIndex = 46;
            this.labelSSHUserAsteric.Text = "*";
            // 
            // labelSSHPortNo
            // 
            this.labelSSHPortNo.AutoSize = true;
            this.labelSSHPortNo.Location = new System.Drawing.Point(23, 52);
            this.labelSSHPortNo.Name = "labelSSHPortNo";
            this.labelSSHPortNo.Size = new System.Drawing.Size(155, 13);
            this.labelSSHPortNo.TabIndex = 37;
            this.labelSSHPortNo.Text = "SSH Port No (Default: 22)";
            // 
            // labelSSHPortAsteric
            // 
            this.labelSSHPortAsteric.AutoSize = true;
            this.labelSSHPortAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelSSHPortAsteric.Location = new System.Drawing.Point(400, 53);
            this.labelSSHPortAsteric.Name = "labelSSHPortAsteric";
            this.labelSSHPortAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelSSHPortAsteric.TabIndex = 45;
            this.labelSSHPortAsteric.Text = "*";
            // 
            // labelSSHUserName
            // 
            this.labelSSHUserName.AutoSize = true;
            this.labelSSHUserName.Location = new System.Drawing.Point(80, 90);
            this.labelSSHUserName.Name = "labelSSHUserName";
            this.labelSSHUserName.Size = new System.Drawing.Size(99, 13);
            this.labelSSHUserName.TabIndex = 38;
            this.labelSSHUserName.Text = "SSH UserName:";
            // 
            // labelSSHHostnameAsteric
            // 
            this.labelSSHHostnameAsteric.AutoSize = true;
            this.labelSSHHostnameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelSSHHostnameAsteric.Location = new System.Drawing.Point(400, 16);
            this.labelSSHHostnameAsteric.Name = "labelSSHHostnameAsteric";
            this.labelSSHHostnameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelSSHHostnameAsteric.TabIndex = 44;
            this.labelSSHHostnameAsteric.Text = "*";
            // 
            // labelSSHPassword
            // 
            this.labelSSHPassword.AutoSize = true;
            this.labelSSHPassword.Location = new System.Drawing.Point(84, 126);
            this.labelSSHPassword.Name = "labelSSHPassword";
            this.labelSSHPassword.Size = new System.Drawing.Size(94, 13);
            this.labelSSHPassword.TabIndex = 39;
            this.labelSSHPassword.Text = "SSH Password:";
            // 
            // textBoxSSHPassword
            // 
            this.textBoxSSHPassword.Location = new System.Drawing.Point(196, 124);
            this.textBoxSSHPassword.Name = "textBoxSSHPassword";
            this.textBoxSSHPassword.PasswordChar = '*';
            this.textBoxSSHPassword.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPassword.TabIndex = 12;
            // 
            // textBoxSSHPortNo
            // 
            this.textBoxSSHPortNo.Location = new System.Drawing.Point(196, 49);
            this.textBoxSSHPortNo.Name = "textBoxSSHPortNo";
            this.textBoxSSHPortNo.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPortNo.TabIndex = 10;
            // 
            // textBoxSSHUserName
            // 
            this.textBoxSSHUserName.Location = new System.Drawing.Point(196, 87);
            this.textBoxSSHUserName.Name = "textBoxSSHUserName";
            this.textBoxSSHUserName.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHUserName.TabIndex = 11;
            // 
            // checkBoxSSHTunnel
            // 
            this.checkBoxSSHTunnel.AutoSize = true;
            this.checkBoxSSHTunnel.Location = new System.Drawing.Point(226, 257);
            this.checkBoxSSHTunnel.Name = "checkBoxSSHTunnel";
            this.checkBoxSSHTunnel.Size = new System.Drawing.Size(117, 17);
            this.checkBoxSSHTunnel.TabIndex = 8;
            this.checkBoxSSHTunnel.Text = "Use SSH Tunnel";
            this.checkBoxSSHTunnel.UseVisualStyleBackColor = true;
            this.checkBoxSSHTunnel.CheckedChanged += new System.EventHandler(this.checkBoxSSHTunnel_CheckedChanged);
            // 
            // buttonSSHConnection
            // 
            this.buttonSSHConnection.Location = new System.Drawing.Point(3, 459);
            this.buttonSSHConnection.Name = "buttonSSHConnection";
            this.buttonSSHConnection.Size = new System.Drawing.Size(143, 23);
            this.buttonSSHConnection.TabIndex = 14;
            this.buttonSSHConnection.Text = "Test Connection";
            this.buttonSSHConnection.UseVisualStyleBackColor = true;
            this.buttonSSHConnection.Click += new System.EventHandler(this.buttonSSHConnection_Click);
            // 
            // labelSSHMark
            // 
            this.labelSSHMark.AutoSize = true;
            this.labelSSHMark.ForeColor = System.Drawing.Color.Red;
            this.labelSSHMark.Location = new System.Drawing.Point(431, 43);
            this.labelSSHMark.Name = "labelSSHMark";
            this.labelSSHMark.Size = new System.Drawing.Size(14, 13);
            this.labelSSHMark.TabIndex = 31;
            this.labelSSHMark.Text = "*";
            // 
            // textBoxDBPortNo
            // 
            this.textBoxDBPortNo.Location = new System.Drawing.Point(226, 42);
            this.textBoxDBPortNo.Name = "textBoxDBPortNo";
            this.textBoxDBPortNo.Size = new System.Drawing.Size(199, 21);
            this.textBoxDBPortNo.TabIndex = 3;
            // 
            // labelPortno
            // 
            this.labelPortno.AutoSize = true;
            this.labelPortno.Location = new System.Drawing.Point(154, 46);
            this.labelPortno.Name = "labelPortno";
            this.labelPortno.Size = new System.Drawing.Size(54, 13);
            this.labelPortno.TabIndex = 29;
            this.labelPortno.Text = "Port No:";
            // 
            // dateTimePickerOSCommerceLastDate
            // 
            this.dateTimePickerOSCommerceLastDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerOSCommerceLastDate.Location = new System.Drawing.Point(226, 434);
            this.dateTimePickerOSCommerceLastDate.Name = "dateTimePickerOSCommerceLastDate";
            this.dateTimePickerOSCommerceLastDate.Size = new System.Drawing.Size(112, 21);
            this.dateTimePickerOSCommerceLastDate.TabIndex = 13;
            // 
            // labelOSClastdate
            // 
            this.labelOSClastdate.AutoSize = true;
            this.labelOSClastdate.Location = new System.Drawing.Point(54, 440);
            this.labelOSClastdate.Name = "labelOSClastdate";
            this.labelOSClastdate.Size = new System.Drawing.Size(156, 13);
            this.labelOSClastdate.TabIndex = 27;
            this.labelOSClastdate.Text = "Select Last Updated Date:";
            // 
            // comboBoxOSCShippingItem
            // 
            this.comboBoxOSCShippingItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOSCShippingItem.FormattingEnabled = true;
            this.comboBoxOSCShippingItem.Location = new System.Drawing.Point(225, 226);
            this.comboBoxOSCShippingItem.Name = "comboBoxOSCShippingItem";
            this.comboBoxOSCShippingItem.Size = new System.Drawing.Size(298, 21);
            this.comboBoxOSCShippingItem.TabIndex = 7;
            // 
            // labelOSCShippingItem
            // 
            this.labelOSCShippingItem.AutoSize = true;
            this.labelOSCShippingItem.Location = new System.Drawing.Point(78, 230);
            this.labelOSCShippingItem.Name = "labelOSCShippingItem";
            this.labelOSCShippingItem.Size = new System.Drawing.Size(131, 13);
            this.labelOSCShippingItem.TabIndex = 24;
            this.labelOSCShippingItem.Text = "Select Shipping Item:\r\n";
            // 
            // labelDatabaseNameAsteric
            // 
            this.labelDatabaseNameAsteric.AutoSize = true;
            this.labelDatabaseNameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelDatabaseNameAsteric.Location = new System.Drawing.Point(431, 151);
            this.labelDatabaseNameAsteric.Name = "labelDatabaseNameAsteric";
            this.labelDatabaseNameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelDatabaseNameAsteric.TabIndex = 23;
            this.labelDatabaseNameAsteric.Text = "*";
            // 
            // textBoxDatabaseName
            // 
            this.textBoxDatabaseName.Location = new System.Drawing.Point(224, 150);
            this.textBoxDatabaseName.MaxLength = 20;
            this.textBoxDatabaseName.Name = "textBoxDatabaseName";
            this.textBoxDatabaseName.Size = new System.Drawing.Size(200, 21);
            this.textBoxDatabaseName.TabIndex = 6;
            // 
            // labelDatabaseName
            // 
            this.labelDatabaseName.AutoSize = true;
            this.labelDatabaseName.Location = new System.Drawing.Point(105, 154);
            this.labelDatabaseName.Name = "labelDatabaseName";
            this.labelDatabaseName.Size = new System.Drawing.Size(103, 13);
            this.labelDatabaseName.TabIndex = 21;
            this.labelDatabaseName.Text = "Database Name:";
            // 
            // textBoxOSCPassword
            // 
            this.textBoxOSCPassword.Location = new System.Drawing.Point(225, 113);
            this.textBoxOSCPassword.MaxLength = 20;
            this.textBoxOSCPassword.Name = "textBoxOSCPassword";
            this.textBoxOSCPassword.PasswordChar = '*';
            this.textBoxOSCPassword.Size = new System.Drawing.Size(200, 21);
            this.textBoxOSCPassword.TabIndex = 5;
            // 
            // labelOSCPassword
            // 
            this.labelOSCPassword.AutoSize = true;
            this.labelOSCPassword.Location = new System.Drawing.Point(59, 120);
            this.labelOSCPassword.Name = "labelOSCPassword";
            this.labelOSCPassword.Size = new System.Drawing.Size(150, 13);
            this.labelOSCPassword.TabIndex = 19;
            this.labelOSCPassword.Text = "OSCommerce Password:";
            // 
            // labelOSCUsernameAsteric
            // 
            this.labelOSCUsernameAsteric.AutoSize = true;
            this.labelOSCUsernameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelOSCUsernameAsteric.Location = new System.Drawing.Point(431, 81);
            this.labelOSCUsernameAsteric.Name = "labelOSCUsernameAsteric";
            this.labelOSCUsernameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelOSCUsernameAsteric.TabIndex = 18;
            this.labelOSCUsernameAsteric.Text = "*";
            // 
            // labelOSCommerceWebStoreURL
            // 
            this.labelOSCommerceWebStoreURL.AutoSize = true;
            this.labelOSCommerceWebStoreURL.Location = new System.Drawing.Point(27, 13);
            this.labelOSCommerceWebStoreURL.Name = "labelOSCommerceWebStoreURL";
            this.labelOSCommerceWebStoreURL.Size = new System.Drawing.Size(182, 13);
            this.labelOSCommerceWebStoreURL.TabIndex = 1;
            this.labelOSCommerceWebStoreURL.Text = "OSCommerce Web Store URL:\r\n";
            // 
            // textBoxWebStoreURL
            // 
            this.textBoxWebStoreURL.Location = new System.Drawing.Point(226, 7);
            this.textBoxWebStoreURL.MaxLength = 30;
            this.textBoxWebStoreURL.Name = "textBoxWebStoreURL";
            this.textBoxWebStoreURL.Size = new System.Drawing.Size(200, 21);
            this.textBoxWebStoreURL.TabIndex = 2;
            // 
            // labelWebStoreURLAsteric
            // 
            this.labelWebStoreURLAsteric.AutoSize = true;
            this.labelWebStoreURLAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelWebStoreURLAsteric.Location = new System.Drawing.Point(432, 10);
            this.labelWebStoreURLAsteric.Name = "labelWebStoreURLAsteric";
            this.labelWebStoreURLAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelWebStoreURLAsteric.TabIndex = 17;
            this.labelWebStoreURLAsteric.Text = "*";
            // 
            // labelOSCUsername
            // 
            this.labelOSCUsername.AutoSize = true;
            this.labelOSCUsername.Location = new System.Drawing.Point(54, 81);
            this.labelOSCUsername.Name = "labelOSCUsername";
            this.labelOSCUsername.Size = new System.Drawing.Size(154, 13);
            this.labelOSCUsername.TabIndex = 3;
            this.labelOSCUsername.Text = "OSCommerce Username:";
            // 
            // textBoxOSCUsername
            // 
            this.textBoxOSCUsername.Location = new System.Drawing.Point(225, 78);
            this.textBoxOSCUsername.MaxLength = 20;
            this.textBoxOSCUsername.Name = "textBoxOSCUsername";
            this.textBoxOSCUsername.Size = new System.Drawing.Size(200, 21);
            this.textBoxOSCUsername.TabIndex = 4;
            // 
            // panelXCart
            // 
            this.panelXCart.Controls.Add(this.panelSSHXcart);
            this.panelXCart.Controls.Add(this.checkBoxXCartSSHTunnel);
            this.panelXCart.Controls.Add(this.dateTimePickerXCartLastDate);
            this.panelXCart.Controls.Add(this.labelXCartLastDate);
            this.panelXCart.Controls.Add(this.comboBoxXCartShippingItem);
            this.panelXCart.Controls.Add(this.labelXCartShippingItem);
            this.panelXCart.Controls.Add(this.labelXCartPrefExample);
            this.panelXCart.Controls.Add(this.textBoxXCartPrefix);
            this.panelXCart.Controls.Add(this.labelXCartPrefix);
            this.panelXCart.Controls.Add(this.buttonXTestConnection);
            this.panelXCart.Controls.Add(this.labelXCartDBAsterisk);
            this.panelXCart.Controls.Add(this.textBoxXDBName);
            this.panelXCart.Controls.Add(this.labelXCartDBName);
            this.panelXCart.Controls.Add(this.labelXPAsterisk);
            this.panelXCart.Controls.Add(this.textBoxXCartPort);
            this.panelXCart.Controls.Add(this.labelXPort);
            this.panelXCart.Controls.Add(this.labelXUserAsterisk);
            this.panelXCart.Controls.Add(this.textBoxXCartUserName);
            this.panelXCart.Controls.Add(this.labelXCartUserName);
            this.panelXCart.Controls.Add(this.labelXPwdAsterisk);
            this.panelXCart.Controls.Add(this.textBoxXCartPwd);
            this.panelXCart.Controls.Add(this.labelXCartPwd);
            this.panelXCart.Controls.Add(this.labelXURLAsterisk);
            this.panelXCart.Controls.Add(this.textBoxXCartUrl);
            this.panelXCart.Controls.Add(this.labelXCartUrl);
            this.panelXCart.Location = new System.Drawing.Point(24, 113);
            this.panelXCart.Name = "panelXCart";
            this.panelXCart.Size = new System.Drawing.Size(566, 453);
            this.panelXCart.TabIndex = 22;
            this.panelXCart.Visible = false;
            // 
            // panelSSHXcart
            // 
            this.panelSSHXcart.Controls.Add(this.textBoxSSHHostnameXcart);
            this.panelSSHXcart.Controls.Add(this.label30);
            this.panelSSHXcart.Controls.Add(this.labelSSHHostnameXcart);
            this.panelSSHXcart.Controls.Add(this.label32);
            this.panelSSHXcart.Controls.Add(this.labelSSHPortnoXcart);
            this.panelSSHXcart.Controls.Add(this.label34);
            this.panelSSHXcart.Controls.Add(this.labelSSHUserNameXcart);
            this.panelSSHXcart.Controls.Add(this.label36);
            this.panelSSHXcart.Controls.Add(this.labelSSHPasswordXcart);
            this.panelSSHXcart.Controls.Add(this.textBoxSSHPasswordXcart);
            this.panelSSHXcart.Controls.Add(this.textBoxSSHPortnoXcart);
            this.panelSSHXcart.Controls.Add(this.textBoxSSHUserNameXcart);
            this.panelSSHXcart.Location = new System.Drawing.Point(61, 256);
            this.panelSSHXcart.Name = "panelSSHXcart";
            this.panelSSHXcart.Size = new System.Drawing.Size(493, 134);
            this.panelSSHXcart.TabIndex = 54;
            this.panelSSHXcart.Visible = false;
            // 
            // textBoxSSHHostnameXcart
            // 
            this.textBoxSSHHostnameXcart.Location = new System.Drawing.Point(196, 4);
            this.textBoxSSHHostnameXcart.Name = "textBoxSSHHostnameXcart";
            this.textBoxSSHHostnameXcart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHHostnameXcart.TabIndex = 9;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(400, 95);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 13);
            this.label30.TabIndex = 47;
            this.label30.Text = "*";
            // 
            // labelSSHHostnameXcart
            // 
            this.labelSSHHostnameXcart.AutoSize = true;
            this.labelSSHHostnameXcart.Location = new System.Drawing.Point(11, 7);
            this.labelSSHHostnameXcart.Name = "labelSSHHostnameXcart";
            this.labelSSHHostnameXcart.Size = new System.Drawing.Size(167, 13);
            this.labelSSHHostnameXcart.TabIndex = 36;
            this.labelSSHHostnameXcart.Text = "SSH Hostname/ IP address:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(400, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 46;
            this.label32.Text = "*";
            // 
            // labelSSHPortnoXcart
            // 
            this.labelSSHPortnoXcart.AutoSize = true;
            this.labelSSHPortnoXcart.Location = new System.Drawing.Point(23, 34);
            this.labelSSHPortnoXcart.Name = "labelSSHPortnoXcart";
            this.labelSSHPortnoXcart.Size = new System.Drawing.Size(155, 13);
            this.labelSSHPortnoXcart.TabIndex = 37;
            this.labelSSHPortnoXcart.Text = "SSH Port No (Default: 22)";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(400, 35);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(14, 13);
            this.label34.TabIndex = 45;
            this.label34.Text = "*";
            // 
            // labelSSHUserNameXcart
            // 
            this.labelSSHUserNameXcart.AutoSize = true;
            this.labelSSHUserNameXcart.Location = new System.Drawing.Point(80, 63);
            this.labelSSHUserNameXcart.Name = "labelSSHUserNameXcart";
            this.labelSSHUserNameXcart.Size = new System.Drawing.Size(99, 13);
            this.labelSSHUserNameXcart.TabIndex = 38;
            this.labelSSHUserNameXcart.Text = "SSH UserName:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(400, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(14, 13);
            this.label36.TabIndex = 44;
            this.label36.Text = "*";
            // 
            // labelSSHPasswordXcart
            // 
            this.labelSSHPasswordXcart.AutoSize = true;
            this.labelSSHPasswordXcart.Location = new System.Drawing.Point(84, 94);
            this.labelSSHPasswordXcart.Name = "labelSSHPasswordXcart";
            this.labelSSHPasswordXcart.Size = new System.Drawing.Size(94, 13);
            this.labelSSHPasswordXcart.TabIndex = 39;
            this.labelSSHPasswordXcart.Text = "SSH Password:";
            // 
            // textBoxSSHPasswordXcart
            // 
            this.textBoxSSHPasswordXcart.Location = new System.Drawing.Point(196, 92);
            this.textBoxSSHPasswordXcart.Name = "textBoxSSHPasswordXcart";
            this.textBoxSSHPasswordXcart.PasswordChar = '*';
            this.textBoxSSHPasswordXcart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPasswordXcart.TabIndex = 12;
            // 
            // textBoxSSHPortnoXcart
            // 
            this.textBoxSSHPortnoXcart.Location = new System.Drawing.Point(196, 31);
            this.textBoxSSHPortnoXcart.Name = "textBoxSSHPortnoXcart";
            this.textBoxSSHPortnoXcart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHPortnoXcart.TabIndex = 10;
            // 
            // textBoxSSHUserNameXcart
            // 
            this.textBoxSSHUserNameXcart.Location = new System.Drawing.Point(196, 60);
            this.textBoxSSHUserNameXcart.Name = "textBoxSSHUserNameXcart";
            this.textBoxSSHUserNameXcart.Size = new System.Drawing.Size(200, 21);
            this.textBoxSSHUserNameXcart.TabIndex = 11;
            // 
            // checkBoxXCartSSHTunnel
            // 
            this.checkBoxXCartSSHTunnel.AutoSize = true;
            this.checkBoxXCartSSHTunnel.Location = new System.Drawing.Point(257, 239);
            this.checkBoxXCartSSHTunnel.Name = "checkBoxXCartSSHTunnel";
            this.checkBoxXCartSSHTunnel.Size = new System.Drawing.Size(117, 17);
            this.checkBoxXCartSSHTunnel.TabIndex = 50;
            this.checkBoxXCartSSHTunnel.Text = "Use SSH Tunnel";
            this.checkBoxXCartSSHTunnel.UseVisualStyleBackColor = true;
            this.checkBoxXCartSSHTunnel.CheckedChanged += new System.EventHandler(this.checkBoxXCartSSHTunnel_CheckedChanged);
            // 
            // dateTimePickerXCartLastDate
            // 
            this.dateTimePickerXCartLastDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerXCartLastDate.Location = new System.Drawing.Point(257, 395);
            this.dateTimePickerXCartLastDate.Name = "dateTimePickerXCartLastDate";
            this.dateTimePickerXCartLastDate.Size = new System.Drawing.Size(112, 21);
            this.dateTimePickerXCartLastDate.TabIndex = 51;
            // 
            // labelXCartLastDate
            // 
            this.labelXCartLastDate.AutoSize = true;
            this.labelXCartLastDate.Location = new System.Drawing.Point(85, 401);
            this.labelXCartLastDate.Name = "labelXCartLastDate";
            this.labelXCartLastDate.Size = new System.Drawing.Size(156, 13);
            this.labelXCartLastDate.TabIndex = 53;
            this.labelXCartLastDate.Text = "Select Last Updated Date:";
            // 
            // comboBoxXCartShippingItem
            // 
            this.comboBoxXCartShippingItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxXCartShippingItem.FormattingEnabled = true;
            this.comboBoxXCartShippingItem.Location = new System.Drawing.Point(256, 208);
            this.comboBoxXCartShippingItem.Name = "comboBoxXCartShippingItem";
            this.comboBoxXCartShippingItem.Size = new System.Drawing.Size(298, 21);
            this.comboBoxXCartShippingItem.TabIndex = 49;
            // 
            // labelXCartShippingItem
            // 
            this.labelXCartShippingItem.AutoSize = true;
            this.labelXCartShippingItem.Location = new System.Drawing.Point(109, 212);
            this.labelXCartShippingItem.Name = "labelXCartShippingItem";
            this.labelXCartShippingItem.Size = new System.Drawing.Size(131, 13);
            this.labelXCartShippingItem.TabIndex = 52;
            this.labelXCartShippingItem.Text = "Select Shipping Item:\r\n";
            // 
            // labelXCartPrefExample
            // 
            this.labelXCartPrefExample.AutoSize = true;
            this.labelXCartPrefExample.Location = new System.Drawing.Point(258, 189);
            this.labelXCartPrefExample.Name = "labelXCartPrefExample";
            this.labelXCartPrefExample.Size = new System.Drawing.Size(112, 13);
            this.labelXCartPrefExample.TabIndex = 32;
            this.labelXCartPrefExample.Text = "(For eg : \"xcart_\")";
            // 
            // textBoxXCartPrefix
            // 
            this.textBoxXCartPrefix.Location = new System.Drawing.Point(258, 161);
            this.textBoxXCartPrefix.Name = "textBoxXCartPrefix";
            this.textBoxXCartPrefix.Size = new System.Drawing.Size(208, 21);
            this.textBoxXCartPrefix.TabIndex = 7;
            // 
            // labelXCartPrefix
            // 
            this.labelXCartPrefix.AutoSize = true;
            this.labelXCartPrefix.Location = new System.Drawing.Point(116, 165);
            this.labelXCartPrefix.Name = "labelXCartPrefix";
            this.labelXCartPrefix.Size = new System.Drawing.Size(122, 13);
            this.labelXCartPrefix.TabIndex = 30;
            this.labelXCartPrefix.Text = "X-Cart Table Prefix:";
            // 
            // buttonXTestConnection
            // 
            this.buttonXTestConnection.Location = new System.Drawing.Point(74, 423);
            this.buttonXTestConnection.Name = "buttonXTestConnection";
            this.buttonXTestConnection.Size = new System.Drawing.Size(136, 23);
            this.buttonXTestConnection.TabIndex = 8;
            this.buttonXTestConnection.Text = "Test Connection";
            this.buttonXTestConnection.UseVisualStyleBackColor = true;
            this.buttonXTestConnection.Click += new System.EventHandler(this.buttonXTestConnection_Click);
            // 
            // labelXCartDBAsterisk
            // 
            this.labelXCartDBAsterisk.AutoSize = true;
            this.labelXCartDBAsterisk.ForeColor = System.Drawing.Color.Red;
            this.labelXCartDBAsterisk.Location = new System.Drawing.Point(471, 134);
            this.labelXCartDBAsterisk.Name = "labelXCartDBAsterisk";
            this.labelXCartDBAsterisk.Size = new System.Drawing.Size(14, 13);
            this.labelXCartDBAsterisk.TabIndex = 29;
            this.labelXCartDBAsterisk.Text = "*";
            // 
            // textBoxXDBName
            // 
            this.textBoxXDBName.Location = new System.Drawing.Point(258, 128);
            this.textBoxXDBName.MaxLength = 100;
            this.textBoxXDBName.Name = "textBoxXDBName";
            this.textBoxXDBName.Size = new System.Drawing.Size(210, 21);
            this.textBoxXDBName.TabIndex = 6;
            // 
            // labelXCartDBName
            // 
            this.labelXCartDBName.AutoSize = true;
            this.labelXCartDBName.Location = new System.Drawing.Point(93, 134);
            this.labelXCartDBName.Name = "labelXCartDBName";
            this.labelXCartDBName.Size = new System.Drawing.Size(145, 13);
            this.labelXCartDBName.TabIndex = 27;
            this.labelXCartDBName.Text = "X-Cart Database Name:";
            // 
            // labelXPAsterisk
            // 
            this.labelXPAsterisk.AutoSize = true;
            this.labelXPAsterisk.ForeColor = System.Drawing.Color.Red;
            this.labelXPAsterisk.Location = new System.Drawing.Point(473, 41);
            this.labelXPAsterisk.Name = "labelXPAsterisk";
            this.labelXPAsterisk.Size = new System.Drawing.Size(14, 13);
            this.labelXPAsterisk.TabIndex = 26;
            this.labelXPAsterisk.Text = "*";
            // 
            // textBoxXCartPort
            // 
            this.textBoxXCartPort.Location = new System.Drawing.Point(257, 35);
            this.textBoxXCartPort.MaxLength = 100;
            this.textBoxXCartPort.Name = "textBoxXCartPort";
            this.textBoxXCartPort.Size = new System.Drawing.Size(210, 21);
            this.textBoxXCartPort.TabIndex = 3;
            // 
            // labelXPort
            // 
            this.labelXPort.AutoSize = true;
            this.labelXPort.Location = new System.Drawing.Point(185, 38);
            this.labelXPort.Name = "labelXPort";
            this.labelXPort.Size = new System.Drawing.Size(54, 13);
            this.labelXPort.TabIndex = 24;
            this.labelXPort.Text = "Port No:";
            // 
            // labelXUserAsterisk
            // 
            this.labelXUserAsterisk.AutoSize = true;
            this.labelXUserAsterisk.ForeColor = System.Drawing.Color.Red;
            this.labelXUserAsterisk.Location = new System.Drawing.Point(473, 70);
            this.labelXUserAsterisk.Name = "labelXUserAsterisk";
            this.labelXUserAsterisk.Size = new System.Drawing.Size(14, 13);
            this.labelXUserAsterisk.TabIndex = 23;
            this.labelXUserAsterisk.Text = "*";
            // 
            // textBoxXCartUserName
            // 
            this.textBoxXCartUserName.Location = new System.Drawing.Point(257, 64);
            this.textBoxXCartUserName.MaxLength = 100;
            this.textBoxXCartUserName.Name = "textBoxXCartUserName";
            this.textBoxXCartUserName.Size = new System.Drawing.Size(210, 21);
            this.textBoxXCartUserName.TabIndex = 4;
            // 
            // labelXCartUserName
            // 
            this.labelXCartUserName.AutoSize = true;
            this.labelXCartUserName.Location = new System.Drawing.Point(128, 67);
            this.labelXCartUserName.Name = "labelXCartUserName";
            this.labelXCartUserName.Size = new System.Drawing.Size(112, 13);
            this.labelXCartUserName.TabIndex = 21;
            this.labelXCartUserName.Text = "X-Cart Username:";
            // 
            // labelXPwdAsterisk
            // 
            this.labelXPwdAsterisk.AutoSize = true;
            this.labelXPwdAsterisk.ForeColor = System.Drawing.Color.Red;
            this.labelXPwdAsterisk.Location = new System.Drawing.Point(471, 102);
            this.labelXPwdAsterisk.Name = "labelXPwdAsterisk";
            this.labelXPwdAsterisk.Size = new System.Drawing.Size(14, 13);
            this.labelXPwdAsterisk.TabIndex = 20;
            this.labelXPwdAsterisk.Text = "*";
            // 
            // textBoxXCartPwd
            // 
            this.textBoxXCartPwd.Location = new System.Drawing.Point(258, 96);
            this.textBoxXCartPwd.MaxLength = 100;
            this.textBoxXCartPwd.Name = "textBoxXCartPwd";
            this.textBoxXCartPwd.PasswordChar = '*';
            this.textBoxXCartPwd.Size = new System.Drawing.Size(210, 21);
            this.textBoxXCartPwd.TabIndex = 5;
            // 
            // labelXCartPwd
            // 
            this.labelXCartPwd.AutoSize = true;
            this.labelXCartPwd.Location = new System.Drawing.Point(131, 100);
            this.labelXCartPwd.Name = "labelXCartPwd";
            this.labelXCartPwd.Size = new System.Drawing.Size(108, 13);
            this.labelXCartPwd.TabIndex = 18;
            this.labelXCartPwd.Text = "X-Cart Password:";
            // 
            // labelXURLAsterisk
            // 
            this.labelXURLAsterisk.AutoSize = true;
            this.labelXURLAsterisk.ForeColor = System.Drawing.Color.Red;
            this.labelXURLAsterisk.Location = new System.Drawing.Point(473, 12);
            this.labelXURLAsterisk.Name = "labelXURLAsterisk";
            this.labelXURLAsterisk.Size = new System.Drawing.Size(14, 13);
            this.labelXURLAsterisk.TabIndex = 17;
            this.labelXURLAsterisk.Text = "*";
            // 
            // textBoxXCartUrl
            // 
            this.textBoxXCartUrl.Location = new System.Drawing.Point(257, 6);
            this.textBoxXCartUrl.MaxLength = 100;
            this.textBoxXCartUrl.Name = "textBoxXCartUrl";
            this.textBoxXCartUrl.Size = new System.Drawing.Size(210, 21);
            this.textBoxXCartUrl.TabIndex = 2;
            // 
            // labelXCartUrl
            // 
            this.labelXCartUrl.AutoSize = true;
            this.labelXCartUrl.Location = new System.Drawing.Point(101, 9);
            this.labelXCartUrl.Name = "labelXCartUrl";
            this.labelXCartUrl.Size = new System.Drawing.Size(140, 13);
            this.labelXCartUrl.TabIndex = 0;
            this.labelXCartUrl.Text = "X-Cart Web Store URL:";
            // 
            // panelPOP3
            // 
            this.panelPOP3.Controls.Add(this.labelEmailAddress);
            this.panelPOP3.Controls.Add(this.textBoxTPEmailAddress);
            this.panelPOP3.Controls.Add(this.textBoxTPClientID);
            this.panelPOP3.Controls.Add(this.labelEmailAddressAsteric);
            this.panelPOP3.Controls.Add(this.labelXSDSchema);
            this.panelPOP3.Controls.Add(this.buttonTPSchemaBrowse);
            this.panelPOP3.Controls.Add(this.labelAlterEmailAddress);
            this.panelPOP3.Controls.Add(this.textBoxTPAlternateEmail);
            this.panelPOP3.Controls.Add(this.labelClientID);
            this.panelPOP3.Controls.Add(this.textBoxTPSchemaLoc);
            this.panelPOP3.Location = new System.Drawing.Point(54, 118);
            this.panelPOP3.Name = "panelPOP3";
            this.panelPOP3.Size = new System.Drawing.Size(557, 212);
            this.panelPOP3.TabIndex = 18;
            this.panelPOP3.Visible = false;
            // 
            // panelAlliedExpress
            // 
            this.panelAlliedExpress.Controls.Add(this.groupBox1);
            this.panelAlliedExpress.Controls.Add(this.label5);
            this.panelAlliedExpress.Controls.Add(this.comboBoxAlliedExpressType);
            this.panelAlliedExpress.Controls.Add(this.labelAlledExpressType);
            this.panelAlliedExpress.Controls.Add(this.label2);
            this.panelAlliedExpress.Controls.Add(this.label3);
            this.panelAlliedExpress.Controls.Add(this.textBoxPrefix);
            this.panelAlliedExpress.Controls.Add(this.labelPrefix);
            this.panelAlliedExpress.Controls.Add(this.labelAlliedExpressFromPhone);
            this.panelAlliedExpress.Controls.Add(this.label1);
            this.panelAlliedExpress.Controls.Add(this.textBoxCompanyPhone);
            this.panelAlliedExpress.Controls.Add(this.labelAECompanyNameAsteric);
            this.panelAlliedExpress.Controls.Add(this.labelAlliedExpressCompanyName);
            this.panelAlliedExpress.Controls.Add(this.textBoxAlliedExpressCompanyName);
            this.panelAlliedExpress.Location = new System.Drawing.Point(32, 83);
            this.panelAlliedExpress.Name = "panelAlliedExpress";
            this.panelAlliedExpress.Size = new System.Drawing.Size(567, 469);
            this.panelAlliedExpress.TabIndex = 21;
            this.panelAlliedExpress.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxState);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxAccountCode);
            this.groupBox1.Controls.Add(this.textBoxAccountNumber);
            this.groupBox1.Controls.Add(this.labelAlliedExpressAccountNumber);
            this.groupBox1.Controls.Add(this.labelAlliedExpressAccountNumberAsteric);
            this.groupBox1.Location = new System.Drawing.Point(18, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(490, 158);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Details";
            // 
            // comboBoxState
            // 
            this.comboBoxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxState.FormattingEnabled = true;
            this.comboBoxState.Location = new System.Drawing.Point(239, 112);
            this.comboBoxState.Name = "comboBoxState";
            this.comboBoxState.Size = new System.Drawing.Size(201, 21);
            this.comboBoxState.TabIndex = 31;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(175, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Allied Express Account Code:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(447, 37);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(446, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(14, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(82, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Allied Express State:";
            // 
            // textBoxAccountCode
            // 
            this.textBoxAccountCode.Location = new System.Drawing.Point(241, 73);
            this.textBoxAccountCode.MaxLength = 15;
            this.textBoxAccountCode.Name = "textBoxAccountCode";
            this.textBoxAccountCode.Size = new System.Drawing.Size(200, 21);
            this.textBoxAccountCode.TabIndex = 29;
            // 
            // textBoxAccountNumber
            // 
            this.textBoxAccountNumber.Location = new System.Drawing.Point(240, 29);
            this.textBoxAccountNumber.MaxLength = 32;
            this.textBoxAccountNumber.Name = "textBoxAccountNumber";
            this.textBoxAccountNumber.Size = new System.Drawing.Size(200, 21);
            this.textBoxAccountNumber.TabIndex = 2;
            // 
            // labelAlliedExpressAccountNumber
            // 
            this.labelAlliedExpressAccountNumber.AutoSize = true;
            this.labelAlliedExpressAccountNumber.Location = new System.Drawing.Point(24, 32);
            this.labelAlliedExpressAccountNumber.Name = "labelAlliedExpressAccountNumber";
            this.labelAlliedExpressAccountNumber.Size = new System.Drawing.Size(190, 13);
            this.labelAlliedExpressAccountNumber.TabIndex = 1;
            this.labelAlliedExpressAccountNumber.Text = "Allied Express Account Number:";
            // 
            // labelAlliedExpressAccountNumberAsteric
            // 
            this.labelAlliedExpressAccountNumberAsteric.AutoSize = true;
            this.labelAlliedExpressAccountNumberAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelAlliedExpressAccountNumberAsteric.Location = new System.Drawing.Point(447, 115);
            this.labelAlliedExpressAccountNumberAsteric.Name = "labelAlliedExpressAccountNumberAsteric";
            this.labelAlliedExpressAccountNumberAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelAlliedExpressAccountNumberAsteric.TabIndex = 17;
            this.labelAlliedExpressAccountNumberAsteric.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(465, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "*";
            // 
            // comboBoxAlliedExpressType
            // 
            this.comboBoxAlliedExpressType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAlliedExpressType.FormattingEnabled = true;
            this.comboBoxAlliedExpressType.Items.AddRange(new object[] {
            "Test",
            "Production"});
            this.comboBoxAlliedExpressType.Location = new System.Drawing.Point(258, 37);
            this.comboBoxAlliedExpressType.Name = "comboBoxAlliedExpressType";
            this.comboBoxAlliedExpressType.Size = new System.Drawing.Size(201, 21);
            this.comboBoxAlliedExpressType.TabIndex = 27;
            // 
            // labelAlledExpressType
            // 
            this.labelAlledExpressType.AutoSize = true;
            this.labelAlledExpressType.Location = new System.Drawing.Point(102, 41);
            this.labelAlledExpressType.Name = "labelAlledExpressType";
            this.labelAlledExpressType.Size = new System.Drawing.Size(124, 13);
            this.labelAlledExpressType.TabIndex = 26;
            this.labelAlledExpressType.Text = "Allied Express Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(103, 410);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(342, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "(Enter three Digit Prefix within the  Allied Express System)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(427, 353);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "*";
            // 
            // textBoxPrefix
            // 
            this.textBoxPrefix.Location = new System.Drawing.Point(262, 345);
            this.textBoxPrefix.MaxLength = 3;
            this.textBoxPrefix.Name = "textBoxPrefix";
            this.textBoxPrefix.Size = new System.Drawing.Size(149, 21);
            this.textBoxPrefix.TabIndex = 5;
            this.textBoxPrefix.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrefix_KeyPress);
            // 
            // labelPrefix
            // 
            this.labelPrefix.AutoSize = true;
            this.labelPrefix.Location = new System.Drawing.Point(36, 345);
            this.labelPrefix.Name = "labelPrefix";
            this.labelPrefix.Size = new System.Drawing.Size(200, 13);
            this.labelPrefix.TabIndex = 22;
            this.labelPrefix.Text = "Enter Prefix for Cannote Number:";
            // 
            // labelAlliedExpressFromPhone
            // 
            this.labelAlliedExpressFromPhone.AutoSize = true;
            this.labelAlliedExpressFromPhone.Location = new System.Drawing.Point(47, 301);
            this.labelAlliedExpressFromPhone.Name = "labelAlliedExpressFromPhone";
            this.labelAlliedExpressFromPhone.Size = new System.Drawing.Size(190, 13);
            this.labelAlliedExpressFromPhone.TabIndex = 21;
            this.labelAlliedExpressFromPhone.Text = "Allied Express Company Phone:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(464, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "*";
            // 
            // textBoxCompanyPhone
            // 
            this.textBoxCompanyPhone.Location = new System.Drawing.Point(261, 301);
            this.textBoxCompanyPhone.MaxLength = 12;
            this.textBoxCompanyPhone.Name = "textBoxCompanyPhone";
            this.textBoxCompanyPhone.Size = new System.Drawing.Size(200, 21);
            this.textBoxCompanyPhone.TabIndex = 4;
            this.textBoxCompanyPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxCompanyPhone_KeyPress);
            // 
            // labelAECompanyNameAsteric
            // 
            this.labelAECompanyNameAsteric.AutoSize = true;
            this.labelAECompanyNameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelAECompanyNameAsteric.Location = new System.Drawing.Point(467, 269);
            this.labelAECompanyNameAsteric.Name = "labelAECompanyNameAsteric";
            this.labelAECompanyNameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelAECompanyNameAsteric.TabIndex = 18;
            this.labelAECompanyNameAsteric.Text = "*";
            // 
            // labelAlliedExpressCompanyName
            // 
            this.labelAlliedExpressCompanyName.AutoSize = true;
            this.labelAlliedExpressCompanyName.Location = new System.Drawing.Point(49, 264);
            this.labelAlliedExpressCompanyName.Name = "labelAlliedExpressCompanyName";
            this.labelAlliedExpressCompanyName.Size = new System.Drawing.Size(188, 13);
            this.labelAlliedExpressCompanyName.TabIndex = 3;
            this.labelAlliedExpressCompanyName.Text = "Allied Express Company Name:";
            // 
            // textBoxAlliedExpressCompanyName
            // 
            this.textBoxAlliedExpressCompanyName.Location = new System.Drawing.Point(261, 260);
            this.textBoxAlliedExpressCompanyName.MaxLength = 25;
            this.textBoxAlliedExpressCompanyName.Name = "textBoxAlliedExpressCompanyName";
            this.textBoxAlliedExpressCompanyName.Size = new System.Drawing.Size(200, 21);
            this.textBoxAlliedExpressCompanyName.TabIndex = 3;
            // 
            // paneleBay
            // 
            this.paneleBay.Controls.Add(this.labeleBayTypeAsteric);
            this.paneleBay.Controls.Add(this.comboBoxeBayType);
            this.paneleBay.Controls.Add(this.label4);
            this.paneleBay.Controls.Add(this.labelDiscountItemsAsteric);
            this.paneleBay.Controls.Add(this.labelItemNameAsteric);
            this.paneleBay.Controls.Add(this.comboBoxDiscountItems);
            this.paneleBay.Controls.Add(this.comboBoxItemName);
            this.paneleBay.Controls.Add(this.labelDiscountItemName);
            this.paneleBay.Controls.Add(this.labelItemName);
            this.paneleBay.Controls.Add(this.labelUpdatedDateAsterick);
            this.paneleBay.Controls.Add(this.labelDateMessage);
            this.paneleBay.Controls.Add(this.dateTimePickerLastUpdated);
            this.paneleBay.Controls.Add(this.labelLastUpdatedDate);
            this.paneleBay.Controls.Add(this.buttoneBayTicket);
            this.paneleBay.Controls.Add(this.textBoxeBayAuthToken);
            this.paneleBay.Controls.Add(this.labelEbayToken);
            this.paneleBay.Controls.Add(this.buttonAuthorization);
            this.paneleBay.Location = new System.Drawing.Point(53, 106);
            this.paneleBay.Name = "paneleBay";
            this.paneleBay.Size = new System.Drawing.Size(546, 431);
            this.paneleBay.TabIndex = 19;
            this.paneleBay.Visible = false;
            // 
            // labeleBayTypeAsteric
            // 
            this.labeleBayTypeAsteric.AutoSize = true;
            this.labeleBayTypeAsteric.ForeColor = System.Drawing.Color.Red;
            this.labeleBayTypeAsteric.Location = new System.Drawing.Point(432, 18);
            this.labeleBayTypeAsteric.Name = "labeleBayTypeAsteric";
            this.labeleBayTypeAsteric.Size = new System.Drawing.Size(14, 13);
            this.labeleBayTypeAsteric.TabIndex = 39;
            this.labeleBayTypeAsteric.Text = "*";
            // 
            // comboBoxeBayType
            // 
            this.comboBoxeBayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxeBayType.FormattingEnabled = true;
            this.comboBoxeBayType.Items.AddRange(new object[] {
            "SandBox",
            "Production"});
            this.comboBoxeBayType.Location = new System.Drawing.Point(227, 15);
            this.comboBoxeBayType.Name = "comboBoxeBayType";
            this.comboBoxeBayType.Size = new System.Drawing.Size(199, 21);
            this.comboBoxeBayType.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(135, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "eBay Type:";
            // 
            // labelDiscountItemsAsteric
            // 
            this.labelDiscountItemsAsteric.AutoSize = true;
            this.labelDiscountItemsAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelDiscountItemsAsteric.Location = new System.Drawing.Point(487, 266);
            this.labelDiscountItemsAsteric.Name = "labelDiscountItemsAsteric";
            this.labelDiscountItemsAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelDiscountItemsAsteric.TabIndex = 36;
            this.labelDiscountItemsAsteric.Text = "*";
            // 
            // labelItemNameAsteric
            // 
            this.labelItemNameAsteric.AutoSize = true;
            this.labelItemNameAsteric.ForeColor = System.Drawing.Color.Red;
            this.labelItemNameAsteric.Location = new System.Drawing.Point(487, 207);
            this.labelItemNameAsteric.Name = "labelItemNameAsteric";
            this.labelItemNameAsteric.Size = new System.Drawing.Size(14, 13);
            this.labelItemNameAsteric.TabIndex = 35;
            this.labelItemNameAsteric.Text = "*";
            // 
            // comboBoxDiscountItems
            // 
            this.comboBoxDiscountItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDiscountItems.FormattingEnabled = true;
            this.comboBoxDiscountItems.Location = new System.Drawing.Point(224, 258);
            this.comboBoxDiscountItems.Name = "comboBoxDiscountItems";
            this.comboBoxDiscountItems.Size = new System.Drawing.Size(257, 21);
            this.comboBoxDiscountItems.TabIndex = 34;
            // 
            // comboBoxItemName
            // 
            this.comboBoxItemName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxItemName.FormattingEnabled = true;
            this.comboBoxItemName.Location = new System.Drawing.Point(225, 199);
            this.comboBoxItemName.Name = "comboBoxItemName";
            this.comboBoxItemName.Size = new System.Drawing.Size(256, 21);
            this.comboBoxItemName.TabIndex = 33;
            // 
            // labelDiscountItemName
            // 
            this.labelDiscountItemName.AutoSize = true;
            this.labelDiscountItemName.Location = new System.Drawing.Point(30, 261);
            this.labelDiscountItemName.Name = "labelDiscountItemName";
            this.labelDiscountItemName.Size = new System.Drawing.Size(182, 13);
            this.labelDiscountItemName.TabIndex = 32;
            this.labelDiscountItemName.Text = "Select Discount/Charge Item: ";
            // 
            // labelItemName
            // 
            this.labelItemName.AutoSize = true;
            this.labelItemName.Location = new System.Drawing.Point(18, 202);
            this.labelItemName.Name = "labelItemName";
            this.labelItemName.Size = new System.Drawing.Size(191, 13);
            this.labelItemName.TabIndex = 31;
            this.labelItemName.Text = "               Select Shipping Item:";
            // 
            // labelUpdatedDateAsterick
            // 
            this.labelUpdatedDateAsterick.AutoSize = true;
            this.labelUpdatedDateAsterick.ForeColor = System.Drawing.Color.Red;
            this.labelUpdatedDateAsterick.Location = new System.Drawing.Point(344, 322);
            this.labelUpdatedDateAsterick.Name = "labelUpdatedDateAsterick";
            this.labelUpdatedDateAsterick.Size = new System.Drawing.Size(14, 13);
            this.labelUpdatedDateAsterick.TabIndex = 30;
            this.labelUpdatedDateAsterick.Text = "*";
            // 
            // labelDateMessage
            // 
            this.labelDateMessage.AutoSize = true;
            this.labelDateMessage.ForeColor = System.Drawing.Color.Red;
            this.labelDateMessage.Location = new System.Drawing.Point(55, 386);
            this.labelDateMessage.Name = "labelDateMessage";
            this.labelDateMessage.Size = new System.Drawing.Size(443, 13);
            this.labelDateMessage.TabIndex = 28;
            this.labelDateMessage.Text = "Please do not select 30 days before last updated date for getting eBay data.";
            // 
            // dateTimePickerLastUpdated
            // 
            this.dateTimePickerLastUpdated.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerLastUpdated.Location = new System.Drawing.Point(226, 316);
            this.dateTimePickerLastUpdated.Name = "dateTimePickerLastUpdated";
            this.dateTimePickerLastUpdated.Size = new System.Drawing.Size(112, 21);
            this.dateTimePickerLastUpdated.TabIndex = 8;
            // 
            // labelLastUpdatedDate
            // 
            this.labelLastUpdatedDate.AutoSize = true;
            this.labelLastUpdatedDate.Location = new System.Drawing.Point(53, 322);
            this.labelLastUpdatedDate.Name = "labelLastUpdatedDate";
            this.labelLastUpdatedDate.Size = new System.Drawing.Size(156, 13);
            this.labelLastUpdatedDate.TabIndex = 26;
            this.labelLastUpdatedDate.Text = "Select Last Updated Date:";
            // 
            // buttoneBayTicket
            // 
            this.buttoneBayTicket.Location = new System.Drawing.Point(264, 141);
            this.buttoneBayTicket.Name = "buttoneBayTicket";
            this.buttoneBayTicket.Size = new System.Drawing.Size(100, 23);
            this.buttoneBayTicket.TabIndex = 5;
            this.buttoneBayTicket.Text = "eBay Token";
            this.buttoneBayTicket.UseVisualStyleBackColor = true;
            this.buttoneBayTicket.Visible = false;
            this.buttoneBayTicket.Click += new System.EventHandler(this.buttoneBayTicket_Click);
            // 
            // textBoxeBayAuthToken
            // 
            this.textBoxeBayAuthToken.ForeColor = System.Drawing.Color.DarkViolet;
            this.textBoxeBayAuthToken.Location = new System.Drawing.Point(226, 116);
            this.textBoxeBayAuthToken.Multiline = true;
            this.textBoxeBayAuthToken.Name = "textBoxeBayAuthToken";
            this.textBoxeBayAuthToken.ReadOnly = true;
            this.textBoxeBayAuthToken.Size = new System.Drawing.Size(307, 60);
            this.textBoxeBayAuthToken.TabIndex = 6;
            this.textBoxeBayAuthToken.Visible = false;
            // 
            // labelEbayToken
            // 
            this.labelEbayToken.AutoSize = true;
            this.labelEbayToken.Location = new System.Drawing.Point(99, 144);
            this.labelEbayToken.Name = "labelEbayToken";
            this.labelEbayToken.Size = new System.Drawing.Size(110, 13);
            this.labelEbayToken.TabIndex = 20;
            this.labelEbayToken.Text = "eBay Auth Token:";
            this.labelEbayToken.Visible = false;
            // 
            // buttonAuthorization
            // 
            this.buttonAuthorization.Location = new System.Drawing.Point(264, 74);
            this.buttonAuthorization.Name = "buttonAuthorization";
            this.buttonAuthorization.Size = new System.Drawing.Size(100, 25);
            this.buttonAuthorization.TabIndex = 4;
            this.buttonAuthorization.Text = "&Authorisation";
            this.buttonAuthorization.UseVisualStyleBackColor = true;
            this.buttonAuthorization.Click += new System.EventHandler(this.buttonAuthorization_Click);
            // 
            // panelTPABShow
            // 
            this.panelTPABShow.Controls.Add(this.dataGridViewTPABShow);
            this.panelTPABShow.Controls.Add(this.buttonCancelForm);
            this.panelTPABShow.Controls.Add(this.buttonDelete);
            this.panelTPABShow.Controls.Add(this.buttonEdit);
            this.panelTPABShow.Controls.Add(this.buttonAddNew);
            this.panelTPABShow.Location = new System.Drawing.Point(12, 26);
            this.panelTPABShow.Name = "panelTPABShow";
            this.panelTPABShow.Size = new System.Drawing.Size(617, 396);
            this.panelTPABShow.TabIndex = 18;
            // 
            // dataGridViewTPABShow
            // 
            this.dataGridViewTPABShow.AllowUserToAddRows = false;
            this.dataGridViewTPABShow.AllowUserToDeleteRows = false;
            this.dataGridViewTPABShow.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTPABShow.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTPABShow.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewTPABShow.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewTPABShow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTPABShow.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTPABShow.MultiSelect = false;
            this.dataGridViewTPABShow.Name = "dataGridViewTPABShow";
            this.dataGridViewTPABShow.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Green;
            this.dataGridViewTPABShow.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTPABShow.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridViewTPABShow.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTPABShow.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTPABShow.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.LightCyan;
            this.dataGridViewTPABShow.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Green;
            this.dataGridViewTPABShow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTPABShow.Size = new System.Drawing.Size(610, 327);
            this.dataGridViewTPABShow.TabIndex = 12;
            this.dataGridViewTPABShow.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewTPABShow_CellMouseDoubleClick);
            // 
            // buttonCancelForm
            // 
            this.buttonCancelForm.Location = new System.Drawing.Point(412, 370);
            this.buttonCancelForm.Name = "buttonCancelForm";
            this.buttonCancelForm.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelForm.TabIndex = 4;
            this.buttonCancelForm.Text = "&Cancel";
            this.buttonCancelForm.UseVisualStyleBackColor = true;
            this.buttonCancelForm.Click += new System.EventHandler(this.buttonCancelForm_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(322, 370);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "&Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Location = new System.Drawing.Point(227, 370);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(75, 23);
            this.buttonEdit.TabIndex = 2;
            this.buttonEdit.Text = "&Edit";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAddNew
            // 
            this.buttonAddNew.Location = new System.Drawing.Point(129, 370);
            this.buttonAddNew.Name = "buttonAddNew";
            this.buttonAddNew.Size = new System.Drawing.Size(75, 23);
            this.buttonAddNew.TabIndex = 1;
            this.buttonAddNew.Text = "&Add";
            this.buttonAddNew.UseVisualStyleBackColor = true;
            this.buttonAddNew.Click += new System.EventHandler(this.buttonAddNew_Click);
            // 
            // TradingPartnerAddressBook
            // 
            this.AcceptButton = this.buttonAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(658, 631);
            this.Controls.Add(this.panelTPABSave);
            this.Controls.Add(this.panelTPABShow);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TradingPartnerAddressBook";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trading Partner Address Book";
            this.Load += new System.EventHandler(this.TradingPartnerAddressBook_Load);
            this.panelTPABSave.ResumeLayout(false);
            this.panelTPABSave.PerformLayout();
            this.panelZencart.ResumeLayout(false);
            this.panelZencart.PerformLayout();
            this.panelSSHZencart.ResumeLayout(false);
            this.panelSSHZencart.PerformLayout();
            this.panelOSCommerce.ResumeLayout(false);
            this.panelOSCommerce.PerformLayout();
            this.panelSSH.ResumeLayout(false);
            this.panelSSH.PerformLayout();
            this.panelXCart.ResumeLayout(false);
            this.panelXCart.PerformLayout();
            this.panelSSHXcart.ResumeLayout(false);
            this.panelSSHXcart.PerformLayout();
            this.panelPOP3.ResumeLayout(false);
            this.panelPOP3.PerformLayout();
            this.panelAlliedExpress.ResumeLayout(false);
            this.panelAlliedExpress.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.paneleBay.ResumeLayout(false);
            this.paneleBay.PerformLayout();
            this.panelTPABShow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTPABShow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.TextBox textBoxTPClientID;
        private System.Windows.Forms.TextBox textBoxTPEmailAddress;
        private System.Windows.Forms.TextBox textBoxTPName;
        private System.Windows.Forms.Label labelClientID;
        private System.Windows.Forms.Label labelEmailAddress;
        private System.Windows.Forms.Label labelTradingPartnerName;
        private System.Windows.Forms.Button buttonTPSchemaBrowse;
        private System.Windows.Forms.Label labelXSDSchema;
        private System.Windows.Forms.OpenFileDialog openFileDialogTPSchemaBrowse;
        private System.Windows.Forms.Label labelMailProtocol;
        private System.Windows.Forms.ComboBox comboBoxTPMailProtocol;
        private System.Windows.Forms.TextBox textBoxTPSchemaLoc;
        private System.Windows.Forms.TextBox textBoxTPAlternateEmail;
        private System.Windows.Forms.Label labelAlterEmailAddress;
        private System.Windows.Forms.Label labelTPNAsterik;
        private System.Windows.Forms.Label labelMailProtocolAsteric;
        private System.Windows.Forms.Label labelEmailAddressAsteric;
        private System.Windows.Forms.Panel panelTPABSave;
        private System.Windows.Forms.Panel panelTPABShow;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAddNew;
        private System.Windows.Forms.Button buttonCancelForm;
        private System.Windows.Forms.DataGridView dataGridViewTPABShow;
        private System.Windows.Forms.Panel panelPOP3;
        private System.Windows.Forms.Panel paneleBay;
        private System.Windows.Forms.Button buttonAuthorization;
        private System.Windows.Forms.Label labelEbayToken;
        private System.Windows.Forms.Panel panelOSCommerce;
        private System.Windows.Forms.Label labelOSCUsernameAsteric;
        private System.Windows.Forms.Label labelOSCommerceWebStoreURL;
        private System.Windows.Forms.TextBox textBoxWebStoreURL;
        private System.Windows.Forms.Label labelWebStoreURLAsteric;
        private System.Windows.Forms.Label labelOSCUsername;
        private System.Windows.Forms.TextBox textBoxOSCUsername;
        private System.Windows.Forms.TextBox textBoxOSCPassword;
        private System.Windows.Forms.Label labelOSCPassword;
        private System.Windows.Forms.TextBox textBoxDatabaseName;
        private System.Windows.Forms.Label labelDatabaseName;
        private System.Windows.Forms.Panel panelAlliedExpress;
        private System.Windows.Forms.Label labelAECompanyNameAsteric;
        private System.Windows.Forms.Label labelAlliedExpressAccountNumber;
        private System.Windows.Forms.TextBox textBoxAccountNumber;
        private System.Windows.Forms.Label labelAlliedExpressAccountNumberAsteric;
        private System.Windows.Forms.Label labelAlliedExpressCompanyName;
        private System.Windows.Forms.TextBox textBoxAlliedExpressCompanyName;
        private System.Windows.Forms.Label labelDatabaseNameAsteric;
        private System.Windows.Forms.ComboBox comboBoxOSCShippingItem;
        private System.Windows.Forms.Label labelOSCShippingItem;
        private System.Windows.Forms.TextBox textBoxeBayAuthToken;
        private System.Windows.Forms.Button buttoneBayTicket;
        private System.Windows.Forms.Label labelLastUpdatedDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerLastUpdated;
        private System.Windows.Forms.Label labelDateMessage;
        private System.Windows.Forms.Label labelUpdatedDateAsterick;
        private System.Windows.Forms.Label labelDiscountItemName;
        private System.Windows.Forms.Label labelItemName;
        private System.Windows.Forms.ComboBox comboBoxDiscountItems;
        private System.Windows.Forms.ComboBox comboBoxItemName;
        private System.Windows.Forms.Label labelItemNameAsteric;
        private System.Windows.Forms.Label labelDiscountItemsAsteric;
        private System.Windows.Forms.DateTimePicker dateTimePickerOSCommerceLastDate;
        private System.Windows.Forms.Label labelOSClastdate;
        private System.Windows.Forms.Label labelPortno;
        private System.Windows.Forms.Label labelSSHMark;
        private System.Windows.Forms.TextBox textBoxDBPortNo;
        private System.Windows.Forms.Button buttonSSHConnection;
        private System.Windows.Forms.CheckBox checkBoxSSHTunnel;
        private System.Windows.Forms.Label labelSSHPassword;
        private System.Windows.Forms.Label labelSSHUserName;
        private System.Windows.Forms.Label labelSSHPortNo;
        private System.Windows.Forms.Label labelSSHHostname;
        private System.Windows.Forms.TextBox textBoxSSHPassword;
        private System.Windows.Forms.TextBox textBoxSSHUserName;
        private System.Windows.Forms.TextBox textBoxSSHPortNo;
        private System.Windows.Forms.TextBox textBoxSSHHostName;
        private System.Windows.Forms.Label labelSSHUserAsteric;
        private System.Windows.Forms.Label labelSSHPortAsteric;
        private System.Windows.Forms.Label labelSSHHostnameAsteric;
        private System.Windows.Forms.Label labelPasswordAsteric;
        private System.Windows.Forms.Panel panelSSH;
    	private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxCompanyPhone;
        private System.Windows.Forms.Label labelAlliedExpressFromPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPrefix;
        private System.Windows.Forms.Label labelPrefix;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labeleBayTypeAsteric;
        private System.Windows.Forms.ComboBox comboBoxeBayType;
        private System.Windows.Forms.ComboBox comboBoxAlliedExpressType;
        private System.Windows.Forms.Label labelAlledExpressType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxState;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxAccountCode;
        private System.Windows.Forms.Label PrefixLabel;
        private System.Windows.Forms.TextBox textBox_Prefix;
        private System.Windows.Forms.Label labelPrefixNote;
        private System.Windows.Forms.Panel panelXCart;
        private System.Windows.Forms.Label labelXCartUrl;
        private System.Windows.Forms.Label labelXURLAsterisk;
        private System.Windows.Forms.TextBox textBoxXCartUrl;
        private System.Windows.Forms.Label labelXPAsterisk;
        private System.Windows.Forms.TextBox textBoxXCartPort;
        private System.Windows.Forms.Label labelXPort;
        private System.Windows.Forms.Label labelXUserAsterisk;
        private System.Windows.Forms.TextBox textBoxXCartUserName;
        private System.Windows.Forms.Label labelXCartUserName;
        private System.Windows.Forms.Label labelXCartDBAsterisk;
        private System.Windows.Forms.TextBox textBoxXDBName;
        private System.Windows.Forms.Label labelXCartDBName;
        private System.Windows.Forms.Label labelXPwdAsterisk;
        private System.Windows.Forms.TextBox textBoxXCartPwd;
        private System.Windows.Forms.Label labelXCartPwd;
        private System.Windows.Forms.Button buttonXTestConnection;
        private System.Windows.Forms.Label labelXCartPrefix;
        private System.Windows.Forms.TextBox textBoxXCartPrefix;
        private System.Windows.Forms.Label labelXCartPrefExample;
	    private System.Windows.Forms.Panel panelZencart;
        private System.Windows.Forms.TextBox textBoxZenPortNo;
        private System.Windows.Forms.Label labelZenPortNo;
        private System.Windows.Forms.TextBox textBoxZenWebStoreUrl;
        private System.Windows.Forms.Label labelZencartWebUrl;
        private System.Windows.Forms.TextBox textBoxZenUsername;
        private System.Windows.Forms.Label labelZenUserName;
        private System.Windows.Forms.TextBox textBoxZenDatabaseName;
        private System.Windows.Forms.TextBox textBoxZenPassword;
        private System.Windows.Forms.Label labelZenDatabaseName;
        private System.Windows.Forms.Label labelZenPassword;
        private System.Windows.Forms.Label labelZenDatabasenameAsteric;
        private System.Windows.Forms.Label labelZenUsernameAsteric;
        private System.Windows.Forms.Label labelZenPortNoAsteric;
        private System.Windows.Forms.Label labelZenWebStoreAsteric;
        private System.Windows.Forms.Button buttonZenTestConnection;
        private System.Windows.Forms.TextBox textBoxZenPrefix;
        private System.Windows.Forms.Label labelZenPrefix;
        private System.Windows.Forms.Label labelPrefixExample;
        private System.Windows.Forms.CheckBox checkBoxZenCartSSHTunnel;
        private System.Windows.Forms.Label labelZenCartShippingItem;
        private System.Windows.Forms.ComboBox comboBoxZenCartShippingItem;
        private System.Windows.Forms.Panel panelSSHZencart;
        private System.Windows.Forms.TextBox textBoxSSHHostnameZencart;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelSSHHostnameZencart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelSSHPortnoZencart;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelSSHUsernameZencart;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelSSHPasswordZencart;
        private System.Windows.Forms.TextBox textBoxSSHPasswordZencart;
        private System.Windows.Forms.TextBox textBoxSSHPortnoZencart;
        private System.Windows.Forms.TextBox textBoxSSHUsernameZencart;
        private System.Windows.Forms.DateTimePicker dateTimePickerZencartLastDate;
        private System.Windows.Forms.Label labelZencartLastDate;
        private System.Windows.Forms.Panel panelSSHXcart;
        private System.Windows.Forms.TextBox textBoxSSHHostnameXcart;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelSSHHostnameXcart;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label labelSSHPortnoXcart;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label labelSSHUserNameXcart;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelSSHPasswordXcart;
        private System.Windows.Forms.TextBox textBoxSSHPasswordXcart;
        private System.Windows.Forms.TextBox textBoxSSHPortnoXcart;
        private System.Windows.Forms.TextBox textBoxSSHUserNameXcart;
        private System.Windows.Forms.CheckBox checkBoxXCartSSHTunnel;
        private System.Windows.Forms.DateTimePicker dateTimePickerXCartLastDate;
        private System.Windows.Forms.Label labelXCartLastDate;
        private System.Windows.Forms.ComboBox comboBoxXCartShippingItem;
        private System.Windows.Forms.Label labelXCartShippingItem;
    }
}

