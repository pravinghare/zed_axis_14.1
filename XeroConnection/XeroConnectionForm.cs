using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.Runtime.CompilerServices;
using EDI.Constant;

namespace DataProcessingBlocks.XeroConnection
{
    public partial class XeroConnectionForm : Telerik.WinControls.UI.RadForm
    {
        public XeroConnectionDetails connectionDetails;
        private static XeroConnectionInfo _xeroConnection;
        private static XeroConnectionForm m_xeroConnectionInfo;

        private static string accessToken;
        private static string refreshToken;
        private static string tenantId;

        public XeroConnectionForm()
        {
            InitializeComponent();
            connectionDetails = new XeroConnectionDetails();
            _xeroConnection = new XeroConnectionInfo();
        }

        /// <summary>
        /// This method is used to get static instance.
        /// </summary>
        /// <returns></returns>
        public static XeroConnectionForm GetInstance(bool isDestroy = false)
        {
            if (isDestroy)
            {
                m_xeroConnectionInfo = new XeroConnectionForm();

            }
            else
            {
                if (m_xeroConnectionInfo == null)
                    m_xeroConnectionInfo = new XeroConnectionForm();
            }
            return m_xeroConnectionInfo;
        }

        private void XeroConnectionForm_Load(object sender, EventArgs e)
        {
            ResetValues();
        }

        private void ResetValues()
        {
            txtAccessToken.Clear();
            txtRefreshToken.Clear();
            txtTenantId.Clear();
            lblValidationStatus.Text = "";
            txtAccessToken.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        private async void btnSave_Click(object sender, EventArgs e)
        {
            accessToken = txtAccessToken.Text;
            refreshToken = txtRefreshToken.Text;
            tenantId = txtTenantId.Text;
            if (accessToken == null || accessToken == "")
            {
                MessageBox.Show("Please enter Access Token");
                return;
            }
            else if (refreshToken == null || refreshToken == "")
            {
                MessageBox.Show("Please enter Refresh Token");
                return;
            }
            else if (tenantId == null || tenantId == "")
            {
                MessageBox.Show("Please enter TenantId");
                return;
            }

            connectionDetails.AccessToken = accessToken;
            connectionDetails.RefreshToken = refreshToken;
            connectionDetails.TenantID = tenantId;

            var details = await _xeroConnection.validateConnectionDetails(connectionDetails);

            if (details == null || details.CompanyName == null)
            {
                MessageBox.Show("Invalid values, Please try again");
                return;
            }
            else
            {
                lblValidationStatus.Text = "Connected to : " + details.CompanyName;
                var saveConnection = await _xeroConnection.SaveConnectionDetailsToXml(connectionDetails);
                this.Close();
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ResetValues();
        }

        private void buttonHelp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Constants.ConnectToXeroHelpURL);
        }
    }
}
