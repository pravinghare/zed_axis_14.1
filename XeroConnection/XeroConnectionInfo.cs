﻿using DataProcessingBlocks.OnlineConnection;
using EDI.Constant;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Config;
using Xero.NetStandard.OAuth2.Model;
using System.Xml.Serialization;
using System.Runtime.Serialization;
namespace DataProcessingBlocks.XeroConnection
{
    class XeroConnectionInfo
    {
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public async System.Threading.Tasks.Task<XeroConnectionDetails> validateConnectionDetails(XeroConnectionDetails connectionDetails)
        {
            try
            {
                var _accountingApi = new AccountingApi();
                //var accessToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjFDQUY4RTY2NzcyRDZEQzAyOEQ2NzI2RkQwMjYxNTgxNTcwRUZDMTkiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJISy1PWm5jdGJjQW8xbkp2MENZVmdWY09fQmsifQ.eyJuYmYiOjE1Nzg0Nzg0NjEsImV4cCI6MTU3ODQ4MDI2MSwiaXNzIjoiaHR0cHM6Ly9pZGVudGl0eS54ZXJvLmNvbSIsImF1ZCI6Imh0dHBzOi8vaWRlbnRpdHkueGVyby5jb20vcmVzb3VyY2VzIiwiY2xpZW50X2lkIjoiOUE3QTcyQjA2QUZCNDlGQUI5Q0YyNkRDRDI0MTQyMkMiLCJzdWIiOiIyZGFkMDU3ZTFkNDY1MWM4YTYxOTc1NDgwODM2ZDNkNyIsImF1dGhfdGltZSI6MTU3ODQ3NzczMCwieGVyb191c2VyaWQiOiIwNDU3MjRlZi04ZDBjLTRjOTgtYmQ4MS00NjY1MGRhMDg1OTMiLCJnbG9iYWxfc2Vzc2lvbl9pZCI6IjRkM2VhMDZkYmE0YzQzMzJiYzM1ZDQwYzQ5ZjJjMTJiIiwianRpIjoiOGZjMzQxMzE0ZDUxYTRlZTVmYTkwYjQwMzBiZTM5NjQiLCJzY29wZSI6WyJlbWFpbCIsInByb2ZpbGUiLCJvcGVuaWQiLCJmaWxlcyIsImFjY291bnRpbmcuc2V0dGluZ3MiLCJhY2NvdW50aW5nLnRyYW5zYWN0aW9ucyIsImFjY291bnRpbmcuY29udGFjdHMiLCJvZmZsaW5lX2FjY2VzcyJdfQ.ygWnU7ZhU43bQT-fgMk3giP1VFAbPiHfwXsFBt2IggpMZU3AXeBnOwtNJ3flUAAFErTlyKTPBF6LWzwHRDE7KGljUscx5i7mmsAlqe4RrqcuNKyfpnRV2Y52KHIWns2b1C8abdzPZNSKp3gJ8azPkHqe4vpmcmZFwCb27t9IWRPKCF4oIvKijfJU809XpEXgIHbZkk_P-DFFXtFfXCvJv5537n7Nuyx_IG-E2Y3LFf2v1gyWDUgCvSawahEz7qvhdnwu6MAySB1PSYQBRY5MsGDwFhyvGVxVfTIcrPd7sWC_nGvdzVrx6-Yu13wK4dzC_enm6MVemm8YldspcK-83w";
                //var tenantId = "428e3f36-bc39-4e42-a194-7111ec063889";
                var details = GetDecryptedConnectionDetails(connectionDetails);
                var organisations = await _accountingApi.GetOrganisationsAsync(details.AccessToken, details.TenantID);
                connectionDetails.CompanyName = organisations._Organisations[0].Name;
                connectionDetails.CountryVersion = organisations._Organisations[0].Version.ToString();
                return connectionDetails;

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        private XeroConnectionDetails GetDecryptedConnectionDetails(XeroConnectionDetails connectionDetails)
        {
            Cryptography cryptography = new Cryptography();
            connectionDetails.AccessToken = cryptography.Decrypt(connectionDetails.AccessToken);
            connectionDetails.TenantID = cryptography.Decrypt(connectionDetails.TenantID);
            connectionDetails.RefreshToken = cryptography.Decrypt(connectionDetails.RefreshToken);
            //CommonUtilities.GetInstance().QBOConnectionDetails = connectionDetails;
            return connectionDetails;
        }

        public XeroConnectionDetails GetEncryptedConnectionDetails(XeroConnectionDetails connectionDetails)
        {
            Cryptography cryptography = new Cryptography();
            connectionDetails.AccessToken = cryptography.Encrypt(connectionDetails.AccessToken);
            connectionDetails.RefreshToken = cryptography.Encrypt(connectionDetails.RefreshToken);
            connectionDetails.TenantID = cryptography.Encrypt(connectionDetails.TenantID);

            return connectionDetails;
        }

        public string GetIPPFilePath()
        {
            string ippFilePath = string.Empty;

            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                        ippFilePath += @"\Ipp.xml";
                    }
                    else
                    {
                        ippFilePath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                        ippFilePath += @"\Ipp.xml";
                    }
                }
                else
                {
                    ippFilePath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                    ippFilePath += @"\Ipp.xml";
                }
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = ippFilePath.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        ippFilePath = ippFilePath.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (ippFilePath.Contains("Program Files (x86)"))
                    {
                        ippFilePath = ippFilePath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        ippFilePath = ippFilePath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            return ippFilePath;
        }

        public XmlDocument CreateNewConnectionXml(XeroConnectionDetails connectionDetails)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlElement functionsElement = xdoc.CreateElement("ipp");
            xdoc.AppendChild(functionsElement);

            XmlAttribute attribute = functionsElement.OwnerDocument.CreateAttribute("version");
            attribute.Value = Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);
            functionsElement.Attributes.Append(attribute);

            XmlElement functionElement = xdoc.CreateElement("XeroConnectionInfo");
            functionsElement.AppendChild(functionElement);
            XmlElement AccessToken = xdoc.CreateElement("AccessToken");
            AccessToken.InnerText = connectionDetails.AccessToken;
            functionElement.AppendChild(AccessToken);

            XmlElement RefreshToken = xdoc.CreateElement("RefreshToken");
            RefreshToken.InnerText = connectionDetails.RefreshToken;
            functionElement.AppendChild(RefreshToken);

            XmlElement RealmId = xdoc.CreateElement("TenantId");
            RealmId.InnerText = connectionDetails.TenantID;
            functionElement.AppendChild(RealmId);

            XmlElement ExpiryDate = xdoc.CreateElement("ExpiryDate");
            ExpiryDate.InnerText = DateTime.Now.ToString();
            functionElement.AppendChild(ExpiryDate);

            XmlElement CompanyName = xdoc.CreateElement("CompanyName");
            CompanyName.InnerText = connectionDetails.CompanyName;
            functionElement.AppendChild(CompanyName);

            XmlElement CountryVersion = xdoc.CreateElement("CountryVersion");
            CountryVersion.InnerText = connectionDetails.CountryVersion;
            functionElement.AppendChild(CountryVersion);

            return xdoc;
        }

        public async Task<XeroConnectionDetails> SaveConnectionDetailsToXml(XeroConnectionDetails connectionDetails, bool refreshToken = true)
        {
            if (refreshToken)
            {
                connectionDetails = await CommonUtilities.refreshXeroAccessToken(connectionDetails);
            }
            connectionDetails = GetEncryptedConnectionDetails(connectionDetails);
            string ippFilePath = GetIPPFilePath();
            // connectionDetails.ExpiryDate = DateTime.Now;
            XmlDocument xdoc = new XmlDocument();
            if (!File.Exists(ippFilePath))
            {
                xdoc = CreateNewConnectionXml(connectionDetails);
                xdoc.Save(ippFilePath);
            }
            else
            {
                try
                {
                    //Update ipp.xml with latest tokens
                    xdoc.Load(ippFilePath);
                    XmlNode root = xdoc.DocumentElement;
                    XmlNode ippNode = root.SelectSingleNode("//XeroConnectionInfo");
                    ippNode.SelectSingleNode("./AccessToken").InnerText = connectionDetails.AccessToken;
                    ippNode.SelectSingleNode("./RefreshToken").InnerText = connectionDetails.RefreshToken;
                    ippNode.SelectSingleNode("./TenantId").InnerText = connectionDetails.TenantID;
                    ippNode.SelectSingleNode("./ExpiryDate").InnerText = DateTime.Now.ToString();
                    ippNode.SelectSingleNode("./CompanyName").InnerText = connectionDetails.CompanyName;
                    ippNode.SelectSingleNode("./CountryVersion").InnerText = connectionDetails.CountryVersion;
                    xdoc.Save(ippFilePath);
                }
                catch (Exception ex)
                {
                    // OAuth 1.0 is having encrypted xml, it will give error, so deleted that file and created new ipp.xml file
                    File.Delete(ippFilePath);
                    xdoc = CreateNewConnectionXml(connectionDetails);
                    xdoc.Save(ippFilePath);
                }
            }

            return GetDecryptedConnectionDetails(connectionDetails);
        }

        public async Task<XeroConnectionDetails> GetXeroConnectionDetailsFromXml()
        {
            XeroConnectionDetails connectionDetails = new XeroConnectionDetails();
            string ippFilePath = GetIPPFilePath();
            XmlDocument xdoc = new XmlDocument();
            if (!File.Exists(ippFilePath))
            {
                return null;
            }
            else
            {
                try
                {
                    xdoc.Load(ippFilePath);
                    XmlNode root = xdoc.DocumentElement;
                    XmlNode ippNode = root.SelectSingleNode("//XeroConnectionInfo");
                    connectionDetails.AccessToken = ippNode.SelectSingleNode("./AccessToken").InnerText;
                    connectionDetails.RefreshToken = ippNode.SelectSingleNode("./RefreshToken").InnerText;
                    connectionDetails.TenantID = ippNode.SelectSingleNode("./TenantId").InnerText;
                    connectionDetails.ExpiryDate = Convert.ToDateTime(ippNode.SelectSingleNode("./ExpiryDate").InnerText);
                    connectionDetails.CompanyName = ippNode.SelectSingleNode("./CompanyName").InnerText;
                    connectionDetails.CountryVersion = ippNode.SelectSingleNode("./CountryVersion").InnerText;


                    connectionDetails = GetDecryptedConnectionDetails(connectionDetails);
                    //If xero token is expired then refresh the token and update IPP file.
                    if (IsXeroAccessTokenExpired(connectionDetails.ExpiryDate))
                    {
                        connectionDetails = await SaveConnectionDetailsToXml(connectionDetails);
                    }
                    return connectionDetails;

                }
                catch (Exception ex)
                {
                    //File.Delete(ippFilePath);
                    return null;
                }
            }
        }


        public static bool IsXeroAccessTokenExpired(DateTime ExpiryDate)
        {
            DateTime now = DateTime.Now;
            TimeSpan difference = now.Subtract(ExpiryDate);
            if (difference.TotalMinutes > 25) { return true; }
            return false;
        }

    }

    public class XeroApiData
    {
        public static async Task<Invoices> InvoiceList()
        {
            XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();

            var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
            if (CheckConnectionInfo == null)
            {
                return null;
            }
            AccountingApi accountingApi = new AccountingApi();
            var InvoicesTask = accountingApi.GetInvoicesAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID);
            InvoicesTask.Wait();
            Invoices Invoices = InvoicesTask.Result;
            return Invoices;

        }

        public static async Task<BankTransactions> BankTransactionsList()
        {
            XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();

            var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
            if (CheckConnectionInfo == null)
            {
                return null;
            }
            AccountingApi accountingApi = new AccountingApi();
            var BankTransactionTask = accountingApi.GetBankTransactionsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID);
            BankTransactionTask.Wait();
            BankTransactions BankTransaction = BankTransactionTask.Result;
            return BankTransaction;

        }

    }


    public class XeroConnectionDetails
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string TenantID { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CompanyName { get; set; }
        public string CountryVersion { get; set; }
        public string CountryCode { get; set; }
    }
    public class XeroAccessTokendata
    {
        public string authorizationUrl { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime ExpiryTime { get; set; }
    }

    public class AxisDesktopXeroDetailsEntity
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string TenantId { get; set; }
    }
   
}
