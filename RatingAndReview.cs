﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Microsoft.Win32;

namespace DataProcessingBlocks
{
    public partial class RatingAndReview : Form
    {
        RegistryKey HasGivenFeedBack;
        RegistryKey keyName;
        RegistryKey currentUserKey = Registry.CurrentUser;

        private RegistryKey baseRegistryKey = Registry.LocalMachine;

         RegistryKey currentUser;// = Registry.CurrentUser;
            string rating; // = currentUser.ToString();
            string name; // = rating; 
            object value; // = radRating1.Value.ToString();

        private bool m_GivenFeedback;

        public bool GivenFeedback
        {
            get { return m_GivenFeedback; }
            set { m_GivenFeedback = value; }
        }
                     

        public RatingAndReview()
        {
            InitializeComponent();
        }

        private void radRating1_Click(object sender, EventArgs e)
        {
        }

        public bool SaveRating()
        {
            currentUser = Registry.CurrentUser;
            //rating = currentUser.ToString();
            name = "HasGivenRating";
            value = radRating1.Value.ToString();
            if (radRating1.Value.ToString() != null)
            {
                value = true;
            }
            keyName = currentUserKey.OpenSubKey(@"SOFTWARE", true);
            RegistryKey newKey = keyName.CreateSubKey("Zed Systems");
            try
            {
                HasGivenFeedBack = newKey.CreateSubKey(name);
                HasGivenFeedBack.SetValue(name, value);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool getRating()
        {
            try
            {
                //  name = "HasGivenRating"; 
                HasGivenFeedBack = currentUserKey.OpenSubKey(@"SOFTWARE\");
                RegistryKey AxisRegKey;
                AxisRegKey = HasGivenFeedBack.OpenSubKey("Zed Systems\\HasGivenRating", true);
                this.GivenFeedback = false;
                if (AxisRegKey != null)
                {
                    object obj = AxisRegKey.GetValue("HasGivenRating");
                    if ("True".Equals(obj))
                    {
                        this.GivenFeedback = true;
                    }
                    else
                        this.GivenFeedback = false;
                }
            }
            catch (Exception ex)
            {
            }
            return this.GivenFeedback;
        }

        private void btnFeedback_Click(object sender, EventArgs e)
        {
            int i = 7;
            if (radRating1.Value < 1)
            {
                MessageBox.Show("Please select ratings to submit feedback");

            }
            else
            {
                SaveRating();

                if (radRating1.Value > i)
                {
                    MessageBox.Show("Thanks for your feedback,Please give your valuable feedback on Intuit website");
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBOnlinestring)
                    {
                        string urlQBO = "https://appcenter.intuit.com/zed-axis-import-qbo#review";
                        System.Diagnostics.Process.Start(urlQBO);
                    }
                    else if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
                    {
                        string urlQBD = "https://desktop.apps.com/apps/130091#!reviews";
                        System.Diagnostics.Process.Start(urlQBD);
                    }
                }
                else
                {
                   
                    MessageBox.Show("Thanks for your feedback,Please give your valuable feedback on Zed Axis website");
                    string url = "https://support.zed-systems.com/community/search.aspx?tagID=11&sortBy=0";
                    System.Diagnostics.Process.Start(url);
                }
                object value = null;
                if (radRating1.Value.ToString() != null)
                {
                    value = true;

                }

                this.Close();

            }
        }

        private void btnDismiss_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

      

      
    }
}
