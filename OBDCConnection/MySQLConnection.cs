using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.Odbc;
using System.Data;

namespace OBDCConnection
{
    
    /// <summary>
   /// Static class to open a OdbcConnection and close a OdbcConnection
   /// </summary>
    public static class MySQLConnection
    {
        #region Private Members
            private static OdbcConnection connection =null;
        #endregion

       
        /// <summary>
        /// This function is used to open a odbc connection using the connection string from AppSettings.
        /// </summary>
        /// <returns>OdbcConnection object</returns>
        public static OdbcConnection Open()
        {
            //Create a OdbcConnection object using the connection string from AppSettings
            if (connection == null)
            {
                connection = new OdbcConnection(ConfigurationManager.AppSettings["connectionString"].ToString());
            }
            try
            {
                //if connection notopen , then open a connection
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                //If connection open , then return the connection object.
                if (connection.State == ConnectionState.Open)
                {
                    return connection;
                }
                
                //Else return a null object
                else 
                {
                    return null;
                }
                
            }
                //Handle exception
            catch (OdbcException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
           
        }

        /// <summary>
        /// Close the ODBC connection
        /// </summary>
        /// <returns>OdbcConnection object</returns>
        public static OdbcConnection Close()
        {
            try
            {
                //if connection notopen , then open a connection
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }

                //If connection open , then return the connection object.
                if (connection.State == ConnectionState.Closed)
                {
                    return connection;
                }
                //Else return a null object
                else
                {
                    connection.Dispose();
                    return null;
                }
            }
                //Handle Exception
            catch (OdbcException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            
        }

   }
}
