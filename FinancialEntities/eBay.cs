﻿// ===============================================================================
// 
// eBay.cs
//
// This eBay class contains the Methods of connection to eBay
// ,GetEbayAuthToken method,GettingOrders,SaveOrder etc.
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
//using GeteBayOfficialTime.com.ebay.developer;
using eBay.Service.Core.Soap;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Collections;
using System.Data;
using DataProcessingBlocks;



namespace FinancialEntities
{
    /// <summary>
    /// This class is used for manage eBay Transactions.
    /// </summary>
    public class eBayLibrary : General
    {
        #region Private Members

        /// <summary>
        /// Private Members for create eBay Transactions.
        /// </summary>
        private string version;//version of the application.
        private string appId;//application id for eBay.
        private string devId;//Developer Id for eBay.
        private string certificateId;//Certificate id for eBay.
        private int siteId;//It indicates country of eBay.
        private string serverId;//Server id (SandBox Server for testing and Prduction Server for Production.)
        private string itemMapping;//User mapping for eBay.
        private string eBayAuthToken;//Authentication token requiered for eBay Transactions.
        private DateTime lastUpdated;

        private static eBayLibrary m_eBay;
        private string[] orderIDArray;
        #endregion

        #region Public Properties

        /// <summary>
        /// Get or Set version of eBay Server.
        /// </summary>
        public string Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        /// <summary>
        /// Get or Set Application ID.
        /// </summary>
        public string AppID
        {
            get
            {
                return appId;
            }
            set
            {
                appId = value;
            }
        }

        /// <summary>
        /// Get or Set Developer ID of application.
        /// </summary>
        public string DevID
        {
            get
            {
                return devId;
            }
            set
            {
                devId = value;
            }
        }

        /// <summary>
        /// Get or Set Certificate ID of Application.
        /// </summary>
        public string CertificateID
        {
            get
            {
                return certificateId;
            }
            set
            {
                certificateId = value;
            }
        }

        /// <summary>
        /// Get or Set Site ID of eBay version.
        /// </summary>
        public int SiteID
        {
            get
            {
                return siteId;
            }
            set
            {
                siteId = value;
            }
        }

        /// <summary>
        /// Get or Set Server ID of eBay Application.
        /// </summary>
        public string ServerID
        {
            get
            {
                return serverId;
            }
            set
            {
                serverId = value;
            }
        }

        /// <summary>
        /// Get or Set Item Mapping for eBay application.
        /// </summary>
        public string ItemMapping
        {
            get
            {
                return itemMapping;
            }
            set
            {
                itemMapping = value;
            }
        }

        /// <summary>
        /// Get or Set eBay Authentication Token for transactions.
        /// </summary>
        public string EBayAuthToken
        {
            get
            {
                return eBayAuthToken;
            }
            set
            {
                eBayAuthToken = value;
            }
        }

        /// <summary>
        /// Get or Set Last Updated date for getting orders.
        /// </summary>
        public DateTime LastUpdated
        {
            get
            {
                return lastUpdated;
            }
            set
            {
                lastUpdated = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor code for generate eBay Class.
        /// </summary>
        public eBayLibrary()
        {


        }

        #endregion

        #region Static Method

        /// <summary>
        /// This method is used for getting eBay Object.
        /// </summary>
        /// <returns>Object of eBay Class.</returns>
        public static eBayLibrary GetInstance()
        {
            if (m_eBay == null)
                m_eBay = new eBayLibrary();
            return m_eBay;
        }

        #endregion

        #region Methods and Events


        /// <summary>
        /// This method is used for getting eBayAuthToken from
        /// Username and Password of registered members.
        /// </summary>
        /// <param name="userName">User name of Registered eBay Member</param>
        /// <param name="password">Password of Registered eBay Member</param>
        /// <returns>eBay Authentication Token of Registered members.</returns>
        public string GetSessionID(string userName, string password , string eBayType)
        {
            try
            {
                //Get RuName of eBay SingIn account               
                //string ruName = SetRuName(userName, password , eBayType);
                //if (ruName == "Invalid user name or password.")
                //{
                    //return string.Empty;
                //}
                //ruName = GetRuName(userName, password, eBayType);
                string ruName = string.Empty;
                if (eBayType == "SandBox")
                    ruName = "Zed_Systems-ZedSyste-897e-4-wabquley";
                else if (eBayType == "Production")
                    ruName = "Zed_Systems-ZedSyste-1857-4-ggbgh";

                string sessionID = GetSession(ruName, eBayType);
                if (string.IsNullOrEmpty(sessionID))
                {
                    return string.Empty;
                }
                else
                {
                    if (eBayType == "Production")   
                    {
                        MessageBox.Show("Please sign in eBay site with your Production Username and password for varification of Production account.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Process.Start("IExplore.exe", "https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&runame=" + ruName + "&SessID=" + sessionID);
                    }
                    else
                    {
                        MessageBox.Show("Please sign in eBay site with your SandBox Username and password for varification of SandBox account.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Process.Start("IExplore.exe", "https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn&runame=" + ruName + "&SessID=" + sessionID);
                    }
                    return sessionID;
                }
            }
            catch (Exception ex)
            {
                //Log the exceptions.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return string.Empty;
            }

        }

        /// <summary>
        /// This method is used for getting eBay Auth Token.
        /// </summary>
        /// <param name="sessionID"></param>
        /// <returns></returns>
        public string GetEBayAuthToken(string sessionID , string eBayType)
        {
            try
            {
                //Get XML into a string for use in encoding
                //string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?><GetSessionIDRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RuName>" + ruName.Trim() + "</RuName></GetSessionIDRequest>";
                string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><FetchTokenRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><Version>551</Version><SessionID>" + sessionID + "</SessionID></FetchTokenRequest>";


                //Put the data into a UTF8 encoded  byte array
                UTF8Encoding encoding = new UTF8Encoding();
                int dataLen = encoding.GetByteCount(xmlText);
                byte[] utf8Bytes = new byte[dataLen];
                Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

                #region Setup The Request inc. HTTP Headers
                //Create a new HttpWebRequest object for the ServerUrl
                HttpWebRequest request = null;

                if (eBayType.Equals("SandBox"))
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
                }
                else
                {
                    request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
                }

                //Set Request Method (POST) and Content Type (text/xml)
                request.Method = "POST";
                request.ContentType = "text/xml";
                request.ContentLength = utf8Bytes.Length;

                if (eBayType.Equals("SandBox"))
                {
                    //Add the Keys to the HTTP Headers
                    request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                    request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                    request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
                }
                else
                {
                    //Add the Keys to the HTTP Headers
                    request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                    request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                    request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");
                }


                //Add Compatability Level to HTTP Headers
                //Regulates versioning of the XML interface for the API
                request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 551");

                //Add function name, SiteID and Detail Level to HTTP Headers
                request.Headers.Add("X-EBAY-API-CALL-NAME: FetchToken");
                request.Headers.Add("X-EBAY-API-SITEID: 0");

                //Time out = 150 seconds,  set to -1 for no timeout.
                //If times-out - throws a WebException with the
                //Status property set to WebExceptionStatus.Timeout.
                request.Timeout = 150000;

                #endregion

                #region Send The Request
                Stream str = null;
                try
                {
                    //Set the request Stream
                    str = request.GetRequestStream();
                    //Write the equest to the Request Steam
                    str.Write(utf8Bytes, 0, utf8Bytes.Length);
                    str.Close();
                    //Get response into stream
                    WebResponse resp = request.GetResponse();
                    str = resp.GetResponseStream();
                }
                catch (WebException wEx)
                {
                    //Error has occured whilst requesting
                    //Display error message and exit.
                    if (wEx.Status == WebExceptionStatus.Timeout)
                        Console.WriteLine("Request Timed-Out.");
                    else
                        Console.WriteLine(wEx.Message);

                    Console.WriteLine("Press Enter to Continue...");
                    Console.ReadLine();
                    return string.Empty;
                }
                #endregion

                #region Process Response
                // Get Response into String
                StreamReader sr = new StreamReader(str);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(sr.ReadToEnd());
                sr.Close();
                str.Close();

                //get the root node, for ease of use
                XmlNode root = xmlDoc["FetchTokenResponse"];

                string eBayToken = string.Empty;
                //There have been Errors
                if (root["Errors"] != null)
                {
                    string errorCode = root["Errors"]["ErrorCode"].InnerText;
                    string errorShort = root["Errors"]["ShortMessage"].InnerText;
                    string errorLong = root["Errors"]["LongMessage"].InnerText;

                    //Output the error message
                    Console.WriteLine(errorCode + " ERROR: " + errorShort);
                    Console.WriteLine(errorLong + "\n");
                }
                else
                {
                    eBayAuthToken = root["eBayAuthToken"].InnerText;
                }

                return eBayAuthToken;

                #endregion

            }
            catch (Exception ex)
            {
                //Log the exceptions.
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return string.Empty;
            }

        }

        /// <summary>
        /// This method is used for getting Session.
        /// </summary>
        /// <param name="ruName"></param>
        /// <returns></returns>
        public string GetSession(string ruName , string eBayType)
        {

            #region Setup The Request inc. HTTP Headers
            //Get XML into a string for use in encoding
            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><GetSessionIDRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RuName>" + ruName.Trim() + "</RuName></GetSessionIDRequest>";

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

            //Create a new HttpWebRequest object for the ServerUrl
            HttpWebRequest request = null;

            if (eBayType.Equals("SandBox"))
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
            }

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            if (eBayType.Equals("SandBox"))
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
            }
            else
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");
            }

            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 647");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: GetSessionID");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 150000;

            #endregion

            #region Send The Request
            Stream str = null;
            try
            {
                //Set the request Stream
                str = request.GetRequestStream();
                //Write the equest to the Request Steam
                str.Write(utf8Bytes, 0, utf8Bytes.Length);
                str.Close();
                //Get response into stream
                WebResponse resp = request.GetResponse();
                str = resp.GetResponseStream();
            }
            catch (WebException)
            {
                return string.Empty;
            }
            #endregion

            #region Process Response
            // Get Response into String
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["GetSessionIDResponse"];

            string sessionID = string.Empty;
            //There have been Errors
            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;

                //Output the error message
                Console.WriteLine(errorCode + " ERROR: " + errorShort);
                Console.WriteLine(errorLong + "\n");
            }
            else
            {
                //Get Result String
                sessionID = root["SessionID"].InnerText;
            }

            return sessionID;

            #endregion
        }

        /// <summary>
        /// This method is used for set Ru Name.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string SetRuName(string username, string password , string eBayType)
        {

            #region Setup The Request inc. HTTP Headers

            //Get XML into a string for use in encoding
            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><SetReturnURLRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><Username>" + username + "</Username><Password>" + password + "</Password></RequesterCredentials><Version>551</Version><AuthenticationEntry><AcceptURL>https://arribada.ebay.com/aw-secure/auth_auth_thanks.html</AcceptURL><RejectURL>https://arribada.ebay.com/aw-secure/auth_auth_cancel.html</RejectURL><RuName>Zed_Systems-ZedSyste-5011-7-eoalqme</RuName><TokenReturnMethod>FetchToken</TokenReturnMethod></AuthenticationEntry><ApplicationDisplayName>Axis 5 Application</ApplicationDisplayName><ApplicationType>Desktop Client</ApplicationType><Action>Add</Action></SetReturnURLRequest>";

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);
            //Create a new HttpWebRequest object for the ServerUrl
            HttpWebRequest request = null;

            if (eBayType.Equals("SandBox"))
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
            }

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            if (eBayType.Equals("SandBox"))
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
            }
            else
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");               
            }
           

            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 551");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: SetReturnURL");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 150000;

            #endregion

            #region Send The Request
            Stream str = null;
            try
            {
                //Set the request Stream
                str = request.GetRequestStream();
                //Write the equest to the Request Steam
                str.Write(utf8Bytes, 0, utf8Bytes.Length);
                str.Close();
                //Get response into stream
                WebResponse resp = request.GetResponse();
                str = resp.GetResponseStream();
            }
            catch (WebException)
            {
                return string.Empty;
            }
            #endregion

            #region Process Response
            // Get Response into String
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["SetReturnURLResponse"];

            string ruName = "Zed_Systems-ZedSyste-5011-7-eoalqme";
            //There have been Errors
            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorCode);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorShort);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorLong);
                if (errorShort == "Invalid user name or password.")
                {
                    return errorShort;
                }
            }

            return ruName;

            #endregion
        }

        /// <summary>
        /// This method is used for getting RuName.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetRuName(string username, string password, string eBayType)
        {
            //Get XML into a string for use in encoding
            //string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?><GetSessionIDRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RuName>" + ruName.Trim() + "</RuName></GetSessionIDRequest>";
            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><GetReturnURLRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><Username>" + username + "</Username><Password>" + password + "</Password></RequesterCredentials><Version>551</Version></GetReturnURLRequest>";

            //string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?> " +
            //                  "<GetSessionIDRequest xmlns=\"urn:ebay:apis:eBLBaseComponents\">" +
            //                  "<RuName>" + ruName + "</RuName></GetSessionIDRequest>";
            // 
            //<RequesterCredentials><DevId>06287722-9b5e-4af2-aa3e-c6d89520b35d</DevId><AppId>ZedSyste-897e-4a18-a573-07714198a5d0</AppId><AuthCert>fb608b80-f4d3-411f-b7a4-eb1f703de66d</AuthCert></RequesterCredentials>

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

            #region Setup The Request inc. HTTP Headers
            //Create a new HttpWebRequest object for the ServerUrl
            HttpWebRequest request = null;

            if (eBayType.Equals("SandBox"))
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
            }

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            if (eBayType.Equals("SandBox"))
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
            }
            else
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");
            }
            
            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 551");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: GetReturnURL");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 150000;

            #endregion

            #region Send The Request
            Stream str = null;
            try
            {
                //Set the request Stream
                str = request.GetRequestStream();
                //Write the equest to the Request Steam
                str.Write(utf8Bytes, 0, utf8Bytes.Length);
                str.Close();
                //Get response into stream
                WebResponse resp = request.GetResponse();
                str = resp.GetResponseStream();
            }
            catch (WebException wEx)
            {
                //Error has occured whilst requesting
                //Display error message and exit.
                if (wEx.Status == WebExceptionStatus.Timeout)
                    Console.WriteLine("Request Timed-Out.");
                else
                    Console.WriteLine(wEx.Message);

                Console.WriteLine("Press Enter to Continue...");
                Console.ReadLine();
                return string.Empty;
            }
            #endregion

            #region Process Response
            // Get Response into String
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["GetReturnURLResponse"];

            string ruName = "Zed_Systems-ZedSyste-5011-7-eoalqme";
            //There have been Errors
            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;

                //Output the error message
                Console.WriteLine(errorCode + " ERROR: " + errorShort);
                Console.WriteLine(errorLong + "\n");
            }
            else
            {
                //ruName = root["RuName"].InnerText;
            }


            return ruName;

            #endregion
        }

        public string GetOrderByTradingPartner(DateTime updatedDate, string eBayToken, string tradingPartnerName)
        {
            string message = string.Empty;

            string headerLine = string.Empty;

            string[] middleLine = null;

            StringBuilder sb = new StringBuilder();

            //For getting Seller Transactions
            //string xmlText = @"<?xml version=""1.0"" encoding=""ISO-8859-1""?><GetSellerTransactionsRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><IncludeContainingOrder>1</IncludeContainingOrder><ModTimeFrom>" + updatedDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeFrom><ModTimeTo>" + DateTime.Now.AddMinutes(-5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeTo><TransactionArray><Transaction><ContainingOrder><OrderStatus>Completed</OrderStatus></ContainingOrder></Transaction></TransactionArray></GetSellerTransactionsRequest>";
            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><GetSellerTransactionsRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><IncludeContainingOrder>1</IncludeContainingOrder><ModTimeFrom>" + updatedDate.AddMinutes(-5).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeFrom><ModTimeTo>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ModTimeTo></GetSellerTransactionsRequest>";

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

            #region Setup The Request inc. HTTP Headers
            //Create a new HttpWebRequest object for the SandBox ServerUrl
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            //Add the Keys to the HTTP Headers
            request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
            request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
            request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");


            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 647");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: GetSellerTransactions");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 150000;

            #endregion

            #region Send The Request
            Stream str = null;
            try
            {
                //Set the request Stream
                str = request.GetRequestStream();
                //Write the equest to the Request Steam
                str.Write(utf8Bytes, 0, utf8Bytes.Length);
                str.Close();
                //Get response into stream
                WebResponse resp = request.GetResponse();
                str = resp.GetResponseStream();
            }
            catch (WebException wEx)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(wEx.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(wEx.StackTrace.ToString());
                message = wEx.Message;
                return message;
            }
            #endregion

            #region Process Response
            // Get Response into String
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            DataTable dataTable = new DataTable();

            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["GetSellerTransactionsResponse"];

            string sessionID = string.Empty;
            //There have been Errors
            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;

                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorCode + "\n " + errorShort);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorCode + "\n " + errorLong);
                return errorLong;

            }
            else
            {
                #region Add Orders to database
                try
                {
                    #region Datatable Columns

                    dataTable.Columns.Add("orderID");
                    dataTable.Columns.Add("amountPaid");
                    dataTable.Columns.Add("buyerUserID");
                    dataTable.Columns.Add("createdTime");
                    dataTable.Columns.Add("shippedTime");
                    dataTable.Columns.Add("shippingAddressName");
                    dataTable.Columns.Add("shippingAddressPhone");
                    dataTable.Columns.Add("shippingAddressStreet1");
                    dataTable.Columns.Add("shippingAddressStreet2");
                    dataTable.Columns.Add("shippingAddressCityName");
                    dataTable.Columns.Add("shippingAddressStateOrProvince");
                    dataTable.Columns.Add("shippingAddressPostalCode");
                    dataTable.Columns.Add("shippingAddressCountryName");
                    dataTable.Columns.Add("shippingDetailsInsuranceFee");

                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingServiceCost");
                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingInsuranceCost");
                    dataTable.Columns.Add("shippingDetailsServiceOptionShippingServiceAdditionalCost");
                    dataTable.Columns.Add("shippingAdjustmentAmount");
                    dataTable.Columns.Add("sellingManagerSalesRecordId");
                    dataTable.Columns.Add("itemId");
                    dataTable.Columns.Add("sku");
                    dataTable.Columns.Add("title");
                    dataTable.Columns.Add("quantityPurchased");
                    dataTable.Columns.Add("transactionPrice");

                    #endregion
                    foreach (System.Xml.XmlNode mainNode in xmlDoc.ChildNodes)
                    {
                        foreach (System.Xml.XmlNode oNode in mainNode.ChildNodes)
                        {
                            if (oNode.Name.Equals("TransactionArray"))
                            {

                                foreach (XmlNode orderArrayNodes in oNode.ChildNodes)
                                {

                                    string orderID = string.Empty;
                                    string amountPaid = string.Empty;
                                    string buyerUserID = string.Empty;
                                    string createdTime = string.Empty;
                                    string shippedTime = string.Empty;
                                    string shippingAddressName = string.Empty;
                                    string shippingAddressPhone = string.Empty;
                                    string shippingAddressStreet1 = string.Empty;
                                    string shippingAddressStreet2 = string.Empty;
                                    string shippingAddressCityName = string.Empty;
                                    string shippingAddressStateOrProvince = string.Empty;
                                    string shippingAddressPostalCode = string.Empty;
                                    string shippingAddressCountryName = string.Empty;
                                    string shippingDetailsInsuranceFee = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingServiceCost = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingInsuranceCost = string.Empty;
                                    string shippingDetailsInternationalServiceOptionShippingServiceAdditionalCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingServiceCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingInsuranceCost = string.Empty;
                                    string shippingDetailsServiceOptionShippingServiceAdditionalCost = string.Empty;
                                    string shippingAdjustmentAmount = string.Empty;
                                    string sellingManagerSalesRecordId = string.Empty;
                                    bool isOrder = false;
                                    object[] objArray = new object[24];

                                    //Checking TxnID
                                    #region Transaction Nodes
                                    if (orderArrayNodes.Name.Equals("Transaction"))
                                    {

                                        middleLine = new string[orderArrayNodes.ChildNodes.Count];
                                        int count = 0;
                                        string itemId = string.Empty;
                                        string sku = string.Empty;
                                        string title = string.Empty;
                                        string quantityPurchased = string.Empty;
                                        string transactionPrice = string.Empty;

                                        foreach (XmlNode orderNodes in orderArrayNodes.ChildNodes)
                                        {
                                            if (orderNodes.Name.Equals("AdjustmentAmount"))
                                            {
                                                shippingAdjustmentAmount = orderNodes.InnerText;
                                            }
                                            if (orderNodes.Name.Equals("Buyer"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("UserID"))
                                                    {
                                                        buyerUserID = ordNode.InnerText;
                                                    }
                                                    if (ordNode.Name.Equals("BuyerInfo"))
                                                    {
                                                        foreach (XmlNode shipNode in ordNode.ChildNodes)
                                                        {
                                                            if (shipNode.Name.Equals("ShippingAddress"))
                                                            {
                                                                foreach (XmlNode shippingNode in shipNode.ChildNodes)
                                                                {
                                                                    if (shippingNode.Name.Equals("Name"))
                                                                    {
                                                                        shippingAddressName = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Phone"))
                                                                    {
                                                                        shippingAddressPhone = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Street1"))
                                                                    {
                                                                        shippingAddressStreet1 = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("Street2"))
                                                                    {
                                                                        shippingAddressStreet2 = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("CityName"))
                                                                    {
                                                                        shippingAddressCityName = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("StateOrProvince"))
                                                                    {
                                                                        shippingAddressStateOrProvince = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("PostalCode"))
                                                                    {
                                                                        shippingAddressPostalCode = shippingNode.InnerText;
                                                                    }
                                                                    if (shippingNode.Name.Equals("CountryName"))
                                                                    {
                                                                        shippingAddressCountryName = shippingNode.InnerText;
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            if (orderNodes.Name.Equals("ConvertedAmountPaid"))
                                            {

                                                amountPaid = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("CreatedDate"))
                                            {
                                                createdTime = orderNodes.InnerText;
                                            }
                                            if (orderNodes.Name.Equals("ShippedTime"))
                                            {
                                                shippedTime = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("ShippingDetails"))
                                            {
                                                foreach (XmlNode shippingDetailsNode in orderNodes.ChildNodes)
                                                {
                                                    if (shippingDetailsNode.Name.Equals("InsuranceFee"))
                                                    {
                                                        shippingDetailsInsuranceFee = shippingDetailsNode.InnerText;
                                                    }
                                                    if (shippingDetailsNode.Name.Equals("SellingManagerSalesRecordNumber"))
                                                    {
                                                        sellingManagerSalesRecordId = shippingDetailsNode.InnerText;
                                                    }

                                                }
                                            }
                                            if (orderNodes.Name.Equals("ContainingOrder"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("ShippingDetails"))
                                                    {
                                                        foreach (XmlNode shippingDetailsNode in ordNode.ChildNodes)
                                                        {
                                                            if (shippingDetailsNode.Name.Equals("SellingManagerSalesRecordNumber"))
                                                            {
                                                                sellingManagerSalesRecordId = shippingDetailsNode.InnerText;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                            if (orderNodes.Name.Equals("Item"))
                                            {

                                                foreach (XmlNode itemListNode in orderNodes.ChildNodes)
                                                {
                                                    if (itemListNode.Name.Equals("ItemID"))
                                                    {
                                                        itemId = itemListNode.InnerText;
                                                    }
                                                    if (itemListNode.Name.Equals("SKU"))
                                                    {
                                                        sku = itemListNode.InnerText;
                                                    }
                                                    if (itemListNode.Name.Equals("Title"))
                                                    {
                                                        title = itemListNode.InnerText;
                                                    }
                                                }
                                            }

                                            if (orderNodes.Name.Equals("QuantityPurchased"))
                                            {
                                                quantityPurchased = orderNodes.InnerText;

                                            }
                                            if (orderNodes.Name.Equals("ConvertedTransactionPrice"))
                                            {
                                                transactionPrice = orderNodes.InnerText;
                                            }

                                            if (orderNodes.Name.Equals("ShippingServiceSelected"))
                                            {
                                                foreach (XmlNode shippingServiceNode in orderNodes.ChildNodes)
                                                {
                                                    if (shippingServiceNode.Name.Equals("ShippingServiceCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingServiceCost = shippingServiceNode.InnerText;
                                                    }
                                                    if (shippingServiceNode.Name.Equals("ShippingInsuranceCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingInsuranceCost = shippingServiceNode.InnerText;
                                                    }
                                                    if (shippingServiceNode.Name.Equals("ShippingServiceAdditionalCost"))
                                                    {
                                                        shippingDetailsServiceOptionShippingServiceAdditionalCost = shippingServiceNode.InnerText;
                                                    }
                                                }

                                            }
                                            if (orderNodes.Name.Equals("ContainingOrder"))
                                            {
                                                foreach (XmlNode ordNode in orderNodes.ChildNodes)
                                                {
                                                    if (ordNode.Name.Equals("OrderID"))
                                                    {
                                                        orderID = ordNode.InnerText;
                                                        isOrder = true;
                                                        objArray[0] = orderID;
                                                        objArray[1] = amountPaid;

                                                        objArray[2] = buyerUserID;
                                                        objArray[3] = createdTime;
                                                        objArray[4] = shippedTime;
                                                        objArray[5] = shippingAddressName;
                                                        objArray[6] = shippingAddressPhone;
                                                        objArray[7] = shippingAddressStreet1;
                                                        objArray[8] = shippingAddressStreet2;
                                                        objArray[9] = shippingAddressCityName;
                                                        objArray[10] = shippingAddressStateOrProvince;
                                                        objArray[11] = shippingAddressPostalCode;
                                                        objArray[12] = shippingAddressCountryName;

                                                        objArray[13] = shippingDetailsInsuranceFee;

                                                        objArray[14] = shippingDetailsServiceOptionShippingServiceCost;
                                                        objArray[15] = shippingDetailsServiceOptionShippingInsuranceCost;
                                                        objArray[16] = shippingDetailsServiceOptionShippingServiceAdditionalCost;
                                                        objArray[17] = shippingAdjustmentAmount;
                                                        objArray[18] = sellingManagerSalesRecordId;
                                                        objArray[19] = itemId;
                                                        objArray[20] = sku;
                                                        objArray[21] = title;
                                                        objArray[22] = quantityPurchased;
                                                        objArray[23] = transactionPrice;

                                                        dataTable.LoadDataRow(objArray, false);

                                                    }
                                                }
                                            }
                                        }

                                        if (orderID == string.Empty)
                                        {
                                            if (itemId != string.Empty)
                                            {

                                                middleLine[count] += "eBay2|" + sellingManagerSalesRecordId + "|" + itemId + "|" + sku + "|";
                                                middleLine[count] += "" + title + "|" + quantityPurchased + "|" + transactionPrice + "\n";
                                                count++;

                                            }

                                            #region Insert data into database
                                            headerLine += "eBay|" + tradingPartnerName + "|" + sellingManagerSalesRecordId + "\n";
                                            headerLine += "eBay1|" + amountPaid + "|" + buyerUserID + "|" + sellingManagerSalesRecordId + "|" + createdTime + "|" + shippedTime + "|";
                                            headerLine += "" + shippingAddressName + "|" + shippingAddressPhone + "|" + shippingAddressStreet1 + "|";
                                            headerLine += "" + shippingAddressStreet2 + "|" + shippingAddressCityName + "|" + shippingAddressStateOrProvince + "|";
                                            headerLine += "" + shippingAddressPostalCode + "|" + shippingAddressCountryName + "|" + shippingDetailsInsuranceFee + "|";
                                            headerLine += "" + shippingDetailsInternationalServiceOptionShippingServiceCost + "|" + shippingDetailsInternationalServiceOptionShippingInsuranceCost + "|" + shippingDetailsInternationalServiceOptionShippingServiceAdditionalCost + "|";
                                            headerLine += "" + shippingDetailsServiceOptionShippingServiceCost + "|" + shippingDetailsServiceOptionShippingInsuranceCost + "|" + shippingDetailsServiceOptionShippingServiceAdditionalCost + "|" + shippingAdjustmentAmount + "\n";

                                            sb.AppendLine(headerLine);


                                            foreach (string item in middleLine)
                                            {
                                                if (item != null)
                                                {
                                                    if (item != string.Empty)
                                                    {
                                                        sb.AppendLine(item);
                                                    }
                                                }
                                            }


                                            SaveOrder(sb, sellingManagerSalesRecordId, tradingPartnerName);

                                            headerLine = string.Empty;
                                            sb = new StringBuilder();
                                            #endregion
                                        }

                                    }
                                    else
                                    {
                                        FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                        StreamWriter wrtLog = new StreamWriter(fs);
                                        wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                                        wrtLog.WriteLine(" There are no orders available on EBay Server \n");
                                        wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                                        wrtLog.WriteLine("\n------------------------------------------------------------\n");
                                        wrtLog.Close();
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                    DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());


                }
                #endregion
                //GetMultipleOrderID(dataTable, tradingPartnerName);
                GetMultipleSku(dataTable, tradingPartnerName);

            }

            return string.Empty;

        }

        #endregion

        /// <summary>
        /// This method is used for Getting Order into Proper format
        /// and Store that order into Axis v5.0 database.
        /// </summary>
        /// <param name="respData">Object of String Buider which will be stored in database.</param>
        private void SaveOrder(StringBuilder sb, string orderId, string tradingPartnerName)
        {

            OdbcConnection con = new OdbcConnection();
            con.ConnectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");

            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = con;
            //Getting Trading partner id.
            cmd.CommandText = "SELECT TradingPartnerID FROM trading_partner_schema WHERE TPName='" + tradingPartnerName + "'";
            con.Open();
            OdbcDataReader dr = cmd.ExecuteReader();

            string tradingPartnerID = string.Empty;

            while (dr.Read())
            {
                tradingPartnerID = dr[0].ToString();
            }

            dr.Close();
            cmd.Dispose();
            con.Close();
            MemoryStream stream = null;
            string Attachname = "eBay  " + tradingPartnerName + " " + orderId;

            try
            {
                con.Open();
                string myString = sb.ToString();
                byte[] myByteArray = System.Text.Encoding.ASCII.GetBytes(myString);
                stream = new MemoryStream(myByteArray);

                BinaryReader mmsfileReader = new BinaryReader((Stream)stream);
                string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //Insert Message data into database.
                OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)stream.Length));
                string serviceText = string.Empty;
                //string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','3','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'4')";
                string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','10','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'4')";
                cmd.Connection = con;
                cmd.CommandText = commandText;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                //For getting Message id from message schema.
                OdbcCommand cmdObj = new OdbcCommand();
                con.Open();
                cmdObj.Connection = con;
                cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                object messageID = cmdObj.ExecuteScalar();
                cmdObj.Dispose();
                con.Close();
                //For adding eBay message into Database.
                string insertMessageCommand = string.Empty;
                if (messageID != null)
                    //insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'3')";
                    insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'10')";
                OdbcCommand cmdFile = new OdbcCommand();
                con.Open();
                cmdFile.Connection = con;
                cmdFile.CommandText = insertMessageCommand;
                cmdFile.Parameters.Add(paramFile);
                cmdFile.ExecuteNonQuery();
                con.Close();
                mmsfileReader.Close();
                stream.Close();

                //For update LastUpdated ebay date.
                string commandDateText = " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tradingPartnerID + "";
                OdbcCommand commandDate = new OdbcCommand();
                commandDate.Connection = con;
                con.Open();
                commandDate.CommandText = commandDateText;
                commandDate.ExecuteNonQuery();
                commandDate.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }

        }

        private void GetMultipleOrderID(DataTable dataTable, string tradingPartnerName)
        {
            try
            {
                string SManagerSalesRecordId = dataTable.Rows[0][18].ToString();
                int count1 = 0;
                int rowIndex = 0;
                string message = string.Empty;
                string headerLine = string.Empty;
                string[] middleLine = new string[dataTable.Rows.Count];
                StringBuilder sb = new StringBuilder();

                #region Insert data into database
                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                headerLine += "eBay1|" + dataTable.Rows[rowIndex][1].ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                sb.AppendLine(headerLine);

                for (rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
                {

                    if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                    {

                        middleLine[count1] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                        middleLine[count1] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                        count1++;
                    }


                    foreach (string item in middleLine)
                    {
                        if (item != null)
                        {
                            if (item != string.Empty)
                            {
                                sb.AppendLine(item);
                            }
                        }
                    }
                    middleLine = new string[dataTable.Rows.Count];

                }
                SaveOrder(sb, SManagerSalesRecordId, tradingPartnerName);

                headerLine = string.Empty;
                sb = new StringBuilder();
                #endregion
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
            }

        }

        public string CompleteSale(string ItemId, string OrderID, DateTime ShippedTime, bool status, string TransactionID, string eBayToken, string consignmentNote,string eBayType)
        {
            string message = string.Empty;
            string headerLine = string.Empty;
            //consignmentNote = "Shipped via Allied Express Consignment note " + consignmentNote + ".";
            StringBuilder sb = new StringBuilder();

            DateTime gmtTime = new DateTime();
            gmtTime = ShippedTime.ToUniversalTime();
            //Construct xml for sending status request to ebay as shipped.
            string xmlText = @"<?xml version=""1.0"" encoding=""iso88591_to_utf8""?><CompleteSaleRequest xmlns=""urn:ebay:apis:eBLBaseComponents""><RequesterCredentials><eBayAuthToken>" + eBayToken + "</eBayAuthToken></RequesterCredentials><ItemID>" + ItemId + "</ItemID><OrderID>" + OrderID + "</OrderID><Shipment><ShipmentTrackingDetails><ShipmentTrackingNumber>" + consignmentNote + "</ShipmentTrackingNumber><ShippingCarrierUsed>" + "Allied Express" + "</ShippingCarrierUsed></ShipmentTrackingDetails><ShippedTime>" + gmtTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "</ShippedTime></Shipment><Shipped>" + status + "</Shipped><TransactionID>" + TransactionID + "</TransactionID></CompleteSaleRequest>";            

            //Put the data into a UTF8 encoded  byte array
            UTF8Encoding encoding = new UTF8Encoding();
            int dataLen = encoding.GetByteCount(xmlText);
            byte[] utf8Bytes = new byte[dataLen];
            Encoding.UTF8.GetBytes(xmlText, 0, xmlText.Length, utf8Bytes, 0);

            #region Setup The Request inc. HTTP Headers
            //Create a new HttpWebRequest object for the SandBox ServerUrl
            HttpWebRequest request = null;

            if (eBayType.Equals("SandBox"))
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.sandbox.ebay.com/ws/api.dll");
            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create("https://api.ebay.com/ws/api.dll");
            }

            //Set Request Method (POST) and Content Type (text/xml)
            request.Method = "POST";
            request.ContentType = "text/xml";
            request.ContentLength = utf8Bytes.Length;

            if (eBayType.Equals("SandBox"))
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-897e-4a18-a573-07714198a5d0");
                request.Headers.Add("X-EBAY-API-CERT-NAME: fb608b80-f4d3-411f-b7a4-eb1f703de66d");
            }
            else
            {
                //Add the Keys to the HTTP Headers
                request.Headers.Add("X-EBAY-API-DEV-NAME: 06287722-9b5e-4af2-aa3e-c6d89520b35d");
                request.Headers.Add("X-EBAY-API-APP-NAME: ZedSyste-1857-4ef5-80db-d9d837e3db06");
                request.Headers.Add("X-EBAY-API-CERT-NAME: ce11b74b-d701-45bd-b097-5b1cc9a42dcb");
            }


            //Add Compatability Level to HTTP Headers
            //Regulates versioning of the XML interface for the API
            request.Headers.Add("X-EBAY-API-COMPATIBILITY-LEVEL: 647");

            //Add function name, SiteID and Detail Level to HTTP Headers
            request.Headers.Add("X-EBAY-API-CALL-NAME: CompleteSale");
            request.Headers.Add("X-EBAY-API-SITEID: 0");

            //Time out = 15 seconds,  set to -1 for no timeout.
            //If times-out - throws a WebException with the
            //Status property set to WebExceptionStatus.Timeout.
            request.Timeout = 150000;

            #endregion

            #region Send The Request
            Stream str = null;

            //Set the request Stream
            str = request.GetRequestStream();
            //Write the request to the Request Stream
            str.Write(utf8Bytes, 0, utf8Bytes.Length);
            str.Close();
            //Get response into stream
            WebResponse resp = request.GetResponse();
            str = resp.GetResponseStream();

            //Process response.
            StreamReader sr = new StreamReader(str);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(sr.ReadToEnd());
            sr.Close();
            str.Close();

            //get the root node, for ease of use
            XmlNode root = xmlDoc["CompleteSaleResponse"];

            if (root["Errors"] != null)
            {
                string errorCode = root["Errors"]["ErrorCode"].InnerText;
                string errorShort = root["Errors"]["ShortMessage"].InnerText;
                string errorLong = root["Errors"]["LongMessage"].InnerText;

                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorCode + "\n " + errorShort);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(errorCode + "\n " + errorLong);
                return errorLong;

            }
            else //If there are no errors.
            {
                string ack = root["Ack"].InnerText;
                return ack;
            }

            #endregion
        }

        /// <summary>
        /// This method is used for update Status of Order in to Axis v5.0
        /// </summary>
        public void UpdateStatus(int messageID, int statusID)
        {
            //Update status of order in mail.
            DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ059, statusID.ToString(), messageID));
            DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ060, statusID.ToString(), messageID));
        }

        private void GetMultipleSku(DataTable dataTable, string tradingPartnerName)
        {
            DataView view = dataTable.DefaultView;
            decimal totalSum = 0;
            view.Sort = "orderID";
            dataTable = view.ToTable();

            #region Format Orders

            int count = 0;
            string headerLine = string.Empty;
            StringBuilder sb = new StringBuilder();
            string[] middleLine = new string[dataTable.Rows.Count];

            #region For Single orderId & SKU.

            if (dataTable.Rows.Count == 1)
            {
                if (dataTable.Rows[0][19].ToString() != string.Empty)
                {
                    middleLine[0] += "eBay2|" + dataTable.Rows[0][18].ToString() + "|" + dataTable.Rows[0][19].ToString() + "|" + dataTable.Rows[0][20].ToString() + "|";
                    middleLine[0] += "" + dataTable.Rows[0][21].ToString() + "|" + dataTable.Rows[0][22].ToString() + "|" + dataTable.Rows[0][23].ToString() + "\n";
                }

                #region Insert data into database


                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[0][18].ToString() + "\n";
                headerLine += "eBay1|" + dataTable.Rows[0][1].ToString() + "|" + dataTable.Rows[0][2].ToString() + "|" + dataTable.Rows[0][18].ToString() + "|" + dataTable.Rows[0][3].ToString() + "|" + dataTable.Rows[0][4].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][5].ToString() + "|" + dataTable.Rows[0][6].ToString() + "|" + dataTable.Rows[0][7].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][8].ToString() + "|" + dataTable.Rows[0][9].ToString() + "|" + dataTable.Rows[0][10].ToString() + "|";
                headerLine += "" + dataTable.Rows[0][11].ToString() + "|" + dataTable.Rows[0][12].ToString() + "|" + dataTable.Rows[0][13].ToString() + "|";
                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                headerLine += "" + dataTable.Rows[0][14].ToString() + "|" + dataTable.Rows[0][15].ToString() + "|" + dataTable.Rows[0][16].ToString() + "|" + dataTable.Rows[0][17].ToString() + "\n";

                sb.AppendLine(headerLine);


                foreach (string item in middleLine)
                {
                    if (item != null)
                    {
                        if (item != string.Empty)
                        {
                            sb.AppendLine(item);
                        }
                    }
                }


                SaveOrder(sb, dataTable.Rows[0][18].ToString(), tradingPartnerName);
                headerLine = string.Empty;
                sb = new StringBuilder();
                count = 0;
                middleLine = new string[dataTable.Rows.Count];
                #endregion

                return;

            }

            #endregion

            for (int rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
            {

                if (rowIndex == dataTable.Rows.Count - 1)//If Last Row
                {
                    if (dataTable.Rows[rowIndex][0].ToString() != dataTable.Rows[rowIndex - 1][0].ToString())//if (count > 0)
                    {
                        #region Insert data into database

                        headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                        headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                        headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                        headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                        sb.AppendLine(headerLine);

                        foreach (string item in middleLine)
                        {
                            if (item != null)
                            {
                                if (item != string.Empty)
                                {
                                    sb.AppendLine(item);
                                }
                            }
                        }


                        SaveOrder(sb, dataTable.Rows[rowIndex - 1][18].ToString(), tradingPartnerName);
                        headerLine = string.Empty;
                        sb = new StringBuilder();
                        count = 0;
                        totalSum = 0;
                        middleLine = new string[dataTable.Rows.Count];
                        #endregion

                        //For the last current row to insert***********************************
                        if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                        {
                            middleLine[0] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                            middleLine[0] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                        }

                        #region Insert data into database


                        headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                        headerLine += "eBay1|" + dataTable.Rows[rowIndex][1].ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                        headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                        headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                        sb.AppendLine(headerLine);


                        foreach (string item in middleLine)
                        {
                            if (item != null)
                            {
                                if (item != string.Empty)
                                {
                                    sb.AppendLine(item);
                                }
                            }
                        }


                        SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                        headerLine = string.Empty;
                        sb = new StringBuilder();
                        count = 0;
                        totalSum = 0;
                        middleLine = new string[dataTable.Rows.Count];
                        #endregion
                    }
                    else
                    {

                        //To Check whether current row orderid == previous row orderId.
                        if (dataTable.Rows[rowIndex][0].ToString() == dataTable.Rows[rowIndex - 1][0].ToString())
                        {

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                            }

                            //totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex - 1][0].ToString());

                            //For the Current row.
                            #region Insert data into database
                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                            sb.AppendLine(headerLine);


                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion
                        }

                        else
                        {
                            //For the Previous row. 
                            #region Insert data into database

                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                            sb.AppendLine(headerLine);

                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex - 1][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());

                                middleLine[0] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[0] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                            }

                            //For the Current row.
                            #region Insert data into database

                            headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                            headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                            headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                            headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                            sb.AppendLine(headerLine);


                            foreach (string item in middleLine)
                            {
                                if (item != null)
                                {
                                    if (item != string.Empty)
                                    {
                                        sb.AppendLine(item);
                                    }
                                }
                            }


                            SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                            headerLine = string.Empty;
                            sb = new StringBuilder();
                            count = 0;
                            totalSum = 0;
                            middleLine = new string[dataTable.Rows.Count];
                            #endregion
                        }
                    }
                }
                else
                {
                    if (rowIndex == 0)
                    {
                        if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                        {
                            totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                            middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                            middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                        }

                    }
                    else
                    {
                        if (dataTable.Rows[rowIndex][0].ToString() == dataTable.Rows[rowIndex - 1][0].ToString())
                        {

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";
                                count++;
                            }
                            if (rowIndex == dataTable.Rows.Count - 1)
                            {
                                #region Insert data into database


                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }

                        }
                        else
                        {

                            if (dataTable.Rows.Count > 1)
                            {
                                #region Insert data into database

                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex - 1][2].ToString() + "|" + dataTable.Rows[rowIndex - 1][18].ToString() + "|" + dataTable.Rows[rowIndex - 1][3].ToString() + "|" + dataTable.Rows[rowIndex - 1][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][5].ToString() + "|" + dataTable.Rows[rowIndex - 1][6].ToString() + "|" + dataTable.Rows[rowIndex - 1][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][8].ToString() + "|" + dataTable.Rows[rowIndex - 1][9].ToString() + "|" + dataTable.Rows[rowIndex - 1][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][11].ToString() + "|" + dataTable.Rows[rowIndex - 1][12].ToString() + "|" + dataTable.Rows[rowIndex - 1][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex - 1][14].ToString() + "|" + dataTable.Rows[rowIndex - 1][15].ToString() + "|" + dataTable.Rows[rowIndex - 1][16].ToString() + "|" + dataTable.Rows[rowIndex - 1][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex - 1][0].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }

                            if (dataTable.Rows[rowIndex][19].ToString() != string.Empty)
                            {
                                totalSum += Convert.ToDecimal(dataTable.Rows[rowIndex][23].ToString());
                                middleLine[rowIndex] += "eBay2|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][19].ToString() + "|" + dataTable.Rows[rowIndex][20].ToString() + "|";
                                middleLine[rowIndex] += "" + dataTable.Rows[rowIndex][21].ToString() + "|" + dataTable.Rows[rowIndex][22].ToString() + "|" + dataTable.Rows[rowIndex][23].ToString() + "\n";

                            }
                            if (rowIndex == dataTable.Rows.Count - 1)
                            {
                                #region Insert data into database


                                headerLine += "eBay|" + tradingPartnerName + "|" + dataTable.Rows[rowIndex][18].ToString() + "\n";
                                headerLine += "eBay1|" + totalSum.ToString() + "|" + dataTable.Rows[rowIndex][2].ToString() + "|" + dataTable.Rows[rowIndex][18].ToString() + "|" + dataTable.Rows[rowIndex][3].ToString() + "|" + dataTable.Rows[rowIndex][4].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][5].ToString() + "|" + dataTable.Rows[rowIndex][6].ToString() + "|" + dataTable.Rows[rowIndex][7].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][8].ToString() + "|" + dataTable.Rows[rowIndex][9].ToString() + "|" + dataTable.Rows[rowIndex][10].ToString() + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][11].ToString() + "|" + dataTable.Rows[rowIndex][12].ToString() + "|" + dataTable.Rows[rowIndex][13].ToString() + "|";
                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + string.Empty + "|";
                                headerLine += "" + dataTable.Rows[rowIndex][14].ToString() + "|" + dataTable.Rows[rowIndex][15].ToString() + "|" + dataTable.Rows[rowIndex][16].ToString() + "|" + dataTable.Rows[rowIndex][17].ToString() + "\n";

                                sb.AppendLine(headerLine);


                                foreach (string item in middleLine)
                                {
                                    if (item != null)
                                    {
                                        if (item != string.Empty)
                                        {
                                            sb.AppendLine(item);
                                        }
                                    }
                                }


                                SaveOrder(sb, dataTable.Rows[rowIndex][18].ToString(), tradingPartnerName);
                                headerLine = string.Empty;
                                sb = new StringBuilder();
                                count = 0;
                                totalSum = 0;
                                middleLine = new string[dataTable.Rows.Count];
                                #endregion
                            }
                            //}

                        }
                    }
                }
            }
            #endregion
        }

        #endregion
    }
}
