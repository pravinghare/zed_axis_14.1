﻿// ===============================================================================
// 
// AlliedExpress.cs
//
// This AlliedExpress class contains the methods which are
// SentBooking , CheckPendingJobs , SaveJobs, Dispatch Jobs etc. 
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace FinancialEntities
{
    /// <summary>
    /// This class is used for manage AlliedExpress Jobs.
    /// </summary>
    public partial class AlliedExpress 
    {
        #region Private Members

        /// <summary>
        /// Private Members for create AlliedExpress Transactions.
        /// </summary>
        private string accountNumber;
        private string fromCompanyName;
        private string fromCompanyPhone;
        private string toCompanyName;
        private string customerName;
        private string prefix;
        private string addressLine1;
        private string addressLine2;
        private string city;
        private string state;
        private string postalCode;
        private string country;
        private string phone;
        private string emailAddress;
        private string transactionID;
        private string type;
        private string accountCode;
        private string m_AEState;
        private string shippingInstruction;
        private Collection<Items> m_Items = new Collection<Items>();
        private string serviceName;
        private string refNumber;
        private string bookedByName;
        private DateTime pickUpTime;
        private string jobBooked;
        private DateTime jobDispatched;
        private DateTime shippedTIme;
        private int itemTotalCount;
        private decimal totalWeight;
        private decimal totalVolume;

        private static AlliedExpress m_AlliedExpress;

        #endregion

        #region Public Properties


        public DateTime ShippedTIme
        {
            get { return shippedTIme; }
            set { shippedTIme = value; }
        }
        /// <summary>
        /// Get or Set user entered Account Number.
        /// </summary>
        public string AccountNumber
        {
            get 
            {
                return accountNumber;
            }
            set
            {
                accountNumber = value;
            }
        }

        /// <summary>
        /// Get or Set From Company name.
        /// </summary>
        public string FromCompanyName
        {
            get
            {
                return fromCompanyName;
            }
            set
            {
                fromCompanyName = value;
            }
        }

        /// <summary>
        /// Get or Set From Company Phone.
        /// </summary>
        public string FromCompanyPhone
        {
            get 
            {
                return fromCompanyPhone;
            }
            set
            {
                fromCompanyPhone = value;
            }
        }

        //Get or set prefix for cannote number.
        public string Prefix
        {
            get 
            {
                return prefix;
            }
            set 
            {
                prefix = value;
            }
        }

        /// <summary>
        /// Get or Set To Company Name.
        /// </summary>
        public string ToCompanyName
        {
            get
            {
                return toCompanyName;
            }
            set
            {
                toCompanyName = value;
            }
        }

        /// <summary>
        /// Get or Set Customer Name of Allied Express.
        /// </summary>
        public string CustomerName
        {
            get
            {
                return customerName;
            }
            set
            {
                customerName = value;
            }
        }

        /// <summary>
        /// Get or Set Address Line1
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return addressLine1;
            }
            set
            {
                addressLine1 = value;
            }
        }

        /// <summary>
        /// Get or Set Address Line2
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return addressLine2;
            }
            set
            {
                addressLine2 = value;
            }
        }

        /// <summary>
        /// Get or Set City of Allied Express.
        /// </summary>
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        /// <summary>
        /// Get or Set State of Allied Express.
        /// </summary>
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        /// <summary>
        /// Get or Set Postal Code.
        /// </summary>
        public string PostalCode
        {
            get
            {
                return postalCode;
            }
            set
            {
                postalCode = value;
            }
        }

        /// <summary>
        /// Get or Set Country of Allied Express.
        /// </summary>
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                country = value;
            }
        }

        /// <summary>
        /// Get or Set Phone of Allied Express Customer.
        /// </summary>
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        /// <summary>
        /// Get or Set Email Address.
        /// </summary>
        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                emailAddress = value;
            }
        }

       

        /// <summary>
        /// Get or Set TransationID.
        /// </summary>
        public string TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }

        /// <summary>
        /// Get or Set SHipping Instruction.
        /// </summary>
        public string ShippingInstruction
        {
            get
            {
                return shippingInstruction;
            }
            set
            {
                shippingInstruction = value;
            }
        }

        /// <summary>
        /// Get or Set Items Array for Allied Express.
        /// </summary>
        public Collection<Items> ItemsArray
        {
            get
            {
                return m_Items;
            }
            set
            {
                m_Items = value;
            }
        }

        /// <summary>
        /// Get or Set Service Name.
        /// </summary>
        public string ServiceName
        {
            get
            {
                return serviceName;
            }
            set
            {
                serviceName = value;
            }
        }

        /// <summary>
        /// Get or Set Ref Number.
        /// </summary>
        public string RefNumber
        {
            get
            {
                return refNumber;
            }
            set
            {
                refNumber = value;
            }
        }

        /// <summary>
        /// Get or Set Booked by name.
        /// </summary>
        public string BookedByName
        {
            get
            {
                return bookedByName;
            }
            set
            {
                bookedByName = value;
            }
        }

        /// <summary>
        /// Get or Set PickUpTime or Ready Date.
        /// </summary>
        public DateTime PickUpTime
        {
            get
            {
                return pickUpTime;
            }
            set
            {
                pickUpTime = value;
            }
        }

        /// <summary>
        /// Get or Set Job Booked of Customer.
        /// </summary>
        public string JobBooked
        {
            get
            {
                return jobBooked;
            }
            set
            {
                jobBooked = value;
            }
        }

        /// <summary>
        /// Get or Set Job Dispatched.
        /// </summary>
        public DateTime JobDispatched
        {
            get
            {
                return jobDispatched;
            }
            set
            {
                jobDispatched = value;
            }
        }

        /// <summary>
        /// Get or Set Item Total Count.
        /// </summary>
        public int ItemTotalCount
        {
            get
            {
                return itemTotalCount;
            }
            set
            {
                itemTotalCount = value;
            }
        }

        /// <summary>
        /// Get or Set Total Weight.
        /// </summary>
        public decimal TotalWeight
        {
            get
            {
                return totalWeight;
            }
            set
            {
                totalWeight = value;
            }
        }

        /// <summary>
        /// Get or Set Total volume
        /// </summary>
        public decimal TotalVolume
        {
            get
            {
                return totalVolume;
            }
            set
            {
                totalVolume = value;
            }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        public string AccountCode
        {
            get { return accountCode;}
            set { accountCode = value; }
        }

        public string AESTate
        {
            get { return m_AEState; }
            set { m_AEState = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor code for generate AlliedExpress Class.
        /// </summary>
        public AlliedExpress()
        {
           

        }

        #endregion

        #region Static Method

        /// <summary>
        /// This static method is used to getting current 
        /// Allied Express Static object.
        /// </summary>
        /// <returns>Allied Express Static Object.</returns>
        public static AlliedExpress GetInstance()
        {
            if (m_AlliedExpress == null)
                m_AlliedExpress = new AlliedExpress();
            return m_AlliedExpress;
        }

        #endregion

        #region Methods and Events

        /// <summary>
        /// This method is used for validate booking before
        /// sending to Allied Express.
        /// </summary>
        /// <param name="bookingParameters">Booking parametes required for Shipping.</param>
        protected void ValidateBooking(List<object> bookingParameters)
        {
            //Add coding Logic here.
        }

        /// <summary>
        /// This method is used for cancel job of Allied Express.
        /// </summary>
        /// <param name="jobParameters">Object of Job Details</param>
        /// <returns>Return true if successful otherwise false.</returns>
        protected bool CancelJob(List<object> jobParameters)
        {
            //Add coding logic here.
            return true;
        }

        /// <summary>
        /// This method is used for Save pending Job of Allied Express.
        /// </summary>
        /// <param name="jobDetails">Object of Job Details.</param>
        /// <param name="message">Message regarding Error or Success.</param>
        /// <returns>Return true if successful otherwise false.</returns>
        protected bool SaveJob(List<object> jobDetails,ref string message)
        {
            //Add coding logic here.
            return true;
        }

        /// <summary>
        /// This method is used for Dispatch job of Allied Express.
        /// </summary>
        /// <param name="jobDetails">Job Details of Allied Express.</param>
        /// <param name="message">Message Regarding Errors or Success.</param>
        /// <returns>Return true if successful otherwise false.</returns>
        protected bool DispatchJob(List<object> jobDetails, ref string message)
        {
            //Add coding logic here.
            return true;
        }

        /// <summary>
        /// This method is used for update Status of Message in Axis 5.0
        /// database.
        /// </summary>
        /// <param name="orderId">Passing Order ID for update message.</param>
        /// <returns>Return true if successful otherwise false.</returns>
        protected bool UpdateStatusOfJob(string orderId)
        {
            //Add coding logic here.
            return true;
        }

        #endregion
    }

    /// <summary>
    /// This class is created for Allied Express Shipping Item Array.
    /// </summary>
    public class Items
    {
        #region Private Members

        /// <summary>
        /// Private Members for create AlliedExpress Items Array.
        /// </summary>
        private string itemName;
        private int quantity;
        private double length;
        private double width;
        private double height;
        private string volume;
        private double weight;
        private string dangerousGoodFlag;
        private string itemID;
        private string orderID;

        #endregion   

        #region Public Properties

        /// <summary>
        /// Get or Set Item Name
        /// </summary>
        public string ItemName
        {
            get
            {
                return itemName;
            }
            set
            {
                itemName = value;
            }
        }

        /// <summary>
        /// Get or Set Quantity of Item
        /// </summary>
        public int Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
            }
        }

        /// <summary>
        /// Get or Set length of Item.
        /// </summary>
        public double Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }

        /// <summary>
        /// Get or Set Width of Item.
        /// </summary>
        public double Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }

        /// <summary>
        /// Get or Set height of Item.
        /// </summary>
        public double Height
        {
            get
            {
                return height;
            }
            set
            {
                height = value;
            }
        }

        /// <summary>
        /// Get or Set Volume of Item.
        /// </summary>
        public string Volume
        {
            get
            {
                return volume;
            }
            set
            {
                volume = value;
            }
        }

        /// <summary>
        /// Get or set Wight of Item.
        /// </summary>
        public double Weight
        {
            get 
            {
                return weight;
            }
            set 
            {
                weight = value;
            }
        }

        public string DangerousGood
        {
            get { return dangerousGoodFlag; }
            set { dangerousGoodFlag = value; }
        }

        /// <summary>
        /// Get and Set ItemID.
        /// </summary>
        public string ItemID
        {
            get { return itemID; }
            set { itemID = value; }
        }

        /// <summary>
        /// GEt and set OrderID.
        /// </summary>
        
        public string OrderID
        {
            get { return orderID; }
            set { orderID = value; }
        }

        #endregion
    }
}
