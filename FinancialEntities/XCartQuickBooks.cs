using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;
using DataProcessingBlocks;
using System.Xml.Serialization;
using TransactionImporter;
using System.Windows.Forms;
using EDI.Message;
using QuickBookEntities;
using System.Collections.ObjectModel;
using Streams;
using System.Xml;
using System.Data.SqlClient;
using System.Collections;
using System.Data;

namespace FinancialEntities
{
    /// <summary>
    /// XCart QuickBooks class for Sales Receipt transaction and Parsing functionality.
    /// </summary>
    [XmlRootAttribute("SalesReceiptAddRq", Namespace = "", IsNullable = false)]
    public class XCartQuickBooks
    {
        #region Private Member Variable
        /// <summary>
        /// Private members required for Quickbooks Sales Receipt.
        /// </summary>
        private int m_messageId;
        private Streams.XCartSalesReceipt m_salesReceiptAdd;
        private Streams.XCartSalesOrder m_salesOrderAdd;
        private Streams.XCartInvoice m_invoiceAdd;
        private string m_companyFileName;
        private string m_response;
        private int m_failureCount;
        private int m_successCount;
        private bool m_requestCancel;
        private bool m_refExists;
        private string itemMapping;
        private string shippingItem;
        private string discountItem;
        private string serviceCost;
        private string AmountAdjusted;
        private string RefNumber = string.Empty;

        #endregion

        #region Construtor

        public XCartQuickBooks(int messageID)
        {
            m_messageId = messageID;
            m_requestCancel = false;
            m_response = string.Empty;
            m_failureCount = 0;
            m_successCount = 0;
      
            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                m_salesReceiptAdd = new XCartSalesReceipt();
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                m_salesOrderAdd = new XCartSalesOrder();
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                m_invoiceAdd = new XCartInvoice();
        }

        public XCartQuickBooks()
        {

        }
        #endregion

        #region Public Properties

        public XCartSalesReceipt SalesReceiptAdd
        {
            get { return m_salesReceiptAdd; }
            set { m_salesReceiptAdd = value; }
        }

        public XCartSalesOrder SalesOrderAdd
        {
            get { return m_salesOrderAdd; }
            set { m_salesOrderAdd = value; }
        }

        public XCartInvoice InvoiceAdd
        {
            get { return m_invoiceAdd; }
            set { m_invoiceAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public string ShippingItem
        {
            get
            {
                return shippingItem;
            }
            set
            {
                shippingItem = value;
            }
        }

        [XmlIgnoreAttribute()]
        public string CompanyFileName
        {
            get { return m_companyFileName; }
            set { m_companyFileName = value; }
        }

        [XmlIgnoreAttribute()]
        public string DiscountItem
        {
            get
            {
                return discountItem;
            }
            set
            {
                discountItem = value;
            }
        }

        [XmlIgnoreAttribute()]
        public string ItemMapping
        {
            get
            {
                return itemMapping;
            }
            set
            {
                itemMapping = value;
            }
        }

        #endregion

        #region Private Methods
        private void AddCustomer(string customerName)
        {
            #region Set Customer Query


            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
            qbXMLMsgsRq.AppendChild(CustomerQueryRq);
            //CustomerQueryRq.SetAttribute("requestID", "0");
            XmlElement FullName = pxmldoc.CreateElement("FullName");
            FullName.InnerText = customerName;
            CustomerQueryRq.AppendChild(FullName);

            string pinput = pxmldoc.OuterXml;

            string resp = string.Empty;
            try
            {
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch
                { }
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                ;

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    string statusSeverity = string.Empty;
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                    {
                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                    }
                    outputXMLDoc.RemoveAll();
                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                    {
                        #region Customer Add Query

                        XmlDocument xmldocadd = new XmlDocument();
                        xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                        xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                        xmldocadd.AppendChild(qbXMLcust);
                        XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                        qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                        qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                        XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                        qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                        //                                    CustomerAddRq.SetAttribute("requestID", "1");
                        XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                        CustomerAddRq.AppendChild(CustomerAdd);

                        XmlElement Name = xmldocadd.CreateElement("Name");
                        Name.InnerText = customerName;
                        CustomerAdd.AppendChild(Name);

                        string custinput = xmldocadd.OuterXml;
                        string respcust = string.Empty;
                        try
                        {
                            try
                            {
                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                            }
                            catch
                            { }
                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {
                            if (respcust != string.Empty)
                            {
                                System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                outputcustXMLDoc.LoadXml(respcust);
                                foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                {
                                    string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                    if (statusSeveritycust == "Error")
                                    {
                                        string msg = "New Customer could not be created into QuickBooks \n ";
                                        msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                        //Task 1435 (Axis 6.0):
                                        ErrorSummary summary = new ErrorSummary(msg);
                                        summary.ShowDialog();
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                }
            }

            #endregion
        }

        private bool AddXCart1(string[] stream)
        {
            if (stream.Length > 7)
            {
                if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                {
                    #region For Sales Receipt
                    m_salesReceiptAdd.SalesReceiptLineAdd.Clear();

                    if (stream[1].Trim() != string.Empty)
                        m_salesReceiptAdd.RefNumber = stream[1].Trim().Replace("'", string.Empty);
                    //If AUTONUMBERING ON
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                        m_salesReceiptAdd.RefNumber = null;

                    if (stream[2].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Addr1 = stream[2].Trim().Replace("'", string.Empty);
                    if (stream[3].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Addr2 = stream[3].Trim().Replace("'", string.Empty);
                    //if (stream[4].Trim() != string.Empty)
                    //    m_salesReceiptAdd.ShipAddress[0].Addr3 = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[4].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].City = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[5].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].PostalCode = stream[5].Trim().Replace("'", string.Empty);
                    if (stream[6].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].State = stream[6].Trim().Replace("'", string.Empty);                    
                    if (stream[7].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Country = stream[7].Trim().Replace("'", string.Empty);
                    if (stream[8].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Note = stream[8].Trim().Replace("'", string.Empty);
                    if (stream[9].Trim() != string.Empty)
                    {
                        if (stream[9].Trim().Length > 31)
                        {
                            m_salesReceiptAdd.PaymentMethodRef.FullName = stream[9].Trim().Remove(31).Replace("'", string.Empty);
                        }
                        else
                        {
                            m_salesReceiptAdd.PaymentMethodRef.FullName = stream[9].Trim().Replace("'", string.Empty);
                        }
                    }
                    else
                        m_salesReceiptAdd.PaymentMethodRef = null;

                    if (stream[10].Trim() != string.Empty)
                    {
                        string txnDt = stream[10].Trim();
                        m_salesReceiptAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[12].Trim() != string.Empty)
                    {
                        serviceCost = stream[12].ToString().Replace("'", string.Empty);
                    }
                    //if (stream[13].Trim() != string.Empty)
                    //{
                    //    m_salesReceiptAdd.ShipMethodRef.FullName = stream[13].ToString().Replace("'", string.Empty);
                    //}
                    //else
                    //    m_salesReceiptAdd.ShipMethodRef = null;
                    //if (stream[16].Trim() != string.Empty)
                    //{
                    //    m_salesReceiptAdd.Memo = stream[16].ToString().Replace("'", string.Empty);
                    //}
                    #endregion
                }
                else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                {
                    #region For Sales Order
                    m_salesOrderAdd.SalesOrderLineAdd.Clear();
                    if (stream[1].Trim() != string.Empty)
                        m_salesOrderAdd.RefNumber = stream[1].Trim().Replace("'", string.Empty);
                    //If AUTONUMBERING ON
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                        m_salesOrderAdd.RefNumber = null;

                    if (stream[2].Trim() != string.Empty)
                    {
                        m_salesOrderAdd.CustomerRef.FullName = stream[2].Trim().Replace("'", string.Empty);
                        AddCustomer(stream[2].Trim().Replace("'", string.Empty));
                    }

                    if (stream[2].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Addr1 = stream[2].Trim().Replace("'", string.Empty);
                    if (stream[3].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Addr2 = stream[3].Trim().Replace("'", string.Empty);
                    //if (stream[4].Trim() != string.Empty)
                    //    m_salesOrderAdd.ShipAddress[0].Addr3 = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[4].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].City = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[5].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].PostalCode = stream[5].Trim().Replace("'", string.Empty);
                    if (stream[6].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].State = stream[6].Trim().Replace("'", string.Empty);                   
                    if (stream[7].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Country = stream[7].Trim().Replace("'", string.Empty);
                    if (stream[8].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Note = stream[8].Trim().Replace("'", string.Empty);
                    //if (stream[9].Trim() != string.Empty)
                    //{
                    //    if (stream[9].Trim().Length > 31)
                    //    {
                    //        m_salesOrderAdd.PaymentMethodRef.FullName = stream[9].Trim().Remove(31).Replace("'", string.Empty);
                    //    }
                    //    else
                    //    {
                    //        m_salesOrderAdd.PaymentMethodRef.FullName = stream[9].Trim().Replace("'", string.Empty);
                    //    }
                    //}
                    //else
                    //    m_salesOrderAdd.PaymentMethodRef = null;

                    if (stream[10].Trim() != string.Empty)
                    {
                        string txnDt = stream[10].Trim();
                        m_salesOrderAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[12].Trim() != string.Empty)
                    {
                        serviceCost = stream[12].ToString().Replace("'", string.Empty);
                    }
                    //if (stream[13].Trim() != string.Empty)
                    //{
                    //    m_salesOrderAdd.ShipMethodRef.FullName = stream[13].ToString().Replace("'", string.Empty);
                    //}
                    //else
                    //    m_salesOrderAdd.ShipMethodRef = null;
                    //if (stream[16].Trim() != string.Empty)
                    //{
                    //    m_salesOrderAdd.Memo = stream[16].ToString().Replace("'", string.Empty);
                    //}
                    #endregion
                }
                else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                {
                    #region For Invoice
                    m_invoiceAdd.InvoiceLineAdd.Clear();
                    if (stream[1].Trim() != string.Empty)
                        m_invoiceAdd.RefNumber = stream[1].Trim().Replace("'", string.Empty);
                    //If AUTONUMBERING ON
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                        m_invoiceAdd.RefNumber = null;

                    if (stream[2].Trim() != string.Empty)
                    {
                        m_invoiceAdd.CustomerRef.FullName = stream[2].Trim().Replace("'", string.Empty);
                        AddCustomer(stream[2].Trim().Replace("'", string.Empty));
                    }

                    if (stream[2].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Addr1 = stream[2].Trim().Replace("'", string.Empty);
                    if (stream[3].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Addr2 = stream[3].Trim().Replace("'", string.Empty);
                    //if (stream[4].Trim() != string.Empty)
                    //    m_invoiceAdd.ShipAddress[0].Addr3 = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[4].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].City = stream[4].Trim().Replace("'", string.Empty);
                    if (stream[5].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].PostalCode = stream[5].Trim().Replace("'", string.Empty);
                    if (stream[6].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].State = stream[6].Trim().Replace("'", string.Empty);                    
                    if (stream[7].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Country = stream[7].Trim().Replace("'", string.Empty);
                    if (stream[8].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Note = stream[8].Trim().Replace("'", string.Empty);
                    //if (stream[9].Trim() != string.Empty)
                    //{
                    //    if (stream[9].Trim().Length > 31)
                    //    {
                    //        m_invoiceAdd.PaymentMethodRef.FullName = stream[9].Trim().Remove(31).Replace("'", string.Empty);
                    //    }
                    //    else
                    //    {
                    //        m_invoiceAdd.PaymentMethodRef.FullName = stream[9].Trim().Replace("'", string.Empty);
                    //    }
                    //}
                    //else
                    //    m_invoiceAdd.PaymentMethodRef = null;

                    if (stream[10].Trim() != string.Empty)
                    {
                        string txnDt = stream[10].Trim();
                        m_invoiceAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[12].Trim() != string.Empty)
                    {
                        serviceCost = stream[12].ToString().Replace("'", string.Empty);
                    }
                    //if (stream[13].Trim() != string.Empty)
                    //{
                    //    m_invoiceAdd.ShipMethodRef.FullName = stream[13].ToString().Replace("'", string.Empty);
                    //}
                    //else
                    //    m_invoiceAdd.ShipMethodRef = null;
                    //if (stream[16].Trim() != string.Empty)
                    //{
                    //    m_invoiceAdd.Memo = stream[16].ToString().Replace("'", string.Empty);
                    //}
                    #endregion
                }
                return true;
            }
            else
            {
                throw new Exception("Attachment is not in correct format.");
            }

        }

        private void CreateItem(string[] stream)
        {
            try
            {
                DefaultAccountSettings defaultSettings = new DefaultAccountSettings();
                defaultSettings = defaultSettings.GetDefaultAccountSettings();
                //Code to check whether Item Name conatins ":"
                string ItemName = stream[5].Trim();
                string[] arr = new string[15];
                if (ItemName.Contains(":"))
                {
                    arr = ItemName.Split(':');
                }
                else
                {
                    arr[0] = stream[5].Trim();
                }

                #region Set Item Query

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] != null && arr[i] != string.Empty)
                    {
                        #region Passing Items Query
                        XmlDocument pxmldoc = new XmlDocument();
                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                        pxmldoc.AppendChild(qbXML);
                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                        qbXML.AppendChild(qbXMLMsgsRq);
                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                        ItemQueryRq.SetAttribute("requestID", "1");

                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                        FullName.InnerText = arr[i];
                        ItemQueryRq.AppendChild(FullName);

                        string pinput = pxmldoc.OuterXml;

                        string resp = string.Empty;
                        try
                        {
                            try
                            {
                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                            }
                            catch
                            { }
                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {

                            if (resp != string.Empty)
                            {
                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                outputXMLDoc.LoadXml(resp);
                                string statusSeverity = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                {
                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                }
                                outputXMLDoc.RemoveAll();
                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                {
                                    //statusMessage += "\n ";
                                    //statusMessage += oNode.Attributes["statusMessage"].Value.ToString();

                                    if (defaultSettings.Type == "NonInventoryPart")
                                    {
                                        #region Item NonInventory Add Query

                                        XmlDocument ItemNonInvendoc = new XmlDocument();
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                        XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                        ItemNonInvendoc.AppendChild(qbXMLINI);
                                        XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                        qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                        qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                        ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                        ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                        XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                        ININame.InnerText = arr[i];
                                        //ININame.InnerText = dr["ItemFullName"].ToString();
                                        ItemNonInventoryAdd.AppendChild(ININame);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }

                                        //Adding Tax Code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }

                                        XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;
                                        //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                        //Adding Desc and Rate

                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemNonInvendoc.CreateElement("Price");
                                            ISCost.InnerText = stream[4].Trim();
                                            INISalesAndPurchase.AppendChild(ISCost);
                                            IsPresent = true;
                                        }

                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                            INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                            //INIFullName.InnerText = "Sales";
                                            INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                        }

                                        string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                        //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                        string respItemNonInvendoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strtest2 = respItemNonInvendoc;

                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "Service")
                                    {
                                        #region Item Service Add Query

                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                        ItemServiceAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                        ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                        XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                        ItemServiceAdd.AppendChild(NameIS);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                ItemServiceAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }
                                        //Adding Tax code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                            }
                                        }


                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;
                                        //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                        //Adding Desc and Rate

                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemServiceAdddoc.CreateElement("Price");
                                            ISCost.InnerText = stream[4].Trim();
                                            ISSalesAndPurchase.AppendChild(ISCost);
                                            IsPresent = true;
                                        }

                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                            ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                            //Adding IncomeAccount FullName.
                                            XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                            ISIncomeAccountRef.AppendChild(ISFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                        }

                                        string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                        string respItemServiceAddinputdoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strTest3 = respItemServiceAddinputdoc;
                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "InventoryPart")
                                    {
                                        #region Inventory Add Query
                                        XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                        ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                        ItemInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                        XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                        ItemInventoryAdd.AppendChild(NameIS);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                ItemInventoryAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                //Adding Tax code Element.
                                                XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }

                                        //Adding Desc and Rate

                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                            ISCost.InnerText = stream[4].Trim();
                                            ItemInventoryAdd.AppendChild(ISCost);
                                        }


                                        //Adding IncomeAccountRef
                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                            ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                        }

                                        //Adding COGSAccountRef
                                        if (defaultSettings.COGSAccount != string.Empty)
                                        {
                                            XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                            ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                            XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                            INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                        }

                                        //Adding AssetAccountRef
                                        if (defaultSettings.AssetAccount != string.Empty)
                                        {
                                            XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                            ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                            XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                            INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                        }

                                        string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                        string respItemInventoryAddinputdoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strTest4 = respItemInventoryAddinputdoc;
                                        #endregion
                                    }
                                }
                            }

                        }

                        #endregion
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        private void CreateNewItem(string[] stream)
        {
            try
            {
                DefaultAccountSettings defaultSettings = new DefaultAccountSettings();
                defaultSettings = defaultSettings.GetDefaultAccountSettings();
                //Code to check whether Item Name conatins ":"
                string ItemName = stream[2].Trim();

                #region Passing Items Query
                XmlDocument pxmldoc = new XmlDocument();
                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                pxmldoc.AppendChild(qbXML);
                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                qbXML.AppendChild(qbXMLMsgsRq);
                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                qbXMLMsgsRq.AppendChild(ItemQueryRq);
                ItemQueryRq.SetAttribute("requestID", "1");

                XmlElement FullName = pxmldoc.CreateElement("FullName");
                FullName.InnerText = ItemName;
                ItemQueryRq.AppendChild(FullName);

                string pinput = pxmldoc.OuterXml;

                string resp = string.Empty;
                try
                {
                    try
                    {
                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    }
                    catch
                    { }
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
                finally
                {

                    if (resp != string.Empty)
                    {
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        string statusSeverity = string.Empty;
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                        }
                        outputXMLDoc.RemoveAll();
                        if (statusSeverity == "Error" || statusSeverity == "Warn")
                        {


                            if (defaultSettings.Type == "NonInventoryPart")
                            {
                                #region Item NonInventory Add Query

                                XmlDocument ItemNonInvendoc = new XmlDocument();
                                ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                ItemNonInvendoc.AppendChild(qbXMLINI);
                                XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                ININame.InnerText = ItemName;
                                //ININame.InnerText = dr["ItemFullName"].ToString();
                                ItemNonInventoryAdd.AppendChild(ININame);


                                //Adding Tax Code Element.
                                if (defaultSettings.TaxCode != string.Empty)
                                {
                                    if (defaultSettings.TaxCode.Length < 4)
                                    {
                                        XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                        ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                        XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                        INIFullName.InnerText = defaultSettings.TaxCode;
                                        INISalesTaxCodeRef.AppendChild(INIFullName);
                                    }
                                }

                                XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                bool IsPresent = false;
                                //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                //Adding Desc and Rate

                                if (stream[3].Trim() != string.Empty)
                                {
                                    XmlElement ISCost = ItemNonInvendoc.CreateElement("Price");
                                    ISCost.InnerText = stream[3].Trim();
                                    INISalesAndPurchase.AppendChild(ISCost);
                                    IsPresent = true;
                                }

                                if (defaultSettings.IncomeAccount != string.Empty)
                                {
                                    XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                    INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                    XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                    //INIFullName.InnerText = "Sales";
                                    INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                    INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                    IsPresent = true;
                                }

                                if (IsPresent == true)
                                {
                                    ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                }

                                string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                string respItemNonInvendoc = string.Empty;
                                try
                                {
                                    try
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                    }
                                    catch
                                    { }
                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                    respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                }
                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                string strtest2 = respItemNonInvendoc;

                                #endregion
                            }
                            else if (defaultSettings.Type == "Service")
                            {
                                #region Item Service Add Query

                                XmlDocument ItemServiceAdddoc = new XmlDocument();
                                ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                ItemServiceAdddoc.AppendChild(qbXMLIS);
                                XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                ItemServiceAddRq.SetAttribute("requestID", "1");

                                XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                NameIS.InnerText = ItemName;
                                //NameIS.InnerText = dr["ItemFullName"].ToString();
                                ItemServiceAdd.AppendChild(NameIS);


                                //Adding Tax code Element.
                                if (defaultSettings.TaxCode != string.Empty)
                                {
                                    if (defaultSettings.TaxCode.Length < 4)
                                    {
                                        XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                        ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                        XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                        INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                        INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                    }
                                }


                                XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                bool IsPresent = false;
                                //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                //Adding Desc and Rate

                                if (stream[3].Trim() != string.Empty)
                                {
                                    XmlElement ISCost = ItemServiceAdddoc.CreateElement("Price");
                                    ISCost.InnerText = stream[3].Trim();
                                    ISSalesAndPurchase.AppendChild(ISCost);
                                    IsPresent = true;
                                }

                                if (defaultSettings.IncomeAccount != string.Empty)
                                {
                                    XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                    ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                    //Adding IncomeAccount FullName.
                                    XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                    ISFullName.InnerText = defaultSettings.IncomeAccount;
                                    ISIncomeAccountRef.AppendChild(ISFullName);

                                    IsPresent = true;
                                }

                                if (IsPresent == true)
                                {
                                    ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                }

                                string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                string respItemServiceAddinputdoc = string.Empty;
                                try
                                {
                                    try
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                    }
                                    catch
                                    { }
                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                    respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);

                                }
                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                string strTest3 = respItemServiceAddinputdoc;
                                #endregion
                            }
                            else if (defaultSettings.Type == "InventoryPart")
                            {
                                #region Inventory Add Query
                                XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                ItemInventoryAddRq.SetAttribute("requestID", "1");

                                XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                NameIS.InnerText = ItemName;
                                //NameIS.InnerText = dr["ItemFullName"].ToString();
                                ItemInventoryAdd.AppendChild(NameIS);


                                if (defaultSettings.TaxCode != string.Empty)
                                {
                                    if (defaultSettings.TaxCode.Length < 4)
                                    {
                                        //Adding Tax code Element.
                                        XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                        ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                        XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                        INIFullName.InnerText = defaultSettings.TaxCode;
                                        INISalesTaxCodeRef.AppendChild(INIFullName);
                                    }
                                }

                                //Adding Desc and Rate

                                if (stream[3].Trim() != string.Empty)
                                {
                                    XmlElement ISCost = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                    ISCost.InnerText = stream[3].Trim();
                                    ItemInventoryAdd.AppendChild(ISCost);
                                }


                                //Adding IncomeAccountRef
                                if (defaultSettings.IncomeAccount != string.Empty)
                                {
                                    XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                    ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                    XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                    INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                    INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                }

                                //Adding COGSAccountRef
                                if (defaultSettings.COGSAccount != string.Empty)
                                {
                                    XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                    ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                    XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                    INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                    INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                }

                                //Adding AssetAccountRef
                                if (defaultSettings.AssetAccount != string.Empty)
                                {
                                    XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                    ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                    XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                    INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                    INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                }

                                string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                string respItemInventoryAddinputdoc = string.Empty;
                                try
                                {
                                    try
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                    }
                                    catch
                                    { }
                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                    respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);

                                }
                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                string strTest4 = respItemInventoryAddinputdoc;
                                #endregion
                            }
                        }
                    }

                }

                #endregion


            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        private bool AddXCart2(string[] stream)
        {
            DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();

            #region Checking and setting TaxCode


            string TaxRateValue = string.Empty;
            string ItemSaleTaxFullName = string.Empty;

            if (defaultSettings.GrossToNet == "1")
            {

                if (defaultSettings.TaxCode != string.Empty)
                {
                    ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, defaultSettings.TaxCode);

                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                }

            }
            #endregion

            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                #region For Sales Receipt

                FinancialEntities.SalesReceiptLineAdd salesReceiptLineAdd = new SalesReceiptLineAdd();

                if (stream[5].Trim() != string.Empty)
                {
                    if (stream[5].Trim().Length > 31)
                    {
                        stream[5] = stream[5].Trim().Remove(31, (stream[5].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    try
                    {
                        if (stream[5].Trim().Contains(":"))
                            stream[5] = stream[5].Remove(stream[5].IndexOf(":") + 1, 1);
                    }
                    catch
                    { }
                    //stream[2] = stream[2].Replace(" ", string.Empty);
                    salesReceiptLineAdd.ItemRef.FullName = stream[5].Trim().Replace("'", string.Empty);
                }
                else
                    salesReceiptLineAdd.ItemRef = null;

                if (stream[6].Trim() != string.Empty)
                    salesReceiptLineAdd.Desc = stream[6].Trim().Replace("'", string.Empty);

                //if (stream[4].Trim() != string.Empty)
                //    salesReceiptLineAdd.Rate = stream[4].Trim().Replace("'", string.Empty);

                #region newly calculated rate
                if (stream[4].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA" ||
                                CommonUtilities.GetInstance().CountryVersion == "US")
                        {
                            decimal Rate = Convert.ToDecimal(stream[4].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));

                            salesReceiptLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            salesReceiptLineAdd.Rate = stream[4].Trim();
                        }
                    }
                    else
                    {
                        salesReceiptLineAdd.Rate = stream[4].Trim();
                    }
                }
                #endregion

                //if (stream[5].Trim() != string.Empty)
                //{
                //    double amount = 0;
                //    try
                //    {
                //        if (double.TryParse(stream[5].Trim(), out amount))
                //        {
                //            //string str = Math.Round(amount, 2).ToString();
                //            salesReceiptLineAdd.Amount = string.Format("{0:00.00}", Math.Round(amount, 2));
                //        }
                //        else
                //            salesReceiptLineAdd.Amount = stream[5].Trim();
                //    }
                //    catch
                //    {
                //        salesReceiptLineAdd.Amount = stream[5].Trim();
                //    }
                //}
                //if (stream[7].Trim() != string.Empty)
                //    salesReceiptLineAdd.Quantity = stream[7].Trim().Replace("'", string.Empty);
                m_salesReceiptAdd.SalesReceiptLineAdd.Add(salesReceiptLineAdd);
                #endregion
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                #region For Sales Order

                FinancialEntities.SalesOrderLineAdd salesOrderLineAdd = new SalesOrderLineAdd();

                if (stream[5].Trim() != string.Empty)
                {
                    if (stream[5].Trim().Length > 31)
                    {
                        stream[5] = stream[5].Trim().Remove(31, (stream[5].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    try
                    {
                        if (stream[5].Trim().Contains(":"))
                            stream[5] = stream[5].Remove(stream[5].IndexOf(":") + 1, 1);
                    }
                    catch
                    { }
                    //stream[2] = stream[2].Replace(" ", string.Empty);
                    salesOrderLineAdd.ItemRef.FullName = stream[5].Trim().Replace("'", string.Empty);
                }
                else
                    salesOrderLineAdd.ItemRef = null;

                if (stream[6].Trim() != string.Empty)
                    salesOrderLineAdd.Desc = stream[6].Trim().Replace("'", string.Empty);

                //if (stream[4].Trim() != string.Empty)
                //    salesOrderLineAdd.Rate = stream[4].Trim().Replace("'", string.Empty);

                #region newly calculated rate
                if (stream[4].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA" ||
                                CommonUtilities.GetInstance().CountryVersion == "US")
                        {
                            decimal Rate = Convert.ToDecimal(stream[4].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));

                            salesOrderLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            salesOrderLineAdd.Rate = stream[4].Trim();
                        }
                    }
                    else
                    {
                        salesOrderLineAdd.Rate = stream[4].Trim();
                    }
                }
                #endregion

                //if (stream[5].Trim() != string.Empty)
                //{
                //    double amount = 0;
                //    try
                //    {
                //        if (double.TryParse(stream[5].Trim(), out amount))
                //        {
                //            //string str = Math.Round(amount, 2).ToString();
                //            salesOrderLineAdd.Amount = string.Format("{0:00.00}", Math.Round(amount, 2));
                //        }
                //        else
                //            salesOrderLineAdd.Amount = stream[5].Trim();
                //    }
                //    catch
                //    {
                //        salesOrderLineAdd.Amount = stream[5].Trim();
                //    }
                //}
                //if (stream[7].Trim() != string.Empty)
                //    salesOrderLineAdd.Quantity = stream[7].Trim().Replace("'", string.Empty);
                m_salesOrderAdd.SalesOrderLineAdd.Add(salesOrderLineAdd);

                #endregion
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                #region For Invoice

                FinancialEntities.InvoiceLineAdd invoiceLineAdd = new InvoiceLineAdd();

                if (stream[5].Trim() != string.Empty)
                {
                    if (stream[5].Trim().Length > 31)
                    {
                        stream[5] = stream[5].Trim().Remove(31, (stream[5].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    try
                    {
                        if (stream[5].Trim().Contains(":"))
                            stream[5] = stream[5].Remove(stream[5].IndexOf(":") + 1, 1);
                    }
                    catch
                    { }
                    //stream[2] = stream[2].Replace(" ", string.Empty);
                    invoiceLineAdd.ItemRef.FullName = stream[5].Trim().Replace("'", string.Empty);
                }
                else
                    invoiceLineAdd.ItemRef = null;

                if (stream[6].Trim() != string.Empty)
                    invoiceLineAdd.Desc = stream[6].Trim().Replace("'", string.Empty);

                //if (stream[4].Trim() != string.Empty)
                //    invoiceLineAdd.Rate = stream[4].Trim().Replace("'", string.Empty);

                #region newly calculated rate
                if (stream[4].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA" ||
                                CommonUtilities.GetInstance().CountryVersion == "US")
                        {
                            decimal Rate = Convert.ToDecimal(stream[4].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));

                            invoiceLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            invoiceLineAdd.Rate = stream[4].Trim();
                        }
                    }
                    else
                    {
                        invoiceLineAdd.Rate = stream[4].Trim();
                    }
                }
                #endregion

                //if (stream[5].Trim() != string.Empty)
                //{
                //    double amount = 0;
                //    try
                //    {
                //        if (double.TryParse(stream[5].Trim(), out amount))
                //        {
                //            //string str = Math.Round(amount, 2).ToString();
                //            invoiceLineAdd.Amount = string.Format("{0:00.00}", Math.Round(amount, 2));
                //        }
                //        else
                //            invoiceLineAdd.Amount = stream[5].Trim();
                //    }
                //    catch
                //    {
                //        invoiceLineAdd.Amount = stream[5].Trim();
                //    }
                //}
                //if (stream[7].Trim() != string.Empty)
                //    invoiceLineAdd.Quantity = stream[7].Trim().Replace("'", string.Empty);
                m_invoiceAdd.InvoiceLineAdd.Add(invoiceLineAdd);

                #endregion
            }
            return true;
        }

        //For Shipping details.
        private bool AddXCart2Shipping()
        {

            if (this.serviceCost != null && this.serviceCost.Trim() != string.Empty && Convert.ToDecimal(this.serviceCost) > 0)
            {
                if (this.ShippingItem != null && this.ShippingItem.Trim() != string.Empty)
                {
                    if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                    {
                        #region For Sales Receipt
                        FinancialEntities.SalesReceiptLineAdd salesReceiptLineAdd = new SalesReceiptLineAdd();

                        salesReceiptLineAdd.ItemRef.FullName = this.ShippingItem;
                        decimal cost = Convert.ToDecimal(this.serviceCost);
                        salesReceiptLineAdd.Amount = string.Format("{0:0.00}", cost);

                        m_salesReceiptAdd.SalesReceiptLineAdd.Add(salesReceiptLineAdd);
                        #endregion
                    }
                    if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                    {
                        #region For Sales Order
                        FinancialEntities.SalesOrderLineAdd salesOrderLineAdd = new SalesOrderLineAdd();
                        salesOrderLineAdd.ItemRef.FullName = this.ShippingItem;
                        decimal cost = Convert.ToDecimal(this.serviceCost);
                        salesOrderLineAdd.Amount = string.Format("{0:0.00}", cost);
                        m_salesOrderAdd.SalesOrderLineAdd.Add(salesOrderLineAdd);
                        #endregion
                    }
                    if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                    {
                        #region For Invoice
                        FinancialEntities.InvoiceLineAdd invoiceLineAdd = new InvoiceLineAdd();
                        invoiceLineAdd.ItemRef.FullName = this.ShippingItem;
                        decimal cost = Convert.ToDecimal(this.serviceCost);
                        invoiceLineAdd.Amount = string.Format("{0:0.00}", cost);
                        m_invoiceAdd.InvoiceLineAdd.Add(invoiceLineAdd);
                        #endregion
                    }
                }

            }



            return true;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used for parse the OSCommerce order 
        /// for Sales Receipt Add to QuickBooks.
        /// </summary>
        /// <param name="attachmentContent">attachment content of order.</param>
        public void Parse(string attachmentContent, string appName)
        {
            XCartQuickBooks qb = new XCartQuickBooks(this.m_messageId);
            this.CompanyFileName = appName;
            ShipAddress shipAddress = new ShipAddress();

            #region For particular transaction

            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                m_salesReceiptAdd.ShipAddress = new Collection<ShipAddress>();
                m_salesReceiptAdd.ShipAddress.Add(shipAddress);
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                m_salesOrderAdd.ShipAddress = new Collection<ShipAddress>();
                m_salesOrderAdd.ShipAddress.Add(shipAddress);
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                m_invoiceAdd.ShipAddress = new Collection<ShipAddress>();
                m_invoiceAdd.ShipAddress.Add(shipAddress);
            }

            #endregion

            DataTable tempdt = new DataTable();
            int startIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
            string messageResp = string.Empty;
            int m_failureCount = 0;
            int m_successCount = 0;
            while (endIndex <= (attachmentContent.Length))
            {
                string[] stream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                if (stream[0].Trim().Equals("XCart1"))
                {
                    if (!AddXCart1(stream))
                    {
                        throw new Exception("Error in parsing XCart stream in the attachment.");
                    }
                }
                else if (stream[0].Trim().Equals("XCart2"))
                {
                    if (!AddXCart2(stream))
                    {
                        throw new Exception("Error in parsing the XCart Item stream in the attachment.");
                    }

                }

                startIndex = endIndex + Constants.NEWLINE.Length;
                if (startIndex < attachmentContent.Length)
                {
                    endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                }
                else
                {
                    break;
                }
            }
            //Add the Shipping details.
            AddXCart2Shipping();


            if (!ExportToQuickBooks(ref messageResp, appName))
                m_failureCount++;
            else
                m_successCount++;



            StringBuilder sb = new StringBuilder();
            sb.Append(messageResp);

            if (CommonUtilities.GetInstance().IsInboundFlag == true)
            {
                CommonUtilities.WriteErrorLog(sb.ToString());

                if (m_failureCount > 0)
                    CommonUtilities.GetInstance().FailureCount = CommonUtilities.GetInstance().FailureCount + 1;
                else if (m_successCount > 0)
                    CommonUtilities.GetInstance().ProcessedCount = CommonUtilities.GetInstance().ProcessedCount + 1;

            }
            else
                CommonUtilities.GetInstance().DisplaySummary(m_failureCount, m_successCount, sb, tempdt, 0);

            if (m_failureCount > 0)
            {
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Failed);
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Failed);
                CommonUtilities.WriteErrorLog(messageResp);
            }
            else
            {
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Processed);
                new MessageController().ChangeAttachmentStatus(this.m_messageId, (int)MessageStatus.Processed);
            }
        }

        /// <summary>
        /// This method is used to Import Sales Receipt Data into Quickbooks
        /// Company File.
        /// </summary>
        /// <param name="statusMessage">Status message with errors or Success message.</param>
        /// <param name="requestText">Error or Success Mesage text</param>
        /// <param name="rowcount">Number of Row Count.</param>
        /// <param name="AppName">Application Name (l.e Axis 5.0)</param>
        /// <returns>return true means data imported successfully otherwise false.</returns>
        public bool ExportToQuickBooks(ref string statusMessage, string AppName)
        {

            string typeTran = string.Empty;

            #region Check whether which transaction
            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                typeTran = "SalesReceipt";
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                typeTran = "SalesOrder";
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                typeTran = "Invoice";
            #endregion

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<FinancialEntities.XCartQuickBooks>.Save(this, fileName);
            }
            catch
            {
                //statusMessage += "\n ";
                //statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesReceiptAddRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptAddRq = requestXmlDoc.CreateElement(typeTran + "AddRq");
            inner.AppendChild(SalesReceiptAddRq);

            //Create SalesReceiptAdd aggregate and fill in field values for it
            //System.Xml.XmlElement SalesReceiptAdd = requestXmlDoc.CreateElement("SalesReceiptAdd");
            //SalesReceiptAddRq.AppendChild(SalesReceiptAdd);

            requestXML = requestXML.Replace("<" + typeTran + "LineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<" + typeTran + "LineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</" + typeTran + "LineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ShipMethodRef />", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            SalesReceiptAddRq.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/" + typeTran + "AddRq/" + typeTran + "Add"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }

            string requestText = requestXmlDoc.OuterXml;

            string resp = string.Empty;
            try
            {
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch
                { }
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {

                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + typeTran + "AddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                //requestXmlDoc.OuterXml
                if (typeTran.Equals("SalesReceipt"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofXCartSalesReceipt(this);
                else if (typeTran.Equals("SalesOrder"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofXCartSalesOrder(this);
                else if (typeTran.Equals("Invoice"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofXCartInvoice(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        #endregion
    }
}
