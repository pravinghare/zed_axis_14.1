﻿// ===============================================================================
// 
// QuickBooks.cs
//
// This QuickBooks class contains the methods which are
// Import To QuickBooks ,Parsing of Orders etc. 
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;
using DataProcessingBlocks;
using System.Xml.Serialization;
using TransactionImporter;
using System.Windows.Forms;
using EDI.Message;
using QuickBookEntities;
using System.Collections.ObjectModel;
using Streams;
using System.Xml;
using System.Data.SqlClient;
using System.Collections;
using System.Data;



namespace FinancialEntities
{
    /// <summary>
    /// QuickBooks class for Sales Receipt transaction and Parsing functionality.
    /// </summary>
    [XmlRootAttribute("SalesReceiptAddRq", Namespace = "", IsNullable = false)]
    public class QuickBooks
    {
        #region Private Member Variable
        /// <summary>
        /// Private members required for Quickbooks Sales Receipt.
        /// </summary>
        private int m_messageId;
        
        //for Sales Receipt
        private Streams.SalesReceipt m_salesReceiptAdd;

        //for Sales Order
        private Streams.SalesOrder m_salesOrderAdd;

        //for Invoice
        private Streams.Invoice m_invoiceAdd;

        //private Streams.InvoiceAdd m_Invoice;
        private string m_companyFileName;
        private string m_response;
        private int m_failureCount;
        private int m_successCount;
        private bool m_requestCancel;
        private bool m_refExists;
        private string itemMapping;
        private string shippingItem;
        private string discountItem;
        private string serviceCost;
        private string AmountAdjusted;
        private string RefNumber = string.Empty;

        #endregion

        #region Construtor

        public QuickBooks(int messageID)
        {
            //Add Constructor code here..

            m_messageId = messageID;
            m_requestCancel = false;
            m_response = string.Empty;
            m_failureCount = 0;
            m_successCount = 0;

            if(CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                m_salesReceiptAdd = new SalesReceipt();
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                m_salesOrderAdd = new SalesOrder();
            else if(CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                m_invoiceAdd = new Invoice();
        }

        public QuickBooks()
        {

        }
        #endregion

        #region Public Properties

        public SalesReceipt SalesReceiptAdd
        {
            get { return m_salesReceiptAdd; }
            set { m_salesReceiptAdd = value; }
        }

        public SalesOrder SalesOrderAdd
        {
            get { return m_salesOrderAdd; }
            set { m_salesOrderAdd = value; }
        }

        public Invoice InvoiceAdd
        {
            get { return m_invoiceAdd; }
            set { m_invoiceAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public string ShippingItem
        {
            get
            {
                return shippingItem;
            }
            set
            {
                shippingItem = value;
            }
        }

        [XmlIgnoreAttribute()]
        public string DiscountItem
        {
            get
            {
                return discountItem;
            }
            set
            {
                discountItem = value;
            }
        }

        [XmlIgnoreAttribute()]
        public string ItemMapping
        {
            get
            {
                return itemMapping;
            }
            set
            {
                itemMapping = value;
            }
        }

        [XmlIgnoreAttribute()]
        public string CompanyFileName
        {
            get { return m_companyFileName; }
            set { m_companyFileName = value; }
        }

        #endregion

        #region Private Methods

        private bool AddeBay1(string[] stream)
        {
            if (stream.Length > 7)
            {
                if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                {
                    #region For Sales Receipt 
                    m_salesReceiptAdd.SalesReceiptLineAdd.Clear();
                    if (stream[2].Trim() != string.Empty)
                    {
                        CustomerQuery(stream);//bug

                        m_salesReceiptAdd.CustomerRef.FullName = stream[2].Trim();
                        //m_salesReceiptAdd.Other = stream[2].Trim();
                    }
                    if (stream[3].Trim() != string.Empty)
                    {
                        m_salesReceiptAdd.RefNumber = stream[3].Trim();
                        RefNumber = stream[3].Trim();
                    }
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                    {
                        m_salesReceiptAdd.RefNumber = null;
                        RefNumber = null;
                    }
                    if (stream[4].Trim() != string.Empty)
                    {
                        string txnDt = stream[4].Trim().Replace("T", " ").Replace("Z", " ");
                        m_salesReceiptAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[5].Trim() != string.Empty)
                    {
                        string shipDt = stream[5].Trim().Replace("T", " ").Replace("Z", " ");
                        m_salesReceiptAdd.ShipDate = shipDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(shipDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[6].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Addr1 = stream[6].Trim();
                    if (stream[7].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Note = stream[7].Trim();
                    if (stream[8].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Addr2 = stream[8].Trim();
                    if (stream[9].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Addr3 = stream[9].Trim();
                    if (stream[10].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].City = stream[10].Trim();
                    if (stream[11].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].State = stream[11].Trim();
                    if (stream[12].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].PostalCode = stream[12].Trim();
                    if (stream[13].Trim() != string.Empty)
                        m_salesReceiptAdd.ShipAddress[0].Country = stream[13].Trim();

                    if (stream[21].Trim() != string.Empty)
                    {
                        this.AmountAdjusted = stream[21].ToString();
                    }
                    //if (this.ItemMapping == "Shipping")
                    //{
                    if (stream[15].Trim() != string.Empty)
                    {

                    }
                    if (stream[18].Trim() != string.Empty)
                    {
                        serviceCost = stream[18].ToString();
                    }
                    //}
                    //else
                    //if (this.ItemMapping == "Additional Shipping")
                    //{

                    //if (stream[16].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[20].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[20].ToString();
                    //}
                    //}
                    //else
                    //if (this.ItemMapping == "Shipping Insurance")
                    //{
                    //if (stream[17].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[19].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[19].ToString();
                    //}
                    //}
                    #endregion
                }
                else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                {
                    #region For Sales Order

                    m_salesOrderAdd.SalesOrderLineAdd.Clear();

                    if (stream[2].Trim() != string.Empty)
                    {
                        CustomerQuery(stream);//bug

                        m_salesOrderAdd.CustomerRef.FullName = stream[2].Trim();
                        //m_salesReceiptAdd.Other = stream[2].Trim();
                    }
                    if (stream[3].Trim() != string.Empty)
                    {
                        m_salesOrderAdd.RefNumber = stream[3].Trim();
                        RefNumber = stream[3].Trim();
                    }
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                    {
                        m_salesOrderAdd.RefNumber = null;
                        RefNumber = null;
                    }
                    if (stream[4].Trim() != string.Empty)
                    {
                        string txnDt = stream[4].Trim().Replace("T", " ").Replace("Z", " ");
                        m_salesOrderAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[5].Trim() != string.Empty)
                    {
                        string shipDt = stream[5].Trim().Replace("T", " ").Replace("Z", " ");
                        m_salesOrderAdd.ShipDate = shipDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(shipDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[6].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Addr1 = stream[6].Trim();
                    if (stream[7].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Note = stream[7].Trim();
                    if (stream[8].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Addr2 = stream[8].Trim();
                    if (stream[9].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Addr3 = stream[9].Trim();
                    if (stream[10].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].City = stream[10].Trim();
                    if (stream[11].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].State = stream[11].Trim();
                    if (stream[12].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].PostalCode = stream[12].Trim();
                    if (stream[13].Trim() != string.Empty)
                        m_salesOrderAdd.ShipAddress[0].Country = stream[13].Trim();

                    if (stream[21].Trim() != string.Empty)
                    {
                        this.AmountAdjusted = stream[21].ToString();
                    }
                    //if (this.ItemMapping == "Shipping")
                    //{
                    if (stream[15].Trim() != string.Empty)
                    {

                    }
                    if (stream[18].Trim() != string.Empty)
                    {
                        serviceCost = stream[18].ToString();
                    }
                    //}
                    //else
                    //if (this.ItemMapping == "Additional Shipping")
                    //{

                    //if (stream[16].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[20].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[20].ToString();
                    //}
                    //}
                    //else
                    //if (this.ItemMapping == "Shipping Insurance")
                    //{
                    //if (stream[17].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[19].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[19].ToString();
                    //}
                    //}
                    
                    #endregion
                }
                else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                {
                    #region For Invoice 

                    
                    m_invoiceAdd.InvoiceLineAdd.Clear();

                    if (stream[2].Trim() != string.Empty)
                    {
                        CustomerQuery(stream);//bug

                        m_invoiceAdd.CustomerRef.FullName = stream[2].Trim();
                        //m_salesReceiptAdd.Other = stream[2].Trim();
                    }
                    if (stream[3].Trim() != string.Empty)
                    {
                        m_invoiceAdd.RefNumber = stream[3].Trim();
                        RefNumber = stream[3].Trim();
                    }
                    if (CommonUtilities.GetInstance().IsAutoNumbering == true)
                    {
                        m_invoiceAdd.RefNumber = null;
                        RefNumber = null;
                    }
                    if (stream[4].Trim() != string.Empty)
                    {
                        string txnDt = stream[4].Trim().Replace("T", " ").Replace("Z", " ");
                        m_invoiceAdd.TxnDate = txnDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(txnDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[5].Trim() != string.Empty)
                    {
                        string shipDt = stream[5].Trim().Replace("T", " ").Replace("Z", " ");
                        m_invoiceAdd.ShipDate = shipDt == string.Empty ? DateTime.Now.ToString("yyyy-MM-dd") : Convert.ToDateTime(shipDt).ToString("yyyy-MM-dd");
                    }
                    if (stream[6].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Addr1 = stream[6].Trim();
                    if (stream[7].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Note = stream[7].Trim();
                    if (stream[8].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Addr2 = stream[8].Trim();
                    if (stream[9].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Addr3 = stream[9].Trim();
                    if (stream[10].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].City = stream[10].Trim();
                    if (stream[11].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].State = stream[11].Trim();
                    if (stream[12].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].PostalCode = stream[12].Trim();
                    if (stream[13].Trim() != string.Empty)
                        m_invoiceAdd.ShipAddress[0].Country = stream[13].Trim();

                    if (stream[21].Trim() != string.Empty)
                    {
                        this.AmountAdjusted = stream[21].ToString();
                    }
                    //if (this.ItemMapping == "Shipping")
                    //{
                    if (stream[15].Trim() != string.Empty)
                    {

                    }
                    if (stream[18].Trim() != string.Empty)
                    {
                        serviceCost = stream[18].ToString();
                    }
                    //}
                    //else
                    //if (this.ItemMapping == "Additional Shipping")
                    //{

                    //if (stream[16].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[20].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[20].ToString();
                    //}
                    //}
                    //else
                    //if (this.ItemMapping == "Shipping Insurance")
                    //{
                    //if (stream[17].Trim() != string.Empty)
                    //{

                    //}
                    //if (stream[19].Trim() != string.Empty)
                    //{
                    //    serviceCost = stream[19].ToString();
                    //}
                    //}
                    

                    #endregion
                }
                return true;

            }
            else
            {
                throw new Exception("Attachment is not in correct format.");
            }

        }

        private void CreateItem(string[] stream)
        {
            try
            {
                DefaultAccountSettings defaultSettings = new DefaultAccountSettings();
                defaultSettings = defaultSettings.GetDefaultAccountSettings();
                //Code to check whether Item Name conatins ":"
                string ItemName = stream[3].Trim();
                string[] arr = new string[15];
                if (ItemName.Contains(":"))
                {
                    arr = ItemName.Split(':');
                }
                else
                {
                    arr[0] = stream[3].Trim();
                }

                #region Set Item Query

                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] != null && arr[i] != string.Empty)
                    {
                        #region Passing Items Query
                        XmlDocument pxmldoc = new XmlDocument();
                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                        pxmldoc.AppendChild(qbXML);
                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                        qbXML.AppendChild(qbXMLMsgsRq);
                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                        ItemQueryRq.SetAttribute("requestID", "1");

                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                        FullName.InnerText = arr[i];
                        ItemQueryRq.AppendChild(FullName);

                        string pinput = pxmldoc.OuterXml;

                        string resp = string.Empty;
                        try
                        {
                            try
                            {
                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                            }
                            catch
                            { }
                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {

                            if (resp != string.Empty)
                            {
                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                outputXMLDoc.LoadXml(resp);
                                string statusSeverity = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                {
                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                }
                                outputXMLDoc.RemoveAll();
                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                {
                                    //statusMessage += "\n ";
                                    //statusMessage += oNode.Attributes["statusMessage"].Value.ToString();

                                    if (defaultSettings.Type == "NonInventoryPart")
                                    {
                                        #region Item NonInventory Add Query

                                        XmlDocument ItemNonInvendoc = new XmlDocument();
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                        XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                        ItemNonInvendoc.AppendChild(qbXMLINI);
                                        XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                        qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                        qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                        ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                        ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                        XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                        ININame.InnerText = arr[i];
                                        //ININame.InnerText = dr["ItemFullName"].ToString();
                                        ItemNonInventoryAdd.AppendChild(ININame);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }

                                        //Adding Tax Code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }

                                        XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;
                                        //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                        //Adding Desc and Rate


                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISDesc = ItemNonInvendoc.CreateElement("Desc");
                                            ISDesc.InnerText = stream[4].Trim().ToString();
                                            INISalesAndPurchase.AppendChild(ISDesc);
                                            IsPresent = true;
                                        }

                                        if (stream[6].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemNonInvendoc.CreateElement("Price");
                                            ISCost.InnerText = stream[6].Trim();
                                            INISalesAndPurchase.AppendChild(ISCost);
                                            IsPresent = true;
                                        }

                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                            INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                            //INIFullName.InnerText = "Sales";
                                            INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                        }

                                        string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                        //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                        string respItemNonInvendoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strtest2 = respItemNonInvendoc;

                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "Service")
                                    {
                                        #region Item Service Add Query

                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                        ItemServiceAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                        ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                        XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                        ItemServiceAdd.AppendChild(NameIS);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                ItemServiceAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }
                                        //Adding Tax code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                            }
                                        }


                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;
                                        //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                        //Adding Desc and Rate
                                        //Solution for BUG 631 nad 632
                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                            ISDesc.InnerText = stream[4].Trim();
                                            ISSalesAndPurchase.AppendChild(ISDesc);
                                            IsPresent = true;
                                        }

                                        if (stream[6].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemServiceAdddoc.CreateElement("Price");
                                            ISCost.InnerText = stream[6].Trim();
                                            ISSalesAndPurchase.AppendChild(ISCost);
                                            IsPresent = true;
                                        }

                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                            ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                            //Adding IncomeAccount FullName.
                                            XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                            ISIncomeAccountRef.AppendChild(ISFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                        }

                                        string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                        string respItemServiceAddinputdoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strTest3 = respItemServiceAddinputdoc;
                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "InventoryPart")
                                    {
                                        #region Inventory Add Query
                                        XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                        ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                        ItemInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                        XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                        ItemInventoryAdd.AppendChild(NameIS);

                                        //Solution for BUG 633
                                        if (i > 0 && i <= 2)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                //Adding Parent
                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                ItemInventoryAdd.AppendChild(INIParent);

                                                if (i == 2)
                                                {
                                                    XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                                else if (i == 1)
                                                {
                                                    XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    INIChildFullName.InnerText = arr[i - 1];
                                                    INIParent.AppendChild(INIChildFullName);
                                                }

                                            }
                                        }
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                //Adding Tax code Element.
                                                XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }

                                        //Adding Desc and Rate
                                        //Solution for BUG 631 nad 632
                                        if (stream[4].Trim() != string.Empty)
                                        {
                                            XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                            ISDesc.InnerText = stream[4].Trim();
                                            ItemInventoryAdd.AppendChild(ISDesc);
                                        }
                                        if (stream[6].Trim() != string.Empty)
                                        {
                                            XmlElement ISCost = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                            ISCost.InnerText = stream[6].Trim();
                                            ItemInventoryAdd.AppendChild(ISCost);
                                        }


                                        //Adding IncomeAccountRef
                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                            ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                        }

                                        //Adding COGSAccountRef
                                        if (defaultSettings.COGSAccount != string.Empty)
                                        {
                                            XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                            ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                            XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                            INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                        }

                                        //Adding AssetAccountRef
                                        if (defaultSettings.AssetAccount != string.Empty)
                                        {
                                            XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                            ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                            XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                            INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                        }

                                        string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                        string respItemInventoryAddinputdoc = string.Empty;
                                        try
                                        {
                                            try
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            }
                                            catch
                                            { }
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        string strTest4 = respItemInventoryAddinputdoc;
                                        #endregion
                                    }
                                }
                            }

                        }

                        #endregion
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        private bool AddeBay2(string[] stream)
        {
            DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();

            #region Checking and setting TaxCode

            string TaxRateValue = string.Empty;
            string ItemSaleTaxFullName = string.Empty;

            if (defaultSettings.GrossToNet == "1")
            {

                if (defaultSettings.TaxCode != string.Empty)
                {
                    ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, defaultSettings.TaxCode);

                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                }

            }
            #endregion


            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                #region For Sales Receipt

                //string ItemSaleTaxFullName = string.Empty;
                //DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();
                FinancialEntities.SalesReceiptLineAdd salesReceiptLineAdd = new SalesReceiptLineAdd();
                if (stream[1].Trim() != string.Empty)
                    salesReceiptLineAdd.Other1 = stream[2].Trim();
                if (stream[3].Trim() != string.Empty)
                {
                    if (stream[3].Trim().Length > 31)
                    {
                        stream[3] = stream[3].Trim().Remove(31, (stream[3].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    salesReceiptLineAdd.ItemRef.FullName = stream[3].Trim();

                    //if (defaultSettings.GrossToNet == "1")
                    //{
                    //    string TaxCodeRefValue = GetSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, stream[3].Trim());
                    //    if (TaxCodeRefValue.Trim() != string.Empty)
                    //    {
                    //        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, TaxCodeRefValue.Trim());
                    //        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                    //    }
                    //}
                }
                if (stream[4].Trim() != string.Empty)
                    salesReceiptLineAdd.Desc = stream[4].Trim();
                if (stream[5].Trim() != string.Empty)
                    salesReceiptLineAdd.Quantity = stream[5].Trim();
                if (stream[6].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                        {
                            decimal Rate = Convert.ToDecimal(stream[6].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));
                            //netRate = string.Format("{0:0.00}", rate);
                            salesReceiptLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            salesReceiptLineAdd.Rate = stream[6].Trim();
                        }
                    }
                    else
                    {
                        salesReceiptLineAdd.Rate = stream[6].Trim();
                    }
                }
                m_salesReceiptAdd.SalesReceiptLineAdd.Add(salesReceiptLineAdd);
                

                #endregion
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                #region For Sales Order

                
                //string ItemSaleTaxFullName = string.Empty;
                //DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();
                FinancialEntities.SalesOrderLineAdd salesOrderLineAdd = new FinancialEntities.SalesOrderLineAdd();
                if (stream[1].Trim() != string.Empty)
                    salesOrderLineAdd.Other1 = stream[2].Trim();
                if (stream[3].Trim() != string.Empty)
                {
                    if (stream[3].Trim().Length > 31)
                    {
                        stream[3] = stream[3].Trim().Remove(31, (stream[3].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    salesOrderLineAdd.ItemRef.FullName = stream[3].Trim();

                    //if (defaultSettings.GrossToNet == "1")
                    //{
                    //    string TaxCodeRefValue = GetSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, stream[3].Trim());
                    //    if (TaxCodeRefValue.Trim() != string.Empty)
                    //    {
                    //        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, TaxCodeRefValue.Trim());
                    //        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                    //    }
                    //}
                }
                if (stream[4].Trim() != string.Empty)
                    salesOrderLineAdd.Desc = stream[4].Trim();
                if (stream[5].Trim() != string.Empty)
                    salesOrderLineAdd.Quantity = stream[5].Trim();
                if (stream[6].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                        {
                            decimal Rate = Convert.ToDecimal(stream[6].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));
                            //netRate = string.Format("{0:0.00}", rate);
                            salesOrderLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            salesOrderLineAdd.Rate = stream[6].Trim();
                        }
                    }
                    else
                    {
                        salesOrderLineAdd.Rate = stream[6].Trim();
                    }
                }
                m_salesOrderAdd.SalesOrderLineAdd.Add(salesOrderLineAdd);


                

                #endregion
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                #region For Invoices

                

                //string ItemSaleTaxFullName = string.Empty;
                //DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();
                FinancialEntities.InvoiceLineAdd invoiceLineAdd = new FinancialEntities.InvoiceLineAdd();

                if (stream[1].Trim() != string.Empty)
                    invoiceLineAdd.Other1 = stream[2].Trim();
                if (stream[3].Trim() != string.Empty)
                {
                    if (stream[3].Trim().Length > 31)
                    {
                        stream[3] = stream[3].Trim().Remove(31, (stream[3].Trim().Length - 31));
                    }
                    CreateItem(stream);
                    invoiceLineAdd.ItemRef.FullName = stream[3].Trim();

                    //if (defaultSettings.GrossToNet == "1")
                    //{
                    //    string TaxCodeRefValue = GetSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, stream[3].Trim());
                    //    if (TaxCodeRefValue.Trim() != string.Empty)
                    //    {
                    //        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, TaxCodeRefValue.Trim());
                    //        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                    //    }
                    //}
                }
                if (stream[4].Trim() != string.Empty)
                    invoiceLineAdd.Desc = stream[4].Trim();
                if (stream[5].Trim() != string.Empty)
                    invoiceLineAdd.Quantity = stream[5].Trim();
                if (stream[6].Trim() != string.Empty)
                {
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                        {
                            decimal Rate = Convert.ToDecimal(stream[6].Trim());
                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                            rate = Rate / (1 + (TaxRate / 100));
                            //netRate = string.Format("{0:0.00}", rate);
                            invoiceLineAdd.Rate = Math.Round(rate, 5).ToString();
                        }
                        else
                        {
                            invoiceLineAdd.Rate = stream[6].Trim();
                        }
                    }
                    else
                    {
                        invoiceLineAdd.Rate = stream[6].Trim();
                    }
                }
                m_invoiceAdd.InvoiceLineAdd.Add(invoiceLineAdd);


                

                #endregion
            }
            return true;
        }

        //For Shipping details.
        private bool AddeBay2Shipping()
        {
            string TaxRateValue = string.Empty;

            string ItemSaleTaxFullName = string.Empty;
            DefaultAccountSettings defaultSettings = new DefaultAccountSettings().GetDefaultAccountSettings();
            if (defaultSettings.GrossToNet == "1")
            {
                string TaxCodeRefValue = GetSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, this.ShippingItem.Trim());
                if (TaxCodeRefValue.Trim() != string.Empty)
                {
                    ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, TaxCodeRefValue.Trim());
                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                }
            }
            if (this.serviceCost != null && this.serviceCost.Trim() != string.Empty && Convert.ToDecimal(this.serviceCost) != 0)
            {
                if (this.ShippingItem != null && this.ShippingItem.Trim() != string.Empty)
                {
                    if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                    {
                        #region For Sales Receipt
                    FinancialEntities.SalesReceiptLineAdd salesReceiptLineAdd = new SalesReceiptLineAdd();
                    

                    salesReceiptLineAdd.ItemRef.FullName = this.ShippingItem;

                    
                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                            CommonUtilities.GetInstance().CountryVersion == "CA")
                        {
                        decimal Rate = Convert.ToDecimal(this.serviceCost.Trim());
                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                        rate = Rate / (1 + (TaxRate / 100));
                            salesReceiptLineAdd.Amount = string.Format("{0:0.00}", rate);
                        }
                        else
                        {
                            decimal cost = Convert.ToDecimal(this.serviceCost);
                            salesReceiptLineAdd.Amount = string.Format("{0:0.00}", cost);
                        }
                    }
                    else
                    {
                        decimal cost = Convert.ToDecimal(this.serviceCost);
                        salesReceiptLineAdd.Amount = string.Format("{0:0.00}", cost);
                    }

                    m_salesReceiptAdd.SalesReceiptLineAdd.Add(salesReceiptLineAdd);
                        #endregion
                    }
                    else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
                    {
                        #region For Sales Order
                        FinancialEntities.SalesOrderLineAdd salesOrderLineAdd = new SalesOrderLineAdd();
                        salesOrderLineAdd.ItemRef.FullName = this.ShippingItem;
                        if (TaxRateValue != string.Empty)
                        {
                            decimal rate = 0;
                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                            {
                                decimal Rate = Convert.ToDecimal(this.serviceCost.Trim());
                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                rate = Rate / (1 + (TaxRate / 100));
                                salesOrderLineAdd.Amount = string.Format("{0:0.00}", rate);
                            }
                            else
                            {
                                decimal cost = Convert.ToDecimal(this.serviceCost);
                                salesOrderLineAdd.Amount = string.Format("{0:0.00}", cost);
                            }
                        }
                        else
                        {
                            decimal cost = Convert.ToDecimal(this.serviceCost);
                            salesOrderLineAdd.Amount = string.Format("{0:0.00}", cost);
                        }
                        m_salesOrderAdd.SalesOrderLineAdd.Add(salesOrderLineAdd);
                        #endregion
                    }
                    else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                    {
                        #region for Invoice
                        FinancialEntities.InvoiceLineAdd invoiceLineAdd = new InvoiceLineAdd();
                        invoiceLineAdd.ItemRef.FullName = this.ShippingItem;
                        if (TaxRateValue != string.Empty)
                        {
                            decimal rate = 0;
                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                            {
                                decimal Rate = Convert.ToDecimal(this.serviceCost.Trim());
                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                rate = Rate / (1 + (TaxRate / 100));
                                invoiceLineAdd.Amount = string.Format("{0:0.00}", rate);
                            }
                            else
                            {
                                decimal cost = Convert.ToDecimal(this.serviceCost);
                                invoiceLineAdd.Amount = string.Format("{0:0.00}", cost);
                            }
                        }
                        else
                        {
                            decimal cost = Convert.ToDecimal(this.serviceCost);
                            invoiceLineAdd.Amount = string.Format("{0:0.00}", cost);
                        }
                        m_invoiceAdd.InvoiceLineAdd.Add(invoiceLineAdd);
                        #endregion
                    }
                }

            }

            if (defaultSettings.GrossToNet == "1")
            {
                string TaxCodeRefValue = GetSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, this.DiscountItem.Trim());
                if (TaxCodeRefValue.Trim() != string.Empty)
                {
                    ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(CommonUtilities.GetInstance().CompanyFile, TaxCodeRefValue.Trim());
                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(CommonUtilities.GetInstance().CompanyFile, ItemSaleTaxFullName);
                }
            }
            if (this.AmountAdjusted != null && this.AmountAdjusted.Trim() != string.Empty && Convert.ToDecimal(this.AmountAdjusted) != 0)
            {
                if (this.DiscountItem != null && this.DiscountItem.Trim() != string.Empty)
                {
                    if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                    {
                        #region For Sales Receipt
                    FinancialEntities.SalesReceiptLineAdd salesReceiptLineAdd = new SalesReceiptLineAdd();
                                        

                    salesReceiptLineAdd.ItemRef.FullName = this.DiscountItem;


                    if (TaxRateValue != string.Empty)
                    {
                        decimal rate = 0;

                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                            CommonUtilities.GetInstance().CountryVersion == "CA")
                        {
                        decimal Rate = Convert.ToDecimal(this.AmountAdjusted.Trim());
                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                        rate = Rate / (1 + (TaxRate / 100));
                            salesReceiptLineAdd.Amount = string.Format("{0:0.00}", rate);
                        }
                        else
                        {
                            decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                            salesReceiptLineAdd.Amount = string.Format("{0:0.00}", Amount);
                        }
                    }
                    else
                    {
                        decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                        salesReceiptLineAdd.Amount = string.Format("{0:0.00}", Amount);
                    }

                    m_salesReceiptAdd.SalesReceiptLineAdd.Add(salesReceiptLineAdd);
                        #endregion
                    }
                    else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
                    {
                        #region For Sales Order
                        FinancialEntities.SalesOrderLineAdd salesOrderLineAdd = new SalesOrderLineAdd();
                        salesOrderLineAdd.ItemRef.FullName = this.DiscountItem;
                        if (TaxRateValue != string.Empty)
                        {
                            decimal rate = 0;
                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                            {
                                decimal Rate = Convert.ToDecimal(this.AmountAdjusted.Trim());
                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                rate = Rate / (1 + (TaxRate / 100));
                                salesOrderLineAdd.Amount = string.Format("{0:0.00}", rate);
                            }
                            else
                            {
                                decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                                salesOrderLineAdd.Amount = string.Format("{0:0.00}", Amount);
                            }
                        }
                        else
                        {
                            decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                            salesOrderLineAdd.Amount = string.Format("{0:0.00}", Amount);
                        }
                        m_salesOrderAdd.SalesOrderLineAdd.Add(salesOrderLineAdd);
                        #endregion
                    }
                    else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
                    {
                        #region For Invoice
                        FinancialEntities.InvoiceLineAdd invoiceLineAdd = new InvoiceLineAdd();
                        invoiceLineAdd.ItemRef.FullName = this.DiscountItem;
                        if (TaxRateValue != string.Empty)
                        {
                            decimal rate = 0;
                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                CommonUtilities.GetInstance().CountryVersion == "CA")
                            {
                                decimal Rate = Convert.ToDecimal(this.AmountAdjusted.Trim());
                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                rate = Rate / (1 + (TaxRate / 100));
                                invoiceLineAdd.Amount = string.Format("{0:0.00}", rate);
                            }
                            else
                            {
                                decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                                invoiceLineAdd.Amount = string.Format("{0:0.00}", Amount);
                            }
                        }
                        else
                        {
                            decimal Amount = Convert.ToDecimal(this.AmountAdjusted);
                            invoiceLineAdd.Amount = string.Format("{0:0.00}", Amount);
                        }
                        m_invoiceAdd.InvoiceLineAdd.Add(invoiceLineAdd);
                        #endregion
                    }
                }
            }

            return true;
        }

        #endregion

        #region Public Methods

        
        /// <summary>
        /// This method is used for parse the ebay or OSCommerce order 
        /// for Sales Receipt Add to QuickBooks.
        /// <param name="attachmentContent"></param>
        /// <param name="appName"></param>
        public void Parse(string attachmentContent, string appName)
        {
            QuickBooks qb = new QuickBooks(this.m_messageId);
            this.CompanyFileName = appName;
            ShipAddress shipAddress = new ShipAddress();

            #region For particular transaction

            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                m_salesReceiptAdd.ShipAddress = new Collection<ShipAddress>();
                m_salesReceiptAdd.ShipAddress.Add(shipAddress);
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                m_salesOrderAdd.ShipAddress = new Collection<ShipAddress>();
                m_salesOrderAdd.ShipAddress.Add(shipAddress);
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                m_invoiceAdd.ShipAddress = new Collection<ShipAddress>();
                m_invoiceAdd.ShipAddress.Add(shipAddress);
            }

            #endregion


            //m_salesReceiptAdd.SalesReceiptLineAdd.Clear();
            int startIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
            string messageResp = string.Empty;
            int m_failureCount = 0;
            int m_successCount = 0;
            DataTable tempdt = new DataTable();
            while (endIndex <= (attachmentContent.Length))
            {
                string[] stream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                if (stream[0].Trim().Equals("eBay1"))
                {
                    if (!AddeBay1(stream))
                    {
                        throw new Exception("Error in parsing eBay stream in the attachment.");
                    }
                }
                else if (stream[0].Trim().Equals("eBay2"))
                {
                    if (!AddeBay2(stream))
                    {
                        throw new Exception("Error in parsing the eBay Item stream in the attachment.");
                    }
                }

                startIndex = endIndex + Constants.NEWLINE.Length;
                if (startIndex < attachmentContent.Length)
                {
                    endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                }
                else
                {
                    break;
                }
            }
            //Add the Shipping details.
            AddeBay2Shipping();

            if (this.RefNumber == null)
            {
                if (!ExportToQuickBooks(ref messageResp, appName))
                    m_failureCount++;
                else
                    m_successCount++;

            }
            else if (this.RefNumber != string.Empty)
            {
                Hashtable responseTable = CheckAndGetIDExistsInQuickBooks(this.RefNumber, CommonUtilities.GetInstance().CompanyFile);

                if (responseTable.Count == 0)
                {
                    if (!ExportToQuickBooks(ref messageResp, appName))
                        m_failureCount++;
                    else
                        m_successCount++;
                }
                else if (responseTable.Count > 0)
                {
                    string TxnID = string.Empty;
                    string editSq = string.Empty;
                    string message = string.Empty;
                    string request = string.Empty;

                    //Getting listID and Edit Sequence number for modifing price level data.
                    foreach (DictionaryEntry de in responseTable)
                    {
                        TxnID = de.Key.ToString();
                        editSq = de.Value.ToString();
                    }

                    //This method is used to update price level in QUickbooks.
                    if (!UpdateSalesRecieptQuickBooks(ref messageResp, ref request, 1, CommonUtilities.GetInstance().CompanyFile, TxnID, editSq))
                    {
                        m_failureCount++;
                    }
                    else
                    { m_successCount++; }
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(messageResp);

            
            

            //check whether its for Automatic Inbound setting or normal parsing.
            if (CommonUtilities.GetInstance().IsInboundFlag == true)
            {
                CommonUtilities.WriteErrorLog(sb.ToString());
                if (m_failureCount > 0)
                    CommonUtilities.GetInstance().FailureCount = CommonUtilities.GetInstance().FailureCount + 1;
                else if (m_successCount > 0)
                    CommonUtilities.GetInstance().ProcessedCount = CommonUtilities.GetInstance().ProcessedCount + 1;

            }
            else
                CommonUtilities.GetInstance().DisplaySummary(m_failureCount, m_successCount, sb, tempdt, 0);

            if (m_failureCount > 0)
            {
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Failed);
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Failed);
                CommonUtilities.WriteErrorLog(messageResp);
            }
            else
            {
                new MessageController().ChangeMessageStatus(this.m_messageId, (int)MessageStatus.Processed);
                new MessageController().ChangeAttachmentStatus(this.m_messageId, (int)MessageStatus.Processed);
            }



        }


        /// <summary>
        /// This method is used to Import Sales Receipt Data into Quickbooks
        /// Company File.
        /// </summary>
        /// <param name="statusMessage">Status message with errors or Success message.</param>
        /// <param name="requestText">Error or Success Mesage text</param>
        /// <param name="rowcount">Number of Row Count.</param>
        /// <param name="AppName">Application Name (l.e Axis 5.0)</param>
        /// <returns>return true means data imported successfully otherwise false.</returns>
        public bool ExportToQuickBooks(ref string statusMessage, string AppName)
        {

            string typeTran = string.Empty;

            #region Check whether which transaction
            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                typeTran = "SalesReceipt";
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                typeTran = "SalesOrder";
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                typeTran = "Invoice";
            }
            #endregion

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<FinancialEntities.QuickBooks>.Save(this, fileName);
            }
            catch
            {
                //statusMessage += "\n ";
                //statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesReceiptAddRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptAddRq = requestXmlDoc.CreateElement(typeTran+"AddRq");
            inner.AppendChild(SalesReceiptAddRq);

            //Create SalesReceiptAdd aggregate and fill in field values for it
            //System.Xml.XmlElement SalesReceiptAdd = requestXmlDoc.CreateElement("SalesReceiptAdd");
            //SalesReceiptAddRq.AppendChild(SalesReceiptAdd);

            requestXML = requestXML.Replace("<" + typeTran + "LineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<" + typeTran + "LineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</" + typeTran + "LineAddREM>", string.Empty);


            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            SalesReceiptAddRq.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/" + typeTran + "AddRq/" + typeTran + "Add"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }

            string requestText = requestXmlDoc.OuterXml;

            string resp = string.Empty;
            try
            {
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch
                { }
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {

                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/"+typeTran+"AddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";

                if (typeTran.Equals("SalesReceipt"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesReceipt(this);
                else if (typeTran.Equals("SalesOrder"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesOrder(this);
                else if (typeTran.Equals("Invoice"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);

                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion

        #region Customer Query

        public void CustomerQuery(string[] stream)
        {
            #region Set Customer Query

            if (stream[2].Trim() != string.Empty)
            {
                XmlDocument pxmldoc = new XmlDocument();
                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                pxmldoc.AppendChild(qbXML);
                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                qbXML.AppendChild(qbXMLMsgsRq);
                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                CustomerQueryRq.SetAttribute("requestID", "1");
                XmlElement FullName = pxmldoc.CreateElement("FullName");
                FullName.InnerText = stream[2].Trim();
                CustomerQueryRq.AppendChild(FullName);

                string pinput = pxmldoc.OuterXml;

                string resp = string.Empty;
                try
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                }
                catch
                { }
                try
                {
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                }
                finally
                {
                    if (resp != string.Empty)
                    {

                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        string statusSeverity = string.Empty;
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                        }
                        outputXMLDoc.RemoveAll();
                        if (statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            #region Customer Add Query

                            XmlDocument xmldocadd = new XmlDocument();
                            xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                            xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                            XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                            xmldocadd.AppendChild(qbXMLcust);
                            XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                            qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                            qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                            XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                            qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                            CustomerAddRq.SetAttribute("requestID", "1");
                            XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                            CustomerAddRq.AppendChild(CustomerAdd);

                            XmlElement Name = xmldocadd.CreateElement("Name");
                            Name.InnerText = stream[2].ToString();
                            CustomerAdd.AppendChild(Name);

                            #region Adding Ship Address of Customer.

                            XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                            CustomerAdd.AppendChild(ShipAddress);

                            if (stream[6].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                ShipAdd1.InnerText = stream[6].ToString();
                                ShipAddress.AppendChild(ShipAdd1);
                            }

                            if (stream[8].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                ShipAdd2.InnerText = stream[8].ToString();
                                ShipAddress.AppendChild(ShipAdd2);
                            }

                            if (stream[9].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                ShipAdd3.InnerText = stream[9].ToString();
                                ShipAddress.AppendChild(ShipAdd3);
                            }

                            if (stream[10].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipCity = xmldocadd.CreateElement("City");
                                ShipCity.InnerText = stream[10].ToString();
                                ShipAddress.AppendChild(ShipCity);
                            }

                            if (stream[11].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipState = xmldocadd.CreateElement("State");
                                ShipState.InnerText = stream[11].ToString();
                                ShipAddress.AppendChild(ShipState);
                            }

                            if (stream[12].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                ShipPostalCode.InnerText = stream[12].ToString();
                                ShipAddress.AppendChild(ShipPostalCode);
                            }

                            if (stream[13].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                ShipCountry.InnerText = stream[13].ToString();
                                ShipAddress.AppendChild(ShipCountry);
                            }

                            if (stream[7].ToString().Trim() != string.Empty)
                            {
                                XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                ShipNote.InnerText = stream[7].ToString();
                                ShipAddress.AppendChild(ShipNote);
                            }

                            #endregion

                            string custinput = xmldocadd.OuterXml;
                            string respcust = string.Empty;
                            try
                            {
                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                            }
                            catch
                            { }
                            try
                            {
                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(this.CompanyFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                            }
                            catch { }
                            finally
                            {
                                if (respcust != string.Empty)
                                {
                                    System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                    outputcustXMLDoc.LoadXml(respcust);
                                    foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                    {
                                        string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                        if (statusSeveritycust == "Error")
                                        {
                                            string msg = "New Customer could not be created into QuickBooks \n ";
                                            msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                            CommonUtilities.WriteErrorLog(msg);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// This method is used to get "TaxRate" from "SalesTaxCode".
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <param name="fullname"></param>
        /// <returns></returns>
        public static string GetSalesTaxCode(string QBFileName, string name)
        {
            string TaxCodeRef = string.Empty;

            //Executing SalesTaxCodeQuery
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
            xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmldoc.CreateElement("QBXML");
            xmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemSalesTaxQueryRq = xmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemSalesTaxQueryRq);
            ItemSalesTaxQueryRq.SetAttribute("requestID", "1");
            XmlElement fullName = xmldoc.CreateElement("FullName");
            fullName.InnerText = name;
            ItemSalesTaxQueryRq.AppendChild(fullName);

            string input = xmldoc.OuterXml;
            string ticket = string.Empty;
            string response = string.Empty;

            try
            {
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch
            { }

            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

            try
            {
                response = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, input);
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception(ex.Message);
            }

            XmlDocument outputXMLDoc = new XmlDocument();

            if (response != string.Empty)
            {
                //Loading ItemSalesTaxCode List into XML.
                outputXMLDoc.LoadXml(response);

                #region Getting Values from XML

                //Getting values from QuickBooks response.
                XmlNodeList nodeList = outputXMLDoc.GetElementsByTagName("SalesTaxCodeRef");
                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode nodes in node.ChildNodes)
                    {
                        if (nodes.Name.Equals("FullName"))
                        {
                            TaxCodeRef = nodes.InnerText;
                        }
                    }
                }
                #endregion
            }

            return TaxCodeRef;
        }

        #endregion

        /// <summary>
        /// This method is used for getting existing  Txnid and editsequence.
        /// If not exists then it return null.
        /// </summary>
        /// <param name=""></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public Hashtable CheckAndGetIDExistsInQuickBooks(string RefNumber, string AppName)
        {
            string typeTran = string.Empty;

            #region Check whether which transaction
            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                typeTran="SalesReceipt";
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                typeTran="SalesOrder";
            }
            else if(CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                typeTran = "Invoice";
            }
            #endregion


            Hashtable SalesReciptTable = new Hashtable();

            XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PriceLevelQueryRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptQueryRq = requestXmlDoc.CreateElement(typeTran+"QueryRq");
            inner.AppendChild(SalesReceiptQueryRq);

            //Create  aggregate and fill in field values for it
            System.Xml.XmlElement RefNo = requestXmlDoc.CreateElement("RefNumber");
            RefNo.InnerText = RefNumber;
            SalesReceiptQueryRq.AppendChild(RefNo);


            string resp = string.Empty;
            try
            {
                //Sending request for checking price level in QuickBooks.
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                //Getting response of Price Level.
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return SalesReciptTable;
            }

            if (resp == string.Empty)
            {
                return SalesReciptTable;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no  exists.
                    return SalesReciptTable;

                }
                else
                    if (resp.Contains("statusSeverity=\"Warn\""))
                    {
                        //Returning means there is no exists.
                        return SalesReciptTable;
                    }
                    else
                    {
                        //Getting listid and Edit Sequence details of Price Level.
                        string TxnID = string.Empty;
                        string editSequence = string.Empty;
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/"+typeTran+"QueryRs/"+typeTran+"Ret"))
                        {
                            //Get ListID of price Level.
                            if (oNode.SelectSingleNode("TxnID") != null)
                            {
                                TxnID = oNode.SelectSingleNode("TxnID").InnerText.ToString();
                            }
                            //Get Edit Sequence of Price Level.
                            if (oNode.SelectSingleNode("EditSequence") != null)
                            {
                                editSequence = oNode.SelectSingleNode("EditSequence").InnerText.ToString();
                            }
                        }
                        if (TxnID != string.Empty && editSequence != string.Empty)
                        {
                            SalesReciptTable.Add(TxnID, editSequence);
                        }
                        return SalesReciptTable;
                    }
            }
        }


        /// <summary>
        /// This method is used for updating price level information
        /// of existing with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateSalesRecieptQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string txnID, string editSequence)
        {

            string typeTran = string.Empty;

            #region Check whether which transaction
            if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Receipt"))
            {
                typeTran = "SalesReceipt";
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Sales Order"))
            {
                typeTran = "SalesOrder";
            }
            else if (CommonUtilities.GetInstance().EDIClickedItem.Equals("Invoice"))
            {
                typeTran = "Invoice";
            }
            #endregion


            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<FinancialEntities.QuickBooks>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesReceiptModRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptModRq = requestXmlDoc.CreateElement(typeTran+"ModRq");
            inner.AppendChild(SalesReceiptModRq);

            //Create SalesReceiptMod aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptMod = requestXmlDoc.CreateElement(typeTran+"Mod");
            SalesReceiptModRq.AppendChild(SalesReceiptMod);



            requestXML = requestXML.Replace("<SalesReceiptPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptPerItemREM>", string.Empty);
            
            requestXML = requestXML.Replace("<SalesReceiptAdd>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptAdd>", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderAdd>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderAdd>", string.Empty);
            requestXML = requestXML.Replace("<InvoiceAdd>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceAdd>", string.Empty);


            requestXML = requestXML.Replace("<SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesReceiptLineAdd>", "<SalesReceiptLineMod>");
            requestXML = requestXML.Replace("</SalesReceiptLineAdd>", "</SalesReceiptLineMod>");
            requestXML = requestXML.Replace("<SalesOrderLineAdd>", "<SalesOrderLineMod>");
            requestXML = requestXML.Replace("</SalesOrderLineAdd>", "</SalesOrderLineMod>");
            requestXML = requestXML.Replace("<InvoiceLineAdd>", "<InvoiceLineMod>");
            requestXML = requestXML.Replace("</InvoiceLineAdd>", "</InvoiceLineMod>");

            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/" + typeTran + "ModRq/" + typeTran + "Mod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesReceiptModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By RefNumber) : " + rowcount.ToString());
            else
                SalesReceiptModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;
            SalesReceiptMod.InnerXml = requestXML;


            XmlNode nameChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/" + typeTran + "ModRq/" + typeTran + "Mod/CustomerRef");
            //Create TxnID aggregate and fill in field values for it
            System.Xml.XmlElement TxnID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/" + typeTran + "ModRq/" + typeTran + "Mod").InsertBefore(TxnID, nameChild).InnerText = txnID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;

            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");

            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/" + typeTran + "ModRq/" + typeTran + "Mod").InsertAfter(EditSequence, TxnID).InnerText = editSequence;

            //System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
            
            //nameChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod");

            //XmlElement childElement = requestXmlDoc.CreateElement("TxnLineID");
            //childElement.InnerText = "-1";
            //XmlNode parentNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod");

            //foreach (XmlNode nodes in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            //{
            //    foreach (XmlNode childnodes in nodes.ChildNodes)
            //    {
            //        if (childnodes.Name.Equals("SalesReceiptLineMod"))
            //        {
            //            parentNode.InsertBefore(childElement, parentNode.FirstChild);
            //            parentNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod");
            //        }
            //     }
            //}

            //foreach (XmlNode nodes in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            //{
            //    foreach (XmlNode childnodes in nodes.ChildNodes)
            //    {
            //        if(childnodes.Name.Equals("SalesReceiptLineMod"))
            //        {
            //            childnodes.InsertBefore(TxnLineID,nameChild.FirstChild).InnerText = "-1"; 
                      
            //        }
            //    }
            //    //requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod").InsertBefore(TxnLineID, nameChild).InnerText = "-1";
            //}
            //requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod").InsertBefore(TxnLineID, nameChild).InnerText = "-1";

            string resp = string.Empty;
            try
            {
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                ////CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + typeTran + "ModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";

                if (typeTran.Equals("SalesReceipt"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesReceipt(this);
                else if (typeTran.Equals("SalesOrder"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesOrder(this);
                else if (typeTran.Equals("Invoice"))
                    statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);

                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

    }

    /// <summary>
    /// QuickBooks collection class.
    /// </summary>
    public class SalesReceiptAddCollection : Collection<QuickBooks>
    {
        
    }

    /// <summary>
    /// Sales Receipt Line Add class for item array.
    /// </summary>
    [XmlRootAttribute("SalesReceiptLineAdd", Namespace = "", IsNullable = false)]
    public class SalesReceiptLineAdd
    {
        #region Private Member Variables


        private ItemRef m_ItemRef;
       
        private string m_Desc;
        private string m_Quantity;
        private string m_Rate;
        private string m_Amount;
        private string m_Other1;
       
        #endregion

        #region  Constructors

        public SalesReceiptLineAdd()
        {
            m_ItemRef = new ItemRef();
        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }   

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }


        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

 
        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }
   
        #endregion
    }


    /// <summary>
    /// Sales Receipt Line Add class for item array.
    /// </summary>
    [XmlRootAttribute("InvoiceLineAdd", Namespace = "", IsNullable = false)]
    public class InvoiceLineAdd
    {
        #region Private Member Variables


        private ItemRef m_ItemRef;

        private string m_Desc;
        private string m_Quantity;
        private string m_Rate;
        private string m_Amount;
        private string m_Other1;

        #endregion

        #region  Constructors

        public InvoiceLineAdd()
        {
            m_ItemRef = new ItemRef();
        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }


        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }


        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        #endregion
    }


    /// <summary>
    /// Sales Order Line Add class for item array.
    /// </summary>
    [XmlRootAttribute("SalesOrderLineAdd", Namespace = "", IsNullable = false)]
    public class SalesOrderLineAdd
    {
        #region Private Member Variables


        private ItemRef m_ItemRef;

        private string m_Desc;
        private string m_Quantity;
        private string m_Rate;
        private string m_Amount;
        private string m_Other1;

        #endregion

        #region  Constructors

        public SalesOrderLineAdd()
        {
            m_ItemRef = new ItemRef();
        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }


        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }


        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        #endregion
    }
}
