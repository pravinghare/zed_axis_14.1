using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.IO;
using Tamir.SharpSsh.jsch;
using System.Windows.Forms;
using EDI.Constant;
using TradingPartnerDetails;
using System.Net.Sockets;
using DataProcessingBlocks;
using MySql.Data.MySqlClient;

namespace FinancialEntities
{
    public class XCart : General
    {        
        #region Private Members
       
        private string xcartWebStoreUrl;
        private string xcartUserName;
        private string xcartPassword;
        private string databaseName;
        private string xcartPort;
        private static XCart m_XCart;
        private string xcartConnectionString;
        Session session;

        #endregion 

        #region Public Properties
        /// <summary>
        /// Get or Set Web Store Url or Server of X-Cart.
        /// </summary>
        public string WebStoreUrl
        {
            get
            {
                return xcartWebStoreUrl;
            }
            set
            {
                xcartWebStoreUrl = value;
            }
        }

        /// <summary>
        /// Get or Set xcartommerce User Name.
        /// </summary>
        public string UserName
        {
            get
            {
                return xcartUserName;
            }
            set
            {
                xcartUserName = value;
            }
        }

        /// <summary>
        /// Get or Set Password of xcartommerce.
        /// </summary>
        public string Password
        {
            get
            {
                return xcartPassword;
            }
            set
            {
                xcartPassword = value;
            }
        }

        /// <summary>
        /// Get or Set Database Name of xcartommerce.
        /// </summary>
        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
            set
            {
                databaseName = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor code for generate X-Cart Class.
        /// </summary>
        public XCart()
        {
           

        }

        #endregion

        #region Static Methods
        /// <summary>
        /// This method is used for getting Current Instance of X-Cart.
        /// </summary>
        /// <returns></returns>
        public static XCart GetInstance()
        {
            if (m_XCart == null)
                m_XCart = new XCart();
            return m_XCart;
        }

        #endregion

        #region Methods and Events
        

        /// <summary>
        /// This method is used for checking connection of X-Cart.
        /// </summary>
        /// <param name="xcartServer">Server Name of X-Cart.</param>
        /// <param name="userName">UserName of X-Cart.</param>
        /// <param name="password">Password of X-Cart.</param>
        /// <param name="databaseName">Database name of X-Cart.</param>
        /// <returns></returns>
        public bool CheckConnection(string xcartServer, string userName, string password, string databaseName, string port)
        {
            MySqlConnection con = new MySqlConnection();

            try
            {
                if (port == string.Empty || port == "3306")
                {
                    con.ConnectionString = "Server=" + xcartServer + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }
                else
                {
                    con.ConnectionString = "Server=" + xcartServer + ";Port=" + port + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }

            }
            catch (ArgumentException ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                return false;
            }
            try
            {
                //Connect to X-Cart Server.
                con.Open();
            }
            catch (Exception ex)
            {
                //Log the error
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }

            con.Close();
            return true;
        }


        public bool UpdatePendingStatus(string orderID, string tradingPartnerID)
        {
            session = null;
            bool result = false;
            string port = string.Empty;
            DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.TPWithXCart, tradingPartnerID));
            if (dataset != null)
            {
                xcartWebStoreUrl = dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                xcartUserName = dataset.Tables[0].Rows[0]["UserName"].ToString();
                xcartPassword = dataset.Tables[0].Rows[0]["Password"].ToString();

                if (xcartPassword.Contains(";"))
                    xcartPassword = "'" + xcartPassword + "'";
                xcartPort = dataset.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                
                if (dataset.Tables[0].Rows[0][2].ToString() == "5")
                {
                    //int StatusID = GetStatusID(dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), xcartPassword, xcartPort);
                   // result = UpdateXcartommercePendingStatus(orderID, StatusID, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dataset.Tables[0].Rows[0]["Password"].ToString(), xcartPort);
                }
            }
            return result;
        }

        private bool UpdateXcartommercePendingStatus(string orderID, int orderstatusID, string xcartServer, string dbName, string userName, string password, string port)
        {
            try
            {
                MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                try
                {
                    if (password.Contains(";"))
                        password = "'" + password + "'";
                    string constring = string.Empty;
                    if (port == string.Empty || port == "3306")
                        constring = "Server=" + xcartServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                    else
                        constring = "Server=" + xcartServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                    con.ConnectionString = constring;
                    this.xcartConnectionString  = con.ConnectionString;
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    return false;
                }
               // MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
               // cmd.Connection = con;
               //// cmd.CommandText = string.Format(SQLQueries.Queries.UpdatePendingStatus, orderID, "3");
               // con.Open();
               // cmd.ExecuteNonQuery();
               // cmd.Dispose();
               // con.Close();
                return true;

            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return false;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }


        #endregion
    }
}
