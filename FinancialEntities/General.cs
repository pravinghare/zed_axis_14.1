﻿// ===============================================================================
// 
// General.cs
//
// This base class contains the basic common methods which will be used by
// eBay,OSCommerce, Allied Express,MYOB and QuickBooks classes. 
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace FinancialEntities
{
    /// <summary>
    /// General class is base class of eBay ,OSCommerce, 
    /// MYOB and QuickBooks derived class.
    /// </summary>
    public class General
    {
        
        #region Private Members Or Attributes

        /// <summary>
        /// Private Attributes for General base class.
        /// </summary>
        private string m_FinancialEntityName;//Name of the Financial Entity (Which can be eBay,OSCommerce,Allied Express , MYOB OR QuickBooks).

        #endregion

        #region Public Properties

        /// <summary>
        /// Set the Financial Entity Name of General Class
        /// </summary>
        public string FinancialEntity
        {
            set 
            {
                m_FinancialEntityName = value;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// This protected method is used for Connect Related Financial Entities.This is base class method
        /// which will be override by derived classes.
        /// User can Connect to eBay,OSCommerce,Allied Express
        /// ,MYOB and QuickBooks using this method.
        /// </summary>
        /// <typeparam name="T">Type Param indicate the Class.</typeparam>
        /// <param name="obj">Object of Financial Entities Class.</param>
        virtual protected void Connect<T>(T obj)
        {
 
        }

        /// <summary>
        /// This protected method is used for Disconnect or Closing Existing connections
        /// Related Financial Entities.This is base class method
        /// which will be override by derived classes.
        /// User can Disconnect to eBay,OSCommerce,Allied Express
        /// ,MYOB and QuickBooks servers using this method.
        /// </summary>
        /// <typeparam name="T">Type Param indicate the Class.</typeparam>
        /// <param name="obj">Object of Financial entities Class.</param>
        virtual protected void DisConnect<T>(T obj)
        {

        }

        /// <summary>
        /// This method is used for Save all transaction into Axis v5.0 database.
        /// This is base class method which will be accessed by derived class.
        /// User can use this method for storing eBay Orders or OSCommerce Orders
        /// for all trading Partners.
        /// </summary>
        /// <typeparam name="T">Type Parameter of Class.</typeparam>
        /// <param name="coll">Object of Class Collection.</param>
        /// <returns>This method return flag if true then transaction is successfull
        /// Stored in database otherwise there is error</returns>
        virtual protected bool Save<T>(Collection<T> coll)
        {
            return true;
        }

        /// <summary>
        /// This is static protected method which will be accessible only by Financial Entities classes.
        /// WriteErrorLog maintains all the log details which can contains
        /// Errors,Success Messages,Warnings in a day wise log file.
        ///     User can check trasaction by log. 
        /// </summary>
        /// <param name="message">Pass the Actual message string for log.</param>
        protected static void WriteErrorLog(string message)
        {
            
        }

        #endregion

    }
}
