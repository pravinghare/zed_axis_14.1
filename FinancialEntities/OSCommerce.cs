﻿// ===============================================================================
// 
// OSCommerce.cs
//
// This OSCommerce class contains the methods which are
// Connect , Disconnect , Save Order, GetOrder etc. 
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.IO;
using Tamir.SharpSsh.jsch;
using System.Windows.Forms;
using EDI.Constant;
using TradingPartnerDetails;
using System.Net.Sockets;
using DataProcessingBlocks;
using MySql.Data.MySqlClient;

namespace FinancialEntities
{
    /// <summary>
    /// This class is used for manage OSCommerce Transactions.
    /// </summary>
    public class OSCommerce : General
    {
        #region Private Members

        /// <summary>
        /// Private Members for create OSCommerce Transactions.
        /// </summary>
        private string oscWebStoreUrl;//OSCommerce Web Store Url or server.
        private string oscUserName;//OSCommerce User Name for database.
        private string oscPassword;//OSCommerce Password for database.
        private string databaseName;//OSCommerce Database Name.
        private DateTime lastUpdated;
        private string osCommerceConnectionString;
        private static OSCommerce m_OSCommerce;

        private string dbHostName = string.Empty;
        private string dbPort = string.Empty;
        private string dbUserName = string.Empty;
        private string dbPassword = string.Empty;
        private string dbPrefix = string.Empty;
        private string dbName = string.Empty;
        private string sshHostName = string.Empty;
        private string sshPortNo = string.Empty;
        private string sshUserName = string.Empty;
        private string sshPassword = string.Empty;
        Session session;
        JSch jsch = new JSch();
        #endregion

        #region Public Properties

        /// <summary>
        /// Get or Set Web Store Url or Server of OSCommerce.
        /// </summary>
        public string WebStoreUrl
        {
            get
            {
                return oscWebStoreUrl;
            }
            set
            {
                oscWebStoreUrl = value;
            }
        }

        /// <summary>
        /// Get or Set OSCommerce User Name.
        /// </summary>
        public string UserName
        {
            get
            {
                return oscUserName;
            }
            set
            {
                oscUserName = value;
            }
        }

        /// <summary>
        /// Get or Set Password of OSCommerce.
        /// </summary>
        public string Password
        {
            get
            {
                return oscPassword;
            }
            set
            {
                oscPassword = value;
            }
        }

        /// <summary>
        /// Get or Set Database Name of OSCommerce.
        /// </summary>
        public string DatabaseName
        {
            get
            {
                return databaseName;
            }
            set
            {
                databaseName = value;
            }
        }

        /// <summary>
        /// Get or Set Last Updated date for getting orders.
        /// </summary>
        public DateTime LastUpdated
        {
            get
            {
                return lastUpdated;
            }
            set
            {
                lastUpdated = value;
            }
        }
         

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor code for generate OSCommerce Class.
        /// </summary>
        public OSCommerce()
        {
           

        }

        #endregion

        #region Static Methods
        /// <summary>
        /// This method is used for getting Current Instance of OSCommerce.
        /// </summary>
        /// <returns></returns>
        public static OSCommerce GetInstance()
        {
            if (m_OSCommerce == null)
                m_OSCommerce = new OSCommerce();
            return m_OSCommerce;
        }

        #endregion

        #region Methods and Events

        /// <summary>
        /// This method is used for checking connection of OSCommerce.
        /// </summary>
        /// <param name="oscServer">Server Name of OSCommerce.</param>
        /// <param name="userName">UserName of OSCommerce.</param>
        /// <param name="password">Password of OSCommerce.</param>
        /// <param name="databaseName">Database name of OSCommerce.</param>
        /// <returns></returns>
        public bool CheckConnection(string oscServer, string userName, string password, string databaseName , string port)
        {           
            MySqlConnection con = new MySqlConnection();
          
            try
            {
                if (port == string.Empty || port == "3306")
                {
                    con.ConnectionString = "Server=" + oscServer + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }
                else
                {
                    con.ConnectionString = "Server=" + oscServer + ";Port=" + port + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }
                
            }
            catch (ArgumentException ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                return false;
            }
            try
            {
                //Connect to OSCommerce Server.
                con.Open();
            }
            catch (Exception ex)
            {
                //Log the error
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
            
            con.Close();
            return true;                       
        }

        public bool CheckOrderV2(string oscServer, string userName, string password, string databaseName,string port)
        {
            try
            {
                MySqlConnection con = new MySqlConnection();
                string connectString=string.Empty;
                if (port == string.Empty || port == "3306")
                    connectString = "Server=" + oscServer + ";Database=" + databaseName + ";User Id=" + userName + ";Pwd=" + password + ";";
                else
                    connectString = "Server=" + oscServer + ";Port=" + port + ";Database=" + databaseName + ";User Id=" + userName + ";Pwd=" + password + ";";
                try
                {
                    con.ConnectionString = connectString;
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    return false;
                }
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                this.osCommerceConnectionString = connectString;
                //cmd.CommandText = "SHOW FULL TABLES FROM " + databaseName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + databaseName + "='Orders'";
                cmd.CommandText = "SHOW TABLES IN " + databaseName;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                bool flag = true;
                while (dr.Read())
                {
                    if (dr[0].ToString().ToLower() == dbPrefix.Trim()+"orders")
                    {
                        flag = true;
                        break;
                    }
                    else
                        flag = false;
                }
                dr.Close();
                cmd.Dispose();
                con.Close();
                return flag;
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
        }

        public bool CheckOrderV3(string oscServer, string userName, string password, string databaseName,string port)
        {
            try
            {
                MySqlConnection con = new  MySqlConnection();
                string connectString = string.Empty;
                if (port == string.Empty || port == "3306")
                    connectString = "Server=" + oscServer + ";Database=" + databaseName + ";User Id=" + userName + ";Pwd=" + password + ";";
                else
                    connectString = "Server=" + oscServer + ";Port=" + port + ";Database=" + databaseName + ";User Id=" + userName + ";Pwd=" + password + ";";

                try
                {
                    con.ConnectionString = connectString;
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    return false;
                }
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                this.osCommerceConnectionString = connectString;
                //cmd.CommandText = "SHOW FULL TABLES FROM " + databaseName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + databaseName + "='osc_Orders'";
                cmd.CommandText = "SHOW TABLES IN " + databaseName;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                bool flag = true;
                while (dr.Read())
                {
                    if (dr[0].ToString().ToLower() == "osc_orders")
                    {
                        flag = true;
                        break;
                    }
                    else
                        flag = false;
                }
                dr.Close();
                cmd.Dispose();
                con.Close();
                return flag;
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
        }

        public string[] GetShippingDetails(string connectionString,int version,string orderID)
        {
            string[] shipDetails = new string[2];

            MySqlConnection con = new MySqlConnection();
            try
            {
                con.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                return shipDetails;
            }
            MySqlCommand cmd = new  MySqlCommand();
            cmd.Connection = con;
            string Query = string.Empty;
            if (version == 2)
            {
                Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldShipping, orderID);
            }
            else
            {
                Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringShipping, orderID);
            }
            
            cmd.CommandText = Query;
            con.Open();
            MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
            DataSet orderDataSet = new DataSet();
            orderAdapter.Fill(orderDataSet);
            cmd.Dispose();
            con.Close();

            if (orderDataSet == null)
            {
                return null;
            }
            else
                if (orderDataSet.Tables[0].Rows.Count == 0)
                {
                    return null;
                }

            shipDetails[0] = orderDataSet.Tables[0].Rows[0][0].ToString();
            shipDetails[1] = orderDataSet.Tables[0].Rows[0][1].ToString();
           
            return shipDetails;
        }

        public string GetTotal(string connectionString, int version, string orderID)
        {
            string shipDetails = string.Empty;

            MySqlConnection con = new  MySqlConnection();
            try
            {
                con.ConnectionString = connectionString;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                return shipDetails;
            }

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            string Query = string.Empty;
            if (version == 2)
            {
                Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldTotal, orderID);
            }
            else
            {
                Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringTotal, orderID);
            }

            cmd.CommandText = Query;
            con.Open();
            MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
            DataSet orderDataSet = new DataSet();
            orderAdapter.Fill(orderDataSet);
            cmd.Dispose();
            con.Close();

            if (orderDataSet == null)
            {
                return string.Empty;
            }
            else
                if (orderDataSet.Tables[0].Rows.Count == 0)
                {
                    return string.Empty;
                }

            shipDetails = orderDataSet.Tables[0].Rows[0][0].ToString();
            return shipDetails;
        }

        /// <summary>
        /// This method is used for Getting Orders by single Trading partner 
        /// Using TP's login credential details.this method will use MYSQL ODBC for  
        /// Communication to OSCommerce Database.
        /// </summary>
        /// <param name="updatedDate">Last updated Date</param>
        /// <returns>Return Order Details into String Builder.</returns>
        public string GetOrderByTradingPartner(DateTime updatedDate, string oscommerceServer,string userName,string password,string dbName, string tradingPartnerName,string port)
        {
            #region Checking Which Version is used

            int version = 2;
            if (CheckOrderV2(oscommerceServer, userName, password, dbName,port))
                version = 2;
            else
                if (CheckOrderV3(oscommerceServer, userName, password, dbName,port))
                    version = 3;

            #endregion

            StringBuilder sb = new StringBuilder();

            string orderQuery = string.Empty;
            if (version == 2)
            {
                orderQuery = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldOrder, updatedDate.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else
            {
                orderQuery = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOrder, updatedDate.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            try
            {
                MySqlConnection con = new MySqlConnection();
                try
                {
                    string connectStr = string.Empty;
                    if (port == string.Empty || port == "3306")
                        connectStr = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                    else
                        connectStr = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";

                    con.ConnectionString = connectStr;
                    this.osCommerceConnectionString = con.ConnectionString;
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    return string.Empty;
                }

                MySqlCommand cmd = new MySqlCommand();

                cmd.Connection = con;
                //Getting Trading partner id.
                cmd.CommandText = orderQuery;
                con.Open();
                MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
                DataSet orderDataSets = new DataSet();
                orderAdapter.Fill(orderDataSets);
                cmd.Dispose();
                con.Close();

                if (orderDataSets == null)
                {
                    return string.Empty;
                }
                else
                {
                    if (orderDataSets.Tables[0].Rows.Count == 0)
                    {
                        return string.Empty;
                    }
                }

                DataView orderDataSet = orderDataSets.Tables[0].DefaultView;
                orderDataSet.Sort = "orders_id";

                for (int count = 0; count < orderDataSet.Table.Rows.Count; count++)
                {

                    #region Process Order

                    string Query = string.Empty;
                    if (version == 2)
                    {
                        Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringOldMain, orderDataSet.Table.Rows[count][0].ToString());
                    }
                    else
                    {
                        Query = string.Format(EDI.Constant.Constants.OSCommerceMySqlServerOrderStringMain, orderDataSet.Table.Rows[count][0].ToString());
                    }
                    try
                    {
                        MySqlConnection orderCon = new MySqlConnection();
                        try
                        {
                            string connectString=string.Empty;

                            if (port == string.Empty || port == "3306")
                                connectString = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                            else
                                connectString = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";

                            orderCon.ConnectionString = connectString;
                            this.osCommerceConnectionString = connectString;
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            return string.Empty;
                        }
                        MySqlCommand orderCmd = new  MySqlCommand();
                        orderCmd.Connection = orderCon;
                        orderCmd.CommandText = Query;
                        orderCon.Open();

                        MySqlDataAdapter dtAdapter = new MySqlDataAdapter(orderCmd);
                        DataSet ds = new DataSet();
                        dtAdapter.Fill(ds);
                        orderCmd.Dispose();
                        orderCon.Close();

                        if (ds == null)
                        {
                            continue;
                        }
                        else
                        {
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                continue;
                            }
                            else
                            {
                                #region Store OSCommerce Order

                                string headerLine = string.Empty;
                                string[] middleLine = new string[ds.Tables[0].Rows.Count];
                                for (int temp = 0; temp < ds.Tables[0].Rows.Count; temp++)
                                {
                                    string[] shipInfo = new string[2];
                                    if (ds.Tables[0].Rows[temp][0].ToString() != string.Empty)
                                    {
                                        shipInfo = GetShippingDetails(this.osCommerceConnectionString, version, ds.Tables[0].Rows[temp][0].ToString());
                                        string total = GetTotal(this.osCommerceConnectionString, version, ds.Tables[0].Rows[temp][0].ToString());

                                        if (temp == ds.Tables[0].Rows.Count - 1)
                                        {
                                            #region Insert data into database

                                            if (version == 2)
                                            {
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            }
                                            else
                                            {
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            }


                                            headerLine += "OSCommerce|" + tradingPartnerName + "|" + ds.Tables[0].Rows[temp][0].ToString() + "\n";
                                            headerLine += "OSCommerce1|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][1].ToString() + "|" + ds.Tables[0].Rows[temp][2].ToString() + "|" + ds.Tables[0].Rows[temp][3].ToString() + "|" + ds.Tables[0].Rows[temp][4].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][5].ToString() + "|" + ds.Tables[0].Rows[temp][6].ToString() + "|" + ds.Tables[0].Rows[temp][7].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][8].ToString() + "|" + ds.Tables[0].Rows[temp][9].ToString() + "|" + ds.Tables[0].Rows[temp][10].ToString() + "|";
                                            if (shipInfo == null)
                                                headerLine += "" + string.Empty + "|" + string.Empty + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                            else
                                                headerLine += "" + (shipInfo == null ? string.Empty : shipInfo[1].ToString()) + "|" + (shipInfo == null ? string.Empty : shipInfo[0].ToString()) + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][12].ToString() + "|" + ds.Tables[0].Rows[temp][13].ToString() + "\n";

                                            sb.AppendLine(headerLine);


                                            foreach (string item in middleLine)
                                            {
                                                if (item != null)
                                                {
                                                    if (item != string.Empty)
                                                    {
                                                        sb.AppendLine(item);
                                                    }
                                                }
                                            }

                                            Save(sb, ds.Tables[0].Rows[temp][0].ToString(), tradingPartnerName);
                                            headerLine = string.Empty;
                                            sb = new StringBuilder();
                                           
                                            middleLine = new string[ds.Tables[0].Rows.Count];
                                            #endregion
                                        }
                                        else
                                        {
                                            if (version == 2)
                                            {
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            }
                                            else
                                            {
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            }

                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                        ds = null;

                    }
                    catch (Exception ex)
                    {
                        DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                        DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                        return ex.Message;
                    }

                    #endregion
                }

                return string.Empty;

            }
            catch(Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return string.Empty;
            }

                     
        }
        
        /// <summary>
        /// This method is used for Getting Order into Proper format
        /// and Store that order into Axis v5.0 database.
        /// </summary>
        /// <param name="respData">Object of String Buider which will be stored in database.</param>
        private void Save(StringBuilder respData, string orderId, string tradingPartnerName)
        {
            OdbcConnection con = new OdbcConnection();
            try
            {
                con.ConnectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                return;
            }

            OdbcCommand cmd = new OdbcCommand();

            cmd.Connection = con;
            //Getting Trading partner id.
            cmd.CommandText = string.Format(EDI.Constant.Constants.GetTPIDByTPName, tradingPartnerName);
            con.Open();
            OdbcDataReader dr = cmd.ExecuteReader();

            string tradingPartnerID = string.Empty;

            while (dr.Read())
            {
                tradingPartnerID = dr[0].ToString();
            }

            dr.Close();
            cmd.Dispose();
            con.Close();

            MemoryStream stream = null;
            string Attachname = "OSCommerce  " + tradingPartnerName + " " + orderId;

            try
            {
                con.Open();
                string myString = respData.ToString();
                byte[] myByteArray = System.Text.Encoding.ASCII.GetBytes(myString);
                stream = new MemoryStream(myByteArray);

                BinaryReader mmsfileReader = new BinaryReader((Stream)stream);
                string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                //Insert Message data into database.
                OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)stream.Length));
                string serviceText = string.Empty;
                //string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','3','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'5')";
                string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolID) VALUES ('" + MessageDate + "','10','" + tradingPartnerName + "','Order " + orderId + "',0," + tradingPartnerID + ",'5')";
                cmd.Connection = con;
                cmd.CommandText = commandText;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                //For getting Message id from message schema.
                OdbcCommand cmdObj = new OdbcCommand();
                con.Open();
                cmdObj.Connection = con;
                cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                object messageID = cmdObj.ExecuteScalar();
                cmdObj.Dispose();
                con.Close();
                //For adding eBay message into Database.
                string insertMessageCommand = string.Empty;
                if (messageID != null)
                    //insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'3')";
                    insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'10')";
                OdbcCommand cmdFile = new OdbcCommand();
                con.Open();
                cmdFile.Connection = con;
                cmdFile.CommandText = insertMessageCommand;
                cmdFile.Parameters.Add(paramFile);
                cmdFile.ExecuteNonQuery();
                con.Close();
                mmsfileReader.Close();
                stream.Close();

                //For update LastUpdated ebay date.
                string commandDateText = " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tradingPartnerID + "";
                OdbcCommand commandDate = new OdbcCommand();
                commandDate.Connection = con;
                con.Open();
                commandDate.CommandText = commandDateText;
                commandDate.ExecuteNonQuery();
                commandDate.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());

            }

        }

        /// <summary>
        /// This method is used for update the status of OSCommerce Order
        /// to Shipped in OSCommerce Database.
        /// </summary>
        /// <param name="orderID">Order ID of Message</param>
        public bool UpdateStatus(string orderID, string tradingPartnerID,DateTime ShippedTime, string consignmentNote)
        {
            session = null;
            bool result = false;
            string port = string.Empty;
            consignmentNote = "Shipped via Allied Express Consignment note " + consignmentNote + ".";
            //Get OSCommerce details by tradingPartnerID.
            DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.TPWithOSCommerce, tradingPartnerID));
            if (dataset != null)
            {
                dbHostName = dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                dbUserName = dataset.Tables[0].Rows[0]["UserName"].ToString();
                dbPassword = dataset.Tables[0].Rows[0]["Password"].ToString();
                dbPrefix = dataset.Tables[0].Rows[0]["XSDLocation"].ToString();
               
                if (dbPassword.Contains(";"))
                    dbPassword = "'" + dbPassword + "'";
                dbPort = dataset.Tables[0].Rows[0]["eBayAuthToken"].ToString();

                if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString()))
                {
                    sshHostName = dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                    sshPortNo = dataset.Tables[0].Rows[0]["eBayDevId"].ToString();
                    sshUserName = dataset.Tables[0].Rows[0]["eBayCertID"].ToString();
                    sshPassword = dataset.Tables[0].Rows[0]["eBayAppId"].ToString();
                    if (sshPassword.Contains(";"))
                        sshPassword = "'" + sshPassword + "'";
                    dbName = dataset.Tables[0].Rows[0]["OSCDatabase"].ToString();
                }

                if (dataset.Tables[0].Rows[0][2].ToString() == "5")
                {
                    //Get max status id from orders_status_history
                    int StatusID = GetMaxStatusID(orderID, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dbPassword, dbPort);
                   
                    if (StatusID == 3)
                    { 
                      //Update orders_status_history
                        result = UpdateOSCommerceStatus(orderID, StatusID, ShippedTime, consignmentNote, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dataset.Tables[0].Rows[0]["Password"].ToString(),dbPort);
                    }
                    else
                    { 
                       //Insert Orders_status_history
                        result = InsertOrderStatusHistory(orderID, StatusID, ShippedTime, consignmentNote, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dataset.Tables[0].Rows[0]["Password"].ToString(),dbPort);
                    }
                }               
            }

            return result;
        }

        public bool UpdatePendingStatus(string orderID, string tradingPartnerID)
        {
            session = null;
            bool result = false;
            string port = string.Empty;
            DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.TPWithOSCommerce, tradingPartnerID));
            if (dataset != null)
            {
                dbHostName = dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                dbUserName = dataset.Tables[0].Rows[0]["UserName"].ToString();
                dbPassword = dataset.Tables[0].Rows[0]["Password"].ToString();
                dbPrefix = dataset.Tables[0].Rows[0]["XSDLocation"].ToString();
                if (dbPassword.Contains(";"))
                    dbPassword = "'" + dbPassword + "'";
                dbPort = dataset.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString()))
                {
                    sshHostName = dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                    sshPortNo = dataset.Tables[0].Rows[0]["eBayDevId"].ToString();
                    sshUserName = dataset.Tables[0].Rows[0]["eBayCertID"].ToString();
                    sshPassword = dataset.Tables[0].Rows[0]["eBayAppId"].ToString();
                    if (sshPassword.Contains(";"))
                        sshPassword = "'" + sshPassword + "'";
                    dbName = dataset.Tables[0].Rows[0]["OSCDatabase"].ToString();
                }
                if (dataset.Tables[0].Rows[0][2].ToString() == "5")
                {
                    int StatusID = GetStatusID(dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dbPassword, dbPort);
                    result = UpdateOSCommercePendingStatus(orderID, StatusID, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dataset.Tables[0].Rows[0]["Password"].ToString(), dbPort);
                }
            }
            return result;
        }
        /// <summary>
        ///  Get the max status id present for order ID in orders_status_history.
        /// </summary>
        /// <returns></returns>
        private int GetMaxStatusID(string orderID,string oscommerceServer, string dbName, string userName, string password,string newport)
        {
            try
            {
                int id = 0;
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    int port = 22;

                    session = jsch.getSession(sshUserName, sshHostName, port);
                    session.setHost(sshHostName);
                    session.setPassword(sshPassword);
                    UserInfo ui = new MyUserInfo();
                    session.setUserInfo(ui);

                    session.connect();
                    //Set port forwarding on the opened session.
                    session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));
                    if (session.isConnected())
                    {
                        //Create a ODBC Connection.
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();

                        string constring = string.Empty;
                        //if (newport == string.Empty || newport == "3306")
                            constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        //else
                        //    constring = "Server=" + dbHostName + ";Port=" + newport + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                        //this.osCommerceConnectionString = conn.ConnectionString;

                        cmmd.CommandText = string.Format(SQLQueries.Queries.OSCommerceMaxStatusID,dbPrefix.Trim(), orderID);
                        id = Convert.ToInt32(cmmd.ExecuteScalar());
                        conn.Close();
                    }
                    return id;
                }
                else
                {
                     MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();

                    try
                    {
                        string constring = string.Empty;

                        if (newport == string.Empty || newport == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + newport + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";

                        con.ConnectionString = constring;
                        this.osCommerceConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return id;
                    }

                     MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    //Getting Trading partner id.
                    cmd.CommandText = string.Format(SQLQueries.Queries.OSCommerceMaxStatusID,dbPrefix.Trim(), orderID);
                    con.Open();
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Dispose();
                    con.Close();
                    return id;
                }

            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return 0;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }

        private int GetStatusID(string oscommerceServer, string dbName, string userName, string password, string newport)
        {
            try
            {
                int id = 0;
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    int port = 22;
                    session = jsch.getSession(sshUserName, sshHostName, port);
                    session.setHost(sshHostName);
                    session.setPassword(sshPassword);
                    UserInfo ui = new MyUserInfo();
                    session.setUserInfo(ui);
                    session.connect();
                    session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        string constring = string.Empty;
                        constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                        cmmd.CommandText = string.Format(SQLQueries.Queries.SelectPendingStatus, dbPrefix.Trim());
                        MySql.Data.MySqlClient.MySqlDataReader dr = cmmd.ExecuteReader();
                        while (dr.Read())
                        {
                            id = Convert.ToInt32(dr[0].ToString());
                        }
                        dr.Close();
                        conn.Close();
                    }
                    return id;
                }
                else
                {
                    MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                    try
                    {
                        string constring = string.Empty;
                        if (newport == string.Empty || newport == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + newport + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        con.ConnectionString = constring;
                        this.osCommerceConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return id;
                    }
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format(SQLQueries.Queries.SelectPendingStatus, dbPrefix.Trim());
                    con.Open();
                    MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr[0].ToString());
                    }
                    dr.Close();
                    cmd.Dispose();
                    con.Close();
                    return id;
                }
            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return 0;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }
        /// <summary>
        /// UPdate order status ID in orders_status_history
        /// </summary>
        /// <returns></returns>
        private bool UpdateOSCommerceStatus(string orderID,int orderstatusID,DateTime ShippedTime,string consignmentNote, string oscommerceServer, string dbName, string userName, string password,string port)
        {

            try
            {
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    if (session.isConnected())
                    {

                        //Create a ODBC Connection.
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();

                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        //if (port == string.Empty || port == "3306")
                            constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        //else
                        //    constring = "Server=" + dbHostName + ";Port=" + port + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                     
                        cmmd.CommandText = string.Format(SQLQueries.Queries.UpdateStatus,dbPrefix.Trim(), ShippedTime.ToString("yyyy-MM-dd HH:mm:ss"),  consignmentNote, orderID, "3");
                        cmmd.ExecuteNonQuery();
                        conn.Close();
                        cmmd.Dispose();
                        return true;
                    }
                    else
                    {
                        return false;
                    }                    
                }
                else
                {
                    MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                    try
                    {
                        if (password.Contains(";"))
                             password = "'" + password + "'";
                        string constring = string.Empty;
                        if (port == string.Empty || port == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        //con.ConnectionString = "Driver={MySQL ODBC 3.51 Driver};Server=" + oscommerceServer + ";Database=" + dbName + ";User=" + userName + ";Password=" + password + ";";
                        con.ConnectionString = constring;
                        this.osCommerceConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return false;
                    }

                     MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    //Getting Trading partner id.
                    cmd.CommandText = string.Format(SQLQueries.Queries.UpdateStatus,dbPrefix.Trim(), ShippedTime.ToString("yyyy-MM-dd HH:mm:ss"), consignmentNote, orderID, "3");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    return true;
                }
            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return false;
            }
            
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        private bool UpdateOSCommercePendingStatus(string orderID, int orderstatusID, string oscommerceServer, string dbName, string userName, string password, string port)
        {
            try
            {
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                        cmmd.CommandText = string.Format(SQLQueries.Queries.UpdatePendingStatus , dbPrefix.Trim(), orderID, "3");
                        cmmd.ExecuteNonQuery();
                        conn.Close();
                        cmmd.Dispose();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                    try
                    {
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        if (port == string.Empty || port == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        con.ConnectionString = constring;
                        this.osCommerceConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return false;
                    }
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format(SQLQueries.Queries.UpdatePendingStatus, dbPrefix.Trim(), orderID, "3");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    return true;
                }
            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return false;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }
        /// <summary>
        /// Insert order in orders_status_history.
        /// </summary>
        /// <returns></returns>
        private bool InsertOrderStatusHistory(string orderID, int orderstatusID,DateTime ShippedTime,string consignmentNote, string oscommerceServer, string dbName, string userName, string password,string port)
        {
            try
            {
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    if (session.isConnected())
                    {

                        //Create a ODBC Connection.
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        //if (port == string.Empty || port == "3306")
                            constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        //else
                          //  constring = "Server=" + dbHostName + ";Port=" + port + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        
                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                        //this.osCommerceConnectionString = conn.ConnectionString;

                        cmmd.CommandText = string.Format(SQLQueries.Queries.InsertStatus,dbPrefix.Trim(), orderID, "3", ShippedTime.ToString("yyyy-MM-dd HH:mm:ss"), consignmentNote);
                        cmmd.ExecuteNonQuery();
                        conn.Close();
                        cmmd.Dispose();
                        return true;
                    }
                    else
                    {
                        return false;
                    }                    
                }
                else
                {
                    MySqlConnection con = new MySqlConnection();
                    try
                    {
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        if (port == string.Empty || port == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        
                        con.ConnectionString = constring;
                        this.osCommerceConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return false;
                    }

                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    //Getting Trading partner id.
                    cmd.CommandText = string.Format(SQLQueries.Queries.InsertStatus,dbPrefix.Trim(), orderID, "3", ShippedTime.ToString("yyyy-MM-dd HH:mm:ss"), consignmentNote);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    return true;
                }

            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return false;
            }
            
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }


        public bool CheckOrderV2Test(string db_Port,string OscHostName, string OscUserName, string OscPassword, string OscDatabaseName,string OscPort,string SSHHostname,string SSHPassword,string SSHPortNo,string SSHUsername,string dbPrefix)
        {
            try
            {
                MySqlConnection con = new MySqlConnection();
                MySqlCommand cmd = new MySqlCommand();

                #region Whether SSH

                if (!string.IsNullOrEmpty(SSHHostname))
                {
                    int port = 22;

                    try
                    {
                        session = jsch.getSession(SSHUsername, SSHHostname, port);
                        session.setHost(SSHHostname);
                        session.setPassword(SSHPassword);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);

                        session.connect();

                        //Set port forwarding on the opened session.
                        //session.setPortForwardingL(Convert.ToInt32(dbPort), OscHostName, Convert.ToInt32(SSHPortNo));
                        //int db_Port = Convert.ToInt32(textBoxDBPortNo.Text.Trim()).ToString();
                        //session.setPortForwardingL(Convert.ToInt32(db_Port), OscHostName, Convert.ToInt32(SSHPortNo));
                        if (session.isConnected())
                        {
                            try
                            {
                                //Create a ODBC Connection.
                                MySqlConnection conn = new MySqlConnection();
                                MySqlCommand cmmd = new MySqlCommand();
                                MySqlDataReader dr;

                                conn.ConnectionString = "Server=" + OscHostName + ";User id=" + OscUserName + ";Pwd=" + OscPassword + ";Database=" + OscDatabaseName + ";Connection Timeout = 30;";
                                if (conn.State != ConnectionState.Open)
                                    conn.Open();
                                cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                                cmmd.Connection = conn;
                                this.osCommerceConnectionString = conn.ConnectionString;

                                //cmmd.CommandText = "SHOW FULL TABLES FROM " + dbName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + dbName + "='Orders'";
                                cmmd.CommandText = "SHOW TABLES IN " + OscDatabaseName;
                                dr = cmmd.ExecuteReader();
                                bool flag = true;
                                while (dr.Read())
                                {
                                    if (dr[0].ToString().ToLower() == dbPrefix.Trim() + "orders")
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else
                                        flag = false;
                                }
                                dr.Close();
                                cmmd.Dispose();
                                if (conn.State != ConnectionState.Closed)
                                    conn.Close();
                                return flag;
                            }
                            catch (SocketException)
                            {
                                MessageBox.Show("Connection timeout for the OSCommerce.Please try again.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                session.disconnect();
                                return false;
                            }
                            catch { }
                        }
                    }
                    catch (SocketException ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message + ex.StackTrace);
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        session.disconnect();
                        return false;

                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message + ex.StackTrace);
                        MessageBox.Show("Test Connection Failed.Please enter the correct details & try again.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        session.disconnect();
                        return false;
                    }
                    session.disconnect();
                }
                #endregion
                else
                {
                    con = new MySqlConnection();
                    MySqlDataReader dr;
                    string connectString = string.Empty;
                    if (OscPort == string.Empty || OscPort == "3306")
                        connectString = "Server=" + OscHostName + ";User id=" + OscUserName + ";Pwd=" + OscPassword + ";Database=" + OscDatabaseName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + OscHostName + ";Port=" + OscPort + ";User id=" + OscUserName + ";Pwd=" + OscPassword + ";Database=" + OscDatabaseName + ";Connection Timeout = 30;";
                    con.ConnectionString = connectString;
                    cmd = new MySqlCommand();
                    cmd.Connection = con;
                    this.osCommerceConnectionString = connectString;


                    //cmd.CommandText = "SHOW FULL TABLES FROM " + databaseName + " WHERE TABLE_TYPE='BASE TABLE' AND TABLES_IN_" + databaseName + "='Orders'";
                    cmd.CommandText = "SHOW TABLES IN " + OscDatabaseName;
                    con.Open();
                    dr = cmd.ExecuteReader();
                    bool flag = true;
                    while (dr.Read())
                    {
                        if (dr[0].ToString().ToLower() == dbPrefix.Trim() + "orders")
                        {
                            flag = true;
                            break;
                        }
                        else
                            flag = false;
                    }
                    dr.Close();
                    cmd.Dispose();
                    con.Close();
                    return flag;
                }
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }
            return false;
        }

        
        #endregion
    }
}
