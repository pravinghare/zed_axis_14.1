using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.IO;
using Tamir.SharpSsh.jsch;
using System.Windows.Forms;
using EDI.Constant;
using TradingPartnerDetails;
using System.Net.Sockets;
using DataProcessingBlocks;
using MySql.Data.MySqlClient;

namespace FinancialEntities
{
    public class ZenCart : General
    {
        #region Private Members

        private string zencartWebUrl;
        private string zencartPort;
        private string zencartUsername;
        private string zencartPassword;
        private string zencartDatabaseName;
        private static ZenCart m_Zencart;
        private string zenConnectionString;
        Session session;

        private string dbHostName = string.Empty;
        private string dbPort = string.Empty;
        private string dbUserName = string.Empty;
        private string dbPassword = string.Empty;
        private string dbPrefix = string.Empty;
        private string dbName = string.Empty;
        private string sshHostName = string.Empty;
        private string sshPortNo = string.Empty;
        private string sshUserName = string.Empty;
        private string sshPassword = string.Empty;

        #endregion

        #region Public Properties

        public string WebUrl
        {
            get
            {
                return zencartWebUrl;
            }
            set
            {
                zencartWebUrl = value;
            }
        }

        public string Port
        {
            get
            {
                return zencartPort;
            }
            set
            {
                zencartPort = value;
            }
        }

        public string Username
        {
            get
            {
                return zencartUsername;
            }
            set
            {
                zencartUsername = value;
            }
        }

        public string Password
        {
            get
            {
                return zencartPassword;
            }
            set
            {
                zencartPassword = value;
            }
        }

        public string DatabaseName
        {
            get
            {
                return zencartDatabaseName;
            }
            set
            {
                zencartDatabaseName = value;
            }
        }

        #endregion

        #region Constructor

        public ZenCart()
        {
        }

        #endregion

        #region Static Methods

        public static ZenCart GetInstance()
        {
            if (m_Zencart == null)
                m_Zencart = new ZenCart();
            return m_Zencart;
        }


        #endregion

        #region Public methods

        /// <summary>
        /// Method is used to check database connectivity
        /// </summary>
        /// <param name="zenServer"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="databaseName"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public bool CheckConnection(string zenServer, string userName, string password, string databaseName, string port)
        {
            MySqlConnection con = new MySqlConnection();

            try
            {
                if (port == string.Empty || port == "3306")
                {
                    con.ConnectionString = "Server=" + zenServer + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }
                else
                {
                    con.ConnectionString = "Server=" + zenServer + ";Port=" + port + ";User id=" + userName + ";Pwd=" + password + ";Database=" + databaseName + ";Connection Timeout = 30;";
                }

            }
            catch (ArgumentException ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                return false;
            }
            try
            {
                //Connect to OSCommerce Server.
                con.Open();
            }
            catch (Exception ex)
            {
                //Log the error
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message.ToString());
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return false;
            }

            con.Close();
            return true;
        }

        public bool UpdatePendingStatus(string orderID, string tradingPartnerID)
        {
            session = null;
            bool result = false;
            string port = string.Empty;
            DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.TPWithOSCommerce, tradingPartnerID));
            if (dataset != null)
            {
                dbHostName = dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                dbUserName = dataset.Tables[0].Rows[0]["UserName"].ToString();
                dbPassword = dataset.Tables[0].Rows[0]["Password"].ToString();
                dbPrefix = dataset.Tables[0].Rows[0]["XSDLocation"].ToString();
                if (dbPassword.Contains(";"))
                    dbPassword = "'" + dbPassword + "'";
                dbPort = dataset.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString()))
                {
                    sshHostName = dataset.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                    sshPortNo = dataset.Tables[0].Rows[0]["eBayDevId"].ToString();
                    sshUserName = dataset.Tables[0].Rows[0]["eBayCertID"].ToString();
                    sshPassword = dataset.Tables[0].Rows[0]["eBayAppId"].ToString();
                    if (sshPassword.Contains(";"))
                        sshPassword = "'" + sshPassword + "'";
                    dbName = dataset.Tables[0].Rows[0]["OSCDatabase"].ToString();
                }
                if (dataset.Tables[0].Rows[0][2].ToString() == "5")
                {
                    int StatusID = GetStatusID(dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dbPassword, dbPort);
                    result = UpdateZencartPendingStatus(orderID, StatusID, dataset.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString(), dataset.Tables[0].Rows[0]["OSCDatabase"].ToString(), dataset.Tables[0].Rows[0]["UserName"].ToString(), dataset.Tables[0].Rows[0]["Password"].ToString(), dbPort);
                }
            }
            return result;
        }

        private bool UpdateZencartPendingStatus(string orderID, int orderstatusID, string oscommerceServer, string dbName, string userName, string password, string port)
        {
            try
            {
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    if (session.isConnected())
                    {
                        MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
                        MySql.Data.MySqlClient.MySqlCommand cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        constring = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        conn.ConnectionString = constring;
                        conn.Open();
                        cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                        cmmd.Connection = conn;
                        cmmd.CommandText = string.Format(SQLQueries.Queries.UpdatePendingStatus, "", orderID, "3");
                        cmmd.ExecuteNonQuery();
                        conn.Close();
                        cmmd.Dispose();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                    try
                    {
                        if (password.Contains(";"))
                            password = "'" + password + "'";
                        string constring = string.Empty;
                        if (port == string.Empty || port == "3306")
                            constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        else
                            constring = "Server=" + oscommerceServer + ";Port=" + port + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                        con.ConnectionString = constring;
                        this.zenConnectionString = con.ConnectionString;
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        return false;
                    }
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = con;
                    cmd.CommandText = string.Format(SQLQueries.Queries.UpdatePendingStatus, dbPrefix.Trim(), orderID, "3");
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    return true;
                }
            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return false;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        private int GetStatusID(string oscommerceServer, string dbName, string userName, string password, string newport)
        {
            try
            {
                int id = 0;

                MySql.Data.MySqlClient.MySqlConnection con = new MySqlConnection();
                try
                {
                    string constring = string.Empty;
                    if (newport == string.Empty || newport == "3306")
                        constring = "Server=" + oscommerceServer + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                    else
                        constring = "Server=" + oscommerceServer + ";Port=" + newport + ";Database=" + dbName + ";User Id=" + userName + ";Pwd=" + password + ";";
                    con.ConnectionString = constring;
                    this.zenConnectionString = con.ConnectionString;
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    return id;
                }
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = string.Format(SQLQueries.Queries.SelectPendingStatus, dbPrefix.Trim());
                con.Open();
                MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr[0].ToString());
                }
                dr.Close();
                cmd.Dispose();
                con.Close();
                return id;

            }
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                session.disconnect();
                return 0;
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }

        #endregion

    }
}
