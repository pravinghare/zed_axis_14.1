﻿namespace DataProcessingBlocks
{
    partial class ColumnChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxColChooser = new System.Windows.Forms.GroupBox();
            this.grpbEditTemplate = new System.Windows.Forms.GroupBox();
            this.cmbEditTemplate = new System.Windows.Forms.ComboBox();
            this.lblEditDel = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.grpbDelete = new System.Windows.Forms.GroupBox();
            this.cmbDelete = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnColChooser = new System.Windows.Forms.Button();
            this.textBoxColChooser = new System.Windows.Forms.TextBox();
            this.lblTemplateName = new System.Windows.Forms.Label();
            this.panelColChooser = new System.Windows.Forms.Panel();
            this.groupBoxColChooser.SuspendLayout();
            this.grpbEditTemplate.SuspendLayout();
            this.grpbDelete.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxColChooser
            // 
            this.groupBoxColChooser.Controls.Add(this.grpbEditTemplate);
            this.groupBoxColChooser.Controls.Add(this.btnCancel);
            this.groupBoxColChooser.Controls.Add(this.grpbDelete);
            this.groupBoxColChooser.Controls.Add(this.btnEdit);
            this.groupBoxColChooser.Controls.Add(this.btnColChooser);
            this.groupBoxColChooser.Controls.Add(this.textBoxColChooser);
            this.groupBoxColChooser.Controls.Add(this.lblTemplateName);
            this.groupBoxColChooser.Controls.Add(this.panelColChooser);
            this.groupBoxColChooser.Location = new System.Drawing.Point(12, 1);
            this.groupBoxColChooser.Name = "groupBoxColChooser";
            this.groupBoxColChooser.Size = new System.Drawing.Size(443, 482);
            this.groupBoxColChooser.TabIndex = 13;
            this.groupBoxColChooser.TabStop = false;
            this.groupBoxColChooser.Text = "Column chooser";
            // 
            // grpbEditTemplate
            // 
            this.grpbEditTemplate.Controls.Add(this.cmbEditTemplate);
            this.grpbEditTemplate.Controls.Add(this.lblEditDel);
            this.grpbEditTemplate.Location = new System.Drawing.Point(16, 34);
            this.grpbEditTemplate.Name = "grpbEditTemplate";
            this.grpbEditTemplate.Size = new System.Drawing.Size(405, 79);
            this.grpbEditTemplate.TabIndex = 14;
            this.grpbEditTemplate.TabStop = false;
            this.grpbEditTemplate.Text = "EditTemplate";
            // 
            // cmbEditTemplate
            // 
            this.cmbEditTemplate.FormattingEnabled = true;
            this.cmbEditTemplate.Location = new System.Drawing.Point(126, 29);
            this.cmbEditTemplate.Name = "cmbEditTemplate";
            this.cmbEditTemplate.Size = new System.Drawing.Size(231, 21);
            this.cmbEditTemplate.TabIndex = 1;
            this.cmbEditTemplate.SelectedIndexChanged += new System.EventHandler(this.cmbEditTemplate_SelectedIndexChanged);
            // 
            // lblEditDel
            // 
            this.lblEditDel.AutoSize = true;
            this.lblEditDel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEditDel.Location = new System.Drawing.Point(17, 30);
            this.lblEditDel.Name = "lblEditDel";
            this.lblEditDel.Size = new System.Drawing.Size(103, 17);
            this.lblEditDel.TabIndex = 0;
            this.lblEditDel.Text = "Edit Template :";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(346, 443);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 33);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // grpbDelete
            // 
            this.grpbDelete.Controls.Add(this.cmbDelete);
            this.grpbDelete.Controls.Add(this.label1);
            this.grpbDelete.Location = new System.Drawing.Point(27, 11);
            this.grpbDelete.Name = "grpbDelete";
            this.grpbDelete.Size = new System.Drawing.Size(394, 84);
            this.grpbDelete.TabIndex = 15;
            this.grpbDelete.TabStop = false;
            this.grpbDelete.Text = "Delete Template";
            // 
            // cmbDelete
            // 
            this.cmbDelete.FormattingEnabled = true;
            this.cmbDelete.Location = new System.Drawing.Point(134, 35);
            this.cmbDelete.Name = "cmbDelete";
            this.cmbDelete.Size = new System.Drawing.Size(223, 21);
            this.cmbDelete.TabIndex = 16;
            this.cmbDelete.SelectedIndexChanged += new System.EventHandler(this.cmbDelete_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Delete Template :";
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(153, 443);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 33);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Update";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnColChooser
            // 
            this.btnColChooser.Location = new System.Drawing.Point(248, 443);
            this.btnColChooser.Name = "btnColChooser";
            this.btnColChooser.Size = new System.Drawing.Size(75, 33);
            this.btnColChooser.TabIndex = 2;
            this.btnColChooser.Text = "Save";
            this.btnColChooser.UseVisualStyleBackColor = true;
            this.btnColChooser.Click += new System.EventHandler(this.btnColChooser_Click);
            // 
            // textBoxColChooser
            // 
            this.textBoxColChooser.Location = new System.Drawing.Point(146, 119);
            this.textBoxColChooser.Name = "textBoxColChooser";
            this.textBoxColChooser.Size = new System.Drawing.Size(238, 20);
            this.textBoxColChooser.TabIndex = 1;
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.AutoSize = true;
            this.lblTemplateName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemplateName.Location = new System.Drawing.Point(24, 120);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Size = new System.Drawing.Size(116, 17);
            this.lblTemplateName.TabIndex = 0;
            this.lblTemplateName.Text = "Template Name :";
            // 
            // panelColChooser
            // 
            this.panelColChooser.AutoScroll = true;
            this.panelColChooser.AutoScrollMargin = new System.Drawing.Size(1, 2);
            this.panelColChooser.AutoScrollMinSize = new System.Drawing.Size(54, 20);
            this.panelColChooser.Location = new System.Drawing.Point(27, 140);
            this.panelColChooser.Name = "panelColChooser";
            this.panelColChooser.Size = new System.Drawing.Size(394, 297);
            this.panelColChooser.TabIndex = 1;
            // 
            // ColumnChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 495);
            this.Controls.Add(this.groupBoxColChooser);
            this.Name = "ColumnChooser";
            this.Text = "ColumnChooser";
            this.Load += new System.EventHandler(this.ColumnChooser_Load);
            this.groupBoxColChooser.ResumeLayout(false);
            this.groupBoxColChooser.PerformLayout();
            this.grpbEditTemplate.ResumeLayout(false);
            this.grpbEditTemplate.PerformLayout();
            this.grpbDelete.ResumeLayout(false);
            this.grpbDelete.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBoxColChooser;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnColChooser;
        public System.Windows.Forms.TextBox textBoxColChooser;
        public System.Windows.Forms.Label lblTemplateName;
        public System.Windows.Forms.Panel panelColChooser;
        public System.Windows.Forms.GroupBox grpbEditTemplate;
        public System.Windows.Forms.ComboBox cmbEditTemplate;
        public System.Windows.Forms.Label lblEditDel;
        public System.Windows.Forms.Button btnEdit;
        public System.Windows.Forms.GroupBox grpbDelete;
        public System.Windows.Forms.ComboBox cmbDelete;
        public System.Windows.Forms.Label label1;
    }
}