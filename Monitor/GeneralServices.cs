﻿// ===============================================================================
// 
// GeneralServices.cs
//
// This base class contains the basic common methods which will be used by
// eBay and Allied Express classes. 
// This class contains implementation of Properties and Methods.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Monitor
{
    /// <summary>
    /// General Services class is base class of eBay and OSCommerce class.
    /// </summary>
    public class GeneralServices
    {
        
        #region Private Members Or Attributes

        /// <summary>
        /// Private Attributes for General base class.
        /// </summary>
        private string m_ServiceName;//Name of the Service.
        private string m_ProtocolName;//Name of Protocol type.

        #endregion

        #region Public Properties

        /// <summary>
        /// Set the Service Name of General Service Class
        /// </summary>
        public string ServiceName
        {
            set 
            { 
                m_ServiceName = value; 
            }
        }

        /// <summary>
        /// Get or Set ProtocolName for Services.
        /// </summary>
        public string ProtocolName
        {
            get 
            { 
                return m_ProtocolName; 
            }
            set
            {
                m_ProtocolName = value;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// This protected method is used for Create Service.This is base class method
        /// which will be override by derived classes.
        /// User can create Service of eBay or OSCommerce.
        /// </summary>
        /// <typeparam name="T">Type Param indicate the Class.</typeparam>
        /// <param name="obj">Object of Service Class.</param>
        virtual protected void CreateService<T>(T obj)
        {
 
        }

        /// <summary>
        /// This method is used for Save all transaction into Axis v5.0 database.
        /// This is base class method which will be accessed by derived class.
        /// User can use this method for storing eBay Orders or OSCommerce Orders.
        /// </summary>
        /// <typeparam name="T">Type Parameter of Class.</typeparam>
        /// <param name="coll">Object of Class Collection.</param>
        /// <returns>This method return flag if true then transaction is successfull
        /// Stored in database otherwise there is error</returns>
        virtual protected bool Save<T>(Collection<T> coll)
        {
            return true;
        }

        /// <summary>
        /// This is static protected method which will be accessible only by service classes.
        /// WriteErrorLog maintains all the log details which can contains
        /// Errors,Success Messages,Warnings in a day wise log file.
        ///     User can check trasaction by log. 
        /// </summary>
        /// <param name="message">Pass the Actual message string for log.</param>
        protected static void WriteErrorLog(string message)
        {
            
        }

        #endregion

    }
}
