﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Configuration.Install;
using System.Threading;
using System.Xml;

namespace MessageMonitoringService
{
    public class ServiceClass : System.ServiceProcess.ServiceBase
    {
        static Timer monitorTimer;

        public ServiceClass()
        {

            this.ServiceName = string.Empty;
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;

        }
        
        protected override void OnStart(string[] args)
        {

            try
            {
                TimerCallback timerdelegates = new TimerCallback(DoWork);
                monitorTimer = new Timer(timerdelegates, null, 1000, 2000);

            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();

            }

        }

        private void DoWork(object state)
        {
            try
            {
                string directoryPath = string.Empty;
                string[] prevFiles = Directory.GetFiles(directoryPath);
                if (prevFiles != null)
                {
                    foreach (string file in prevFiles)
                    {
                        FileInfo flinfo = new FileInfo(file);
                        if (flinfo.Extension.ToLower() == ".txt" || flinfo.Extension.ToLower() == ".csv" || flinfo.Extension.ToLower() == ".xls" || flinfo.Extension.ToLower() == ".xlsx")
                        {
                            string csvFile = string.Empty;
                            if (flinfo.Extension.ToLower() == ".xls" || flinfo.Extension.ToLower() == ".xlsx")
                            {
                                string connectionStringFormat = string.Empty;
                                string sheetName = string.Empty;

                                if (flinfo.Extension.ToLower() == ".xls")
                                {
                                    connectionStringFormat = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + file + "; Extended Properties='Excel 8.0; IMEX=1; HDR=1'";
                                }

                                if (flinfo.Extension.ToLower() == ".xlsx")
                                {
                                    connectionStringFormat = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + file + "; Extended Properties='Excel 12.0 Xml; IMEX=1; HDR=1'";
                                }
                                try
                                {
                                    OleDbConnection objConn = new OleDbConnection(connectionStringFormat);

                                    objConn.Open();

                                    DataTable dtSheets = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                                    if (dtSheets != null)
                                    {
                                        sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString().Replace("'", string.Empty).Replace("$", string.Empty);
                                        objConn.Close();
                                        dtSheets = null;
                                    }

                                    OleDbConnection conn = null;
                                    StreamWriter wrtr = null;
                                    OleDbCommand cmdcsv = null;
                                    OleDbDataAdapter da = null;
                                    try
                                    {
                                        conn = new OleDbConnection(connectionStringFormat);
                                        conn.Open();
                                        cmdcsv = new OleDbCommand("SELECT * FROM [" + sheetName + "$]", conn);
                                        cmdcsv.CommandType = CommandType.Text;
                                        csvFile = file.Replace(".xlsx", ".csv").Replace(".xls", ".csv").ToString();
                                        wrtr = new StreamWriter(csvFile);
                                        da = new OleDbDataAdapter(cmdcsv);
                                        DataTable dt = new DataTable();
                                        da.Fill(dt);
                                        for (int z = 0; z < dt.Columns.Count; z++)
                                        {
                                            string colString = "";
                                            colString += "" + dt.Columns[z].ToString().Replace(",", " ") + ",";
                                            if (z == dt.Columns.Count - 1)
                                                colString = colString.Remove(colString.Length - 1, 1);
                                            wrtr.Write(colString);
                                        }
                                        wrtr.Write("\n");
                                        for (int x = 0; x < dt.Rows.Count; x++)
                                        {
                                            string rowString = "";
                                            for (int y = 0; y < dt.Columns.Count; y++)
                                            {
                                                rowString += "" + dt.Rows[x][y].ToString().Replace(",", string.Empty) + ",";
                                                if (y == dt.Columns.Count - 1)
                                                    rowString = rowString.Remove(rowString.Length - 1, 1);
                                            }

                                            wrtr.WriteLine(rowString);
                                        }

                                    }
                                    catch { }
                                    finally
                                    {
                                        if (conn.State == ConnectionState.Open)
                                            conn.Close();
                                        conn.Dispose();
                                        cmdcsv.Dispose();
                                        da.Dispose();
                                        wrtr.Close();
                                        wrtr.Dispose();
                                    }

                                }
                                catch
                                {
                                }
                            }
                            OdbcConnection con = new OdbcConnection();
                            con.ConnectionString = string.Empty;
                           
                            OdbcCommand cmd = new OdbcCommand();
                            cmd.Connection = con;
                            con.Open();
                            FileStream mmsfileStream = null;
                            string Attachname = flinfo.Name;
                            FileInfo csvfl = null;
                            try
                            {
                                if (flinfo.Extension.ToLower() == ".xls" || flinfo.Extension.ToLower() == ".xlsx")
                                {
                                    mmsfileStream = new FileStream(csvFile, FileMode.Open, FileAccess.Read);
                                    csvfl = new FileInfo(csvFile);
                                    Attachname = csvfl.Name;
                                }
                                else
                                    if (flinfo.Extension.ToLower() == ".txt" || flinfo.Extension.ToLower() == ".csv")
                                    {
                                        mmsfileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                                    }
                            }
                            catch (Exception ex)
                            {

                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                                wrtLog.Close();

                            }
                            BinaryReader mmsfileReader = new BinaryReader(mmsfileStream);
                            string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                            OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)mmsfileStream.Length));
                            string serviceText = string.Empty;
                            string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag) VALUES ('" + MessageDate + "','3','" + serviceText + "','" + Attachname + "',0)";
                            cmd.CommandText = commandText;
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();

                            OdbcCommand cmdObj = new OdbcCommand();
                            cmdObj.Connection = con;
                            cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                            object messageID = cmdObj.ExecuteScalar();
                            cmdObj.Dispose();

                            string insertMessageCommand = string.Empty;
                            if (messageID != null)
                                insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'3')";
                            OdbcCommand cmdFile = new OdbcCommand();
                            cmdFile.Connection = con;
                            cmdFile.CommandText = insertMessageCommand;
                            cmdFile.Parameters.Add(paramFile);
                            cmdFile.ExecuteNonQuery();
                            con.Close();
                            mmsfileReader.Close();
                            mmsfileStream.Close();
                            try
                            {
                                flinfo.CopyTo(string.Empty, true);
                                flinfo.Delete();
                                csvfl.CopyTo(string.Empty, true);
                                csvfl.Delete();

                            }
                            catch
                            { }
                            try
                            {
                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                                wrtLog.WriteLine("This file (" + Attachname + ") is copied successfully \n from " + directoryPath + "  to Zed Axis EDI Inbox.");
                                wrtLog.WriteLine("Transaction Date : " + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                                wrtLog.Close();
                            }
                            catch
                            { }
                        }
                        else
                        {

                            FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                            StreamWriter wrtLog = new StreamWriter(fs);
                            wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                            wrtLog.WriteLine("There are no files exists");
                            wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                            wrtLog.WriteLine("\n------------------------------------------------------------\n");
                            wrtLog.Close();

                        }
                    }

                }
                else
                {

                    FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter wrtLog = new StreamWriter(fs);
                    wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                    wrtLog.WriteLine("There are no files exists");
                    wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    wrtLog.WriteLine("\n------------------------------------------------------------\n");
                    wrtLog.Close();

                }
            }
            catch (Exception ex)
            {

                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();

            }
        }

        protected override void OnStop()
        {

        }
        
    }
}
