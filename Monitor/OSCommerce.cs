﻿// ===============================================================================
// 
// OSCommerce.cs
//
// This Service Installer class contains implementation of GetOrders Method of OSCommerce, 
// Connect , Disconnect methods which will be used by
// OSCommerce classes. 
// This class contains implementation of Properties and Methods.Also this class will
// modified while creating service with real time values.
//  OSCommerce Database is similar like MYSQL database.
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Configuration.Install;
using System.Threading;
using System.Xml;
using Tamir.SharpSsh.jsch;
using System.Net.Sockets;
using MySql.Data.MySqlClient;



namespace Monitor
{
    public class OSCommerce : System.ServiceProcess.ServiceBase
    {
        #region Private Members 

        /// <summary>
        /// Private Members for create eBay Service.
        /// </summary>
        static Timer monitorTimer;
        private string connectionString = string.Empty;
        private string tpID = string.Empty;
        private string mailID = string.Empty;
        //private string oscServer = string.Empty;
        //private string userName = string.Empty;
        //private string password = string.Empty;
        //private string databaseName = string.Empty;
        //private int rPort = -1;
        //int port = 22;

        private string dbHostName = string.Empty;
        private string dbPort = string.Empty;
        private string dbUserName = string.Empty;
        private string dbPassword = string.Empty;
        private string dbName = string.Empty;
        private string sshHostName = string.Empty;
        private string sshPortNo = string.Empty;
        private string sshUserName = string.Empty;
        private string sshPassword = string.Empty;
        private string dbPrefix = string.Empty;
       
        Session session;
        JSch jsch = new JSch();

        #endregion
                
        #region Constructor

        /// <summary>
        /// Constructor code for generate Service.
        /// </summary>
        public OSCommerce()
        {
            this.ServiceName = string.Empty;
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        #endregion

        #region Methods and Events

        /// <summary>
        /// This event occured when windows service is running.This event excutes methods which
        /// request to eBay Server for getting Orders and Stored in database.
        /// </summary>
        /// <param name="args">Send Arguments for start.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                TimerCallback timerdelegates = new TimerCallback(DoWork);
                monitorTimer = new Timer(timerdelegates, null, 1000, 2000);

            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
            }
        }

        /// <summary>
        /// This Timer Callback method which execute every interval and
        /// getting updated orders.
        /// </summary>
        /// <param name="state">State of Timer.</param>
        private void DoWork(object state)
        {
            try
            {
                OdbcConnection con = new OdbcConnection();
                con.ConnectionString = connectionString;
                //string tpID = string.Empty;
                OdbcCommand cmd = new OdbcCommand();
                cmd.Connection = con;
                cmd.CommandText = "SELECT LastUpdatedDate FROM trading_partner_schema WHERE TradingPartnerID=" + tpID + "";
                con.Open();
                OdbcDataReader dr = cmd.ExecuteReader();

                string lastUpdatedDate = DateTime.Now.ToString();

                while (dr.Read())
                {
                    lastUpdatedDate = dr[0].ToString();
                }

                dr.Close();
                cmd.Dispose();
                con.Close();

               

               

                GetOrderByTradingPartner(Convert.ToDateTime(lastUpdatedDate));

            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString());
                wrtLog.WriteLine(ex.StackTrace.ToString());
                wrtLog.WriteLine(ex.TargetSite.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
            }

        }

        public bool CheckOrderV2()
        {
           
            try
            {
                
                MySqlConnection conn = new MySqlConnection();
                MySqlCommand cmmd = new MySqlCommand();

                #region Whether SSH

                if (!string.IsNullOrEmpty(sshHostName))
                {
                    int port = 22;

                    try
                    {
                        session = jsch.getSession(sshUserName, sshHostName, port);
                        session.setHost(sshHostName);
                        session.setPassword(sshPassword);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);
                        session.connect();

                        //Set port forwarding on the opened session.
                        session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));
                        if (session.isConnected())
                        {
                            try
                            {
                                //Create a ODBC Connection.
                                conn = new MySqlConnection();
                                cmmd = new MySqlCommand();
                                MySqlDataReader dr;

                                conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                                conn.Open();
                                cmmd = new MySqlCommand();
                                cmmd.Connection = conn;                                

                                cmmd.CommandText = "SHOW TABLES IN " + dbName;
                                dr = cmmd.ExecuteReader();
                                bool flag = true;
                                while (dr.Read())
                                {
                                    if (dr[0].ToString().ToLower() == dbPrefix + "orders")
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else
                                        flag = false;
                                }
                                dr.Close();
                                cmmd.Dispose();
                                conn.Close();
                                return flag;
                            }
                            catch (SocketException ex)
                            {
                                //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message,EDI.Constant.Constants.ProductName , MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                session.disconnect();
                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                                wrtLog.Close();
                                fs.Close();
                                return false;
                            }
                            catch(Exception ex) {
                                session.disconnect();
                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.WriteLine(ex.Message);
                                wrtLog.Close();
                                fs.Close();
                                return false;

                            }
                        }
                        
                    }
                    catch(Exception ex) {
                        session.disconnect();
                        FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                        StreamWriter wrtLog = new StreamWriter(fs);
                        wrtLog.WriteLine(ex.Message);
                        wrtLog.Close();
                        fs.Close();
                        return false;

                    }
                }
                #endregion
                else
                {
                    conn = new MySqlConnection();
                    MySqlDataReader dr;
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    conn.ConnectionString = connectString;
                    cmmd = new MySqlCommand();
                    cmmd.Connection = conn;

                    cmmd.CommandText = "SHOW TABLES IN " + dbName;
                    conn.Open();
                    dr = cmmd.ExecuteReader();
                    bool flag = true;
                    while (dr.Read())
                    {
                        if (dr[0].ToString().ToLower() == dbPrefix + "orders")
                        {
                            flag = true;
                            break;
                        }
                        else
                            flag = false;
                    }
                    dr.Close();
                    cmmd.Dispose();
                    conn.Close();
                    return flag;
                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
                return false;

            }
            return false;
        }

        public bool CheckOrderV3()
        {
            try
            {
                MySqlCommand cmmd = new MySqlCommand();
                MySqlConnection conn = new MySqlConnection();
                MySqlDataReader dr;
                if (!string.IsNullOrEmpty(sshHostName))
                {

                    conn = new MySqlConnection();
                    cmmd = new MySqlCommand();
                    MySqlDataReader reader;
                    //Set port forwarding on the opened session.
                    //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.
                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                            conn.Open();
                            cmmd = new MySql.Data.MySqlClient.MySqlCommand();
                            cmmd.Connection = conn;

                            cmmd.CommandText = "SHOW TABLES IN " + dbName;
                            reader = cmmd.ExecuteReader();
                            bool flag = true;
                            while (reader.Read())
                            {
                                if (reader[0].ToString().ToLower() == dbPrefix + "orders")
                                {
                                    flag = true;
                                    break;
                                }
                                else
                                    flag = false;
                            }
                            reader.Close();
                            cmmd.Dispose();
                            conn.Close();
                            return flag;
                        }
                        catch (SocketException ex)
                        {
                            //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message,EDI.Constant.Constants.ProductName , MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            session.disconnect();
                            FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                            StreamWriter wrtLog = new StreamWriter(fs);
                            wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                            wrtLog.Close();
                            fs.Close();
                            return false;
                        }
                    }
                }

                else
                {
                    conn = new MySqlConnection();
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                    conn.ConnectionString = connectString;
                    cmmd = new MySqlCommand();
                    cmmd.Connection = conn;
                    cmmd.CommandText = "SHOW TABLES IN " + dbName;
                    conn.Open();
                    dr = cmmd.ExecuteReader();
                    bool flag = true;
                    while (dr.Read())
                    {
                        if (dr[0].ToString().ToLower() == dbPrefix + "orders")
                        {
                            flag = true;
                        }
                        else
                            flag = false;
                    }
                    dr.Close();
                    cmmd.Dispose();
                    conn.Close();
                    return flag;
                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
                return false;
            }
            return false;
        }

        public string[] GetShippingDetails(int version, string orderID)
        {
            string[] shipDetails = new string[2];
            if (!string.IsNullOrEmpty(sshHostName))
            {                
                try
                {
                    
                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.
                            MySqlConnection conn = new MySqlConnection();
                            MySqlCommand cmmd = new MySqlCommand();
                            MySqlDataAdapter da;

                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            conn.Open();
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;

                            string Query = string.Empty;
                            //if (version == 2)
                            //{
                                Query = "SELECT title,value FROM "+ dbPrefix.Trim() +"orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                            //}
                            //else
                            //{
                               // Query = "SELECT title,value FROM " + dbPrefix.Trim() + "osc_orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                            //}

                            cmmd.CommandText = Query;
                            da = new MySqlDataAdapter(cmmd);
                            DataSet orderDataSet = new DataSet();
                            da.Fill(orderDataSet);
                            if (orderDataSet == null)
                            {
                                return null;
                            }
                            else
                                if (orderDataSet.Tables[0].Rows.Count == 0)
                                {
                                    return null;
                                }

                            shipDetails[0] = orderDataSet.Tables[0].Rows[0][0].ToString();
                            shipDetails[1] = orderDataSet.Tables[0].Rows[0][1].ToString();
                            cmmd.Dispose();
                            conn.Close();

                        }
                        catch (SocketException ex)
                        {
                            //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            session.disconnect();
                            FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                            StreamWriter wrtLog = new StreamWriter(fs);
                            wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                            wrtLog.Close();
                            fs.Close();
                        }
                        catch { }
                    }
                }
                catch { }
            }
            else
            {

                MySqlConnection con = new MySqlConnection();
                string connectString = string.Empty;
                if (dbPort == string.Empty || dbPort == "3306")
                    connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                else
                    connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                con.ConnectionString = connectString;

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                string Query = string.Empty;
                //if (version == 2)
                //{
                    Query = "SELECT title,value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                //}
                //else
                //{
                //    Query = "SELECT title,value FROM " + dbPrefix.Trim() + "osc_orders_total WHERE orders_id =" + orderID + " AND class='ot_shipping'";
                //}

                cmd.CommandText = Query;
                con.Open();
                MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
                DataSet orderDataSet = new DataSet();
                orderAdapter.Fill(orderDataSet);
                cmd.Dispose();
                con.Close();

                if (orderDataSet == null)
                {
                    return null;
                }
                else
                    if (orderDataSet.Tables[0].Rows.Count == 0)
                    {
                        return null;
                    }

                shipDetails[0] = orderDataSet.Tables[0].Rows[0][0].ToString();
                shipDetails[1] = orderDataSet.Tables[0].Rows[0][1].ToString();
            }

            return shipDetails;
        }

        public string GetTotal(int version, string orderID)
        {
            string shipDetails = string.Empty;

            if (!string.IsNullOrEmpty(sshHostName))
            {
               
                try
                {

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a MySqlConnection Connection.
                            MySqlConnection conn = new MySqlConnection();
                            MySqlCommand cmmd = new MySqlCommand();
                            MySqlDataAdapter da;

                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            conn.Open();
                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;

                            string Query = string.Empty;
                            //if (version == 2)
                            //{
                                Query = "SELECT value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";
                            //}
                            //else
                            //{
                                //Query = "SELECT value FROM " + dbPrefix.Trim() + "osc_orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";
                            //}


                            cmmd.CommandText = Query;
                            da = new MySqlDataAdapter(cmmd);
                            DataSet orderDataSet = new DataSet();
                            da.Fill(orderDataSet);
                            if (orderDataSet == null)
                            {
                                return null;
                            }
                            else
                                if (orderDataSet.Tables[0].Rows.Count == 0)
                                {
                                    return null;
                                }

                            shipDetails = orderDataSet.Tables[0].Rows[0][0].ToString();
                            cmmd.Dispose();
                            conn.Close();
                            //session.disconnect();
                        }
                        catch (SocketException ex)
                        {
                            //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            session.disconnect();
                            FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                            StreamWriter wrtLog = new StreamWriter(fs);
                            wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                            wrtLog.Close();
                            fs.Close();
                        }
                        catch { }
                    }
                }
                catch { }
            }
            else
            {
                MySqlConnection con = new MySqlConnection();
                string connectString = string.Empty;
                if (dbPort == string.Empty || dbPort == "3306")
                    connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                else
                    connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                con.ConnectionString = connectString;
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                string Query = string.Empty;
                //if (version == 2)
                //{
                    Query = "SELECT value FROM " + dbPrefix.Trim() + "orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";
                //}
                //else
                //{
                //    Query = "SELECT value FROM " + dbPrefix.Trim() + "osc_orders_total WHERE orders_id =" + orderID + " AND class='ot_total'";
                //}

                cmd.CommandText = Query;
                con.Open();
                MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmd);
                DataSet orderDataSet = new DataSet();
                orderAdapter.Fill(orderDataSet);
                cmd.Dispose();
                con.Close();

                if (orderDataSet == null)
                {
                    return string.Empty;
                }
                else
                    if (orderDataSet.Tables[0].Rows.Count == 0)
                    {
                        return string.Empty;
                    }

                shipDetails = orderDataSet.Tables[0].Rows[0][0].ToString();
            }
            return shipDetails;
        }

        /// <summary>
        /// This method is used for Getting Orders by single Trading partner 
        /// Using TP's authentication details.
        /// </summary>
        /// <param name="updatedDate">Last updated Date</param>
        /// <returns>Return Order Details into String Builder.</returns>
        private void GetOrderByTradingPartner(DateTime updatedDate)
        {
            #region Checking Which Version is used
            OdbcCommand cmd = new OdbcCommand();
            OdbcConnection con = new OdbcConnection();
            MySqlConnection conn = new MySqlConnection();
            MySqlCommand cmmd = new MySqlCommand();
            DataSet orderDataSets = new DataSet();
            DataSet ds = new DataSet();
          
            int version = 2;
            if (CheckOrderV2())
                version = 2;
            //else
            //    if (CheckOrderV3())
            //        version = 3;
             else
                 return;

            #endregion

            StringBuilder sb = new StringBuilder();

            string tradingPartnerName = string.Empty;
           
            string orderQuery = string.Empty;
            //if (version == 2)
            //{
            orderQuery = "SELECT orders_id from " + dbPrefix.Trim() + "orders WHERE last_modified >= '" + updatedDate.ToString("yyyy-MM-dd HH:mm:ss") + "' AND last_modified <= '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            //}
            //else
            //{
            //    orderQuery = "SELECT orders_id from " + dbPrefix.Trim() + "osc_orders WHERE date_purchased >= '" + updatedDate.ToString("yyyy-MM-dd HH:mm:ss") + "' AND date_purchased <= '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            //}
           
            try
            {

                if (!string.IsNullOrEmpty(sshHostName))
                {                    
                    conn = new MySqlConnection();
                    cmmd = new MySqlCommand();
                    //Set port forwarding on the opened session.
                    //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));

                    if (session.isConnected())
                    {
                        try
                        {
                            //Create a ODBC Connection.
                            conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                            cmmd = new MySqlCommand();
                            cmmd.Connection = conn;
                            cmmd.CommandText = orderQuery;
                            conn.Open();

                            MySqlDataAdapter ordAdapter = new MySqlDataAdapter(cmmd);
                            orderDataSets = new DataSet();
                            ordAdapter.Fill(orderDataSets);
                            cmmd.Dispose();
                            conn.Close();

                        }
                        catch (SocketException ex)
                        {
                            //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            session.disconnect();
                            FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                            StreamWriter wrtLog = new StreamWriter(fs);
                            wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                            wrtLog.Close();
                            fs.Close();
                            return;
                        }
                        catch { }
                    }
                }
                else
                {

                        conn = new MySqlConnection();

                        string connectString = string.Empty;
                        if (dbPort == string.Empty || dbPort == "3306")
                            connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                        else
                            connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                        conn.ConnectionString = connectString;
                        cmmd = new MySqlCommand();
                        cmmd.Connection = conn;
                        cmmd.CommandText = orderQuery;
                        conn.Open();

                        MySqlDataAdapter orderAdapter = new MySqlDataAdapter(cmmd);
                        orderDataSets = new DataSet();
                        orderAdapter.Fill(orderDataSets);
                        cmmd.Dispose();
                        conn.Close();                 
                }


                if (orderDataSets == null)
                {
                    return;
                }
                else
                {
                    if (orderDataSets.Tables[0].Rows.Count == 0)
                    {
                        return;
                    }
                }

                DataView orderDataSet = orderDataSets.Tables[0].DefaultView;
                orderDataSet.Sort = "orders_id";

                for (int count = 0; count < orderDataSet.Table.Rows.Count; count++)
                {

                    #region Process Order

                    string Query = string.Empty;
                    //if (version == 2)
                    //{
                          //Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "orders ordr LEFT OUTER join " + dbPrefix.Trim() + "orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.customer_notified= (SELECT MAX(customer_notified) FROM " + dbPrefix.Trim() + "orders_status_history where orders_status_id = ( select max(orders_status_id) from " + dbPrefix.Trim() + "orders_status_history where orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + ")) AND ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";
                          Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.final_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "orders ordr LEFT OUTER join " + dbPrefix.Trim() + "orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "orders_status_history osh on ordr.orders_id = osh.orders_id  WHERE pd.language_id=1 and osh.orders_status_history_id = (SELECT MAX(orders_status_history_id) FROM " + dbPrefix.Trim() + "orders_status_history where orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + ") AND ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";  
                    //}
                    //else
                    //{
                    //    Query = "SELECT ordr.orders_id,ordr.delivery_name,ordr.delivery_street_address,ordr.delivery_suburb,ordr.delivery_city,ordr.delivery_postcode,ordr.delivery_state,ordr.delivery_country,ordr.customers_telephone,ordr.payment_method,ordr.date_purchased,ordr.orders_status,ordr.orders_date_finished,osh.comments,pd.products_name,op.products_price,op.products_tax,op.products_quantity,op.products_model FROM " + dbPrefix.Trim() + "osc_orders ordr LEFT OUTER join " + dbPrefix.Trim() + "osc_orders_products op on ordr.orders_id = op.orders_id LEFT OUTER join " + dbPrefix.Trim() + "osc_products_description pd on op.products_id = pd.products_id LEFT OUTER join " + dbPrefix.Trim() + "osc_orders_total ot on ordr.orders_id = ot.orders_id LEFT OUTER join " + dbPrefix.Trim() + "osc_orders_status_history osh on ordr.orders_id = osh.orders_id WHERE pd.language_id= 1 and and osh.orders_status_id = 1 and ordr.orders_id = " + orderDataSet.Table.Rows[count][0].ToString() + "";
                    //}
                    try
                    {
                        if (!string.IsNullOrEmpty(sshHostName))
                        {

                            conn = new MySqlConnection();
                            cmmd = new MySqlCommand();

                            //Set port forwarding on the opened session.
                            //session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));

                            if (session.isConnected())
                            {
                                try
                                {
                                    //Create a ODBC Connection.

                                    conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                                    cmmd = new MySqlCommand();
                                    cmmd.Connection = conn;
                                    cmmd.CommandText = Query;
                                    conn.Open();
                                    
                                    //wrtLog.WriteLine("In the 2nd half.");
                                 

                                    MySqlDataAdapter dataAdapter = new MySqlDataAdapter(cmmd);
                                    ds = new DataSet();
                                    dataAdapter.Fill(ds);
                                    cmmd.Dispose();
                                    conn.Close();
                                    //session.disconnect();
                                }
                                catch (SocketException ex)
                                {
                                    //MessageBox.Show("Connection timeout for the OSCommerce.Please try again." + "\n" + ex.Message, ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    session.disconnect();
                                    FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                    StreamWriter wrtLog = new StreamWriter(fs);
                                    wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                                    wrtLog.Close();
                                    fs.Close();
                                    return;
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            MySqlConnection orderCon = new MySqlConnection();
                            string connectString = string.Empty;
                            if (dbPort == string.Empty || dbPort == "3306")
                                connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                            else
                                connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";

                            orderCon.ConnectionString = connectString;
                            MySqlCommand orderCmd = new MySqlCommand();
                            orderCmd.Connection = orderCon;
                            orderCmd.CommandText = Query;
                            orderCon.Open();

                            MySqlDataAdapter dtAdapter = new MySqlDataAdapter(orderCmd);
                            ds = new DataSet();
                            dtAdapter.Fill(ds);
                            orderCmd.Dispose();
                            orderCon.Close();
                        }
                        if (ds == null)
                        {
                            continue;
                        }
                        else
                        {
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                continue;
                            }
                            else
                            {
                                #region Store OSCommerce Order

                                string headerLine = string.Empty;
                                string[] middleLine = new string[ds.Tables[0].Rows.Count];
                                for (int temp = 0; temp < ds.Tables[0].Rows.Count; temp++)
                                {
                                    string[] shipInfo = new string[2];
                                    if (ds.Tables[0].Rows[temp][0].ToString() != string.Empty)
                                    {
                                        shipInfo = GetShippingDetails(version, ds.Tables[0].Rows[temp][0].ToString());
                                        string total = GetTotal(version, ds.Tables[0].Rows[temp][0].ToString());

                                        if (temp == ds.Tables[0].Rows.Count - 1)
                                        {
                                            #region Insert data into database

                                            //if (version == 2)
                                            //{
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}
                                            //else
                                            //{
                                             //   middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                             //   middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}


                                            headerLine += "OSCommerce|" + tradingPartnerName + "|" + ds.Tables[0].Rows[temp][0].ToString() + "\n";
                                            headerLine += "OSCommerce1|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][1].ToString() + "|" + ds.Tables[0].Rows[temp][2].ToString() + "|" + ds.Tables[0].Rows[temp][3].ToString() + "|" + ds.Tables[0].Rows[temp][4].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][5].ToString() + "|" + ds.Tables[0].Rows[temp][6].ToString() + "|" + ds.Tables[0].Rows[temp][7].ToString() + "|";
                                            headerLine += "" + ds.Tables[0].Rows[temp][8].ToString() + "|" + ds.Tables[0].Rows[temp][9].ToString() + "|" + ds.Tables[0].Rows[temp][10].ToString() + "|";
                                            if (shipInfo != null)
                                            {
                                                if (shipInfo[0] == null && shipInfo[1] == null)
                                                    headerLine += "" + string.Empty + "|" + string.Empty + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                                else
                                                    headerLine += "" + (shipInfo == null ? string.Empty : shipInfo[1].ToString()) + "|" + (shipInfo == null ? string.Empty : shipInfo[0].ToString()) + "|" + ds.Tables[0].Rows[temp][11].ToString() + "|";
                                            }
                                            headerLine += "" + ds.Tables[0].Rows[temp][12].ToString() + "|" + ds.Tables[0].Rows[temp][13].ToString() + "\n";

                                            sb.AppendLine(headerLine);


                                            foreach (string item in middleLine)
                                            {
                                                if (item != null)
                                                {
                                                    if (item != string.Empty)
                                                    {
                                                        sb.AppendLine(item);
                                                    }
                                                }
                                            }

                                            SaveOrder(sb, ds.Tables[0].Rows[temp][0].ToString(), tradingPartnerName);
                                            headerLine = string.Empty;
                                            sb = new StringBuilder();

                                            middleLine = new string[ds.Tables[0].Rows.Count];
                                            #endregion
                                        }
                                        else
                                        {
                                            //if (version == 2)
                                            //{
                                                middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                                middleLine[temp] += "" + ds.Tables[0].Rows[temp][16].ToString() + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}
                                            //else
                                            //{
                                            //    middleLine[temp] += "OSCommerce2|" + ds.Tables[0].Rows[temp][0].ToString() + "|" + ds.Tables[0].Rows[temp][19].ToString() + "|" + ds.Tables[0].Rows[temp][14].ToString() + "|" + ds.Tables[0].Rows[temp][15].ToString() + "|";
                                            //    middleLine[temp] += "" + string.Empty + "|" + ds.Tables[0].Rows[temp][17].ToString() + "|" + ds.Tables[0].Rows[temp][18].ToString() + "|" + total + "\n";
                                            //}

                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                        ds = null;
                    }
                    catch (Exception ex)
                    {
                        FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                        StreamWriter wrtLog = new StreamWriter(fs);
                        wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                        wrtLog.WriteLine(ex.Message.ToString());
                        wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                        wrtLog.WriteLine("\n------------------------------------------------------------\n");
                        wrtLog.Close();
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
            }
            finally
            {
                if (session != null)
                {
                    session.disconnect();
                }
            }
        }

        /// <summary>
        /// This method is used for Getting Order into Proper format
        /// and Store that order into Axis v5.0 database.
        /// </summary>
        /// <param name="respData">Object of String Buider which will be stored in database.</param>
        private void SaveOrder(StringBuilder sb, string orderId, string tradingPartnerName)
        {

            OdbcConnection con = new OdbcConnection();
            con.ConnectionString = connectionString;

            OdbcCommand cmd = new OdbcCommand();
            cmd.Connection = con;
            con.Open();

            MemoryStream stream = null;
            string Attachname = "eBay  " + tradingPartnerName + " " + orderId;

            try
            {
                string myString = sb.ToString();
                byte[] myByteArray = System.Text.Encoding.ASCII.GetBytes(myString);
                stream = new MemoryStream(myByteArray);

                BinaryReader mmsfileReader = new BinaryReader((Stream)stream);
                string MessageDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                OdbcParameter paramFile = new OdbcParameter("parameterName", mmsfileReader.ReadBytes((int)stream.Length));
                string serviceText = string.Empty;
                string commandText = "INSERT INTO message_schema (MessageDate,Status,FromAddress,Subject,ReadFlag,TradingPartnerID,MailProtocolId) VALUES ('" + MessageDate + "','10','" + tradingPartnerName + "','Order " + orderId + "',0," + tpID + "," + mailID + ")";
                cmd.CommandText = commandText;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();

                con.Open();
                OdbcCommand cmdObj = new OdbcCommand();
                cmdObj.Connection = con;
                cmdObj.CommandText = "SELECT MAX(MessageID) FROM message_schema";
                object messageID = cmdObj.ExecuteScalar();
                cmdObj.Dispose();
                con.Close();
                string insertMessageCommand = string.Empty;
                if (messageID != null)
                    insertMessageCommand = "INSERT INTO mail_attachment (MessageID,AttachName,AttachFile,Status) VALUES (" + messageID.ToString() + ",'" + Attachname + "',?,'10')";
                OdbcCommand cmdFile = new OdbcCommand();
                con.Open();
                cmdFile.Connection = con;
                cmdFile.CommandText = insertMessageCommand;
                cmdFile.Parameters.Add(paramFile);
                cmdFile.ExecuteNonQuery();
                con.Close();
                mmsfileReader.Close();
                stream.Close();

                string commandDateText = " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tpID + "";
                OdbcCommand commandDate = new OdbcCommand();
                con.Open();
                commandDate.Connection = con;
                commandDate.CommandText = commandDateText;
                commandDate.ExecuteNonQuery();
                commandDate.Dispose();
                con.Close();
                UpdatePendingStatus(orderId);

            }
            catch (Exception ex)
            {

                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();

            }

        }
        public bool UpdatePendingStatus(string orderID)
        {
            try
            {
                MySqlConnection conn = new MySqlConnection();
                MySqlCommand cmmd = new MySqlCommand();
                #region Whether SSH
                if (!string.IsNullOrEmpty(sshHostName))
                {
                    int port = 22;
                    try
                    {
                        session = jsch.getSession(sshUserName, sshHostName, port);
                        session.setHost(sshHostName);
                        session.setPassword(sshPassword);
                        UserInfo ui = new MyUserInfo();
                        session.setUserInfo(ui);
                        session.connect();
                        session.setPortForwardingL(Convert.ToInt32(dbPort), dbHostName, Convert.ToInt32(sshPortNo));
                        if (session.isConnected())
                        {
                            try
                            {
                                conn = new MySqlConnection();
                                cmmd = new MySqlCommand();
                                MySqlDataReader dr;
                                conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                                conn.Open();
                                cmmd = new MySqlCommand();
                                cmmd.Connection = conn;
                                cmmd.CommandText = " SELECT order_status_id FROM " + dbPrefix.Trim() + "order_status WHERE order_status_name = 'Processing'";
                                dr = cmmd.ExecuteReader();
                                int statusID = 0;
                                while (dr.Read())
                                {
                                    statusID = Convert.ToInt32(dr[0].ToString());
                                }
                                dr.Close();
                                cmmd.Dispose();
                                conn.Close();
                                conn = new MySqlConnection();
                                cmmd = new MySqlCommand();
                                conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                                conn.Open();
                                cmmd = new MySqlCommand();
                                cmmd.Connection = conn;
                                cmmd.CommandText = "UPDATE " + dbPrefix.Trim() + "orders SET orders_status = " + statusID + " WHERE orders_id = " + orderID;
                                cmmd.ExecuteNonQuery();
                                cmmd.Dispose();
                                conn.Close();
                                return true;
                            }
                            catch (SocketException ex)
                            {
                                session.disconnect();
                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.WriteLine(ex.Message + "\n Socket Exception.Please try again.");
                                wrtLog.Close();
                                fs.Close();
                                return false;
                            }
                            catch (Exception ex)
                            {
                                session.disconnect();
                                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                                StreamWriter wrtLog = new StreamWriter(fs);
                                wrtLog.WriteLine(ex.Message);
                                wrtLog.Close();
                                fs.Close();
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        session.disconnect();
                        FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                        StreamWriter wrtLog = new StreamWriter(fs);
                        wrtLog.WriteLine(ex.Message);
                        wrtLog.Close();
                        fs.Close();
                        return false;
                    }
                }
                #endregion
                else
                {
                    conn = new MySqlConnection();
                    MySqlDataReader dr;
                    string connectString = string.Empty;
                    if (dbPort == string.Empty || dbPort == "3306")
                        connectString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    else
                        connectString = "Server=" + dbHostName + ";Port=" + dbPort + "User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    conn.ConnectionString = connectString;
                    cmmd = new MySqlCommand();
                    cmmd.Connection = conn;
                    conn.Open();
                    dr = cmmd.ExecuteReader();
                    cmmd.CommandText = "SELECT order_status_id FROM " + dbPrefix.Trim() + "order_status WHERE order_status_name = 'Processing'";
                    dr = cmmd.ExecuteReader();
                    int statusID = 0;
                    while (dr.Read())
                    {
                        statusID = Convert.ToInt32(dr[0].ToString());
                    }
                    dr.Close();
                    cmmd.Dispose();
                    conn.Close();
                    conn = new MySqlConnection();
                    cmmd = new MySqlCommand();
                    conn.ConnectionString = "Server=" + dbHostName + ";User id=" + dbUserName + ";Pwd=" + dbPassword + ";Database=" + dbName + ";Connection Timeout = 30;";
                    conn.Open();
                    cmmd = new MySqlCommand();
                    cmmd.Connection = conn;
                    cmmd.CommandText = "UPDATE " + dbPrefix.Trim() + "orders SET orders_status = " + statusID + " WHERE orders_id = " + orderID;
                    cmmd.ExecuteNonQuery();
                    cmmd.Dispose();
                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter wrtLog = new StreamWriter(fs);
                wrtLog.BaseStream.Seek(0, SeekOrigin.End);
                wrtLog.WriteLine(ex.Message.ToString() + "\n\n" + ex.StackTrace.ToString());
                wrtLog.WriteLine("Log Date :" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                wrtLog.WriteLine("\n------------------------------------------------------------\n");
                wrtLog.Close();
                return false;
            }
            return false;
        }

        public class MyUserInfo : UserInfo
        {
            /// &lt;summary&gt;
            /// Holds the user password
            /// &lt;/summary&gt;
            private String passwd;

            /// &lt;summary&gt;
            /// Returns the user password
            /// &lt;/summary&gt;
            public String getPassword() { return passwd; }

            /// &lt;summary&gt;
            /// Prompt the user for a Yes/No input
            /// &lt;/summary&gt;
            public bool promptYesNo(String str)
            {
                return true;
            }

            /// &lt;summary&gt;
            /// Returns the user passphrase (passwd for the private key file)
            /// &lt;/summary&gt;
            public String getPassphrase() { return null; }

            /// &lt;summary&gt;
            /// Prompt the user for a passphrase (passwd for the private key file)
            /// &lt;/summary&gt;
            public bool promptPassphrase(String message) { return true; }

            /// &lt;summary&gt;
            /// Prompt the user for a password
            /// &lt;/summary&gt;
            public bool promptPassword(String message) { return true; }

            /// &lt;summary&gt;
            /// Shows a message to the user
            /// &lt;/summary&gt;
            public void showMessage(String message) { }

        }

        #endregion

    }
}
