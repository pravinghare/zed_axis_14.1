﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EDI.Constant;
using System.IO;
using System.Reflection;
using System.Xml;

namespace DataProcessingBlocks
{
    public partial class ColumnChooser : Form
    {
        //TransactionImporter.TransactionImporter trnsact = new TransactionImporter.TransactionImporter();

        //ColumnChooser colch = new ColumnChooser();
        private string m_columnName;

        public string columnList
        {
            get { return this.m_columnName; }
            set { this.m_columnName = value; }
        }

        private static ColumnChooser m_Mappings;
        public static ColumnChooser GetInstance()
        {
            if (m_Mappings == null)
                m_Mappings = new ColumnChooser();
            return m_Mappings;
        }

        public ColumnChooser()
        {
            InitializeComponent();
        }
        TransactionImporter.TransactionImporter trnsact = null;

        //    private Form1 mainForm = null;
        public ColumnChooser(Form callingForm)
        {
            trnsact = callingForm as TransactionImporter.TransactionImporter;
            InitializeComponent();
        }

        private void ColumnChooser_Load(object sender, EventArgs e)
        {
            List<string> lists = TemplateList();

            foreach (string item in lists)
            {
                cmbEditTemplate.Items.Add(item);
            }
            foreach (string item in lists)
            {
                cmbDelete.Items.Add(item);
            }
        }

        //607
        public List<string> TemplateList()
        {
            System.Xml.XmlDocument xdoc = new XmlDocument();
            List<string> coll = new List<string>();
            List<string> TempList = new List<string>();
            List<string> TempName = new List<string>();
            string filePath = CommonUtilities.GetInstance().getSettingFilePath();
            try
            {
                xdoc.Load(filePath);
                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            bool isTypeMatch = false;
                            bool isConnectionTypeMatch = false;
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {
                                if (childNodes.Name == "ColumnChooserDataType")
                                {
                                    if (trnsact.comboBoxDatatype.SelectedItem.ToString() == childNodes.InnerText)
                                    {
                                        isTypeMatch = true;
                                    }
                                }
                                if (childNodes.Name == "ConnectionType" && childNodes.InnerText == CommonUtilities.GetInstance().ConnectedSoftware)
                                {
                                    isConnectionTypeMatch = true;
                                }
                                if (childNodes.Name == "ColumnChooserName" && isTypeMatch == true && isConnectionTypeMatch == true)
                                {
                                    TempList.Add(childNodes.InnerText);
                                    isTypeMatch = false;
                                    isConnectionTypeMatch = false;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return TempList;
        }

        //607
        public static List<string> ColumnList()
        {
            System.Xml.XmlDocument xdoc = new XmlDocument();


            UserMappingsCollection coll = new UserMappingsCollection();
            List<string> columnList = new List<string>();

            List<string> TempName = new List<string>();

            string filePath = CommonUtilities.GetInstance().getSettingFilePath();
           

                    xdoc.Load(filePath);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;


                            foreach (XmlNode node in userMappingsXML)
                            {

                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "ColumnName")
                                        columnList.Add(childNodes.InnerText);
                                }

                            }
                            break;
                        }
                    }
            return columnList;
        }



        private void btnColChooser_Click(object sender, EventArgs e)
        {
            List<string> lst = TemplateList();
            if (lst.Contains(textBoxColChooser.Text))
            {
                MessageBox.Show(textBoxColChooser.Text + " Template Name already exist");
                textBoxColChooser.Clear();
                return;
            }
            if (textBoxColChooser.Text == string.Empty)
            {
                MessageBox.Show("Please Add Template Name");
                return;
            }
            if (textBoxColChooser.Text != string.Empty)
            {
                //string ColChooserName = textBoxColChooser.Text;
                List<string> ChoosedColumns = new List<string>();
                //int i = 0;

                foreach (Control item in this.panelColChooser.Controls)
                {
                    if (item is CheckBox)
                    {
                        CheckBox tempCheckBox = item as CheckBox;
                        if (tempCheckBox.Checked == true)
                        {
                            ChoosedColumns.Add(tempCheckBox.Text.TrimStart().TrimEnd().Trim());

                        }
                    }
                }

                if (ChoosedColumns.Count == 0)
                {
                    MessageBox.Show("Please select columns");
                    return;
                }
                string filePath = CommonUtilities.GetInstance().getSettingFilePath();
                try
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(filePath);
                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                        {
                            XmlNode mappToAdd = root.ChildNodes.Item(i).AppendChild(xdoc.CreateElement("TemplateName"));
                            mappToAdd.AppendChild(xdoc.CreateElement("ColumnChooserDataType")).InnerText = trnsact.comboBoxDatatype.SelectedItem.ToString();
                            mappToAdd.AppendChild(xdoc.CreateElement("ConnectionType")).InnerText = CommonUtilities.GetInstance().ConnectedSoftware;
                            mappToAdd.AppendChild(xdoc.CreateElement("ColumnChooserName")).InnerText = textBoxColChooser.Text;
                            for (int j = 0; j < ChoosedColumns.Count; j++)
                            {
                                mappToAdd.AppendChild(xdoc.CreateElement("ColumnName")).InnerText = ChoosedColumns[j].ToString();
                            }
                            xdoc.Save(filePath);
                            MessageBox.Show("Template Saved Successfully.");
                            break;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            trnsact.comboBoxColChooser.Items.Add(textBoxColChooser.Text);
            trnsact.comboBoxColChooser.SelectedIndex = trnsact.comboBoxColChooser.Items.Count - 1;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbEditTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (Control c in panelColChooser.Controls)
            {
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
            }
            List<string> Temp = TemplateList();
            List<string> colmList = ColumnList();
            List<string> colllist = DataProcessingBlocks.ColumnChooser.ColumnList();
            List<string> TempltList = TemplateList();
            List<string> Coldata = new List<string>();
            List<string> Innercoll = new List<string>();

            groupBoxColChooser.Enabled = true;
            panelColChooser.Enabled = true;
            btnColChooser.Enabled = false;
            var collist = trnsact.dataGridViewDataPreview.Columns.ToList();
            for (int i = 0; i < panelColChooser.Controls.Count; i++)
            {
                if (panelColChooser.Controls[i].Text.Equals("Select All"))
                {
                    panelColChooser.Controls.Remove(panelColChooser.Controls[i]);
                }
            }
            for (int i = 0; i < Temp.Count; i++)
            {
                if (cmbEditTemplate.SelectedItem.ToString().Equals(Temp[i].ToString()))
                {
                    textBoxColChooser.Text = Temp[i].ToString();
                }
            }
            foreach (string lst in TempltList)
            {
                if (lst == cmbEditTemplate.SelectedItem.ToString())
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();


                    List<string> TempList = new List<string>();

                    List<string> TempName = new List<string>();


                    try
                    {
                        string filePath = CommonUtilities.GetInstance().getSettingFilePath();


                        xdoc.Load(filePath);
                        XmlNode root = (XmlNode)xdoc.DocumentElement;
                        for (int i = 0; i < root.ChildNodes.Count; i++)
                        {
                            if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                            {
                                XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                                foreach (XmlNode node in userMappingsXML)
                                {
                                    foreach (XmlNode childNodes in node.ChildNodes)
                                    {
                                        if (childNodes.Name == "ColumnChooserName")
                                        {
                                            if (childNodes.InnerText == cmbEditTemplate.SelectedItem.ToString())
                                            {
                                                foreach (XmlNode colchildNodes in node.ChildNodes)
                                                {
                                                    if (colchildNodes.Name == "ColumnName")
                                                        Innercoll.Add(colchildNodes.InnerText);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }


            List<string> difference = Innercoll.Except(Coldata).ToList();

            if (difference.Contains("Select All"))
            {
                difference.Remove("Select All");
            }
            for (int i = 0; i < Innercoll.Count; i++)
            {
                foreach (Control c in panelColChooser.Controls)
                {
                    if (c is CheckBox)
                    {
                        if (c.Text.ToString() == Innercoll[i].ToString())
                        {
                            ((CheckBox)c).Checked = true;
                        }
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            List<string> Innerlst = new List<string>();
            List<string> Temp = TemplateList();
            List<string> colList = ColumnList();
            List<string> lst = TemplateList();
            if (textBoxColChooser.Text == string.Empty)
            {
                MessageBox.Show("Please Select Template Name");
                return;
            }
            if (textBoxColChooser.Text != string.Empty)
            {
                List<string> ChoosedColumns = new List<string>();
                foreach (Control item in this.panelColChooser.Controls)
                {
                    if (item is CheckBox)
                    {
                        CheckBox tempCheckBox = item as CheckBox;
                        if (tempCheckBox.Checked == true)
                        {
                            ChoosedColumns.Add(tempCheckBox.Text.TrimStart().TrimEnd().Trim());
                        }
                    }
                }
                if (ChoosedColumns.Count == 0)
                {
                    MessageBox.Show("please select columns");
                    return;
                }
                System.Xml.XmlDocument xdoc = new XmlDocument();
                string filePath = CommonUtilities.GetInstance().getSettingFilePath();
                xdoc.Load(filePath);
                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            //foreach (XmlNode childNodes in node.ChildNodes)
                            //{
                                 if (node.ChildNodes.Item(0).Name == "ColumnChooserDataType" && node.ChildNodes.Item(0).InnerText == trnsact.comboBoxDatatype.SelectedItem.ToString())
                                    {
                                        if (node.ChildNodes.Item(1).Name == "ConnectionType" && node.ChildNodes.Item( 1).InnerText == CommonUtilities.GetInstance().ConnectedSoftware)
                                        {
                                            if (node.ChildNodes.Item( 2).Name == "ColumnChooserName" && node.ChildNodes.Item(2).InnerText == cmbEditTemplate.SelectedItem.ToString())
                                            {
                                         while (true)
                                        {
                                            bool removed = false;
                                            foreach (XmlNode colchildNodes in node.ChildNodes)
                                            {
                                                if (colchildNodes.Name == "ColumnName")
                                                {
                                                    node.RemoveChild(colchildNodes);
                                                    removed = true;
                                                    break;
                                                }
                                            }
                                            if (!removed)
                                                break;
                                        }
                                            }

                                        }
                                     for (int j = 0; j < ChoosedColumns.Count; j++)
                                        {
                                            node.AppendChild(xdoc.CreateElement("ColumnName")).InnerText = ChoosedColumns[j].ToString();
                                        }
                                 
                                 }
                                //if (childNodes.Name == "ColumnChooserDataType" &&)
                                //{
                                //    if (childNodes.InnerText == cmbEditTemplate.SelectedItem.ToString())
                                //    {
                                //        childNodes.InnerText = textBoxColChooser.Text;
                                //        while (true)
                                //        {
                                //            bool removed = false;
                                //            foreach (XmlNode colchildNodes in node.ChildNodes)
                                //            {
                                //                if (colchildNodes.Name == "ColumnName")
                                //                {
                                //                    node.RemoveChild(colchildNodes);
                                //                    removed = true;
                                //                    break;
                                //                }
                                //            }
                                //            if (!removed)
                                //                break;
                                //        }
                                //        //node.AppendChild(xdoc.CreateElement("ColumnChooserDataType")).InnerText = trnsact.comboBoxDatatype.SelectedItem.ToString();
                                //        //node.AppendChild(xdoc.CreateElement("ConnectionType")).InnerText = CommonUtilities.GetInstance().ConnectedSoftware;
                          
                                        
                                //    }
                                //}
                                //break;
                            //}
                        }
                        xdoc.Save(filePath);
                        MessageBox.Show("Template Updated Successfully.");
                        break;
                    }
                }
            }
            groupBoxColChooser.Enabled = false;
            trnsact.comboBoxColChooser.Items.Add(textBoxColChooser.Text);
            trnsact.comboBoxColChooser.SelectedIndex = trnsact.comboBoxColChooser.Items.Count - 1;
            this.Close();
        }

        private void cmbDelete_SelectedIndexChanged(object sender, EventArgs e)
        {
            //groupBoxColChooser.Visible = tr;
            panelColChooser.Visible = false;
            btnColChooser.Enabled = false;

            var confirmResult = MessageBox.Show("Are you sure you want to delete this Template..?",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                List<string> Innerlst = new List<string>();
                List<string> Temp = TemplateList();
                List<string> colList = ColumnList();
                List<string> lst = TemplateList();
                try
                {
                    string filePath = CommonUtilities.GetInstance().getSettingFilePath();
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(filePath);
                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "ColumnChooser")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                            foreach (XmlNode node in userMappingsXML)
                            {
                                    if (node.ChildNodes.Item(0).Name == "ColumnChooserDataType" && node.ChildNodes.Item(0).InnerText == trnsact.comboBoxDatatype.SelectedItem.ToString())
                                    {
                                        if (node.ChildNodes.Item(1).Name == "ConnectionType" && node.ChildNodes.Item( 1).InnerText == CommonUtilities.GetInstance().ConnectedSoftware)
                                        {
                                            if (node.ChildNodes.Item( 2).Name == "ColumnChooserName" && node.ChildNodes.Item(2).InnerText == cmbDelete.SelectedItem.ToString())
                                            {
                                                node.ParentNode.RemoveChild(node);
                                            }
                                        }
                                    }
                                  

                                //foreach (XmlNode childNodes in node.ChildNodes)
                                //{
                                //    if (childNodes.Name == "ColumnChooserDataType" && childNodes.InnerText == trnsact.comboBoxDatatype.SelectedItem.ToString())
                                //    {
                                //        //node.AppendChild(xdoc.CreateElement("ColumnChooserDataType")).InnerText = trnsact.comboBoxDatatype.SelectedItem.ToString();
                                //        //node.AppendChild(xdoc.CreateElement("ConnectionType")).InnerText = CommonUtilities.GetInstance().ConnectedSoftware;
                                //        //if (childNodes.InnerText == cmbDelete.SelectedItem.ToString())
                                //        //{
                                //        foreach (XmlNode colchildNodes in node.ChildNodes)
                                //        {
                                //            if (colchildNodes.InnerText == cmbDelete.SelectedItem.ToString())
                                //            {
                                //            while (true)
                                //            {
                                //                bool removed = false;
                                //                foreach (XmlNode colchildNodes1 in node.ChildNodes)
                                //                {

                                //                    if (colchildNodes1.Name == "ColumnName")
                                //                    {
                                //                        node.RemoveChild(colchildNodes1);
                                //                        removed = true;
                                //                        break;
                                //                    }
                                //                }
                                //                if (!removed)
                                //                    break;
                                //            }
                                //        }
                                //        }
                                //            node.RemoveChild(childNodes);
                                //       // }
                                //    }
                                //    else if (childNodes.Name == "ColumnChooserName" && childNodes.InnerText == trnsact.comboBoxDatatype.SelectedItem.ToString())
                                //    { 
                                    
                                //    }
                                //    if (node.ChildNodes.Count == 0)
                                //    {
                                //        node.ParentNode.RemoveChild(node);
                                //    }
                                //    else
                                //    {
                                //        break;
                                //    }
                                //    break;
                                //}
                            }
                            cmbDelete.Items.Remove(cmbDelete.SelectedItem);
                            xdoc.Save(filePath);
                            MessageBox.Show("Template Deleted Successfully.");
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                // If 'No', do something here.
            }
            this.Close();


        }

    }
}
