using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TransactionImporter
{
    public partial class MessageMapping : Form
    {
        private static MessageMapping frm_Messagemapping;
        private bool flag = false;
        private static MessageMapping getInstances()
        {
            if (frm_Messagemapping == null)
                frm_Messagemapping = new MessageMapping();
            return frm_Messagemapping;
        }

        public string mapName
        {
            get
            {
                return this.textBoxMappingName.Text.TrimStart().TrimEnd().Trim();
            }
        }

        public MessageMapping()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.textBoxMappingName.Text.Trim().TrimStart().TrimEnd() == string.Empty)
            {
                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG017"), "Zed Axis");
                return;
            }
            else
            {
                DataProcessingBlocks.UserMappingsCollection col = new DataProcessingBlocks.UserMappingsCollection().Load();
                DataProcessingBlocks.CommonUtilities.GetInstance().SelectedMapping.Name = textBoxMappingName.Text;
                if (this.textBoxMappingName.Text == "<Select Mapping Name>")
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG026"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                flag = col.IsExists(this.textBoxMappingName.Text.TrimStart().TrimEnd().Trim());
                if (flag == true)
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG026"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}