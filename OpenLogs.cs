using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class OpenLogs : Form
    {  
        private string m_OpenLogsContent;

        public string OpenLogsContent
        {
            get { return OpenLogsContent; }
            set { m_OpenLogsContent = value; }
        }        

        public OpenLogs()
        {
            InitializeComponent();
            m_OpenLogsContent= "Empty";
        }

        private void OpenLog_Load(object sender, EventArgs e)
        {
            richTextBoxOpenLog.Text = m_OpenLogsContent;
        }

        private void buttonLogFiles_Click(object sender, EventArgs e)
        {

        }      
       
    }
}