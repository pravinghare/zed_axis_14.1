﻿// ===============================================================================
// 
// LicenseMessage.cs
//
// This file contains the implementations of the Message License members, 
// Properties and Display alert message of license.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class LicenseMessage : Form
    {
        private static LicenseMessage m_LicenseMessage;

        public LicenseMessage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// getting a current instance of License Message module.
        /// </summary>
        /// <returns></returns>
        public static LicenseMessage GetInstance()
        {
            if (m_LicenseMessage == null)
                m_LicenseMessage = new LicenseMessage();
            return m_LicenseMessage;
        }

        private void linkLabelLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Open the Internet Browser for purchasing license.
            try
            {
                System.Diagnostics.Process.Start( EDI.Constant.Constants.BuyID);
            }
            catch
            { }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            //close the License Message form.
            this.Close();
        }

      
    }
}
