// ===============================================================================
// 
// SetupMonitoringService.cs
//
// This file contains the implementations of the Monitoring Services methods which monitor folders , 
// Some methods which installs services and delete the services.
//
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Configuration.Install;
using System.IO;
using System.Globalization;
using System.Security;
using EDI.Constant;
using DataProcessingBlocks;
using DBConnection;
using SQLQueries;


namespace MessageMonitoringService
{
    public partial class SetupMonitoringService : Form
    {
        #region Private Members

        private static SetupMonitoringService m_SetupMonitoringService;
        private string serviceName = string.Empty;

        #endregion

        #region Constructor
        
        public SetupMonitoringService()
        {
            InitializeComponent();
            AddFolderPeriod();
        }

        #endregion

        #region Properties
        /// <summary>
        /// Get or Set Service Name value.
        /// </summary>
        public string ServiceName
        {
            get
            {
                return serviceName;
            }
            set
            {
                serviceName = value;
            }
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Getting new or current instance of Setup Monitoring Service.
        /// </summary>
        /// <returns>Object of Setup Monitoring Service.</returns>
        public static SetupMonitoringService GetInstance()
        {
            if (m_SetupMonitoringService == null)
                m_SetupMonitoringService = new SetupMonitoringService();
            return m_SetupMonitoringService;
        }
        #endregion

        #region Private Methods of Setup Monitoring Service

        /// <summary>
        /// This event is used for Edi Service details and add service details.
        /// </summary>
        /// <param name="sender">sender of the object.</param>
        /// <param name="e">Event arguments of object.</param>
        private void SetupMonitoringService_Load(object sender, EventArgs e)
        {
            ClearForm();
            try
            {

                try
                {
                    DataTable dtebay = MailSettings.TradingPartner.GeteBayTradingPartner();
                    if (dtebay != null)
                    {
                        if (dtebay.Rows.Count > 0)
                        {
                            //comboBoxTradingPartner.Items.Clear();
                            //comboBoxTradingPartner.Items.Add("< Select >");
                            for (int count = 0; count < dtebay.Rows.Count; count++)
                            {
                                //comboBoxTradingPartner.Items.Add(dtebay.Rows[count][0].ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    MessageBox.Show("Unable to ", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                //Add service details when form status mode will be edit.
                if (SetupMonitoringService.GetInstance().ServiceName != null)
                {
                    if (SetupMonitoringService.GetInstance().ServiceName.ToString() != string.Empty)
                    {
                        this.textBoxServiceName.Text = SetupMonitoringService.GetInstance().ServiceName;
                        //Format command text query of MySql
                        string commandText = string.Format(SQLQueries.Queries.SQMS31, SetupMonitoringService.GetInstance().ServiceName.ToString());
                        //Getting Service details by passing Service name
                        DataSet dsServiceDetails = DBConnection.MySqlDataAcess.ExecuteDataset(commandText);

                        this.textBoxServiceName.ReadOnly = true;

                        if (dsServiceDetails != null)
                        {
                            if (dsServiceDetails.Tables[0].Rows.Count != 0)
                            {
                                //Getting folder path of Message monitor.
                                if (dsServiceDetails.Tables[0].Rows[0][DataProcessingBlocks.MessageMonitorColumns.Folder.ToString()].ToString().Contains("/"))
                                    this.textBoxFolderPath.Text = dsServiceDetails.Tables[0].Rows[0][DataProcessingBlocks.MessageMonitorColumns.Folder.ToString()].ToString();
                                else
                                {
                                    int selectedIndex = 0;
                                    //for (int temp = 0; temp < comboBoxTradingPartner.Items.Count; temp++)
                                    //{
                                    //    if (dsServiceDetails.Tables[0].Rows[0][DataProcessingBlocks.MessageMonitorColumns.Folder.ToString()].ToString() == comboBoxTradingPartner.Items[temp].ToString())
                                    //        selectedIndex = temp;
                                    //}
                                    //comboBoxTradingPartner.SelectedIndex = selectedIndex;
                                }
                                int statusCount = Convert.ToInt32(dsServiceDetails.Tables[0].Rows[0][DataProcessingBlocks.MessageMonitorColumns.IsActive.ToString()].ToString());
                                if (statusCount == 0)
                                    checkBoxStatus.Checked = false;
                                else
                                    checkBoxStatus.Checked = true;

                                //Assinging period to combobox.
                                string period = dsServiceDetails.Tables[0].Rows[0][DataProcessingBlocks.MessageMonitorColumns.Period.ToString()].ToString();
                                int index = 0;
                                for (int temp = 0; temp < comboBoxPeriod.Items.Count; temp++)
                                {
                                    if (period == comboBoxPeriod.Items[temp].ToString())
                                        index = temp;
                                }
                                comboBoxPeriod.SelectedIndex = index;
                            }
                        }
                    }
                    else
                    {
                        textBoxServiceName.ReadOnly = false;
                        textBoxServiceName.Focus();
                    }
                }
                else
                {
                    textBoxServiceName.ReadOnly = false;
                    textBoxServiceName.Focus();
                }

               
            }

            catch (Exception ex)
            {
                //Catching the exception.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
        }

        /// <summary>
        /// This method is used for close the Setup Form.
        /// </summary>
        /// <param name="sender">Sender of the object.</param>
        /// <param name="e">Event arguments of the object.</param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.textBoxServiceName.Text = string.Empty;
            this.textBoxFolderPath.Text = string.Empty;
            this.comboBoxPeriod.SelectedIndex = -1;
            this.folderBrowserDialogService.SelectedPath = null;
           
            //Closing Setup Monitoring service form.
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// This event is used for create Monitoring service
        /// Save the services details into Database
        /// </summary>
        /// <param name="sender">sender of the object</param>
        /// <param name="e">Event arguments of object</param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            #region Create Windows Monitoring Service
            //This block is used for when service mode is Edit.
            if (textBoxServiceName.ReadOnly == true)
            {
                #region Save Edited Service

                if (comboBoxPeriod.SelectedIndex <= 0)
                {
                    //Display message of blank period.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG106"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    comboBoxPeriod.Focus();
                    return;
                }

                #region Updating Message Monitoring Service into Machin

                String exeName = string.Empty;
                #region commented code for ebay oscommerce removal
                //if (comboBoxTradingPartner.SelectedIndex > 0)
                //{
                //    string commandText = string.Format(SQLQueries.Queries.eBaySQL0007, comboBoxTradingPartner.SelectedItem.ToString());
                //    //Getting eBay Trading Partner details by passing Trading Partner name.
                //    DataSet dsTradingPartnerDetails = DBConnection.MySqlDataAcess.ExecuteDataset(commandText);
                //    if (dsTradingPartnerDetails != null)
                //    {
                //        if (dsTradingPartnerDetails.Tables[0].Rows.Count > 0)
                //        {
                //            #region Remove Message Monitoring Service

                //            string result = RemoveMonitoringServices("SC DELETE " + textBoxServiceName.Text);

                //            if (result.Contains(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG113").ToString().Replace("\n", string.Empty).Replace("\r", string.Empty)))
                //            {
                //                //Refreshing Services.
                //                string newresult = RemoveMonitoringServices("SC stop " + textBoxServiceName.Text.Replace(" ", string.Empty));

                //                newresult = null;

                //            }
                //            else
                //            {
                //                //MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //                CommonUtilities.WriteErrorLog(result);
                //                //return;
                //            }
                //            #endregion

                //            if (dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString() == "4")
                //            {
                //                #region Convert eBay class to exe

                //                string eBayToken = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();

                //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                //                string eBayType = dsTradingPartnerDetails.Tables[0].Rows[0]["CompanyName"].ToString();

                //                if (string.IsNullOrEmpty(eBayType))
                //                    eBayType = "SandBox";

                //                string sourceName = Application.StartupPath + "\\eBay.cs";

                //                //Create log directory for storing Messages.
                //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                //                //Adding Directory separate character to Logdirectory file path.
                //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                //                {
                //                    logDirectory = Application.StartupPath + logDirectory;
                //                    try
                //                    {
                //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //                        else
                //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                //                    }
                //                    catch
                //                    {
 
                //                    }
                //                }
                //                //Checking if directory does not exists then create it.
                //                if (!Directory.Exists(logDirectory))
                //                {
                //                    Directory.CreateDirectory(logDirectory);
                //                }
                //                //string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog " + textBoxServiceName.Text + ".txt";
                //                //string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog\\" + textBoxServiceName.Text + ".txt";
                //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                //                try
                //                {
                //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //                    else
                //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                //                }
                //                catch
                //                {

                //                }

                //                string destName = logDirectory + "\\eBay.cs";

                //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new eBay());  }";

                //                string period = string.Empty;
                //                switch (comboBoxPeriod.SelectedItem.ToString())
                //                {
                //                    case "1 minute":
                //                        period = Convert.ToString(1 * 1000 * 60);
                //                        break;
                //                    case "5 minute":
                //                        period = Convert.ToString(5 * 1000 * 60);
                //                        break;
                //                    case "10 minute":
                //                        period = Convert.ToString(10 * 1000 * 60);
                //                        break;
                //                    case "15 minute":
                //                        period = Convert.ToString(15 * 1000 * 60);
                //                        break;
                //                    case "30 minute":
                //                        period = Convert.ToString(30 * 1000 * 60);
                //                        break;
                //                    case "45 minute":
                //                        period = Convert.ToString(45 * 1000 * 60);
                //                        break;
                //                    case "1 hour":
                //                        period = Convert.ToString(60 * 1000 * 60);
                //                        break;
                //                    case "2 hour":
                //                        period = Convert.ToString(120 * 1000 * 60);
                //                        break;
                //                    case "5 hour":
                //                        period = Convert.ToString(300 * 1000 * 60);
                //                        break;
                //                    case "10 hour":
                //                        period = Convert.ToString(600 * 1000 * 60);
                //                        break;
                //                    case "12 hour":
                //                        period = Convert.ToString(720 * 1000 * 60);
                //                        break;
                //                    default:
                //                        period = Convert.ToString(10 * 1000 * 60);
                //                        break;
                //                }
                //                //StreamReader strReader = new StreamReader(destName);
                //                int lineCounter = 1;
                //                StringBuilder sb = new StringBuilder();
                //                //StreamWriter strWriter = new StreamWriter(destName, true);
                //                string connectionString = string.Empty;
                //                string file = string.Empty;
                //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");//NewMySQLConnection
                //                using (StreamReader sr = new StreamReader(sourceName))
                //                {

                //                    string line;

                //                    while ((line = sr.ReadLine()) != null)
                //                    {
                //                        if (line.Trim() == "}")
                //                        {
                //                            if (lineCounter == 1)
                //                            {
                //                                sb.AppendLine(line);
                //                                sb.AppendLine();
                //                                sb.AppendLine(addData);
                //                                sb.AppendLine();
                //                                lineCounter = 0;
                //                            }
                //                            else
                //                                sb.AppendLine(line);
                //                            //break;

                //                        }
                //                        else
                //                        {
                //                            if (line.Trim() == "private string connectionString = string.Empty;")
                //                            {
                //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                //                            }
                //                            else
                //                                if (line.Trim() == "private string tpID = string.Empty;")
                //                                {
                //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                //                                }
                //                                else
                //                                    if (line.Trim() == "private string mailID = string.Empty;")
                //                                    {
                //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                //                                    }
                //                                    else
                //                                        if (line.Trim() == "private string eBayType = string.Empty;")
                //                                        {
                //                                            sb.AppendLine("private string eBayType = \"" + eBayType + "\";");
                //                                        }
                //                                    else
                //                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                //                                        {
                //                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                //                                        }
                //                                        else
                //                                            if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                //                                            {
                //                                                sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                //                                            }
                //                                            else
                //                                                if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                //                                                {
                //                                                    sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                //                                                }
                //                                                else
                //                                                    if (line.Trim() == "string eBayToken = string.Empty;")
                //                                                    {
                //                                                        sb.AppendLine("string eBayToken = @\"" + eBayToken + "\";");
                //                                                    }
                //                                                    else
                //                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                //                                                        {
                //                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                //                                                        }
                //                                                        else
                //                                                            sb.AppendLine(line);
                //                        }


                //                    }

                //                }
                //                using (StreamWriter strw = new StreamWriter(destName))
                //                {
                //                    strw.Write(sb.ToString());
                //                }


                //                FileInfo sourceFile = new FileInfo(destName);
                //                CodeDomProvider provider = null;
                //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                //                //// Select the code provider based on the input file extension.
                //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                //                {
                //                    provider = CodeDomProvider.CreateProvider("CSharp");
                //                }


                //                if (provider != null)
                //                {

                //                    // Format the executable file name.
                //                    // Build the output assembly path using the current directory
                //                    // and <source>_cs.exe or <source>_vb.exe.

                //                    exeName = String.Format(@"{0}\{1}.exe",
                //                        System.Environment.CurrentDirectory,
                //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text));

                //                    CompilerParameters cp = new CompilerParameters();
                //                    cp.ReferencedAssemblies.Add("System.dll");
                //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                //                    //cp.ReferencedAssemblies.Add("System.Net.dll");
                //                    //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                //                    //Microsoft.Office.Interop.Excel
                //                    // Generate an executable instead of 
                //                    // a class library.
                //                    cp.GenerateExecutable = true;

                //                    // Specify the assembly file name to generate.
                //                    cp.OutputAssembly = exeName;

                //                    // Save the assembly as a physical file.
                //                    cp.GenerateInMemory = false;

                //                    // Set whether to treat all warnings as errors.
                //                    cp.TreatWarningsAsErrors = false;

                //                    // Invoke compilation of the source file.
                //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                //                        destName);

                //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                //                    if (cr.Errors.Count > 0)
                //                    {

                //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //                        return;
                //                    }



                //                }

                //                #endregion

                //            }
                //            else
                //            {

                //                #region Convert OSCommerce class to exe

                //                string dbHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                //                string dbUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["UserName"].ToString();
                //                string dbPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["Password"].ToString();
                //                if (dbPassword.Contains(";"))
                //                    dbPassword = "'" + dbPassword + "'";

                //                string dbName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCDatabase"].ToString();
                //                string dbPrefix = dsTradingPartnerDetails.Tables[0].Rows[0]["XSDLocation"].ToString();
                          
                //                string dbPort = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                //                string sshHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                //                string sshPortNo = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayDevId"].ToString();
                //                string sshUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayCertID"].ToString();
                //                string sshPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAppId"].ToString();
                //                if (sshPassword.Contains(";"))
                //                    sshPassword = "'" + sshPassword + "'";

                //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                //                string sourceName = Application.StartupPath + "\\OSCommerce.cs";

                //                //Create log directory for storing Messages.
                //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                //                //Adding Directory separate character to Logdirectory file path.
                //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                //                {
                //                    logDirectory = Application.StartupPath + logDirectory;
                //                    try
                //                    {
                //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //                        else
                //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                //                    }
                //                    catch
                //                    {

                //                    }
                //                }
                //                //Checking if directory does not exists then create it.
                //                if (!Directory.Exists(logDirectory))
                //                {
                //                    Directory.CreateDirectory(logDirectory);
                //                }
                //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog " + textBoxServiceName.Text + ".txt";
                //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog\\" + textBoxServiceName.Text + ".txt";
                //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                //                try
                //                {
                //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //                    else
                //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                //                }
                //                catch
                //                {

                //                }

                //                string destName = logDirectory + "\\OSCommerce.cs";

                //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new OSCommerce());  }";

                //                string period = string.Empty;
                //                switch (comboBoxPeriod.SelectedItem.ToString())
                //                {
                //                    case "1 minute":
                //                        period = Convert.ToString(1 * 1000 * 60);
                //                        break;
                //                    case "5 minute":
                //                        period = Convert.ToString(5 * 1000 * 60);
                //                        break;
                //                    case "10 minute":
                //                        period = Convert.ToString(10 * 1000 * 60);
                //                        break;
                //                    case "15 minute":
                //                        period = Convert.ToString(15 * 1000 * 60);
                //                        break;
                //                    case "30 minute":
                //                        period = Convert.ToString(30 * 1000 * 60);
                //                        break;
                //                    case "45 minute":
                //                        period = Convert.ToString(45 * 1000 * 60);
                //                        break;
                //                    case "1 hour":
                //                        period = Convert.ToString(60 * 1000 * 60);
                //                        break;
                //                    case "2 hour":
                //                        period = Convert.ToString(120 * 1000 * 60);
                //                        break;
                //                    case "5 hour":
                //                        period = Convert.ToString(300 * 1000 * 60);
                //                        break;
                //                    case "10 hour":
                //                        period = Convert.ToString(600 * 1000 * 60);
                //                        break;
                //                    case "12 hour":
                //                        period = Convert.ToString(720 * 1000 * 60);
                //                        break;
                //                    default:
                //                        period = Convert.ToString(10 * 1000 * 60);
                //                        break;
                //                }
                //                //StreamReader strReader = new StreamReader(destName);
                //                int lineCounter = 1;
                //                StringBuilder sb = new StringBuilder();
                //                //StreamWriter strWriter = new StreamWriter(destName, true);
                //                string connectionString = string.Empty;
                //                string file = string.Empty;
                //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
                //                using (StreamReader sr = new StreamReader(sourceName))
                //                {

                //                    string line;

                //                    while ((line = sr.ReadLine()) != null)
                //                    {
                //                        if (line.Trim() == "}")
                //                        {
                //                            if (lineCounter == 1)
                //                            {
                //                                sb.AppendLine(line);
                //                                sb.AppendLine();
                //                                sb.AppendLine(addData);
                //                                sb.AppendLine();
                //                                lineCounter = 0;
                //                            }
                //                            else
                //                                sb.AppendLine(line);
                //                            //break;

                //                        }
                //                        else
                //                        {
                //                            if (line.Trim() == "private string connectionString = string.Empty;")
                //                            {
                //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                //                            }
                //                            else
                //                                if (line.Trim() == "private string tpID = string.Empty;")
                //                                {
                //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                //                                }
                //                                else
                //                                    if (line.Trim() == "private string mailID = string.Empty;")
                //                                    {
                //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                //                                    }
                //                                    else
                //                                        if (line.Trim() == "private string dbHostName = string.Empty;")
                //                                        {
                //                                            sb.AppendLine("private string dbHostName = \"" + dbHostName + "\";");
                //                                        }
                //                                        else
                //                                            if (line.Trim() == "private string dbUserName = string.Empty;")
                //                                            {
                //                                                sb.AppendLine("private string dbUserName = \"" + dbUserName + "\";");
                //                                            }
                //                                            else
                //                                                if (line.Trim() == "private string dbPassword = string.Empty;")
                //                                                {
                //                                                    sb.AppendLine("private string dbPassword = \"" + dbPassword + "\";");
                //                                                }
                //                                                else
                //                                                    if (line.Trim() == "private string dbName = string.Empty;")
                //                                                    {
                //                                                        sb.AppendLine("private string dbName = \"" + dbName + "\";");
                //                                                    }
                //                                                    else
                //                                                        if (line.Trim() == "private string dbPrefix = string.Empty;")
                //                                                        {
                //                                                            sb.AppendLine("private string dbPrefix = \"" + dbPrefix + "\";");
                //                                                        }
                //                                                    else
                //                                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                //                                                        {
                //                                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                //                                                        }
                //                                                        else
                //                                                            if (line.Trim() == "private string sshHostName = string.Empty;")
                //                                                            {
                //                                                                sb.AppendLine("private string sshHostName = \"" + sshHostName + "\";");

                //                                                            }
                //                                                            else
                //                                                                if (line.Trim() == "private string sshPortNo = string.Empty;")
                //                                                                {
                //                                                                    sb.AppendLine("private string sshPortNo = \"" + sshPortNo + "\";");

                //                                                                }
                //                                                                else
                //                                                                    if (line.Trim() == "private string sshUserName = string.Empty;")
                //                                                                    {
                //                                                                        sb.AppendLine("private string sshUserName = \"" + sshUserName + "\";");

                //                                                                    }
                //                                                                    else
                //                                                                        if (line.Trim() == "private string sshPassword = string.Empty;")
                //                                                                        {
                //                                                                            sb.AppendLine("private string sshPassword = \"" + sshPassword + "\";");

                //                                                                        }
                //                                                                        else
                //                                                                            if (line.Trim() == "private string dbPort = string.Empty;")
                //                                                                            {
                //                                                                                sb.AppendLine("private string dbPort = \"" + dbPort + "\";");

                //                                                                            }
                //                                                                            else
                //                                                                                if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                //                                                                                {
                //                                                                                    sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                //                                                                                }
                //                                                                                else
                //                                                                                    if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                //                                                                                    {
                //                                                                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                //                                                                                    }
                //                                                                                    else

                //                                                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                //                                                                                        {
                //                                                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                //                                                                                        }
                //                                                                                        else
                //                                                                                            sb.AppendLine(line);
                //                        }
                //                    }

                //                }
                //                using (StreamWriter strw = new StreamWriter(destName))
                //                {
                //                    strw.Write(sb.ToString());
                //                }


                //                FileInfo sourceFile = new FileInfo(destName);
                //                CodeDomProvider provider = null;
                //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                //                //// Select the code provider based on the input file extension.
                //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                //                {
                //                    provider = CodeDomProvider.CreateProvider("CSharp");
                //                }


                //                if (provider != null)
                //                {

                //                    // Format the executable file name.
                //                    // Build the output assembly path using the current directory
                //                    // and <source>_cs.exe or <source>_vb.exe.

                //                    exeName = String.Format(@"{0}\{1}.exe",
                //                        System.Environment.CurrentDirectory,
                //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text + "_Edited" + DateTime.Now.Second.ToString()));

                //                    CompilerParameters cp = new CompilerParameters();
                //                    cp.ReferencedAssemblies.Add("System.dll");
                //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                //                    cp.ReferencedAssemblies.Add("Tamir.SharpSSH.dll");
                //                    cp.ReferencedAssemblies.Add("Org.Mentalis.Security.dll");
                //                    cp.ReferencedAssemblies.Add("DiffieHellman.dll");
                //                    cp.ReferencedAssemblies.Add("MySql.Data.dll");
                //                    //cp.ReferencedAssemblies.Add("System.Net.dll");
                //                    //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                //                    //Microsoft.Office.Interop.Excel
                //                    // Generate an executable instead of 
                //                    // a class library.
                //                    cp.GenerateExecutable = true;

                //                    // Specify the assembly file name to generate.
                //                    cp.OutputAssembly = exeName;

                //                    // Save the assembly as a physical file.
                //                    cp.GenerateInMemory = false;

                //                    // Set whether to treat all warnings as errors.
                //                    cp.TreatWarningsAsErrors = false;

                //                    // Invoke compilation of the source file.
                //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                //                        destName);

                //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                //                    if (cr.Errors.Count > 0)
                //                    {

                //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //                        return;
                //                    }



                //                }

                //                #endregion
                //            }

                //        }
                //        else
                //        {
                //            MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //            return;
                //        }

                //    }
                //    else
                //    {
                //        MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //        return;
                //    }


                //}
                #endregion
                //else
                //{
                    if (string.IsNullOrEmpty(textBoxFolderPath.Text))
                    {
                        //Display message of blank folder path.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG105"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBoxFolderPath.Focus();
                        return;
                    }
                    else if (!string.IsNullOrEmpty(textBoxFolderPath.Text.Trim()))
                    {
                        if (!System.IO.Directory.Exists(textBoxFolderPath.Text))
                        {
                            MessageBox.Show("Invalid Folder Path.Please select different Folder Path.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBoxFolderPath.Focus();
                            return;
                        }
                    }                   
                              
                    #region Remove Message Monitoring Service

                    string result = RemoveMonitoringServices("SC DELETE " + textBoxServiceName.Text);

                    if (result.Contains(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG113").ToString().Replace("\n", string.Empty).Replace("\r", string.Empty)))
                    {
                        //Refreshing Services.
                        string newresult = RemoveMonitoringServices("SC stop " + textBoxServiceName.Text.Replace(" ", string.Empty));

                        newresult = null;

                    }
                    else
                    {
                        //MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CommonUtilities.WriteErrorLog(result);
                        //return;
                    }
                    #endregion

                    #region Convert class to exe

                    string sourceName = Application.StartupPath + "\\ServiceClass.cs";

                    //Create log directory for storing Messages.
                    string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                    //Adding Directory separate character to Logdirectory file path.
                    if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    {
                        logDirectory = Application.StartupPath + logDirectory;
                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            else
                                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        }
                        catch
                        {

                        }
                    }
                    //Checking if directory does not exists then create it.
                    if (!Directory.Exists(logDirectory))
                    {
                        Directory.CreateDirectory(logDirectory);
                    }
                    //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog " + textBoxServiceName.Text + ".txt";
                    //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                    //string logFileName = logDirectory + "\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                    string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        else
                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                    }
                    catch
                    {

                    }

                    string destName = logDirectory + "\\ServiceClass.cs";

                    string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new ServiceClass());  }";

                    string period = string.Empty;
                    switch (comboBoxPeriod.SelectedItem.ToString())
                    {
                        case "1 minute":
                            period = Convert.ToString(1 * 1000 * 60);
                            break;
                        case "5 minute":
                            period = Convert.ToString(5 * 1000 * 60);
                            break;
                        case "10 minute":
                            period = Convert.ToString(10 * 1000 * 60);
                            break;
                        case "15 minute":
                            period = Convert.ToString(15 * 1000 * 60);
                            break;
                        case "30 minute":
                            period = Convert.ToString(30 * 1000 * 60);
                            break;
                        case "45 minute":
                            period = Convert.ToString(45 * 1000 * 60);
                            break;
                        case "1 hour":
                            period = Convert.ToString(60 * 1000 * 60);
                            break;
                        case "2 hour":
                            period = Convert.ToString(120 * 1000 * 60);
                            break;
                        case "5 hour":
                            period = Convert.ToString(300 * 1000 * 60);
                            break;
                        case "10 hour":
                            period = Convert.ToString(600 * 1000 * 60);
                            break;
                        case "12 hour":
                            period = Convert.ToString(720 * 1000 * 60);
                            break;
                        default:
                            period = Convert.ToString(10 * 1000 * 60);
                            break;
                    }
                    //StreamReader strReader = new StreamReader(destName);
                    int lineCounter = 1;
                    StringBuilder sb = new StringBuilder();
                    //StreamWriter strWriter = new StreamWriter(destName, true);
                    string connectionString = string.Empty;
                    string file = string.Empty;
                    //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                    connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("NewMySQLConnection");
                    using (StreamReader sr = new StreamReader(sourceName))
                    {

                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            if (line.Trim() == "}")
                            {
                                if (lineCounter == 1)
                                {
                                    sb.AppendLine(line);
                                    sb.AppendLine();
                                    sb.AppendLine(addData);
                                    sb.AppendLine();
                                    lineCounter = 0;
                                }
                                else
                                    sb.AppendLine(line);
                                //break;

                            }
                            else
                            {
                                if (line.Trim() == "this.ServiceName = string.Empty;")
                                {
                                    sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                                }
                                else
                                    if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                                    {
                                        sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                                        //sb.AppendLine("monitorTimer.Interval = " + period + ";");
                                    }
                                    else

                                        if (line.Trim() == "string directoryPath = string.Empty;")
                                        {
                                            sb.AppendLine("string directoryPath = @\"" + textBoxFolderPath.Text + "\";");
                                        }
                                        else
                                            if (line.Trim() == "con.ConnectionString = string.Empty;")
                                            {
                                                sb.AppendLine("con.ConnectionString = @\"" + connectionString + "\";");
                                            }
                                            else
                                                if (line.Trim() == "flinfo.CopyTo(string.Empty, true);")
                                                {
                                                    string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                                    try
                                                    {
                                                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                            destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                        else
                                                            destPath = destPath.Replace("Program Files", "Users\\Public\\Documents"); 
                                                    }
                                                    catch
                                                    {
                                                    }
                                                    sb.AppendLine("flinfo.CopyTo(@" + destPath + ", true);");
                                                }
                                                else
                                                    if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                                                    {
                                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                                                    }
                                                    else
                                                        if (line.Trim() == "csvfl.CopyTo(string.Empty, true);")
                                                        {
                                                            string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";

                                                            try
                                                            {
                                                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                                    destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                                else
                                                                    destPath = destPath.Replace("Program Files", "Users\\Public\\Documents"); 
                                                            }
                                                            catch
                                                            {
                                                            }
                                                            sb.AppendLine("csvfl.CopyTo(@" + destPath + ", true);");
                                                        }
                                                        else
                                                            if (line.Trim() == "string serviceText = string.Empty;")
                                                            {
                                                                sb.AppendLine("string serviceText = \"" + textBoxServiceName.Text + "\";");
                                                            }
                                                            else
                                                                sb.AppendLine(line);
                            }

                        }

                    }
                    using (StreamWriter strw = new StreamWriter(destName))
                    {
                        strw.Write(sb.ToString());
                    }


                    FileInfo sourceFile = new FileInfo(destName);
                    CodeDomProvider provider = null;
                    //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                    //ICodeCompiler provider = codeProvider.CreateCompiler();


                    //// Select the code provider based on the input file extension.
                    if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                    {
                        provider = CodeDomProvider.CreateProvider("CSharp");
                    }

                    //String exeName = string.Empty;
                    if (provider != null)
                    {

                        // Format the executable file name.
                        // Build the output assembly path using the current directory
                        // and <source>_cs.exe or <source>_vb.exe.

                        exeName = String.Format(@"{0}\{1}.exe",
                            System.Environment.CurrentDirectory,
                            sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text));

                        CompilerParameters cp = new CompilerParameters();
                        cp.ReferencedAssemblies.Add("System.dll");
                        cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                        cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                        cp.ReferencedAssemblies.Add("System.Configuration.dll");
                        cp.ReferencedAssemblies.Add("System.Data.dll");
                        cp.ReferencedAssemblies.Add("System.Xml.dll");
                        cp.ReferencedAssemblies.Add("MySql.Data.dll");
                        //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                        //Microsoft.Office.Interop.Excel
                        // Generate an executable instead of 
                        // a class library.
                        cp.GenerateExecutable = true;

                        // Specify the assembly file name to generate.
                        cp.OutputAssembly = exeName;

                        // Save the assembly as a physical file.
                        cp.GenerateInMemory = false;

                        // Set whether to treat all warnings as errors.
                        cp.TreatWarningsAsErrors = false;

                        // Invoke compilation of the source file.
                        CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                            destName);

                        //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                        if (cr.Errors.Count > 0)
                        {

                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }



                    }

                    #endregion
               // }

                SetupMonitoringService.GetInstance().ServiceName = string.Empty;
                #region Installing Service into System

                //Adding Service Process Installer to class
                ServiceProcessInstaller ProcesServiceInstaller = new ServiceProcessInstaller();
                ProcesServiceInstaller.Account = ServiceAccount.LocalSystem;
                ProcesServiceInstaller.Username = null;
                ProcesServiceInstaller.Password = null;
                //Creating Service installer object for service.
                ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                InstallContext Context = new System.Configuration.Install.InstallContext();
                string svcApp = Application.StartupPath;

                //Assinging path to Service class
                String path = String.Format("/assemblypath={0}", exeName);
                String[] cmdline = { path };
                //Adding context into service.
                Context = new System.Configuration.Install.InstallContext("", cmdline);
                ServiceInstallerObj.Context = Context;
                //Assinging display name to Service installer.
                ServiceInstallerObj.DisplayName = "Zed Axis Service " + textBoxServiceName.Text;
                ServiceInstallerObj.Description = "Zed Axis Message Monitoring Service";
                ServiceInstallerObj.ServiceName = textBoxServiceName.Text;
                if (checkBoxStatus.Checked)
                    ServiceInstallerObj.StartType = ServiceStartMode.Automatic;
                else
                    ServiceInstallerObj.StartType = ServiceStartMode.Manual;
                ServiceInstallerObj.Parent = ProcesServiceInstaller;


                System.Collections.Specialized.ListDictionary state = new System.Collections.Specialized.ListDictionary();
                try
                {
                    //Installing Service into Local Machine
                    ServiceInstallerObj.Install(state);

                    //Success message for updating service.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG119"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ServiceController sc = new ServiceController(textBoxServiceName.Text, System.Environment.MachineName);
                    if (!checkBoxStatus.Checked)
                    {
                        //If status is inactive then service has been stopped.

                        try
                        {
                            if (sc.Status == ServiceControllerStatus.Running)
                            {
                                //Service stopped.
                                sc.Stop();
                                sc.WaitForStatus(ServiceControllerStatus.Stopped);
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            if (sc.Status != ServiceControllerStatus.Running)
                            {
                                //Service running.
                                sc.Start();
                                sc.WaitForStatus(ServiceControllerStatus.Running);
                            }

                        }
                        catch
                        { }
                    }

                    #region Updating Service Details in Database

                    //Updating the Message Monitoring Service details.
                    try
                    {
                        int status = 0;
                        if (checkBoxStatus.Checked)
                            status = 1;
                        object monitorId = MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMS34, textBoxServiceName.Text));
                        if (monitorId != null)
                        {
                            string updateQuery = string.Empty;

                            //if (comboBoxTradingPartner.SelectedIndex > 0)

                            //    //Generate the MYSQL Query for update Message Monitoring data.
                            //    updateQuery = string.Format(SQLQueries.Queries.SQMS29, textBoxServiceName.Text, comboBoxTradingPartner.SelectedItem.ToString(), comboBoxPeriod.SelectedItem.ToString(), status.ToString(), monitorId.ToString());
                            //else
                                //Generate the MYSQL Query for update Message Monitoring data.
                                updateQuery = string.Format(SQLQueries.Queries.SQMS29, textBoxServiceName.Text, textBoxFolderPath.Text, comboBoxPeriod.SelectedItem.ToString(), status.ToString(), monitorId.ToString());

                            updateQuery = updateQuery.Replace("\\", "//");

                            //Performs the update query into database.
                            DBConnection.MySqlDataAcess.ExecuteNonQuery(updateQuery);
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        //Catching the exception.
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                        MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;

                    }

                    #endregion

                    #region Refreshing Data
                    try
                    {
                        DataSet dsServiceData = MySqlDataAcess.ExecuteDataset(SQLQueries.Queries.SQMS33);
                        DataTable dsCloneData = new DataTable();
                        if (dsServiceData != null)
                        {
                            DataColumn dcMonitorID = new DataColumn("MonitorId");
                            DataColumn dcName = new DataColumn("Name");
                            DataColumn dcFolder = new DataColumn("Folder");
                            DataColumn dcPeriod = new DataColumn("Period");
                            DataColumn dcIsActive = new DataColumn("IsActive");
                            dsCloneData.Columns.Add(dcMonitorID);
                            dsCloneData.Columns.Add(dcName);
                            dsCloneData.Columns.Add(dcFolder);
                            dsCloneData.Columns.Add(dcPeriod);
                            dsCloneData.Columns.Add(dcIsActive);

                            //Checking dataset rows count.
                            if (dsServiceData.Tables[0].Rows.Count != 0)
                            {
                                object[] objArray = new object[5];
                                foreach (DataRow dr in dsServiceData.Tables[0].Rows)
                                {
                                    objArray[0] = (object)dr[0].ToString();
                                    objArray[1] = (object)dr[1].ToString();
                                    objArray[2] = (object)dr[2].ToString();
                                    objArray[3] = (object)dr[3].ToString();

                                    if (dr["IsActive"].ToString() == "0")
                                        objArray[4] = (object)"InActive";
                                    else
                                        objArray[4] = (object)"Active";

                                    dsCloneData.LoadDataRow(objArray, true);

                                }
                                dsCloneData.AcceptChanges();
                                MessageMonitoring frmMessageMonitoring = (MessageMonitoring)Application.OpenForms["MessageMonitoring"];

                                DataGridView dgvServices = (DataGridView)frmMessageMonitoring.Controls["dataGridViewMessageServices"];

                                dgvServices.DataSource = dsCloneData;

                                dgvServices.Columns[0].Visible = false;

                                dsServiceData = null;
                            }
                        }
                        this.Close();
                    }
                    catch { }
                    #endregion

                    return;
                }
                catch (SecurityException se)
                {
                    //Log the security exceptions for unsufficient privileges.
                    CommonUtilities.WriteErrorLog(se.Message);
                    CommonUtilities.WriteErrorLog(se.Source);
                    //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG119"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                catch (Exception se)
                {
                    //Log the exceptions for sufficient privileges.
                    CommonUtilities.WriteErrorLog(se.Message);
                    CommonUtilities.WriteErrorLog(se.Source);
                    //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG119"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #endregion

                #endregion


                #endregion

            }
            else
            {
                //This block is used when service mode is New.
                #region Save New Services

                if (string.IsNullOrEmpty(textBoxServiceName.Text.Trim()))
                {
                    //Display message of blank Service Name.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG107"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxServiceName.Focus();
                    return;
                }
                if (comboBoxPeriod.SelectedIndex <= 0)
                {
                    //Display message of blank period.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG106"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    comboBoxPeriod.Focus();
                    return;
                }

                #region Validating Service Name and Saving Service into database.
                //Checking Duplicates Services.
                object countService = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQMS32, textBoxServiceName.Text.Trim()));
                if (countService == null)
                {
                    //Display message of Duplicate Service Name.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG110"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxServiceName.Focus();
                    return;
                }
                if (Convert.ToInt32(countService) > 0)
                {
                    //Display message of Duplicate Service Name.
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG110"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBoxServiceName.Focus();
                    return;
                }

                //Storing the Message Monitoring Service details.
                try
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG112"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                  
                    #region Creating and Installing Windows Monitoring Service
                    
                    String exeName = string.Empty;
                    #region Commented code of ebay OsCommerse in rule
                    //if (comboBoxTradingPartner.SelectedIndex > 0)
                    //{
                    //    string commandText = string.Format(SQLQueries.Queries.eBaySQL0007, comboBoxTradingPartner.SelectedItem.ToString());
                    //    //Getting eBay Trading Partner details by passing Trading Partner name.
                    //    DataSet dsTradingPartnerDetails = DBConnection.MySqlDataAcess.ExecuteDataset(commandText);
                    //    if (dsTradingPartnerDetails != null)
                    //    {
                    //        if (dsTradingPartnerDetails.Tables[0].Rows.Count > 0)
                    //        {
                    //            if (dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString() == "4")
                    //            {
                    //                #region Convert eBay class to exe

                    //                string eBayToken = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();

                    //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                    //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                    //                string eBayType = dsTradingPartnerDetails.Tables[0].Rows[0]["CompanyName"].ToString();

                    //                if (string.IsNullOrEmpty(eBayType))
                    //                    eBayType = "SandBox";

                    //                string sourceName = Application.StartupPath + "\\eBay.cs";

                    //                //Create log directory for storing Messages.
                    //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                    //                //Adding Directory separate character to Logdirectory file path.
                    //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    //                {
                    //                    logDirectory = Application.StartupPath + logDirectory;
                    //                    try
                    //                    {
                    //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    //                        else
                    //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                    //                    }
                    //                    catch
                    //                    {
                    //                    }
                    //                }
                    //                //Checking if directory does not exists then create it.
                    //                if (!Directory.Exists(logDirectory))
                    //                {
                    //                    Directory.CreateDirectory(logDirectory);
                    //                }
                    //                //string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog " + textBoxServiceName.Text + ".txt";
                    //               // string logFileName = Application.StartupPath + "\\LOG\\AxiseBayServiceLog\\" + textBoxServiceName.Text + ".txt";
                    //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                    //                try
                    //                {
                    //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    //                    else
                    //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                    //                }
                    //                catch
                    //                {
                    //                }
                    //                string destName = logDirectory + "\\eBay.cs";

                    //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new eBay());  }";

                    //                string period = string.Empty;
                    //                switch (comboBoxPeriod.SelectedItem.ToString())
                    //                {
                    //                    case "1 minute":
                    //                        period = Convert.ToString(1 * 1000 * 60);
                    //                        break;
                    //                    case "5 minute":
                    //                        period = Convert.ToString(5 * 1000 * 60);
                    //                        break;
                    //                    case "10 minute":
                    //                        period = Convert.ToString(10 * 1000 * 60);
                    //                        break;
                    //                    case "15 minute":
                    //                        period = Convert.ToString(15 * 1000 * 60);
                    //                        break;
                    //                    case "30 minute":
                    //                        period = Convert.ToString(30 * 1000 * 60);
                    //                        break;
                    //                    case "45 minute":
                    //                        period = Convert.ToString(45 * 1000 * 60);
                    //                        break;
                    //                    case "1 hour":
                    //                        period = Convert.ToString(60 * 1000 * 60);
                    //                        break;
                    //                    case "2 hour":
                    //                        period = Convert.ToString(120 * 1000 * 60);
                    //                        break;
                    //                    case "5 hour":
                    //                        period = Convert.ToString(300 * 1000 * 60);
                    //                        break;
                    //                    case "10 hour":
                    //                        period = Convert.ToString(600 * 1000 * 60);
                    //                        break;
                    //                    case "12 hour":
                    //                        period = Convert.ToString(720 * 1000 * 60);
                    //                        break;
                    //                    default:
                    //                        period = Convert.ToString(10 * 1000 * 60);
                    //                        break;
                    //                }
                    //                //StreamReader strReader = new StreamReader(destName);
                    //                int lineCounter = 1;
                    //                StringBuilder sb = new StringBuilder();
                    //                //StreamWriter strWriter = new StreamWriter(destName, true);
                    //                string connectionString = string.Empty;
                    //                string file = string.Empty;
                    //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                    //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
                    //                using (StreamReader sr = new StreamReader(sourceName))
                    //                {

                    //                    string line;

                    //                    while ((line = sr.ReadLine()) != null)
                    //                    {
                    //                        if (line.Trim() == "}")
                    //                        {
                    //                            if (lineCounter == 1)
                    //                            {
                    //                                sb.AppendLine(line);
                    //                                sb.AppendLine();
                    //                                sb.AppendLine(addData);
                    //                                sb.AppendLine();
                    //                                lineCounter = 0;
                    //                            }
                    //                            else
                    //                                sb.AppendLine(line);
                    //                            //break;

                    //                        }
                    //                        else
                    //                        {
                    //                            if (line.Trim() == "private string connectionString = string.Empty;")
                    //                            {
                    //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                    //                            }
                    //                            else
                    //                                if (line.Trim() == "private string tpID = string.Empty;")
                    //                                {
                    //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                    //                                }
                    //                                else
                    //                                    if (line.Trim() == "private string mailID = string.Empty;")
                    //                                    {
                    //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                    //                                    }
                    //                                    else
                    //                                        if (line.Trim() == "private string eBayType = string.Empty;")
                    //                                        {
                    //                                            sb.AppendLine("private string eBayType = \"" + eBayType + "\";");
                    //                                        }
                    //                                    else
                    //                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                    //                                        {
                    //                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                    //                                        }
                    //                                        else
                    //                                            if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                    //                                            {
                    //                                                sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                    //                                            }
                    //                                            else
                    //                                                if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                    //                                                {
                    //                                                    sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                    //                                                }
                    //                                                else
                    //                                                    if (line.Trim() == "string eBayToken = string.Empty;")
                    //                                                    {
                    //                                                        sb.AppendLine("string eBayToken = @\"" + eBayToken + "\";");
                    //                                                    }
                    //                                                    else
                    //                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                    //                                                        {
                    //                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                    //                                                        }
                    //                                                        else
                    //                                                            sb.AppendLine(line);
                    //                        }

                    //                    }

                    //                }
                    //                using (StreamWriter strw = new StreamWriter(destName))
                    //                {
                    //                    strw.Write(sb.ToString());
                    //                }


                    //                FileInfo sourceFile = new FileInfo(destName);
                    //                CodeDomProvider provider = null;
                    //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                    //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                    //                //// Select the code provider based on the input file extension.
                    //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                    //                {
                    //                    provider = CodeDomProvider.CreateProvider("CSharp");
                    //                }


                    //                if (provider != null)
                    //                {

                    //                    // Format the executable file name.
                    //                    // Build the output assembly path using the current directory
                    //                    // and <source>_cs.exe or <source>_vb.exe.

                    //                    exeName = String.Format(@"{0}\{1}.exe",
                    //                        System.Environment.CurrentDirectory,
                    //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text + "_Edited" + DateTime.Now.Second.ToString()));

                    //                    CompilerParameters cp = new CompilerParameters();
                    //                    cp.ReferencedAssemblies.Add("System.dll");
                    //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                    //                    //cp.ReferencedAssemblies.Add("System.Net.dll");
                    //                    //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                    //                    //Microsoft.Office.Interop.Excel
                    //                    // Generate an executable instead of 
                    //                    // a class library.
                    //                    cp.GenerateExecutable = true;

                    //                    // Specify the assembly file name to generate.
                    //                    cp.OutputAssembly = exeName;

                    //                    // Save the assembly as a physical file.
                    //                    cp.GenerateInMemory = false;

                    //                    // Set whether to treat all warnings as errors.
                    //                    cp.TreatWarningsAsErrors = false;

                    //                    // Invoke compilation of the source file.
                    //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                    //                        destName);

                    //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                    //                    if (cr.Errors.Count > 0)
                    //                    {

                    //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //                        return;
                    //                    }

                    //                }

                    //                #endregion
                    //            }
                    //            else
                    //            {
                    //                #region Convert OSCommerce class to exe

                    //                string dbHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCWebStoreUrl"].ToString();
                    //                string dbUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["UserName"].ToString();
                    //                string dbPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["Password"].ToString();
                    //                if (dbPassword.Contains(";"))
                    //                    dbPassword = "'" + dbPassword + "'";
                    //                string dbName = dsTradingPartnerDetails.Tables[0].Rows[0]["OSCDatabase"].ToString();
                    //                string dbPrefix = dsTradingPartnerDetails.Tables[0].Rows[0]["XSDLocation"].ToString();
                              
                    //                string dbPort = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAuthToken"].ToString();
                    //                string sshHostName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayItemMapping"].ToString();
                    //                string sshPortNo = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayDevId"].ToString();
                    //                string sshUserName = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayCertID"].ToString();
                    //                string sshPassword = dsTradingPartnerDetails.Tables[0].Rows[0]["eBayAppId"].ToString();
                    //                if (sshPassword.Contains(";"))
                    //                    sshPassword = "'" + sshPassword + "'";

                    //                string mailProtocolID = dsTradingPartnerDetails.Tables[0].Rows[0]["MailProtocolID"].ToString();

                    //                string tradingPartnerID = dsTradingPartnerDetails.Tables[0].Rows[0]["TradingPartnerID"].ToString();

                    //                string sourceName = Application.StartupPath + "\\OSCommerce.cs";

                    //                //Create log directory for storing Messages.
                    //                string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");

                    //                //Adding Directory separate character to Logdirectory file path.
                    //                if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    //                {
                    //                    logDirectory = Application.StartupPath + logDirectory;
                    //                    try
                    //                    {
                    //                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    //                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    //                        else
                    //                            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                    //                    }
                    //                    catch
                    //                    {
                    //                    }
                    //                }
                    //                //Checking if directory does not exists then create it.
                    //                if (!Directory.Exists(logDirectory))
                    //                {
                    //                    Directory.CreateDirectory(logDirectory);
                    //                }
                    //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog " + textBoxServiceName.Text + ".txt";
                    //                //string logFileName = Application.StartupPath + "\\LOG\\AxisOSCommerceServiceLog\\" + textBoxServiceName.Text + ".txt";
                    //                string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                    //                try
                    //                {
                    //                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    //                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    //                    else
                    //                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                    //                }
                    //                catch
                    //                {
                    //                }
                    //                string destName = logDirectory + "\\OSCommerce.cs";

                    //                string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new OSCommerce());  }";

                    //                string period = string.Empty;
                    //                switch (comboBoxPeriod.SelectedItem.ToString())
                    //                {
                    //                    case "1 minute":
                    //                        period = Convert.ToString(1 * 1000 * 60);
                    //                        break;
                    //                    case "5 minute":
                    //                        period = Convert.ToString(5 * 1000 * 60);
                    //                        break;
                    //                    case "10 minute":
                    //                        period = Convert.ToString(10 * 1000 * 60);
                    //                        break;
                    //                    case "15 minute":
                    //                        period = Convert.ToString(15 * 1000 * 60);
                    //                        break;
                    //                    case "30 minute":
                    //                        period = Convert.ToString(30 * 1000 * 60);
                    //                        break;
                    //                    case "45 minute":
                    //                        period = Convert.ToString(45 * 1000 * 60);
                    //                        break;
                    //                    case "1 hour":
                    //                        period = Convert.ToString(60 * 1000 * 60);
                    //                        break;
                    //                    case "2 hour":
                    //                        period = Convert.ToString(120 * 1000 * 60);
                    //                        break;
                    //                    case "5 hour":
                    //                        period = Convert.ToString(300 * 1000 * 60);
                    //                        break;
                    //                    case "10 hour":
                    //                        period = Convert.ToString(600 * 1000 * 60);
                    //                        break;
                    //                    case "12 hour":
                    //                        period = Convert.ToString(720 * 1000 * 60);
                    //                        break;
                    //                    default:
                    //                        period = Convert.ToString(10 * 1000 * 60);
                    //                        break;
                    //                }
                    //                //StreamReader strReader = new StreamReader(destName);
                    //                int lineCounter = 1;
                    //                StringBuilder sb = new StringBuilder();
                    //                //StreamWriter strWriter = new StreamWriter(destName, true);
                    //                string connectionString = string.Empty;
                    //                string file = string.Empty;
                    //                //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                    //                connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("MySQLConnectionString");
                    //                using (StreamReader sr = new StreamReader(sourceName))
                    //                {

                    //                    string line;

                    //                    while ((line = sr.ReadLine()) != null)
                    //                    {
                    //                        if (line.Trim() == "}")
                    //                        {
                    //                            if (lineCounter == 1)
                    //                            {
                    //                                sb.AppendLine(line);
                    //                                sb.AppendLine();
                    //                                sb.AppendLine(addData);
                    //                                sb.AppendLine();
                    //                                lineCounter = 0;
                    //                            }
                    //                            else
                    //                                sb.AppendLine(line);
                    //                            //break;

                    //                        }
                    //                        else
                    //                        {
                    //                            if (line.Trim() == "private string connectionString = string.Empty;")
                    //                            {
                    //                                sb.AppendLine("private string connectionString = \"" + connectionString + "\";");
                    //                            }
                    //                            else
                    //                                if (line.Trim() == "private string tpID = string.Empty;")
                    //                                {
                    //                                    sb.AppendLine("private string tpID = \"" + tradingPartnerID + "\";");
                    //                                }
                    //                                else
                    //                                    if (line.Trim() == "private string mailID = string.Empty;")
                    //                                    {
                    //                                        sb.AppendLine("private string mailID = \"" + mailProtocolID + "\";");
                    //                                    }
                    //                                    else
                    //                                        if (line.Trim() == "private string dbHostName = string.Empty;")
                    //                                        {
                    //                                            sb.AppendLine("private string dbHostName = \"" + dbHostName + "\";");
                    //                                        }
                    //                                        else
                    //                                            if (line.Trim() == "private string dbUserName = string.Empty;")
                    //                                            {
                    //                                                sb.AppendLine("private string dbUserName = \"" + dbUserName + "\";");
                    //                                            }
                    //                                            else
                    //                                                if (line.Trim() == "private string dbPassword = string.Empty;")
                    //                                                {
                    //                                                    sb.AppendLine("private string dbPassword = \"" + dbPassword + "\";");
                    //                                                }
                    //                                                else
                    //                                                    if (line.Trim() == "private string dbName = string.Empty;")
                    //                                                    {
                    //                                                        sb.AppendLine("private string dbName = \"" + dbName + "\";");
                    //                                                    }
                    //                                                    else
                    //                                                        if (line.Trim() == "private string dbPrefix = string.Empty;")
                    //                                                        {
                    //                                                            sb.AppendLine("private string dbPrefix = \"" + dbPrefix + "\";");
                    //                                                        }
                    //                                                    else
                    //                                                        if (line.Trim() == "this.ServiceName = string.Empty;")
                    //                                                        {
                    //                                                            sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                    //                                                        }
                    //                                                        else
                    //                                                            if (line.Trim() == "private string sshHostName = string.Empty;")
                    //                                                            {
                    //                                                                sb.AppendLine("private string sshHostName = \"" + sshHostName + "\";");

                    //                                                            }
                    //                                                            else
                    //                                                                if (line.Trim() == "private string sshPortNo = string.Empty;")
                    //                                                                {
                    //                                                                    sb.AppendLine("private string sshPortNo = \"" + sshPortNo + "\";");

                    //                                                                }
                    //                                                                else
                    //                                                                    if (line.Trim() == "private string sshUserName = string.Empty;")
                    //                                                                    {
                    //                                                                        sb.AppendLine("private string sshUserName = \"" + sshUserName + "\";");

                    //                                                                    }
                    //                                                                    else
                    //                                                                        if (line.Trim() == "private string sshPassword = string.Empty;")
                    //                                                                        {
                    //                                                                            sb.AppendLine("private string sshPassword = \"" + sshPassword + "\";");

                    //                                                                        }
                    //                                                                        else
                    //                                                                            if (line.Trim() == "private string dbPort = string.Empty;")
                    //                                                                            {
                    //                                                                                sb.AppendLine("private string dbPort = \"" + dbPort + "\";");

                    //                                                                            }
                    //                                                                            else
                    //                                                                                if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                    //                                                                                {
                    //                                                                                    sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                    //                                                                                }
                    //                                                                                else
                    //                                                                                    if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                    //                                                                                    {
                    //                                                                                        sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                    //                                                                                    }
                    //                                                                                    else

                    //                                                                                        if (line.Trim() == "string tradingPartnerName = string.Empty;")
                    //                                                                                        {
                    //                                                                                            sb.AppendLine("string tradingPartnerName = \"" + comboBoxTradingPartner.SelectedItem.ToString() + "\";");
                    //                                                                                        }
                    //                                                                                        else
                    //                                                                                            sb.AppendLine(line);
                    //                        }

                    //                    }

                    //                }
                    //                using (StreamWriter strw = new StreamWriter(destName))
                    //                {
                    //                    strw.Write(sb.ToString());
                    //                }


                    //                FileInfo sourceFile = new FileInfo(destName);
                    //                CodeDomProvider provider = null;
                    //                //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                    //                //ICodeCompiler provider = codeProvider.CreateCompiler();


                    //                //// Select the code provider based on the input file extension.
                    //                if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                    //                {
                    //                    provider = CodeDomProvider.CreateProvider("CSharp");
                    //                }


                    //                if (provider != null)
                    //                {

                    //                    // Format the executable file name.
                    //                    // Build the output assembly path using the current directory
                    //                    // and <source>_cs.exe or <source>_vb.exe.

                    //                    exeName = String.Format(@"{0}\{1}.exe",
                    //                        System.Environment.CurrentDirectory,
                    //                        sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text + "_Edited" + DateTime.Now.Second.ToString()));

                    //                    CompilerParameters cp = new CompilerParameters();
                    //                    cp.ReferencedAssemblies.Add("System.dll");
                    //                    cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Configuration.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Data.dll");
                    //                    cp.ReferencedAssemblies.Add("System.Xml.dll");
                    //                    cp.ReferencedAssemblies.Add("Tamir.SharpSSH.dll");
                    //                    cp.ReferencedAssemblies.Add("Org.Mentalis.Security.dll");
                    //                    cp.ReferencedAssemblies.Add("DiffieHellman.dll");
                    //                    cp.ReferencedAssemblies.Add("MySql.Data.dll");
                    //                    //Microsoft.Office.Interop.Excel
                    //                    // Generate an executable instead of 
                    //                    // a class library.
                    //                    cp.GenerateExecutable = true;

                    //                    // Specify the assembly file name to generate.
                    //                    cp.OutputAssembly = exeName;

                    //                    // Save the assembly as a physical file.
                    //                    cp.GenerateInMemory = false;

                    //                    // Set whether to treat all warnings as errors.
                    //                    cp.TreatWarningsAsErrors = false;

                    //                    // Invoke compilation of the source file.
                    //                    CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                    //                        destName);

                    //                    //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                    //                    if (cr.Errors.Count > 0)
                    //                    {

                    //                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG118"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //                        return;
                    //                    }



                    //                }

                    //                #endregion
                    //            }
                    //        }
                    //        else
                    //        {
                    //            MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //            return;
                    //        }

                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("Please check if Trading Partner exist & then try again", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //        return;
                    //    }


                    //}
                    //else
                    //{
                    #endregion
                    if (string.IsNullOrEmpty(textBoxFolderPath.Text.Trim()))
                        {
                            //Display message of blank folder path.
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG105"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            textBoxFolderPath.Focus();
                            return;
                        }

                        #region Convert class to exe

                        string sourceName = Application.StartupPath + "\\ServiceClass.cs";
                        //Create log directory for storing Messages.
                        string logDirectory = ConfigurationManager.AppSettings.Get("LOGS");
                        //Adding Directory separate character to Logdirectory file path.
                        if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                        {
                            logDirectory = Application.StartupPath + logDirectory;
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                    logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                                else
                                    logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                            }
                            catch
                            {
                            }
                        }
                        //Checking if directory does not exists then create it.
                        if (!Directory.Exists(logDirectory))
                        {
                            Directory.CreateDirectory(logDirectory);
                        }
                        //string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog " + textBoxServiceName.Text + ".txt";
                       // string logFileName = Application.StartupPath + "\\LOG\\AxisServiceLog\\" + textBoxServiceName.Text + ".txt";
                        string logFileName = logDirectory + "\\" + textBoxServiceName.Text + ".txt";

                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            else
                                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents"); 
                        }
                        catch
                        {
                        }

                        string destName = logDirectory + "\\ServiceClass.cs";
                        //File.Copy(sourceName, destName,true);

                        string addData = @"static void Main() { System.ServiceProcess.ServiceBase.Run(new ServiceClass());  }";

                        string period = string.Empty;
                        switch (comboBoxPeriod.SelectedItem.ToString())
                        {
                            case "1 minute":
                                period = Convert.ToString(1 * 1000 * 60);
                                break;
                            case "5 minute":
                                period = Convert.ToString(5 * 1000 * 60);
                                break;
                            case "10 minute":
                                period = Convert.ToString(10 * 1000 * 60);
                                break;
                            case "15 minute":
                                period = Convert.ToString(15 * 1000 * 60);
                                break;
                            case "30 minute":
                                period = Convert.ToString(30 * 1000 * 60);
                                break;
                            case "45 minute":
                                period = Convert.ToString(45 * 1000 * 60);
                                break;
                            case "1 hour":
                                period = Convert.ToString(60 * 1000 * 60);
                                break;
                            case "2 hour":
                                period = Convert.ToString(120 * 1000 * 60);
                                break;
                            case "5 hour":
                                period = Convert.ToString(300 * 1000 * 60);
                                break;
                            case "10 hour":
                                period = Convert.ToString(600 * 1000 * 60);
                                break;
                            case "12 hour":
                                period = Convert.ToString(720 * 1000 * 60);
                                break;
                            default:
                                period = Convert.ToString(10 * 1000 * 60);
                                break;
                        }
                        //StreamReader strReader = new StreamReader(destName);
                        int lineCounter = 1;
                        StringBuilder sb = new StringBuilder();
                        //StreamWriter strWriter = new StreamWriter(destName, true);
                        string connectionString = string.Empty;
                        string file = string.Empty;
                        //connectionString = CommonUtilities.GetAppSettingValue("MySQLConnectionString");
                        connectionString = TransactionImporter.TransactionImporter.GetConnectionValueFromReg("NewMySQLConnection");
                        using (StreamReader sr = new StreamReader(sourceName))
                        {

                            string line;

                            while ((line = sr.ReadLine()) != null)
                            {
                                if (line.Trim() == "}")
                                {
                                    if (lineCounter == 1)
                                    {
                                        sb.AppendLine(line);
                                        sb.AppendLine();
                                        sb.AppendLine(addData);
                                        sb.AppendLine();
                                        lineCounter = 0;
                                    }
                                    else
                                        sb.AppendLine(line);
                                    //break;

                                }
                                else
                                {
                                    if (line.Trim() == "this.ServiceName = string.Empty;")
                                    {
                                        sb.AppendLine("this.ServiceName = \"" + textBoxServiceName.Text + "\";");

                                    }
                                    else
                                        if (line.Trim() == "monitorTimer = new Timer(timerdelegates, null, 1000, 2000);")
                                        {
                                            sb.AppendLine("monitorTimer = new Timer(timerdelegates, null, 1000, " + period + ");");
                                            //sb.AppendLine("monitorTimer.Interval = " + period + ";");
                                        }
                                        else

                                            if (line.Trim() == "string directoryPath = string.Empty;")
                                            {
                                                sb.AppendLine("string directoryPath = @\"" + textBoxFolderPath.Text + "\";");
                                            }
                                            else
                                                if (line.Trim() == "con.ConnectionString = string.Empty;")
                                                {
                                                    sb.AppendLine("con.ConnectionString = @\"" + connectionString + "\";");
                                                }
                                                else
                                                    if (line.Trim() == "flinfo.CopyTo(string.Empty, true);")
                                                    {
                                                        string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                                        try
                                                        {
                                                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                                destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                            else
                                                                destPath = destPath.Replace("Program Files", "Users\\Public\\Documents"); 
                                                        }
                                                        catch
                                                        {
                                                        }
                                                        sb.AppendLine("flinfo.CopyTo(@" + destPath + ", true);");
                                                    }
                                                    else
                                                        if (line.Trim() == "FileStream fs = new FileStream(string.Empty, FileMode.OpenOrCreate, FileAccess.Write);")
                                                        {
                                                            sb.AppendLine("FileStream fs = new FileStream(@\"" + logFileName + "\", FileMode.OpenOrCreate, FileAccess.Write);");
                                                        }
                                                        else
                                                            if (line.Trim() == "csvfl.CopyTo(string.Empty, true);")
                                                            {
                                                                string destPath = "\"" + Application.StartupPath + "\\LOG\\\"+Attachname";
                                                                try
                                                                {
                                                                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                                                        destPath = destPath.Replace("Program Files", Constants.xpPath);
                                                                    else
                                                                        destPath = destPath.Replace("Program Files", "Users\\Public\\Documents"); 
                                                                }
                                                                catch
                                                                {
                                                                }
                                                                sb.AppendLine("csvfl.CopyTo(@" + destPath + ", true);");
                                                            }
                                                            else
                                                                if (line.Trim() == "string serviceText = string.Empty;")
                                                                {
                                                                    sb.AppendLine("string serviceText = \"" + textBoxServiceName.Text + "\";");
                                                                }
                                                                else
                                                                    sb.AppendLine(line);
                                }

                            }

                        }
                        using (StreamWriter strw = new StreamWriter(destName))
                        {
                            strw.Write(sb.ToString());
                        }


                        FileInfo sourceFile = new FileInfo(destName);
                        CodeDomProvider provider = null;
                        //CSharpCodeProvider codeProvider = new CSharpCodeProvider();
                        //ICodeCompiler provider = codeProvider.CreateCompiler();


                        //// Select the code provider based on the input file extension.
                        if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                        {
                            provider = CodeDomProvider.CreateProvider("CSharp");
                        }

                        exeName = string.Empty;
                        if (provider != null)
                        {

                            // Format the executable file name.
                            // Build the output assembly path using the current directory
                            // and <source>_cs.exe or <source>_vb.exe.

                            exeName = String.Format(@"{0}\{1}.exe",
                                System.Environment.CurrentDirectory,
                                sourceFile.Name.Replace(".", "_" + textBoxServiceName.Text));

                            CompilerParameters cp = new CompilerParameters();
                            cp.ReferencedAssemblies.Add("System.dll");
                            cp.ReferencedAssemblies.Add("System.ServiceProcess.dll");
                            cp.ReferencedAssemblies.Add("System.Configuration.Install.dll");
                            cp.ReferencedAssemblies.Add("System.Configuration.dll");
                            cp.ReferencedAssemblies.Add("System.Data.dll");
                            cp.ReferencedAssemblies.Add("System.Xml.dll");
                            cp.ReferencedAssemblies.Add("MySql.Data.dll");
                            //cp.ReferencedAssemblies.Add("Microsoft.Office.Interop.Excel.dll");
                            // Generate an executable instead of 
                            // a class library.
                            cp.GenerateExecutable = true;

                            // Specify the assembly file name to generate.
                            cp.OutputAssembly = exeName;

                            // Save the assembly as a physical file.
                            cp.GenerateInMemory = false;

                            // Set whether to treat all warnings as errors.
                            cp.TreatWarningsAsErrors = false;


                            // Invoke compilation of the source file.
                            CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                                destName);

                            //CompilerResults cr = provider.CompileAssemblyFromSource(cp, sb);

                            if (cr.Errors.Count > 0)
                            {

                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG115"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }



                        }

                        #endregion
                    

                    SetupMonitoringService.GetInstance().ServiceName = string.Empty;

                    #region Installing Service into System

                    //Adding Service Process Installer to class
                    ServiceProcessInstaller ProcesServiceInstaller = new ServiceProcessInstaller();
                    ProcesServiceInstaller.Account = ServiceAccount.LocalSystem;
                    ProcesServiceInstaller.Username = null;
                    ProcesServiceInstaller.Password = null;
                    //Creating Service installer object for service.
                    ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                    InstallContext Context = new System.Configuration.Install.InstallContext();
                    string svcApp = Application.StartupPath;
                    
                    //Assinging path to Service class
                    String path = String.Format("/assemblypath={0}", exeName);
                    String[] cmdline = { path };
                    //Adding context into service.
                    Context = new System.Configuration.Install.InstallContext("", cmdline);
                    ServiceInstallerObj.Context = Context;
                    //Assinging display name to Service installer.
                    ServiceInstallerObj.DisplayName = "Zed Axis Service " + textBoxServiceName.Text;
                    ServiceInstallerObj.Description = "Zed Axis Message Monitoring Service";
                    ServiceInstallerObj.ServiceName = textBoxServiceName.Text;
                    if (checkBoxStatus.Checked)
                        ServiceInstallerObj.StartType = ServiceStartMode.Automatic;
                    else
                        ServiceInstallerObj.StartType = ServiceStartMode.Manual;
                    ServiceInstallerObj.Parent = ProcesServiceInstaller;
                                        

                    System.Collections.Specialized.ListDictionary state = new System.Collections.Specialized.ListDictionary();
                    try
                    {
                        //Installing Service into Local Machine
                        ServiceInstallerObj.Install(state);

                        //Success message for creating service.
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG116"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ServiceController sc = new ServiceController(textBoxServiceName.Text, System.Environment.MachineName);
                        if (!checkBoxStatus.Checked)
                        {
                            //If status is inactive then service has been stopped.

                            try
                            {
                                if (sc.Status == ServiceControllerStatus.Running)
                                {
                                    //Service stopped.
                                    sc.Stop();
                                    sc.WaitForStatus(ServiceControllerStatus.Stopped);
                                }
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                if (sc.Status != ServiceControllerStatus.Running)
                                {
                                    //Service running.
                                    sc.Start();
                                    sc.WaitForStatus(ServiceControllerStatus.Running);
                                }

                            }
                            catch
                            { }
                        }

                        #region Message Monitoring Service details into Database

                        int status = 0;
                        if (checkBoxStatus.Checked)
                            status = 1;
                        string insertQuery = string.Empty;


                        //if (comboBoxTradingPartner.SelectedIndex > 0)

                        //    //Generate the MYSQL Query for update Message Monitoring data.
                        //    insertQuery = string.Format(SQLQueries.Queries.SQMS28, textBoxServiceName.Text, comboBoxTradingPartner.SelectedItem.ToString(), comboBoxPeriod.SelectedItem.ToString(), status.ToString());
                        //else
                            //Generate the MYSQL Query for update Message Monitoring data.
                            insertQuery = string.Format(SQLQueries.Queries.SQMS28, textBoxServiceName.Text, textBoxFolderPath.Text, comboBoxPeriod.SelectedItem.ToString(), status.ToString());
                        ////Generate the MYSQL Query for insert Message Monitoring data.
                        //insertQuery = string.Format(SQLQueries.Queries.SQMS28, textBoxServiceName.Text, textBoxFolderPath.Text, comboBoxPeriod.SelectedItem.ToString(), status.ToString());

                        insertQuery = insertQuery.Replace("\\", "//");

                        //Performs the insert query into database.
                        DBConnection.MySqlDataAcess.ExecuteNonQuery(insertQuery);

                        textBoxServiceName.Text = string.Empty;
                        textBoxFolderPath.Text = string.Empty;
                        comboBoxPeriod.SelectedIndex = 0;

                        #endregion

                        #region Refreshing Data
                        try
                        {
                            DataSet dsServiceData = MySqlDataAcess.ExecuteDataset(SQLQueries.Queries.SQMS33);
                            DataTable dsCloneData = new DataTable();
                            if (dsServiceData != null)
                            {
                                DataColumn dcMonitorID = new DataColumn("MonitorId");
                                DataColumn dcName = new DataColumn("Name");
                                DataColumn dcFolder = new DataColumn("Folder");
                                DataColumn dcPeriod = new DataColumn("Period");
                                DataColumn dcIsActive = new DataColumn("IsActive");
                                dsCloneData.Columns.Add(dcMonitorID);
                                dsCloneData.Columns.Add(dcName);
                                dsCloneData.Columns.Add(dcFolder);
                                dsCloneData.Columns.Add(dcPeriod);
                                dsCloneData.Columns.Add(dcIsActive);

                                //Checking dataset rows count.
                                if (dsServiceData.Tables[0].Rows.Count != 0)
                                {
                                    object[] objArray = new object[5];
                                    foreach (DataRow dr in dsServiceData.Tables[0].Rows)
                                    {
                                        objArray[0] = (object)dr[0].ToString();
                                        objArray[1] = (object)dr[1].ToString();
                                        objArray[2] = (object)dr[2].ToString();
                                        objArray[3] = (object)dr[3].ToString();

                                        if (dr["IsActive"].ToString() == "0")
                                            objArray[4] = (object)"InActive";
                                        else
                                            objArray[4] = (object)"Active";

                                        dsCloneData.LoadDataRow(objArray, true);

                                    }
                                    dsCloneData.AcceptChanges();
                                    MessageMonitoring frmMessageMonitoring = (MessageMonitoring)Application.OpenForms["MessageMonitoring"];

                                    DataGridView dgvServices = (DataGridView)frmMessageMonitoring.Controls["dataGridViewMessageServices"];

                                    Button btnDelete = (Button)frmMessageMonitoring.Controls["buttonDelete"];
                                    btnDelete.Enabled = true;

                                    Button btnEdit = (Button)frmMessageMonitoring.Controls["buttonEdit"];
                                    btnEdit.Enabled = true;

                                    dgvServices.DataSource = dsCloneData;
                                    dgvServices.Columns[0].Visible = false;
                                    dsServiceData = null;
                                }
                            }
                        }
                        catch { }
                        #endregion

                        return;
                    }
                    catch (SecurityException se)
                    {
                        //Log the security exceptions for unsufficient privileges.
                        CommonUtilities.WriteErrorLog(se.Message);
                        CommonUtilities.WriteErrorLog(se.Source);
                        //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG117"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    catch (Exception se)
                    {
                        //Log the exceptions for sufficient privileges.
                        CommonUtilities.WriteErrorLog(se.Message);
                        CommonUtilities.WriteErrorLog(se.Source);
                        //MessageBox.Show(se.Message.ToString() + "\n StackTrace :" + se.StackTrace);
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG117"), Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    #endregion

                    #endregion

                }
                catch (Exception ex)
                {
                    //Catching the exception.
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                #endregion

                #endregion

            }


            #endregion


        }

        /// <summary>
        /// This event is used for browse specific folder path to
        /// monitoring files which can contains text files or
        /// spreadsheets. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            
            DialogResult key = this.folderBrowserDialogService.ShowDialog();
            if (key == DialogResult.OK)
            {
                string logDirectoryPath = string.Empty;
                logDirectoryPath = Application.StartupPath + "\\LOG";
                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        logDirectoryPath = logDirectoryPath.Replace("Program Files", Constants.xpPath);
                    else
                        logDirectoryPath = logDirectoryPath.Replace("Program Files", "Users\\Public\\Documents"); 
                }
                catch
                {
                }
                //Checking folder path to log Directory.
                if (this.folderBrowserDialogService.SelectedPath == logDirectoryPath)
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG108"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                    //Assinging folder path to textbox.
                    this.textBoxFolderPath.Text = this.folderBrowserDialogService.SelectedPath;

            }
        }

        /// <summary>
        /// This method is used for not allowing spaces into service name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxServiceName_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = " ";
            e.Handled = str.Contains(e.KeyChar.ToString());
        }
        
        /// <summary>
        /// This method is used for remove services from System.
        /// </summary>
        /// <param name="commandName">Passing Service Controller command</param>
        /// <returns></returns>
        protected string RemoveMonitoringServices(string commandName)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + commandName);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                //Returning result.
                return result;
            }
            catch (Exception objException)
            {
                // Log the exception
                CommonUtilities.WriteErrorLog(objException.Message);
                CommonUtilities.WriteErrorLog(objException.StackTrace);
                return string.Empty;
            }

        }

        private void comboBoxTradingPartner_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (comboBoxTradingPartner.SelectedIndex > 0)
            //{
            //    labelFolderMessage.Enabled = false;
            //    textBoxFolderPath.Enabled = false;
            //    buttonBrowse.Enabled = false;
            //    labelCondition.Text = "Check the eBay OR OSCommerce Transactions every..";
            //    AddTransactionFolderPeriod();

            //}
            //else
            //{
                labelFolderMessage.Enabled = true;
                textBoxFolderPath.Enabled = true;
                buttonBrowse.Enabled = true;
                labelCondition.Text = "Check the folder every..";
                AddFolderPeriod();
            //}
        }

        /// <summary>
        /// This method is used for adding period to watching files.
        /// </summary>
        private void AddFolderPeriod()
        {
            try
            {
                comboBoxPeriod.Items.Clear();
                object[] objItems = new object[12] { "< Select >", "1 minute", "5 minute", "10 minute", "15 minute", "30 minute", "45 minute", "1 hour", "2 hour", "5 hour", "10 hour", "12 hour" };
                comboBoxPeriod.Items.AddRange(objItems);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        /// <summary>
        /// This method is used for add time period for eBay,OSCommerce.
        /// </summary>
        private void AddTransactionFolderPeriod()
        {
            try
            {
                comboBoxPeriod.Items.Clear();
                object[] objItems = new object[11] { "< Select >", "5 minute", "10 minute", "15 minute", "30 minute", "45 minute", "1 hour", "2 hour", "5 hour", "10 hour", "12 hour" };
                comboBoxPeriod.Items.AddRange(objItems);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        private void ClearForm()
        {
            textBoxFolderPath.Text = string.Empty;
            textBoxServiceName.Text = string.Empty;
           // comboBoxTradingPartner.SelectedIndex = -1;
            comboBoxPeriod.SelectedIndex = -1;
        }

        #endregion
    }
}