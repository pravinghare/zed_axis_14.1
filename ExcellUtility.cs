using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.Linq;
using System.Data;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EDI.Constant;
using System.Xml;
using TransactionImporter;
using System.Diagnostics;
using Tamir.SharpSsh.java.lang;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics.Contracts;
using LumenWorks.Framework.IO.Csv;
using GemBox.Spreadsheet;
using System.Collections;
using LinqToSqlShared;
using CsvFile;
using System.Linq;


namespace DataProcessingBlocks
{
    /// <summary>
    /// This Class provides the utility to operate on Excel sheets
    /// </summary>
    public class ExcellUtility
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        #region Private Constants
        private const string connectionStringFormat = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0}; Extended Properties='Excel 8.0; IMEX=1; HDR={1}'";
        //private const string connectionStringFormat = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0}; Extended Properties=Excel 8.0;";
        private const string connectionStringFormatXLSX = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties='Excel 12.0 Xml; IMEX=1; HDR={1}'";
        private const string connectionStringXLS = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties='Excel 12.0; IMEX=1; HDR={1}'";
        private const string connectionStringCSV = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0};Extended Properties=""text;HDR={1};FMT={2};""";
        private const string tableNames = "TABLE_NAME";
        private const string SELECTQUERY = "SELECT * FROM [{0}$]";
        private const string SELECTFILEQUERY = "SELECT * FROM [{0}]";
        private const string SELECTCOLUMNQUERY = "SELECT TOP 1 * FROM [{0}]";
        //private const string SELECTCOLUMNQUERY = "SELECT * FROM [{0}]";

        private const string COLUMNNAME = "COLUMN_NAME";
        private const string DEFUALTCOLNAME = "Column";
        private const string DefaultError = "The process cannot access the file";

        private const string ERROR_STRING = "The 'Microsoft.ACE.OLEDB.12.0' provider is not registered on the local machine";
        string ParentTag = string.Empty;
        List<string> xmllist = new List<string>();


        #endregion

        #region Private Members
        private static ExcellUtility m_ExcellUtility;
        string str;
        private string m_filePath;
        private string m_CurrentSheetname;
        private bool m_HDR = false;
        private int m_Datanumber = 0;
        private System.Data.DataTable textDataFromSheet;
        List<object> result = new List<object>();
        private string m_FMT = string.Empty;
        public string Err = string.Empty;
        public static string strXlsSheet;


        // Axis 10 .0 
        XmlNodeList nodeList;

        //Axis 12.0
        protected char[] SpecialChars = new char[] { ',', '"', '\r', '\n' };

        // Indexes into SpecialChars for characters with specific meaning
        private const int DelimiterIndex = 0;
        private const int QuoteIndex = 1;

        /// <summary>
        /// Gets/sets the character used for column delimiters.
        /// </summary>
        public char Delimiter
        {
            get { return SpecialChars[DelimiterIndex]; }
            set { SpecialChars[DelimiterIndex] = value; }
        }

        /// <summary>
        /// Gets/sets the character used for column quotes.
        /// </summary>
        public char Quote
        {
            get { return SpecialChars[QuoteIndex]; }
            set { SpecialChars[QuoteIndex] = value; }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets Or Sets the file path on which operations to be done.
        /// </summary>
        public string FilePath
        {
            get { return this.m_filePath; }
            set { this.m_filePath = value; }
        }

        public string ERR
        {
            get { return this.Err; }
            set { this.Err = value; }
        }
        public bool HDR
        {
            get
            {
                return this.m_HDR;
            }
            set { this.m_HDR = value; }
        }

        public string FMT
        {
            get
            {
                return this.m_FMT;
            }
            set
            {
                this.m_FMT = value;
            }

        }

        public string CurrentSheetName
        {
            get
            {
                return this.m_CurrentSheetname;
            }
            set
            {
                this.m_CurrentSheetname = value;
            }
        }

        public int DataNumber
        {
            get
            {
                return this.m_Datanumber;
            }
            set
            {
                this.m_Datanumber = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// ParameterLess constructor for ExcellUtility
        /// </summary>
        public ExcellUtility()
        {

        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Get current instance fo Excell Utility Object.
        /// </summary>
        /// <returns></returns>
        public static ExcellUtility GetInstance()
        {
            if (m_ExcellUtility == null)
                m_ExcellUtility = new ExcellUtility();
            return m_ExcellUtility;
        }

        /// <summary>
        /// Creates new instance of Excell Utility. And makes this as current instance.
        /// </summary>
        /// <returns></returns>
        public static ExcellUtility CreateInstance()
        {
            m_ExcellUtility = new ExcellUtility();
            return m_ExcellUtility;
        }


        /// <summary>
        /// Crreates new instance of the ExcellUtitlity Object with the file path. And makes this as the current instance.
        /// </summary>
        /// <param name="filePath">file path of the excell file.</param>
        /// <returns></returns>
        public static ExcellUtility CreateInstance(string filePath)
        {
            m_ExcellUtility = new ExcellUtility(filePath);
            return m_ExcellUtility;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Excell utility with the file path.
        /// All the operations will be done on this file
        /// </summary>
        /// <param name="filePath">file Path</param>
        public ExcellUtility(string filePath)
        {

           str = filePath;

            this.m_filePath = filePath;
        }

        /// <summary>
        /// This mehtod retrieves the excel sheet names from 
        /// an excel workbook.
        /// </summary>
        /// <returns>String[]</returns>
        public System.Collections.ObjectModel.Collection<string> GetExcelSheetNames()
        {
            if ((System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".iif") && (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".txt"))
            {

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".xml")
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qif")
                    {
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qfx")
                        {
                            if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".ofx")
                            {
                                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qbo")
                                {                                 

                                        try
                                        {
                                            SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");
                                            ExcelFile ef = ExcelFile.Load(this.m_filePath);
                                            StringBuilder sb = new StringBuilder();

                                            System.Collections.ObjectModel.Collection<string> excelSheets = new System.Collections.ObjectModel.Collection<string>();

                                            foreach (ExcelWorksheet sheet in ef.Worksheets)
                                            {
                                                if (sheet.CalculateMaxUsedColumns() != 0)
                                                {

                                                    if (sheet.Name != null)
                                                    {

                                                        //if (!sheet.Name.ToString().Contains("#"))
                                                        {
                                                            if (!excelSheets.Contains(sheet.Name.ToString().Replace("'", string.Empty).Replace("$", string.Empty)))
                                                                sb.AppendFormat("{0}({1})", sheet.Name, sheet.Name.GetType().Name);
                                                            excelSheets.Add(sheet.Name.ToString().Replace("'", string.Empty).Replace("$", string.Empty));

                                                        }
                                                    }
                                                }

                                            }

                                            return excelSheets;


                                        }
                                        catch (OleDbException OleEx)
                                        {
                                            if (OleEx.Message.Contains(ERROR_STRING))
                                            {

                                                CommonUtilities.WriteErrorLog(OleEx.Message);
                                                CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                                throw new TIException("Zed Axis ER004");
                                            }
                                            else if (OleEx.ErrorCode == -2147467259)
                                            {
                                                CommonUtilities.WriteErrorLog(OleEx.Message);
                                                CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                                throw new TIException("Zed Axis ER003");


                                            }
                                            else
                                            {
                                                CommonUtilities.WriteErrorLog(OleEx.Message);
                                                CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                                throw new TIException("Zed Axis ER001");

                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            throw new TIException("Zed Axis ER001");

                                        }

                                    }
                                }
                            }
                        }
                    }               
            }
            return null;

        }

        /// <summary>
        /// Gets the data from the file by sheet name.
        /// </summary>
        /// <param name="sheetName">Name of the sheet from which data to be fetched</param>
        /// <returns></returns>
        public System.Data.DataTable GetDataBySheetName(string sheetName)
        {
            textDataFromSheet = null;
            int transCount = 0;
            int splCount = 0;
            int gemboxused = 0;
           // int transCount = 0;
            OleDbConnection oleDBConn = null;
            try
            {
                this.m_CurrentSheetname = sheetName;
                
               // string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
             
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                string exfile = this.m_filePath;
                #region IIF File Parsing
                //This is used for IIF file parsing logic. So that we can import those files.

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                    if (this.m_FMT != "Fixed")
                    {
                        textDataFromSheet = new System.Data.DataTable();
                        char[] delimiter = new char[1];
                        if (this.m_FMT == "TabDelimited")
                            delimiter[0] = '\t';
                        string[] lines = File.ReadAllLines(this.m_filePath);
                        string textHeaderData = string.Empty;

                        //List contains header data
                        List<string> header = new List<string>();

                        //List contains actual detail TRNS data
                        List<string> detail = new List<string>();

                        //List contains actual detail SPL data
                        List<string> detailSpl = new List<string>();

                        //List contains actual detail TIMEACT data
                        List<string> detailTimeact = new List<string>();


                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (!string.IsNullOrEmpty(lines[i].Trim()))
                            {

                                try
                                {
                                    if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("!SPL") || lines[i].StartsWith("!ENDTRNS"))
                                    {
                                        #region Header data of !TRNS and !SPL
                                        //Add header data of TRNS and SPL into List.
                                        string col = lines[i].ToString();
                                        string[] columnHead = col.Split(delimiter);
                                        foreach (string columnHeader in columnHead)
                                        {
                                            if (columnHeader.Trim().ToLower() != "!trns")
                                            {
                                                if (columnHeader.Trim().ToLower() != "!spl")
                                                {
                                                    if (columnHeader.Trim().ToLower() != "!endtrns")
                                                    {
                                                        if (columnHeader != "")
                                                        {
                                                            header.Add(columnHeader);
                                                            if (lines[i].StartsWith("!TRNS"))
                                                                transCount++;
                                                            if (lines[i].StartsWith("!SPL"))
                                                                splCount++;
                                                        }
                                                    }
                                                    else
                                                        break;
                                                }
                                            }
                                        }
                                        #endregion
                                    }

                                    else if (lines[i].StartsWith("TRNS"))
                                    {
                                        #region Actual TRNS data

                                        //Add Actual detail TRNS data into List.
                                        string col = lines[i].ToString();
                                        string[] columnDetail = col.Split(delimiter);
                                        Array.Resize(ref columnDetail, transCount+1);
                                        foreach (string columnDetData in columnDetail)
                                        {
                                            detail.Add(columnDetData);
                                        }
                                        #endregion
                                    }

                                    else if (lines[i].StartsWith("SPL") || lines[i].StartsWith("ENDTRNS"))
                                    {
                                        #region Actual SPL data

                                        //Add Actual detail SPL data into List.
                                        string col = lines[i].ToString();
                                        string[] columnDetailSPL = col.Split(delimiter);
                                        if (lines[i].StartsWith("SPL"))
                                        Array.Resize(ref columnDetailSPL, splCount + 1);
                                        foreach (string columnDetDataSPL in columnDetailSPL)
                                        {
                                            detailSpl.Add(columnDetDataSPL);
                                        }
                                        #endregion
                                    }

                                    else if (lines[i].StartsWith("!TIMEACT"))
                                    {
                                        #region Header data of !TIMEACT

                                        //Add header data of TIMEACT into List.
                                        string[] columnHead = lines[i].Split(delimiter);
                                        foreach (string columnHeader in columnHead)
                                        {
                                            if (columnHeader.Trim().ToLower() != "!timeact")
                                            {
                                                header.Add(columnHeader);
                                            }
                                        }
                                        #endregion
                                    }

                                    else if (lines[i].StartsWith("TIMEACT"))
                                    {
                                        #region Actual TIMEACT data

                                        //Add Actual detail TIMEACT data into List.
                                        string[] columnTimeact = lines[i].Split(delimiter);

                                        foreach (string columnTimeData in columnTimeact)
                                        {
                                            detailTimeact.Add(columnTimeData);
                                        }
                                        #endregion
                                    }
                                }
                                catch
                                {
                                }
                            }
                        }


                        #region Display Header data in preview grid in header line and actual detail data in grid.

                        #region Display Header Data in header line


                        foreach (string head in header)
                        {

                            try
                            {
                                textDataFromSheet.Columns.Add(head);
                            }
                            catch
                            {
                                //If any column have same name as previous any column then word "Line" will be appended to that column.
                                try
                                {
                                    textDataFromSheet.Columns.Add(head + "Line");
                                }
                                catch
                                {
                                    textDataFromSheet.Columns.Add();
                                }
                            }
                        }

                        #endregion


                        #region Display Actual detail data of TRNS and SPL
                        try
                        {
                            string actualData = string.Empty;
                            string actualDataSpl = string.Empty;
                            string actualSplEndtrns = string.Empty;

                            //Get TRNS detail data
                            foreach (string data in detail)
                            {
                                if (data.Equals("TRNS"))
                                {
                                    actualData += "\n";
                                }
                                actualData += data + "~";
                                actualData = actualData.Replace("\"", string.Empty);
                            }

                            //Get SPL detail data
                            foreach (string dataSpl in detailSpl)
                            {
                                if (dataSpl.Equals("SPL") || dataSpl.Equals("ENDTRNS"))
                                {
                                    actualDataSpl += "\n";
                                }
                                actualDataSpl += dataSpl + "~";
                                actualDataSpl = actualDataSpl.Replace("\"", string.Empty);
                            }

                            string FinalData = string.Empty;

                            string newTrns = string.Empty;
                            string newSpl = string.Empty;

                            for (int c = 0; c < actualDataSpl.Length; c++)
                            {
                                int indexSpl = actualDataSpl.IndexOf("SPL");

                                for (int t = 0; t < actualData.Length; t++)
                                {
                                    int indexTrns = actualData.IndexOf("TRNS");
                                    actualData = actualData.Substring(indexTrns);
                                    if (actualData.Contains("\n"))
                                    {
                                        int indexNewLineTrns = actualData.IndexOf("\n");

                                        //Get first TRNS line
                                        if (indexTrns >= 1)
                                            newTrns = actualData.Substring(indexTrns - 1, indexNewLineTrns);
                                        else
                                            newTrns = actualData.Substring(indexTrns, indexNewLineTrns);
                                    }
                                    else
                                        //If only one TRNS line present then assign actualData to newTrns.
                                        newTrns = actualData;
                                    if (actualDataSpl.Contains("ENDTRNS"))
                                    {

                                        int indexSPL = actualDataSpl.IndexOf("SPL");
                                        if (indexSpl < 0)
                                        {
                                            actualDataSpl = actualDataSpl.Remove(0, actualDataSpl.IndexOf("\n"));
                                        }
                                        else
                                        {
                                            actualDataSpl = actualDataSpl.Remove(0, indexSPL);



                                            int indexEndtrns = actualDataSpl.IndexOf("ENDTRNS");
                                            if (indexSpl > 0)
                                            {
                                                actualSplEndtrns = actualDataSpl.Substring(indexSpl - 1, indexEndtrns);
                                            }
                                            else
                                                actualSplEndtrns = actualDataSpl.Substring(indexSpl, indexEndtrns);
                                            // actualSplEndtrns = actualSplEndtrns.Remove(0, actualSplEndtrns.IndexOf("SPL"));
                                            // int start = actualSplEndtrns.IndexOf("SPL");


                                            for (int s = 0; s < actualSplEndtrns.Length; s++)//actualSplEndtrns.Length
                                            {
                                                int indexNewSpl = actualSplEndtrns.IndexOf("SPL");
                                                actualSplEndtrns = actualSplEndtrns.Substring(indexNewSpl);
                                                if (actualSplEndtrns.Contains("\n"))
                                                {
                                                    int indexNewLine = actualSplEndtrns.IndexOf("\n");
                                                    if (indexNewSpl >= 1)
                                                        newSpl = actualSplEndtrns.Substring(indexNewSpl - 1, indexNewLine);
                                                    else
                                                        newSpl = actualSplEndtrns.Substring(indexNewSpl, indexNewLine);
                                                }
                                                else
                                                    newSpl = actualSplEndtrns.ToString();
                                                //Append SPL data to TRNS data
                                                FinalData += newTrns + newSpl;

                                                FinalData = FinalData.Remove(FinalData.Length - 1);
                                                FinalData = FinalData.TrimEnd();

                                                //Remove TRNS, SPL, ENDTRNS words.
                                                FinalData = FinalData.Replace("TRNS~", string.Empty).Replace("SPL~", string.Empty).Replace("ENDTRNS~", string.Empty);
                                                         
                                                object[] textRowData = new object[header.ToArray().Length];
                                                textRowData = Regex.Split(FinalData, "~");
                                               
                                                #region Parsing Values

                                                //AMOUNT : negative to positive
                                                if (textDataFromSheet.Columns.Contains("AMOUNT"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("AMOUNT");
                                                    string amt = textRowData[index].ToString();
                                                    if (amt != string.Empty)
                                                    {
                                                        if (amt.StartsWith("-"))
                                                        {
                                                            textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);
                                                        }
                                                    }

                                                }

                                                //AMOUNTLine : negative to positive
                                                if (textDataFromSheet.Columns.Contains("AMOUNTLine"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("AMOUNTLine");
                                                    string amt = textRowData[index].ToString();
                                                    if (amt != string.Empty)
                                                    {
                                                        if (amt.StartsWith("-"))
                                                        {
                                                            textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);
                                                        }
                                                    }
                                                }

                                                //TOPRINT : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("TOPRINT"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("TOPRINT");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }

                                                }

                                                //TOPRINTLine : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("TOPRINTLine"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("TOPRINTLine");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }
                                                }

                                                //NAMEISTAXABLE : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("NAMEISTAXABLE"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("NAMEISTAXABLE");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }
                                                }

                                                //NAMEISTAXABLELine : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("NAMEISTAXABLELine"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("NAMEISTAXABLELine");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }
                                                }

                                                //TAXABLE : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("TAXABLE"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("TAXABLE");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }
                                                }

                                                //TAXABLELine : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("TAXABLELine"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("TAXABLELine");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }
                                                }

                                                //CLEAR : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("CLEAR"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("CLEAR");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }

                                                }

                                                //CLEARLine : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("CLEARLine"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("CLEARLine");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }

                                                }

                                                //PAID : Y = true, N = false
                                                if (textDataFromSheet.Columns.Contains("PAID"))
                                                {
                                                    int index = textDataFromSheet.Columns.IndexOf("PAID");
                                                    if (textRowData[index].ToString() != string.Empty)
                                                    {
                                                        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                                                    }

                                                }

                                                //DATE : MM/DD/YY to regional format
                                                if (textDataFromSheet.Columns.Contains("DATE"))
                                                {
                                                    DateTime NewDt = new DateTime();
                                                    int index = textDataFromSheet.Columns.IndexOf("DATE");
                                                    string dt = textRowData[index].ToString();
                                                    if (dt != string.Empty)
                                                    {
                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                        {
                                                            DateTime dttest = new DateTime();
                                                            bool IsValid = false;

                                                            try
                                                            {
                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                IsValid = true;
                                                            }
                                                            catch
                                                            {
                                                                IsValid = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            NewDt = Convert.ToDateTime(dt);
                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                        }
                                                        textRowData[index] = dt;
                                                    }

                                                }

                                                //DATELine : MM/DD/YY to regional format
                                                if (textDataFromSheet.Columns.Contains("DATELine"))
                                                {
                                                    DateTime NewDt = new DateTime();
                                                    int index = textDataFromSheet.Columns.IndexOf("DATELine");
                                                    string dt = textRowData[index].ToString();
                                                    if (dt != string.Empty)
                                                    {
                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                        {
                                                            DateTime dttest = new DateTime();
                                                            bool IsValid = false;

                                                            try
                                                            {
                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                IsValid = true;
                                                            }
                                                            catch
                                                            {
                                                                IsValid = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            NewDt = Convert.ToDateTime(dt);
                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                        }
                                                        textRowData[index] = dt;
                                                    }

                                                }


                                                //DUEDATE : MM/DD/YY to regional format
                                                if (textDataFromSheet.Columns.Contains("DUEDATE"))
                                                {
                                                    DateTime NewDt = new DateTime();
                                                    int index = textDataFromSheet.Columns.IndexOf("DUEDATE");
                                                    string dt = textRowData[index].ToString();
                                                    if (dt != string.Empty)
                                                    {
                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                        {
                                                            DateTime dttest = new DateTime();
                                                            bool IsValid = false;

                                                            try
                                                            {
                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                IsValid = true;
                                                            }
                                                            catch
                                                            {
                                                                IsValid = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            NewDt = Convert.ToDateTime(dt);
                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                        }
                                                        textRowData[index] = dt;
                                                    }
                                                }

                                                //SERVICEDATE : MM/DD/YY to regional format
                                                if (textDataFromSheet.Columns.Contains("SERVICEDATE"))
                                                {
                                                    DateTime NewDt = new DateTime();
                                                    int index = textDataFromSheet.Columns.IndexOf("SERVICEDATE");
                                                    string dt = textRowData[index].ToString();
                                                    if (dt != string.Empty)
                                                    {
                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                        {
                                                            DateTime dttest = new DateTime();
                                                            bool IsValid = false;

                                                            try
                                                            {
                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                IsValid = true;
                                                            }
                                                            catch
                                                            {
                                                                IsValid = false;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            NewDt = Convert.ToDateTime(dt);
                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                        }
                                                        textRowData[index] = dt;
                                                    }
                                                }



                                                #endregion

                                                //LoadDataRow A
                                                if (textRowData.Length > textDataFromSheet.Columns.Count)
                                                {
                                                    while (textRowData.Length != textDataFromSheet.Columns.Count)
                                                    {
                                                        //object[] textRowData1 = new object[header.ToArray().Length - 1
                                                      //  textRowData.Length = textRowData.Length - 1;
                                                        Array.Resize(ref textRowData, textDataFromSheet.Columns.Count);
                                                       
                                                    }
                                                    textDataFromSheet.LoadDataRow(textRowData, true);
                                                    
                                                }
                                                else 
                                                {
                                                    textDataFromSheet.LoadDataRow(textRowData, true);
                                                }

                                                FinalData = string.Empty;
                                                textRowData = null;
                                                //}
                                                actualSplEndtrns = actualSplEndtrns.Replace(newSpl, string.Empty);
                                                //actualSplEndtrns = actualSplEndtrns.Replace("\n", string.Empty);
                                                actualDataSpl = actualDataSpl.Remove(1, actualDataSpl.IndexOf("SPL"));
                                                int indexTile = actualDataSpl.IndexOf("~");
                                                actualDataSpl = actualDataSpl.Substring(indexTile + 1);
                                                // actualDataSpl = actualDataSpl.Replace(newSpl, string.Empty);
                                                //actualData = actualData.Replace(newTrns, string.Empty);

                                            }
                                        }

                                    }
                                    actualData = actualData.Replace(newTrns, string.Empty);
                                    int indexSPLTile = actualDataSpl.IndexOf("SPL");
                                    actualDataSpl = actualDataSpl.Substring(indexSPLTile);
                                }
                            }

                        #endregion


                        #region Display Actual detail data of TIMEACT

                            string actualTimeact = string.Empty;
                            string newTime = string.Empty;

                            //Get TIMEACT detail data
                            foreach (string dataTime in detailTimeact)
                            {
                                if (dataTime.Equals("TIMEACT"))
                                {
                                    actualTimeact += "\n";
                                }
                                actualTimeact += dataTime + "~";
                            }

                            for (int c = 0; c < actualTimeact.Length; c++)
                            {
                                int indexTime = actualTimeact.IndexOf("TIMEACT");

                                actualTimeact = actualTimeact.Substring(indexTime);
                                if (actualTimeact.Contains("\n"))
                                {
                                    int indexNewLine = actualTimeact.IndexOf("\n");

                                    newTime = actualTimeact.Substring(indexTime - 1, indexNewLine);
                                    FinalData += newTime;
                                    actualTimeact = actualTimeact.Replace(newTime, string.Empty);
                                }
                                else
                                {
                                    FinalData += actualTimeact;
                                    actualTimeact = string.Empty;
                                }
                                FinalData = FinalData.Remove(FinalData.Length - 1);

                                FinalData = FinalData.Replace("TIMEACT~", string.Empty);

                                object[] textRowData = Regex.Split(FinalData, "~");

                                #region Parsing logic for Date

                                //DATE : MM/DD/YY to regional format
                                if (textDataFromSheet.Columns.Contains("DATE"))
                                {
                                    DateTime NewDt = new DateTime();
                                    int index = textDataFromSheet.Columns.IndexOf("DATE");
                                    string dt = textRowData[index].ToString();
                                    if (dt != string.Empty)
                                    {
                                        if (!DateTime.TryParse(dt, out NewDt))
                                        {
                                            DateTime dttest = new DateTime();
                                            bool IsValid = false;

                                            try
                                            {
                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                IsValid = true;
                                            }
                                            catch
                                            {
                                                IsValid = false;
                                            }
                                        }
                                        else
                                        {
                                            NewDt = Convert.ToDateTime(dt);
                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                        }
                                        textRowData[index] = dt;
                                    }

                                }
                                #endregion

                                //LoadDataRow B
                                if (textRowData.Length > textDataFromSheet.Columns.Count)
                                {
                                    while (textRowData.Length != textDataFromSheet.Columns.Count)
                                    {
                                        //object[] textRowData1 = new object[header.ToArray().Length - 1
                                        //  textRowData.Length = textRowData.Length - 1;
                                        Array.Resize(ref textRowData, textDataFromSheet.Columns.Count);

                                    }
                                    textDataFromSheet.LoadDataRow(textRowData, true);

                                }
                                else
                                {
                                    textDataFromSheet.LoadDataRow(textRowData, true);
                                }

                                FinalData = string.Empty;
                                textRowData = null;
                            }

                        }
                        catch 
                        {
                           
                        }
                            #endregion

                        #endregion

                        return textDataFromSheet;
                    }
                    else
                    {
                        // Create connection object by using the preceding connection string.
                        oleDBConn = new OleDbConnection(connString);
                        // Open connection with the database.
                        oleDBConn.Open();

                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);

                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                        }
                        System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                        oleDBAdapter.Fill(excellDataFromSheet);


                        return excellDataFromSheet;
                    }
                    //return textDataFromSheet;

                }
                #endregion                

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                {
                    // List<object> result = new List<object>();
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                    if (this.m_FMT != "Fixed")
                    {
                        textDataFromSheet = new System.Data.DataTable();

                        char[] delimiter = new char[1];
                        if (this.m_FMT == "TabDelimited")
                            delimiter[0] = '\t';
                        if (this.m_FMT == "CSVDelimited")
                            delimiter[0] = ',';
                        if (this.m_FMT == "Delimited(|)")
                            delimiter[0] = '|';


                        string strsymbol = "";
                        string[] alllines = File.ReadAllLines(this.m_filePath);
                        List<string> tmp = new List<string>(alllines);

                        if (tmp.Contains(strsymbol))
                        {
                            string newline = strsymbol;
                            int numIdx = Array.IndexOf(alllines, newline);
                            tmp.RemoveAt(numIdx);
                            alllines = tmp.ToArray();
                        }
                        string tempData = string.Empty;

                        for (int cnt = 0; cnt < alllines.Length; cnt++)
                        {
                            char[] data = new char[alllines[cnt].Length];
                            data = alllines[cnt].ToCharArray();
                            int count = 0;

                            if (data.Length > 0)
                            {

                                for (int x = 0; x < data.Length; x++)
                                {
                                    if (data[x] == '"')
                                    {
                                        count++;
                                    }
                                }
                                //}
                                if (count % 2 == 0)
                                {
                                    tempData += alllines[cnt] + "~";
                                }
                                else
                                {
                                    tempData += alllines[cnt++] + alllines[cnt] + "~";
                                }
                            }
                        }

                        char[] symbol = new char[1];
                        symbol[0] = '~';
                        string[] lines = tempData.Split(symbol);
                        var col_header = new string[] { };
                       
                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (i == 0)
                            {
                                col_header = lines[i].Split(delimiter);
                                for (int p = 0; p < col_header.Length; p++)
                                {
                                    int no = 2;
                                    string temp_str = col_header[p];
                                    for (int q = p+1; q < col_header.Length; q++)
                                    {
                                        if (temp_str == col_header[q])
                                        {
                                            col_header[q] = temp_str +" ("+ no +")" ;
                                            no++;

                                        }

                                    }

                                }
                            }
                        }
                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (i == 0)
                            {
                                
                                //    string[] columns = lines[i].Split(delimiter);
                                //    columnCount = columns.Length;
                                foreach (string column in col_header)
                                {
                                    //try
                                    //{
                                        //if (this.m_HDR)
                                            textDataFromSheet.Columns.Add(column);
                                    //    else
                                    //        textDataFromSheet.Columns.Add();
                                    //}
                                    //catch
                                    //{
                                    //    try
                                    //    {
                                    //        textDataFromSheet.Columns.Add();
                                    //    }
                                    //    catch
                                    //    {
                                    //        textDataFromSheet.Columns.Add();
                                    //    }
                                    //}
                                }
                                if (this.m_HDR)
                                    continue;
                            }
                            #region String Parser Logic

                            //Converts all rows into Object array
                            //This is String parser logic for handling invalid files.
                            if (lines[i] != string.Empty)
                            {
                                //for (int k = 1; k < lines[i]-1; k++)

                                //{
                                string str = lines[i].Replace("\"\"", "^");
                                string middleStr = "";


                                if (str.Contains("\""))
                                {
                                    
                                    int index = str.IndexOf("\"") + 1;
                                    int indexaa = str.IndexOf("\",");                                   

                                    if (middleStr == "")
                                    {
                                    }
                                    else
                                    {
                                        middleStr = str.Substring(index, indexaa - index);                                       
                                        str = str.Replace(middleStr, "~");

                                        for (int m = 0; m < middleStr.Length; m++)
                                        {
                                            middleStr = middleStr.Replace("^", "\"");
                                        }
                                    }
                                }

                                //Axis 12 bug 510
                                string[] array = (string[])str.Split(delimiter);
                                string strng = "";
                                char[] chrarray = strng.ToCharArray();
                                String str2 = "";
                                int lengtharr = array.Length;
                                string[] newarray = new string[lengtharr];
                                for (int j = 0; j < array.Length; j++)
                                {
                                    if (array[j].Contains("~"))
                                    {
                                        array[j] = middleStr;
                                    }
                                    if (array[j].Equals("^") && array[j].Length == 1)
                                    {
                                        array[j] = "";
                                    }

                                    strng = array[j].ToString();
                                    chrarray = strng.ToCharArray();
                                    for (int z = 0; z < chrarray.Length; z++)
                                    {
                                        if (chrarray[z] == '"' || chrarray[z] == '\"')
                                        {
                                            chrarray[z] = ' ';

                                        }
                                        str2 = new String(chrarray);
                                    }

                                    newarray[j] = str2;
                                    str2 = string.Empty;
                                }

                               
                                int lengtharr1 = array.Length;

                                string[] objTextFile = new string[lengtharr1];
                                for (int temp = 0; temp < lengtharr1; temp++)
                                {                                  
                                    objTextFile[temp] = newarray[temp];
                                }                            
                               
                                    textDataFromSheet.LoadDataRow(objTextFile, true);    
                            }
                            #endregion
                        }

                        return textDataFromSheet;
                    }
                    else
                    {
                        // Create connection object by using the preceding connection string.
                        oleDBConn = new OleDbConnection(connString);
                        // Open connection with the database.
                        oleDBConn.Open();

                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);

                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                        }
                        System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                        oleDBAdapter.Fill(excellDataFromSheet);

                        return excellDataFromSheet;
                    }
                    //return textDataFromSheet;

                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                {
                    #region Axis 10.2 csv file parsing to get column headers and data
                    textDataFromSheet = new System.Data.DataTable();

                    //bug 505

                    //Cursor = Cursors.WaitCursor;
                    try
                    {
                        //dataGridView1.Rows.Clear();
                        List<string> columns = new List<string>();
                        int cnt = 0;
                        using (var reader = new CsvFileReader(this.m_filePath))
                        {
                            while (reader.ReadRow(columns))
                            {
                                if (cnt != 0)
                                {
                                    textDataFromSheet.LoadDataRow(columns.ToArray(), true);
                                }
                                else if (cnt == 0)
                                {
                                    var temp_header = columns.ToArray();
                                    for (int p = 0; p < temp_header.Length; p++)
                                    {
                                        int no = 2;
                                        string temp_str = temp_header[p];
                                        for (int q = p + 1; q < temp_header.Length; q++)
                                        {
                                            if (temp_str.Trim() == temp_header[q].Trim())
                                            {
                                                temp_header[q] = temp_str + " (" + no+")" ;
                                                no++;

                                            }

                                        }

                                    }

                                    for (int c = 0; c < temp_header.Length; c++)
                                    {
                                        textDataFromSheet.Columns.Add(temp_header[c]);
                                    }
                                }
                                cnt++;
                            }
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Error reading from {0}.\r\n\r\n{1}", this.m_filePath, ex.Message));
                    }
                    finally
                    {
                      
                    }
                    return textDataFromSheet;
                    #endregion
                }
                else
                {
                    //if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                    //{
                    

                    #region For Unformated Xls file
                   
                    System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();

                        gemboxused = 1;
                        SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");                       

                        try
                        {
                           // Axis 12.0 bug 510
                            FileInfo flinfo = new FileInfo(this.m_filePath);
                            string Flname = flinfo.Name;
                            flinfo.GetAccessControl();
                           
                            string extension = flinfo.Extension;
                            ExcelFile ef = ExcelFile.Load(this.m_filePath);
                            foreach (ExcelWorksheet sheet in ef.Worksheets)
                            {
                                if (sheet.Name == sheetName)
                                {
                                    int colcount = sheet.CalculateMaxUsedColumns();
                                    int usedCols = 0;
                                    if (extension.ToLower() == ".ods")
                                    {
                                        for (int i = 0; i < colcount; i++)
                                        {
                                            if (sheet.Rows[0].Cells[i].Value != null)
                                            { usedCols++; }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        usedCols = colcount; 
                                    }
                                    excellDataFromSheet = sheet.CreateDataTable(new CreateDataTableOptions()
                                     {
                                         ColumnHeaders = true,
                                         StartRow = 0,
                                        
                                         NumberOfColumns = usedCols,
                                         NumberOfRows = sheet.Rows.Count ,
                                         Resolution = ColumnTypeResolution.Auto,
                                         ExtractDataOptions = ExtractDataOptions.StopAtFirstEmptyRow,
                                     });
                                    //if (sheet.Rows[0].Cells[i].Value == null)
                                    //{
                                    //    excellDataFromSheet.Columns.RemoveAt(i);
                                    //}
                                    for (int i = 0; i < excellDataFromSheet.Columns.Count;i++ )
                                    {
                                        if (sheet.Rows[0].Cells[i].Value == null)
                                        {
                                            excellDataFromSheet.Columns.RemoveAt(i);
                                        }
                                        //var ws = ef.Worksheets[sheetName];
                                        //if ((ws.Cells[0, i]).Value == null)
                                        //{
                                        //    excellDataFromSheet.Columns.RemoveAt(i);
                                        //}
                                    }

                                    break;
                                }
                            }
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            string sheetnametest = System.IO.Path.GetFileName(this.m_filePath);
                        }
                        catch
                        {
                           
                        }
                    #endregion

                    return excellDataFromSheet;
                }

            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER003");

                }
                else
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER001");

                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains(DefaultError))
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    Err = DefaultError;
                    throw new TIException("Zed Axis MSG037", this.m_filePath);
                }
                else
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                    throw new TIException("Zed Axis ER001");
                }

            }
            finally
            {
               
                try
                {
                    if (oleDBConn != null)
                    {
                        if (oleDBConn.State.ToString() == "Open")
                        {
                            oleDBConn.Close();
                            oleDBConn.Dispose();
                        }
                    }
                }
                catch
                { }
            }
        }
        
        /// <summary>
        /// Gets the first row data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sheetIndex"></param>
        /// <returns></returns>
        public string[] GetFirstRow(string fileName, int sheetIndex)
        {
            string[] first_row = null;
            try
            {
                string extension = System.IO.Path.GetExtension(this.m_filePath).ToLower();
                //Axis 12.0  bug 510
                if (extension == ".xlsx" || extension == ".xls" || extension == ".xlsm" || extension == ".ods")
                {
                    // _Application xlApp;
                    // Workbook xlWorkBook;
                    //  Worksheet xlWorkSheet;
                    // Range used_range;
                    SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");
                    ExcelFile ef = new ExcelFile();
                    ef = ExcelFile.Load(this.FilePath);
                    var ws = ef.Worksheets[sheetIndex];
                    int columnCount = ws.CalculateMaxUsedColumns();

                    int usedCols = 0;
                    int row_no = 1, col_no = 0;
                    if (extension == ".ods")
                    {
                        for (int j = 0; j < columnCount; j++)
                        {
                            if (ws.Rows[0].Cells[j].Value != null)
                            { usedCols++; }
                            else
                            {
                                break;
                            }
                        }
                        columnCount = usedCols;
                    }
                  
                    int i = 0;
                    for (col_no = 0; col_no < columnCount; col_no++)
                    {
                        if ((ws.Cells[0, col_no]).Value != null)
                        {
                            usedCols++;
                        }
                    
                    }
                  //  columnCount = usedCols;
                    first_row = new string[usedCols];

                    for (col_no = 0; col_no < columnCount; col_no++)
                    {
                        if ((ws.Cells[0, col_no]).Value != null)
                        {
                            if ((ws.Cells[row_no, col_no]).Value != null)
                            {
                                first_row[i++] = (ws.Cells[row_no, col_no]).Value.ToString();
                            }
                            else
                            {
                                first_row[i++] = "";
                            }
                        }
                    }
                }
                else if (extension == ".txt")
                {
                    StreamReader sr_reader = new StreamReader(this.m_filePath);

                    string line = sr_reader.ReadLine();//This line contains the column names
                    line = sr_reader.ReadLine();//This line contains the first line of the file.

                    char[] delimiter = new char[1];
                    if (this.m_FMT == "TabDelimited")
                        delimiter[0] = '\t';
                    if (this.m_FMT == "CSVDelimited")
                        delimiter[0] = ',';
                    if (this.m_FMT == "Delimited(|)")
                        delimiter[0] = '|';

                    first_row = line.Split(delimiter);

                }
                else if (extension == ".iif")
                {
                    ArrayList arTables = new ArrayList();
                    int k = 0;
                    foreach (DataRow dr in this.textDataFromSheet.Rows)
                    {
                        for (int i = 0; i < dr.ItemArray.Length; i++)
                        {
                            arTables.Add(dr.ItemArray[i].ToString());
                        }
                        first_row = new string[arTables.Count];
                        for (int j = k; j < arTables.Count; j++)
                        {
                            if (arTables[j] != null)
                            {
                                first_row[k++] = arTables[j].ToString();
                            }
                            else
                            {
                                first_row[k++] = "";
                            }
                        }
                        break;
                    }

                }
            }
            catch(Exception e)
            { throw e; }
            return first_row;
        }


   
        /// <summary>
        /// Get name columns 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public System.Collections.ObjectModel.Collection<string> GetColumnNames(string sheetName)
        {
           int gembox = 0;
            OleDbConnection oleDBConn = null;
            try
            {
                this.m_CurrentSheetname = sheetName;
                // Connection String. Change the excel file to the file you
                // will search.

                string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                {
                    //connString = string.Format(connectionStringFormatXLSX, this.m_filePath, this.m_HDR ? "YES" : "NO");
                      connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                }
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    sheetName = System.IO.Path.GetFileName(this.m_filePath);                 
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                }
                else
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                    {
                        if (sheetName == string.Empty)
                            sheetName = this.CurrentSheetName;
                        if (sheetName.Contains("[") == false)
                        {
                            sheetName = "[" + sheetName + "$]";
                        }
                        else
                        {
                            sheetName += "$";
                        }
                    }
                }
                // Create connection object by using the preceding connection string.
                oleDBConn = new OleDbConnection(connString);
                // Open connection with the database.
                string counter = string.Empty;
                try
                {
                    oleDBConn.Open();
                }
                catch
                {
                    counter = "1";
                }

                #region For Unformated Xls file
                if (counter == "1")
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                    {

                        //Microsoft.Office.Interop.Excel.Application objExcel = new Microsoft.Office.Interop.Excel.Application();
                        try
                        {
                            string logDirectory = System.Windows.Forms.Application.StartupPath + "\\LOG";
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(EDI.Constant.Constants.xpOSName))
                                {
                                    if (logDirectory.Contains("Program Files (x86)"))
                                    {
                                        logDirectory = logDirectory.Replace("Program Files (x86)", EDI.Constant.Constants.xpPath);
                                    }
                                    else
                                    {
                                        logDirectory = logDirectory.Replace("Program Files", EDI.Constant.Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (logDirectory.Contains("Program Files (x86)"))
                                    {
                                        logDirectory = logDirectory.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }
                            }
                            catch { }

                            if (!System.IO.Directory.Exists(logDirectory))
                            {
                                System.IO.Directory.CreateDirectory(logDirectory);
                            }
                            FileInfo flinfo = new FileInfo(this.m_filePath);
                            string Flname = flinfo.Name;
                            string filenewpath = logDirectory + "\\" + Flname;
                            File.Copy(this.m_filePath, filenewpath, true);

                            // Microsoft.Office.Interop.Excel.Workbook objWorkBook = objExcel.Workbooks.Open(filenewpath, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                            ExcelFile ef = ExcelFile.Load(filenewpath);

                            string filePth = logDirectory + "\\test123456.xls";

                            try
                            {
                                if (File.Exists(filePth))
                                    File.Delete(filePth);                                
                                ef.Save(filePth);
                            }
                            catch
                            { }
                            
                            uint _ExcelPID = 0;                           
                            try
                            {
                                CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                            }
                            catch { }
                            try
                            {
                                //kill that process
                                Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                                excel.Kill();
                                excel.WaitForExit();
                            }
                            catch { }


                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            this.FilePath = filePth;
                            this.m_filePath = filePth;
                        }
                        catch
                        {

                            uint _ExcelPID = 0;                          

                            try
                            {
                                CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                            }
                            catch { }

                            try
                            {
                                //kill that process
                                Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                                excel.Kill();
                                excel.WaitForExit();
                            }
                            catch { }

                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                        }
                       // string connStringnew = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");                      
                        string connStringnew = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                        OleDbConnection oleDBConntest = new OleDbConnection(connStringnew);
                        // Open connection with the database.
                        if (oleDBConn.State.ToString() != "Open")
                        {
                            oleDBConn.Open();
                        }
                        string sheetnametest = this.CurrentSheetName;
                        if (sheetnametest.Contains("[") == false)
                        {
                            sheetnametest = "[" + sheetnametest + "$]";
                        }
                        else
                        {
                            sheetnametest += "$";
                        }
                       
                        if (sheetnametest.Contains("$"))
                        {
                            sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                        System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        if (dt.Rows.Count == 0)
                        {
                            if (sheetnametest.Contains("[") == false)
                            {
                                sheetnametest = "[" + sheetnametest + "$]";
                            }
                            else
                            {
                                sheetnametest += "$";
                            }
                            System.Data.DataTable dtt = new System.Data.DataTable();
                            if (sheetnametest.Contains("$"))
                            {
                                sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                            }

                            OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                            oleDBAdapter.Fill(dtt);
                            for (int i = 0; i < dtt.Columns.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName.Trim() : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME).Trim());
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString().Trim() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME).Trim());
                            }
                        }
                        oleDBConn.Close();
                        return columnNames;
                    }
                }
                #endregion
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    System.Data.DataTable dtGetTxtData = GetDataBySheetName(string.Empty);
                    DataColumnCollection dtColumns = dtGetTxtData.Columns;
                    System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    for (int j = 0; j < dtColumns.Count; j++)
                    {
                        dtTxtFile.Columns.Add(dtColumns[j].ColumnName);
                    }
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    for (int i = 0; i < dtTxtFile.Columns.Count; i++)
                    {
                        columnNames.Add(this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString().Trim() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME).Trim());
                    }
                    oleDBConn.Close();
                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                {
                    System.Data.DataTable dtGetTxtData = GetDataBySheetName(string.Empty);
                    DataColumnCollection dtColumns = dtGetTxtData.Columns;
                    System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    string[] exceldata = new string[100];

                    for (int j = 0; j < dtColumns.Count; j++)
                    {
                        dtTxtFile.Columns.Add(dtColumns[j].ColumnName);
                    }
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    for (int i = 0; i < dtTxtFile.Columns.Count; i++)
                    {

                        //exceldata[i] = this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME);
                        columnNames.Add(this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString().Trim() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME).Trim());
                    }

                    //for (int i = 0; i < exceldata.Length; i++)
                    //  {
                    //     int no = 1;
                    //        for (int j = 1; i < exceldata.Length; j++)
                    //        {
                                           
                    //         if (exceldata[i] == exceldata[j])
                    //             {
                                              
                    //               exceldata[j] = exceldata[i] + no;
                    //                 no++;

                    //             }
                                
                    //         }
                          
                    //   }
                    //for (int i = 0; i < exceldata.Length; i++)
                    //{
                    //    columnNames.Add(exceldata[i].ToString());
                    //}


                    oleDBConn.Close();
                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                {
                    //OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTCOLUMNQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                   // System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    //oleDBAdapter.Fill(dtTxtFile);                    
                    
                    #region For the bug 1502 (axis 6.0)
                   
                    #endregion
                    //bug 505
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    foreach (DataColumn col in textDataFromSheet.Columns)
                    {
                        columnNames.Add(col.ColumnName.Trim());
                    }

                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx"||System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                {                  
                      
                       try
                       {
                           gembox = 1;
                           #region commented code of xlsx file load , do not delete this code , bug no 399
                           SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");
                           ExcelFile ef1 = ExcelFile.Load(this.FilePath);

                           StringBuilder sb = new StringBuilder();
                           System.Collections.ObjectModel.Collection<string> excelSheets = new System.Collections.ObjectModel.Collection<string>();
                           //bug 505
                           int colcount=ef1.Worksheets[sheetName].CalculateMaxUsedColumns();
                           FileInfo flinfo = new FileInfo(this.m_filePath);
                           string Flname = flinfo.Name;
                           string extension = flinfo.Extension;
                         
                          // string celValue = "";
                           //excelSheets.Add(celValue);
                           foreach (ExcelWorksheet sheet in ef1.Worksheets)
                           {

                               if (sheet.Name == sheetName)
                               {
                                   int usedCols = 0;
                                   if (extension.ToLower() == ".ods")
                                   {
                                       for (int i = 0; i < colcount; i++)
                                       {
                                           if (sheet.Rows[0].Cells[i].Value != null)
                                           { usedCols++; }
                                           else
                                           {
                                               break;
                                           }
                                       }
                                   }
                                   else
                                   {
                                       for (int i = 0; i < colcount; i++)
                                       {
                                           if (sheet.Rows[0].Cells[i].Value != null)
                                           {
                                               usedCols++;
                                           
                                           }
                                       }
                                      // usedCols = colcount;
                                   }

                                  
                                     
                                   
                                        //DataTable excellDataFromSheet = sheet.CreateDataTable(new CreateDataTableOptions()
                                        //   {
                                        //       ColumnHeaders = true,
                                        //       StartRow = 0,
                                               
                                        //       NumberOfColumns = usedCols,
                                        //       NumberOfRows = sheet.Rows.Count,
                                        //       Resolution = ColumnTypeResolution.Auto,
                                        //       ExtractDataOptions = ExtractDataOptions.StopAtFirstEmptyRow,
                                        //   });

                                        string[] exceldata = new string[usedCols];
                                        //for (int i = 0; i < usedCols; i++)
                                        //{
                                        //    exceldata[i] = excellDataFromSheet.Columns[i].ToString();
                                        //}

                                        //excellDataFromSheet = null;


                                        int k = -1;
                                        for (int i = 0; i < colcount; i++)
                                           {
                                              
                                               if (sheet.Rows[0].Cells[i].Value != null)
                                               {
                                                   k++;
                                                   exceldata[k] = sheet.Rows[0].Cells[i].Value.ToString();
                                                   
                                               }
                                               else
                                               {
                                                   //int unicode = 65 + i;
                                                   //char character = (char)unicode;
                                                   //exceldata[i] = character.ToString();
                                               }

                                           }
                                        for (int i = 0; i < usedCols; i++)
                                       {
                                           int no = 2;
                                           for (int j = i + 1; j < usedCols - i; j++)
                                           {
                                               if (exceldata[i] != null && exceldata[j] != null)
                                               {
                                               if (exceldata[i].ToString().Trim() == exceldata[j].ToString().Trim())
                                                   {
                                                   exceldata[j] = exceldata[j].Trim() + " ("+no+")";
                                                       no++;
                                                   }
                                               }

                                           }

                                       }
                                   for (int i = 0; i < usedCols; i++)
                                       {
                                           excelSheets.Add(exceldata[i].ToString().Trim());
                                       }
                                       //ef1.Save(this.FilePath);

                                      // releaseObject(ef1.Worksheets);

                                   return excelSheets;
                                   // }

                               }



                               // releaseObject(ef1);
                           }
                           #endregion


                           #region code of xlsx file load , replacement of above code ,bug no 399
                           uint _ExcelPID = 0;
                           try
                           {
                               CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                           }
                           catch { }
                           try
                           {
                               //kill that process
                               Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                               excel.Kill();
                               excel.WaitForExit();
                           }
                           catch { }

                           GC.Collect();
                           GC.WaitForPendingFinalizers();


                           // Open connection with the database.
                           if (oleDBConn.State.ToString() != "Open")
                           {
                               oleDBConn.Open();
                           }
                           string sheetnametest = this.CurrentSheetName;
                           if (sheetnametest.Contains("[") == false)
                           {
                               sheetnametest = "[" + sheetnametest + "$]";
                           }
                           else
                           {
                               sheetnametest += "$";
                           }


                           if (sheetnametest.Contains("$"))
                           {
                               sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                           }
                           System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                           System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                           if (dt.Rows.Count == 0)
                           {
                               if (sheetnametest.Contains("[") == false)
                               {
                                   sheetnametest = "[" + sheetnametest + "$]";
                               }
                               else
                               {
                                   sheetnametest += "$";
                               }
                               System.Data.DataTable dtt = new System.Data.DataTable();
                               if (sheetnametest.Contains("$"))
                               {
                                   sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                               }

                               OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                               oleDBAdapter.Fill(dtt);
                               for (int i = 0; i < dtt.Columns.Count; i++)
                               {
                                   columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                               }

                           }
                           else
                           {
                               for (int i = 0; i < dt.Rows.Count; i++)
                               {
                                   columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                               }
                           }
                           oleDBConn.Close();
                           #endregion
                           return columnNames;
                    }
                        catch (Exception ex)
                    {
                            uint _ExcelPID = 0;                          
                            try
                            {
                                CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                            }
                            catch { }
                            try
                            {
                                //kill that process
                                Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                                excel.Kill();
                                excel.WaitForExit();
                            }
                            catch { }

                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                        
                     
                        // Open connection with the database.
                        if (oleDBConn.State.ToString() != "Open")
                        {
                            oleDBConn.Open();
                        }
                        string sheetnametest = this.CurrentSheetName;
                        if (sheetnametest.Contains("[") == false)
                        {
                            sheetnametest = "[" + sheetnametest + "$]";
                        }
                        else
                        {
                            sheetnametest += "$";
                        }
                       

                        if (sheetnametest.Contains("$"))
                        {
                            sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                        System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        if (dt.Rows.Count == 0)
                        {
                            if (sheetnametest.Contains("[") == false)
                            {
                                sheetnametest = "[" + sheetnametest + "$]";
                            }
                            else
                            {
                                sheetnametest += "$";
                            }
                            System.Data.DataTable dtt = new System.Data.DataTable();
                            if (sheetnametest.Contains("$"))
                            {
                                sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                            }

                            OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                            oleDBAdapter.Fill(dtt);
                            for (int i = 0; i < dtt.Columns.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                            }
                        }
                        oleDBConn.Close();
                        return columnNames;
                    }

                    finally
                    {
                        // Clean up.
                        
                    try
                     {
                    if (oleDBConn.State == ConnectionState.Open)
                    {
                        oleDBConn.Close();
                        oleDBConn.Dispose();
                    }
                  }
                 catch(Exception ex)
                  { }
                    }

                    return null;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xml")
                {
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();

                    if (CommonUtilities.GetInstance().XmlCheckExportFlag.Equals(false))
                    {
                        #region Normal browse xml file file

                        List<string> list = new List<string>();
                        sheetName = System.IO.Path.GetFullPath(this.m_filePath);
                        if (File.Exists(sheetName))
                        {
                            XmlDocument xmlDoc = new XmlDocument();

                            try
                            {
                                xmlDoc.Load(sheetName);
                                XmlNode root = (XmlNode)xmlDoc.DocumentElement;

                                // Axis 10.0 chnages
                                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring || TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Myobstring)
                                {

                                    nodeList = root.ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes;
                                    
                                }
                                else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                                {
                                    nodeList = root.ChildNodes;
                                }
                                //End Of Axis 10.0 Changes.

                                string xmlFileType = Mappings.GetInstance().XMLNodedata;
                                string nodeName = string.Empty;

                                foreach (XmlNode node in nodeList)
                                {
                                    xmllist.RemoveRange(0, xmllist.Count);
                                    list = getXmlElements(node);
                                    foreach (string xmlData in list)
                                    {
                                        columnNames.Add(xmlData);
                                    }
                                }

                            }
                            catch (XmlException)
                            {
                                MessageBox.Show("Error while loading the file.Please check the xml.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        #endregion
                    }
                    else
                    {
                        #region Exported xml file

                        System.Xml.XmlDocument xdoc = new XmlDocument();

                        xdoc.Load(CommonUtilities.GetInstance().BrowseFileName);

                        XmlNode root = (XmlNode)xdoc.DocumentElement;

                        for (int i = 0; i < root.ChildNodes.Count; i++)
                        {
                            if (root.ChildNodes.Item(i).Name == "UserMappings")
                            {
                                XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes[0].ChildNodes;
                                foreach (XmlNode node in userMappingsXML)
                                {
                                    if (!string.IsNullOrEmpty(node.InnerText) && !node.Name.Equals("name") && !node.Name.Equals("MappingType") && !node.Name.Equals("ImportType"))
                                        columnNames.Add(node.InnerText);
                                }
                            }
                        }

                        #endregion
                    }
                    return columnNames;
                }
                else
                {
                    if (sheetName.Contains("$"))
                    {
                        sheetName = sheetName.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                    }
                    System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetName, null });
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    if (dt.Rows.Count == 0)
                    {
                        System.Data.DataTable dtt = new System.Data.DataTable();
                        if (sheetName.Contains("$"))
                        {
                            sheetName = sheetName.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);
                        oleDBAdapter.Fill(dtt);
                        for (int i = 0; i < dtt.Columns.Count; i++)
                        {
                            columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                        }

                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                        }
                    }

                    return columnNames;
                }
            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER003");

                }
                else
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER001");

                }

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                if (Err != string.Empty)
                    throw new TIException("Zed Axis MSG037", this.m_filePath);
                else
                    throw new TIException("Zed Axis ER001");

            }
            finally
            {
                // Clean up.
               if (oleDBConn != null )
               // if (gembox != 1 && oleDBConn != null)
                {
                    oleDBConn.Close();
                    oleDBConn.Dispose();
                }
            }
        }
        /// <summary>
        /// creating xml file
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<string> getXmlElements(XmlNode node)
        {
            if (node.ChildNodes.Count > 0 && !node.FirstChild.Name.ToString().Equals("#text"))
            {

                string nodeName = ParentTag + node.Name;
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.ToString().Equals("#text"))
                    {
                        ParentTag = node.Name;
                        xmllist = getXmlElements(node.ChildNodes[i]);
                    }
                    else
                    {
                        ParentTag = string.Empty;
                        xmllist.Add(nodeName + node.ChildNodes[i].Name);
                    }
                }
            }
            else
                xmllist.Add(node.Name);
            return xmllist;
        }


        /// <summary>
        /// export datatable to csv file
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>
        //bug 487 Export to csv format
        public void ExportToCsvFile(DataTable dt, string outputPath)
        {
            StringBuilder sb = new StringBuilder();

            //Header not Required

            //string[] columnNames = dt.Columns.Cast<DataColumn>().
            //                                  Select(column => column.ColumnName).
            //                                  ToArray();
            //sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                ToArray();
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(outputPath, sb.ToString());
        }

        /// <summary>
        /// export dataset to excel
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>

        public void ExportToExcel(DataSet dataSet, string outputPath)
        {
            //#region for try
            try
            {                
                SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");

                ExcelFile ef = new ExcelFile();

                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet ws = ef.Worksheets.Add(fileName);


                //bug 510
                // Copy each DataTable
                foreach (System.Data.DataTable dt in dataSet.Tables)
                {

                //    // Insert the data from DataTable to the worksheet starting at cell "A1".
                //    worksheet.InsertDataTable(dataTable,
                //        new InsertDataTableOptions("A1") { ColumnHeaders = true ,StartColumn=0,StartRow=0});
                //}
                //ef.Save(outputPath);

                    // Copy the DataTable to an object array
                    object[,] rawData = new object[dt.Rows.Count + 2, dt.Columns.Count];
                   
                    // Copy the column names to the first row of the object array
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                    }                   

                    //GemBoxTry
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                    }

                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {
                            rawData[row +1, col] = dt.Rows[row].ItemArray[col];
                        }
                    }


                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {                           
                            ws.Cells[row , col].Value = rawData[row, col];
                        }
                    }
                    //ExcelCell cell

                    // Calculate the final column letter
                    string finalColLetter = string.Empty;
                    string colCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    int colCharsetLen = colCharset.Length;

                    if (dt.Columns.Count > colCharsetLen)
                    {
                        finalColLetter = colCharset.Substring(
                            (dt.Columns.Count - 1) / colCharsetLen - 1, 1);
                    }

                    finalColLetter += colCharset.Substring(
                            (dt.Columns.Count - 1) % colCharsetLen, 1);

                  

                    if (dt.TableName == string.Empty)
                        dt.TableName = "Sheet1";
                    // excelSheet.Name = dt.TableName; 
                    ws.Name = dt.TableName;
                    this.CurrentSheetName = dt.TableName;                   
                   string excelRange = string.Format("A1:{0}{1}",
                   finalColLetter, dt.Rows.Count + 1);
                  // ws.InsertDataTable(dt);
                   ws.InsertDataTable(dt,
                   new InsertDataTableOptions()
                   {
                       ColumnHeaders = false,
                       StartRow = 1,
                       StartColumn=0
                       
                   });
                   ef.Save(outputPath);
                   releaseObject(ef.Worksheets);       
                }

            }
            catch (Exception ex)
            {
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        /// <summary>
        /// export dataset to excel 2007
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>
        public void ExportToExcel2007(DataSet dataSet, string outputPath)
        {

            #region GemBox
            try
            {
                SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");

                ExcelFile ef = new ExcelFile();

                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet worksheet = ef.Worksheets.Add(fileName);


                // Copy each DataTable
                foreach (System.Data.DataTable dataTable in dataSet.Tables)
                {

                    // Insert the data from DataTable to the worksheet starting at cell "A1".
                    worksheet.InsertDataTable(dataTable,
                        new InsertDataTableOptions("A1") { ColumnHeaders = true });
                }
                ef.Save(outputPath);
                   // // Copy the DataTable to an object array
                   // object[,] rawData = new object[dt.Rows.Count + 2, dt.Columns.Count];
                   // // rawData[0, dt.Columns.Count / 2] = reportTitle;

                   // // Copy the column names to the first row of the object array
                   // for (int col = 0; col < dt.Columns.Count; col++)
                   // {
                   //     rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                   // }

                   // // Copy the values to the object array
                   // //for (int col = 0; col < dt.Columns.Count; col++)
                   // //{
                   // //    for (int row = 0; row < dt.Rows.Count; row++)
                   // //    {
                   // //        rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                   // //    }
                   // //}

                   // //GemBoxTry
                   // for (int col = 0; col < dt.Columns.Count; col++)
                   // {
                   //     rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                   // }

                   // for (int col = 0; col < dt.Columns.Count; col++)
                   // {
                   //     for (int row = 0; row < dt.Rows.Count; row++)
                   //     {
                   //         rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                   //     }
                   // }


                   // for (int col = 0; col < dt.Columns.Count; col++)
                   // {
                   //     for (int row = 0; row <= dt.Rows.Count; row++)
                   //     {
                   //         //rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                   //         ws.Cells[row, col].Value = rawData[row, col];
                   //     }
                   // }

                   // // Calculate the final column letter
                   // string finalColLetter = string.Empty;
                   // string colCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                   // int colCharsetLen = colCharset.Length;

                   // if (dt.Columns.Count > colCharsetLen)
                   // {
                   //     finalColLetter = colCharset.Substring(
                   //         (dt.Columns.Count - 1) / colCharsetLen - 1, 1);
                   // }

                   // finalColLetter += colCharset.Substring(
                   //         (dt.Columns.Count - 1) % colCharsetLen, 1);
                  

                   // if (dt.TableName == string.Empty)
                   //     dt.TableName = "Sheet1";
                   // // excelSheet.Name = dt.TableName; 
                   // ws.Name = dt.TableName;
                   // this.CurrentSheetName = dt.TableName;

                   // //    excelSheet.Columns.NumberFormat = "@";        //For bug no.557
                   // // Fast data export to Excel
                   // string excelRange = string.Format("A1:{0}{1}",
                   //finalColLetter, dt.Rows.Count + 1);

                   // //  releaseObject(ef.Worksheets);
                   // uint _ExcelPID = 0;
                   // ef.Save(outputPath);
                   // releaseObject(ef.Worksheets);
        //    }

            }
            catch (Exception ex)
            {
            }
            #endregion           
           
           
        }

        //For excel interop objects to get released from memory
        private void releaseObject(object obj)
        {
            try
            {
                //System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                //obj = null;

                //bug 480
                if (Marshal.IsComObject(obj))
                {
                    Marshal.ReleaseComObject(obj);
                 
                }
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Axis 11.0 -131 Exports errors from import summary to an xls file
        /// </summary>
        public void exportErrors(DataSet dataSet, string outputPath, string matchColumn)
        {
            try
            {
                string errorColumnName = "Ref";
                if (matchColumn.Contains("Name") || matchColumn == "Code" || matchColumn == "Reason" || matchColumn == "Desc1" || matchColumn == "PriceAdjustmentName" || matchColumn == "PriceDiscountName" || matchColumn == "CreatedBy" || matchColumn == "FromStoreNumber")
                {
                    errorColumnName = "Name";
                }

                SpreadsheetInfo.SetLicense("EAAM-KCAG-1FTI-UQA5");

                ExcelFile ef = new ExcelFile();
                int row = 0, col = 0, index = 0;               
           

                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet ws = ef.Worksheets.Add(fileName);
           
                ws.Name = "Import Errors";


                System.Data.DataTable importData = dataSet.Tables[0];
                System.Data.DataTable errorData = dataSet.Tables[1];
                index = 0;
                for (col = 0; col < errorData.Columns.Count - 1; col++)
                {
                    ws.Cells[row, col].Value = errorData.Columns[index++].ColumnName;
                }

                index = 0;
                for (int i = 0; i < importData.Columns.Count ; i++)
                {
                    ws.Cells[row, col++].Value = importData.Columns[index++].ColumnName;
                }

                row = 1;
                foreach (DataRow import in importData.Rows)
                {
                    foreach (DataRow error in errorData.Rows)
                    {
                        string val1 = import[matchColumn].ToString();
                        string val2 = error[errorColumnName].ToString();
                        if (val1 == val2)
                        {
                            index = 0;
                            for (col = 0; col < errorData.Columns.Count - 1; col++)
                            {
                                ws.Cells[row, col].Value = error[index++].ToString();
                            }
                        }
                    }

                    index = 0;
                    for (int i = 0; i < importData.Columns.Count ; i++)
                    {
                        ws.Cells[row, col++].Value = import[index++].ToString();
                    }
                    row++;
                }

                ef.Save(outputPath);
                releaseObject(ef.Worksheets);       
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// format data using selected mapping by using file extesion.
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public System.Data.DataTable GetDataForSelectedMapping(string sheetName)
        {
            OleDbConnection oleDBConn = null;
            try
            {
                string newfilePath = string.Empty;
                this.m_CurrentSheetname = sheetName;
                // Connection String. Change the excel file to the file you will search.
                string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
                int checkExcel = 0;
               
                System.Data.DataTable TextDatafromSelectedMapping = new System.Data.DataTable();

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                    {
                        TextDatafromSelectedMapping = GetDataBySheetName(sheetName);
                    }
                    else
                    {
                        TextDatafromSelectedMapping = GetDataBySheetName(string.Empty);
                    }


                    string logDirectory = ConfigurationManager.AppSettings.Get("FL");

                    if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    {
                        logDirectory = System.Windows.Forms.Application.StartupPath + "\\LOG" + logDirectory;
                    }

                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(EDI.Constant.Constants.xpOSName))
                        {
                            if (logDirectory.Contains("Program Files (x86)"))
                            {
                                logDirectory = logDirectory.Replace("Program Files (x86)", EDI.Constant.Constants.xpPath);
                            }
                            else
                            {
                                logDirectory = logDirectory.Replace("Program Files", EDI.Constant.Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (logDirectory.Contains("Program Files (x86)"))
                            {
                                logDirectory = logDirectory.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    if (!Directory.Exists(logDirectory))
                    {
                        Directory.CreateDirectory(logDirectory);
                    }
                    string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory + "JournalSelectDatatext.xls" : logDirectory + Path.DirectorySeparatorChar + "JournalSelectDatatext.xls";
                    if (!filePath.Contains("JournalSelectDatatext.xls"))
                    {
                        filePath = logDirectory + "\\JournalSelectDatatext.xls";
                    }

                    TextDatafromSelectedMapping.TableName = "JournalText";
                    DataSet dsTextDT = new DataSet();


                    if (!this.m_HDR)
                    {

                        dsTextDT.Tables.Add(TextDatafromSelectedMapping);
                    }
                    else
                    {

                        dsTextDT.Tables.Add(TextDatafromSelectedMapping);
                    }
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    try
                    {
                       ExportToExcel(dsTextDT, filePath);
                        if (newfilePath != string.Empty)
                        {
                            try
                            {
                                File.Delete(newfilePath);
                            }
                            catch
                            { }
                        }
                    }
                    catch (COMException ce)
                    {
                        int errorCode = ce.ErrorCode;

                        checkExcel = 1;

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Retrieving the COM class factory for component with CLSID {00024500-0000-0000-C000-000000000046} failed due to the following error: 80040154.")
                        {
                            checkExcel = 1;

                        }
                        else
                        {
                            throw new Exception();
                        }

                    }
                    if (checkExcel != 1)
                    {
                        this.m_filePath = filePath;
                        sheetName = this.CurrentSheetName;
                        strXlsSheet = sheetName;
                    }

                }

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                {
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);

                    #region For the bug 1502 (axis 6.0)
                    //TextDatafromSelectedMapping = GetDataBySheetName(string.Empty);


                    //string logDirectory = ConfigurationManager.AppSettings.Get("FL");

                    //if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                    //{
                    //    logDirectory = System.Windows.Forms.Application.StartupPath + logDirectory;

                    //}
                    //try
                    //{
                    //    //logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                    //}
                    //catch { }
                    //if (!Directory.Exists(logDirectory))
                    //{
                    //    Directory.CreateDirectory(logDirectory);
                    //}
                    //string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory + "JournalSelectDatatext.xls" : logDirectory + Path.DirectorySeparatorChar + "JournalSelectDatatext.xls";

                    ////objExport.ExportDetails(TextDatafromSelectedMapping, Export.ExportFormat.Excel, filePath);

                    //TextDatafromSelectedMapping.TableName = "JournalText";
                    //DataSet dsTextDT = new DataSet();
                    //dsTextDT.Tables.Add(TextDatafromSelectedMapping);

                    //if (File.Exists(filePath))
                    //{
                    //    File.Delete(filePath);
                    //    //CheckCondition = 1;
                    //}

                    //try
                    //{
                    //    ExportToExcel(dsTextDT, filePath);
                    //}
                    //catch (COMException ce)
                    //{
                    //    int errorCode = ce.ErrorCode;
                    //    //if (errorCode == -2146827284)
                    //    //{
                    //    checkExcel = 1;
                    //    //}
                    //}
                    //catch (Exception ex)
                    //{
                    //    if (ex.Message == "Retrieving the COM class factory for component with CLSID {00024500-0000-0000-C000-000000000046} failed due to the following error: 80040154.")
                    //    {
                    //        checkExcel = 1;

                    //    }
                    //    else
                    //    {
                    //        throw new Exception();
                    //    }

                    //}
                    //if (checkExcel != 1)
                    //{
                    //    this.m_filePath = filePath;
                    //    sheetName = this.CurrentSheetName;
                    //    strXlsSheet = sheetName;
                    //}

                    #endregion
                }
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm")
                {
                   // connString = string.Format(connectionStringFormatXLSX, this.m_filePath, this.m_HDR ? "YES" : "NO");
                     connString= "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                }
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                {
                    //connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO", this.m_FMT);
                     connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                }
                System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                //bug  msg..
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".txt" && System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".iif" && System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".csv")
                {
                    string counter = string.Empty;
                    // Create connection object by using the preceding connection string.

                    oleDBConn = new OleDbConnection(connString);
                    // Open connection with the database.
                    try
                    {
                        oleDBConn.Open();
                    }
                    catch
                    {
                        counter = "1";
                    }
                    if (counter == "1")
                    {
                        #region For Unformated Xls File
                        // Microsoft.Office.Interop.Excel.Application objExcel = new Microsoft.Office.Interop.Excel.Application();
                        try
                        {
                            string logDirectory = System.Windows.Forms.Application.StartupPath + "\\LOG";

                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(EDI.Constant.Constants.xpOSName))
                                {
                                    if (logDirectory.Contains("Program Files (x86)"))
                                    {
                                        logDirectory = logDirectory.Replace("Program Files (x86)", EDI.Constant.Constants.xpPath);
                                    }
                                    else
                                    {
                                        logDirectory = logDirectory.Replace("Program Files", EDI.Constant.Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (logDirectory.Contains("Program Files (x86)"))
                                    {
                                        logDirectory = logDirectory.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }
                            }
                            catch { }

                            if (!System.IO.Directory.Exists(logDirectory))
                            {
                                System.IO.Directory.CreateDirectory(logDirectory);
                            }
                            FileInfo flinfo = new FileInfo(this.m_filePath);
                            string Flname = flinfo.Name;
                            string filenewpath = logDirectory + "\\" + Flname;
                            File.Copy(this.m_filePath, filenewpath, true);
                            ExcelFile ef = ExcelFile.Load(filenewpath);                            
                            string filePth = logDirectory + "\\test132456.xls";

                            try
                            {
                                if (File.Exists(filePth))
                                    File.Delete(filePth);
                                ef.Save(filePth);                                
                            }
                            catch
                            { }
                            
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            this.FilePath = filePth;
                        }
                        catch
                        {                           
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                        }
                        string connStringnew = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
                        OleDbConnection oleDBConntest = new OleDbConnection(connStringnew);
                        // Open connection with the database.
                        oleDBConntest.Open();
                        //string sheetnametest = System.IO.Path.GetFileName(this.m_filePath);
                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter();
                        if (!this.m_HDR)
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                        }
                        else
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                        }
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                        {
                            if (this.DataNumber == 1)
                            {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConntest);
                            }
                            else
                                if (!this.m_HDR)
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                        }
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                        {
                            if (this.DataNumber == 1)
                            {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConntest);
                            }
                            else
                                if (!this.m_HDR)
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                        }
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                        {
                            //if (sheetName == string.Empty)
                            //    sheetName = strXlsSheet;

                            //string columns = CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader1();

                            //string[] colList = columns.Split(',');
                            ////  string extension = flinfo.Extension;
                            //ExcelFile ef = ExcelFile.Load(this.m_filePath);
                            //foreach (ExcelWorksheet sheet in ef.Worksheets)
                            //{
                            //    if (sheet.Name == sheetName)
                            //    {
                            //        int colcount = sheet.CalculateMaxUsedColumns();
                            //        int usedCols = 0;
                                   
                            //            usedCols = colcount;
                                    
                            //        excellDataFromSheet = sheet.CreateDataTable(new CreateDataTableOptions()
                            //        {
                            //            ColumnHeaders = true,
                            //            StartRow = 0,

                            //            NumberOfColumns = usedCols,
                            //            NumberOfRows = sheet.Rows.Count,
                            //            Resolution = ColumnTypeResolution.Auto,
                            //            ExtractDataOptions = ExtractDataOptions.StopAtFirstEmptyRow,
                            //        });
                            //        break;
                            //    }
                            //}
                           
                            //int no=0;
                            //foreach (string str in colList)
                            //{
                            //    bool valid = true;
                            //    string[] ColName = str.Split('`');
                            // ColName[0]=  ColName[0].ToString().Replace(".", string.Empty);
                            //    for (int i = 0; i < excellDataFromSheet.Columns.Count; i++)
                            //    {
                                   
                            //        {
                            //            if (ColName[0].ToString() == excellDataFromSheet.Columns[i].ToString())
                            //            {
                            //                valid = true;
                            //                no = i;
                            //                break;
                            //            }
                            //            else
                            //            {
                            //                valid = false;
                            //            }
                            //        }
                            //    }

                            //    if (valid == false)
                            //    {
                            //        excellDataFromSheet.Columns.Remove(ColName[0].ToString());
                            //    }
                            //    else
                            //    {
                            //        excellDataFromSheet.Columns[ColName[0].ToString()].ColumnName = ColName[1].ToString();
                            //    }
                            //}

                            //return excellDataFromSheet;
                            

                                 //foreach (DataRow sourcerow in source.Rows)
                                 //{
                                 //  DataRow destRow = dest.NewRow();
                                 //   foreach(string colname in columns)
                                 //   {
                                 //     destRow[colname] = sourcerow[colname];
                                 //   }
                                 //  dest.Rows.Add(destRow);
                                 // }




                           // if (this.DataNumber == 1)
                           // {
                           //     oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConntest);
                          //  }
                           // else
                          //      if (!this.m_HDR)
                          //      {
                           //         excellDataFromSheet.Columns.Remove(ColName[0].ToString());
                           //     }
                           //     else
                          //      {
                          //          excellDataFromSheet.Columns[ColName[0].ToString()].ColumnName = ColName[1].ToString();
                          //      }
                          //  }

                         //   return excellDataFromSheet;
                            

                                 //foreach (DataRow sourcerow in source.Rows)
                                 //{
                                 //  DataRow destRow = dest.NewRow();
                                 //   foreach(string colname in columns)
                                 //   {
                                 //     destRow[colname] = sourcerow[colname];
                                 //   }
                                 //  dest.Rows.Add(destRow);
                                 // }
                                


  						if (this.DataNumber == 1)
                        {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConntest);
                            }
                            else
                                if (!this.m_HDR)
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConntest);
                                }
                        }
                        if (!this.m_HDR)
                            oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace("Column", "F");
                        
                        try
                        {
                            oleDBAdapter.Fill(excellDataFromSheet);
                        }
                        catch
                        {
                            return null;
                        }
                        #endregion
                    }
                    else
                    {
                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter();
                        if (!this.m_HDR)
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);//GetSelectList(TextDatafromSelectedMapping)), oleDBConn);
                        }
                        else
                        {
                            oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                        }


                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                        {
                            if (this.DataNumber == 1)
                            {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                            }
                            else
                                if (!this.m_HDR)
                                {
                                   oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }

                            if (!this.m_HDR)
                                oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace("Column", "F");
                        }

                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                        {
                            if (sheetName == string.Empty)
                                sheetName = strXlsSheet;
                            if (this.m_filePath.Contains("JournalSelectDatatext.xls"))
                            {
                                sheetName = strXlsSheet;
                            }
                            if (this.DataNumber == 1)
                            {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);
                            }
                            else
                            {
                                //if (CommonUtilities.GetInstance().SelectedMapping.GetSelectList() != string.Empty)
                                if (!this.m_HDR)
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }

                            }
                            if (!this.m_HDR)
                            {
                                oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace("Column", "F");
                                oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace(".", string.Empty);
                            }                            
                        }
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx")
                        {
                            if (this.DataNumber == 1)
                            {
                                oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);
                            }
                            else
                            {
                                //if (CommonUtilities.GetInstance().SelectedMapping.GetSelectList() != string.Empty)
                                if (!this.m_HDR)
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }
                                else
                                {
                                    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName).Replace("*", CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader()), oleDBConn);
                                }                               
                            }

                            if (!this.m_HDR)
                                oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace("Column", "F");
                        }

                        try
                        {
                            //Bug No: 296
                            oleDBAdapter.SelectCommand.CommandText = oleDBAdapter.SelectCommand.CommandText.Replace(".", "");
                            oleDBAdapter.Fill(excellDataFromSheet);
                            if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx")
                            {                                
                                    excellDataFromSheet.Rows[0].Delete();                                   
                                    excellDataFromSheet.AcceptChanges();                              
                            }
                            else
                            {
                                if (!this.HDR)
                                {

                                    excellDataFromSheet.Rows[0].Delete();
                                     excellDataFromSheet.Rows[1].Delete();
                                    excellDataFromSheet.AcceptChanges();
                                }
                            }
                        }
                        catch (Exception ex)
                        {                            
                            return null;
                        }
                    }
                }

                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                {
                    if (checkExcel == 1)
                    {
                        string strSelect = string.Empty;
                        if (this.DataNumber == 1)
                        {
                            strSelect = "*";
                        }
                        else if (!this.m_HDR)
                        {                          
                           strSelect = CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader();
                        }

                        if (!this.m_HDR)
                        {
                            strSelect = strSelect.Replace("Column", "F");
                        }

                        if (strSelect == "*")
                        {
                            this.DataNumber = 1;
                            return GetDataBySheetName(string.Empty);
                        }
                        string[] newColumns = new string[100];
                        string[] newColumnsExpected = new string[100];
                        if (strSelect != "*")
                        {
                            string[] oldColumn = strSelect.Split(',');
                            for (int count = 0; count < oldColumn.Length; count++)
                            {
                                try
                                {
                                    string newStr = oldColumn[count].Remove(0, oldColumn[count].IndexOf("As") + 2);
                                    newColumns[count] = newStr.Trim();
                                    newColumnsExpected[count] = oldColumn[count].Remove(oldColumn[count].IndexOf("As"), oldColumn[count].Length - oldColumn[count].IndexOf("As"));
                                }
                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog("Error :" + ex.Message.ToString());
                                    CommonUtilities.WriteErrorLog("Error Stack Trace :" + ex.StackTrace.ToString());
                                    CommonUtilities.WriteErrorLog("File information:" + strSelect);
                                    CommonUtilities.WriteErrorLog("File information:" + newColumns[count]);
                                    CommonUtilities.WriteErrorLog("File information:" + newColumnsExpected[count]);
                                }

                            }

                        }

                        char[] delimiter = new char[1];
                        if (this.m_FMT == "TabDelimited")
                            delimiter[0] = '\t';
                        if (this.m_FMT == "CSVDelimited")
                            delimiter[0] = ',';
                        if (this.m_FMT == "Delimited(|)")
                            delimiter[0] = '|';
                        string[] lines = File.ReadAllLines(this.m_filePath);
                        string[] colCount = new string[newColumns.Length];
                        int columnCount = 0;
                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (i == 0)
                            {
                                string[] columns = lines[i].Split(delimiter);

                                for (int temp = 0; temp < columns.Length; temp++)
                                {
                                    for (int count = 0; count < newColumns.Length; count++)
                                    {
                                        if (newColumnsExpected[count] != null)
                                        {
                                            if (columns[temp] == newColumnsExpected[count].Replace("[", string.Empty).Replace("]", string.Empty).Trim())
                                            {
                                                colCount[count] = temp.ToString();
                                            }
                                        }
                                    }
                                }
                            }

                            if (i == 0)
                            {
                                foreach (string column in newColumns)
                                {
                                    try
                                    {
                                        if (column != null)
                                        {
                                            if (this.m_HDR)
                                            {
                                                excellDataFromSheet.Columns.Add(column.Replace("[", string.Empty).Replace("]", string.Empty).Trim());
                                                columnCount++;
                                            }
                                            else
                                                excellDataFromSheet.Columns.Add();
                                        }
                                    }
                                    catch
                                    {
                                        
                                    }
                                }
                                if (this.m_HDR)
                                    continue;
                            }

                            #region String Parser Logic
                            //Converts all rows into Object array
                            //This is String parser logic for handling invalid files.
                            if (lines[i] != string.Empty)
                            {
                                Object[] textRowData = (Object[])lines[i].Split(delimiter);

                                Object[] rowDt = new Object[columnCount];
                                for (int temp = 0; temp < colCount.Length; temp++)
                                {
                                    if (colCount[temp] != null)
                                        rowDt[temp] = textRowData[Convert.ToInt32(colCount[temp])];
                                }

                                excellDataFromSheet.LoadDataRow(rowDt, true);

                            }

                            #endregion
                        }
                        return excellDataFromSheet;
                    }

                }

                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    if (checkExcel == 1)
                    {
                        string strSelect = string.Empty;
                        System.Data.DataTable dtIIFData = new System.Data.DataTable();
                        dtIIFData = GetDataBySheetName(string.Empty);
                        if (this.DataNumber == 1)
                        {
                            strSelect = "*";
                        }
                        else
                            strSelect = CommonUtilities.GetInstance().SelectedMapping.GetSelectListWithHeader();
                        if (!this.m_HDR)
                        {
                            strSelect = strSelect.Replace("Column", "F");
                        }

                        if (strSelect == "*")
                        {
                            this.DataNumber = 1;
                            return dtIIFData;
                        }

                       
                        string[] newColumns = new string[100];
                        string[] newColumnsExpected = new string[100];
                        int columnCount = 0;
                        if (strSelect != "*")
                        {
                            string[] oldColumn = strSelect.Split(',');
                            for (int count = 0; count < oldColumn.Length; count++)
                            {
                                if (oldColumn[count] != string.Empty)
                                {
                                    try
                                    {
                                        string newStr = oldColumn[count].Remove(0, oldColumn[count].IndexOf("As") + 2);
                                        newColumns[count] = newStr.Trim();
                                        newColumnsExpected[count] = oldColumn[count].Remove(oldColumn[count].IndexOf("As"), oldColumn[count].Length - oldColumn[count].IndexOf("As"));
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog("Error :" + ex.Message.ToString());
                                        CommonUtilities.WriteErrorLog("Error Stack Trace :" + ex.StackTrace.ToString());
                                        CommonUtilities.WriteErrorLog("File information:" + strSelect);
                                        CommonUtilities.WriteErrorLog("File information:" + newColumns[count]);
                                        CommonUtilities.WriteErrorLog("File information:" + newColumnsExpected[count]);
                                    }
                                }
                            }
                        }

                        #region IIF new mapping preview into grid
                        foreach (string column in newColumns)
                        {
                            try
                            {
                                if (column != null)
                                {

                                    excellDataFromSheet.Columns.Add(column.Replace("[", string.Empty).Replace("]", string.Empty).Trim());
                                    columnCount++;

                                }
                            }
                            catch
                            {
                                excellDataFromSheet.Columns.Add(column + "new");
                            }
                        }

                        //Object[] rowDt = new Object[newColumnsExpected.Length];
                        Object[] rowDt = new Object[columnCount];

                        for (int temp = 0; temp < dtIIFData.Rows.Count; temp++)
                        {
                            for (int count = 0; count < rowDt.Length; count++)
                            {
                                if (newColumns[count] != null)

                                    rowDt[count] = dtIIFData.Rows[temp][newColumnsExpected[count].ToString().Replace("[", string.Empty).Replace("]", string.Empty).Trim()].ToString();
                                else
                                    break;
                            }
                            excellDataFromSheet.LoadDataRow(rowDt, true);
                        }

                    }
                    return excellDataFromSheet;

                        #endregion

                }
                this.DataNumber = 0;
                return excellDataFromSheet;
            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER003");
                }
                else
                {
                    CommonUtilities.WriteErrorLog(OleEx.Message);
                    CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER001");
                }

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);

                throw new TIException("Zed Axis ER001");
            }
            finally
            {
                // Clean up.
                if (oleDBConn != null)
                {
                    oleDBConn.Close();
                    oleDBConn.Dispose();
                }
            }
        }

        #endregion

    }
}












