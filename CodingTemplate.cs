using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataProcessingBlocks;

namespace TransactionImporter
{
    public partial class CodingTemplate : Form
    {
        private static CodingTemplate frm_Codingtemplate;
        private bool flag = false;
        private static CodingTemplate getInstances()
        {
            if (frm_Codingtemplate == null)
                frm_Codingtemplate = new CodingTemplate();
            return frm_Codingtemplate;
        }

        public string codingTemplateName
        {
            get
            {
                return this.textBoxCodingTemplateName.Text.TrimStart().TrimEnd().Trim();
            }
        }

        public CodingTemplate()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            string templateName = string.Empty;

            if (this.textBoxCodingTemplateName.Text.Trim().TrimStart().TrimEnd() == string.Empty)
            {
                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0131"), "Zed Axis");
                return;
            }
            else
            {
                #region if Coding Template name exist then add that name to Codings.xml

                templateName=this.textBoxCodingTemplateName.Text.Trim().TrimStart().TrimEnd();

                #region check whether same name already exist
                
                bool flag=false;

                List<string> codingTemplate = new List<string>();
                
                codingTemplate = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();

                foreach(string name in codingTemplate)
                {
                    if (name.Equals(templateName))
                    {
                        flag = true;
                        break;
                    }
                }


                #endregion

                if (!flag)
                    CommonUtilities.GetInstance().WriteCodingTemplateFile(templateName);
                else
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0132"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                this.DialogResult = DialogResult.OK;

                this.Close();

                #endregion
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}