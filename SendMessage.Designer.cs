namespace DataProcessingBlocks
{
    partial class SendMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendMessage));
            this.buttonTo = new System.Windows.Forms.Button();
            this.buttonCc = new System.Windows.Forms.Button();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.textBoxCc = new System.Windows.Forms.TextBox();
            this.richTextBoxBody = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSubject = new System.Windows.Forms.TextBox();
            this.buttonDraft = new System.Windows.Forms.Button();
            this.linkLabelAttachment = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonTo
            // 
            this.buttonTo.Location = new System.Drawing.Point(102, 4);
            this.buttonTo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonTo.Name = "buttonTo";
            this.buttonTo.Size = new System.Drawing.Size(73, 23);
            this.buttonTo.TabIndex = 0;
            this.buttonTo.Text = "To...";
            this.buttonTo.UseVisualStyleBackColor = true;
            this.buttonTo.Click += new System.EventHandler(this.buttonTo_Click);
            // 
            // buttonCc
            // 
            this.buttonCc.Location = new System.Drawing.Point(102, 31);
            this.buttonCc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonCc.Name = "buttonCc";
            this.buttonCc.Size = new System.Drawing.Size(73, 23);
            this.buttonCc.TabIndex = 1;
            this.buttonCc.Text = "Cc...";
            this.buttonCc.UseVisualStyleBackColor = true;
            this.buttonCc.Click += new System.EventHandler(this.buttonCc_Click);
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(183, 4);
            this.textBoxTo.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxTo.MaxLength = 400;
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(467, 21);
            this.textBoxTo.TabIndex = 2;
            // 
            // textBoxCc
            // 
            this.textBoxCc.Location = new System.Drawing.Point(183, 32);
            this.textBoxCc.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxCc.MaxLength = 400;
            this.textBoxCc.Name = "textBoxCc";
            this.textBoxCc.Size = new System.Drawing.Size(467, 21);
            this.textBoxCc.TabIndex = 3;
            // 
            // richTextBoxBody
            // 
            this.richTextBoxBody.Location = new System.Drawing.Point(6, 151);
            this.richTextBoxBody.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.richTextBoxBody.MaxLength = 2000;
            this.richTextBoxBody.Name = "richTextBoxBody";
            this.richTextBoxBody.Size = new System.Drawing.Size(650, 416);
            this.richTextBoxBody.TabIndex = 4;
            this.richTextBoxBody.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Subject:";
            // 
            // textBoxSubject
            // 
            this.textBoxSubject.Location = new System.Drawing.Point(183, 60);
            this.textBoxSubject.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxSubject.MaxLength = 255;
            this.textBoxSubject.Name = "textBoxSubject";
            this.textBoxSubject.Size = new System.Drawing.Size(467, 21);
            this.textBoxSubject.TabIndex = 7;
            // 
            // buttonDraft
            // 
            this.buttonDraft.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonDraft.Location = new System.Drawing.Point(6, 70);
            this.buttonDraft.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonDraft.Name = "buttonDraft";
            this.buttonDraft.Size = new System.Drawing.Size(88, 23);
            this.buttonDraft.TabIndex = 8;
            this.buttonDraft.Text = "Save Draft";
            this.buttonDraft.UseVisualStyleBackColor = true;
            this.buttonDraft.Click += new System.EventHandler(this.buttonDraft_Click);
            // 
            // linkLabelAttachment
            // 
            this.linkLabelAttachment.AutoSize = true;
            this.linkLabelAttachment.Location = new System.Drawing.Point(189, 94);
            this.linkLabelAttachment.Name = "linkLabelAttachment";
            this.linkLabelAttachment.Size = new System.Drawing.Size(0, 13);
            this.linkLabelAttachment.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Attachment:";
            // 
            // buttonSend
            // 
            this.buttonSend.Image = ((System.Drawing.Image)(resources.GetObject("buttonSend.Image")));
            this.buttonSend.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonSend.Location = new System.Drawing.Point(6, 2);
            this.buttonSend.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(88, 62);
            this.buttonSend.TabIndex = 5;
            this.buttonSend.Text = "Send";
            this.buttonSend.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // SendMessage
            // 
            this.AcceptButton = this.buttonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(663, 577);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabelAttachment);
            this.Controls.Add(this.buttonDraft);
            this.Controls.Add(this.textBoxSubject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSend);
            this.Controls.Add(this.richTextBoxBody);
            this.Controls.Add(this.textBoxCc);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.buttonCc);
            this.Controls.Add(this.buttonTo);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1029, 615);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(668, 615);
            this.Name = "SendMessage";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create Message";
            this.Load += new System.EventHandler(this.SendMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTo;
        private System.Windows.Forms.Button buttonCc;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDraft;
        private System.Windows.Forms.LinkLabel linkLabelAttachment;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBoxTo;
        public System.Windows.Forms.TextBox textBoxCc;
        public System.Windows.Forms.TextBox textBoxSubject;
        public System.Windows.Forms.RichTextBox richTextBoxBody;
    }
}