﻿using EDI.Constant;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Windows.Forms;
using System.Xml;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TransactionImporter;

namespace DataProcessingBlocks
{
    public partial class ReplaceFunction : Telerik.WinControls.UI.RadForm
    {
        #region Private Members

        private static ReplaceFunction m_ReplaceFunction;
        private bool closeForm = true;
        private string assemblyVersion = string.Empty;
        private string invalidExpression = "Invalid expression please try with correct syntax";
        private string incorrectExpression = "Invalid Expression";
        #endregion

        #region Public Members

        public int incomplete = 0;
        public int flagNewEdit = 0;
        public int flagDelete = 0;
        public string SelectedFunction = string.Empty;

        #endregion
        public ReplaceFunction()
        {
            InitializeComponent();
        }



        private void ReplaceFunction_Load(object sender, EventArgs e)
        {
            this.FormElement.TitleBar.FillPrimitive.BackColor = Color.White;
            this.FormElement.TitleBar.FillPrimitive.GradientStyle = GradientStyles.Solid;
            this.FormElement.TitleBar.TitlePrimitive.ForeColor = Color.Black;
            this.FormElement.TitleBar.MaximizeButton.Padding = new Padding(14, 0, 14, 0);

            // Condition to check whether ReplaceFunction Form called because of invalid data
            if (comboBoxType.SelectedItem != null)
            {
                if (flagDelete == 1)
                {
                    textboxFunctionName.Visible = false;
                    comboBoxFunctionName.Visible = true;
                    buttonOk.Visible = false;
                    buttonSaveAsXml.Visible = false;
                }
                else if (flagNewEdit == 0)
                {
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        CreateFunctionGrid();
                        textboxFunctionName.Visible = true;
                        comboBoxFunctionName.Visible = false;
                    }
                    else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                    {
                        CreateConcateFunctionGrid();
                        textboxFunctionName.Visible = true;
                        comboBoxFunctionName.Visible = false;
                    }
                    buttonOk.Visible = true;
                    buttonSaveAsXml.Visible = true;

                }
                else
                {
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        textboxFunctionName.Visible = false;
                        comboBoxFunctionName.Visible = true;
                        if (comboBoxFunctionName.SelectedItem != null)
                        {
                            var  functionDetails = CommonUtilities.GetInstance().FetchFunctionDetails(comboBoxFunctionName.SelectedItem.ToString());
                            if(functionDetails.Item2==true)
                            {
                                chkExactMatch.Checked = true;
                            }
                            else
                            {
                                chkExactMatch.Checked = false;
                            }
                            CreateFunctionGrid(functionDetails.Item1);
                        }
                        else
                        {
                            CreateFunctionGrid();
                        }
                    }
                    else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                    {
                        textboxFunctionName.Visible = false;
                        comboBoxFunctionName.Visible = true;
                        if (comboBoxFunctionName.SelectedItem != null)
                        {
                            List<string> expression = CommonUtilities.GetInstance().FetchConcatFunctionDetails(comboBoxFunctionName.SelectedItem.ToString());

                            CreateConcateFunctionGrid(expression);
                        }
                        else
                        {
                            CreateConcateFunctionGrid();
                        }
                    }
                    buttonOk.Visible = true;
                    buttonSaveAsXml.Visible = true;
                }
            }
            else
            {
                buttonSaveAsXml.Visible = true;
            }
            if (flagDelete == 1)
            {
                textboxFunctionName.Visible = false;
                comboBoxFunctionName.Visible = true;
                buttonOk.Visible = false;
                buttonSaveAsXml.Visible = false;
                dataGridViewFunction.Visible = false;
            }
            else
            {
                dataGridViewFunction.Visible = true;
            }
            chkExactMatch.Checked = true;
            comboBoxType.SelectedIndex = 0;
            if (incomplete == 2)
            {
                comboBoxType.SelectedIndex = 1;
            }
        }

        private void buttonSaveAsXml_Click(object sender, EventArgs e)
        {
            string functionName = string.Empty;
            closeForm = false;
            if (flagNewEdit == 0)   // Create new function
            {
                functionName = textboxFunctionName.Text.Trim();
            }
            else if (flagNewEdit == 1)
            {
                functionName = comboBoxFunctionName.SelectedItem.ToString();
            }
            if (string.IsNullOrEmpty(functionName))
            {
                MessageBox.Show("Please Enter function name");
                return;
            }
            else if ((functionName.Contains("<") || functionName.Contains(">")))
            {
                MessageBox.Show("Enter valid function name, (Avoid keywords as '<' and '>').");
                textboxFunctionName.Focus();
                return;
            }
            else if (comboBoxType.SelectedItem.Equals("Search and Replace"))
            {
                if (dataGridViewFunction.Rows.Count > 0)
                {
                    for (int i = 0; i < dataGridViewFunction.Rows.Count; i++)
                    {
                        if (dataGridViewFunction.Rows[i].Cells[0].Value.ToString().Trim() == "" || dataGridViewFunction.Rows[i].Cells[1].Value.ToString().Trim() == "")
                        {
                            MessageBox.Show("Fill Data Properly.");
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Fill Data Properly.");
                    return;
                }
            }
            else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
            {
                if (dataGridViewFunction.Rows[0].Cells[0].Value.ToString() == "") { MessageBox.Show("Please add expression."); return; }
                if (dataGridViewFunction.Rows[0].Cells[1].Value.ToString().Trim() == incorrectExpression)
                {
                    MessageBox.Show(invalidExpression);
                    incomplete = 2;
                    return;
                }

            }
            incomplete = 1;
            saveFileDialogExportFunction.Filter = "Xml File |*.xml";
            saveFileDialogExportFunction.FileOk -= new CancelEventHandler(ExportMapping);
            saveFileDialogExportFunction.FileOk += new CancelEventHandler(ExportMapping);
            saveFileDialogExportFunction.ShowDialog();
            return;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                {
                    if (flagNewEdit == 0)   // Create new function
                    {
                        incomplete = 0;

                        if (!string.IsNullOrEmpty(textboxFunctionName.Text.Trim()))
                        {
                            if (!(textboxFunctionName.Text.Contains("<") || textboxFunctionName.Text.Contains(">")))
                            {
                                FetchConcateFunctionName();
                                FetchFunctionName();
                                if (!comboBoxFunctionName.Items.Contains(textboxFunctionName.Text.Trim()))
                                {
                                    if (dataGridViewFunction.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dataGridViewFunction.Rows.Count ; i++)
                                        {
                                            if (dataGridViewFunction.Rows[i].Cells[0].Value.ToString().Trim() == "" || dataGridViewFunction.Rows[i].Cells[1].Value.ToString().Trim() == "")
                                                incomplete++;
                                        }

                                        if (incomplete > 0)
                                        {
                                            closeForm = false;

                                            MessageBox.Show("Fill Data Properly.");
                                            incomplete = 1;
                                        }
                                        else
                                        {
                                            AddOrEditReplaceFunction();
                                            incomplete = 0;
                                            CloseFormWithSuccess();
                                        }
                                    }
                                    else
                                    {
                                        closeForm = false;

                                        MessageBox.Show("Fill Data Properly.");
                                        incomplete = 1;
                                    }
                                }
                                else
                                {
                                    closeForm = false;

                                    MessageBox.Show("Function Name already exists.");
                                    textboxFunctionName.Focus();
                                    textboxFunctionName.Clear();
                                    incomplete = 1;
                                }
                            }
                            else
                            {
                                closeForm = false;

                                MessageBox.Show("Enter valid function name (Avoid keywords as '<' and '>').");
                                textboxFunctionName.Focus();
                                incomplete = 1;
                            }
                        }
                        else
                        {
                            closeForm = false;

                            incomplete = 1;
                            MessageBox.Show("Please enter function name.");
                            textboxFunctionName.Focus();

                        }
                    }
                    else if (flagNewEdit == 1)  // Edit existing function
                    {
                        incomplete = 0;

                        if (comboBoxFunctionName.SelectedIndex > -1)
                        {
                            if (dataGridViewFunction.Rows.Count > 0)
                            {
                                for (int i = 0; i < dataGridViewFunction.Rows.Count; i++)
                                {
                                    if (dataGridViewFunction.Rows[i].Cells[0].Value.ToString().Trim() == "" || dataGridViewFunction.Rows[i].Cells[1].Value.ToString().Trim() == "")
                                        incomplete++;
                                }

                                if (incomplete > 0)
                                {
                                    closeForm = false;

                                    MessageBox.Show("Fill Data Properly");
                                    incomplete = 1;
                                }
                                else
                                {
                                    AddOrEditReplaceFunction(comboBoxFunctionName.SelectedItem.ToString());
                                    //UpdateEditFunction();
                                    incomplete = 0;
                                    CloseFormWithSuccess();
                                }
                            }
                            else
                            {
                                closeForm = false;

                                MessageBox.Show("Fill Data Properly");
                                incomplete = 1;
                            }
                        }
                        else
                        {
                            closeForm = false;

                            incomplete = 1;
                            MessageBox.Show("Please Select Function Name");
                        }
                    }
                }
                else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                {
                    if (dataGridViewFunction.Rows[0].Cells[0].Value == null) { MessageBox.Show("Please add expression."); }
                    string ConcatExpression = dataGridViewFunction.Rows[0].Cells[0].Value.ToString().Trim();
                    if (dataGridViewFunction.Rows[0].Cells[1].Value.ToString().Trim() == incorrectExpression)
                    {
                        closeForm = false;

                        MessageBox.Show(invalidExpression);
                        incomplete = 2;
                    }
                    else if (ConcatExpression.Contains("&&") || ConcatExpression.Contains("\"\""))
                    {
                        closeForm = false;

                        MessageBox.Show(invalidExpression);
                        incomplete = 2;
                    }
                    else if (flagNewEdit == 0)   // Create new function
                    {
                        if (!string.IsNullOrEmpty(textboxFunctionName.Text.Trim()))
                        {
                            if (!(textboxFunctionName.Text.Contains("<") || textboxFunctionName.Text.Contains(">")))
                            {
                                FetchConcateFunctionName();
                                FetchFunctionName();

                                if (!comboBoxFunctionName.Items.Contains(textboxFunctionName.Text.Trim()))
                                {
                                    if (ConcatExpression == "")
                                    {
                                        closeForm = false;

                                        MessageBox.Show("Please add expression.");
                                        incomplete = 2;
                                    }
                                    else
                                    {
                                        List<string> MapColList = DataProcessingBlocks.CommonUtilities.GetInstance().MappingColumnList;

                                        try
                                        {
                                            string[] SplitList = ConcatExpression.Split(new string[] { "&" }, StringSplitOptions.None);
                                            List<string> InvalidColList = new List<string>();

                                            List<string> ColList = new List<string>();
                                            List<string> DeliminatorList = new List<string>();
                                            for (int colIndex = 0; colIndex < SplitList.Length; colIndex++)
                                            {
                                                if (SplitList[colIndex].Length <= 1)
                                                {
                                                    closeForm = false;

                                                    MessageBox.Show(invalidExpression);
                                                    incomplete = 2;
                                                }
                                                else if (SplitList[colIndex].StartsWith("\"") && SplitList[colIndex].EndsWith("\""))
                                                {
                                                    string Deliminator = SplitList[colIndex].Replace("\"", string.Empty).Trim();
                                                    DeliminatorList.Add(Deliminator);
                                                }
                                                else if (SplitList[colIndex].StartsWith("{") && SplitList[colIndex].EndsWith("}"))
                                                {
                                                    string ColumnName = SplitList[colIndex].Replace("{", string.Empty).Replace("}", string.Empty).Trim();
                                                    ColList.Add(ColumnName);
                                                }
                                                else
                                                {
                                                    closeForm = false;

                                                    MessageBox.Show(invalidExpression);
                                                    incomplete = 2;
                                                }
                                            }
                                            //754
                                            if (DeliminatorList.Count < ColList.Count)
                                            {
                                                if (!(DeliminatorList.Count == ColList.Count - 1))
                                                {
                                                    incomplete = 2;
                                                    closeForm = false;

                                                    MessageBox.Show(invalidExpression);
                                                }
                                            }

                                            foreach (var item in ColList)
                                            {
                                                if (item == "")
                                                {
                                                    closeForm = false;

                                                    incomplete = 2;
                                                    MessageBox.Show(invalidExpression);
                                                }
                                                if (!MapColList.Contains(item))
                                                    InvalidColList.Add(item);
                                            }
                                            if (InvalidColList.Count > 0)
                                            {
                                                incomplete = 2;
                                                closeForm = false;

                                                MessageBox.Show(invalidExpression + ", Column Names " + string.Join(",", InvalidColList) + " are invalid for this map");
                                            }
                                            else
                                            {
                                                incomplete = -1; //Valid Expression
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            incomplete = 2;
                                            closeForm = false;

                                            MessageBox.Show(invalidExpression);
                                        }
                                    }
                                }
                                else
                                {
                                    incomplete = 2;
                                    closeForm = false;

                                    MessageBox.Show("Function Name already exists.");
                                    textboxFunctionName.Focus();
                                    textboxFunctionName.Clear();
                                }
                            }
                            else
                            {
                                incomplete = 2;
                                closeForm = false;

                                MessageBox.Show("Enter valid concatenate function name (Avoid keywords as '<' and '>').");
                                textboxFunctionName.Focus();
                            }
                        }
                    }
                    else if (flagNewEdit == 1)   // Create new function
                    {
                        List<string> MapColList = DataProcessingBlocks.CommonUtilities.GetInstance().MappingColumnList;

                        try
                        {
                            string[] SplitList = ConcatExpression.Split(new string[] { "&" }, StringSplitOptions.None);
                            List<string> InvalidColList = new List<string>();

                            List<string> ColList = new List<string>();
                            List<string> DeliminatorList = new List<string>();
                            for (int colIndex = 0; colIndex < SplitList.Length; colIndex++)
                            {
                                if (SplitList[colIndex].Length <= 1)
                                {
                                    MessageBox.Show(invalidExpression);
                                    incomplete = 2;
                                }
                                else if (SplitList[colIndex].StartsWith("\"") && SplitList[colIndex].EndsWith("\""))
                                {
                                    string Deliminator = SplitList[colIndex].Replace("\"", string.Empty).Trim();
                                    DeliminatorList.Add(Deliminator);
                                }
                                else if (SplitList[colIndex].StartsWith("{") && SplitList[colIndex].EndsWith("}"))
                                {
                                    string ColumnName = SplitList[colIndex].Replace("{", string.Empty).Replace("}", string.Empty).Trim();
                                    ColList.Add(ColumnName);
                                }
                                else
                                {
                                    closeForm = false;

                                    MessageBox.Show(invalidExpression);
                                    incomplete = 2;
                                }
                            }
                            //754
                            if (DeliminatorList.Count < ColList.Count)
                            {
                                if (!(DeliminatorList.Count == ColList.Count - 1))
                                {
                                    closeForm = false;

                                    incomplete = 2;
                                    MessageBox.Show(invalidExpression);
                                }
                            }

                            foreach (var item in ColList)
                            {
                                if (item == "")
                                {
                                    closeForm = false;

                                    incomplete = 2;
                                    MessageBox.Show(invalidExpression);
                                }
                                if (!MapColList.Contains(item))
                                    InvalidColList.Add(item);

                            }
                            if (InvalidColList.Count > 0)
                            {
                                closeForm = false;

                                incomplete = 2;
                                MessageBox.Show(invalidExpression + ", Column Names " + string.Join(",", InvalidColList) + " are invalid for this map");
                            }
                            else
                            {
                                incomplete = -1; //Valid Expression
                            }
                        }
                        catch (Exception ex)
                        {
                            closeForm = false;

                            incomplete = 2;
                            MessageBox.Show(invalidExpression);
                        }
                    }
                    else
                    {
                        closeForm = false;

                        incomplete = 2;
                        MessageBox.Show("Please enter Concatenate Function name.");
                        textboxFunctionName.Focus();
                    }
                    if (flagNewEdit == 0 && incomplete == -1)   // Create new ConcatenateFunction
                    {
                        AddNewOrEditConcateFunction();
                        CloseFormWithSuccess();
                    }
                    else if (flagNewEdit == 1 && incomplete == -1)  // Edit existing ConcatenateFunction
                    {
                        AddNewOrEditConcateFunction(comboBoxFunctionName.SelectedItem.ToString(), ConcatExpression);
                        CloseFormWithSuccess();
                    }
                }
            }

            catch (Exception okException)
            {
                closeForm = false;

                incomplete = 1;
                MessageBox.Show(okException.Message.ToString());
            }
        }

        private void CloseFormWithSuccess()
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        #region Private Methods

        /// <summary>
        /// Method is used to fetch data related to function selected using ComboBox
        /// </summary>
        /// <param name="functionName"></param>
        private void GetFunctionData(string functionName)
        {
            try
            {
                if (!string.IsNullOrEmpty(GetFunctionFilePath()))
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(GetFunctionFilePath());

                    XmlNode root = xdoc.DocumentElement;

                    XmlNodeList retList = root.SelectNodes("//Function");

                    DataTable dt = new DataTable();
                    DataColumn dc = new DataColumn("search");
                    DataColumn dc1 = new DataColumn("replace");

                    dt.Columns.Add(dc);
                    dt.Columns.Add(dc1);

                    string importType = string.Empty;
                    if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                        importType = Mappings.GetInstance().comboboxImportTypeValue;
                    else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                        importType = Mappings.GetInstance().textboxImportTypeValue;

                    for (int j = 0; j < retList.Count; j++)
                    {
                        XmlNode node = retList.Item(j);

                            if (functionName.Equals(node.SelectSingleNode("./Name").InnerText))
                            {
                                XmlNode exactMatch = node.SelectSingleNode("./ExactMatch");
                                if(exactMatch==null)
                                {
                                    chkExactMatch.Checked = false;
                                }
                               else if (exactMatch.InnerText=="true")
                                {
                                    chkExactMatch.Checked = true;
                                }
                                else
                                {
                                    chkExactMatch.Checked = false;
                                }
                                XmlNodeList searchlist = node.SelectNodes("./string");

                                for (int list = 0; list < searchlist.Count; list++)
                                {
                                    XmlNodeList searchnodeRetList = searchlist[list].SelectNodes("./search");
                                    XmlNodeList replacenodeRetList = searchlist[list].SelectNodes("./replace");

                                    if (searchnodeRetList.Count == replacenodeRetList.Count)
                                    {
                                        int count = replacenodeRetList.Count;

                                        for (int k = 0; k < count; k++)
                                        {
                                            DataRow dr = dt.NewRow();

                                            dr["search"] = searchnodeRetList.Item(k).InnerText;
                                            dr["replace"] = replacenodeRetList.Item(k).InnerText;

                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }
                            }
                        
                    }
                    dataGridViewFunction.DataSource = dt;
                }
            }
            catch (Exception dataException)
            {
                incomplete = 1;
                MessageBox.Show(dataException.Message.ToString());
            }
        }

        private string ExpressionPreview(string Expression)
        {
            string Preview = "";

            if (Expression == "") { return ""; }
            if (Expression.Contains("&&") || Expression.Contains("\"\""))
            {
                return Preview = incorrectExpression;
            }
            else
            {
                List<string> MapColList = CommonUtilities.GetInstance().MappingColumnList;
                string[] SplitList = Expression.Split(new string[] { "&" }, StringSplitOptions.None);
                List<string> InvalidColList = new List<string>();

                List<string> ColList = new List<string>();
                List<string> DeliminatorList = new List<string>();
                //754
                for (int colIndex = 0; colIndex < SplitList.Length; colIndex++)
                {
                    if (SplitList[colIndex].Length <= 1)
                    {
                        return Preview = incorrectExpression;
                    }
                    else if (SplitList[colIndex].StartsWith("\"") && SplitList[colIndex].EndsWith("\""))
                    {
                        string Deliminator = SplitList[colIndex].Replace("\"", string.Empty);
                        if (Deliminator == "") DeliminatorList.Add(" ");
                        else DeliminatorList.Add(Deliminator);
                    }
                    else if (SplitList[colIndex].StartsWith("{") && SplitList[colIndex].EndsWith("}"))
                    {
                        string ColumnName = SplitList[colIndex].Replace("{", string.Empty).Replace("}", string.Empty).Trim();
                        if (ColumnName != "") ColList.Add(ColumnName);
                        else { return Preview = incorrectExpression; }
                    }
                    else
                    {
                        return Preview = incorrectExpression;
                    }
                }

                if (DeliminatorList.Count < ColList.Count)
                {
                    if (!(DeliminatorList.Count == ColList.Count - 1))
                    {
                        return Preview = incorrectExpression;
                    }
                }

                foreach (var item in ColList)
                {
                    if (!MapColList.Contains(item))
                        InvalidColList.Add(item);
                }
                if (InvalidColList.Count > 0)
                {
                    return Preview = incorrectExpression;
                }
                else
                {
                    List<string> PreviewValueList = CommonUtilities.GetInstance().GetPreviewList;
                    int j = 0; int k = 0;
                    for (int i = 0; i < SplitList.Length; i++)
                    {
                        if (SplitList[i].StartsWith("\"") && SplitList[i].EndsWith("\""))
                        {
                            Preview += DeliminatorList[j];
                            j++;
                        }
                        if (SplitList[i].StartsWith("{") && SplitList[i].EndsWith("}"))
                        {
                            int index = MapColList.IndexOf(ColList[k]);
                            Preview += PreviewValueList[index - 1];
                            k++;
                        }
                    }

                    //  dataGridViewFunction.Rows[0].Cells[1].Value = Preview;
                }
            }

            return Preview;
        }

        /// <summary>
        /// Method is used to fetch data related to ConcatenateFunction selected using ComboBox
        /// </summary>
        /// <param name="ConcatefunctionName"></param>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        private void GetConcateFunctionData(string ConcatefunctionName)
        {
            try
            {
                if (!string.IsNullOrEmpty(GetConcateFunctionFilePath()))
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(GetConcateFunctionFilePath().ToString());

                    XmlNode root = xdoc.DocumentElement;

                    XmlNodeList retList = root.SelectNodes("//ConcatenateFunction");

                    DataTable dt = new DataTable();
                    DataColumn dc = new DataColumn("Expression");
                    DataColumn dc1 = new DataColumn("Preview");

                    dt.Columns.Add(dc);
                    dt.Columns.Add(dc1);

                    string importType = string.Empty;
                    if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                        importType = Mappings.GetInstance().comboboxImportTypeValue;
                    else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                        importType = Mappings.GetInstance().textboxImportTypeValue;

                    for (int j = 0; j < retList.Count; j++)
                    {
                        XmlNode node = retList.Item(j);

                        if (node.SelectSingleNode("./ImportType").InnerText.Equals(importType) && node.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                        {
                            if (ConcatefunctionName.Equals(node.SelectSingleNode("./Name").InnerText))
                            {
                                XmlNodeList searchlist = node.SelectNodes("./string");

                                for (int list = 0; list < searchlist.Count; list++)
                                {
                                    XmlNodeList searchnodeRetList = searchlist[list].SelectNodes("./expression");
                                    XmlNodeList replacenodeRetList = searchlist[list].SelectNodes("./preview");


                                    int count = searchnodeRetList.Count;

                                    for (int k = 0; k < count; k++)
                                    {
                                        DataRow dr = dt.NewRow();

                                        dr["Expression"] = searchnodeRetList.Item(k).InnerText;
                                        dr["Preview"] = ExpressionPreview(searchnodeRetList.Item(k).InnerText);
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                    dataGridViewFunction.DataSource = dt;
                    dataGridViewFunction.AllowAddNewRow = false;
                    dataGridViewFunction.AllowDeleteRow = false;
                }
            }
            catch (Exception dataException)
            {
                incomplete = 1;
                MessageBox.Show(dataException.Message.ToString());
            }
        }
        /// <summary>
        /// Method is used to update/Edit Selected Function
        /// </summary>
        /// <returns></returns>
        private bool UpdateEditFunction()
        {
            bool flag = false;
            System.Xml.XmlDocument xdoc = new XmlDocument();
            try
            {
                #region code to delete function

                xdoc.Load(GetFunctionFilePath().ToString());

                XmlNode root = xdoc.DocumentElement;
                XmlNodeList functionRetList = root.SelectNodes("//Function");
                int i = 0;

                for (i = 0; i < functionRetList.Count; i++)
                {
                    XmlNode node = functionRetList.Item(i);

                    //To get Function based on MappingType
                    //if (node.SelectSingleNode("./ImportType") != null && node.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                    //{
                        if (comboBoxFunctionName.SelectedItem.ToString().Equals(node.SelectSingleNode("./Name").InnerText))
                        {
                            node.RemoveAll();
                            flag = true;
                            break;
                        }
                   // }
                }

                #endregion

                #region Append updated data to function

                XmlElement nameelement = xdoc.CreateElement("Name");
                nameelement.InnerText = comboBoxFunctionName.SelectedItem.ToString();
                xdoc.GetElementsByTagName("Function")[i].AppendChild(nameelement);

                XmlElement mappingType = xdoc.CreateElement("MappingType");
                xdoc.GetElementsByTagName("Function")[i].AppendChild(mappingType);
                mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                XmlElement importType = xdoc.CreateElement("ImportType");
                xdoc.GetElementsByTagName("Function")[i].AppendChild(importType);

                if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                    importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                    importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;

                for (int count = 0; count < dataGridViewFunction.Rows.Count - 1; count++)
                {
                    XmlElement stringElement = xdoc.CreateElement("string");
                    xdoc.GetElementsByTagName("Function")[i].AppendChild(stringElement);

                    XmlElement searchElement = xdoc.CreateElement("search");
                    searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                    stringElement.AppendChild(searchElement);

                    XmlElement replaceElement = xdoc.CreateElement("replace");
                    replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                    stringElement.AppendChild(replaceElement);

                }

                //  xdoc.GetElementsByTagName("function")[i].AppendChild(newelement);

                #endregion

            }
            catch (Exception newEx)
            {
                incomplete = 1;
            }

            if (flag)
            {
                xdoc.Save(GetFunctionFilePath().ToString());
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Method is used to update/Edit  Selected  Concat Function
        /// </summary>
        /// <returns></returns>
        private bool UpdateEditConcatFunction()
        {
            bool flag = false;
            System.Xml.XmlDocument xdoc = new XmlDocument();
            try
            {
                #region code to delete function

                xdoc.Load(GetConcateFunctionFilePath().ToString());

                XmlNode root = xdoc.DocumentElement;
                XmlNodeList functionRetList = root.SelectNodes("//ConcatenateFunction");
                int i = 0;

                for (i = 0; i < functionRetList.Count; i++)
                {
                    XmlNode node = functionRetList.Item(i);

                    //To get Function based on MappingType
                    if (node.SelectSingleNode("./ImportType") != null && node.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                    {
                        if (comboBoxFunctionName.SelectedItem.ToString().Equals(node.SelectSingleNode("./Name").InnerText))
                        {
                            node.RemoveAll();
                            flag = true;
                            break;
                        }
                    }
                }

                #endregion

                #region Append updated data to function

                XmlElement nameelement = xdoc.CreateElement("Name");
                nameelement.InnerText = comboBoxFunctionName.SelectedItem.ToString();
                xdoc.GetElementsByTagName("ConcatenateFunction")[i].AppendChild(nameelement);

                XmlElement mappingType = xdoc.CreateElement("MappingType");
                xdoc.GetElementsByTagName("ConcatenateFunction")[i].AppendChild(mappingType);
                mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                XmlElement importType = xdoc.CreateElement("ImportType");
                xdoc.GetElementsByTagName("ConcatenateFunction")[i].AppendChild(importType);

                if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                    importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                    importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;

                for (int count = 0; count < dataGridViewFunction.Rows.Count - 1; count++)
                {
                    XmlElement stringElement = xdoc.CreateElement("string");
                    xdoc.GetElementsByTagName("ConcatenateFunction")[i].AppendChild(stringElement);

                    XmlElement searchElement = xdoc.CreateElement("expression");
                    searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                    stringElement.AppendChild(searchElement);

                    XmlElement replaceElement = xdoc.CreateElement("preview");
                    replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                    stringElement.AppendChild(replaceElement);

                }

                //  xdoc.GetElementsByTagName("function")[i].AppendChild(newelement);

                #endregion

            }
            catch (Exception newEx)
            {
                MessageBox.Show(newEx.Message.ToString());
                incomplete = 1;
            }

            if (flag)
            {
                xdoc.Save(GetFunctionFilePath().ToString());
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Method is used to fetch function names from xml file
        /// </summary>
        private void FetchFunctionName()
        {
            try
            {
                if (!string.IsNullOrEmpty(GetFunctionFilePath()))
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(GetFunctionFilePath().ToString());
                    // comboBoxFunctionName.SelectedIndex = -1;
                    comboBoxFunctionName.Items.Clear();
                    XmlNode root = xdoc.DocumentElement;
                    XmlNodeList functionList = xdoc.GetElementsByTagName("Functions");
                    XmlNodeList functionRetList = root.SelectNodes("//Function");
                    string importType = string.Empty;
                    if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                        importType = Mappings.GetInstance().comboboxImportTypeValue;
                    else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                        importType = Mappings.GetInstance().textboxImportTypeValue;

                    for (int j = 0; j < functionRetList.Count; j++)
                    {
                        XmlNode node = functionRetList.Item(j);
                            if (node.SelectSingleNode("./Name") != null)
                            {
                                comboBoxFunctionName.Items.Add(node.SelectSingleNode("./Name").InnerText);
                            }
                    }
                }
            }
            catch (Exception functionNameEx)
            {
                incomplete = 1;
            }
        }

        private void FetchConcateFunctionName()
        {
            try
            {
                if (!string.IsNullOrEmpty(GetConcateFunctionFilePath()))
                {
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(GetConcateFunctionFilePath().ToString());
                    // comboBoxFunctionName.SelectedIndex = -1;
                    comboBoxFunctionName.Items.Clear();
                    XmlNode root = xdoc.DocumentElement;
                    XmlNodeList functionList = xdoc.GetElementsByTagName("ConcatenateFunctions");
                    XmlNodeList functionRetList = root.SelectNodes("//ConcatenateFunction");

                    string importType = string.Empty;
                    if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                        importType = Mappings.GetInstance().comboboxImportTypeValue;
                    else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                        importType = Mappings.GetInstance().textboxImportTypeValue;

                    for (int j = 0; j < functionRetList.Count; j++)
                    {
                        XmlNode node = functionRetList.Item(j);

                        if (node.SelectSingleNode("./ImportType") != null && node.SelectSingleNode("./ImportType").InnerText.Equals(importType))
                        {
                            if (node.SelectSingleNode("./Name") != null && node.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                            {
                                comboBoxFunctionName.Items.Add(node.SelectSingleNode("./Name").InnerText);
                            }
                        }
                    }
                }
            }
            catch (Exception functionNameEx)
            {
                incomplete = 1;
            }
        }

        /// <summary>
        /// Method is used to add new function in Function.xml
        /// </summary>
        private void AddOrEditReplaceFunction(string FunctionName = "")
        {
            try
            {
                if (textboxFunctionName.Text != null)
                {

                    //Axis 10.0

                    string logDirectory = string.Empty;
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (logDirectory.Contains("Program Files (x86)"))
                        {
                            logDirectory = logDirectory.Replace("Program Files (x86)", Constants.xpPath);
                            logDirectory += @"\Function.xml";
                        }
                        else
                        {
                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            logDirectory += @"\Function.xml";
                        }
                    }
                    else
                    {
                        logDirectory = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                        logDirectory += @"\Function.xml";
                    }

                    assemblyVersion = Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);

                    //Filepath to create Function.xml file
                    string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory : logDirectory;

                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            filePath = filePath.Replace("Program Files", Constants.xpPath);
                        else
                            filePath = filePath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                    catch
                    {
                        incomplete = 1;
                    }


                    try
                    {
                        XmlDocument xdoc = new XmlDocument();

                        //check whether file exists or not
                        if (!File.Exists(filePath))
                        {
                            XmlElement functionsElement = xdoc.CreateElement("Functions");
                            xdoc.AppendChild(functionsElement);

                            //Axis 10.0
                            XmlAttribute attribute = functionsElement.OwnerDocument.CreateAttribute("version");
                            attribute.Value = assemblyVersion;
                            functionsElement.Attributes.Append(attribute);

                            XmlElement functionElement = xdoc.CreateElement("Function");
                            functionsElement.AppendChild(functionElement);

                            string functionName = textboxFunctionName.Text.Trim();
                            XmlElement nameelement = xdoc.CreateElement("Name");
                            SelectedFunction = nameelement.InnerText = functionName;
                            functionElement.AppendChild(nameelement);


                            XmlElement mappingType = xdoc.CreateElement("MappingType");
                            functionElement.AppendChild(mappingType);

                            // Mapping type value from mapping form
                            mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                            XmlElement importType = xdoc.CreateElement("ImportType");
                            functionElement.AppendChild(importType);

                            //Check Mapping mode, if edit then fetch import type from TextBox
                            // if new then fetch import type from comboBox.


                            if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                                importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                            else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                                importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;

                            // Exact Match 
                            XmlElement exactMatch = xdoc.CreateElement("ExactMatch");
                            functionElement.AppendChild(exactMatch);
                            if (chkExactMatch.Checked == true)
                            {
                                exactMatch.InnerText = "true";
                            }
                            else
                            {
                                exactMatch.InnerText = "false";
                            }

                            for (int count = 0; count < dataGridViewFunction.Rows.Count - 1; count++)
                            {
                                XmlElement stringElement = xdoc.CreateElement("string");
                                functionElement.AppendChild(stringElement);

                                XmlElement searchElement = xdoc.CreateElement("search");
                                searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                                stringElement.AppendChild(searchElement);

                                XmlElement replaceElement = xdoc.CreateElement("replace");
                                replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                                stringElement.AppendChild(replaceElement);
                            }

                            //xdoc.AppendChild(functionElement);
                            xdoc.Save(filePath);
                            incomplete = 0;
                        }
                        else
                        {
                            xdoc.Load(filePath);

                            if (FunctionName != "")
                            {
                                XmlNode root = xdoc.DocumentElement;
                                XmlNodeList functionRetList = root.SelectNodes("//Function");
                                int i = 0;
                                for (i = 0; i < functionRetList.Count; i++)
                                {
                                    XmlNode node1 = functionRetList.Item(i);

                                    //To get Function based on MappingType
                                        if (comboBoxFunctionName.SelectedItem.ToString().Equals(node1.SelectSingleNode("./Name").InnerText))
                                        {
                                                // Exact Match 
                                                XmlElement exactMatch = (XmlElement)node1.SelectSingleNode("./ExactMatch");
                                                if (exactMatch != null)
                                                {
                                                    if (chkExactMatch.Checked == true)
                                                    {
                                                        exactMatch.InnerText = "true";
                                                    }
                                                    else
                                                    {
                                                        exactMatch.InnerText = "false";
                                                    }
                                                }
                                                else
                                                {
                                                    // Exact Match 
                                                    var existingNamenode = node1.SelectSingleNode("./Name");
                                                    XmlElement AddExactMatch = xdoc.CreateElement("ExactMatch");
                                                    node1.InsertAfter(AddExactMatch, existingNamenode);
                                                   // node1.AppendChild(AddExactMatch);
                                                    if (chkExactMatch.Checked == true)
                                                    {
                                                        AddExactMatch.InnerText = "true";
                                                    }
                                                    else
                                                    {
                                                        AddExactMatch.InnerText = "false";
                                                    }
                                                }
                                                   

                                                if (FunctionName.Equals(node1.SelectSingleNode("./Name").InnerText))
                                                {
                                                       //Axis 752
                                                        XmlNodeList nodes = node1.SelectNodes("./string");
                                                        for (int nodeCount = nodes.Count - 1; nodeCount >= 0; nodeCount--)
                                                        {
                                                            nodes[nodeCount].ParentNode.RemoveChild(nodes[nodeCount]);
                                                        }
                                                    

                                                    for (int count = 0; count < dataGridViewFunction.Rows.Count; count++)
                                                    {
                                                        if (dataGridViewFunction.Rows[count].Cells[0].Value != null && dataGridViewFunction.Rows[count].Cells[1].Value != null)
                                                        {
                                                            XmlElement stringElement = xdoc.CreateElement("string");
                                                            node1.AppendChild(stringElement);

                                                            XmlElement searchElement = xdoc.CreateElement("search");
                                                            searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                                                            stringElement.AppendChild(searchElement);

                                                            XmlElement replaceElement = xdoc.CreateElement("replace");
                                                            replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                                                            stringElement.AppendChild(replaceElement);
                                                        }
                                                    }
                                                }
                                            
                                        }
                                }
                            }
                            else
                            {
                                XmlElement node = xdoc.DocumentElement;

                                XmlElement functionElement = xdoc.CreateElement("Function");
                                node.AppendChild(functionElement);

                                string functionName = textboxFunctionName.Text.Trim();
                                XmlElement nameelement = xdoc.CreateElement("Name");
                                SelectedFunction = nameelement.InnerText = functionName;
                                functionElement.AppendChild(nameelement);

                                XmlElement mappingType = xdoc.CreateElement("MappingType");
                                functionElement.AppendChild(mappingType);

                                mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                                XmlElement importType = xdoc.CreateElement("ImportType");
                                functionElement.AppendChild(importType);

                                // Exact Match 
                                XmlElement exactMatch = xdoc.CreateElement("ExactMatch");
                                functionElement.AppendChild(exactMatch);
                                if (chkExactMatch.Checked == true)
                                {
                                    exactMatch.InnerText = "true";
                                }
                                else
                                {
                                    exactMatch.InnerText = "false";
                                }

                                if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                                    importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                                else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                                    importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;

                                for (int count = 0; count < dataGridViewFunction.Rows.Count; count++)
                                {
                                    XmlElement stringElement = xdoc.CreateElement("string");
                                    functionElement.AppendChild(stringElement);

                                    XmlElement searchElement = xdoc.CreateElement("search");
                                    searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                                    stringElement.AppendChild(searchElement);

                                    XmlElement replaceElement = xdoc.CreateElement("replace");
                                    replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                                    stringElement.AppendChild(replaceElement);
                                }
                            }
                            xdoc.Save(filePath);
                            incomplete = 0;
                        }
                    }
                    catch (Exception newException)
                    {
                        incomplete = 1;

                    }
                }
            }
            catch (Exception addException)
            {

                incomplete = 1;
            }
        }

        private void AddNewOrEditConcateFunction(string FunctionName = "", string Expression = "")
        {
            try
            {


                string logDirectory = string.Empty;
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (logDirectory.Contains("Program Files (x86)"))
                    {
                        logDirectory = logDirectory.Replace("Program Files (x86)", Constants.xpPath);
                        logDirectory += @"\ConcatenateFunction.xml";
                    }
                    else
                    {
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        logDirectory += @"\ConcatenateFunction.xml";
                    }
                }
                else
                {
                    logDirectory = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                    logDirectory += @"\ConcatenateFunction.xml";
                }

                assemblyVersion = Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);

                //Filepath to create ConcatenateFunction.xml file
                string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory : logDirectory;

                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        filePath = filePath.Replace("Program Files", Constants.xpPath);
                    else
                        filePath = filePath.Replace("Program Files", "Users\\Public\\Documents");
                }
                catch
                {
                    incomplete = 1;
                }

                try
                {
                    XmlDocument xdoc = new XmlDocument();

                    //check whether file exists or not
                    if (!File.Exists(filePath))
                    {
                        XmlElement functionsElement = xdoc.CreateElement("ConcatenateFunctions");
                        xdoc.AppendChild(functionsElement);

                        //Axis 10.0
                        XmlAttribute attribute = functionsElement.OwnerDocument.CreateAttribute("version");
                        attribute.Value = assemblyVersion;
                        functionsElement.Attributes.Append(attribute);

                        XmlElement functionElement = xdoc.CreateElement("ConcatenateFunction");
                        functionsElement.AppendChild(functionElement);

                        string functionName = textboxFunctionName.Text.Trim();
                        XmlElement nameelement = xdoc.CreateElement("Name");
                        SelectedFunction = nameelement.InnerText = functionName;
                        functionElement.AppendChild(nameelement);


                        XmlElement mappingType = xdoc.CreateElement("MappingType");
                        functionElement.AppendChild(mappingType);

                        // Mapping type value from mapping form
                        mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                        XmlElement importType = xdoc.CreateElement("ImportType");
                        functionElement.AppendChild(importType);

                        //Check Mapping mode, if edit then fetch import type from TextBox
                        // if new then fetch import type from comboBox.


                        if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                            importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                        else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                            importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;


                        XmlElement stringElement = xdoc.CreateElement("string");
                        functionElement.AppendChild(stringElement);

                        XmlElement searchElement = xdoc.CreateElement("expression");
                        searchElement.InnerText = dataGridViewFunction.Rows[0].Cells[0].Value.ToString().Trim();
                        stringElement.AppendChild(searchElement);

                        xdoc.Save(filePath);
                        incomplete = 0;
                    }
                    else
                    {
                        xdoc.Load(filePath);

                        XmlNode root = xdoc.DocumentElement;
                        if (Expression != "" && FunctionName != "")
                        {
                            XmlNodeList functionRetList = root.SelectNodes("//ConcatenateFunction");
                            int i = 0;

                            for (i = 0; i < functionRetList.Count; i++)
                            {
                                XmlNode node = functionRetList.Item(i);
                                if (node.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                                {
                                    if (FunctionName.Equals(node.SelectSingleNode("./Name").InnerText))
                                    {
                                        XmlNodeList searchlist = node.SelectNodes("./string");

                                        for (int list = 0; list < searchlist.Count; list++)
                                        {
                                            XmlNodeList expressionNodeRetList = searchlist[list].SelectNodes("./expression");

                                            int count = expressionNodeRetList.Count;

                                            for (int k = 0; k < count; k++)
                                            {
                                                expressionNodeRetList.Item(k).InnerText = Expression;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            XmlElement node = xdoc.DocumentElement;

                            XmlElement functionElement = xdoc.CreateElement("ConcatenateFunction");
                            node.AppendChild(functionElement);

                            string functionName = textboxFunctionName.Text.Trim();
                            XmlElement nameelement = xdoc.CreateElement("Name");
                            SelectedFunction = nameelement.InnerText = functionName;
                            functionElement.AppendChild(nameelement);

                            XmlElement mappingType = xdoc.CreateElement("MappingType");
                            functionElement.AppendChild(mappingType);

                            mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();

                            XmlElement importType = xdoc.CreateElement("ImportType");
                            functionElement.AppendChild(importType);

                            if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("New"))
                                importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                            else if (TransactionImporter.Mappings.GetInstance().Mode.ToString().Equals("Select"))
                                importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;


                            XmlElement stringElement = xdoc.CreateElement("string");
                            functionElement.AppendChild(stringElement);

                            XmlElement searchElement = xdoc.CreateElement("expression");
                            searchElement.InnerText = dataGridViewFunction.Rows[0].Cells[0].Value.ToString().Trim();
                            stringElement.AppendChild(searchElement);
                        }

                        xdoc.Save(filePath);
                        incomplete = 0;
                    }
                }
                catch (Exception newException)
                {
                    incomplete = 1;
                    MessageBox.Show(newException.Message.ToString());
                }

            }
            catch (Exception addException)
            {
                MessageBox.Show(addException.Message.ToString());
                incomplete = 1;
            }
        }


        /// <summary>
        /// Method is used to add new function in Function.xml
        /// </summary>
        private void deleteConcatOrReplaceFunctions(string FunctionName)
        {
            try
            {
                if (FunctionName != null)
                {
                    string logDirectory = string.Empty;
                    string logDirectoryConcat = string.Empty;

                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (logDirectory.Contains("Program Files (x86)"))
                        {

                            logDirectoryConcat = logDirectory.Replace("Program Files (x86)", Constants.xpPath);

                            logDirectory = logDirectory.Replace("Program Files (x86)", Constants.xpPath);
                            logDirectory += @"\Function.xml";
                            logDirectoryConcat += @"\ConcatenateFunction.xml";

                        }
                        else
                        {
                            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                            logDirectoryConcat = logDirectory.Replace("Program Files", Constants.xpPath);


                            logDirectory += @"\Function.xml";
                            logDirectoryConcat += @"\ConcatenateFunction.xml";

                        }
                    }
                    else
                    {
                        logDirectory = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                        logDirectory += @"\Function.xml";

                        logDirectoryConcat = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                        logDirectoryConcat += @"\ConcatenateFunction.xml";
                    }



                    //Remove Function from Function.Xml
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        //Filepath to create Function.xml file
                        string filePath = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory : logDirectory;

                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                filePath = filePath.Replace("Program Files", Constants.xpPath);
                            else
                                filePath = filePath.Replace("Program Files", "Users\\Public\\Documents");
                        }
                        catch
                        {
                        }
                        try
                        {
                            XmlDocument xdoc = new XmlDocument();
                            xdoc.Load(filePath);
                            if (FunctionName != "")
                            {
                                XmlNode root = xdoc.DocumentElement;
                                XmlNodeList functionRetList = root.SelectNodes("//Function");
                                int i = 0;
                                for (i = 0; i < functionRetList.Count; i++)
                                {
                                    XmlNode node1 = functionRetList.Item(i);
                                    //if (node1.SelectSingleNode("./ImportType") != null && node1.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                                    //{
                                        //if (comboBoxFunctionName.SelectedItem.ToString().Equals(node1.SelectSingleNode("./Name").InnerText))
                                        //{
                                                if (FunctionName.Equals(node1.SelectSingleNode("./Name").InnerText))
                                                {
                                                    XmlElement searchlist = (XmlElement)node1.SelectSingleNode("./string");
                                                    if (searchlist != null) node1.ParentNode.RemoveChild(node1);
                                                }
                                       // }
                                   // }
                                }
                            }
                            xdoc.Save(filePath);
                        }

                        catch (Exception newException)
                        {
                            incomplete = 1;
                            // MessageBox.Show(newException.Message.ToString());
                        }

                        // Delete references of Function template from mappings
                        string m_settingsPath = CommonUtilities.GetInstance().getSettingFilePath();

                        XmlDocument xdoc1 = new XmlDocument();
                        xdoc1.Load(m_settingsPath);
                        XmlNode root1 = xdoc1.DocumentElement;

                        string constantColumnList = string.Empty;

                        for (int count = 0; count < root1.ChildNodes.Count; count++)
                        {
                            if (root1.ChildNodes.Item(count).Name == "FunctionMappings")
                            {
                                XmlNodeList userMappingsXML = root1.ChildNodes.Item(count).ChildNodes;
                                foreach (XmlNode node in userMappingsXML)
                                {

                                    for (int childcount = 0; childcount < node.ChildNodes.Count; childcount++)
                                    {
                                        if (!node.SelectSingleNode("./MappingType").InnerText.Equals(CommonUtilities.GetInstance().ConnectedSoftware)) { break; }
                                        if (!node.ChildNodes[childcount].Name.Equals("MappingName"))
                                        {
                                            if (!node.ChildNodes[childcount].Name.Equals("MappingType"))
                                            {
                                                if (!node.ChildNodes[childcount].Name.Equals("ImportType"))
                                                {
                                                    string functionNameinXml = node.ChildNodes[childcount].InnerText;
                                                    if (!string.IsNullOrEmpty(functionNameinXml))
                                                    {
                                                        if (functionNameinXml.Equals(FunctionName)) node.ChildNodes[childcount].InnerText = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        xdoc1.Save(m_settingsPath);
                    }
                    else
                    {
                        try
                        {
                            //Filepath to create Function.xml file
                            string filePath = logDirectoryConcat.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectoryConcat : logDirectoryConcat;

                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                    filePath = filePath.Replace("Program Files", Constants.xpPath);
                                else
                                    filePath = filePath.Replace("Program Files", "Users\\Public\\Documents");
                            }
                            catch
                            {
                            }
                            XmlDocument xdoc = new XmlDocument();
                            xdoc.Load(filePath);
                            if (FunctionName != "")
                            {
                                XmlNode root = xdoc.DocumentElement;
                                XmlNodeList functionRetList = root.SelectNodes("//ConcatenateFunction");
                                int i = 0;
                                for (i = 0; i < functionRetList.Count; i++)
                                {
                                    XmlNode node1 = functionRetList.Item(i);
                                    if (node1.SelectSingleNode("./ImportType") != null && node1.SelectSingleNode("./MappingType").InnerText.Equals(TransactionImporter.Mappings.GetInstance().ConnectedSoft))
                                    {
                                        if (FunctionName.Equals(node1.SelectSingleNode("./Name").InnerText))
                                        {
                                            XmlElement searchlist = (XmlElement)node1.SelectSingleNode("./string");
                                            if (searchlist != null) node1.ParentNode.RemoveChild(node1);
                                        }
                                    }
                                }
                            }
                            xdoc.Save(filePath);
                        }
                        catch (Exception newException)
                        {
                            incomplete = 1;
                            // MessageBox.Show(newException.Message.ToString());
                        }

                        // Delete references of Function template from mappings
                        string m_settingsPath = CommonUtilities.GetInstance().getSettingFilePath();

                        XmlDocument xdoc1 = new XmlDocument();
                        xdoc1.Load(m_settingsPath);
                        XmlNode root1 = xdoc1.DocumentElement;

                        string constantColumnList = string.Empty;

                        for (int count = 0; count < root1.ChildNodes.Count; count++)
                        {
                            if (root1.ChildNodes.Item(count).Name == "FunctionMappings")
                            {
                                XmlNodeList userMappingsXML = root1.ChildNodes.Item(count).ChildNodes;
                                foreach (XmlNode node in userMappingsXML)
                                {

                                    for (int childcount = 0; childcount < node.ChildNodes.Count; childcount++)
                                    {
                                        if (!node.SelectSingleNode("./MappingType").InnerText.Equals(CommonUtilities.GetInstance().ConnectedSoftware)) { break; }
                                        if (!node.ChildNodes[childcount].Name.Equals("MappingName"))
                                        {
                                            if (!node.ChildNodes[childcount].Name.Equals("MappingType"))
                                            {
                                                if (!node.ChildNodes[childcount].Name.Equals("ImportType"))
                                                {
                                                    string functionNameinXml = node.ChildNodes[childcount].InnerText;
                                                    if (!string.IsNullOrEmpty(functionNameinXml))
                                                    {
                                                        if (functionNameinXml.Equals(FunctionName)) node.ChildNodes[childcount].InnerText = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        xdoc1.Save(m_settingsPath);
                    }
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        TransactionImporter.Mappings.GetInstance().SetFunctionsForReplaceForm();

                    }
                    else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                    {
                        TransactionImporter.Mappings.GetInstance().SetFunctionsForReplaceForm("Concat");
                    }
                    MessageBox.Show("Function deleted successfully");
                }
            }
            catch (Exception addException)
            {
                //   MessageBox.Show(addException.Message.ToString());
                incomplete = 1;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Method is used to get function.xml path
        /// </summary>
        /// <returns></returns>
        public string GetFunctionFilePath()
        {
            string filepath = "";
            try
            {

                string logDirectory = string.Empty;
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (logDirectory.Contains("Program Files (x86)"))
                    {
                        logDirectory = logDirectory.Replace("Program Files (x86)", Constants.xpPath);
                        logDirectory += @"\Function.xml";
                    }
                    else
                    {
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        logDirectory += @"\Function.xml";
                    }
                }
                else
                {
                    logDirectory = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                }


                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    else
                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                }
                catch { }
                //Directory.CreateDirectory(logDirectory);

                DirectoryInfo directory = new DirectoryInfo(System.IO.Path.Combine(Constants.CommonActiveDir, "Axis"));

                FileInfo[] files = directory.GetFiles();
                int i = 0;

                if (files.Length > 0)
                {
                    for (i = 0; i < files.Length; i++)
                    {
                        if (files[i].Name.Equals("Function.xml"))
                        {
                            filepath = files[i].FullName.ToString();
                            break;
                        }
                    }
                }
            }
            catch (Exception fetchException)
            {
                MessageBox.Show("In Replace Function get function file path" + fetchException.Message.ToString());
            }

            return filepath;
        }

        /// <summary>
        /// Method is used to get ConcatenateFunction.xml path
        /// </summary>
        /// <returns></returns>
        public string GetConcateFunctionFilePath()
        {
            string filepath = "";
            try
            {

                string logDirectory = string.Empty;
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (logDirectory.Contains("Program Files (x86)"))
                    {
                        logDirectory = logDirectory.Replace("Program Files (x86)", Constants.xpPath);
                        logDirectory += @"\ConcatenateFunction.xml";
                    }
                    else
                    {
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                        logDirectory += @"\ConcatenateFunction.xml";
                    }
                }
                else
                {
                    logDirectory = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                }


                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                    else
                        logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                }
                catch { }
                //Directory.CreateDirectory(logDirectory);

                DirectoryInfo directory = new DirectoryInfo(System.IO.Path.Combine(Constants.CommonActiveDir, "Axis"));

                FileInfo[] files = directory.GetFiles();
                int i = 0;

                if (files.Length > 0)
                {
                    for (i = 0; i < files.Length; i++)
                    {
                        if (files[i].Name.Equals("ConcatenateFunction.xml"))
                        {
                            filepath = files[i].FullName.ToString();
                            break;
                        }
                    }
                }
            }
            catch (Exception fetchException)
            {
                MessageBox.Show("In Replace ConcatenateFunction get ConcatenateFunction file path" + fetchException.Message.ToString());
            }

            return filepath;
        }

        public static ReplaceFunction GetInstance()
        {
            if (m_ReplaceFunction == null)
                m_ReplaceFunction = new ReplaceFunction();
            return m_ReplaceFunction;
        }

        /// <summary>
        /// This method is used to create Grid for Function
        /// </summary>
        public void CreateFunctionGrid(DataTable EditDataTable = null)
        {
            DataTable dtNewTable;
            if (EditDataTable == null)
            {
                DataRow dtrow;
                dtNewTable = new DataTable();
                DataColumn dtcol, dtcol1;
                dtcol = new DataColumn();
                dtcol1 = new DataColumn();
                dtcol.DataType = System.Type.GetType("System.String");
                dtcol.ColumnName = "Search For";
                dtNewTable.Columns.Add(dtcol);
                dtcol1.DataType = System.Type.GetType("System.String");
                dtcol1.ColumnName = "Replace With";
                dtNewTable.Columns.Add(dtcol1);
                dtrow = dtNewTable.NewRow();
                dtrow["Search For"] = "";
                dtrow["Replace With"] = "";
                dtNewTable.Rows.Add(dtrow);
            }
            else
            {
                dtNewTable = EditDataTable;
            }
            dataGridViewFunction.DataSource = dtNewTable;
            dataGridViewFunction.AllowAddNewRow = true;
            dataGridViewFunction.AllowDeleteRow = true;
            dataGridViewFunction.Columns[0].AllowResize = false;
            dataGridViewFunction.Columns[1].AllowResize = false;
            dataGridViewFunction.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
         //   dataGridViewFunction.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
        }

        public void CreateConcateFunctionGrid(List<string> Expression = null)
        {
            DataTable dtNewTable;
            dtNewTable = new DataTable();
            DataRow dtrow;
            DataColumn dtcol, dtcol1;
            dtcol = new DataColumn();
            dtcol1 = new DataColumn();
            dtcol.DataType = System.Type.GetType("System.String");
            dtcol.ColumnName = "Expression";
            dtNewTable.Columns.Add(dtcol);
            dtcol1.DataType = System.Type.GetType("System.String");
            dtcol1.ColumnName = "Preview";
            dtNewTable.Columns.Add(dtcol1);
            dtrow = dtNewTable.NewRow();
            if (Expression == null || Expression.Count == 0)
            {
                dtrow["Expression"] = "";
                dtrow["Preview"] = "";
            }
            else
            {
                dtrow["Expression"] = Expression[0];
                dtrow["Preview"] = ExpressionPreview(Expression[0]);
            }
            dtNewTable.Rows.Add(dtrow);
            dataGridViewFunction.DataSource = dtNewTable;

            dataGridViewFunction.AllowAddNewRow = true;
            dataGridViewFunction.AllowDeleteRow = true;
            dataGridViewFunction.Columns[0].AllowResize = false;
            dataGridViewFunction.Columns[1].AllowResize = false;
            dataGridViewFunction.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
        }

        void ExportMapping(object sender, CancelEventArgs e)
        {
            if (e.Cancel == false)
            {
                string ExportMappingFilePath = saveFileDialogExportFunction.FileName;
                ExportXmlFile(GetFunctionXml(), ExportMappingFilePath);
                MessageBox.Show("Function exported successfully", "Zed Axis", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// This method is used to export the data as xml file.
        /// </summary>
        /// <param name="FileData"></param>
        /// <param name="FilePath"></param>
        private void ExportXmlFile(string FileData, string FilePath)
        {
            StreamWriter sw = null;
            try
            {
                string extension = string.Empty;

                string XmlData = string.Empty;
                XmlData = FileData;

                extension = Path.GetExtension(FilePath);

                if (extension == ".xml")
                {
                    sw = new StreamWriter(FilePath, false);
                    sw.Write(FileData);
                    sw.Close();
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Unauthorized Access : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
            catch (SecurityException ex)
            {
                MessageBox.Show("Security Error : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid Operation : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }


        private string GetFunctionXml()
        {

            try
            {
                XmlDocument createXmlDoc = new XmlDocument();

                //check whether file exists or not

                #region Create XmlDocument
                createXmlDoc.AppendChild(createXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));


                #endregion
                XmlElement functionsElement = createXmlDoc.CreateElement("ExportFunction");
                createXmlDoc.AppendChild(functionsElement);

                //Axis 10.0
                XmlAttribute attribute = functionsElement.OwnerDocument.CreateAttribute("version");
                attribute.Value = assemblyVersion;
                functionsElement.Attributes.Append(attribute);

                XmlElement functionElement = createXmlDoc.CreateElement("Function");
                functionsElement.AppendChild(functionElement);

                string functionName = string.Empty;
                if (flagNewEdit == 0)   // Create new function
                {
                    functionName = textboxFunctionName.Text.Trim();
                }
                else if (flagNewEdit == 1)
                {
                    functionName = comboBoxFunctionName.SelectedItem.ToString();
                }
                XmlElement nameelement = createXmlDoc.CreateElement("Name");
                nameelement.InnerText = functionName;
                functionElement.AppendChild(nameelement);
                XmlElement mappingType = createXmlDoc.CreateElement("MappingType");
                functionElement.AppendChild(mappingType);

                mappingType.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft.ToString();
                // Mapping type value from mapping form

                XmlElement importType = createXmlDoc.CreateElement("ImportType");
                functionElement.AppendChild(importType);

                //Check Mapping mode, if edit then fetch import type from TextBox
                // if new then fetch import type from comboBox.
                if (Mappings.GetInstance().Mode.ToString().Equals("New"))
                    importType.InnerText = Mappings.GetInstance().comboboxImportTypeValue;
                else if (Mappings.GetInstance().Mode.ToString().Equals("Select"))
                    importType.InnerText = Mappings.GetInstance().textboxImportTypeValue;

                XmlElement functionType = createXmlDoc.CreateElement("FunctionType");
                functionElement.AppendChild(functionType);

                if (!comboBoxType.SelectedItem.Equals("Search and Replace"))
                {
                    functionType.InnerText = "Concatenate Function";
                    XmlElement stringElement = createXmlDoc.CreateElement("string");
                    functionElement.AppendChild(stringElement);

                    XmlElement searchElement = createXmlDoc.CreateElement("expression");
                    searchElement.InnerText = dataGridViewFunction.Rows[0].Cells[0].Value.ToString().Trim();
                    stringElement.AppendChild(searchElement);
                }
                else
                {
                    functionType.InnerText = "Search and Replace";
                    XmlElement exactMatch = createXmlDoc.CreateElement("ExactMatch");
                    functionElement.AppendChild(exactMatch);
                    if (chkExactMatch.Checked == true)
                    {
                        exactMatch.InnerText = "true";
                    }
                    else
                    {
                        exactMatch.InnerText = "false";
                    }
                    for (int count = 0; count < dataGridViewFunction.Rows.Count ; count++)
                    {
                        XmlElement stringElement = createXmlDoc.CreateElement("string");
                        functionElement.AppendChild(stringElement);

                        XmlElement searchElement = createXmlDoc.CreateElement("search");
                        searchElement.InnerText = dataGridViewFunction.Rows[count].Cells[0].Value.ToString().Trim();
                        stringElement.AppendChild(searchElement);

                        XmlElement replaceElement = createXmlDoc.CreateElement("replace");
                        replaceElement.InnerText = dataGridViewFunction.Rows[count].Cells[1].Value.ToString().Trim();
                        stringElement.AppendChild(replaceElement);
                    }
                }
                return createXmlDoc.InnerXml;

            }
            catch (Exception newException)
            {

            }

            return null;

        }


        #endregion

        private void ReplaceFunction_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Don't close form in case of invalid columns and Save as XML button click.
            //if (!closeForm)
            //    e.Cancel = true;

            closeForm = true;
        }



        private void comboBoxFunctionName_SelectedIndexChanged(object sender, EventArgs e)
        {
            // read xml function based on selected function name
            try
            {


                if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                {
                    if (comboBoxFunctionName.SelectedIndex > -1)
                    {
                        if (flagDelete == 1)
                        {
                            var confirmResult = MessageBox.Show("Are you sure you want to delete this function..?",
                                   "Confirm Delete!!",
                                   MessageBoxButtons.YesNo);
                            if (confirmResult == DialogResult.Yes)
                            {
                                String FunctionName = comboBoxFunctionName.SelectedItem.ToString();
                                deleteConcatOrReplaceFunctions(FunctionName);

                            }
                        }
                        else
                        {
                            GetFunctionData(comboBoxFunctionName.SelectedItem.ToString());
                        }
                    }
                }
                else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                {
                    if (flagDelete == 1)
                    {
                        var confirmResult = MessageBox.Show("Are you sure you want to delete this function..?",
                               "Confirm Delete!!",
                               MessageBoxButtons.YesNo);
                        if (confirmResult == DialogResult.Yes)
                        {
                            String FunctionName = comboBoxFunctionName.SelectedItem.ToString();

                            deleteConcatOrReplaceFunctions(FunctionName);
                        }
                    }
                    else
                    {
                        if (comboBoxFunctionName.SelectedIndex > -1)
                        {
                            GetConcateFunctionData(comboBoxFunctionName.SelectedItem.ToString());
                        }
                    }
                }
            }
            catch (Exception fetchFuntionEx)
            {
                // MessageBox.Show(fetchFuntionEx.Message.ToString());
            }
        }

        private void chkExactMatch_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExactMatch.Checked == true)
            {
                CommonUtilities.GetInstance().IsExactMatch = true;
            }
            else
            {
                CommonUtilities.GetInstance().IsExactMatch = false;
            }
        }

        private void comboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (flagDelete == 1)
            {
                textboxFunctionName.Visible = false;
                comboBoxFunctionName.Visible = true;
                if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                {
                    Mappings.GetInstance().SetFunctionsForReplaceForm();

                }
                else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                {
                    Mappings.GetInstance().SetFunctionsForReplaceForm("Concat");
                }
                dataGridViewFunction.Visible = false;
                chkExactMatch.Visible = false;
            }
            else
            {
                dataGridViewFunction.Visible = true;
                chkExactMatch.Visible = true;

                if (flagNewEdit == 0)
                {
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        CreateFunctionGrid();
                        textboxFunctionName.Visible = true;
                        comboBoxFunctionName.Visible = false;
                        this.dataGridViewFunction.AllowAddNewRow = true;
                        
                        this.dataGridViewFunction.MasterTemplate.AllowAddNewRow = true;

                    }
                    else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                    {
                        chkExactMatch.Visible = false;

                        CreateConcateFunctionGrid();
                        textboxFunctionName.Visible = true;
                        comboBoxFunctionName.Visible = false;
                        this.dataGridViewFunction.AllowAddNewRow = false;

                        this.dataGridViewFunction.MasterTemplate.AllowAddNewRow = false;


                    }
                }
                else
                {
                    comboBoxFunctionName.Items.Clear();
                    if (comboBoxType.SelectedItem.Equals("Search and Replace"))
                    {
                        chkExactMatch.Visible = true;

                        textboxFunctionName.Visible = false;
                        comboBoxFunctionName.Visible = true;
                        this.dataGridViewFunction.AllowAddNewRow = true;
                        TransactionImporter.Mappings.GetInstance().SetFunctionsForReplaceForm();
                        if (comboBoxFunctionName.SelectedItem != null)
                        {
                            var functionDetails = CommonUtilities.GetInstance().FetchFunctionDetails(comboBoxFunctionName.SelectedItem.ToString());
                            if (functionDetails.Item2 == true)
                            {
                                chkExactMatch.Checked = true;
                            }
                            else
                            {
                                chkExactMatch.Checked = false;
                            }
                            CreateFunctionGrid(functionDetails.Item1);
                        }
                        else
                        {
                            CreateFunctionGrid();
                        }
                    }
                    else if (comboBoxType.SelectedItem.Equals("Concatenate Function"))
                    {
                        chkExactMatch.Visible = false;

                        textboxFunctionName.Visible = false;
                        comboBoxFunctionName.Visible = true;
                        this.dataGridViewFunction.AllowAddNewRow = false;

                        TransactionImporter.Mappings.GetInstance().SetFunctionsForReplaceForm("Concat");
                        if (comboBoxFunctionName.SelectedItem != null)
                        {
                            List<string> expression = CommonUtilities.GetInstance().FetchConcatFunctionDetails(comboBoxFunctionName.SelectedItem.ToString());
                            CreateConcateFunctionGrid(expression);
                        }
                        else CreateConcateFunctionGrid();
                    }
                }
            }
            comboBoxFunctionName.SelectedIndex = -1;
        }

        private void dataGridViewFunction_CellLeave(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (comboBoxType.SelectedItem != null && comboBoxType.SelectedItem.Equals("Concatenate Function") && dataGridViewFunction.Rows[0].Cells[0].Value != null)
            {
                dataGridViewFunction.Rows[0].Cells[1].Value = ExpressionPreview(dataGridViewFunction.Rows[0].Cells[0].Value.ToString());
            }
        }

        private void dataGridViewFunction_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if(e.CellElement.ColumnInfo.HeaderText == "Preview")
            {
                e.CellElement.Enabled =false;
            }
        }
    }
}
