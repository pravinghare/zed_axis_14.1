namespace DataProcessingBlocks
{
    partial class ZedAxisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZedAxisForm));
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabelOEConnector = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unable to connect to QuickBooks Online Edition.\r\nPlease click the link below to i" +
    "nstall the QBOE Connector";
            // 
            // linkLabelOEConnector
            // 
            this.linkLabelOEConnector.AutoSize = true;
            this.linkLabelOEConnector.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelOEConnector.Location = new System.Drawing.Point(132, 79);
            this.linkLabelOEConnector.Name = "linkLabelOEConnector";
            this.linkLabelOEConnector.Size = new System.Drawing.Size(125, 13);
            this.linkLabelOEConnector.TabIndex = 2;
            this.linkLabelOEConnector.TabStop = true;
            this.linkLabelOEConnector.Text = "Download and install";
            this.linkLabelOEConnector.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOEConnector_LinkClicked);
            // 
            // ZedAxisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(423, 123);
            this.Controls.Add(this.linkLabelOEConnector);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(1550, 1050);
            this.Name = "ZedAxisForm";
            this.Text = "Error connecting to QBO";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelOEConnector;
    }
}