namespace DataProcessingBlocks
{
    partial class QuickBookAccountSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            m_options = null;
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuickBookAccountSettings));
            this.groupBoxCreateItems = new System.Windows.Forms.GroupBox();
            this.comboBoxAssetAccount = new System.Windows.Forms.ComboBox();
            this.labelAssetAccount = new System.Windows.Forms.Label();
            this.comboBoxCOGSAccount = new System.Windows.Forms.ComboBox();
            this.comboBoxIncomeAccount = new System.Windows.Forms.ComboBox();
            this.comboBoxTaxCode = new System.Windows.Forms.ComboBox();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.labelCOGSAccount = new System.Windows.Forms.Label();
            this.labelIncomeAccount = new System.Windows.Forms.Label();
            this.labelTaxCode = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkBoxGrossToNet = new System.Windows.Forms.CheckBox();
            this.groupBoxShoppingCart = new System.Windows.Forms.GroupBox();
            this.comboBoxSalesTax = new System.Windows.Forms.ComboBox();
            this.labelSalesTax = new System.Windows.Forms.Label();
            this.comboBoxDiscount = new System.Windows.Forms.ComboBox();
            this.comboBoxInsurance = new System.Windows.Forms.ComboBox();
            this.comboBoxFreight = new System.Windows.Forms.ComboBox();
            this.labelDiscount = new System.Windows.Forms.Label();
            this.labelInsurance = new System.Windows.Forms.Label();
            this.labelFreight = new System.Windows.Forms.Label();
            this.checkBoxPartNumberLookup = new System.Windows.Forms.CheckBox();
            this.checkBoxALULookup = new System.Windows.Forms.CheckBox();
            this.checkBoxSKUlookUp = new System.Windows.Forms.CheckBox();
            this.checkBoxUPCLookup = new System.Windows.Forms.CheckBox();
            this.groupBoxCreateItems.SuspendLayout();
            this.groupBoxShoppingCart.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxCreateItems
            // 
            this.groupBoxCreateItems.Controls.Add(this.comboBoxAssetAccount);
            this.groupBoxCreateItems.Controls.Add(this.labelAssetAccount);
            this.groupBoxCreateItems.Controls.Add(this.comboBoxCOGSAccount);
            this.groupBoxCreateItems.Controls.Add(this.comboBoxIncomeAccount);
            this.groupBoxCreateItems.Controls.Add(this.comboBoxTaxCode);
            this.groupBoxCreateItems.Controls.Add(this.comboBoxType);
            this.groupBoxCreateItems.Controls.Add(this.labelCOGSAccount);
            this.groupBoxCreateItems.Controls.Add(this.labelIncomeAccount);
            this.groupBoxCreateItems.Controls.Add(this.labelTaxCode);
            this.groupBoxCreateItems.Controls.Add(this.labelType);
            this.groupBoxCreateItems.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCreateItems.ForeColor = System.Drawing.Color.Black;
            this.groupBoxCreateItems.Location = new System.Drawing.Point(16, 27);
            this.groupBoxCreateItems.Name = "groupBoxCreateItems";
            this.groupBoxCreateItems.Size = new System.Drawing.Size(371, 212);
            this.groupBoxCreateItems.TabIndex = 0;
            this.groupBoxCreateItems.TabStop = false;
            this.groupBoxCreateItems.Text = "Create Items";
            // 
            // comboBoxAssetAccount
            // 
            this.comboBoxAssetAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAssetAccount.DropDownWidth = 200;
            this.comboBoxAssetAccount.Enabled = false;
            this.comboBoxAssetAccount.FormattingEnabled = true;
            this.comboBoxAssetAccount.Location = new System.Drawing.Point(131, 165);
            this.comboBoxAssetAccount.Name = "comboBoxAssetAccount";
            this.comboBoxAssetAccount.Size = new System.Drawing.Size(220, 21);
            this.comboBoxAssetAccount.TabIndex = 9;
            // 
            // labelAssetAccount
            // 
            this.labelAssetAccount.AutoSize = true;
            this.labelAssetAccount.Location = new System.Drawing.Point(24, 168);
            this.labelAssetAccount.Name = "labelAssetAccount";
            this.labelAssetAccount.Size = new System.Drawing.Size(92, 13);
            this.labelAssetAccount.TabIndex = 8;
            this.labelAssetAccount.Text = "Asset Account:";
            // 
            // comboBoxCOGSAccount
            // 
            this.comboBoxCOGSAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCOGSAccount.DropDownWidth = 200;
            this.comboBoxCOGSAccount.Enabled = false;
            this.comboBoxCOGSAccount.FormattingEnabled = true;
            this.comboBoxCOGSAccount.Location = new System.Drawing.Point(131, 129);
            this.comboBoxCOGSAccount.Name = "comboBoxCOGSAccount";
            this.comboBoxCOGSAccount.Size = new System.Drawing.Size(220, 21);
            this.comboBoxCOGSAccount.TabIndex = 7;
            // 
            // comboBoxIncomeAccount
            // 
            this.comboBoxIncomeAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIncomeAccount.DropDownWidth = 200;
            this.comboBoxIncomeAccount.FormattingEnabled = true;
            this.comboBoxIncomeAccount.Location = new System.Drawing.Point(130, 96);
            this.comboBoxIncomeAccount.Name = "comboBoxIncomeAccount";
            this.comboBoxIncomeAccount.Size = new System.Drawing.Size(221, 21);
            this.comboBoxIncomeAccount.TabIndex = 6;
            // 
            // comboBoxTaxCode
            // 
            this.comboBoxTaxCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTaxCode.DropDownWidth = 200;
            this.comboBoxTaxCode.FormattingEnabled = true;
            this.comboBoxTaxCode.Location = new System.Drawing.Point(130, 63);
            this.comboBoxTaxCode.Name = "comboBoxTaxCode";
            this.comboBoxTaxCode.Size = new System.Drawing.Size(221, 21);
            this.comboBoxTaxCode.TabIndex = 5;
            // 
            // comboBoxType
            // 
            this.comboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxType.DropDownWidth = 200;
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(130, 29);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(221, 21);
            this.comboBoxType.TabIndex = 4;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // labelCOGSAccount
            // 
            this.labelCOGSAccount.AutoSize = true;
            this.labelCOGSAccount.Location = new System.Drawing.Point(20, 132);
            this.labelCOGSAccount.Name = "labelCOGSAccount";
            this.labelCOGSAccount.Size = new System.Drawing.Size(96, 13);
            this.labelCOGSAccount.TabIndex = 3;
            this.labelCOGSAccount.Text = "COGS Account:";
            // 
            // labelIncomeAccount
            // 
            this.labelIncomeAccount.AutoSize = true;
            this.labelIncomeAccount.Location = new System.Drawing.Point(11, 99);
            this.labelIncomeAccount.Name = "labelIncomeAccount";
            this.labelIncomeAccount.Size = new System.Drawing.Size(104, 13);
            this.labelIncomeAccount.TabIndex = 2;
            this.labelIncomeAccount.Text = "Income Account:";
            // 
            // labelTaxCode
            // 
            this.labelTaxCode.AutoSize = true;
            this.labelTaxCode.Location = new System.Drawing.Point(49, 66);
            this.labelTaxCode.Name = "labelTaxCode";
            this.labelTaxCode.Size = new System.Drawing.Size(66, 13);
            this.labelTaxCode.TabIndex = 1;
            this.labelTaxCode.Text = "Tax Code:";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(76, 32);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(39, 13);
            this.labelType.TabIndex = 0;
            this.labelType.Text = "Type:";
            // 
            // buttonOK
            // 
            this.buttonOK.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.Location = new System.Drawing.Point(582, 269);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(680, 269);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(78, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkBoxGrossToNet
            // 
            this.checkBoxGrossToNet.AutoSize = true;
            this.checkBoxGrossToNet.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxGrossToNet.Location = new System.Drawing.Point(22, 247);
            this.checkBoxGrossToNet.Name = "checkBoxGrossToNet";
            this.checkBoxGrossToNet.Size = new System.Drawing.Size(144, 17);
            this.checkBoxGrossToNet.TabIndex = 3;
            this.checkBoxGrossToNet.Text = "Convert gross to net";
            this.checkBoxGrossToNet.UseVisualStyleBackColor = true;
            this.checkBoxGrossToNet.CheckedChanged += new System.EventHandler(this.checkBoxGrossToNet_CheckedChanged);
            // 
            // groupBoxShoppingCart
            // 
            this.groupBoxShoppingCart.Controls.Add(this.comboBoxSalesTax);
            this.groupBoxShoppingCart.Controls.Add(this.labelSalesTax);
            this.groupBoxShoppingCart.Controls.Add(this.comboBoxDiscount);
            this.groupBoxShoppingCart.Controls.Add(this.comboBoxInsurance);
            this.groupBoxShoppingCart.Controls.Add(this.comboBoxFreight);
            this.groupBoxShoppingCart.Controls.Add(this.labelDiscount);
            this.groupBoxShoppingCart.Controls.Add(this.labelInsurance);
            this.groupBoxShoppingCart.Controls.Add(this.labelFreight);
            this.groupBoxShoppingCart.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxShoppingCart.Location = new System.Drawing.Point(401, 28);
            this.groupBoxShoppingCart.Name = "groupBoxShoppingCart";
            this.groupBoxShoppingCart.Size = new System.Drawing.Size(357, 212);
            this.groupBoxShoppingCart.TabIndex = 4;
            this.groupBoxShoppingCart.TabStop = false;
            this.groupBoxShoppingCart.Text = "Shopping Cart Fields";
            // 
            // comboBoxSalesTax
            // 
            this.comboBoxSalesTax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSalesTax.FormattingEnabled = true;
            this.comboBoxSalesTax.Location = new System.Drawing.Point(95, 144);
            this.comboBoxSalesTax.Name = "comboBoxSalesTax";
            this.comboBoxSalesTax.Size = new System.Drawing.Size(245, 21);
            this.comboBoxSalesTax.TabIndex = 14;
            // 
            // labelSalesTax
            // 
            this.labelSalesTax.AutoSize = true;
            this.labelSalesTax.Location = new System.Drawing.Point(13, 144);
            this.labelSalesTax.Name = "labelSalesTax";
            this.labelSalesTax.Size = new System.Drawing.Size(67, 13);
            this.labelSalesTax.TabIndex = 13;
            this.labelSalesTax.Text = "Sales Tax:";
            // 
            // comboBoxDiscount
            // 
            this.comboBoxDiscount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDiscount.DropDownWidth = 200;
            this.comboBoxDiscount.FormattingEnabled = true;
            this.comboBoxDiscount.Location = new System.Drawing.Point(95, 104);
            this.comboBoxDiscount.Name = "comboBoxDiscount";
            this.comboBoxDiscount.Size = new System.Drawing.Size(245, 21);
            this.comboBoxDiscount.TabIndex = 12;
            // 
            // comboBoxInsurance
            // 
            this.comboBoxInsurance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInsurance.DropDownWidth = 200;
            this.comboBoxInsurance.FormattingEnabled = true;
            this.comboBoxInsurance.Location = new System.Drawing.Point(95, 71);
            this.comboBoxInsurance.Name = "comboBoxInsurance";
            this.comboBoxInsurance.Size = new System.Drawing.Size(245, 21);
            this.comboBoxInsurance.TabIndex = 11;
            // 
            // comboBoxFreight
            // 
            this.comboBoxFreight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFreight.DropDownWidth = 200;
            this.comboBoxFreight.FormattingEnabled = true;
            this.comboBoxFreight.Location = new System.Drawing.Point(95, 34);
            this.comboBoxFreight.Name = "comboBoxFreight";
            this.comboBoxFreight.Size = new System.Drawing.Size(245, 21);
            this.comboBoxFreight.TabIndex = 10;
            // 
            // labelDiscount
            // 
            this.labelDiscount.AutoSize = true;
            this.labelDiscount.Location = new System.Drawing.Point(21, 104);
            this.labelDiscount.Name = "labelDiscount";
            this.labelDiscount.Size = new System.Drawing.Size(61, 13);
            this.labelDiscount.TabIndex = 3;
            this.labelDiscount.Text = "Discount:";
            // 
            // labelInsurance
            // 
            this.labelInsurance.AutoSize = true;
            this.labelInsurance.Location = new System.Drawing.Point(13, 71);
            this.labelInsurance.Name = "labelInsurance";
            this.labelInsurance.Size = new System.Drawing.Size(69, 13);
            this.labelInsurance.TabIndex = 2;
            this.labelInsurance.Text = "Insurance:";
            // 
            // labelFreight
            // 
            this.labelFreight.AutoSize = true;
            this.labelFreight.Location = new System.Drawing.Point(31, 37);
            this.labelFreight.Name = "labelFreight";
            this.labelFreight.Size = new System.Drawing.Size(51, 13);
            this.labelFreight.TabIndex = 1;
            this.labelFreight.Text = "Freight:";
            // 
            // checkBoxPartNumberLookup
            // 
            this.checkBoxPartNumberLookup.AutoSize = true;
            this.checkBoxPartNumberLookup.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxPartNumberLookup.Location = new System.Drawing.Point(22, 273);
            this.checkBoxPartNumberLookup.Name = "checkBoxPartNumberLookup";
            this.checkBoxPartNumberLookup.Size = new System.Drawing.Size(187, 17);
            this.checkBoxPartNumberLookup.TabIndex = 4;
            this.checkBoxPartNumberLookup.Text = "Vendor Part Number Lookup";
            this.checkBoxPartNumberLookup.UseVisualStyleBackColor = true;
            this.checkBoxPartNumberLookup.CheckedChanged += new System.EventHandler(this.checkBoxPartNumberLookup_CheckedChanged);
            // 
            // checkBoxALULookup
            // 
            this.checkBoxALULookup.AutoSize = true;
            this.checkBoxALULookup.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxALULookup.Location = new System.Drawing.Point(216, 273);
            this.checkBoxALULookup.Name = "checkBoxALULookup";
            this.checkBoxALULookup.Size = new System.Drawing.Size(93, 17);
            this.checkBoxALULookup.TabIndex = 5;
            this.checkBoxALULookup.Text = "ALU Lookup";
            this.checkBoxALULookup.UseVisualStyleBackColor = true;
            this.checkBoxALULookup.CheckedChanged += new System.EventHandler(this.checkBoxALULookup_CheckedChanged);
            this.checkBoxALULookup.Click += new System.EventHandler(this.checkBoxALULookup_Click);
            // 
            // checkBoxSKUlookUp
            // 
            this.checkBoxSKUlookUp.AutoSize = true;
            this.checkBoxSKUlookUp.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxSKUlookUp.Location = new System.Drawing.Point(216, 250);
            this.checkBoxSKUlookUp.Name = "checkBoxSKUlookUp";
            this.checkBoxSKUlookUp.Size = new System.Drawing.Size(95, 17);
            this.checkBoxSKUlookUp.TabIndex = 6;
            this.checkBoxSKUlookUp.Text = "SKU Lookup";
            this.checkBoxSKUlookUp.UseVisualStyleBackColor = true;
            this.checkBoxSKUlookUp.CheckedChanged += new System.EventHandler(this.checkBoxSKUlookUp_CheckedChanged);
            // 
            // checkBoxUPCLookup
            // 
            this.checkBoxUPCLookup.AutoSize = true;
            this.checkBoxUPCLookup.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxUPCLookup.Location = new System.Drawing.Point(344, 250);
            this.checkBoxUPCLookup.Name = "checkBoxUPCLookup";
            this.checkBoxUPCLookup.Size = new System.Drawing.Size(95, 17);
            this.checkBoxUPCLookup.TabIndex = 7;
            this.checkBoxUPCLookup.Text = "UPC Lookup";
            this.checkBoxUPCLookup.UseVisualStyleBackColor = true;
            this.checkBoxUPCLookup.CheckedChanged += new System.EventHandler(this.checkBoxUPCLookup_CheckedChanged);
            this.checkBoxUPCLookup.Click += new System.EventHandler(this.checkBoxUPCLookup_Click);
            // 
            // QuickBookAccountSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(773, 313);
            this.Controls.Add(this.checkBoxUPCLookup);
            this.Controls.Add(this.checkBoxSKUlookUp);
            this.Controls.Add(this.checkBoxALULookup);
            this.Controls.Add(this.groupBoxShoppingCart);
            this.Controls.Add(this.checkBoxPartNumberLookup);
            this.Controls.Add(this.checkBoxGrossToNet);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBoxCreateItems);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuickBookAccountSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import Settings";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.QuickBookAccountSettings_FormClosed);
            this.Load += new System.EventHandler(this.Options_Load);
            this.groupBoxCreateItems.ResumeLayout(false);
            this.groupBoxCreateItems.PerformLayout();
            this.groupBoxShoppingCart.ResumeLayout(false);
            this.groupBoxShoppingCart.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxCreateItems;
        private System.Windows.Forms.Label labelCOGSAccount;
        private System.Windows.Forms.Label labelIncomeAccount;
        private System.Windows.Forms.Label labelTaxCode;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.ComboBox comboBoxCOGSAccount;
        private System.Windows.Forms.ComboBox comboBoxIncomeAccount;
        private System.Windows.Forms.ComboBox comboBoxTaxCode;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxAssetAccount;
        private System.Windows.Forms.Label labelAssetAccount;
        private System.Windows.Forms.CheckBox checkBoxGrossToNet;
        private System.Windows.Forms.GroupBox groupBoxShoppingCart;
        private System.Windows.Forms.Label labelDiscount;
        private System.Windows.Forms.Label labelInsurance;
        private System.Windows.Forms.Label labelFreight;
        private System.Windows.Forms.ComboBox comboBoxDiscount;
        private System.Windows.Forms.ComboBox comboBoxInsurance;
        private System.Windows.Forms.ComboBox comboBoxFreight;
        private System.Windows.Forms.CheckBox checkBoxPartNumberLookup;
        private System.Windows.Forms.ComboBox comboBoxSalesTax;
        private System.Windows.Forms.Label labelSalesTax;
        private System.Windows.Forms.CheckBox checkBoxALULookup;
        private System.Windows.Forms.CheckBox checkBoxSKUlookUp;
        private System.Windows.Forms.CheckBox checkBoxUPCLookup;
    }
}