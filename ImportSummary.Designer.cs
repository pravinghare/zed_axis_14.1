namespace TransactionImporter
{
    partial class ImportSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportSummary));
            this.lbldeleteheading = new System.Windows.Forms.Label();
            this.labelDeleting = new System.Windows.Forms.Label();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.progressBarDelet = new System.Windows.Forms.ProgressBar();
            this.buttonQuit = new System.Windows.Forms.Button();
            this.labelSkipped = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.richTextBoxSDKMessage = new System.Windows.Forms.RichTextBox();
            this.labelFailMsg = new System.Windows.Forms.Label();
            this.labelSuccesMsg = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbldeleteheading
            // 
            this.lbldeleteheading.AutoSize = true;
            this.lbldeleteheading.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldeleteheading.Location = new System.Drawing.Point(12, 25);
            this.lbldeleteheading.Name = "lbldeleteheading";
            this.lbldeleteheading.Size = new System.Drawing.Size(61, 13);
            this.lbldeleteheading.TabIndex = 21;
            this.lbldeleteheading.Text = "Deleting";
            this.lbldeleteheading.Visible = false;
            // 
            // labelDeleting
            // 
            this.labelDeleting.AutoSize = true;
            this.labelDeleting.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeleting.Location = new System.Drawing.Point(247, 25);
            this.labelDeleting.Name = "labelDeleting";
            this.labelDeleting.Size = new System.Drawing.Size(0, 13);
            this.labelDeleting.TabIndex = 20;
            this.labelDeleting.Visible = false;
            // 
            // progressBarDelet
            // 
            this.progressBarDelet.Location = new System.Drawing.Point(94, 16);
            this.progressBarDelet.Name = "progressBarDelet";
            this.progressBarDelet.Size = new System.Drawing.Size(125, 23);
            this.progressBarDelet.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBarDelet.TabIndex = 19;
            this.progressBarDelet.Visible = false;
            // 
            // buttonQuit
            // 
            this.buttonQuit.Location = new System.Drawing.Point(312, 233);
            this.buttonQuit.Name = "buttonQuit";
            this.buttonQuit.Size = new System.Drawing.Size(87, 23);
            this.buttonQuit.TabIndex = 18;
            this.buttonQuit.Text = "Quit";
            this.buttonQuit.UseVisualStyleBackColor = true;
            this.buttonQuit.Click += new System.EventHandler(this.buttonQuit_Click);
            // 
            // labelSkipped
            // 
            this.labelSkipped.AutoSize = true;
            this.labelSkipped.Location = new System.Drawing.Point(309, 16);
            this.labelSkipped.Name = "labelSkipped";
            this.labelSkipped.Size = new System.Drawing.Size(0, 13);
            this.labelSkipped.TabIndex = 17;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(194, 233);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(87, 23);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // richTextBoxSDKMessage
            // 
            this.richTextBoxSDKMessage.BackColor = System.Drawing.Color.White;
            this.richTextBoxSDKMessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxSDKMessage.Location = new System.Drawing.Point(5, 69);
            this.richTextBoxSDKMessage.Name = "richTextBoxSDKMessage";
            this.richTextBoxSDKMessage.ReadOnly = true;
            this.richTextBoxSDKMessage.Size = new System.Drawing.Size(625, 158);
            this.richTextBoxSDKMessage.TabIndex = 15;
            this.richTextBoxSDKMessage.Text = "";
            // 
            // labelFailMsg
            // 
            this.labelFailMsg.AutoSize = true;
            this.labelFailMsg.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFailMsg.Location = new System.Drawing.Point(358, 53);
            this.labelFailMsg.Name = "labelFailMsg";
            this.labelFailMsg.Size = new System.Drawing.Size(41, 13);
            this.labelFailMsg.TabIndex = 14;
            this.labelFailMsg.Text = "label2";
            // 
            // labelSuccesMsg
            // 
            this.labelSuccesMsg.AutoSize = true;
            this.labelSuccesMsg.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSuccesMsg.Location = new System.Drawing.Point(353, 16);
            this.labelSuccesMsg.Name = "labelSuccesMsg";
            this.labelSuccesMsg.Size = new System.Drawing.Size(0, 13);
            this.labelSuccesMsg.TabIndex = 13;
            // 
            // ImportSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(634, 273);
            this.Controls.Add(this.lbldeleteheading);
            this.Controls.Add(this.labelDeleting);
            this.Controls.Add(this.progressBarDelet);
            this.Controls.Add(this.buttonQuit);
            this.Controls.Add(this.labelSkipped);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.richTextBoxSDKMessage);
            this.Controls.Add(this.labelFailMsg);
            this.Controls.Add(this.labelSuccesMsg);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(650, 360);
            this.MinimizeBox = false;
            this.Name = "ImportSummary";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Summary";
            this.Load += new System.EventHandler(this.ImportSummary_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbldeleteheading;
        private System.Windows.Forms.Label labelDeleting;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.ProgressBar progressBarDelet;
        private System.Windows.Forms.Button buttonQuit;
        private System.Windows.Forms.Label labelSkipped;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.RichTextBox richTextBoxSDKMessage;
        private System.Windows.Forms.Label labelFailMsg;
        private System.Windows.Forms.Label labelSuccesMsg;

    }
}