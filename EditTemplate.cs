using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Streams;
using System.Collections;
using System.Xml;
using System.Configuration;
using EDI.Constant;
using System.IO;
using Telerik.WinControls.UI;



namespace DataProcessingBlocks
{
    public partial class EditTemplate : Form
    {

         private GridViewTextBoxColumn tTextColumn;
         private GridViewCheckBoxColumn tCheckColumn;
     
        private GridViewComboBoxColumn tComboColumn;
        RadGridView rad_griddata_xml = new RadGridView();
        DataTable dt_fillgrid = new DataTable();
        private int rowindex;
       
        #region Private members

        private static EditTemplate m_EditTemplate=null;

        private DataGridViewRow dr;
        
        private DataGridViewCheckBoxColumn checkColumn;
        private DataGridViewCheckBoxCell checkCell;
        
        private DataGridViewTextBoxColumn textColumn;
        private DataGridViewTextBoxCell textCell;
        private DataGridViewCellStyle cellStyle = new DataGridViewCellStyle();
        string newData = string.Empty;
        bool IsAddCodingTemplate = false;
        
        
        private int rowIndex = 0;
        private string m_ConnectedSoft;

        List<string> custList = null;
        List<string> clasList = null;
        List<string> empList = null;
        List<string> vendList = null;
        QBCustomerListCollection customerList;
        QBEmployeeListCollection employeeList;
        QBVendorListCollection vendorList;
        QBClassListCollection classList;
        bool flagForm = false;
        #endregion

        #region public members

        public static EditTemplate GetInstance()
        {
            m_EditTemplate = new EditTemplate();
            return m_EditTemplate;
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// This property is used for set and get Connected Accounting Software value.
        /// </summary>
        public string ConnectedSoft
        {
            get
            { return m_ConnectedSoft; }
            set
            {
                m_ConnectedSoft = value;
            }
        }
        #endregion

        #region Constructor
        public EditTemplate()
        {
            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// Create New Record into DataGridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonNew_Click(object sender, EventArgs e)
        {
            #region Validate ComboBox controll
            ValidateControlls();
            #endregion

            #region  Validation for Keyword textbox

           // if (string.IsNullOrEmpty(this.textBoxKeyword.Text.Trim()))
            if(string.IsNullOrEmpty(this.tTextColumn.ToString().Trim()))
            {
                MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            #endregion


            #region Validation for RadioButton

            //if (!this.radioButtonContains.Checked && !this.radioButtonMatches.Checked)
            //{
            //    MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            #endregion
            #region Validation for Unique keyword and operation pair
            List<string[]> keywordInfo = new List<string[]>();
            for (int i = 0; i < radGridviewforEditTemplate.RowCount;i++)
            // for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                try
                {
                    string[] kdata = new string[2];
                    kdata[0] = radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString();
                    kdata[1] = radGridviewforEditTemplate.Rows[i].Cells[8].Value.ToString();
                    keywordInfo.Add(kdata);
                }
                catch
                {
                }
            }

               // for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
                //{
                //    try
                //    {
                //        string[] kdata = new string[2];
                //        kdata[0] = radGridviewforEditTemplate.Rows[i].Cells[0].FormattedValue.ToString();
                //        kdata[1] = radGridviewforEditTemplate.Rows[i].Cells[6].FormattedValue.ToString();
                //        keywordInfo.Add(kdata);
                //    }
                //    catch
                //    {
                //    }
                //}
            //string operatorValue=string.Empty;
            //if(radioButtonContains.Checked)
            //    operatorValue="contains";
            //else if(radioButtonMatches.Checked)
            //    operatorValue="matches";
            //for (int j = 0; j < keywordInfo.Count; j++)
            //{
            //    try
            //    {
            //        if (!string.IsNullOrEmpty(keywordInfo[j][0]) && !string.IsNullOrEmpty(keywordInfo[j][1]))
            //        {
            //            if (keywordInfo[j][0].ToString().ToLower().Equals(tTextColumn.ToString().Trim().ToLower()) && keywordInfo[j][1].ToString().ToLower().Equals(operatorValue))
            //            {
            //                MessageBox.Show("Please enter unique Keyword/operator pair.");
            //                return;
            //            }
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}

            #endregion

            try
            {

                #region Create/Add New datarow into datagridview

                try
                {
                    DataGridViewRow drNew = new DataGridViewRow();
                  
                    DataGridViewTextBoxCell drNewCell;
                    DataGridViewCheckBoxCell drNewCheckCell;


                    //for Keyword Tag
                    //drNewCell = new DataGridViewTextBoxCell();
                    //drNewCell.Value = this.tTextColumn.ToString().Trim();
                    //drNewCell.Style = cellStyle;
                    //drNew.Cells.Add(drNewCell);

                    drNewCell = new DataGridViewTextBoxCell();
                    drNewCell.Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[0].Value;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);


                    //for Name Tag
                    drNewCell = new DataGridViewTextBoxCell();
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[1].Value != null)
                        // drNewCell.Value = this.comboBoxName.SelectedItem.ToString();
                        drNewCell.Value = tTextColumn.ToString();
                    else
                        drNewCell.Value = string.Empty;

                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);


                    //for Account Tag
                    drNewCell = new DataGridViewTextBoxCell();
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[2].Value != null)
                        drNewCell.Value = this.tComboColumn.ToString();
                    else
                        drNewCell.Value = string.Empty;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);


                    //for TaxCode Tag
                    drNewCell = new DataGridViewTextBoxCell();
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[3].Value != null)
                        drNewCell.Value = this.tComboColumn.ToString();
                    else
                        drNewCell.Value = string.Empty;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);


                    //for CustomerJob Tag
                    drNewCell = new DataGridViewTextBoxCell();
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[4].Value != null)
                        drNewCell.Value = tComboColumn.ToString();
                    else
                        drNewCell.Value = string.Empty;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);
                    
                    //for Billing Tag
                    drNewCheckCell = new DataGridViewCheckBoxCell();
                    drNewCheckCell.Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[5].Value = true;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCheckCell);

                    //for Memo Tag
                    drNewCell = new DataGridViewTextBoxCell();
                    drNewCell.Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[6].Value;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);


                    // for Types(Contains/matches) tag

                    drNewCell = new DataGridViewTextBoxCell();
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[7].Value != null)
                        drNewCell.Value = this.tComboColumn.ToString();
                    else
                        drNewCell.Value = string.Empty;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);
                    ////for Operator tag
                    //drNewCell = new DataGridViewTextBoxCell();
                    //if (this.radioButtonContains.Checked)
                    //    drNewCell.Value = "contains";
                    //else if (this.radioButtonMatches.Checked)
                    //    drNewCell.Value = "matches";
                    //drNewCell.Style = cellStyle;
                    //drNew.Cells.Add(drNewCell);

                    //for RowNumber Tag
                    drNewCell = new DataGridViewTextBoxCell();

                    int rowNumber = 0;

                    try
                    {
                        //get RowCount
                        rowNumber = Convert.ToInt32(CommonUtilities.GetInstance().RowCountOfCodingTemplateGridView);
                        //rowNumber += 1;
                        int newRowNumber = rowNumber + 1;
                        CommonUtilities.GetInstance().RowCountOfCodingTemplateGridView = newRowNumber.ToString();
                    }
                    catch
                    {
                    }


                    drNewCell.Value = rowNumber;
                    drNewCell.Style = cellStyle;
                    drNew.Cells.Add(drNewCell);

                    radGridviewforEditTemplate.Rows.Add(drNew);

                   // radGridviewforEditTemplate.Rows.Add(drNew);
                }
                catch
                {
                }

                #endregion

                #region Copy Newly created row into List so that it can be used later on while updating of datagrid of coding template

             // for Name tag
              
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[1].Value != null)
                        newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[1].Value + "~";
                        
                    else
                        newData += string.Empty + "~";


                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[2].Value != null)
                        newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[2].Value + "~";
                    else
                        newData += string.Empty + "~";


                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[3].Value != null)
                        newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[3].Value.ToString() + "~";
                    else
                        newData += string.Empty + "~";

                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[4].Value != null)
                        newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[4].Value + "~";
                    else
                        newData += string.Empty + "~";
                    if (this.radGridviewforEditTemplate.Rows[rowindex].Cells[5].Value != null)
                        // newData += this.comboBoxName.SelectedItem.ToString() + "~";
                        newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[5].Value + "~";
                    else
                        newData += string.Empty + "~";


                    newData += this.radGridviewforEditTemplate.Rows[rowindex].Cells[6].Value + "\r\n";

                    CommonUtilities.GetInstance().NewRowEditTemplate = newData;
                }
                #endregion
            
            catch
            { }
        
        }

        /// <summary>
        /// Create RADDataGridView for EditTemplate Form
        /// </summary>
        /// <param name="dataGridViewPreviewCodingTemplate"></param>
        /// <param name="AddNewFlagResult">AddNewFlagResult(true) means for adding new codingtemplate
        /// AddNewFlagResult(false) means for exsisting coding template Edit functionality</param>
        /// <returns></returns>
        //public RadGridView CreateRADDatasetForEditTemplatePreview(RadGridView  dataGridViewPreviewCodingTemplate,bool AddNewFlagResult)
        //{
        //    try
        //    {
        //        IsAddCodingTemplate = AddNewFlagResult;
        //        if (IsAddCodingTemplate)
        //        {
        //            //Show textBox instead of comboBox for creating new coding template process
        //            textBoxTemplate.Visible = true;
        //            textBoxTemplate.Enabled = true;
        //            comboBoxTemplate.Visible = false;
        //            comboBoxTemplate.Enabled = false;
        //        }
        //        else
        //        {
        //            //show combobox instead of textBox for edit functionality of existing coding template
        //            textBoxTemplate.Visible = false;
        //            textBoxTemplate.Enabled = false;
        //            comboBoxTemplate.Visible = true;
        //            comboBoxTemplate.Enabled = false;
        //        }
        //        string[] HeaderRow = CommonUtilities.GetInstance().HeaderForEditTemplate;
                        

        //        #region Set DataGridViewStyle
        //        cellStyle.Font = new Font(new FontFamily("Verdana"), 8.25F);
        //        cellStyle.ForeColor = Color.Black;
        //        #endregion

        //        if (this.ConnectedSoft == EDI.Constant.Constants.QBstring)
        //        {

        //            customerList = new QBCustomerListCollection();
        //            employeeList = new QBEmployeeListCollection();
        //            vendorList = new QBVendorListCollection();


        //            //check whether CustomerList is null or not
        //            if (custList == null)
        //            {
        //                custList = customerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
        //                if (empList == null)
        //                    empList = employeeList.GetAllEmployeeList(CommonUtilities.GetInstance().CompanyFile);
        //                if (vendList == null)
        //                    vendList = vendorList.GetAllVendorList(CommonUtilities.GetInstance().CompanyFile);
        //                foreach (string empName in empList)
        //                {
        //                    if (!custList.Contains(empName))
        //                    {
        //                        custList.Add(empName);
        //                    }
        //                }
        //                foreach (string vendName in vendList)
        //                {
        //                    if (!custList.Contains(vendName))
        //                    {
        //                        custList.Add(vendName);
        //                    }
        //                }
        //                custList.Insert(0, "<None>");
        //            }
        //        }
       
        //        #region DataGridViewHeader Data

        //        FillDataGridViewHeaderData(HeaderRow);

        //        #endregion

        //        #region DataGridViewRow Data
        //        if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(CommonUtilities.GetInstance().CodingTemplateName))
        //        {
        //            #region If record found in Coding.xml

        //            FillDataGridViewRowDataFromXML(dataGridViewPreviewCodingTemplate);

        //            #endregion
        //        }
        //        else
        //        {
        //            #region if no data found in Coding.xml for first time

        //            FillDataGridViewRowDataFromXML(dataGridViewPreviewCodingTemplate);

        //            #endregion   
        //        }

        //        #endregion


        //        //Account/Item List

        //        //Name List

        //        //Customer:job List

        //        //Tax Code List

        //        //Coding Template List

        //        //set keyword text value
        //        //this.textBoxKeyword.Text = CommonUtilities.GetInstance().SelectedCodingTemplate;

        //        //set comboBox item 




        //        return radGridviewforEditTemplate;
        //    }
        //    catch
        //    {
        //        return null;
        //    }


        //}


        /// <summary>
        /// Create DataGridView for EditTemplate Form
        /// </summary>
        /// <param name="dataGridViewPreviewCodingTemplate"></param>
        /// <param name="AddNewFlagResult">AddNewFlagResult(true) means for adding new codingtemplate
        /// AddNewFlagResult(false) means for exsisting coding template Edit functionality</param>
        /// <returns></returns>
        public DataGridView CreateDatasetForEditTemplatePreview(RadGridView radGridViewPreviewCodingTemplate,bool AddNewFlagResult)
        {
            try
            {
                IsAddCodingTemplate = AddNewFlagResult;
                if (IsAddCodingTemplate)
                {
                //    //Show textBox instead of comboBox for creating new coding template process
                //    textBoxTemplate.Visible = true;
                //    textBoxTemplate.Enabled = true;
                   comboBoxTemplate.Visible = false;
                    comboBoxTemplate.Enabled = false;
                }
                else
                {
                //    //show combobox instead of textBox for edit functionality of existing coding template
                //    textBoxTemplate.Visible = false;
                //    textBoxTemplate.Enabled = false;
                    comboBoxTemplate.Visible = true;
                  comboBoxTemplate.Enabled = false;
                }
                string[] HeaderRow = CommonUtilities.GetInstance().HeaderForEditTemplate;
                        

                #region Set DataGridViewStyle
                //cellStyle.Font = new Font(new FontFamily("Verdana"), 8.25F);
                //cellStyle.ForeColor = Color.Black;
                #endregion

                if (this.ConnectedSoft == EDI.Constant.Constants.QBstring)
                {

                    customerList = new QBCustomerListCollection();
                    employeeList = new QBEmployeeListCollection();
                    vendorList = new QBVendorListCollection();
                    classList = new QBClassListCollection();

                    //check whether CustomerList is null or not
                    if (custList == null)
                    {
                        custList = customerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
                        if (empList == null)
                            empList = employeeList.GetAllEmployeeList(CommonUtilities.GetInstance().CompanyFile);
                        if (vendList == null)
                            vendList = vendorList.GetAllVendorList(CommonUtilities.GetInstance().CompanyFile);
                        if (clasList == null)
                            clasList = classList.GetAllClassList(CommonUtilities.GetInstance().CompanyFile);
                        foreach (string empName in empList)
                        {
                            if (!custList.Contains(empName))
                            {
                                custList.Add(empName);
                            }
                        }
                        foreach (string vendName in vendList)
                        {
                            if (!custList.Contains(vendName))
                            {
                                custList.Add(vendName);
                            }
                        }
                        custList.Insert(0, "<None>");
                    }
                }
               
                #region DataGridViewHeader Data

                FillDataGridViewHeaderData(HeaderRow);

                #endregion

                #region DataGridViewRow Data
                if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(CommonUtilities.GetInstance().CodingTemplateName))
                {
                    #region If record found in Coding.xml

                   // FillDataGridViewRowDataFromXML(dataGridViewPreviewCodingTemplate);
                    FillDataGridViewRowDataFromXML(radGridviewforEditTemplate);

                    #endregion
                }
                else
                {
                    #region if no data found in Coding.xml for first time

                   // FillDataGridViewRowDataFromXML(dataGridViewPreviewCodingTemplate);
                    FillDataGridViewRowDataFromXML(radGridviewforEditTemplate);

                    #endregion   
                }

                #endregion                

                return null;
            }
            catch
            {
                return null;
            }


        }


        /// <summary>
        /// Create DataGrid using Coding.xml file
        /// </summary>
        /// <param name="dataGridViewPreviewCodingTemplate"></param>
        private void FillDataGridViewRowDataFromXML(RadGridView dataGridViewPreviewCodingTemplate)
        {
            try
            {
                //Get data from Coding.xml file
                List<string[]> xmlData = CommonUtilities.GetInstance().GetXMLCodingData(CommonUtilities.GetInstance().SelectedCodingTemplate);

                bool flag = false;
                object result = null;

                #region New logic Parse Coding.xml file and show keyword record as one row

                if (xmlData.Count.Equals(0) || IsAddCodingTemplate)
                {
                    //create blank row in Edit Template form if there is no keyword information in Coding.xml filr for coding template name
                    CreateBlankRow();
                }
                else
                {

                    dt_fillgrid.Columns.Clear();
                    dt_fillgrid.Columns.Add("Keyword");
                    dt_fillgrid.Columns.Add("Name");
                    dt_fillgrid.Columns.Add("Account");
                    dt_fillgrid.Columns.Add("TaxCode");
                    dt_fillgrid.Columns.Add("CustomerJob");
                    dt_fillgrid.Columns.Add("Class");
                    dt_fillgrid.Columns.Add("BillableStatus");
                    dt_fillgrid.Columns.Add("Memo");
                    dt_fillgrid.Columns.Add("Operator");
                    dt_fillgrid.Columns.Add("RowNumber");

                    DataRow dr1 = dt_fillgrid.NewRow();
                    for (int i = 0; i < xmlData.Count; i++)
                    {
                        dr = new DataGridViewRow();

                        for (int col = 0; col < 10; col++)
                        {

                            switch (col)
                            {
                                #region Create column
                                case 0: textCell = new DataGridViewTextBoxCell();
                                    textCell.Value = xmlData[i][0];
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;                                 
                                   
                                   

                                case 1:

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Value = xmlData[i][3];
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;
                                    

                                case 2:

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Value = xmlData[i][4];
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;

                                case 3:

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Value = xmlData[i][5];
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;

                                case 4:

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Value = xmlData[i][6];
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;

                              

                                case 5:
                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    textCell.Value = xmlData[i][9];
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;
                                case 6:

                                    checkCell = new DataGridViewCheckBoxCell();

                                    if (xmlData[i][7].ToString().Equals("Billable"))
                                        result = true;
                                    else
                                        result = false;

                                    if (result != null)
                                    {
                                        checkCell.Value = result;
                                        dr.Cells.Add(checkCell);
                                    }
                                    dr1[col] = result.ToString();
                                    break;
                                case 7:
                                       textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    textCell.Value = xmlData[i][8];
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;
                                  
                                case 8:
                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    textCell.Value = xmlData[i][6];
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;
                                case 9:
                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    textCell.Value =0;
                                    dr.Cells.Add(textCell);
                                    dr1[col] = textCell.Value.ToString();
                                    break;


                                #endregion
                            }
                          
                        }
                    //    radGridviewforEditTemplate.Rows.Add(dr);

                        dt_fillgrid.Rows.Add(dr1.ItemArray);
                        rad_griddata_xml.DataSource = dt_fillgrid;
                        //  dt_fillgrid.Rows.Add(dr1.ItemArray);
                    }
                   
                  
                }
                #endregion

             //   radGridviewforEditTemplate.Rows[0].IsSelected = true;
                //axis 11 pos
                if (this.ConnectedSoft == Constants.QBstring || this.ConnectedSoft == Constants.QBPOSstring || this.ConnectedSoft == Constants.QBOnlinestring)
                {
                    UpdateComboBoxControll();
                }
               
                UpdateEditTemplateControll(0);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Create Blank row
        /// </summary>
        private void CreateBlankRow()
        {
            dr = new DataGridViewRow();
            if (dr.Cells[-1] == dr.Cells[-1])
            {

            }


            else
            {
                object result = null;

                #region Create blank row
                for (int col = 0; col < 9; col++)
                {
                    switch (col)
                    {
                        #region Create column
                        case 0: textCell = new DataGridViewTextBoxCell();
                            textCell.Value = string.Empty;
                            textCell.Style = cellStyle;
                            dr.Cells.Add(textCell);
                            break;

                        case 1:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Value = string.Empty;
                            textCell.Style = cellStyle;

                            dr.Cells.Add(textCell);
                            break;

                        case 2:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Value = string.Empty;
                            textCell.Style = cellStyle;

                            dr.Cells.Add(textCell);
                            break;

                        case 3:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Value = string.Empty;
                            textCell.Style = cellStyle;

                            dr.Cells.Add(textCell);
                            break;

                        case 4:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Value = string.Empty;
                            textCell.Style = cellStyle;

                            dr.Cells.Add(textCell);

                            break;

                        case 5:
                            checkCell = new DataGridViewCheckBoxCell();
                            result = false;
                            if (result != null)
                            {
                                checkCell.Value = result;
                                dr.Cells.Add(checkCell);
                            }

                            break;

                        case 6:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;
                        case 7:

                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = string.Empty;
                            dr.Cells.Add(textCell);
                            break;
                        case 8:
                            textCell = new DataGridViewTextBoxCell();
                            textCell.Style = cellStyle;
                            textCell.Value = 0;
                            dr.Cells.Add(textCell);
                            break;


                        #endregion
                    }
                }
                #endregion

                radGridviewforEditTemplate.Rows.Add(dr);
            }
        }

        /// <summary>
        /// Assign datasource to Name/Account/Customer/Tax comboBox
        /// </summary>
        private void UpdateComboBoxControll()
        {
            //#region Assign datasource to (Name,Account,Customer,Tax)
            //try
            //{
            //    this.comboBoxAccount.DataSource = CommonUtilities.GetInstance().ItemList;
            //    this.comboBoxAccount.SelectedIndex = -1;
            //}
            //catch
            //{ }

            //foreach (string nameData in custList)
            //{
            //    this.comboBoxName.Items.Add(nameData);
            //}
            //this.comboBoxName.SelectedIndex = -1;

            ////set as selected Row by default first row
            //foreach (string custData in custList)
            //{
            //    this.comboBoxCustomer.Items.Add(custData);
            //}
            //this.comboBoxCustomer.SelectedIndex = -1;

            //try
            //{
            //    this.comboBoxTaxCode.DataSource = CommonUtilities.GetInstance().TaxCodeList;
            //    this.comboBoxTaxCode.SelectedIndex = -1;
            //}
            //catch
            //{ }

            this.comboBoxTemplate.DataSource = CommonUtilities.GetInstance().CodingTemplateList;
            this.comboBoxTemplate.SelectedItem = CommonUtilities.GetInstance().SelectedCodingTemplate;
          //  #endregion
        }

       
        /// <summary>
        /// Fill dataGridView
        /// </summary>
        /// <param name="dataGridViewPreviewCodingTemplate"></param>
        private void FillDataGridViewRowData(DataGridView dataGridViewPreviewCodingTemplate)
        {
            try
            {

                object result = null;
                for (int row = 0; row < dataGridViewPreviewCodingTemplate.Rows.Count; row++)
                {
                    dr = new DataGridViewRow();

                    if (dataGridViewPreviewCodingTemplate.Rows[row].Cells[0].FormattedValue.ToString().Equals("True"))
                    {
                        for (int col = 0; col < 8; col++)
                        {
                            switch (col)
                            {
                                case 0: textCell = new DataGridViewTextBoxCell();
                                    //textCell.Value = CommonUtilities.GetInstance().SelectedCodingTemplate;
                                    textCell.Style = cellStyle;
                                    dr.Cells.Add(textCell);
                                    break;

                                case 1:
                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[4].FormattedValue.ToString();

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                        {
                                            if (result.ToString().Equals("<None>"))
                                                textCell.Value = string.Empty;
                                            else
                                                textCell.Value = result;
                                        }
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);
                                    }
                                    break;

                                case 2:
                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].FormattedValue.ToString();

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                        {
                                            if (result.ToString().Equals("<None>"))
                                                textCell.Value = string.Empty;
                                            else
                                                textCell.Value = result;
                                        }
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);
                                    }
                                    break;

                                case 3:
                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[6].FormattedValue.ToString();

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                        {
                                            if (result.ToString().Equals("<None>"))
                                                textCell.Value = string.Empty;
                                            else
                                                textCell.Value = result;
                                        }
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);
                                    }
                                    break;

                                case 4:
                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].FormattedValue.ToString();

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                        {
                                            if (result.ToString().Equals("<None>"))
                                                textCell.Value = string.Empty;
                                            else
                                                textCell.Value = result;
                                        }
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);

                                    }
                                    break;

                                case 5:
                                    checkCell = new DataGridViewCheckBoxCell();


                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[7].FormattedValue.ToString();
                                    if (result != null)
                                    {
                                        checkCell.Value = result;
                                        dr.Cells.Add(checkCell);
                                    }


                                    break;

                                case 6:
                                    result = dataGridViewPreviewCodingTemplate.Rows[row].Cells[5].FormattedValue.ToString();

                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;



                                    if (result != null)
                                    {
                                        if (!result.Equals(string.Empty))
                                            textCell.Value = result;
                                        else
                                            textCell.Value = string.Empty;

                                        dr.Cells.Add(textCell);

                                    }

                                    break;

                                case 7:
                                    textCell = new DataGridViewTextBoxCell();
                                    textCell.Style = cellStyle;
                                    textCell.Value = row;
                                    dr.Cells.Add(textCell);
                                    break;


                            }
                        }
                        radGridviewforEditTemplate.Rows.Add(dr);
                    }
                }

                radGridviewforEditTemplate.Rows[0].IsSelected = true;
            }
            catch
            {
            }
        }


        /// <summary>
        /// Create Header Row for DataGridView
        /// </summary>
        /// <param name="headerRow"></param>
        private void FillDataGridViewHeaderData(string[] headerRow)
        {
            #region Create Header for DataGridView of EditTemplateForm

           
            //radExpenseAccList = CommonUtilities.GetInstance().CombinedItemList;
            //radExpenseAccList.AddRange(CommonUtilities.GetInstance().ExpenseAccountList);
          

            RadDropDownList radlist = new RadDropDownList();
            string[] dataSource = new string[] { "Contains", "Matches"};
            radlist.DataSource = dataSource;
            //radlist.DataBindings();

            try
            {
                this.radGridviewforEditTemplate.AutoGenerateColumns = false;

                for (int i = 0; i < headerRow.Length; i++)
                {

                    switch (i)
                    {
                        case 0:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 120;
                            radGridviewforEditTemplate.Columns.Add(tTextColumn);
                          //  tTextColumn.FormatString = null;

                            break;



                        case 1:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 120;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = CommonUtilities.GetInstance().allcust;
                            break;

                        case 2:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 120;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = CommonUtilities.GetInstance().ItemList;
                            break;

                        case 3:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 80;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = CommonUtilities.GetInstance().TaxCodeList;

                            break;

                        case 4:
                            tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 120;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = custList;
                            // tComboColumn.DataSource = CommonUtilities.GetInstance().CombinedItemList;

                            break;
                        case 5:
                             tComboColumn = new GridViewComboBoxColumn();
                            tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 120;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = clasList;                           
                            break;

                        case 6:
                            tCheckColumn = new GridViewCheckBoxColumn();
                            tCheckColumn.HeaderText = headerRow[i].ToString();
                            tCheckColumn.Width = 80;
                            radGridviewforEditTemplate.Columns.Add(tCheckColumn);
                            break;

                        case 7:
                            tTextColumn = new GridViewTextBoxColumn();
                            tTextColumn.HeaderText = headerRow[i].ToString();
                            tTextColumn.Width = 115;
                            radGridviewforEditTemplate.Columns.Add(tTextColumn);

                            break;

                        case 8:
                            tComboColumn = new GridViewComboBoxColumn();
                           tComboColumn.HeaderText = headerRow[i].ToString();
                            tComboColumn.Width = 80;
                            radGridviewforEditTemplate.Columns.Add(tComboColumn);
                            tComboColumn.DataSource = dataSource;

                            var row = radGridviewforEditTemplate.Rows.AddNew();
                            row.Cells[i].Value = "Contains";
                            break;

                     
                        //    tComboColumn = new GridViewComboBoxColumn();
                        //    tComboColumn.HeaderText = headerRow[i].ToString();
                        //    tComboColumn.Width = 120;
                        //    radGridviewforEditTemplate.Columns.Add(tComboColumn);
                        //    tComboColumn.DataSource = custList;
                        //    // tComboColumn.DataSource = CommonUtilities.GetInstance().CombinedItemList;
                        //    break;
                        //case 9:
                           

                    }


                }

                foreach (GridViewColumn column in radGridviewforEditTemplate.Columns)
                {
                    GridViewCheckBoxColumn col = column as GridViewCheckBoxColumn;

                    if (column is GridViewCheckBoxColumn)
                    {
                        col.AllowFiltering = false;
                    }
                }


                //for (int i = 0; i < headerRow.Length; i++)
                //{
                //    if (!(radGridCodingTemplate.Columns[i] is GridViewCheckBoxColumn))
                //        radGridCodingTemplate.Columns[i].AllowFiltering = true;
                //}


            }

            catch(Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }


            #endregion
        

        /// <summary>
        /// Get dataGridview rowIndex
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewEditTemplate_MouseClick(object sender, MouseEventArgs e)
        {
           
            //   DataGridView.HitTestInfo hitTestInfo = radGridviewforEditTemplate.HitTest(e.X, e.Y);
            //rowIndex = hitTestInfo.RowIndex;

            //UpdateEditTemplateControll(rowIndex);
        }


        /// <summary>
        /// Update ComboBox value as per selected row from dataGridview
        /// </summary>
        /// <param name="rowIndex"></param>
        private void UpdateEditTemplateControll(int rowIndex)
        {

            //set Name
            if (rowIndex != -1)
            {
                //  rad_griddata_xml.DataSource = dt_fillgrid;

                for (int i = 0; i < dt_fillgrid.Rows.Count; i++)
                {
                    //set keyword
                    //if (radGridviewforEditTemplate.Rows[i].Cells[0].Value != null)
                    //{
                    try
                    {
                        if (i != 0)
                        {
                            this.radGridviewforEditTemplate.Rows.AddNew();
                        }
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString()))
                            this.radGridviewforEditTemplate.Rows[i].Cells[0].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[0].Value = dt_fillgrid.Rows[i][0].ToString();
                    }
                    catch (Exception ex)
                    {
                       this.radGridviewforEditTemplate.Rows[i].Cells[0].Value = dt_fillgrid.Rows[i][0].ToString();
                    }

                    //set name
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString()))
                            this.radGridviewforEditTemplate.Rows[i].Cells[1].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[1].Value = dt_fillgrid.Rows[i][1].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[1].Value = dt_fillgrid.Rows[i][1].ToString();
                    }
                    //set Account
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[2].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[2].Value = dt_fillgrid.Rows[i][2].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[2].Value = dt_fillgrid.Rows[i][2].ToString();
                    }

                    //Set TaxCode
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[3].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[3].Value = dt_fillgrid.Rows[i][3].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[3].Value = dt_fillgrid.Rows[i][3].ToString();
                    }
                    //Set Customer:job
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[4].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[4].Value = dt_fillgrid.Rows[i][4].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[4].Value = dt_fillgrid.Rows[i][4].ToString();
                    }
                    //Set Billing Status                       
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[5].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[5].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[5].Value = dt_fillgrid.Rows[i][5].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[5].Value = dt_fillgrid.Rows[i][5].ToString();
                    }

                    // memo
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[6].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[6].Value = dt_fillgrid.Rows[i][6].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[6].Value = dt_fillgrid.Rows[i][6].ToString();
                    }

                    ////set Contains / Matches radio button
                    try
                    {
                        if (string.IsNullOrEmpty(radGridviewforEditTemplate.Rows[i].Cells[7].Value.ToString()))
                            radGridviewforEditTemplate.Rows[i].Cells[7].Value = "";
                        else
                            this.radGridviewforEditTemplate.Rows[i].Cells[7].Value = dt_fillgrid.Rows[i][7].ToString();
                    }
                    catch (Exception ex)
                    {
                        this.radGridviewforEditTemplate.Rows[i].Cells[7].Value = dt_fillgrid.Rows[i][7].ToString();
                    }
                   
                }
               
            }
        }

        /// <summary>
        /// Save Button functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            #region Validate ComboBox
            ValidateControlls();
            #endregion

            #region  Validation for Keyword textbox

            //if(string.IsNullOrEmpty(this.tTextColumn.ToString().Trim()))
            //{
            //    MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;

            //}
            for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                if (radGridviewforEditTemplate.Rows[i].Cells[0].Value == null)
                {
                    MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (radGridviewforEditTemplate.RowCount == 0)
            {
                MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;

            }


            #endregion


            #region Validation for RadioButton

            //if (!this.radioButtonContains.Checked && !this.radioButtonMatches.Checked)
            //{
            //    MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    return;
            //}
            for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                if (radGridviewforEditTemplate.Rows[i].Cells[8].Value == null)
                {
                    MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (radGridviewforEditTemplate.RowCount == 0)
            {
                MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            #endregion

            #region Validation for Unique keyword and operator
            List<string[]> keywordInfo = new List<string[]>();
            for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                try
                {
                    string[] kdata = new string[2];
                    kdata[0] = radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString();
                    kdata[1] = radGridviewforEditTemplate.Rows[i].Cells[8].Value.ToString();
                    keywordInfo.Add(kdata);
                }
                catch
                {
                }
            }
            //string operatorValue = string.Empty;
            //if (radioButtonContains.Checked)
            //    operatorValue = "contains";
            //else if (radioButtonMatches.Checked)
            //    operatorValue = "matches";
            //for (int j = 0; j < keywordInfo.Count; j++)
            //{
            //    try
            //    {
            //        if (!string.IsNullOrEmpty(keywordInfo[j][0]) && !string.IsNullOrEmpty(keywordInfo[j][1]))
            //        {
            //            if (rowIndex==j)
            //            {
            //            }
            //            else
            //            {
            //                if (keywordInfo[j][0].ToString().ToLower().Equals(tTextColumn.ToString().Trim().ToLower()) && keywordInfo[j][1].ToString().ToLower().Equals(operatorValue))
            //                {
            //                    MessageBox.Show("Please enter unique Keyword/operator pair.");
            //                    return;
            //                }
            //            }
            //        }
            //    }
            //    catch
            //    {
            //    }
            //}
            #endregion

            try
            {
                #region Save changed value into datagridview

                //radGridviewforEditTemplate.Rows[rowIndex].Cells[0].Value = this.textBoxKeyword.Text.Trim();




                //radGridviewforEditTemplate.Rows[rowIndex].Cells[0].Value = this.radGridviewforEditTemplate.Rows[rowIndex].Cells[0].Value;
                //radGridviewforEditTemplate.Rows[rowIndex].Cells[3].Value = this.radGridviewforEditTemplate.Rows[rowIndex].Cells[3].Value;
                //radGridviewforEditTemplate.Rows[rowIndex].Cells[4].Value = this.radGridviewforEditTemplate.Rows[rowIndex].Cells[4].Value;
                //radGridviewforEditTemplate.Rows[rowIndex].Cells[5].Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[5].Value;
                //radGridviewforEditTemplate.Rows[rowIndex].Cells[6].Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[6].Value;
                //radGridviewforEditTemplate.Rows[rowIndex].Cells[8].Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[8].Value;             


                //if (this.ConnectedSoft == Constants.QBstring)
                //{
                //    radGridviewforEditTemplate.Rows[rowIndex].Cells[7].Value = this.radGridviewforEditTemplate.Rows[rowindex].Cells[7].Value;
                //}
              
                //if (this.radioButtonContains.Checked)
                //    radGridviewforEditTemplate.Rows[rowIndex].Cells[1].Value = "contains";
                //else
                //  radGridviewforEditTemplate.Rows[rowIndex].Cells[2].Value = "matches";






                #endregion


                #region if AddNewFlag is true

                if (IsAddCodingTemplate)
                {
                    #region Create name tag in Coding.xml file

                    string templateName = string.Empty;

                    if (this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd() == string.Empty)
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0131"), "Zed Axis");
                        return;
                    }
                    else
                    {
                        #region if Coding Template name exist then add that name to Codings.xml

                        templateName = this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd();

                        #region check whether same name already exist
                        bool flag = false;
                        List<string> codingTemplate = new List<string>();
                        codingTemplate = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();
                        foreach (string name in codingTemplate)
                        {
                            if (name.Equals(templateName))
                            {
                                flag = true;
                                break;
                            }
                        }
                        #endregion

                        if (!flag)
                        {
                            CommonUtilities.GetInstance().ConnectedSoftware = this.ConnectedSoft;
                            CommonUtilities.GetInstance().NewTemplateName = templateName;
                            CommonUtilities.GetInstance().WriteCodingTemplateFile(templateName);
                        }
                        else
                        {
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0132"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            flagForm = true;
                            return;
                        }
                        //  this.DialogResult = DialogResult.OK;
                        #endregion

                    }
                    #endregion

                }
                #endregion

            }
            catch
            {

            }




            try
            {
                List<string[]> keywordData = new List<string[]>();

                CommonUtilities.GetInstance().KeywordList = null;

                #region Create Keyword List
                for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                {
                    string keyword = radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString();
                    string name="";
                    try
                    {
                        if (name == "")
                        {
                        }
                        else
                        {
                            name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                        }
                    }
                    catch(Exception ex)
                    {
                        name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                    }

                    string account = "";

                    try
                    {
                        if (account == "" || account == null)
                        {
                        }
                        else
                        {
                            account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                    }
                  
                 
                    string taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                    string customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();

                    //string billable = (string)radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();

                    string billable = "";
                    try
                    {
                        if ((bool)radGridviewforEditTemplate.Rows[i].Cells[5].Value == true)
                        {
                            billable = "true";
                        }

                        else
                        {
                            billable = "false";
                        }

                    }
                    catch (Exception ex)
                    {
                        billable = "false";
                    }
                    string typeofMatch = radGridviewforEditTemplate.Rows[i].Cells[8].Value.ToString();
                 

                    string memo = "";
                    try
                    {
                        if (memo == "")
                        {
                        }
                        else
                        {
                            memo = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        memo = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                    }
                    

                  //  string memo = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        string[] editData = new string[8];
                        editData[0] = keyword;
                        editData[1] = name;
                        editData[2] = account;
                        editData[3] = taxcode;
                        editData[4] = customer;
                        editData[5] = billable;
                        editData[6] = memo;
                        editData[7] = typeofMatch;
                        keywordData.Add(editData);
                    }
                }
                //        return;
                CommonUtilities.GetInstance().KeywordList = keywordData;
                //}
                #endregion
            }
            catch (Exception ex)
            {
            }

            //ArrayList data = new ArrayList();
            //List<string> dataList = null;
            List<string[]> dataList = new List<string[]>();

            #region Update Coding.xml file



            for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
            {
                //dataList = new List<string>();
                string[] data = new string[9];
                for (int j = 0; j < this.radGridviewforEditTemplate.ColumnCount; j++)
                {

                    if (j == 5)
                    {

                        try
                        {
                            if ((bool)radGridviewforEditTemplate.Rows[i].Cells[5].Value == true)
                            {
                                data[j] = "Billable";
                            }
                            else
                            {
                                data[j] = "NotBillable";
                            }
                        }
                        catch (Exception ex)
                        {
                            data[j] = "NotBillable";
                        }
                    }

                    else
                    {
                        try
                        {
                            if (radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == "" || radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == null)
                            {
                                data[j] = "";
                            }
                            else
                            {

                                data[j] = radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                            }
                        }
                        catch
                        {
                            data[j] = "";
                            // data[j] = radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                            
                        }
                    }
                }
                dataList.Add(data);
            }

            if (dataList.Count > 0)
            {
                //modify Coding.xml as per datagridview
                CommonUtilities.GetInstance().ModifyCodingXML(dataList);
            }


            #endregion


        }


        /// <summary>
        /// Validate ComboBox control on EditTemplate Form If "None" value selected in comboBox
        /// </summary>
        private void ValidateControlls()
        {
            //if (this.comboBoxAccount.SelectedIndex == 0)
            //    this.comboBoxAccount.SelectedIndex = -1;

            //if (this.comboBoxCustomer.SelectedIndex == 0)
            //    this.comboBoxCustomer.SelectedIndex = -1;

            //if (this.comboBoxName.SelectedIndex == 0)
            //    this.comboBoxName.SelectedIndex = -1;

            //if (this.comboBoxTaxCode.SelectedIndex == 0)
               // this.comboBoxTaxCode.SelectedIndex = -1;
        }

        /// <summary>
        /// Cancel functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {

            #region  Validation for Keyword textbox        
            string templateName = string.Empty;

          
         //   CommonUtilities.splashScreen = null;
       
            DataProcessingBlocks.CommonUtilities.ShowSplashcodingtemp(EDI.Constant.Constants.CodingTemplateflashmessage, true);
            for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                if (radGridviewforEditTemplate.Rows[i].Cells[0].Value == null)
                {
                    MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (radGridviewforEditTemplate.RowCount == 0)
            {
                MessageBox.Show("Please enter keyword", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;

            }


            #endregion


            #region Validation for RadioButton

            for (int i = 0; i < radGridviewforEditTemplate.RowCount; i++)
            {
                if (radGridviewforEditTemplate.Rows[i].Cells[8].Value == null)
                {
                    MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (radGridviewforEditTemplate.RowCount == 0)
            {
                MessageBox.Show("Please either select Contains/Matches", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            #endregion

            #region Validate ComboBox controlls
            ValidateControlls();
            #endregion

            #region if AddNewFlag is true

            if (IsAddCodingTemplate)
            {
                #region Create name tag in Coding.xml file

          

                if (this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd() == string.Empty)
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0131"), "Zed Axis");
                    return;
                }
                else
                {
                    #region if Coding Template name exist then add that name to Codings.xml

                    templateName = this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd();

                    #region check whether same name already exist
                    bool flag = false;
                    List<string> codingTemplate = new List<string>();
                    codingTemplate = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();
                    foreach (string name in codingTemplate)
                    {
                        if (name.Equals(templateName))
                        {
                            flag = true;
                            break;
                        }
                    }
                    #endregion

                    if (!flag)
                    {
                        CommonUtilities.GetInstance().ConnectedSoftware = this.ConnectedSoft;
                        CommonUtilities.GetInstance().NewTemplateName = templateName;
                        CommonUtilities.GetInstance().WriteCodingTemplateFile(templateName);
                    }
                    else
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0132"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        flagForm = true;
                        return;
                    }
                    this.DialogResult = DialogResult.OK;

                    this.Close();
                    #endregion
                }
                #endregion
            }
            else
                CommonUtilities.GetInstance().NewTemplateName = string.Empty;
            if (templateName == "")
            {
                templateName = comboBoxTemplate.SelectedItem.ToString();
            }
            #endregion

            try
            {
                List<string[]> keywordData = new List<string[]>();

                CommonUtilities.GetInstance().KeywordList = null;

                #region Create Keyword List
                for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                {

                  
                    string keyword = radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString();

                    //string name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                    //string account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();

                    string name = "";
                    try
                     {
                        if (name == "")
                        {
                        }
                        else
                        {
                            name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                    }

                    string account = "";

                    try
                    {
                        if (account == "" || account == null)
                        {
                        }
                        else
                        {
                            account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                    }
                  
                    //string taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                    //string customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();
                    //string billable = (string)radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();

                    string taxcode = "";
                    try
                    {
                        if (taxcode == "")
                        {
                        }
                        else
                        {
                            taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                    }

                    string customer = "";
                    try
                    {
                        if (customer == "")
                        {
                        }
                        else
                        {
                            customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();
                    }
                       
                    string billable = "";
                    try
                    {
                        if ((bool)radGridviewforEditTemplate.Rows[i].Cells[5].Value == true)
                        {
                            billable = "true";
                        }
                       
                        else
                        {
                            billable = "false";
                        }

                    }
                    catch(Exception ex)
                    {
                        billable = "false";
                    }
                    string typeofMatch = radGridviewforEditTemplate.Rows[i].Cells[8].Value.ToString();

                    string memo = "";
                    try
                    {
                        if (memo == "")
                        {
                        }
                        else
                        {
                            memo = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        memo = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                    }
                     string clas = "";
                     if (radGridviewforEditTemplate.Rows[i].Cells[5].Value != null)
                     {
                         clas = radGridviewforEditTemplate.Rows[i].Cells[5].Value.ToString();
                     }
                 
                    if (!string.IsNullOrEmpty(keyword))
                    {
                        string[] editData = new string[9];
                        editData[0] = keyword;
                        editData[1] = name;
                        editData[2] = account;
                        editData[3] = taxcode;
                        editData[4] = customer;
                        editData[5] = billable;
                        editData[6] = memo;
                        editData[7] = clas;
                       // editData[8] = clas;
                        keywordData.Add(editData);
                    }
                }
                //        return;
                CommonUtilities.GetInstance().KeywordList = keywordData;
                //}
                #endregion

                //ArrayList data = new ArrayList();
                //List<string> dataList = null;
                List<string[]> dataList = new List<string[]>();

                #region Update Coding.xml file

             

                for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                {
                    //dataList = new List<string>();
                    string[] data = new string[9];
                    for (int j = 0; j < this.radGridviewforEditTemplate.ColumnCount; j++)
                    {
                        
                        if (j == 6)
                        {

                           try
                                {
                                    if ((bool)radGridviewforEditTemplate.Rows[i].Cells[6].Value == true)
                                    {
                                        data[j] = "Billable";
                                    }
                                    else
                                    {
                                        data[j] = "NotBillable";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    data[j] = "NotBillable";
                                }
                        }
                       
                        else
                        {                       

                                try
                                    {
                                        if (radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == "" || radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == null)
                                        {
                                            data[j] = "";
                                        }
                                        else
                                        {

                                            data[j] = radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                                        }
                                    }
                                    catch
                                    {
                                        data[j] = "";
                                       // data[j] = radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();

                                    }
                        
                      }
                    }
                    dataList.Add(data);
                }

                if (dataList.Count > 0)
                {
                    //modify Coding.xml as per datagridview
                    CommonUtilities.GetInstance().ModifyCodingXML(dataList);
                }

                #endregion

                #region Create

                string gridviewData = string.Empty;

                List<string[]> objEdit = new List<string[]>();

                for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                {
                    string[] rowData = new string[this.radGridviewforEditTemplate.ColumnCount];

                    for (int j = 0; j < this.radGridviewforEditTemplate.ColumnCount; j++)
                    {
                        if (j == 6)
                        {
                            try
                            {
                                if ((bool)radGridviewforEditTemplate.Rows[i].Cells[6].Value == true)
                                {
                                    rowData[j] = "Billable";
                                }
                                else
                                {
                                    rowData[j] = "NotBillable";
                                }
                            }
                            catch (Exception ex)
                            {
                                rowData[j] = "NotBillable";
                            }
                            //if (this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString().Equals("True"))
                            //    rowData[j] = "Billable";
                            //else
                            //    rowData[j] = "NotBillable";
                          
                        }
                      
                       
                        else
                            try
                            {
                                if (this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == "" || this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString() == null)
                                {
                                    rowData[j] = "";
                                }
                                else
                                {

                                    rowData[j] = this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                rowData[j] = "";
                            }
                    }
                    objEdit.Add(rowData);
                }

                CommonUtilities.GetInstance().EditGridviewData = objEdit;
                CodingTemplatePreview.GetInstance().matchData(templateName,false,"");

               // tempPrew.matchData(templateName);

                #endregion

                this.DialogResult = DialogResult.Cancel;

            }
            catch
            {

            }
            flagForm = true;
            this.Close();
        }

        /// <summary>
        /// ComboBoxName Selected Index changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// ComboBoxCustomer Selected Index changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (comboBoxCustomer.SelectedIndex != -1)
            //        comboBoxCustomer.SelectedItem = this.comboBoxCustomer.SelectedItem.ToString();
            //}
            //catch
            //{ }
        }


        private void dataGridViewEditTemplate_AllowUserToDeleteRowsChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// delete row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewEditTemplate_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            //try
            //{
            //    if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && e.Button == MouseButtons.Right)
            //    {
            //        radGridviewforEditTemplate.Rows[e.RowIndex].IsSelected = true;
            //        Rectangle r = radGridviewforEditTemplate.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
            //        contextMenuStripDeleteRows.Show((Control)sender, r.Left + e.X, r.Top + e.Y);
            //    }
            //}
            //catch
            //{
            //}
        }

        /// <summary>
        /// Delete row from EditTemplate DataGridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    if (radGridviewforEditTemplate.ReadOnly == false)
            //    {
            //        Int32 rowToDelete = this.radGridviewforEditTemplate.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            //        if (rowToDelete > -1)
            //        {
            //            this.radGridviewforEditTemplate.Rows.RemoveAt(rowToDelete);
            //            if (this.radGridviewforEditTemplate.RowCount.Equals(0))
            //            {
            //                CreateBlankRow();
            //            }
            //            else
            //                UpdateEditTemplateControll(0);
            //        }
            //    }
            //}
            //catch
            //{
            //}
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditTemplate_FormClosed(object sender, FormClosedEventArgs e)
        {           
            if (flagForm)
            {
                this.Close();
            }
            else
            {
                #region Validate ComboBox controlls
                ValidateControlls();
                #endregion

                #region if AddNewFlag is true

                if (IsAddCodingTemplate)
                {
                    #region Create name tag in Coding.xml file

                    string templateName = string.Empty;

                    if (this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd() == string.Empty)
                    {
                        //MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0131"), "Zed Axis");
                        return;
                    }
                    else
                    {
                        #region if Coding Template name exist then add that name to Codings.xml

                        templateName = this.textBoxTemplate.Text.Trim().TrimStart().TrimEnd();

                        #region check whether same name already exist
                        bool flag = false;
                        List<string> codingTemplate = new List<string>();
                        codingTemplate = CommonUtilities.GetInstance().getCodingTemplateNameFromFile();
                        foreach (string name in codingTemplate)
                        {
                            if (name.Equals(templateName))
                            {
                                flag = true;
                                break;
                            }
                        }
                        #endregion

                        if (!flag)
                        {
                            CommonUtilities.GetInstance().NewTemplateName = templateName;
                            CommonUtilities.GetInstance().WriteCodingTemplateFile(templateName);
                        }
                        else
                        {
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0132"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        this.DialogResult = DialogResult.Cancel;
                        this.Close();
                        #endregion
                    }
                    #endregion
                }
                else
                    CommonUtilities.GetInstance().NewTemplateName = string.Empty;

                #endregion

                try
                {
                    List<string[]> keywordData = new List<string[]>();

                    CommonUtilities.GetInstance().KeywordList = null;

                    #region Create Keyword List
                    for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                    {
                        string keyword = radGridviewforEditTemplate.Rows[i].Cells[0].Value.ToString();
                        //string name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                        //string account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                        string name = "";
                        try
                        {
                            if (name == "")
                            {
                            }
                            else
                            {
                                name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            name = radGridviewforEditTemplate.Rows[i].Cells[1].Value.ToString();
                        }

                        string account = "";

                        try
                        {
                            if (account == "" || account == null)
                            {
                            }
                            else
                            {
                                account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            account = radGridviewforEditTemplate.Rows[i].Cells[2].Value.ToString();
                        }
                        string taxcode = ""; 
                        try
                        {
                            if (taxcode == "")
                            {
                            }
                            else
                            {
                                taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            taxcode = radGridviewforEditTemplate.Rows[i].Cells[3].Value.ToString();
                        }

                        string customer = "";
                        try
                        {
                            if (customer == "")
                            {
                            }
                            else
                            {
                                customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            customer = radGridviewforEditTemplate.Rows[i].Cells[4].Value.ToString();
                        }
                       
                        string billable = radGridviewforEditTemplate.Rows[i].Cells[5].Value.ToString();
                        string typeofMatch = radGridviewforEditTemplate.Rows[i].Cells[6].Value.ToString();
                        if (!string.IsNullOrEmpty(keyword))
                        {
                            string[] editData = new string[7];
                            editData[0] = keyword;
                            editData[1] = name;
                            editData[2] = account;
                            editData[3] = taxcode;
                            editData[4] = customer;
                            editData[5] = billable;
                            editData[6] = typeofMatch;
                            keywordData.Add(editData);
                        }
                    }
                    //        return;
                    CommonUtilities.GetInstance().KeywordList = keywordData;
                    //}
                    #endregion

                    //ArrayList data = new ArrayList();
                    //List<string> dataList = null;
                    List<string[]> dataList = new List<string[]>();

                    #region Update Coding.xml file

                    for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                    {
                        //dataList = new List<string>();
                        string[] data = new string[8];
                        for (int j = 0; j < this.radGridviewforEditTemplate.ColumnCount; j++)
                        {
                            if (j == 5)
                            {
                                if (radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString().ToLower().Equals("true"))
                                {
                                    data[j] = "Billable";
                                }
                                else
                                {
                                    data[j] = "NotBillable";
                                }

                            }
                            else if (j == 6)
                            {
                                if (radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString().ToLower().Equals("contains"))
                                {
                                    data[j] = "contains";
                                }
                                else if (radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString().ToLower().Equals("matches"))
                                {
                                    data[j] = "matches";
                                }
                            }
                            else
                            {

                                data[j] = radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                            }
                        }
                        dataList.Add(data);
                    }

                    if (dataList.Count > 0)
                    {
                        //modify Coding.xml as per datagridview
                        CommonUtilities.GetInstance().ModifyCodingXML(dataList);
                    }

                    #endregion

                    #region Create

                    string gridviewData = string.Empty;

                    List<string[]> objEdit = new List<string[]>();

                    for (int i = 0; i < this.radGridviewforEditTemplate.RowCount; i++)
                    {
                        string[] rowData = new string[this.radGridviewforEditTemplate.ColumnCount];

                        for (int j = 0; j < this.radGridviewforEditTemplate.ColumnCount; j++)
                        {
                            if (j == 6)
                            {
                                if (this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString().Equals("True"))
                                    rowData[j] = "Billable";
                                else
                                    rowData[j] = "NotBillable";
                            }
                            else
                                rowData[j] = this.radGridviewforEditTemplate.Rows[i].Cells[j].Value.ToString();
                        }
                        objEdit.Add(rowData);
                    }

                    CommonUtilities.GetInstance().EditGridviewData = objEdit;

                    #endregion

                    this.DialogResult = DialogResult.Cancel;

                    this.Close();
                    
                    
                }
                catch
                {

                }
            }         
        }

        private void EditTemplate_ControlRemoved(object sender, ControlEventArgs e)
        {

        }

        private void dataGridViewEditTemplate_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radGridviewforEditTemplate.ReadOnly == false)
            {
                if (this.radGridviewforEditTemplate.Rows.Count == 0)
                {
                    //Display message when mapping is not created or selected
                    MessageBox.Show("Please select or create coding template then make the changes in preview box.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
            }
        }

        private void dataGridViewEditTemplate_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (radGridviewforEditTemplate.ReadOnly == false)
            {
                if (this.radGridviewforEditTemplate.Rows.Count == 0)
                {
                    //Display message when mapping is not created or selected
                    MessageBox.Show("Please select or create coding template then make the changes in preview box.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
            }
        }

        private void dataGridViewEditTemplate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridViewEditTemplate_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (radGridviewforEditTemplate.ReadOnly == false)
            {
                if (this.radGridviewforEditTemplate.Rows.Count == 0)
                {
                    //Display message when mapping is not created or selected
                    MessageBox.Show("Please select or create coding template then make the changes in preview box.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
            }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        }

        private void dataGridViewEditTemplate_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (radGridviewforEditTemplate.ReadOnly == false)
            {
                if (this.radGridviewforEditTemplate.Rows.Count == 0)
                {
                    //Display message when mapping is not created or selected
                    MessageBox.Show("Please select or create coding template then make the changes in preview box.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
            }        
        }

        private void dataGridViewEditTemplate_AllowUserToAddRowsChanged(object sender, EventArgs e)
        {
            if (radGridviewforEditTemplate.ReadOnly == false)
            {
                if (this.radGridviewforEditTemplate.Rows.Count == 0)
                {
                    //Display message when mapping is not created or selected
                    MessageBox.Show("Please select or create coding template then make the changes in preview box.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                    return;
                }
            }        

        }

        private void EditTemplate_Load(object sender, EventArgs e)
        {
            CodingTemplatePreview.GetInstance().radGridCodingTemplate.Dispose();
        }

        private void radGridviewforEditTemplate_CellClick(object sender, GridViewCellEventArgs e)
        {
            rowindex = e.RowIndex;
        }

        private void radGridviewforEditTemplate_Click(object sender, EventArgs e)
        {

        }

        private void radGridviewforEditTemplate_Click_1(object sender, EventArgs e)
        {


            radGridviewforEditTemplate.Rows[radGridviewforEditTemplate.Rows.Count-1].Cells[8].Value = "Contains";

        }

        //Axis 324
        private void radGridviewforEditTemplate_CreateRow(object sender, GridViewCreateRowEventArgs e)
        {
            int rowindex = e.RowInfo.Index;
            if (rowindex != -1)
            {

                radGridviewforEditTemplate.Rows[rowindex].Cells[8].Value = "Contains";
            }

        }
        #region comment
 
        //public RadGridView CreateRADDatasetForCodingTemplatePreview(string fileName, string codingTemplateName, bool flag)
       // {
       //     selectionFlag = flag;

       //     //get data for header row
       //     string[] headerRow = CommonUtilities.GetInstance().HeaderForCodingTemplate;

       //     //QIF file data consist of special keyword which gives meaning to data in file
       //     string[] typeOfDataForQIF = CommonUtilities.GetInstance().TypeOfDataForQIF;

       //     //OFX file data consist of special keyword which gives meaning to data in file
       //     string[] typeOfDataForOFX = CommonUtilities.GetInstance().TypeOfDataForOFX;

       //     //Axis 10.0 Get Expense Account List
       //     radExpenseAccountList = new QBAccountQueryListCollection();

       //     object tmpExpAccList = new object();
       //     radExpenseAccountList = new QBAccountQueryListCollection();
       //     radExpenseAccountList.PopulateExpenseAccountQuery(CommonUtilities.GetInstance().CompanyFile);




       //     if (this.ConnectedSoft == Constants.QBstring)
       //     {

       //         radCustomerList = new QBCustomerListCollection();
       //         radEmployeeList = new QBEmployeeListCollection();
       //         radVendorList = new QBVendorListCollection();
       //         radOtherList = new QBOtherListCollection();

       //         List<string> combinedCustomerList = new List<string>();

       //         //check whether CustomerList is null or not
       //         if (radCustList == null)
       //         {
       //             #region Create customer list

       //             radCustList = radCustomerList.GetAllCustomerList(CommonUtilities.GetInstance().CompanyFile);
       //             if (radEmpList == null)
       //                 radEmpList = radEmployeeList.GetAllEmployeeList(CommonUtilities.GetInstance().CompanyFile);
       //             if (radVendList == null)
       //                 radVendList = radVendorList.GetAllVendorList(CommonUtilities.GetInstance().CompanyFile);
       //             if (radOthrList == null)
       //                 radOthrList = radOtherList.GetAllOtherNameList(CommonUtilities.GetInstance().CompanyFile);

       //             combinedCustomerList.AddRange(CommonUtilities.GetInstance().CustomerListWithType);
       //             combinedCustomerList.AddRange(CommonUtilities.GetInstance().EmployeeListWithType);
       //             combinedCustomerList.AddRange(CommonUtilities.GetInstance().VendorListWithType);
       //             combinedCustomerList.AddRange(CommonUtilities.GetInstance().OtherListWithType);
       //             combinedCustomerList.Insert(0, "<None>");
       //             CommonUtilities.GetInstance().CombineCustomerList = combinedCustomerList;


       //             foreach (string empName in radEmpList)
       //             {
       //                 if (!radCustList.Contains(empName))
       //                 {
       //                     radCustList.Add(empName);
       //                 }
       //             }
       //             foreach (string vendName in radVendList)
       //             {
       //                 if (!radCustList.Contains(vendName))
       //                 {
       //                     radCustList.Add(vendName);
       //                 }
       //             }
       //             radCustList.Insert(0, "<None>");

       //             #endregion
       //         }
       //     }

       //     //if (!flag)
       //     if (this.radGridCodingTemplate.ColumnCount < 11)
       //     {
       //         FillRADHeaderData(headerRow, radCustList);
       //     }

       //     //Set comboBoxPreviewCodingTemplate selected item to codingTemplateName get from TI form
       //     comboBoxPreviewCodingTemplate.SelectedItem = codingTemplateName;

       //     if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("ofx")
       //        || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qfx"))
       //     {
       //         #region Parsing logic for OFX AND QFX

       //         //set flagQBO to false
       //         flagQBO = false;

       //         #region 1]get all List of data from browse ofx or qfx file

       //         //create object of ArrayList to store all Lists of data from browse OFX or QFX
       //         ArrayList fileData = new ArrayList();

       //         fileData = GetListsOfFileData(fileName);

       //         #endregion

       //         #region Not required commented code 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

       //         //consist of coding template matched name list
       //         //ArrayList matchList = new ArrayList();

       //         //matchList = GetMatchListOfData(fileData, codingTemplateName);


       //         #endregion

       //         #region 3]For All data create row in datagridview

       //         if (!fileData.Count.Equals(0))
       //         {

       //             Hashtable hashTableInfo;

       //             int rowCount = 0;

       //             foreach (List<string> dataList in fileData)
       //             {

       //                 #region Create hashtable from fileData

       //                 hashTableInfo = new Hashtable();
       //                 hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

       //                 #endregion

       //                 #region Fill Datarow of datagridview

       //                 //check in Codings.xml file
       //                 if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
       //                 {
       //                     if (this.ConnectedSoft == Constants.QBstring)
       //                     {
       //                         //fill dataGridview with file data
       //                         FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
       //                     }
       
       //                     #region Not Required Commented if data found in Coding.xml
       //                     //if (rowCount.Equals(fileData.Count - 1))
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
       //                     //}
       //                     //else
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
       //                     //}

       //                     #endregion

       //                 }
       //                 else
       //                 {
       //                     if (!CommonUtilities.GetInstance().IsImportProcess)
       //                     {
       //                         #region If no data found in Coding.xml
       //                         if (this.ConnectedSoft == Constants.QBstring)
       //                         {
       //                             //fill dataGridview with file data
       //                             FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
       //                         }
      
       //                         #endregion
       //                     }
       //                 }

       //                 #endregion

       //                 //increment rowCount
       //                 rowCount++;
       //             }
       //         }


       //         #endregion

       //         #endregion
       //     }
       //     else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo"))
       //     {

       //         #region Parsing logic for New QBO

       //         //set flagQBO to false
       //         flagQBO = false;

       //         #region 1]get all List of data from browse QBO

       //         //create object of ArrayList to store all Lists of data from browse OFX or QFX
       //         ArrayList fileData = new ArrayList();

       //         //fileData = GetListsOfFileData(fileName);
       //         fileData = GetListsOfQBOFileData(fileName);

       //         #endregion

       //         #region Not required commented code 2]As u get all List of data in ArrayList format then check for your coding template name in each List of ArrayList

       //         //consist of coding template matched name list
       //         //ArrayList matchList = new ArrayList();

       //         //matchList = GetMatchListOfData(fileData, codingTemplateName);


       //         #endregion

       //         #region 3]For All data create row in datagridview

       //         if (!fileData.Count.Equals(0))
       //         {

       //             Hashtable hashTableInfo;

       //             int rowCount = 0;

       //             foreach (List<string> dataList in fileData)
       //             {

       //                 #region Create hashtable from fileData

       //                 hashTableInfo = new Hashtable();
       //                 hashTableInfo = GetDataInKeyValueFormat(dataList, typeOfDataForOFX);

       //                 #endregion

       //                 #region Fill Datarow of datagridview

       //                 //check in Codings.xml file
       //                 if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
       //                 {
       //                     if (this.ConnectedSoft == Constants.QBstring)
       //                     {
       //                         //fill dataGridview with file data
       //                         FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
       //                     }
      
       //                     #region Not Required Commented if data found in Coding.xml
       //                     //if (rowCount.Equals(fileData.Count - 1))
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
       //                     //}
       //                     //else
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
       //                     //}

       //                     #endregion

       //                 }
       //                 else
       //                 {
       //                     if (!CommonUtilities.GetInstance().IsImportProcess)
       //                     {
       //                         #region If no data found in Coding.xml
       //                         if (this.ConnectedSoft == Constants.QBstring)
       //                         {
       //                             //fill dataGridview with file data
       //                             FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
       //                         }
       
       //                         #endregion
       //                     }
       //                 }

       //                 #endregion

       //                 //increment rowCount
       //                 rowCount++;
       //             }
       //         }


       //         #endregion

       //         #endregion


       //     }

       //     else if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qif"))
       //     {
       //         #region Parsing Logic for QIF file

       //         //set flagQBO to false
       //         flagQBO = false;

       //         #region 1]get all List of data from browse QIF file

       //         ArrayList qifFileData = new ArrayList();

       //         qifFileData = GetQifFileData(fileName);

       //         #endregion



       //         #region 3]For All data create row in datagridview

       //         //if file contains data
       //         if (!qifFileData.Count.Equals(0))
       //         {
       //             Hashtable hashTableInfo;
       //             int rowCount = 0;

       //             foreach (List<string> mdataList in qifFileData)
       //             {
       //                 #region Create hashtable from qifFileData

       //                 hashTableInfo = new Hashtable();
       //                 hashTableInfo = GetDataInKeyValueFormat(mdataList, typeOfDataForQIF);

       //                 #endregion

       //                 #region Fill newly created row with data from file

       //                 if (CommonUtilities.GetInstance().IsRecordExistForCodingTemplateName(codingTemplateName))
       //                 {
       //                     if (this.ConnectedSoft == Constants.QBstring)
       //                     {
       //                         //fill dataGridview with file data
       //                         FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);

       //                     }
     
       //                     #region Not required Commented if data found in Coding.xml
       //                     //if (rowCount.Equals(qifFileData.Count - 1))
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName, rowCount, true);
       //                     //}
       //                     //else
       //                     //{
       //                     //    FillDataGridViewRowWithXMLFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension,codingTemplateName ,rowCount, false);
       //                     //}

       //                     #endregion

       //                 }
       //                 else
       //                 {
       //                     if (!CommonUtilities.GetInstance().IsImportProcess)
       //                     {
       //                         #region If no data found in Coding.xml

       //                         if (this.ConnectedSoft == Constants.QBstring)
       //                         {
       //                             //fill dataGridview with file data
       //                             FillRADGridRowWithFileData(hashTableInfo, CommonUtilities.GetInstance().BrowseFileExtension, codingTemplateName, radCustList);
       //                         }
      
       //                         #endregion
       //                     }
       //                 }
       //                 #endregion

       //                 //increment rowCount
       //                 rowCount++;

       //             }
       //         }

       //         #endregion
       //         #endregion

       //     }




       //     //get keyword information from Coding.xml file based on matching codingTemplateName 
       //     CommonUtilities.GetInstance().ConnectedSoftware = this.ConnectedSoft;
       //     List<string[]> matchedData = CommonUtilities.GetInstance().GetKeywordDataForCodingTemplate(codingTemplateName);


       //     salesReceptInfo = GetSalesReceiptData();
       //     checkInfo = GetChecktData();
       //     creditCardChargeInfo = GetCreditCardChargeData();
       //     creditCardCreditInfo = GetCreditCardCreditData();

       //     //dtTransactionInfo = 

       //     //for (int j = 0; j < matchedData.Count; j++)
       //     //    for (int row = 0; row < radGridCodingTemplate.Rows.Count; row++)

       //     try
       //     {


       //         for (int j = 0; j < matchedData.Count; j++)
       //         {

       //             #region Modify datagridview as per matched Data

       //             for (int row = 0; row < radGridCodingTemplate.Rows.Count; row++)
       //             {
       //                 if (matchedData[j][7].ToString().ToLower().Equals("contains"))
       //                 {


       //                     if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
       //                         || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
       //                     {
       //                         #region Contains found ... update datagridview

       //                         radGridCodingTemplate.Rows[row].Cells[0].Value = true;
       //                         radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
       //                         radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
       //                         radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
       //                         radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
       //                         radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
       //                         radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];

       //                         if (creditAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
       //                         {
       //                             if (radGridCodingTemplate.Rows[row].Cells[10].Style.ForeColor == Color.Red)
       //                                 tranType = "CreditCardCharge";
       //                             else
       //                                 tranType = "CreditCardCredit";

       //                         }

       //                         if (bankAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
       //                         {
       //                             if (radGridCodingTemplate.Rows[row].Cells[10].Style.ForeColor == Color.Red)
       //                                 tranType = "Check";
       //                             else
       //                                 tranType = "SalesReceipt";

       //                         }

       //                         string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

       //                         string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

       //                         string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

       //                         string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

       //                         string tmpPayment;

       //                         if (radGridCodingTemplate.Rows[row].Cells[10].Style.ForeColor == Color.Red)
       //                             tmpPayment = "-" + radGridCodingTemplate.Rows[row].Cells[10].Value.ToString();
       //                         else
       //                             tmpPayment = radGridCodingTemplate.Rows[row].Cells[10].Value.ToString();


       //                         string tmpBillable = string.Empty;

       //                         if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == true)
       //                             tmpBillable = "Billable";
       //                         if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == false)
       //                             tmpBillable = "NotBillable";



       //                         string tmpRefNo = radGridCodingTemplate.Rows[row].Cells[2].Value.ToString();

       //                         string curDate = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
       //                         DateTime currentDate = Convert.ToDateTime(curDate);


       //                         DateTime fromDate = currentDate.AddDays(-5);
       //                         DateTime toDate = currentDate.AddDays(5);



       //                         string matchString = string.Empty;
       //                         matchString = objImportSplashScreen.GetTransactionInformation(tranType, tmpRefNo, fromDate, toDate, accountType, selectedAccountType, tmpPayment);

       //                         string[] match = new string[4];

       //                         if (matchString != null)
       //                         {
       //                             match = matchString.Split(':');

       //                             matchedAmount = Convert.ToDouble(match[1].ToString());
       //                             accType = match[2].ToString();

       //                         }
       //                         if (tranType == "SalesReceipt")
       //                         {


       //                             foreach (DataRow Dr in salesReceptInfo.Rows)
       //                             {
       //                                 string tmp = Dr[5].ToString();
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(tmp);

       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[49].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_S";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }
       //                                 #region Axis 10 commented code
       //                                 //tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
       //                                 //tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
       //                                 //tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
       //                                 //dateResult == 0
       //                                 #endregion

       //                             }

       //                             if (selectedAccountType.Equals(accType) &&
       //                                     matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                    )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_S";

       //                             }


       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == string.Empty)
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }


       //                         }

       //                         if (tranType == "Check")
       //                         {


       //                             foreach (DataRow Dr in checkInfo.Rows)
       //                             {

       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[4].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);


       //                                 if (tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[6].ToString().ToLower()) &&
       //                                      tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_C";
       //                                     tmpTxnId = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();

       //                                 }

       //                                 #region Commented Code
       //                                 //tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
       //                                 //tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
       //                                 //tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
       //                                 //dateResult == 0 

       //                                 #endregion





       //                             }

       //                             if (selectedAccountType.Equals(accType) && matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment))
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_C";

       //                             }



       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == string.Empty)
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }

       //                         }




       //                         if (tranType == "CreditCardCredit")
       //                         {


       //                             foreach (DataRow Dr in creditCardCreditInfo.Rows)
       //                             {
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[3].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_CR";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }

       //                                 #region Commented Code
       //                                 //tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                 //tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                 //tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                 //tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                 //dateResult == 0 
       //                                 #endregion





       //                             }
       //                             if (selectedAccountType.Equals(accType) &&
       //                                     matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                    )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_CR";

       //                             }



       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == string.Empty)
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }


       //                         }



       //                         if (tranType == "CreditCardCharge")
       //                         {


       //                             foreach (DataRow Dr in creditCardChargeInfo.Rows)
       //                             {
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[3].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                      dateResult == 0


       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_CH";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }

       //                                 #region Commented Code
       //                                 //tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                 //tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                 //tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                 //tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                 //dateResult == 0 
       //                                 #endregion




       //                             }

       //                             if (selectedAccountType.Equals(accType) &&
       //                                     matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                    )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_CH";

       //                             }




       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == string.Empty)
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }


       //                         }

       //                         #region commentcode

       //                         //if (matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment))
       //                         //{
       //                         //    radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                         //    if (tranType == "SalesReceipt")
       //                         //        radGridCodingTemplate.Rows[row].Cells[13].Value = match[0]+"_S";
       //                         //    if (tranType == "Check")
       //                         //        radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_C";
       //                         //}


       //                         //for (int i = 0; i < salesReceptInfo.Rows.Count; i++)
       //                         //   {
       //                         //       string test = string.Empty;
       //                         //       test = salesReceptInfo.Rows[i].ItemArray[42].ToString();
       //                         //     if(radGridCodingTemplate.Rows[row].Cells[6].Value.ToString().ToLower().Equals(salesReceptInfo.Rows[i].ItemArray[42].ToString()))
       //                         //       radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                         //   }

       //                         #endregion


       //                         #endregion
       //                     }


       //                 }
       //                 else if (matchedData[j][7].ToString().ToLower().Equals("matches"))
       //                 {
       //                     if (radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower().Contains(matchedData[j][0].ToLower())
       //                        || matchedData[j][0].ToLower().Contains(radGridCodingTemplate.Rows[row].Cells[3].Value.ToString().ToLower()))
       //                     {
       //                         #region Match found ... update datagridview
       //                         radGridCodingTemplate.Rows[row].Cells[0].Value = true;
       //                         radGridCodingTemplate.Rows[row].Cells[4].Value = matchedData[j][6];
       //                         radGridCodingTemplate.Rows[row].Cells[5].Value = matchedData[j][1];
       //                         radGridCodingTemplate.Rows[row].Cells[6].Value = matchedData[j][2];
       //                         radGridCodingTemplate.Rows[row].Cells[7].Value = matchedData[j][3];
       //                         radGridCodingTemplate.Rows[row].Cells[8].Value = matchedData[j][4];
       //                         radGridCodingTemplate.Rows[row].Cells[9].Value = matchedData[j][5];

       //                         if (creditAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
       //                         {
       //                             if (radGridCodingTemplate.Rows[row].Cells[10].Style.ForeColor == Color.Red)
       //                                 tranType = "CreditCardCharge";
       //                             else
       //                                 tranType = "CreditCardCredit";

       //                         }

       //                         if (bankAccount.Contains(CommonUtilities.GetInstance().ComboBoxImportMapping.ToString()))
       //                         {
       //                             if (radGridCodingTemplate.Rows[row].Cells[10].Style.ForeColor == Color.Red)
       //                                 tranType = "Check";
       //                             else
       //                                 tranType = "SalesReceipt";

       //                         }

       //                         string tmpMemo = radGridCodingTemplate.Rows[row].Cells[4].Value.ToString();

       //                         string tmpCustomerName = radGridCodingTemplate.Rows[row].Cells[5].Value.ToString();

       //                         string tmpItemName = radGridCodingTemplate.Rows[row].Cells[6].Value.ToString();

       //                         string tmpTax = radGridCodingTemplate.Rows[row].Cells[7].Value.ToString();

       //                         string tmpPayment = radGridCodingTemplate.Rows[row].Cells[10].Value.ToString();
       //                         string tmpBillable = string.Empty;

       //                         if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == true)
       //                             tmpBillable = "Billable";
       //                         if (bool.Parse(radGridCodingTemplate.Rows[row].Cells[9].Value.ToString()) == false)
       //                             tmpBillable = "NotBillable";



       //                         string tmpRefNo = radGridCodingTemplate.Rows[row].Cells[2].Value.ToString();

       //                         string curDate = radGridCodingTemplate.Rows[row].Cells[1].Value.ToString();
       //                         DateTime currentDate = Convert.ToDateTime(curDate);


       //                         DateTime fromDate = currentDate.AddDays(-5);
       //                         DateTime toDate = currentDate.AddDays(5);



       //                         string matchString = string.Empty;
       //                         matchString = objImportSplashScreen.GetTransactionInformation(tranType, tmpRefNo, fromDate, toDate, accountType, selectedAccountType, tmpPayment);
       //                         string[] match = new string[4];
       //                         if (matchString != null)
       //                         {
       //                             match = matchString.Split(':');
       //                             matchedAmount = Convert.ToDouble(match[1].ToString());
       //                             accType = match[2].ToString();

       //                         }
       //                         if (tranType == "SalesReceipt")
       //                         {


       //                             foreach (DataRow Dr in salesReceptInfo.Rows)
       //                             {
       //                                 string tmp = Dr[5].ToString();
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(tmp);

       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[49].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_S";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }
       //                                 #region Commented code
       //                                 //tmpMemo.ToLower().Equals(Dr[34].ToString().ToLower()) &&
       //                                 //     tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //     tmpItemName.ToLower().Equals(Dr[42].ToString().ToLower()) &&
       //                                 //     tmpTax.ToLower().Equals(Dr[52].ToString().ToLower()) &&
       //                                 //     dateResult == 0 
       //                                 #endregion



       //                             }

       //                             if (selectedAccountType.Equals(accType) &&
       //                                      matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                     )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_S";

       //                             }




       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == "")
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }


       //                         }

       //                         if (tranType == "Check")
       //                         {


       //                             foreach (DataRow Dr in checkInfo.Rows)
       //                             {

       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[4].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);


       //                                 if (tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[6].ToString().ToLower()) &&
       //                                      tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_C";
       //                                     tmpTxnId = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();

       //                                 }
       //                                 #region commented code
       //                                 //tmpMemo.ToLower().Equals(Dr[10].ToString().ToLower()) &&
       //                                 //     tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //     tmpItemName.ToLower().Equals(Dr[27].ToString().ToLower()) &&
       //                                 //     tmpBillable.ToLower().Equals(Dr[26].ToString().ToLower()) &&
       //                                 //     dateResult == 0
       //                                 #endregion



       //                             }
       //                             if (selectedAccountType.Equals(accType) &&
       //                                      matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                     )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_C";

       //                             }




       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == "")
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }

       //                         }




       //                         if (tranType == "CreditCardCredit")
       //                         {


       //                             foreach (DataRow Dr in creditCardCreditInfo.Rows)
       //                             {
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[3].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                      dateResult == 0

       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_CR";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }
       //                                 #region commented code
       //                                 //tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                 //     tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //     tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                 //     tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                 //     tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                 //     dateResult == 0
       //                                 #endregion





       //                             }

       //                             if (selectedAccountType.Equals(accType) &&
       //                                     matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                    )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_CR";

       //                             }


       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == "")
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }


       //                         }



       //                         if (tranType == "CreditCardCharge")
       //                         {


       //                             foreach (DataRow Dr in creditCardChargeInfo.Rows)
       //                             {
       //                                 DateTime tmpDate = new DateTime();
       //                                 tmpDate = Convert.ToDateTime(Dr[3].ToString());
       //                                 int dateResult = DateTime.Compare(currentDate, tmpDate);

       //                                 if (tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                      tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                      tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                      tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                      tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                      dateResult == 0


       //                                     )
       //                                 {
       //                                     radGridCodingTemplate.Rows[row].Cells[12].Value = "Imported";
       //                                     radGridCodingTemplate.Rows[row].Cells[13].Value = Dr[0].ToString() + "_CH";
       //                                     tmpTxnId1 = radGridCodingTemplate.Rows[row].Cells[13].Value.ToString();


       //                                 }

       //                                 #region Commented code
       //                                 //tmpMemo.ToLower().Equals(Dr[14].ToString().ToLower()) &&
       //                                 //     tmpCustomerName.ToLower().Equals(Dr[2].ToString().ToLower()) &&
       //                                 //     tmpItemName.ToLower().Equals(Dr[19].ToString().ToLower()) &&
       //                                 //     tmpTax.ToLower().Equals(Dr[17].ToString().ToLower()) &&
       //                                 //     tmpPayment.ToLower().Equals(Dr[4].ToString().ToLower()) &&
       //                                 //     dateResult == 0
       //                                 #endregion

       //                             }



       //                             if (selectedAccountType.Equals(accType) &&
       //                                  matchedAmount != null && matchedAmount != 0.0 && matchedAmount == Convert.ToDouble(tmpPayment)
       //                                 )
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Matched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = match[0] + "_CH";

       //                             }


       //                             if (radGridCodingTemplate.Rows[row].Cells[12].Value.ToString() == "")
       //                             {
       //                                 radGridCodingTemplate.Rows[row].Cells[12].Value = "Unmatched";
       //                                 radGridCodingTemplate.Rows[row].Cells[13].Value = "";
       //                             }

       //                         }




       //                         #endregion
       //                     }
       //                 }
       //             }

       //             #endregion
       //         }
       //     }
       //     catch (Exception exp)
       //     {
       //         MessageBox.Show("Error :" + exp.Message);
       //     }



       //     selectionFlag = true;

       //     return radGridCodingTemplate;


     //   }


        //private void dataGridViewEditTemplate_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
       
        //}
        #endregion comment
    }
}