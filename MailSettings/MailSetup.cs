using System;
using System.Collections.Generic;
using System.Text;
using DBConnection;
using SQLQueries;
using System.Data;
using DataProcessingBlocks;

namespace MailSettings
{
    /// <summary>
    /// This uses for configure mail account setting.
    /// </summary>
    public class MailSetup
    {
        #region Private Member

        //int m_MailSetupID;
        int m_MailProtcolID;
        int m_InPort;
        int m_OutPort;
        string m_IncommingServer;
        string m_OutgoingServer;
        string m_UserName;
        string m_Email;
        string m_Password;
        bool m_IsPasswordRemembered;
        bool m_IsSSL;
        bool m_IsAuto;
        bool m_IsSend;
        bool m_IsRecieve;
        int m_scheduleInMin;
        #endregion
      

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mProtocol">Protocol name</param>
        /// <param name="inServer">Incomming Server name</param>
        /// <param name="outServer">Outgoing Server name</param>
        /// <param name="inPort">Incomming Port number</param>
        /// <param name="outPort">Outgoing port number</param>
        /// <param name="isSSL">SSL</param>
        /// <param name="userName">user name</param>
        /// <param name="email">email Id</param>
        /// <param name="Pass">password</param>
        public MailSetup(string mProtocol, string inServer, string outServer, string inPort, string outPort, bool isSSL, string userName, string email, string Pass,bool isAuto,bool isSend,bool isReceive,int inMin)
        {
            //this.m_MailSetupID = -1;
            this.m_MailProtcolID = TradingPartner.GetProtocolId(mProtocol);
            this.m_InPort = Convert.ToInt16(inPort);
            this.m_OutPort = Convert.ToInt16(outPort);
            this.m_IsSSL = isSSL;
            this.m_IncommingServer = inServer;
            this.m_OutgoingServer = outServer;
            this.m_UserName = userName;
            this.m_Email = email;
            this.m_Password=Pass;
            this.m_IsAuto = isAuto;
            this.m_IsSend = isSend;
            this.m_IsRecieve = isReceive;
            this.m_scheduleInMin = inMin;
        }

        /// <summary>
        /// Default empty constructor
        /// </summary>
        public MailSetup()
        {
            //empty constructor
        }

       
        #region Public Properties
        /// <summary>
        /// Get or set is password to be rememebered
        /// </summary>
        public bool IsPasswordRemembered
        {
            get { return m_IsPasswordRemembered; }
            set { m_IsPasswordRemembered = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Save the mail account details
        /// </summary>
        /// <returns>true for successful</returns>
        public bool SaveMailSetupDetails()
        {
            string deleteQuery = SQLQueries.Queries.SQ008;
            string insertQuery = string.Empty;
            if(IsPasswordRemembered)
               insertQuery = string.Format(Queries.SQ006,
                                                  m_MailProtcolID,
                                                  m_InPort,
                                                  m_OutPort,
                                                  m_IsSSL,
                                                  m_IncommingServer,
                                                  m_OutgoingServer,
                                                  m_UserName,
                                                  m_Email,
                                                  m_Password,
                                                  m_IsAuto,
                                                  m_IsSend,
                                                  m_IsRecieve,
                                                  m_scheduleInMin);
            else
               insertQuery = string.Format(Queries.SQ007,
                                                 m_MailProtcolID,
                                                 m_InPort,
                                                 m_OutPort,
                                                 m_IsSSL,
                                                 m_IncommingServer,
                                                 m_OutgoingServer,
                                                 m_UserName,
                                                 m_Email,
                                                 m_IsAuto,
                                                 m_IsSend,
                                                 m_IsRecieve,
                                                 m_scheduleInMin);
            try
            {
                MySqlDataAcess.ExecuteNonQuery(deleteQuery);             //Deleting privious record.
                int row = MySqlDataAcess.ExecuteNonQuery(insertQuery);   //Inserting new one.
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Geting mail account details
        /// </summary>
        /// <returns>Dataset of mail account details</returns>
        public static DataSet GetMailSetupDetails()
        {
            string commandText = Queries.SQ015;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Getting new mail arrived in the inbox.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetNewMail()
        {
            string commandText = Queries.SQ048;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// GetAttachment details from the database by messageID.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetAttchmentById(int Id)
        {
            string commandText = string.Format(Queries.SQ049,Id);
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        public static DataSet GetUnReadMail()
        {
            string commandText = Queries.SQLGetMsg;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }
        #endregion 

    }
}
