using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DataProcessingBlocks;
using SQLQueries;
using DBConnection;
using System.Xml;
using System.Windows.Forms;
using EDI.Constant;


namespace DataProcessingBlocks.MailSettings
{
    /// <summary>
    /// MessageRules Class for saving the message ruled details.
    /// </summary>
    class MessageRules
    {
        #region Private Members

        long m_RuleId;
        string m_Name;
        string m_TradingPartner;
        string m_MsgExactMatch;
        string m_MsgSpecificWordMatch;
        string m_FileExactMatch;
        string m_FileSpecificWordMatch;
        string m_TransactionType;
        string m_UseMapping;
        string m_SuccessStatus;
        string m_ErrorStatus;
        string m_RuleStatus;
        bool m_SoundChime;
        bool m_SoundAlarm;
        static DataTable m_dtTradingPartner;
        static DataTable m_serviceName;
        //static DataTable m_dtTransactionType;
        //static DataTable m_dtUseMapping;
        static DataTable m_dtSuccessStatus;
        static DataTable m_dtErrorStatus;
        string m_ConnectedSoftware = null;

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor is used to initialise the member variables.
        /// </summary>
        /// <param name="RuleId"></param>
        /// <param name="Name">Name of the Rule.</param>
        /// <param name="TradingPartner">Email Id of Trading Partner.</param>
        /// <param name="MsgExactMatch">Matching exact criteria with the message.</param>
        /// <param name="MsgSpecificWordMatch">Matching specific word with the message.</param>
        /// <param name="FileExactMatch">Matching exact criteria with the Attachment.</param>
        /// <param name="FileSpecificWordMatch">Matching specific word with the Attachment.</param>
        /// <param name="TransactionType">Type of Transaction.</param>
        /// <param name="UseMapping">Mapping List.</param>
        /// <param name="SuccessStatus">Status of Mail.</param>
        /// <param name="ErrorStatus">Status of Mail.</param>
        /// <param name="RuleStatus">Status rule.</param>
        /// <param name="Chime">Chime sound.</param>
        /// <param name="Alaram">Alaram Sound.</param>

        public MessageRules(long RuleId, string Name, string TradingPartner, string MsgExactMatch, string MsgSpecificWordMatch, string FileExactMatch, string FileSpecificWordMatch, string TransactionType, string UseMapping, string SuccessStatus, string ErrorStatus, string RuleStatus, bool Chime, bool Alarm)
        {
            this.m_RuleId = RuleId;
            this.m_Name = Name;
            this.m_TradingPartner = TradingPartner;
            this.m_MsgExactMatch = MsgExactMatch;
            this.m_MsgSpecificWordMatch = MsgSpecificWordMatch;
            this.m_FileExactMatch = FileExactMatch;
            this.m_FileSpecificWordMatch = FileSpecificWordMatch;
            this.m_TransactionType = TransactionType;
            this.m_UseMapping = UseMapping;
            this.m_SuccessStatus = SuccessStatus;
            this.m_ErrorStatus = ErrorStatus;

            if (RuleStatus == "True")
                this.m_RuleStatus = "Active";
            else
                this.m_RuleStatus = "NotActive";

            this.m_SoundChime = Chime;
            this.m_SoundAlarm = Alarm;
        }

        public MessageRules()
        { }

        public MessageRules(string strConnectedSoftware)
        {
            this.m_ConnectedSoftware = strConnectedSoftware.ToString();
        }

        #endregion


        #region Public Properties

        //public bool IsActive
        //{
        //    get { return m_RuleStatus; }
        //    set { m_RuleStatus = value; }
        //}
        //public bool IsChime
        //{
        //    get { return m_SoundChime; }
        //    set { m_SoundChime = value; }
        //}
        //public bool IsAlarm
        //{
        //    get { return m_SoundAlarm; }
        //    set { m_SoundAlarm = value; }
        //}

        #endregion

        #region Public Methods


        /// <summary>
        /// This function is used to match the rules.
        /// </summary>
        /// <param name="msgRules"></param>
        /// <returns></returns>
        public bool Equal(MessageRules msgRules)
        {
            if (msgRules == null)
                return false;
            if (this.m_RuleId.Equals(msgRules.m_RuleId) &&
                this.m_Name.Equals(msgRules.m_Name) &&
                this.m_TradingPartner.Equals(msgRules.m_TradingPartner) &&
                this.m_MsgExactMatch.Equals(msgRules.m_MsgExactMatch) &&
                this.m_MsgSpecificWordMatch.Equals(msgRules.m_MsgSpecificWordMatch) &&
                this.m_FileExactMatch.Equals(msgRules.m_FileExactMatch) &&
                this.m_FileSpecificWordMatch.Equals(msgRules.m_FileSpecificWordMatch) &&
                this.m_TransactionType.Equals(msgRules.m_TransactionType) &&
                this.m_UseMapping.Equals(msgRules.m_UseMapping) &&
                this.m_SuccessStatus.Equals(msgRules.m_SuccessStatus) &&
                this.m_ErrorStatus.Equals(msgRules.m_ErrorStatus))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get the details of the Trading Partner from the database.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTradingPartner()
        {
            string sqlstring = SQLQueries.Queries.SQ041;
            try
            {
                DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(sqlstring);
                m_dtTradingPartner = dataset.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtTradingPartner;
        }

        /// <summary>
        /// Get the details of the service name from the database.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetServiceName()
        {
            string sqlstring = SQLQueries.Queries.SQMServiceName;
            try
            {
                DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(sqlstring);
                m_serviceName = dataset.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_serviceName;
        }

        /// <summary>
        /// This function is used to get the TP id of the selected TP email id from the TP combobox by the user.
        /// </summary>
        /// <param name="tradingPartnerEmailId"></param>
        /// <returns></returns>
        public static int GetTradingPartnerId(string tradingPartnerEmailId)
        {
            int tpId = 0;
            foreach (DataRow dr in m_dtTradingPartner.Rows)
            {
                if (dr[TradingPartnerColumns.EmailID.ToString()].ToString() == tradingPartnerEmailId)
                {
                    tpId = Convert.ToInt32(dr[TradingPartnerColumns.TradingPartnerID.ToString()]);
                    break;
                }
            }
            return tpId;
        }

        /// <summary>
        /// Get the trading partner email id. 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetTradingPartnerEmailId(int Id)
        {
            string TPEmail = string.Empty;
            foreach (DataRow dr in m_dtTradingPartner.Rows)
            {
                if (Convert.ToInt32(dr[TradingPartnerColumns.TradingPartnerID.ToString()]) == Id)
                {
                    TPEmail = dr[TradingPartnerColumns.EmailID.ToString()].ToString();
                    break;
                }
            }
            return TPEmail;
        }

        /// <summary>
        /// Get the Message status details.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetSuccessMessageStatus()
        {
            string sqlstring = SQLQueries.Queries.SQ042;
            try
            {
                DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(sqlstring);
                m_dtSuccessStatus = dataset.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtSuccessStatus;
        }

        /// <summary>
        /// Get the Message status details for applying Message Rules.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetMessageStatus()
        {
            string sqlstring = SQLQueries.Queries.SQ054;
            try
            {
                DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(sqlstring);
                m_dtSuccessStatus = dataset.Tables[0];
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtSuccessStatus;
        }

        /// <summary>
        /// Get the message status id of the selected status from the SuccessStatus combobox.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int GetSuccessMessageStatusId(string status)
        {
            int statusId = 0;
            foreach (DataRow dr in m_dtSuccessStatus.Rows)
            {
                if (dr[MessageStatusColumns.Name.ToString()].ToString() == status)
                {
                    statusId = Convert.ToInt32(dr[MessageStatusColumns.StatusId.ToString()]);
                    break;
                }
            }
            return statusId;
        }

        /// <summary>
        /// Get the Message status name.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetSuccessMessageStatus(int Id)
        {
            string statusName = string.Empty;
            foreach (DataRow dr in m_dtSuccessStatus.Rows)
            {
                if (Convert.ToInt32(dr[MessageStatusColumns.StatusId.ToString()]) == Id)
                {
                    statusName = dr[MessageStatusColumns.Name.ToString()].ToString();
                    break;
                }
            }
            return statusName;
        }

        /// <summary>
        /// Get the Message status details.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetErrorMessageStatus()
        {
            string sqlstring = SQLQueries.Queries.SQ042;
            try
            {
                DataSet dataset = DBConnection.MySqlDataAcess.ExecuteDataset(sqlstring);
                m_dtErrorStatus = dataset.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtErrorStatus;
        }

        /// <summary>
        /// Get the message status id of the selected status from the SuccessStatus combobox.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int GetErrorMessageStatusId(string status)
        {
            int statusId = 0;
            foreach (DataRow dr in m_dtErrorStatus.Rows)
            {
                if (dr[MessageStatusColumns.Name.ToString()].ToString() == status)
                {
                    statusId = Convert.ToInt32(dr[MessageStatusColumns.StatusId.ToString()]);
                    break;
                }
            }
            return statusId;
        }

        /// <summary>
        /// Get the Message status name.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetErrorMessageStatus(int Id)
        {
            string statusName = string.Empty;
            foreach (DataRow dr in m_dtErrorStatus.Rows)
            {
                if (Convert.ToInt32(dr[MessageStatusColumns.StatusId.ToString()]) == Id)
                {
                    statusName = dr[MessageStatusColumns.Name.ToString()].ToString();
                    break;
                }
            }
            return statusName;
        }

        /// <summary>
        /// Saving the Message Rules details.
        /// </summary>
        /// <returns></returns>
        public bool SaveMessageRuleDetails()
        {
            string sqlQuery = string.Empty;
            if (this.m_RuleId == -1)
                sqlQuery = string.Format(SQLQueries.Queries.SQ043,
                                          m_Name,
                                          m_TradingPartner,
                                          m_MsgExactMatch,
                                          m_MsgSpecificWordMatch,
                                          m_FileExactMatch,
                                          m_FileSpecificWordMatch,
                                          m_TransactionType,
                                          m_UseMapping,
                                          m_SuccessStatus,
                                          m_ErrorStatus,
                                          m_RuleStatus,
                                          m_SoundChime,
                                          m_SoundAlarm);
            else
                sqlQuery = string.Format(SQLQueries.Queries.SQ044,
                                          m_Name,
                                          m_TradingPartner,
                                          m_MsgExactMatch,
                                          m_MsgSpecificWordMatch,
                                          m_FileExactMatch,
                                          m_FileSpecificWordMatch,
                                          m_TransactionType,
                                          m_UseMapping,
                                          m_SuccessStatus,
                                          m_ErrorStatus,
                                          m_RuleStatus,
                                          m_SoundChime,
                                          m_SoundAlarm,
                                          m_RuleId);
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Get the details of Message rules.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetMessageRulesDetails()
        {
            string commandText = SQLQueries.Queries.SQ045;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Delete the message rule from datagrid.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static int DeleteMessageRuleByID(long ID)
        {
            string sqlQuery = string.Format(Queries.SQ046, ID);
            try
            {
                return MySqlDataAcess.ExecuteNonQuery(sqlQuery);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }
        }

        /// <summary>
        /// Check whether the rule is duplicate through rule name.
        /// </summary>
        /// <param name="ruleName"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static bool IsExistRule(string ruleName, long ID)
        {
            string sqlString = string.Empty;

            sqlString = string.Format(SQLQueries.Queries.SQ047, ruleName, ID);
            try
            {
                int row = Convert.ToInt32(DBConnection.MySqlDataAcess.ExecuteScaler(sqlString));
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Get records from message_rule schema which are active.
        /// </summary>
        /// <returns></returns>
        public static DataSet ActiveMessageRuleDetails()
        {
            string commandText = SQLQueries.Queries.SQ050;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// This matches whether the rule contains correct transcation type and mapping of either QB or MYOB. 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public bool MatchTypeWithMapping(string type, string mapping)
        {
            //Axis 328
            string settingsPath = string.Empty;
            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
            {
                settingsPath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                settingsPath += @"\Settings.xml";
            }
            else
            {
                settingsPath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                settingsPath += @"\Settings.xml";
            }

            #region Commented Code

            //////settingsPath = Application.StartupPath;
            //////settingsPath += @"\Settings.xml";

            //////settingsPath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
            //////System.IO.Directory.CreateDirectory(m_settingsPath);

            ////try
            ////{
            ////    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
            ////    {
            ////        //if (settingsPath.Contains("Program Files (x86)"))
            ////        //{
            ////        //    settingsPath = settingsPath.Replace("Program Files (x86)", Constants.xpPath);
            ////        //}
            ////        //else
            ////        //{
            ////        //    settingsPath = settingsPath.Replace("Program Files", Constants.xpPath);
            ////        //}
            ////        settingsPath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");

            ////    }
            ////    else
            ////    {
            ////        if (settingsPath.Contains("Program Files (x86)"))
            ////        {
            ////            settingsPath = settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
            ////        }
            ////        else
            ////        {
            ////         string llsettingsPath = settingsPath.Replace(string.Empty, "Users\\Public\\Documents");
            ////        }
            ////    }    
            ////}
            ////catch { }
            ////settingsPath += "C:\\Users\\Public\\Documents\\Zed\\Axis\\Settings.xml";
            #endregion
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(settingsPath);
            }
            catch (Exception) { }

            XmlNode root = (XmlNode)xmlDoc.DocumentElement;
            try
            {

                for (int index = 0; index < root.ChildNodes.Count; index++)
                {
                    if (root.ChildNodes.Item(index).Name == "UserMappings")
                    {
                        XmlNodeList name = xmlDoc.GetElementsByTagName("mapping");
                        for (int tempIndex = 0; tempIndex < name.Count; tempIndex++)
                        {
                            if (name[tempIndex].ChildNodes[0].InnerText == mapping)
                            {
                                string mapType = name[tempIndex].ChildNodes[1].InnerText;
                                switch (mapType)
                                {
                                    #region Check QuickBooks
                                    case "QuickBooks": if ((DataProcessingBlocks.ImportType.Bill.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.BillPaymentCheck.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.BillPaymentCreditCard.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.Check.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.CreditCardCharge.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.CreditCardCredit.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.CreditMemo.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.Deposit.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.Estimate.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.InventoryAdjustment.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.Invoice.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.JournalEntry.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.PriceLevel.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.PurchaseOrder.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.ReceivePayment.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.SalesOrder.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.SalesReceipt.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.TimeTracking.ToString().Equals(type)) ||
                                                        (DataProcessingBlocks.ImportType.VendorCredits.ToString().Equals(type)))
                                            return true;
                                        else
                                        {
                                            return false;
                                        }
                                    //bug 482
                                    case "QuickBooks Online":
                                        if ((DataProcessingBlocks.OnlineImportType.Bill.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.BillPayment.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CashPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CheckPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CreditCardPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CreditMemo.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Customer.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Deposit.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Employee.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Invoice.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Items.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.JournalEntry.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Payment.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.PurchaseOrder.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.SalesReceipt.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.TimeActivity.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.VendorCredit.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Estimate.ToString().Equals(type))
                                           )
                                            return true;
                                        else
                                        {
                                            return false;
                                        }

                                    #endregion

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        /// <summary>
        /// This checks whether the software connected to the application is correct or not for transaction type and mapping type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="mapping"></param>
        /// <returns></returns>
        public bool CheckSoftwareType(string type, string mapping, string connected)
        {
            string settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(settingsPath);
            }
            catch (Exception) { }

            XmlNode root = (XmlNode)xmlDoc.DocumentElement;
            //Parsing xml attributes.
            for (int index = 0; index < root.ChildNodes.Count; index++)
            {
                if (root.ChildNodes.Item(index).Name == "UserMappings")
                {
                    XmlNodeList name = xmlDoc.GetElementsByTagName("mapping");
                    for (int tempIndex = 0; tempIndex < name.Count; tempIndex++)
                    {
                        if (name[tempIndex].ChildNodes[0].InnerText == mapping)
                        {
                            string mapType = name[tempIndex].ChildNodes[1].InnerText;

                            switch (mapType)
                            {
                                #region Check QuickBooks
                                case "QuickBooks": if (((DataProcessingBlocks.ImportType.Bill.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.BillPaymentCheck.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.BillPaymentCreditCard.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.Check.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.CreditCardCharge.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.CreditCardCredit.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.CreditMemo.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.Deposit.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.Estimate.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.InventoryAdjustment.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.Invoice.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.JournalEntry.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.PriceLevel.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.PurchaseOrder.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.ReceivePayment.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.SalesOrder.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.SalesReceipt.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.TimeTracking.ToString().Equals(type)) ||
                                                    (DataProcessingBlocks.ImportType.VendorCredits.ToString().Equals(type))) && connected == mapType)
                                        return true;
                                    else
                                    {
                                        CommonUtilities.WriteErrorLog("Rule Failed : Selected Transaction Type is different from the Connected Software.");
                                        return false;
                                    }
                                #endregion
								//Bug 482  
                                case "QuickBooks Online":

                                    if (((DataProcessingBlocks.OnlineImportType.Bill.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.BillPayment.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CashPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CheckPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CreditCardPurchase.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.CreditMemo.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Customer.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Deposit.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Employee.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Invoice.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Items.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.JournalEntry.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Payment.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.PurchaseOrder.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.SalesReceipt.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.TimeActivity.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.VendorCredit.ToString().Equals(type)) ||
                                           (DataProcessingBlocks.OnlineImportType.Estimate.ToString().Equals(type))) && connected == mapType)
                                        return true;
                                    else
                                    {
                                        CommonUtilities.WriteErrorLog("Rule Failed : Selected Transaction Type is different from the Connected Software.");
                                        return false;
                                    }
                            }
                        }
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
