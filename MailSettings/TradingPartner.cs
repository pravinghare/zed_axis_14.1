using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Data;
using System.Data.Odbc;
using DataProcessingBlocks;
using SQLQueries;
using DBConnection;



namespace MailSettings
{
    /// <summary>
    /// Trading Partner class used for save the trading partner details.
    /// </summary>
    public class TradingPartner
    {
        #region Private member

        long m_TradingPartnetID;
        string m_Name;
        int m_MailProtocolID;
        string m_EmailAddress;
        string m_ClientID;
        string m_SchemaLocation;
        string m_AlternateEmail;
        static DataTable m_dtProtocol;
        //These id are used for eBay SandBox Server.
        private const string appID = "ZedSyste-897e-4a18-a573-07714198a5d0";
        private const string devID = "06287722-9b5e-4af2-aa3e-c6d89520b35d";
        private const string certID = "fb608b80-f4d3-411f-b7a4-eb1f703de66d";

        #endregion

        /// <summary>
        ///  Constructor is used to initialise the member variable
        /// </summary>
        /// <param name="name">name of trading partner</param>
        /// <param name="protocol">name of protocol</param>
        /// <param name="email">email adress</param>
        /// <param name="altEmail">alternative email address</param>
        /// <param name="clientID">client Id for trading</param>
        /// <param name="schemaLoacation">location xsd schema file</param>
        public TradingPartner(long Id, string name, string protocol, string email, string altEmail, string clientID, string schemaLoacation)
        {
            this.m_TradingPartnetID = Id;
            this.m_Name = name;
            this.m_MailProtocolID = GetProtocolId(protocol);
            this.m_EmailAddress = email;
            this.m_ClientID = clientID;
            this.m_SchemaLocation = schemaLoacation;
            this.m_AlternateEmail = altEmail;

        }
        /// <summary>
        /// Constructor is used to initiate members
        /// </summary>
        /// <param name="Id">Trading Partner ID</param>
        /// <param name="protocol">Trading partner mail protocol.</param>
        public TradingPartner(long Id, string protocol)
        {
            this.m_TradingPartnetID = Id;
            this.m_MailProtocolID = GetProtocolId(protocol);
        }

        #region Public Methods

        /// <summary>
        ///  This method is used to check the trading partner details.
        /// </summary>
        /// <param name="trdPartner"></param>
        /// <returns></returns>
        public bool Equal(TradingPartner trdPartner)
        {
            if (trdPartner == null)
                return false;
            if (this.m_TradingPartnetID.Equals(trdPartner.m_TradingPartnetID) &&
                this.m_Name.Equals(trdPartner.m_Name) &&
                this.m_MailProtocolID.Equals(trdPartner.m_MailProtocolID) &&
                this.m_EmailAddress.Equals(trdPartner.m_EmailAddress) &&
                this.m_AlternateEmail.Equals(trdPartner.m_AlternateEmail) &&
                this.m_ClientID.Equals(trdPartner.m_ClientID) &&
                this.m_SchemaLocation.Equals(trdPartner.m_SchemaLocation))
                return true;
            else
                return false;

        }

        /// <summary>
        /// Get mail protocol details.
        /// </summary>
        /// <returns>DateTable of Mail Protocol</returns>
        public static DataTable GetMailProtocol()
        {
            string sqlString = SQLQueries.Queries.SQ002;
            try
            {
                DataSet ds = DBConnection.MySqlDataAcess.ExecuteDataset(sqlString);
                m_dtProtocol = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtProtocol;
        }

        /// <summary>
        /// This method is used for getting eBay Trading Partner data.
        /// </summary>
        /// <returns></returns>
        public static DataTable GeteBayTradingPartner()
        {
            string sqlString = SQLQueries.Queries.eBaySQL0006;
            try
            {
                DataSet ds = DBConnection.MySqlDataAcess.ExecuteDataset(sqlString);
                m_dtProtocol = ds.Tables[0];
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return m_dtProtocol;
        }


        /// <summary>
        /// Get mail protocol ID 
        /// </summary>
        /// <param name="mailProtocol">Mail protocol</param>
        /// <returns>Mail Protocol ID</returns>
        public static int GetProtocolId(string mailProtocol)
        {
            int protocolID = 0;
            foreach (DataRow dr in m_dtProtocol.Rows)
            {
                if (dr[MailProtocolColumns.MailProtocol.ToString()].ToString() == mailProtocol)
                {
                    protocolID = Convert.ToInt32(dr[MailProtocolColumns.MailProtocolID.ToString()]);
                    break;
                }
            }
            return protocolID;
        }

        public static string GetMailProtocol(int Id)
        {
            string protocol = string.Empty;
            foreach (DataRow dr in m_dtProtocol.Rows)
            {
                if (Convert.ToInt32(dr[MailProtocolColumns.MailProtocolID.ToString()]) == Id)
                {
                    protocol = dr[MailProtocolColumns.MailProtocolID.ToString()].ToString();
                    break;
                }
            }
            return protocol;
        }
        /// <summary>
        /// Saving the trading partner details
        /// </summary>
        /// <returns>true for successful</returns>
        public bool SaveTradingPartnerDetail()
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.SQ003,
                                                  m_Name,
                                                  m_MailProtocolID,
                                                  m_EmailAddress,
                                                  m_AlternateEmail,
                                                  m_ClientID,
                                                  m_SchemaLocation);
            else
                sqlQuery = string.Format(SQLQueries.Queries.SQ038,
                                               m_Name,
                                               m_MailProtocolID,
                                               m_EmailAddress,
                                               m_AlternateEmail,
                                               m_ClientID,
                                               m_SchemaLocation,
                                               m_TradingPartnetID);
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This method is used for save eBay Trading Partner address book.
        /// </summary>
        /// <param name="tpName"></param>
        /// <param name="mailProtocolID"></param>
        /// <param name="eBayUsername"></param>
        /// <param name="eBayPassword"></param>
        /// <param name="eBayAuthToken"></param>
        /// <param name="eBayItemMapping"></param>
        /// <returns></returns>
        public bool SaveeBayTradingPartner(string tpName, string eBayUsername, string eBayPassword, string eBayAuthToken, string eBayItemMapping, string eBayItemName, string eBayDiscountItem, string lastUpdatedDate, string eBayType)
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.eBaySQL0001,
                                                  tpName,
                                                  m_MailProtocolID,
                                                  eBayUsername,
                                                  eBayPassword,
                                                  eBayAuthToken,
                                                  devID, certID, appID,
                                                  eBayItemMapping, eBayItemName, eBayDiscountItem, eBayType, lastUpdatedDate);
            else
                sqlQuery = string.Format(SQLQueries.Queries.eBaySQL0005,
                                               tpName,
                                               m_MailProtocolID,
                                               eBayUsername,
                                               eBayPassword,
                                               eBayAuthToken,
                                               eBayItemMapping, eBayItemName, eBayDiscountItem, eBayType, lastUpdatedDate,
                                               m_TradingPartnetID.ToString());
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row >= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        public bool SaveZenCartTradingPartner(string tpName, string zenUsername, string zenPassword, string zenWebStore, string PortNo, string dbName, string zencartShippingItem, string ssh_Url, string ssh_PortNo, string ssh_Username, string ssh_Password, string lastUpdatedDate, string db_Prefix)
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.ZenCartSQL001,
                                                    tpName,
                                                    m_MailProtocolID,
                                                    zenUsername,
                                                    zenPassword,
                                                    zenWebStore,
                                                    PortNo,
                                                    dbName,
                                                    zencartShippingItem,
                                                    ssh_Url,
                                                    ssh_PortNo,
                                                    ssh_Username,
                                                    ssh_Password,
                                                    db_Prefix,
                                                    lastUpdatedDate
                                                    );
            else
                sqlQuery = string.Format(SQLQueries.Queries.ZenCartSQL0003,
                                               tpName,
                                               m_MailProtocolID,
                                               zenUsername,
                                               zenPassword,
                                               zenWebStore,
                                               PortNo,
                                               dbName,
                                               zencartShippingItem,
                                               ssh_Url,
                                               ssh_PortNo,
                                               ssh_Username,
                                               ssh_Password,
                                               db_Prefix,
                                               lastUpdatedDate,
                                               m_TradingPartnetID.ToString());
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row >= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This method is used for save OSCommerce Trading Partner.
        /// </summary>
        /// <param name="tpName"></param>
        /// <param name="mailProtocolID"></param>
        /// <param name="oscUsername"></param>
        /// <param name="oscPassword"></param>
        /// <param name="oscWebStore"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public bool SaveOSCommerceTradingPartner(string tpName, string oscUsername, string oscPassword, string oscWebStore, string PortNo, string dbName, string oscShippingItem, string ssh_Url, string ssh_PortNo, string ssh_Username, string ssh_Password, string lastUpdatedDate, string db_Prefix)
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.OSCommerceSQL0001,
                                                  tpName,
                                                  m_MailProtocolID,
                                                  oscUsername,
                                                  oscPassword,
                                                  oscWebStore,
                                                  PortNo,
                                                  dbName,
                                                  oscShippingItem,
                                                  ssh_Url,
                                                  ssh_PortNo,
                                                  ssh_Username,
                                                  ssh_Password,
                                                  db_Prefix,
                                                  lastUpdatedDate);
            else
                sqlQuery = string.Format(SQLQueries.Queries.OSCommerceSQL0003,
                                               tpName,
                                               m_MailProtocolID,
                                               oscUsername,
                                               oscPassword,
                                               oscWebStore,
                                               PortNo,
                                               dbName,
                                               oscShippingItem,
                                               ssh_Url,
                                               ssh_PortNo,
                                               ssh_Username,
                                               ssh_Password,
                                               db_Prefix,
                                               lastUpdatedDate,
                                               m_TradingPartnetID.ToString());
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row >= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This method is used for save Allied Express Trading partner details.
        /// </summary>
        /// <param name="tpName"></param>
        /// <param name="mailProtocolID"></param>
        /// <param name="accountNumber"></param>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public bool SaveAlliedExpressTradingPartner(string tpName, string accountNumber, string accountCode, string AEState, string companyName, string CompanyPhone, string AExpressType, string cannoteNumber)
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.AlliedExpressSQL0001,
                                                  tpName,
                                                  m_MailProtocolID,
                                                  accountNumber,
                                                  accountCode,
                                                  AEState,
                                                  companyName,
                                                  CompanyPhone,
                                                  AExpressType,
                                                  cannoteNumber);
            else
                sqlQuery = string.Format(SQLQueries.Queries.AlliedExpressSQL0003,
                                               tpName,
                                               m_MailProtocolID,
                                               accountNumber,
                                               accountCode,
                                               AEState,
                                               companyName,
                                               CompanyPhone,
                                               AExpressType,
                                               cannoteNumber,
                                               m_TradingPartnetID.ToString());
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// This method is used for update the trading partner details.
        /// </summary>
        /// <param name="tpName">trading partner name</param>
        /// <param name="accountNumber">account number of Allied Express.</param>
        /// <param name="companyName">Company name of Allied Express.</param>
        /// <returns></returns>
        public bool UpdateAlliedExpressTradingPartner(string tpName, string accountNumber, string accountCode, string AEState, string companyName, string companyPhone, string AExpressType, string cannoteNumber)
        {
            string sqlQuery = string.Empty;
            sqlQuery = string.Format(SQLQueries.Queries.AlliedExpressSQL0004,
                                               tpName,
                                               accountNumber,
                                               accountCode,
                                               AEState,
                                               companyName,
                                               companyPhone,
                                               AExpressType,
                                               cannoteNumber,
                                               m_MailProtocolID);
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row >= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Getting the trading partner details
        /// </summary>
        /// <returns>true for successful</returns>
        public static DataSet GetTradingPartnerDetails()
        {
            string commandText = Queries.SQ022;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        public static DataSet GetTradingPartnerAddressDetails()
        {
            string commandText = Queries.SQ022POP3;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }


        /// <summary>
        /// Getting the trading partner details with Mail Protocol.
        /// </summary>
        /// <returns>true for successful</returns>
        public static DataSet GetTradingPartner()
        {
            //string commandText = Queries.SQ040;
            string commandText = Queries.TPSQ040;
            try
            {
                return MySqlDataAcess.ExecuteDataset(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Check whether the record is duplicate through email
        /// </summary>
        /// <param name="email">email address of trading partner</param>
        /// <param name="altEmail">alternative email address</param>
        /// <returns></returns>
        public static bool IsExistEmail(string email, string altEmail, long ID)
        {
            string sqlString = string.Empty;

            if (altEmail.Equals(string.Empty))
                sqlString = string.Format(SQLQueries.Queries.SQ004, email, ID);
            else
                sqlString = string.Format(SQLQueries.Queries.SQ005,
                                            email,
                                            altEmail,
                                            ID);
            try
            {
                int row = Convert.ToInt32(DBConnection.MySqlDataAcess.ExecuteScaler(sqlString));
                if (row > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }
        public static int DeleteTPByID(int Id)
        {
            string commandText = string.Format(Queries.SQ039, Id);
            try
            {
                return MySqlDataAcess.ExecuteNonQuery(commandText);
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return 0;
            }

        }




        /// <summary>
        /// This method is used for save X-Cart Trading Partner.
        /// </summary>
        /// <param name="tpName"></param>
        /// <param name="mailProtocolID"></param>
        /// <param name="oscUsername"></param>
        /// <param name="oscPassword"></param>
        /// <param name="oscWebStore"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public bool SaveXCartTradingPartner(string tpName, string oscUsername, string oscPassword, string oscWebStore, string PortNo, string dbName, string xcartShippingItem, string ssh_Url, string ssh_PortNo, string ssh_Username, string ssh_Password, string dbPrefix, string lastUpdatedDate)
        {
            string sqlQuery = string.Empty;
            if (this.m_TradingPartnetID == -1)
                sqlQuery = string.Format(SQLQueries.Queries.XCartSQL0001,
                                                   tpName,
                                                   m_MailProtocolID,
                                                   oscUsername,
                                                   oscPassword,
                                                   oscWebStore,
                                                   PortNo,
                                                   dbName,
                                                   xcartShippingItem,
                                                   ssh_Url,
                                                   ssh_PortNo,
                                                   ssh_Username,
                                                   ssh_Password,
                                                   dbPrefix,
                                                   lastUpdatedDate
                                                   );
            else
                sqlQuery = string.Format(SQLQueries.Queries.XCartSQL0003,
                                               tpName,
                                               m_MailProtocolID,
                                               oscUsername,
                                               oscPassword,
                                               oscWebStore,
                                               PortNo,
                                               dbName,
                                               xcartShippingItem,
                                               ssh_Url,
                                               ssh_PortNo,
                                               ssh_Username,
                                               ssh_Password,
                                               dbPrefix,
                                               lastUpdatedDate,
                                               m_TradingPartnetID.ToString());
            try
            {
                int row = DBConnection.MySqlDataAcess.ExecuteNonQuery(sqlQuery);
                if (row >= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
                //DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.StackTrace);
                return false;
            }
        }




        #endregion
    }

   
}
