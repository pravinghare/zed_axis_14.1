﻿namespace DataProcessingBlocks
{
    partial class RatingAndReview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RatingAndReview));
            this.radRating1 = new Telerik.WinControls.UI.RadRating();
            this.ratingStarVisualElement1 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement2 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement3 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement4 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement5 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement6 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement7 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement8 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement9 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.ratingStarVisualElement10 = new Telerik.WinControls.UI.RatingStarVisualElement();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFeedback = new System.Windows.Forms.Button();
            this.btnDismiss = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.radRating1)).BeginInit();
            this.SuspendLayout();
            // 
            // radRating1
            // 
            this.radRating1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ratingStarVisualElement1,
            this.ratingStarVisualElement2,
            this.ratingStarVisualElement3,
            this.ratingStarVisualElement4,
            this.ratingStarVisualElement5,
            this.ratingStarVisualElement6,
            this.ratingStarVisualElement7,
            this.ratingStarVisualElement8,
            this.ratingStarVisualElement9,
            this.ratingStarVisualElement10});
            this.radRating1.Location = new System.Drawing.Point(117, 76);
            this.radRating1.Margin = new System.Windows.Forms.Padding(4);
            this.radRating1.Maximum = 10D;
            this.radRating1.Minimum = 1D;
            this.radRating1.Name = "radRating1";
            this.radRating1.Size = new System.Drawing.Size(427, 44);
            this.radRating1.TabIndex = 24;
            this.radRating1.Text = "radRating1";
            this.radRating1.Click += new System.EventHandler(this.radRating1_Click);
            // 
            // ratingStarVisualElement1
            // 
            this.ratingStarVisualElement1.Name = "ratingStarVisualElement1";
            // 
            // ratingStarVisualElement2
            // 
            this.ratingStarVisualElement2.Name = "ratingStarVisualElement2";
            // 
            // ratingStarVisualElement3
            // 
            this.ratingStarVisualElement3.Name = "ratingStarVisualElement3";
            // 
            // ratingStarVisualElement4
            // 
            this.ratingStarVisualElement4.Name = "ratingStarVisualElement4";
            // 
            // ratingStarVisualElement5
            // 
            this.ratingStarVisualElement5.Name = "ratingStarVisualElement5";
            // 
            // ratingStarVisualElement6
            // 
            this.ratingStarVisualElement6.Name = "ratingStarVisualElement6";
            this.ratingStarVisualElement6.Text = "ratingStarVisualElement6";
            // 
            // ratingStarVisualElement7
            // 
            this.ratingStarVisualElement7.Name = "ratingStarVisualElement7";
            this.ratingStarVisualElement7.Text = "ratingStarVisualElement7";
            // 
            // ratingStarVisualElement8
            // 
            this.ratingStarVisualElement8.Name = "ratingStarVisualElement8";
            this.ratingStarVisualElement8.Text = "ratingStarVisualElement8";
            // 
            // ratingStarVisualElement9
            // 
            this.ratingStarVisualElement9.Name = "ratingStarVisualElement9";
            this.ratingStarVisualElement9.Text = "ratingStarVisualElement9";
            // 
            // ratingStarVisualElement10
            // 
            this.ratingStarVisualElement10.Name = "ratingStarVisualElement10";
            this.ratingStarVisualElement10.Text = "ratingStarVisualElement10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(721, 36);
            this.label1.TabIndex = 25;
            this.label1.Text = "\"Thanks for using Zed Axis. To help us improve we would be very grateful if you g" +
    "ive us a minute of your time.\r\n How likely would you be to recommend using Zed A" +
    "xis to a friend or colleague?";
            // 
            // btnFeedback
            // 
            this.btnFeedback.Location = new System.Drawing.Point(190, 150);
            this.btnFeedback.Name = "btnFeedback";
            this.btnFeedback.Size = new System.Drawing.Size(137, 31);
            this.btnFeedback.TabIndex = 26;
            this.btnFeedback.Text = "Send Feedback";
            this.btnFeedback.UseVisualStyleBackColor = true;
            this.btnFeedback.Click += new System.EventHandler(this.btnFeedback_Click);
            // 
            // btnDismiss
            // 
            this.btnDismiss.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDismiss.Location = new System.Drawing.Point(364, 150);
            this.btnDismiss.Name = "btnDismiss";
            this.btnDismiss.Size = new System.Drawing.Size(102, 31);
            this.btnDismiss.TabIndex = 27;
            this.btnDismiss.Text = "Dismiss";
            this.btnDismiss.UseVisualStyleBackColor = true;
            this.btnDismiss.Click += new System.EventHandler(this.btnDismiss_Click);
            // 
            // RatingAndReview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 184);
            this.Controls.Add(this.btnDismiss);
            this.Controls.Add(this.btnFeedback);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radRating1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RatingAndReview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rate us";
            ((System.ComponentModel.ISupportInitialize)(this.radRating1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //588
        public Telerik.WinControls.UI.RadRating radRating1;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement1;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement2;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement3;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement4;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement5;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement6;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement7;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement8;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement9;
        private Telerik.WinControls.UI.RatingStarVisualElement ratingStarVisualElement10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFeedback;
        private System.Windows.Forms.Button btnDismiss;

    }
}