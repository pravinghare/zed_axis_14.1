﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;


namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlinePurchaseOrderEntry
    {
        private static ImportOnlinePurchaseOrderEntry m_ImportOnlinePurchaseOrderEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlinePurchaseOrderEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import PO class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlinePurchaseOrderEntry GetInstance()
        {
            if (m_ImportOnlinePurchaseOrderEntry == null)
                m_ImportOnlinePurchaseOrderEntry = new ImportOnlinePurchaseOrderEntry();
            return m_ImportOnlinePurchaseOrderEntry;
        }
        /// setting values to PO data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlinePurchaseOrderQBEntryCollection ImportOnlinePurchaseOrderData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlinePurchaseOrderQBEntryCollection coll = new OnlinePurchaseOrderQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                   
                    string datevalue = string.Empty;
                    DateTime PODate = new DateTime();
                    //Employee Validation
                    OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry purchaseOrder = new OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry();
                    if (dt.Columns.Contains("DocNumber"))
                    {
                        purchaseOrder = coll.FindPurchaseOrderNumberEntry(dr["DocNumber"].ToString());

                        if (purchaseOrder == null)
                        {
                            #region if purchaseOrder is null

                            purchaseOrder = new OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry();

                            if (dt.Columns.Contains("DocNumber"))
                            {
                                #region Validations of docnumber
                                if (dr["DocNumber"].ToString() != string.Empty)
                                {
                                    if (dr["DocNumber"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DocNumber (" + dr["DocNumber"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.DocNumber = dr["DocNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.DocNumber = dr["DocNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.DocNumber = dr["DocNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.DocNumber = dr["DocNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validations of PrivateNote
                                DateTime SODate = new DateTime();
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    purchaseOrder.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    purchaseOrder.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                purchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            purchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }
                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        purchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrivateNote"))
                            {
                                #region Validations of PrivateNote
                                if (dr["PrivateNote"].ToString() != string.Empty)
                                {
                                    if (dr["PrivateNote"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            #region Custom1
                            OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName1"))
                            {
                                #region Validations of Custom1
                                if (dr["CustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Name = dr["CustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue1"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue1"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField.Value = dr["CustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField.Value != null || CustomField.Name != null)

                                purchaseOrder.CustomField.Add(CustomField);

                            #endregion

                            #region Custom2
                            OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Name = dr["CustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue2"))
                            {
                                #region Validations of Custom2
                                if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue2"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField2.Value != null || CustomField2.Name != null)

                                purchaseOrder.CustomField.Add(CustomField2);

                            #endregion

                            #region Custom3
                            OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                            if (dt.Columns.Contains("CustomFieldName3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Name = dr["CustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue3"))
                            {
                                #region Validations of Custom3
                                if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue3"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CustomField3.Value != null || CustomField3.Name != null)
                                purchaseOrder.CustomField.Add(CustomField3);

                            #endregion


                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice= dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name!= null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }
                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 ||  ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0 )
                            {
                                Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                            }

                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();

                            if (dt.Columns.Contains("LineAccountDescription"))
                            {
                                #region Validations of LineAccountDescription
                                if (dr["LineAccountDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountDescription"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAccountAmount"))
                            {
                                #region Validations for LineAccountAmount
                                if (dr["LineAccountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                            if (dt.Columns.Contains("LineAccountCustomerRef"))
                            {
                                #region Validations of LineAccountCustomerRef
                                if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                            }

                            OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                            if (dt.Columns.Contains("LineAccountClassRef"))
                            {
                                #region Validations of LineAccountClassRef
                                if (dr["LineAccountClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }
                           
                            if (dt.Columns.Contains("LineAccountBillableStatus"))
                            {
                                #region Validations of LineAccountBillableStatus
                                if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TaxCodeRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                            }

                            if(AccountBasedExpenseLineDetail.AccountRef.Count!=0||AccountBasedExpenseLineDetail.ClassRef.Count!=0||AccountBasedExpenseLineDetail.customerRef.Count!=0||AccountBasedExpenseLineDetail.BillableStatus!=null||AccountBasedExpenseLineDetail.Description!=null||AccountBasedExpenseLineDetail.AccountAmount!=null)
                                AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);

                          
                            if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                                purchaseOrder.Line.Add(Line);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                purchaseOrder.Line.Add(Line);
                            }
                            else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                            {                                
                                purchaseOrder.Line.Add(Line);                                
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                            }

                            #region TxnTaxDetail
                            OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                            OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                            OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                            OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                            if (dt.Columns.Contains("TxnTaxCodeRefName"))
                            {
                                #region Validations of TxnTaxCodeRefName
                                if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                                {
                                    if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TotalTax"))
                            {
                                #region Validations for TotalTax
                                if (dr["TotalTax"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TotalTax")
                                            {
                                                isIgnoreAll = true;
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                            }
                                            else
                                            {
                                                TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxLineAmount"))
                            {
                                #region Validations for TaxLineAmount
                                if (dr["TaxLineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TaxLineAmount")
                                            {
                                                isIgnoreAll = true;
                                                TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TaxPercentBased"))
                            {
                                #region Validations of IsActive
                                if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                    {
                                        TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "TaxPercentBased";
                                            }
                                            else
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TaxPercent"))
                            {
                                #region Validations for TaxPercent
                                if (dr["TaxPercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {

                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                            if (Convert.ToString(result) == "TaxPercent")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count > 0)
                            {
                                TaxLine.TaxLineDetail.Add(TaxLineDetail);
                            }
                            if (TxnTaxCodeRef.Name != null)
                            {
                                TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                            }
                            if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                            {
                                TxnTaxDetail.TaxLine.Add(TaxLine);
                            }
                            if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                            {
                                purchaseOrder.TxnTaxDetail.Add(TxnTaxDetail);
                            }
                          
                            #endregion

                            OnlineEntities.VendorRef VendorRef = new OnlineEntities.VendorRef();
                            if (dt.Columns.Contains("VendorRef"))
                            {
                                #region Validations of VendorRef
                                if (dr["VendorRef"].ToString() != string.Empty)
                                {
                                    if (dr["VendorRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorRef (" + dr["VendorRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorRef.Name = dr["VendorRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorRef.Name = dr["VendorRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorRef.Name = dr["VendorRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorRef.Name = dr["VendorRef"].ToString();
                                    }
                                }
                                #endregion
                            } 

                            if (VendorRef.Name != null)
                            {
                                purchaseOrder.VendorRef.Add(VendorRef);
                            }

                            //P Axis 13.1 : issue 687
                            if (dt.Columns.Contains("POEmail"))
                            {
                                #region Validations of POEmail
                                if (dr["POEmail"].ToString() != string.Empty)
                                {
                                    if (dr["POEmail"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This POEmail (" + dr["POEmail"].ToString() + ") is not valid. If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.POEmail = dr["POEmail"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.POEmail = dr["POEmail"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.POEmail = dr["POEmail"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.POEmail = dr["POEmail"].ToString();
                                    }
                                }
                                #endregion
                            }
                       
                            //P Axis 13.1 : issue 687 END

                            OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                            if (dt.Columns.Contains("APAccountRef"))
                            {
                                #region Validations of APAccountRef
                                if (dr["APAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                APAccountRef.Name = dr["APAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            APAccountRef.Name = dr["APAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (APAccountRef.Name != null)
                            {
                                purchaseOrder.APAccountRef.Add(APAccountRef);
                            }
                           
                            OnlineEntities.ClassRef ClassRef2 = new OnlineEntities.ClassRef();

                            if (dt.Columns.Contains("ClassRef"))
                            {
                                #region Validations of ClassRef
                                if (dr["ClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ClassRef (" + dr["ClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef2.Name = dr["ClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef2.Name = dr["ClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef2.Name = dr["ClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef2.Name = dr["ClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef2.Name != null)
                            {
                                purchaseOrder.ClassRef.Add(ClassRef2);
                            }

                            OnlineEntities.SalesTermRef SalesTermRef = new OnlineEntities.SalesTermRef();

                            if (dt.Columns.Contains("SalesTermRef"))
                            {
                                #region Validations of SalesTermRef
                                if (dr["SalesTermRef"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTermRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTermRef (" + dr["SalesTermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                SalesTermRef.Name = dr["SalesTermRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (SalesTermRef.Name != null)
                            {
                                purchaseOrder.SalesTermRef.Add(SalesTermRef);
                            }

                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region Validations of DueDate
                                DateTime SODate = new DateTime();
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out SODate))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    purchaseOrder.DueDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    purchaseOrder.DueDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                purchaseOrder.DueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            SODate = dttest;
                                            purchaseOrder.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }
                                    }
                                    else
                                    {
                                        SODate = Convert.ToDateTime(datevalue);
                                        purchaseOrder.DueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            //adding Bill Address 
                            
                            OnlineEntities.VendorAddr VendorAddr = new OnlineEntities.VendorAddr();
                            if (dt.Columns.Contains("VendorAddrLine1"))
                            {
                                #region Validations of VendorAddrLine1
                                if (dr["VendorAddrLine1"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLine1"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLine1 (" + dr["VendorAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrLine2"))
                            {
                                #region Validations of VendorAddrLine2
                                if (dr["VendorAddrLine2"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLine2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLine2 (" + dr["VendorAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrLine3"))
                            {
                                #region Validations of VendorAddrLine3
                                if (dr["VendorAddrLine3"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLine3"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLine3 (" + dr["VendorAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrCity"))
                            {
                                #region Validations of VendorAddrCity
                                if (dr["VendorAddrCity"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrCity"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrCity (" + dr["VendorAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorCity = dr["VendorAddrCity"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrCountry"))
                            {
                                #region Validations of VendorAddrCountry
                                if (dr["VendorAddrCountry"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrCountry"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrCountry (" + dr["VendorAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrSubDivisionCode"))
                            {
                                #region Validations of VendorAddrSubDivisionCode
                                if (dr["VendorAddrSubDivisionCode"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrSubDivisionCode"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrSubDivisionCode (" + dr["VendorAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrPostalCode"))
                            {
                                #region Validations of VendorAddrPostalCode
                                if (dr["VendorAddrPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrPostalCode"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrPostalCode (" + dr["VendorAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrNote"))
                            {
                                #region Validations of VendorAddrNote
                                if (dr["VendorAddrNote"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrNote"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrNote (" + dr["VendorAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorNote = dr["VendorAddrNote"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrLine4"))
                            {
                                #region Validations of VendorAddrLine4
                                if (dr["VendorAddrLine4"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLine4"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLine4 (" + dr["VendorAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrLine5"))
                            {
                                #region Validations of VendorAddrLine5
                                if (dr["VendorAddrLine5"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLine5"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLine5 (" + dr["VendorAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("VendorAddrLat"))
                            {
                                #region Validations of VendorAddrLat
                                if (dr["VendorAddrLat"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLat"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLat (" + dr["VendorAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("VendorAddrLong"))
                            {
                                #region Validations of VendorAddrLong
                                if (dr["VendorAddrLong"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddrLong"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorAddrLong (" + dr["VendorAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (VendorAddr.VendorLine1 != null || VendorAddr.VendorLine2 != null || VendorAddr.VendorLine3 != null || VendorAddr.VendorLine4 != null || VendorAddr.VendorLine5 != null || VendorAddr.VendorCity != null || VendorAddr.VendorCountry != null || VendorAddr.VendorCountrySubDivisionCode != null || VendorAddr.VendorNote != null || VendorAddr.VendorAddrLat != null || VendorAddr.VendorAddrLong != null || VendorAddr.VendorPostalCode != null)
                                purchaseOrder.VendorAddr.Add(VendorAddr);

                            OnlineEntities.ShipAddr shipAddr = new OnlineEntities.ShipAddr();
                            if (dt.Columns.Contains("ShipAddrLine1"))
                            {
                                #region Validations of ShipAddrLine1
                                if (dr["ShipAddrLine1"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine1"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine1 (" + dr["ShipAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine2"))
                            {
                                #region Validations of ShipAddrLine2
                                if (dr["ShipAddrLine2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine2"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine2 (" + dr["ShipAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine3"))
                            {
                                #region Validations of ShipAddrLine3
                                if (dr["ShipAddrLine3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine3"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine3 (" + dr["ShipAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrCity"))
                            {
                                #region Validations of ShipAddrCity
                                if (dr["ShipAddrCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrCity"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrCity (" + dr["ShipAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCity = dr["ShipAddrCity"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrCountry"))
                            {
                                #region Validations of ShipAddrCountry
                                if (dr["ShipAddrCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrCountry"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrCountry (" + dr["ShipAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrSubDivisionCode"))
                            {
                                #region Validations of ShipAddrSubDivisionCode
                                if (dr["ShipAddrSubDivisionCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrSubDivisionCode"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrSubDivisionCode (" + dr["ShipAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString().Substring(0, 255);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrPostalCode"))
                            {
                                #region Validations of ShipAddrPostalCode
                                if (dr["ShipAddrPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrPostalCode"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrPostalCode (" + dr["ShipAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrNote"))
                            {
                                #region Validations of ShipAddrNote
                                if (dr["ShipAddrNote"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrNote"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrNote (" + dr["ShipAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipNote = dr["ShipAddrNote"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine4"))
                            {
                                #region Validations of ShipAddrLine4
                                if (dr["ShipAddrLine4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine4"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine4 (" + dr["ShipAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLine5"))
                            {
                                #region Validations of ShipAddrLine5
                                if (dr["ShipAddrLine5"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLine5"].ToString().Length > 500)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLine5 (" + dr["ShipAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString().Substring(0, 500);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ShipAddrLat"))
                            {
                                #region Validations of ShipAddrLat
                                if (dr["ShipAddrLat"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLat"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLat (" + dr["ShipAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ShipAddrLong"))
                            {
                                #region Validations of ShipAddrLong
                                if (dr["ShipAddrLong"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddrLong"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipAddrLong (" + dr["ShipAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString().Substring(0, 1000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (shipAddr.ShipLine1 != null || shipAddr.ShipLine2 != null || shipAddr.ShipLine3 != null || shipAddr.ShipLine4 != null || shipAddr.ShipLine5 != null || shipAddr.ShipCity != null || shipAddr.ShipCountry != null || shipAddr.ShipCountrySubDivisionCode != null || shipAddr.ShipNote != null || shipAddr.ShipAddrLat != null || shipAddr.ShipAddrLong != null || shipAddr.ShipPostalCode != null)
                                purchaseOrder.ShipAddr.Add(shipAddr);
                           
                            ShipMethodRef ShipMethodRef = new ShipMethodRef();

                            if (dt.Columns.Contains("ShipMethodRef"))
                            {
                                #region Validations of ShipMethodRef
                                if (dr["ShipMethodRef"].ToString() != string.Empty)
                                {
                                    if (dr["ShipMethodRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipMethodRef (" + dr["ShipMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipMethodRef.Name = dr["ShipMethodRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ShipMethodRef.Name != null)
                            {
                                purchaseOrder.ShipMethodRef.Add(ShipMethodRef);
                            }

                            if (dt.Columns.Contains("POStatus"))
                            {
                                #region Validations of POStatus
                                if (dr["POStatus"].ToString() != string.Empty)
                                {
                                    if (dr["POStatus"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Status (" + dr["POStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.POStatus = dr["POStatus"].ToString().Substring(0, 15);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.POStatus = dr["POStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.POStatus = dr["POStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.POStatus = dr["POStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            CurrencyRef CurrencyRef = new CurrencyRef();

                            if (dt.Columns.Contains("CurrencyRef"))
                            {
                                #region Validations of CurrencyRef
                                if (dr["CurrencyRef"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRef"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (CurrencyRef.Name != null)
                            {
                                purchaseOrder.CurrencyRef.Add(CurrencyRef);
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "ExchangeRate")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("GlobalTaxCalculation"))
                            {
                                #region Validations of GlobalTaxCalculation
                                if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                                {
                                    if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                    }
                                }
                                #endregion
                            }
                            #endregion

                            coll.Add(purchaseOrder);
                        }
                        else
                        {
                            #region if purchaseOrder is not null

                            OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                            OnlineEntities.Line Line = new OnlineEntities.Line();
                            OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                            OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                            OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                            OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineDescription"))
                            {
                                #region Validations of Associate
                                if (dr["LineDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineDescription"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineDescription = dr["LineDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LineAmount"))
                            {
                                #region Validations for LineAmount
                                if (dr["LineAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Line.LineAmount = dr["LineAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemRef"))
                            {
                                #region Validations of LineItemRef
                                if (dr["LineItemRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.Name = dr["LineItemRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //bug 486
                            if (dt.Columns.Contains("SKU"))
                            {
                                #region Validations of SKU
                                if (dr["SKU"].ToString() != string.Empty)
                                {
                                    if (dr["SKU"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemRef.SKU = dr["SKU"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineClassRef"))
                            {
                                #region Validations of LineClassRef
                                if (dr["LineClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineClassRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef.Name = dr["LineClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemQty"))
                            {
                                #region Validations for LineItemQty
                                if (dr["LineItemQty"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("LineItemUnitPrice"))
                            {
                                #region Validations for LineItemUnitPrice
                                if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LineItemTaxCodeRef"))
                            {
                                #region Validations of LineItemTaxCodeRef
                                if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("LineCustomerRef"))
                            {
                                #region Validations of LineCustomerRef
                                if (dr["LineCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineBillableStatus"))
                            {
                                #region Validations of LineBillableStatus
                                if (dr["LineBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineBillableStatus"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ItemRef.Name != null || ItemRef.SKU != null)
                            {
                                ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                            }
                            if (TaxCodeRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                            }
                            if (ClassRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                            }
                            if (CustomerRef.Name != null)
                            {
                                ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                            }
                            if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                            {
                                Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                            }

                            OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                            OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();
                            if (dt.Columns.Contains("LineAccountDescription"))
                            {
                                #region Validations of LineAccountDescription
                                if (dr["LineAccountDescription"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountDescription"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LineAccountAmount"))
                            {
                                #region Validations for LineAccountAmount
                                if (dr["LineAccountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                            if (dt.Columns.Contains("LineAccountCustomerRef"))
                            {
                                #region Validations of LineAccountCustomerRef
                                if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (CustomerRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                            }

                            OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                            if (dt.Columns.Contains("LineAccountClassRef"))
                            {
                                #region Validations of LineAccountClassRef
                                if (dr["LineAccountClassRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountClassRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (ClassRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                            }

                            OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                            if (dt.Columns.Contains("LineAccountRef"))
                            {
                                #region Validations of LineAccountRef
                                if (dr["LineAccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountRef.Name = dr["LineAccountRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (AccountRef.Name != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }
                            
                            if (AccountRef.AcctNum != null)
                            {
                                AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                            }

                            if (dt.Columns.Contains("LineAccountBillableStatus"))
                            {
                                #region Validations of LineAccountBillableStatus
                                if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                            if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                            {
                                #region Validations of LineAccountTaxCodeRef
                                if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                                {
                                    if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (TaxCodeRef1.Name != null)
                            {
                                AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                            }

                            if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                                AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);
                         

                            if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                                purchaseOrder.Line.Add(Line);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                            {
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                            }
                            else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                            {
                                purchaseOrder.Line.Add(Line);
                            }
                            else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                            {
                                purchaseOrder.Line.Add(Line);
                                purchaseOrder.Line.Add(AccountBasedExpenseLine);
                            }                          
                            #endregion
                        }
                    }
                    else
                    {
                        purchaseOrder = new OnlinePurchaseOrderEntry();

                        #region if purchaseOrder is null

                        purchaseOrder = new OnlineDataProcessingsImportClass.OnlinePurchaseOrderEntry();                       

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validations of PrivateNote
                            DateTime SODate = new DateTime();
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        purchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }
                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    purchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PrivateNote"))
                        {
                            #region Validations of PrivateNote
                            if (dr["PrivateNote"].ToString() != string.Empty)
                            {
                                if (dr["PrivateNote"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PrivateNote (" + dr["PrivateNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            purchaseOrder.PrivateNote = dr["PrivateNote"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                    }
                                }
                                else
                                {
                                    purchaseOrder.PrivateNote = dr["PrivateNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        #region Custom1
                        OnlineEntities.CustomField CustomField = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName1"))
                        {
                            #region Validations of Custom1
                            if (dr["CustomFieldName1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName1 (" + dr["CustomFieldName1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Name = dr["CustomFieldName1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Name = dr["CustomFieldName1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Name = dr["CustomFieldName1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Name = dr["CustomFieldName1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue1"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue1"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue1 (" + dr["CustomFieldValue1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField.Value = dr["CustomFieldValue1"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField.Value = dr["CustomFieldValue1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField.Value = dr["CustomFieldValue1"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField.Value = dr["CustomFieldValue1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField.Value != null || CustomField.Name != null)

                            purchaseOrder.CustomField.Add(CustomField);

                        #endregion

                        #region Custom2
                        OnlineEntities.CustomField CustomField2 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldName2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName2 (" + dr["CustomFieldName2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Name = dr["CustomFieldName2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Name = dr["CustomFieldName2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Name = dr["CustomFieldName2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Name = dr["CustomFieldName2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue2"))
                        {
                            #region Validations of Custom2
                            if (dr["CustomFieldValue2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue2"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue2 (" + dr["CustomFieldValue2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField2.Value = dr["CustomFieldValue2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField2.Value != null || CustomField2.Name != null)

                            purchaseOrder.CustomField.Add(CustomField2);

                        #endregion

                        #region Custom3
                        OnlineEntities.CustomField CustomField3 = new OnlineEntities.CustomField();

                        if (dt.Columns.Contains("CustomFieldName3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldName3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName3 (" + dr["CustomFieldName3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Name = dr["CustomFieldName3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Name = dr["CustomFieldName3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Name = dr["CustomFieldName3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Name = dr["CustomFieldName3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue3"))
                        {
                            #region Validations of Custom3
                            if (dr["CustomFieldValue3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue3"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue3 (" + dr["CustomFieldValue3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomField3.Value = dr["CustomFieldValue3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CustomField3.Value != null || CustomField3.Name != null)
                            purchaseOrder.CustomField.Add(CustomField3);

                        #endregion

                        OnlineEntities.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new OnlineEntities.ItemBasedExpenseLineDetail();
                        OnlineEntities.Line Line = new OnlineEntities.Line();
                        OnlineEntities.ItemRef ItemRef = new OnlineEntities.ItemRef();
                        OnlineEntities.ClassRef ClassRef = new OnlineEntities.ClassRef();
                        OnlineEntities.CustomerRef CustomerRef = new OnlineEntities.CustomerRef();
                        OnlineEntities.TaxCodeRef TaxCodeRef = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineDescription"))
                        {
                            #region Validations of Associate
                            if (dr["LineDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineDescription"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineDescription (" + dr["LineDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineDescription = dr["LineDescription"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineDescription = dr["LineDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineDescription = dr["LineDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineDescription = dr["LineDescription"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineAmount"))
                        {
                            #region Validations for LineAmount
                            if (dr["LineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmount( " + dr["LineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Line.LineAmount = dr["LineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Line.LineAmount = dr["LineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    Line.LineAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemRef"))
                        {
                            #region Validations of LineItemRef
                            if (dr["LineItemRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemRef (" + dr["LineItemRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.Name = dr["LineItemRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.Name = dr["LineItemRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.Name = dr["LineItemRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.Name = dr["LineItemRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        //bug 486
                        if (dt.Columns.Contains("SKU"))
                        {
                            #region Validations of SKU
                            if (dr["SKU"].ToString() != string.Empty)
                            {
                                if (dr["SKU"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SKU (" + dr["SKU"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemRef.SKU = dr["SKU"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemRef.SKU = dr["SKU"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemRef.SKU = dr["SKU"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemRef.SKU = dr["SKU"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineClassRef"))
                        {
                            #region Validations of LineClassRef
                            if (dr["LineClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineClassRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineClassRef (" + dr["LineClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef.Name = dr["LineClassRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef.Name = dr["LineClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef.Name = dr["LineClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef.Name = dr["LineClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemQty"))
                        {
                            #region Validations for LineItemQty
                            if (dr["LineItemQty"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemQty"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemQty( " + dr["LineItemQty"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.Qty = dr["LineItemQty"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.Qty = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemQty"].ToString()));
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemUnitPrice"))
                        {
                            #region Validations for LineItemUnitPrice
                            if (dr["LineItemUnitPrice"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineItemUnitPrice"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemUnitPrice( " + dr["LineItemUnitPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.UnitPrice = dr["LineItemUnitPrice"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineItemUnitPrice"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemTaxCodeRef"))
                        {
                            #region Validations of LineItemTaxCodeRef
                            if (dr["LineItemTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineItemTaxCodeRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemTaxCodeRef (" + dr["LineItemTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef.Name = dr["LineItemTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("LineCustomerRef"))
                        {
                            #region Validations of LineCustomerRef
                            if (dr["LineCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineCustomerRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineCustomerRef (" + dr["LineCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["LineCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineBillableStatus"))
                        {
                            #region Validations of LineBillableStatus
                            if (dr["LineBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineBillableStatus"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineBillableStatus (" + dr["LineBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = dr["LineBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ItemRef.Name != null || ItemRef.SKU != null)
                        {
                            ItemBasedExpenseLineDetail.ItemRef.Add(ItemRef);
                        }
                        if (TaxCodeRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef);
                        }
                        if (ClassRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.ClassRef.Add(ClassRef);
                        }
                        if (CustomerRef.Name != null)
                        {
                            ItemBasedExpenseLineDetail.CustomerRef.Add(CustomerRef);
                        }
                        if (ItemBasedExpenseLineDetail.ItemRef.Count != 0 || ItemBasedExpenseLineDetail.Qty != null || ItemBasedExpenseLineDetail.UnitPrice != null || ItemBasedExpenseLineDetail.CustomerRef.Count != 0 || ItemBasedExpenseLineDetail.ClassRef.Count != 0 || ItemBasedExpenseLineDetail.TaxCodeRef.Count != 0)
                        {
                            Line.ItemBasedExpenseLineDetail.Add(ItemBasedExpenseLineDetail);
                        }

                        OnlineEntities.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new OnlineEntities.AccountBasedExpenseLineDetail();
                        OnlineEntities.Line AccountBasedExpenseLine = new OnlineEntities.Line();

                        if (dt.Columns.Contains("LineAccountDescription"))
                        {
                            #region Validations of LineAccountDescription
                            if (dr["LineAccountDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountDescription"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountDescription (" + dr["LineAccountDescription"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.Description = dr["LineAccountDescription"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineAccountAmount"))
                        {
                            #region Validations for LineAccountAmount
                            if (dr["LineAccountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["LineAccountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountAmount( " + dr["LineAccountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.AccountAmount = dr["LineAccountAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.AccountAmount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["LineAccountAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        OnlineEntities.CustomerRef CustomerRef1 = new OnlineEntities.CustomerRef();

                        if (dt.Columns.Contains("LineAccountCustomerRef"))
                        {
                            #region Validations of LineAccountCustomerRef
                            if (dr["LineAccountCustomerRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountCustomerRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountCustomerRef (" + dr["LineAccountCustomerRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef1.Name = dr["LineAccountCustomerRef"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (CustomerRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.customerRef.Add(CustomerRef1);
                        }

                        OnlineEntities.ClassRef ClassRef1 = new OnlineEntities.ClassRef();

                        if (dt.Columns.Contains("LineAccountClassRef"))
                        {
                            #region Validations of LineAccountClassRef
                            if (dr["LineAccountClassRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountClassRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountClassRef (" + dr["LineAccountClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef1.Name = dr["LineAccountClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ClassRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.ClassRef.Add(ClassRef1);
                        }

                        OnlineEntities.AccountRef AccountRef = new OnlineEntities.AccountRef();

                        if (dt.Columns.Contains("LineAccountRef"))
                        {
                            #region Validations of LineAccountRef
                            if (dr["LineAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountRef (" + dr["LineAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountRef.Name = dr["LineAccountRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountRef.Name = dr["LineAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountRef.Name = dr["LineAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountRef.Name = dr["LineAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (AccountRef.Name != null)
                        {
                            AccountBasedExpenseLineDetail.AccountRef.Add(AccountRef);
                        }
                       
                        if (dt.Columns.Contains("LineAccountBillableStatus"))
                        {
                            #region Validations of LineAccountBillableStatus
                            if (dr["LineAccountBillableStatus"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountBillableStatus"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountBillableStatus (" + dr["LineAccountBillableStatus"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = dr["LineAccountBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        OnlineEntities.TaxCodeRef TaxCodeRef1 = new OnlineEntities.TaxCodeRef();

                        if (dt.Columns.Contains("LineAccountTaxCodeRef"))
                        {
                            #region Validations of LineAccountTaxCodeRef
                            if (dr["LineAccountTaxCodeRef"].ToString() != string.Empty)
                            {
                                if (dr["LineAccountTaxCodeRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAccountTaxCodeRef (" + dr["LineAccountTaxCodeRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxCodeRef1.Name = dr["LineAccountTaxCodeRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (TaxCodeRef1.Name != null)
                        {
                            AccountBasedExpenseLineDetail.TaxCodeRef.Add(TaxCodeRef1);
                        }

                        if (AccountBasedExpenseLineDetail.AccountRef.Count != 0 || AccountBasedExpenseLineDetail.ClassRef.Count != 0 || AccountBasedExpenseLineDetail.customerRef.Count != 0 || AccountBasedExpenseLineDetail.BillableStatus != null || AccountBasedExpenseLineDetail.Description != null || AccountBasedExpenseLineDetail.AccountAmount != null)
                            AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Add(AccountBasedExpenseLineDetail);

                      

                        if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            purchaseOrder.Line.Add(AccountBasedExpenseLine);
                            purchaseOrder.Line.Add(Line);
                        }
                        else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count > 0 && Line.ItemBasedExpenseLineDetail.Count == 0)
                        {
                            purchaseOrder.Line.Add(AccountBasedExpenseLine);
                        }
                        else if (AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0 && Line.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            purchaseOrder.Line.Add(Line);
                        }
                        else if (Line.ItemBasedExpenseLineDetail.Count == 0 && AccountBasedExpenseLine.AccountBasedExpenseLineDetail.Count == 0)
                        {
                            purchaseOrder.Line.Add(Line);
                            purchaseOrder.Line.Add(AccountBasedExpenseLine);
                        }

                        #region TxnTaxDetail
                        OnlineEntities.TxnTaxDetail TxnTaxDetail = new OnlineEntities.TxnTaxDetail();
                        OnlineEntities.TaxLineDetail TaxLineDetail = new OnlineEntities.TaxLineDetail();
                        OnlineEntities.TxnTaxCodeRef TxnTaxCodeRef = new OnlineEntities.TxnTaxCodeRef();
                        OnlineEntities.TaxLine TaxLine = new OnlineEntities.TaxLine();

                        if (dt.Columns.Contains("TxnTaxCodeRefName"))
                        {
                            #region Validations of TxnTaxCodeRefName
                            if (dr["TxnTaxCodeRefName"].ToString() != string.Empty)
                            {
                                if (dr["TxnTaxCodeRefName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnTaxCodeRefName (" + dr["TxnTaxCodeRefName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                    }
                                }
                                else
                                {
                                    TxnTaxCodeRef.Name = dr["TxnTaxCodeRefName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TotalTax"))
                        {
                            #region Validations for TotalTax
                            if (dr["TotalTax"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TotalTax"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalTax( " + dr["TotalTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TotalTax")
                                        {
                                            isIgnoreAll = true;
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();

                                        }
                                        else
                                        {
                                            TxnTaxDetail.TotalTax = dr["TotalTax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TxnTaxDetail.TotalTax = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TotalTax"].ToString()));
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxLineAmount"))
                        {
                            #region Validations for TaxLineAmount
                            if (dr["TaxLineAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TaxLineAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxLineAmount( " + dr["TaxLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TaxLineAmount")
                                        {
                                            isIgnoreAll = true;
                                            TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLine.Amount = dr["TaxLineAmount"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLine.Amount = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxLineAmount"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercentBased"))
                        {
                            #region Validations of IsActive
                            if (dr["TaxPercentBased"].ToString() != "<None>" || dr["TaxPercentBased"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["TaxPercentBased"].ToString(), out result))
                                {
                                    TaxLineDetail.PercentBased = Convert.ToInt32(dr["TaxPercentBased"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["TaxPercentBased"].ToString().ToLower() == "true")
                                    {
                                        TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["TaxPercentBased"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "TaxPercentBased";
                                        }
                                        else
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TaxPercentBased(" + dr["TaxPercentBased"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            TaxLineDetail.PercentBased = dr["TaxPercentBased"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxPercent"))
                        {
                            #region Validations for TaxPercent
                            if (dr["TaxPercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TaxPercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxPercent( " + dr["TaxPercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                        if (Convert.ToString(result) == "TaxPercent")
                                        {
                                            isIgnoreAll = true;
                                            TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TaxLineDetail.TaxPercent = dr["TaxPercent"].ToString();
                                    }
                                }
                                else
                                {
                                    TaxLineDetail.TaxPercent = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["TaxPercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (TaxLineDetail.NetAmountTaxable != null || TaxLineDetail.PercentBased != null || TaxLineDetail.TaxPercent != null || TaxLineDetail.TaxRateRef.Count > 0)
                        {
                            TaxLine.TaxLineDetail.Add(TaxLineDetail);
                        }
                        if (TxnTaxCodeRef.Name != null)
                        {
                            TxnTaxDetail.TxnTaxCodeRef.Add(TxnTaxCodeRef);
                        }
                        if (TaxLine.Amount != null || TaxLine.TaxLineDetail.Count != 0)
                        {
                            TxnTaxDetail.TaxLine.Add(TaxLine);
                        }
                        if (TxnTaxDetail.TxnTaxCodeRef.Count != 0 || TxnTaxDetail.TaxLine.Count != 0)
                        {
                            purchaseOrder.TxnTaxDetail.Add(TxnTaxDetail);
                        }

                        #endregion

                        OnlineEntities.VendorRef VendorRef = new OnlineEntities.VendorRef();
                        if (dt.Columns.Contains("VendorRef"))
                        {
                            #region Validations of VendorRef
                            if (dr["VendorRef"].ToString() != string.Empty)
                            {
                                if (dr["VendorRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorRef (" + dr["VendorRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorRef.Name = dr["VendorRef"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorRef.Name = dr["VendorRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorRef.Name = dr["VendorRef"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorRef.Name = dr["VendorRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (VendorRef.Name != null)
                        {
                            purchaseOrder.VendorRef.Add(VendorRef);
                        }

                        OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                        if (dt.Columns.Contains("APAccountRef"))
                        {
                            #region Validations of APAccountRef
                            if (dr["APAccountRef"].ToString() != string.Empty)
                            {
                                if (dr["APAccountRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            APAccountRef.Name = dr["APAccountRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    APAccountRef.Name = dr["APAccountRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (APAccountRef.Name != null)
                        {
                            purchaseOrder.APAccountRef.Add(APAccountRef);
                        }
                       
                        OnlineEntities.ClassRef ClassRef2 = new OnlineEntities.ClassRef();
                        if (dt.Columns.Contains("ClassRef"))
                        {
                            #region Validations of ClassRef
                            if (dr["ClassRef"].ToString() != string.Empty)
                            {
                                if (dr["ClassRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ClassRef (" + dr["ClassRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ClassRef2.Name = dr["ClassRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ClassRef2.Name = dr["ClassRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ClassRef2.Name = dr["ClassRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ClassRef2.Name = dr["ClassRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ClassRef2.Name != null)
                        {
                            purchaseOrder.ClassRef.Add(ClassRef2);
                        }

                        OnlineEntities.SalesTermRef SalesTermRef = new OnlineEntities.SalesTermRef();

                        if (dt.Columns.Contains("SalesTermRef"))
                        {
                            #region Validations of SalesTermRef
                            if (dr["SalesTermRef"].ToString() != string.Empty)
                            {
                                if (dr["SalesTermRef"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTermRef (" + dr["SalesTermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesTermRef.Name = dr["SalesTermRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (SalesTermRef.Name != null)
                        {
                            purchaseOrder.SalesTermRef.Add(SalesTermRef);
                        }

                        if (dt.Columns.Contains("DueDate"))
                        {
                            #region Validations of DueDate
                            DateTime SODate = new DateTime();
                            if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["DueDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out SODate))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DueDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                purchaseOrder.DueDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                purchaseOrder.DueDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            purchaseOrder.DueDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        SODate = dttest;
                                        purchaseOrder.DueDate = dttest.ToString("yyyy-MM-dd");
                                    }
                                }
                                else
                                {
                                    SODate = Convert.ToDateTime(datevalue);
                                    purchaseOrder.DueDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        //adding Bill Address 

                        OnlineEntities.VendorAddr VendorAddr = new OnlineEntities.VendorAddr();
                        if (dt.Columns.Contains("VendorAddrLine1"))
                        {
                            #region Validations of VendorAddrLine1
                            if (dr["VendorAddrLine1"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLine1"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLine1 (" + dr["VendorAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorLine1 = dr["VendorAddrLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrLine2"))
                        {
                            #region Validations of VendorAddrLine2
                            if (dr["VendorAddrLine2"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLine2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLine2 (" + dr["VendorAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorLine2 = dr["VendorAddrLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrLine3"))
                        {
                            #region Validations of VendorAddrLine3
                            if (dr["VendorAddrLine3"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLine3"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLine3 (" + dr["VendorAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorLine3 = dr["VendorAddrLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrCity"))
                        {
                            #region Validations of VendorAddrCity
                            if (dr["VendorAddrCity"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrCity"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrCity (" + dr["VendorAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorCity = dr["VendorAddrCity"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorCity = dr["VendorAddrCity"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrCountry"))
                        {
                            #region Validations of VendorAddrCountry
                            if (dr["VendorAddrCountry"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrCountry"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrCountry (" + dr["VendorAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorCountry = dr["VendorAddrCountry"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrSubDivisionCode"))
                        {
                            #region Validations of VendorAddrSubDivisionCode
                            if (dr["VendorAddrSubDivisionCode"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrSubDivisionCode"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrSubDivisionCode (" + dr["VendorAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorCountrySubDivisionCode = dr["VendorAddrSubDivisionCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrPostalCode"))
                        {
                            #region Validations of VendorAddrPostalCode
                            if (dr["VendorAddrPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrPostalCode"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrPostalCode (" + dr["VendorAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorPostalCode = dr["VendorAddrPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrNote"))
                        {
                            #region Validations of VendorAddrNote
                            if (dr["VendorAddrNote"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrNote"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrNote (" + dr["VendorAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorNote = dr["VendorAddrNote"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorNote = dr["VendorAddrNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrLine4"))
                        {
                            #region Validations of VendorAddrLine4
                            if (dr["VendorAddrLine4"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLine4"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLine4 (" + dr["VendorAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorLine4 = dr["VendorAddrLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrLine5"))
                        {
                            #region Validations of VendorAddrLine5
                            if (dr["VendorAddrLine5"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLine5"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLine5 (" + dr["VendorAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorLine5 = dr["VendorAddrLine5"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("VendorAddrLat"))
                        {
                            #region Validations of VendorAddrLat
                            if (dr["VendorAddrLat"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLat"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLat (" + dr["VendorAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorAddrLat = dr["VendorAddrLat"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("VendorAddrLong"))
                        {
                            #region Validations of VendorAddrLong
                            if (dr["VendorAddrLong"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddrLong"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorAddrLong (" + dr["VendorAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddr.VendorAddrLong = dr["VendorAddrLong"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (VendorAddr.VendorLine1 != null || VendorAddr.VendorLine2 != null || VendorAddr.VendorLine3 != null || VendorAddr.VendorLine4 != null || VendorAddr.VendorLine5 != null || VendorAddr.VendorCity != null || VendorAddr.VendorCountry != null || VendorAddr.VendorCountrySubDivisionCode != null || VendorAddr.VendorNote != null || VendorAddr.VendorAddrLat != null || VendorAddr.VendorAddrLong != null || VendorAddr.VendorPostalCode != null)
                            purchaseOrder.VendorAddr.Add(VendorAddr);

                        OnlineEntities.ShipAddr shipAddr = new OnlineEntities.ShipAddr();
                        if (dt.Columns.Contains("ShipAddrLine1"))
                        {
                            #region Validations of ShipAddrLine1
                            if (dr["ShipAddrLine1"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine1"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine1 (" + dr["ShipAddrLine1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine1 = dr["ShipAddrLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine2"))
                        {
                            #region Validations of ShipAddrLine2
                            if (dr["ShipAddrLine2"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine2"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine2 (" + dr["ShipAddrLine2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine2 = dr["ShipAddrLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine3"))
                        {
                            #region Validations of ShipAddrLine3
                            if (dr["ShipAddrLine3"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine3"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine3 (" + dr["ShipAddrLine3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine3 = dr["ShipAddrLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrCity"))
                        {
                            #region Validations of ShipAddrCity
                            if (dr["ShipAddrCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrCity"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrCity (" + dr["ShipAddrCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCity = dr["ShipAddrCity"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrCountry"))
                        {
                            #region Validations of ShipAddrCountry
                            if (dr["ShipAddrCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrCountry"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrCountry (" + dr["ShipAddrCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountry = dr["ShipAddrCountry"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrSubDivisionCode"))
                        {
                            #region Validations of ShipAddrSubDivisionCode
                            if (dr["ShipAddrSubDivisionCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrSubDivisionCode"].ToString().Length > 255)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrSubDivisionCode (" + dr["ShipAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString().Substring(0, 255);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipCountrySubDivisionCode = dr["ShipAddrSubDivisionCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrPostalCode"))
                        {
                            #region Validations of ShipAddrPostalCode
                            if (dr["ShipAddrPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrPostalCode"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrPostalCode (" + dr["ShipAddrPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipPostalCode = dr["ShipAddrPostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrNote"))
                        {
                            #region Validations of ShipAddrNote
                            if (dr["ShipAddrNote"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrNote"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrNote (" + dr["ShipAddrNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipNote = dr["ShipAddrNote"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine4"))
                        {
                            #region Validations of ShipAddrLine4
                            if (dr["ShipAddrLine4"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine4"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine4 (" + dr["ShipAddrLine4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine4 = dr["ShipAddrLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLine5"))
                        {
                            #region Validations of ShipAddrLine5
                            if (dr["ShipAddrLine5"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLine5"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLine5 (" + dr["ShipAddrLine5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString().Substring(0, 500);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipLine5 = dr["ShipAddrLine5"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ShipAddrLat"))
                        {
                            #region Validations of ShipAddrLat
                            if (dr["ShipAddrLat"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLat"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLat (" + dr["ShipAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLat = dr["ShipAddrLat"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipAddrLong"))
                        {
                            #region Validations of ShipAddrLong
                            if (dr["ShipAddrLong"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddrLong"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipAddrLong (" + dr["ShipAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString().Substring(0, 1000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    shipAddr.ShipAddrLong = dr["ShipAddrLong"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (shipAddr.ShipLine1 != null || shipAddr.ShipLine2 != null || shipAddr.ShipLine3 != null || shipAddr.ShipLine4 != null || shipAddr.ShipLine5 != null || shipAddr.ShipCity != null || shipAddr.ShipCountry != null || shipAddr.ShipCountrySubDivisionCode != null || shipAddr.ShipNote != null || shipAddr.ShipAddrLat != null || shipAddr.ShipAddrLong != null || shipAddr.ShipPostalCode != null)
                            purchaseOrder.ShipAddr.Add(shipAddr);

                        ShipMethodRef ShipMethodRef = new ShipMethodRef();

                        if (dt.Columns.Contains("ShipMethodRef"))
                        {
                            #region Validations of ShipMethodRef
                            if (dr["ShipMethodRef"].ToString() != string.Empty)
                            {
                                if (dr["ShipMethodRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipMethodRef (" + dr["ShipMethodRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipMethodRef.Name = dr["ShipMethodRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (ShipMethodRef.Name != null)
                        {
                            purchaseOrder.ShipMethodRef.Add(ShipMethodRef);
                        }

                        if (dt.Columns.Contains("POStatus"))
                        {
                            #region Validations of POStatus
                            if (dr["POStatus"].ToString() != string.Empty)
                            {
                                if (dr["POStatus"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Status (" + dr["POStatus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            purchaseOrder.POStatus = dr["POStatus"].ToString().Substring(0, 15);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            purchaseOrder.POStatus = dr["POStatus"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.POStatus = dr["POStatus"].ToString();
                                    }
                                }
                                else
                                {
                                    purchaseOrder.POStatus = dr["POStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        CurrencyRef CurrencyRef = new CurrencyRef();

                        if (dt.Columns.Contains("CurrencyRef"))
                        {
                            #region Validations of CurrencyRef
                            if (dr["CurrencyRef"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRef"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CurrencyRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (CurrencyRef.Name != null)
                        {
                            purchaseOrder.CurrencyRef.Add(CurrencyRef);
                        }

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "ExchangeRate")
                                        {
                                            isIgnoreAll = true;
                                            purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    purchaseOrder.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("GlobalTaxCalculation"))
                        {
                            #region Validations of GlobalTaxCalculation
                            if (dr["GlobalTaxCalculation"].ToString() != string.Empty)
                            {
                                if (dr["GlobalTaxCalculation"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This GlobalTaxCalculation (" + dr["GlobalTaxCalculation"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();
                                    }
                                }
                                else
                                {
                                    purchaseOrder.GlobalTaxCalculation = dr["GlobalTaxCalculation"].ToString();

                                }
                            }
                            #endregion
                        }
                        #endregion

                        coll.Add(purchaseOrder);
                    }

                }
                else
                {
                    return null;
                }

            }
            #endregion
            
            #endregion

            return coll;

        }
    }
}
