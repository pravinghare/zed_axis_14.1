﻿using System;
using System.Collections.ObjectModel;
using DataProcessingBlocks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Net;
using OnlineEntities;
using System.Threading.Tasks;

namespace OnlineDataProcessingsImportClass
{
    public class OnlineVendorCreditQBEntry
    {
        #region member
        private string m_DocNumber;
        private string m_PrivateNote;
        private string m_TxnDate;
        private Collection<OnlineEntities.DepartmentRef> m_DepartmentRef = new Collection<OnlineEntities.DepartmentRef>();
        private Collection<OnlineEntities.Line> m_Line = new Collection<OnlineEntities.Line>();
      //Bug;;586
        private Collection<OnlineEntities.TxnTaxCodeRef> m_TxnTaxCodeRef = new Collection<OnlineEntities.TxnTaxCodeRef>();

        private Collection<OnlineEntities.VendorRef> m_VendorRef = new Collection<OnlineEntities.VendorRef>();
        private Collection<OnlineEntities.APAccountRef> m_APAccountRef = new Collection<OnlineEntities.APAccountRef>();
        private string m_TotalAmt;

        private string m_GlobalTaxCalculation;
        private string m_TransactionLocationType;

        //594
        private bool m_isAppend;

        
        #endregion

        #region properties
        public string GlobalTaxCalculation
        {
            get { return m_GlobalTaxCalculation; }
            set { m_GlobalTaxCalculation = value; }
        }

        public string TransactionLocationType
        {
            get { return m_TransactionLocationType; }
            set { m_TransactionLocationType = value; }
        }

        public string DocNumber
        {
            get { return m_DocNumber; }
            set { m_DocNumber = value; }
        }

        public string PrivateNote
        {
            get { return m_PrivateNote; }
            set { m_PrivateNote = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public Collection<OnlineEntities.DepartmentRef> DepartmentRef
        {
            get { return m_DepartmentRef; }
            set { m_DepartmentRef = value; }
        }

        public Collection<OnlineEntities.Line> Line
        {
            get { return m_Line; }
            set { m_Line = value; }
        }

      //Bug;;586
        public Collection<OnlineEntities.TxnTaxCodeRef> TxnTaxCodeRef
        {
            get { return m_TxnTaxCodeRef; }
            set { m_TxnTaxCodeRef = value; }
        }

        public Collection<OnlineEntities.VendorRef> VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        public Collection<OnlineEntities.APAccountRef> APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }

        //594
        public bool isAppend
        {
            get { return m_isAppend; }
            set { m_isAppend = value; }
        }
     
        #endregion

        #region public method
        /// <summary>
        /// static method for resize array for Line tag  or taxline tag.
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="line"></param>
        /// <param name="linecount"></param>
        /// <returns></returns>
        public static Intuit.Ipp.Data.Line[] AddLine(Intuit.Ipp.Data.Line[] lines, Intuit.Ipp.Data.Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
        /// <summary>
        /// Creating new Vendor Credit transaction in quickbook online.
        /// </summary>
        /// <param name="coll"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> CreateQBOVendorCredit(OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry coll)
        {
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.VendorCredit vendorCreditAdded = null;
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            Intuit.Ipp.Data.VendorCredit addedvendorCredit = new Intuit.Ipp.Data.VendorCredit();
            OnlineVendorCreditQBEntry vendorCredit = new OnlineVendorCreditQBEntry();
            vendorCredit = coll;
            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add Vendor Credit

                //594
                if (vendorCredit.isAppend == true)
                {
                    addedvendorCredit.sparse = true;
                }

                if (vendorCredit.DocNumber != null)
                {
                    addedvendorCredit.DocNumber = vendorCredit.DocNumber;
                }

                if (vendorCredit.TxnDate != null)
                {
                    try
                    {
                        addedvendorCredit.TxnDate = Convert.ToDateTime(vendorCredit.TxnDate);
                        addedvendorCredit.TxnDateSpecified = true;
                    }
                    catch { }
                }

                if (vendorCredit.DepartmentRef != null)
                {
                    foreach (var DepartmentRef in vendorCredit.DepartmentRef)
                    {
                        string Vendorvalue = string.Empty;
                        if (DepartmentRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentRef.Name.Trim());
                            foreach (var vendor in dataVendor)
                            {
                                Vendorvalue = vendor.Id;
                            }  
                            addedvendorCredit.VendorRef = new ReferenceType()
                            {
                                name = DepartmentRef.Name,
                                Value = Vendorvalue
                            };
                        }
                    }
                }

                addedvendorCredit.PrivateNote = vendorCredit.PrivateNote;

                // Axis 742
                if (vendorCredit.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(vendorCredit.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedvendorCredit.TransactionLocationType = value;
                    }
                    else
                    {
                        addedvendorCredit.TransactionLocationType = vendorCredit.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (vendorCredit.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                        else if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion              

                foreach (var l in vendorCredit.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }                             
                               
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }

                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                   // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {


                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }                                

                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                QueryService<TaxCode> TAxService = new QueryService<TaxCode>(serviceContext);
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                       
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedvendorCredit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                //bug 486 axis 12.0
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }

                                    }

                                }

                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                               // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }


                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                   // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                               // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                    if (accountData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                    if (customerData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                  
                                }
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }


                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedvendorCredit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                }

                //BUG ;; 586
                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    if (vendorCredit.TxnTaxCodeRef != null)
                    {
                        //foreach (var txntaxCode in vendorCredit.TxnTaxCodeRef)
                        //{
                            #region TxnTaxDetail
                            //Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        //if (txntaxCode.Name != null)
                        //{
                        foreach (var taxRef in vendorCredit.TxnTaxCodeRef)
                        {
                            TaxCode TaxCode = new TaxCode();
                            //TaxCode.Name = taxRef.Name;
                            string Taxvalue = string.Empty;
                            string TaxName = string.Empty;
                            TaxName = TaxCode.Name;
                            if (TaxCode.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = TaxName,
                                        Value = Taxvalue
                                    };
                                }
                            }

                        }
                            //}

                            if (txnTaxDetail != null)
                            {
                                addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                            }
                            #endregion
                       // }
                    }

                    //foreach (var txntaxCode in vendorCredit.TxnTaxDetail)
                    //{
                    //    #region TxnTaxDetail
                    //    ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                    //    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    //    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    //    if (txntaxCode.TxnTaxCodeRef != null)
                    //    {
                    //        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                    //        {
                    //            QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);
                    //            TaxCode TaxCode = new TaxCode();
                    //            TaxCode.Name = taxRef.Name;
                    //            string Taxvalue = string.Empty;
                    //            string TaxName = string.Empty;
                    //            TaxName = TaxCode.Name;
                    //            if (TaxCode.Name != null)
                    //            {
                    //                if (TaxCode.Name.Contains("'"))
                    //                {
                    //                    TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                    //                }
                    //                dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + TaxCode.Name.Trim() + "'"));
                    //                if (dataTax != null)
                    //                {
                    //                    foreach (var tax in dataTax)
                    //                    {
                    //                        Taxvalue = tax.Id;
                    //                    }

                    //                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                    //                    {
                    //                        name = TaxName,
                    //                        Value = Taxvalue
                    //                    };
                    //                }
                    //            }

                    //        }
                    //    }

                    //    if (txnTaxDetail != null)
                    //    {
                    //        addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                    //    }
                    //    #endregion
                    //}
                }

                if (vendorCredit.VendorRef != null)
                {
                    foreach (var vendorRef in vendorCredit.VendorRef)
                    {
                        string Vendorvalue = string.Empty;
                        if (vendorRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(vendorRef.Name.Trim());
                            foreach (var customer in dataVendor)
                            {
                                Vendorvalue = customer.Id;
                            }
                           

                            addedvendorCredit.VendorRef = new ReferenceType()
                            {
                                name = vendorRef.Name,
                                Value = Vendorvalue
                            };
                        }

                    }
                }

                if (vendorCredit.APAccountRef != null)
                {
                    foreach (var APAccountRef in vendorCredit.APAccountRef)
                    {
                        if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                            addedvendorCredit.APAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedvendorCredit.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && vendorCredit.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                #endregion

                
                #endregion

                CommonUtilities.GetInstance().newSKUItem = false;
                vendorCreditAdded = new Intuit.Ipp.Data.VendorCredit();
                vendorCreditAdded = dataService.Add<Intuit.Ipp.Data.VendorCredit>(addedvendorCredit);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (vendorCreditAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = vendorCreditAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = vendorCreditAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }
        /// <summary>
        ///  updating Vendor Credit entry in quickbook online by the transaction Id..
        /// </summary>
        /// <param name="coll"></param>
        /// <param name="VendorCreditId"></param>
        /// <param name="syncToken"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<bool> UpdateQBOVendorCredit(OnlineDataProcessingsImportClass.OnlineVendorCreditQBEntry coll, string VendorCreditId, string syncToken, Intuit.Ipp.Data.VendorCredit PreviousData = null)
        {
            bool flag = false;
            int linecount = 1;
            Intuit.Ipp.Data.VendorCredit vendorCreditAdded = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);


            Intuit.Ipp.Data.VendorCredit addedvendorCredit = new Intuit.Ipp.Data.VendorCredit();
            OnlineVendorCreditQBEntry vendorCredit = new OnlineVendorCreditQBEntry();
            vendorCredit = coll;

            Intuit.Ipp.Data.ItemBasedExpenseLineDetail ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
            Intuit.Ipp.Data.AccountBasedExpenseLineDetail AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
            Intuit.Ipp.Data.Line Line = new Intuit.Ipp.Data.Line();
            Intuit.Ipp.Data.Line[] lines_online = new Intuit.Ipp.Data.Line[linecount];

            string taxRateRefName = string.Empty;
            string ratevalue = string.Empty;

            //bug 514
            bool taxFlag = true;
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            
            string GSTtaxRateRefName = string.Empty; //store name of taxRate from GST                 
            ReferenceType GstRef = new ReferenceType(); // store name & value of GST if exists
            int array_count = 0;
            int linecount1 = 0;
            Intuit.Ipp.Data.Line[] lines_online1 = new Intuit.Ipp.Data.Line[linecount1];//array contains bunch of TaxLine Tag

            try
            {
                #region Add Vendor Credit

                //594
                if (vendorCredit.isAppend == true)
                {
                    addedvendorCredit = PreviousData;
                    if (addedvendorCredit.Line.Length != 0)
                    {
                        flag = true;
                        linecount = addedvendorCredit.Line.Length;
                        lines_online = addedvendorCredit.Line;
                        Array.Resize(ref lines_online, linecount);
                    }
					addedvendorCredit.sparse = true;
                    addedvendorCredit.sparseSpecified = true;
                }

                addedvendorCredit.Id = VendorCreditId;
                addedvendorCredit.SyncToken = syncToken;

                if (vendorCredit.DocNumber != null)
                {
                    addedvendorCredit.DocNumber = vendorCredit.DocNumber;
                }

                if (vendorCredit.TxnDate != null)
                {
                    try
                    {
                        addedvendorCredit.TxnDate = Convert.ToDateTime(vendorCredit.TxnDate);
                        addedvendorCredit.TxnDateSpecified = true;
                    }
                    catch { }
                }

                if (vendorCredit.DepartmentRef != null)
                {
                    foreach (var DepartmentRef in vendorCredit.DepartmentRef)
                    {
                        string Vendorvalue = string.Empty;
                        if (DepartmentRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetDepartmentExistsInOnlineQBO(DepartmentRef.Name.Trim());
                            foreach (var vendor in dataVendor)
                            {
                                Vendorvalue = vendor.Id;
                            }
                            addedvendorCredit.VendorRef = new ReferenceType()
                            {
                                name = DepartmentRef.Name,
                                Value = Vendorvalue
                            };
                        }
                    }
                }

                addedvendorCredit.PrivateNote = vendorCredit.PrivateNote;

                if (vendorCredit.TransactionLocationType != null)
                {
                    string value = CommonUtilities.GetTransactionLocationValueForQBO(vendorCredit.TransactionLocationType);
                    if (value != string.Empty)
                    {
                        addedvendorCredit.TransactionLocationType = value;
                    }
                    else
                    {
                        addedvendorCredit.TransactionLocationType = vendorCredit.TransactionLocationType;
                    }
                }

                #region GlobalTaxCalculation

                if (vendorCredit.GlobalTaxCalculation != null)
                {

                    if (CommonUtilities.GetInstance().CountryVersion != "US")
                    {
                        if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.NotApplicable.ToString())
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.NotApplicable;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                        else if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                        else
                        {
                            addedvendorCredit.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxExcluded;
                            addedvendorCredit.GlobalTaxCalculationSpecified = true;
                        }
                    }
                }

                #endregion              
                foreach (var l in vendorCredit.Line)
                {
                    if (flag == false)
                    {
                        #region
                        flag = true;
                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }

                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }
                                    }

                                }

                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    if (i.Name != null)
                                    {
                                        string id = string.Empty;
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {


                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }

                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }
                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    ItemBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;
                            }

                        }
                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }

                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (accountData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    if (customerData.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                          
                                           if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedvendorCredit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }
                    else if (flag == true)
                    {
                        #region
                        Line = new Intuit.Ipp.Data.Line();
                        ItemBasedExpenseLineDetail = new Intuit.Ipp.Data.ItemBasedExpenseLineDetail();
                        AccountBasedExpenseLineDetail = new Intuit.Ipp.Data.AccountBasedExpenseLineDetail();
                        linecount++;

                        if (l.ItemBasedExpenseLineDetail.Count > 0)
                        {
                            Line.Description = l.LineDescription;
                            foreach (var ItemBasedExp in l.ItemBasedExpenseLineDetail)
                            {
                                if (ItemBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                //bug 486 axis 12.0
                                foreach (var i in ItemBasedExp.ItemRef)
                                {
                                    string id = string.Empty;
                                    string name = string.Empty;
                                    
                                    if (CommonUtilities.GetInstance().SKULookup == false || CommonUtilities.GetInstance().newSKUItem == true)
                                    {
                                        if (CommonUtilities.GetInstance().newSKUItem == true && i.SKU !=null )
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.SKU,
                                                Value = id
                                            };
                                        }

                                        else if (i.Name != null)
                                        {
                                            var dataItem = await CommonUtilities.GetItemExistsInOnlineQBO(i.Name.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = i.Name,
                                                Value = id
                                            };
                                        }
                                    }
                                    else
                                    {
                                        if (i.SKU != null)
                                        {
                                            var dataItem = await CommonUtilities.GetSKUItemExistsInOnlineQBO(i.SKU.Trim());
                                            foreach (var item in dataItem)
                                            {
                                                id = item.Id;
                                                name = item.FullyQualifiedName;
                                            }
                                            ItemBasedExpenseLineDetail.ItemRef = new ReferenceType()
                                            {
                                                name = name,
                                                Value = id
                                            };
                                        }

                                    }

                                }

                                foreach (var i in ItemBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var i in ItemBasedExp.CustomerRef)
                                {
                                    string id = string.Empty;
                                    if (i.Name != null)
                                    {
                                        var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(i.Name.Trim());
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        ItemBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = i.Name,
                                            Value = id
                                        };
                                    }
                                }

                                if (l.LineAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                        Line.Amount = Convert.ToDecimal(l.LineAmount);
                                        Line.AmountSpecified = true;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(l.LineAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(l.LineAmount));
                                            Line.Amount = Convert.ToDecimal(l.LineAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                if (ItemBasedExp.UnitPrice != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                        ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                    }
                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {

                                        #region Gross Net for unit price
                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                if (ItemBasedExp.Qty != null)
                                                {
                                                    ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Line.Amount / Convert.ToDecimal(ItemBasedExp.Qty), 2);
                                                    ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                                }
                                            }
                                            else
                                            {
                                                ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 2);
                                                ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                            }

                                        }
                                        else
                                        {
                                            ItemBasedExpenseLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(ItemBasedExp.UnitPrice), 7);
                                            ItemBasedExpenseLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                        }
                                        #endregion
                                    }
                                }
                                if (ItemBasedExp.Qty != null)
                                {
                                    ItemBasedExpenseLineDetail.Qty = Convert.ToDecimal(ItemBasedExp.Qty);
                                    ItemBasedExpenseLineDetail.QtySpecified = true;
                                }


                                foreach (var taxCoderef in ItemBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(l.LineAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(l.LineAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(l.LineAmount);
                                                    taxFlag = false;
                                                }
                                            }
                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }

                                        ItemBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,
                                            Value = id
                                        };
                                    }
                                }

                                #region BillableStatus
                                if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                }
                                else if (ItemBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    ItemBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                }
                                #endregion

                                Line.AnyIntuitObject = ItemBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;
                            }
                        }

                        if (l.AccountBasedExpenseLineDetail.Count > 0)
                        {
                            foreach (var AccountBasedExp in l.AccountBasedExpenseLineDetail)
                            {
                                if (AccountBasedExp != null)
                                {
                                    Line.DetailType = LineDetailTypeEnum.AccountBasedExpenseLineDetail;
                                    Line.DetailTypeSpecified = true;
                                }
                                if (AccountBasedExp.Description != null)
                                {
                                    Line.Description = AccountBasedExp.Description;
                                }
                                if (AccountBasedExp.AccountAmount != null)
                                {
                                    if (CommonUtilities.GetInstance().CountryVersion == "US")
                                    {
                                        Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                        Line.AmountSpecified = true;
                                    }

                                    else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                    {
                                        #region Gross Net Amt

                                        if (CommonUtilities.GetInstance().GrossNet == true)
                                        {
                                            string rate = string.Empty;
                                            if (vendorCredit.GlobalTaxCalculation == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                            {
                                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                                {
                                                    if (taxCoderef.Name != null)
                                                    {
                                                        var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                                        
                                                        if (dataTaxcode != null && dataTaxcode.Count > 0)
                                                        {
                                                            foreach (var taxRateRef in dataTaxcode)
                                                            {
                                                                foreach (var taxRate in taxRateRef.PurchaseTaxRateList.TaxRateDetail)
                                                                {
                                                                    taxRateRefName = taxRate.TaxRateRef.name;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //rate percent
                                                var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(taxRateRefName.Trim());
                                                if (dataTaxRate != null)
                                                {
                                                    foreach (var taxRateval in dataTaxRate)
                                                    {
                                                        ratevalue = taxRateval.RateValue.ToString();
                                                    }
                                                }

                                                if (ratevalue != string.Empty)
                                                {
                                                    //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount) / (1 + (Convert.ToDecimal(ratevalue) / 100)), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }
                                                else
                                                {
                                                    // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                    Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                    Line.AmountSpecified = true;
                                                }

                                            }
                                            else
                                            {
                                                // Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                                Line.Amount = Math.Round(Convert.ToDecimal(AccountBasedExp.AccountAmount), 2, MidpointRounding.AwayFromZero);
                                                Line.AmountSpecified = true;
                                            }
                                        }
                                        else
                                        {
                                            //Line.Amount = Math.Ceiling(Convert.ToDecimal(account.AccountAmount));
                                            Line.Amount = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                            Line.AmountSpecified = true;
                                        }

                                        #endregion
                                    }
                                }

                                foreach (var accountData in AccountBasedExp.ClassRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetClassExistsInOnlineQBO(accountData.Name.Trim());
                                    if (accountData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.ClassRef = new ReferenceType()
                                        {
                                            name = accountData.Name,
                                            Value = id
                                        };
                                    }
                                }

                                foreach (var customerData in AccountBasedExp.customerRef)
                                {
                                    string id = string.Empty;
                                    var dataItem = await CommonUtilities.GetCustomerExistsInOnlineQBO(customerData.Name.Trim());
                                    if (customerData.Name != null)
                                    {
                                        foreach (var item in dataItem)
                                        {
                                            id = item.Id;
                                        }
                                        AccountBasedExpenseLineDetail.CustomerRef = new ReferenceType()
                                        {
                                            name = customerData.Name,
                                            Value = id
                                        };
                                    }
                                }
                                foreach (var accountData in AccountBasedExp.AccountRef)
                                {
                                    if (accountData.Name != null && accountData.Name != string.Empty && accountData.Name != "")
                                    {
                                        ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(accountData.Name);
                                        AccountBasedExpenseLineDetail.AccountRef = new ReferenceType()
                                        {
                                            name = accDetails.name,
                                            Value = accDetails.Value
                                        };
                                    }
                                   
                                }
                                foreach (var taxCoderef in AccountBasedExp.TaxCodeRef)
                                {
                                    if (taxCoderef.Name != null)
                                    {
                                        string id = string.Empty;
                                        if (CommonUtilities.GetInstance().CountryVersion == "US")
                                        {
                                            if (taxCoderef.Name == "TAX")
                                            {
                                                id = "TAX";
                                            }
                                            else if (taxCoderef.Name == "NON")
                                            {
                                                id = "NON";
                                            }
                                        }
                                        else if (CommonUtilities.GetInstance().CountryVersion != "US")
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = taxCoderef.Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(AccountBasedExp.AccountAmount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (taxCoderef.Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = taxCoderef.Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(AccountBasedExp.AccountAmount);
                                                    taxFlag = false;
                                                }
                                            }

                                            var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCoderef.Name.Trim());
                                            foreach (var tax in dataTaxcode)
                                            {
                                                id = tax.Id;
                                            }
                                        }
                                        AccountBasedExpenseLineDetail.TaxCodeRef = new ReferenceType()
                                        {
                                            name = taxCoderef.Name,

                                            Value = id
                                        };
                                    }
                                }


                                #region BillableStatus
                                if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.Billable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.Billable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.HasBeenBilled;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                else if (AccountBasedExp.BillableStatus == Intuit.Ipp.Data.BillableStatusEnum.NotBillable.ToString())
                                {
                                    AccountBasedExpenseLineDetail.BillableStatus = Intuit.Ipp.Data.BillableStatusEnum.NotBillable;
                                    AccountBasedExpenseLineDetail.BillableStatusSpecified = true;
                                }
                                #endregion
                                Line.AnyIntuitObject = AccountBasedExpenseLineDetail;
                                Array.Resize(ref lines_online, linecount);
                                lines_online[linecount - 1] = Line;

                            }
                        }
                        addedvendorCredit.Line = AddLine(lines_online, Line, linecount);
                        #endregion
                    }

                }

                //BUG ;; 586

                if (CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    if (vendorCredit.TxnTaxCodeRef != null)
                    {
                        //foreach (var txntaxCode in vendorCredit.TxnTaxCodeRef)
                        //{
                        #region TxnTaxDetail
                        //Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                        Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                        //if (txntaxCode.Name != null)
                        //{
                        foreach (var taxRef in vendorCredit.TxnTaxCodeRef)
                        {
                            TaxCode TaxCode = new TaxCode();
                            //TaxCode.Name = taxRef.Name;
                            string Taxvalue = string.Empty;
                            string TaxName = string.Empty;
                            TaxName = TaxCode.Name;
                            if (TaxCode.Name != null)
                            {
                                var dataTax = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(TaxCode.Name.Trim());
                                if (dataTax != null)
                                {
                                    foreach (var tax in dataTax)
                                    {
                                        Taxvalue = tax.Id;
                                    }

                                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                                    {
                                        name = TaxName,
                                        Value = Taxvalue
                                    };
                                }
                            }

                        }
                        //}

                        if (txnTaxDetail != null)
                        {
                            addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                        }
                        #endregion
                        // }
                    }

                    //foreach (var txntaxCode in vendorCredit.TxnTaxDetail)
                    //{
                    //    #region TxnTaxDetail
                    //    ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataTax = null;
                    //    Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                    //    Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                    //    if (txntaxCode.TxnTaxCodeRef != null)
                    //    {
                    //        foreach (var taxRef in txntaxCode.TxnTaxCodeRef)
                    //        {
                    //            QueryService<TaxCode> taxCodeQueryService = new QueryService<TaxCode>(serviceContext);
                    //            TaxCode TaxCode = new TaxCode();
                    //            TaxCode.Name = taxRef.Name;
                    //            string Taxvalue = string.Empty;
                    //            string TaxName = string.Empty;
                    //            TaxName = TaxCode.Name;
                    //            if (TaxCode.Name != null)
                    //            {
                    //                if (TaxCode.Name.Contains("'"))
                    //                {
                    //                    TaxCode.Name = TaxCode.Name.Replace("'", "\\'");
                    //                }
                    //                dataTax = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(taxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode where Name='" + TaxCode.Name.Trim() + "'"));
                    //                if (dataTax != null)
                    //                {
                    //                    foreach (var tax in dataTax)
                    //                    {
                    //                        Taxvalue = tax.Id;
                    //                    }

                    //                    txnTaxDetail.TxnTaxCodeRef = new ReferenceType()
                    //                    {
                    //                        name = TaxName,
                    //                        Value = Taxvalue
                    //                    };
                    //                }
                    //            }

                    //        }
                    //    }

                    //    if (txnTaxDetail != null)
                    //    {
                    //        addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                    //    }
                    //    #endregion
                    //}
                }

                if (vendorCredit.VendorRef != null)
                {
                    foreach (var vendorRef in vendorCredit.VendorRef)
                    {
                        string Vendorvalue = string.Empty;
                        if (vendorRef.Name != null)
                        {
                            var dataVendor = await CommonUtilities.GetVendorExistsInOnlineQBO(vendorRef.Name.Trim());
                            foreach (var customer in dataVendor)
                            {
                                Vendorvalue = customer.Id;
                            }

                            addedvendorCredit.VendorRef = new ReferenceType()
                            {
                                name = vendorRef.Name,
                                Value = Vendorvalue
                            };
                        }

                    }
                }


                if (vendorCredit.APAccountRef != null)
                {
                    foreach (var APAccountRef in vendorCredit.APAccountRef)
                    {
                        if (APAccountRef.Name != null && APAccountRef.Name != string.Empty && APAccountRef.Name != "")
                        {
                            ReferenceType accDetails = await CommonUtilities.GetInstance().QBOGetAccountDetails(APAccountRef.Name);
                            addedvendorCredit.APAccountRef = new ReferenceType()
                            {
                                name = accDetails.name,
                                Value = accDetails.Value
                            };
                        }
                       
                    }
                }

                #region TxnTaxDetails bug 514
                if (CommonUtilities.GetInstance().CountryVersion != "US")
                {
                    if (CommonUtilities.GetInstance().GrossNet == true)
                    {

                        if (addedvendorCredit.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString() && vendorCredit.GlobalTaxCalculation != null)
                        {
                            #region TxnTaxDetail
                            decimal TotalTax = 0;
                            Intuit.Ipp.Data.Line taxLine = new Intuit.Ipp.Data.Line();
                            Intuit.Ipp.Data.TxnTaxDetail txnTaxDetail = new Intuit.Ipp.Data.TxnTaxDetail();
                            Intuit.Ipp.Data.TaxLineDetail taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            //taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                            for (int i = 0; i < array_count; i++)
                            {

                                string taxCode = string.Empty;
                                taxCode = Array_taxcodeName[i].ToString();
                                var dataTaxcode = await CommonUtilities.GetTaxCodeDetailsExistsInOnlineQBO(taxCode.Trim());
                                if (dataTaxcode.Count != 0)
                                {
                                    taxLine = new Intuit.Ipp.Data.Line();
                                    taxLineDetail = new Intuit.Ipp.Data.TaxLineDetail();
                                    foreach (var tax in dataTaxcode)
                                    {
                                        if (tax.PurchaseTaxRateList.TaxRateDetail != null)
                                        {
                                            foreach (var taxRate in tax.PurchaseTaxRateList.TaxRateDetail)
                                            {
                                                GstRef = taxRate.TaxRateRef;
                                                GSTtaxRateRefName = taxRate.TaxRateRef.name;
                                            }
                                        }
                                    }

                                    taxLine.DetailType = LineDetailTypeEnum.TaxLineDetail;
                                    taxLine.DetailTypeSpecified = true;

                                    taxLineDetail.PercentBased = true;
                                    taxLineDetail.PercentBasedSpecified = true;

                                    taxLineDetail.TaxRateRef = GstRef;

                                    string taxValue = string.Empty;
                                    var dataTaxRate = await CommonUtilities.GetTaxRateExistsInOnlineQBO(GSTtaxRateRefName.Trim());
                                    foreach (var dTaxRate in dataTaxRate)
                                    {
                                        taxValue = dTaxRate.RateValue.ToString();
                                    }

                                    decimal s = Convert.ToDecimal(taxValue);
                                    decimal taxVal = (s) / 100;
                                    taxLineDetail.TaxPercent = s;
                                    taxLineDetail.TaxPercentSpecified = true;

                                    taxLine.Amount = Math.Round(Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLine.AmountSpecified = true;
                                    TotalTax += Convert.ToDecimal(Array_AmountWithTax[i] - Array_taxAmount[i]);
                                    taxLineDetail.NetAmountTaxable = Math.Round(Convert.ToDecimal(Array_taxAmount[i]), 2, MidpointRounding.AwayFromZero);
                                    taxLineDetail.NetAmountTaxableSpecified = true;

                                    taxLine.AnyIntuitObject = taxLineDetail;
                                    linecount1++;
                                    Array.Resize(ref lines_online1, linecount1);
                                    lines_online1[linecount1 - 1] = taxLine;
                                    txnTaxDetail.TaxLine = AddLine(lines_online1, taxLine, linecount1);
                                }
                            }
                            if (txnTaxDetail.TaxLine != null)
                            {
                                txnTaxDetail.TotalTax = Math.Round(TotalTax, 2, MidpointRounding.AwayFromZero);
                                txnTaxDetail.TotalTaxSpecified = true;
                                addedvendorCredit.TxnTaxDetail = txnTaxDetail;
                            }

                            #endregion
                        }
                    }
                    //Calculate TxnTaxDetail from the type of TaxCodeRef of SalesItemLine or ItemBasedLine.(Tyeps: GST,Zero-rated,Exempt)

                }

                #endregion

                
                #endregion
                vendorCreditAdded = new Intuit.Ipp.Data.VendorCredit();
                vendorCreditAdded = dataService.Update<Intuit.Ipp.Data.VendorCredit>(addedvendorCredit);
            }
            #region error
            catch (Intuit.Ipp.Exception.IdsException ex)
            {
                CommonUtilities.GetInstance().getError = string.Empty;
                CommonUtilities.GetInstance().getError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();

            }
            catch (WebException ex)
            {
                CommonUtilities.GetInstance().getError = "Can not connect to Quickbook";
                //MessageBox.Show(CommonUtilities.GetInstance().getError);
            }
            finally
            {
                if (CommonUtilities.GetInstance().getError == string.Empty)
                {
                    if (vendorCreditAdded.domain == "QBO")
                    {
                        CommonUtilities.GetInstance().TxnId = vendorCreditAdded.Id;
                        CommonUtilities.GetInstance().SyncToken = vendorCreditAdded.SyncToken;

                    }
                }
                else
                {
                }

            }
            if (CommonUtilities.GetInstance().getError != string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
            #endregion

        }

        #endregion
    }

    /// <summary>
    /// checking doc number is present or not in quickbook online for vendor Credit .
    /// </summary>

  public class OnlineVendorCreditQBEntryCollection : Collection<OnlineVendorCreditQBEntry>
  {
      public OnlineVendorCreditQBEntry FindVendorCreditNumberEntry(string docNumber)
      {
          foreach (OnlineVendorCreditQBEntry item in this)
          {
              if (item.DocNumber == docNumber)
              {
                  return item;
              }
          }
          return null;
      }
  }
}
