﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;
using QBPOSEntities;
using System.ComponentModel;
using System.Globalization;
using OnlineEntities;


namespace OnlineDataProcessingsImportClass
{
    public class ImportOnlineVendorEntry
    {
        private static ImportOnlineVendorEntry m_ImportOnlineVendorEntry;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportOnlineVendorEntry()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportOnlineVendorEntry GetInstance()
        {
            if (m_ImportOnlineVendorEntry == null)
                m_ImportOnlineVendorEntry = new ImportOnlineVendorEntry();
            return m_ImportOnlineVendorEntry;
        }
        /// setting values to customer data table and returns collection.
        public OnlineDataProcessingsImportClass.OnlineVendorQBEntryCollection ImportOnlineVendorData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Employee Entry collections.
            OnlineDataProcessingsImportClass.OnlineVendorQBEntryCollection coll = new OnlineVendorQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry
            //DateTime VendorDt = new DateTime();
            string datevalue = string.Empty;
            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }


                    OnlineDataProcessingsImportClass.OnlineVendorQBEntry vendor = new OnlineDataProcessingsImportClass.OnlineVendorQBEntry();


                    if (dt.Columns.Contains("GivenName"))
                    {
                        #region Validations of NameOf
                        if (dr["GivenName"].ToString() != string.Empty)
                        {
                            if (dr["GivenName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This GivenName (" + dr["GivenName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.GivenName = dr["GivenName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.GivenName = dr["GivenName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.GivenName = dr["GivenName"].ToString();
                                }
                            }
                            else
                            {
                                vendor.GivenName = dr["GivenName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.MiddleName = dr["MiddleName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                vendor.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Title"))
                    {
                        #region Validations of Title
                        if (dr["Title"].ToString() != string.Empty)
                        {
                            if (dr["Title"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Title (" + dr["Title"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.Title = dr["Title"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.Title = dr["Title"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.Title = dr["Title"].ToString();
                                }
                            }
                            else
                            {
                                vendor.Title = dr["Title"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FamilyName"))
                    {
                        #region Validations of FamilyName
                        if (dr["FamilyName"].ToString() != string.Empty)
                        {
                            if (dr["FamilyName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FamilyName (" + dr["FamilyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.FamilyName = dr["FamilyName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.FamilyName = dr["FamilyName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.FamilyName = dr["FamilyName"].ToString();
                                }
                            }
                            else
                            {
                                vendor.FamilyName = dr["FamilyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Suffix"))
                    {
                        #region Validations of Suffix
                        if (dr["Suffix"].ToString() != string.Empty)
                        {
                            if (dr["Suffix"].ToString().Length > 10)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Suffix (" + dr["Suffix"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.Suffix = dr["Suffix"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.Suffix = dr["Suffix"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.Suffix = dr["Suffix"].ToString();
                                }
                            }
                            else
                            {
                                vendor.Suffix = dr["Suffix"].ToString();
                            }
                        }
                        #endregion
                    }
                                       

                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of compName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.CompanyName = dr["CompanyName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                vendor.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DisplayName"))
                    {
                        #region Validations of DisplayName
                        if (dr["DisplayName"].ToString() != string.Empty)
                        {
                            if (dr["DisplayName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {

                                    string strMessages = "This DisplayName (" + dr["DisplayName"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.DisplayName = dr["DisplayName"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.DisplayName = dr["DisplayName"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.DisplayName = dr["DisplayName"].ToString();
                                }
                            }
                            else
                            {
                                vendor.DisplayName = dr["DisplayName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Active"))
                    {
                        #region Validations of Description
                        if (dr["Active"].ToString() != string.Empty)
                        {
                            if (dr["Active"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Active (" + dr["Active"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.Active = dr["Active"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.Active = dr["Active"].ToString();
                                    }
                                }
                                else
                                {
                                    vendor.Active = dr["Active"].ToString();
                                }
                            }
                            else
                            {
                                vendor.Active = dr["Active"].ToString();
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.TelephoneNumber telephoneNo = new OnlineEntities.TelephoneNumber();
                    if (dt.Columns.Contains("PrimaryPhone"))
                    {
                        #region Validations of PrimaryPhone
                        if (dr["PrimaryPhone"].ToString() != string.Empty)
                        {
                            if (dr["PrimaryPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrimaryPhone (" + dr["PrimaryPhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.PrimaryPhone = dr["PrimaryPhone"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("AlternatePhone"))
                    {
                        #region Validations of AlternatePhone
                        if (dr["AlternatePhone"].ToString() != string.Empty)
                        {
                            if (dr["AlternatePhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AlternatePhone (" + dr["AlternatePhone"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.AlternatePhone = dr["AlternatePhone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Mobile"))
                    {
                        #region Validations of Mobile
                        if (dr["Mobile"].ToString() != string.Empty)
                        {
                            if (dr["Mobile"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Mobile (" + dr["Mobile"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.Mobile = dr["Mobile"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.Mobile = dr["Mobile"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.Mobile = dr["Mobile"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.Mobile = dr["Mobile"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (telephoneNo.PrimaryPhone != null)
                        vendor.PrimaryPhone.Add(telephoneNo);
                    if (telephoneNo.AlternatePhone != null)
                        vendor.AlternatePhone.Add(telephoneNo);
                    if (telephoneNo.Mobile != null)
                        vendor.Mobile.Add(telephoneNo);

                    if (telephoneNo.Fax != null)
                        vendor.Fax.Add(telephoneNo);

                    OnlineEntities.EmailAddressEmployee emailAddress = new OnlineEntities.EmailAddressEmployee();
                    if (dt.Columns.Contains("PrimaryEmailAddr"))
                    {
                        #region Validations of PrimaryEmailAddr
                        if (dr["PrimaryEmailAddr"].ToString() != string.Empty)
                        {
                            if (dr["PrimaryEmailAddr"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrimaryEmailAddr (" + dr["PrimaryEmailAddr"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                                    }
                                }
                                else
                                {
                                    emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                                }
                            }
                            else
                            {
                                emailAddress.EmailAddress = dr["PrimaryEmailAddr"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("WebAddr"))
                    {
                        #region Validations of WebAddr
                        if (dr["WebAddr"].ToString() != string.Empty)
                        {
                            if (dr["WebAddr"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This WebAddr (" + dr["WebAddr"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        emailAddress.WebAddress = dr["WebAddr"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        emailAddress.WebAddress = dr["WebAddr"].ToString();
                                    }
                                }
                                else
                                {
                                    emailAddress.WebAddress = dr["WebAddr"].ToString();
                                }
                            }
                            else
                            {
                                emailAddress.WebAddress = dr["WebAddr"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (emailAddress.EmailAddress != null)
                        vendor.PrimaryEmailAddr.Add(emailAddress);

                    if (emailAddress.WebAddress != null)
                        vendor.WebAddr.Add(emailAddress);


                    #region bill Addr

                    OnlineEntities.BillAddr billAddr = new OnlineEntities.BillAddr();
                    if (dt.Columns.Contains("BillAddr1"))
                    {
                        #region Validations of BillAddrLine1
                        if (dr["BillAddr1"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr1"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine1 = dr["BillAddr1"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine1 = dr["BillAddr1"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine1 = dr["BillAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr2"))
                    {
                        #region Validations of BillAddrLine2
                        if (dr["BillAddr2"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr2"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine2 = dr["BillAddr2"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine2 = dr["BillAddr2"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine2 = dr["BillAddr2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr3"))
                    {
                        #region Validations of BillAddrLine3
                        if (dr["BillAddr3"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr3"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine3 = dr["BillAddr3"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine3 = dr["BillAddr3"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine3 = dr["BillAddr3"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("BillAddr4"))
                    {
                        #region Validations of BillAddr4
                        if (dr["BillAddr4"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr4"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine4 = dr["BillAddr4"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine4 = dr["BillAddr4"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine4 = dr["BillAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr5"))
                    {
                        #region Validations of BillAddr5
                        if (dr["BillAddr5"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr5"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddr5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillLine5 = dr["BillAddr5"].ToString().Substring(0, 500);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillLine5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillLine5 = dr["BillAddr5"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillLine5 = dr["BillAddr5"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("BillCity"))
                    {
                        #region Validations of BillAddrCity
                        if (dr["BillCity"].ToString() != string.Empty)
                        {
                            if (dr["BillCity"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillCity (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCity = dr["BillCity"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCity = dr["BillCity"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCity = dr["BillCity"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCity = dr["BillCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillCountry"))
                    {
                        #region Validations of BillAddrCountry
                        if (dr["BillCountry"].ToString() != string.Empty)
                        {
                            if (dr["BillCountry"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillCountry (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCountry = dr["BillCountry"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCountry = dr["BillCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountry = dr["BillCountry"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCountry = dr["BillCountry"].ToString();
                            }
                        }
                        #endregion
                    }



                    if (dt.Columns.Contains("BillPostalCode"))
                    {
                        #region Validations of BillAddrPostalCode
                        if (dr["BillPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["BillPostalCode"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillPostalCode (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillPostalCode = dr["BillPostalCode"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillPostalCode = dr["BillPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillNote"))
                    {
                        #region Validations of BillAddrNote
                        if (dr["BillNote"].ToString() != string.Empty)
                        {
                            if (dr["BillNote"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillNote (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillNote = dr["BillNote"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillNote = dr["BillNote"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillNote = dr["BillNote"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillNote = dr["BillNote"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillAddrSubDivisionCode"))
                    {
                        #region Validations of BillAddrSubDivisionCode
                        if (dr["BillAddrSubDivisionCode"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrSubDivisionCode"].ToString().Length > 255)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrSubDivisionCode (" + dr["BillAddrSubDivisionCode"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString().Substring(0, 255);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillCountrySubDivisionCode = dr["BillAddrSubDivisionCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BillAddrLat"))
                    {
                        #region Validations of BillAddrLat
                        if (dr["BillAddrLat"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrLat"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrLat (" + dr["BillAddrLat"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillAddrLat = dr["BillAddrLat"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddrLong"))
                    {
                        #region Validations of BillAddrLong
                        if (dr["BillAddrLong"].ToString() != string.Empty)
                        {
                            if (dr["BillAddrLong"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillAddrLong (" + dr["BillAddrLong"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString().Substring(0, 1000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                    }
                                }
                                else
                                {
                                    billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                                }
                            }
                            else
                            {
                                billAddr.BillAddrLong = dr["BillAddrLong"].ToString();
                            }
                        }
                        #endregion
                    }



                    if (billAddr.BillLine1 != null || billAddr.BillLine2 != null || billAddr.BillLine3 != null || billAddr.BillLine4 != null || billAddr.BillLine5 != null || billAddr.BillCity != null || billAddr.BillCountry != null || billAddr.BillNote != null || billAddr.BillPostalCode != null)
                        vendor.BillAddr.Add(billAddr);

                   

                    
                    #endregion

                    if (dt.Columns.Contains("OtherContactInfo"))
                    {
                        #region Validations of PrimaryPhone
                        if (dr["OtherContactInfo"].ToString() != string.Empty)
                        {
                            if (dr["OtherContactInfo"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OtherContactInfo (" + dr["OtherContactInfo"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        telephoneNo.OtherContactInfo = dr["OtherContactInfo"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        telephoneNo.OtherContactInfo = dr["OtherContactInfo"].ToString();
                                    }
                                }
                                else
                                {
                                    telephoneNo.OtherContactInfo = dr["OtherContactInfo"].ToString();
                                }
                            }
                            else
                            {
                                telephoneNo.OtherContactInfo = dr["OtherContactInfo"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (telephoneNo.OtherContactInfo != null)
                        vendor.OtherContactInfo.Add(telephoneNo);

                    if (dt.Columns.Contains("TaxIdentifier"))
                    {
                        #region Validations of VendorTaxIdent
                        if (dr["TaxIdentifier"].ToString() != string.Empty)
                        {
                            if (dr["TaxIdentifier"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxIdentifier (" + dr["TaxIdentifier"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.TaxIdentifier = dr["TaxIdentifier"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.TaxIdentifier = dr["TaxIdentifier"].ToString();
                                    }
                                }
                                else
                                    vendor.TaxIdentifier = dr["TaxIdentifier"].ToString();
                            }
                            else
                            {
                                vendor.TaxIdentifier = dr["TaxIdentifier"].ToString();
                            }
                        }
                        #endregion
                    }

                    OnlineEntities.CustomerRef TermRef = new OnlineEntities.CustomerRef();
                    if (dt.Columns.Contains("TermRef"))
                    {
                        #region Validations of TermRef
                        if (dr["TermRef"].ToString() != string.Empty)
                        {
                            if (dr["TermRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TermRef (" + dr["TermRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TermRef.Name = dr["TermRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TermRef.Name = dr["TermRef"].ToString();
                                    }
                                }
                                else
                                {
                                    TermRef.Name = dr["TermRef"].ToString();
                                }
                            }
                            else
                            {
                                TermRef.Name = dr["TermRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    vendor.TermRef.Add(TermRef);

                    if (dt.Columns.Contains("AcctNum"))
                    {
                        if (dr["AcctNum"] != null)
                            vendor.AcctNum = dr["AcctNum"].ToString();
                    }
                    //vendor.AcctNum.Add(AcctNum);

                    //if (dt.Columns.Contains("AcctNum"))
                    //{
                    //    #region Validations of AcctNum
                    //    if (dr["AcctNum"].ToString() != string.Empty)
                    //    {
                    //        if (dr["AcctNum"].ToString().Length > 100)
                    //        {
                    //            if (isIgnoreAll == false)
                    //            {
                    //                string strMessages = "This AcctNum (" + dr["AcctNum"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                    //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                    //                if (Convert.ToString(result) == "Cancel")
                    //                {
                    //                    continue;
                    //                }
                    //                if (Convert.ToString(result) == "No")
                    //                {
                    //                    return null;
                    //                }
                    //                if (Convert.ToString(result) == "Ignore")
                    //                {
                    //                    vendor.AcctNum = dr["AcctNum"].ToString().Substring(0, 100);
                    //                }
                    //                if (Convert.ToString(result) == "Abort")
                    //                {
                    //                    isIgnoreAll = true;
                    //                    vendor.AcctNum = dr["AcctNum"].ToString();
                    //                }
                    //            }
                    //            else
                    //            {
                    //                vendor.AcctNum = dr["AcctNum"].ToString();
                    //            }
                    //        }
                    //        else
                    //        {
                    //            vendor.AcctNum = dr["AcctNum"].ToString();
                    //        }
                    //    }
                    //    #endregion
                    //}

                    if (dt.Columns.Contains("Vendor1099"))
                    {
                        #region Validations of Vendor1099
                        if (dr["Vendor1099"].ToString() != "<None>" || dr["Vendor1099"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["Vendor1099"].ToString(), out result))
                            {
                                vendor.Vendor1099 = Convert.ToInt32(dr["Vendor1099"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["Vendor1099"].ToString().ToLower() == "true")
                                {
                                    vendor.Vendor1099 = dr["Vendor1099"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["Vendor1099"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        vendor.Vendor1099 = dr["Vendor1099"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor1099 (" + dr["Vendor1099"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            vendor.Vendor1099 = dr["Vendor1099"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vendor.Vendor1099 = dr["Vendor1099"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vendor.Vendor1099 = dr["Vendor1099"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Balance"))
                    {
                        #region Validations for OpenBalance
                        if (dr["Balance"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Balance"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Balance( " + dr["Balance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vendor.Balance = dr["Balance"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vendor.Balance = dr["Balance"].ToString();
                                    }
                                }
                                else
                                    vendor.Balance = dr["Balance"].ToString();
                            }
                            else
                            {
                                vendor.Balance = string.Format("{0:00000000.00}", Convert.ToDouble(dr["Balance"].ToString()));
                            }
                        }
                        #endregion
                    }



                    OnlineEntities.CurrencyRef CustomerRef = new OnlineEntities.CurrencyRef();

                    if (dt.Columns.Contains("CurrencyRef"))
                    {
                        #region Validations of CustomerRefName
                        if (dr["CurrencyRef"].ToString() != string.Empty)
                        {
                            if (dr["CurrencyRef"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CurrencyRef (" + dr["CurrencyRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        CustomerRef.Name = dr["CurrencyRef"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        CustomerRef.Name = dr["CurrencyRef"].ToString();
                                    }
                                }
                                else
                                {
                                    CustomerRef.Name = dr["CurrencyRef"].ToString();
                                }
                            }
                            else
                            {
                                CustomerRef.Name = dr["CurrencyRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    vendor.CurrencyRef.Add(CustomerRef);

                    OnlineEntities.APAccountRef APAccountRef = new OnlineEntities.APAccountRef();
                    if (dt.Columns.Contains("APAccountRef"))
                    {
                        #region Validations of APAccountRefName
                        if (dr["APAccountRef"].ToString() != string.Empty)
                        {
                            if (dr["APAccountRef"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This APAccountRef (" + dr["APAccountRef"].ToString() + ") is exceeded maximum length of quickbooks online .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        APAccountRef.Name = dr["APAccountRef"].ToString().Substring(0, 30);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        APAccountRef.Name = dr["APAccountRef"].ToString();
                                    }
                                }
                                else
                                {
                                    APAccountRef.Name = dr["APAccountRef"].ToString();
                                }
                            }
                            else
                            {
                                APAccountRef.Name = dr["APAccountRef"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (APAccountRef.Name != null)
                    {
                        vendor.APAccountRef.Add(APAccountRef);
                    }


                    coll.Add(vendor);

                }
                else
                {
                    return null;
                }
            }

            #endregion

            
            #endregion
          
            return coll;

        }
    }
}
